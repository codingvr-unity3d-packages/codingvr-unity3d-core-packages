﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class FlexibleGridLayout : LayoutGroup
{
    public enum FitType
    {
        Uniform,
        Width,
        Height,
        FixedRows,
        FixedColumns
    }

    public enum ContentType
    {
        NoContentFitter,
        VerticalContentFitter,
        HorizontalContentFitter,
        BothContentFitter
    }

    public FitType fitType;
    public ContentType contentType;

    public int rows;
    public int columns;
    public Vector2 cellSize;
    public Vector2 spacing;

    public bool fitX;
    public bool fitY;

    //public float fixCellWidth;
    //public float fixCell

    private List<LayoutElement> listLayoutElements = new List<LayoutElement>();

    public class LayoutElement
    {
        public int indexX = 0;
        public int indexY = 0;
        public float posX = 0;
        public float posY = 0;
        public float width = 0;
        public float height = 0;
    }
   
    public override void CalculateLayoutInputHorizontal()
    {
        base.CalculateLayoutInputHorizontal();

        if(fitType == FitType.Width || fitType == FitType.Height || fitType == FitType.Uniform)
        {
            fitX = true;
            fitY = true;

            float sqrT = Mathf.Sqrt(transform.childCount);
            rows = Mathf.CeilToInt(sqrT);
            columns = Mathf.CeilToInt(sqrT);
        }

        if(fitType == FitType.Width || fitType == FitType.FixedColumns)
        {
            rows = Mathf.CeilToInt(transform.childCount / (float)columns);
        }

        if(fitType == FitType.Height || fitType == FitType.FixedRows)
        {
            columns = Mathf.CeilToInt(transform.childCount / (float)rows);
        }

       

        float parentWidth = rectTransform.rect.width;
        float parentHeight = rectTransform.rect.height;

        float cellWidth = 0;
        float cellHeight = 0;

        if (contentType == ContentType.VerticalContentFitter)
        {
            cellWidth = CalculateCellWidth(parentWidth);
            parentHeight = ((float)rows * (cellWidth + spacing.y)) + padding.top + padding.bottom;
            rectTransform.sizeDelta = new Vector2(parentWidth, parentHeight);
            cellHeight = CalculateCellHeight(parentHeight);
        }

        else if (contentType == ContentType.HorizontalContentFitter)
        {
            cellHeight = CalculateCellHeight(parentHeight);
            parentWidth = ((float)columns * (cellHeight + spacing.x)) + padding.right + padding.left;
            rectTransform.sizeDelta = new Vector2(parentWidth, parentHeight);
            cellWidth = CalculateCellWidth(parentWidth);
        }

        else if (contentType == ContentType.BothContentFitter)
        {
            //cellWidth =
            parentWidth = 0;
            parentHeight = 0;
        }

        else
        {
            cellWidth = CalculateCellWidth(parentWidth);
            cellHeight = CalculateCellHeight(parentHeight);
        }

        cellSize.x = fitX ? cellWidth : cellSize.x;
        cellSize.y = fitY ? cellHeight : cellSize.y;

        int columnCount = 0;
        int rowCount = 0;

        int maxColumnCount = 0;
        int maxRowCount = 0;

        /*
        if (contentType == ContentType.BothContentFitter)
        {
            if(rectChildren.Count > 0)
            {
                cellSize.x = rectChildren[0].rect.width;
                cellSize.y = rectChildren[0].rect.height;

                for (int i = 0; i < rectChildren.Count; i++)
                {
                    var item = rectChildren[i];

                    if(cellSize.x < item.rect.width)
                    {
                        cellSize.x = item.rect.width;
                    }

                    if(cellSize.y < item.rect.height)
                    {
                        cellSize.y = item.rect.height;
                    }
                }
            }    
        }
        */

        listLayoutElements = new List<LayoutElement>();

        float widthParent = 0.0f;
        float heightParent = 0.0f;

        for (int i = 0; i < rectChildren.Count; i++)
        {
            rowCount = i / columns;
            columnCount = i % columns;

            if(rowCount > maxRowCount)
            {
                maxRowCount = rowCount;
            }

            if(columnCount > maxColumnCount)
            {
                maxColumnCount = columnCount;
            }

            var item = rectChildren[i];

            var xPos = 0.0f;
            var yPos = 0.0f;

            if (contentType == ContentType.BothContentFitter)
            {
                cellSize.x = item.rect.width;
                cellSize.y = item.rect.height;

                widthParent = widthParent + cellSize.x;
                heightParent = heightParent + cellSize.y;

                LayoutElement element = new LayoutElement();
                element.indexX = columnCount;
                element.indexY = rowCount;
                element.width = cellSize.x;
                element.height = cellSize.y;

               if(columnCount == 0)
                {
                    xPos = padding.left;
                    
                }

                else
                {
                    LayoutElement elemX = GetLayoutElement(columnCount - 1, rowCount);
                    xPos = elemX.posX + elemX.width + spacing.x;
                }


               if(rowCount == 0)
                {
                    yPos = padding.top;
                }

                else
                {
                    LayoutElement elemY = GetLayoutElement(columnCount, rowCount-1);
                    yPos = elemY.posY + elemY.height + spacing.y;
                }

                element.posX = xPos;
                element.posY = yPos;
                listLayoutElements.Add(element);
            }

            else
            {
                 xPos = (cellSize.x * columnCount) + (spacing.x * columnCount) + padding.left;
                 yPos = (cellSize.y * rowCount) + (spacing.y * rowCount) + padding.top;

            }
                  
            SetChildAlongAxis(item, 0, xPos, cellSize.x);
            SetChildAlongAxis(item, 1, yPos, cellSize.y);
        }

        if(contentType == ContentType.BothContentFitter)
        {
            /*
            parentWidth = (cellSize.x * (maxColumnCount +1)) + (spacing.x * maxColumnCount) + padding.left + padding.right;
            parentHeight = (cellSize.y * (maxRowCount +1)) + (spacing.y * maxRowCount) + padding.top + padding.bottom;
            */
            /*
            parentWidth = widthParent + (spacing.x * maxColumnCount) + padding.left + padding.right;
            parentHeight = heightParent + (spacing.y * maxRowCount) + padding.top + padding.bottom;
            */

            parentWidth = CalculateSizeParent(false) + (spacing.x * maxColumnCount) + padding.left + padding.right;
            parentHeight = CalculateSizeParent(true) + (spacing.y * maxRowCount) + padding.top + padding.bottom;

            rectTransform.sizeDelta = new Vector2(parentWidth, parentHeight);
            rectTransform.anchoredPosition = new Vector2((cellSize.x * maxColumnCount + spacing.x * maxColumnCount)/2,0);
        }
    }

    private LayoutElement GetLayoutElement(int indexX,int indexY)
    {
        LayoutElement element = null;

        foreach(LayoutElement elem in listLayoutElements)
        {
            if(elem.indexX == indexX && elem.indexY == indexY)
            {
                element = elem;
            }
        }

        return element;
    }

    private List<LayoutElement> GetLayoutElements(int index, bool isX)
    {
        List<LayoutElement> layoutElements = new List<LayoutElement>();

        foreach (LayoutElement element in listLayoutElements)
        {
            if ((isX && element.indexX == index) || (!isX && element.indexY == index))
            {
                layoutElements.Add(element);
            }
        }

        return layoutElements;
    }

    private float CalculateSizeParent(bool isX)
    {
        float size = 0.0f;

        int nb = 0;

        if (isX)
        {
            nb = GetMaxColumns();
        }

        else
        {
            nb = GetMaxRows();
        }

        for(int i = 0;i<= nb; i++)
        {
            float sizeTemp = 0.0f;

            List<LayoutElement> layoutElements = GetLayoutElements(i, isX);

            foreach(LayoutElement elem in layoutElements)
            {
                if (isX)
                {
                    sizeTemp = sizeTemp + elem.height;
                }

                else
                {
                    sizeTemp = sizeTemp + elem.width;
                }
            }

            if(sizeTemp > size)
            {
                size = sizeTemp;
            }
        }

        return size;
    }

    private int GetMaxColumns()
    {
        int columns = 0;

        foreach(LayoutElement elem in listLayoutElements)
        {
            if (elem.indexX > columns)
            {
                columns = elem.indexX;
            }
        }

        return columns;
    }

    private int GetMaxRows()
    {
        int rows = 0;

        foreach (LayoutElement elem in listLayoutElements)
        {
            if (elem.indexY > rows)
            {
                rows = elem.indexY;
            }
        }

        return rows;
    }

    private float CalculateCellWidth(float parentWidth)
    {
        return parentWidth / (float)columns - ((spacing.x / (float)columns) * (columns - 1)) - (padding.left / (float)columns) - (padding.right / (float)columns);
    }

    private float CalculateCellHeight(float parentHeight)
    {
        return parentHeight / (float)rows - ((spacing.y / (float)rows) * 2) - (padding.top / (float)rows) - (padding.bottom / (float)rows);
    }

    public override void CalculateLayoutInputVertical()
    {

    }

    public override void SetLayoutHorizontal()
    {

    }

    public override void SetLayoutVertical()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
