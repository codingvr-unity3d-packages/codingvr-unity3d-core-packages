﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameCreator.Core;
using DG.Tweening;

namespace codingVR.InputControls
{
    public class TriggerInteraction : MonoBehaviour
    {
        [HideInInspector]
        [SerializeField]
        Button bInteraction;

        [SerializeField]
        Actions actions;

        [SerializeField]
        float durationFade;

        // Start is called before the first frame update
        void Start()
        {
            bInteraction.onClick.AddListener(() => OnClickInteraction());
        }

        void OnEnable()
        {
            Fade(false);
        }

        private void OnClickInteraction()
        {
            if (actions != null)
            {
                var actions = this.actions;
                actions.Execute();
            }

            Fade(true);
        }

        private void Fade(bool isFadingOut)
        {
            CanvasGroup canvasGroup = this.gameObject.GetComponentInChildren<CanvasGroup>();
            int fade = 0;

            if (isFadingOut)
            {
                fade = 0;
                canvasGroup.blocksRaycasts = false;
            }
            else
            {
                fade = 1;
                canvasGroup.blocksRaycasts = true;
            }

            canvasGroup.DOFade(fade, durationFade).OnComplete(() =>
            {
                if (isFadingOut == true) this.gameObject.SetActive(false);
            });
        }
    }
}
