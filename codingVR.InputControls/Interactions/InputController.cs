﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.InputControls
{
    public class InputController : MonoBehaviour
    {
        private Core.IEventManager eventManager;

        [Inject]
        public void Constructor(Core.IEventManager eventManager)
        {
            this.eventManager = eventManager;
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0) && Time.timeScale == 1f)
            {
                eventManager.QueueEvent(new Data.Event_OnClickMouse());
            }
        }
    }
}