﻿using UnityEngine.SceneManagement;

namespace codingVR.Scene
{
	public delegate void LoadingDone(string sceneName);

	public interface ISceneLoader
	{
		void LoadScene(string sceneName, string loaderSceneName, LoadSceneMode mode, bool addressables, LoadingDone done);
		bool UnloadScene(string sceneName);

		string CurrentScene { get; }
		float LoadingProgress { get; }
		bool IsDownloading { get; }
		bool IsFailed { get; }
	}
}