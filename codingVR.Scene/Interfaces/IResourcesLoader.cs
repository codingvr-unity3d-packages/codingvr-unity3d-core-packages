﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Zenject;

namespace codingVR.Scene
{
	public delegate void GameObjectInstantiationDone(GameObject go);

	public interface IResourcesLoader
	{
		// load resource 
		void LoadResource(string resourcePath, bool addressables);
		void LoadResourceAsset(AssetReference assetReference);
		void LoadResourcesAsset(List<AssetReference> assetReferences);

		// get resources
		UnityEngine.Object GetResource(string resourcePath);
		UnityEngine.Object GetResource(AssetReference assetReference);
		
		// check bound resources
		bool ContainResource(AssetReference assetReference);
		bool ContainResource(string resourcePath);
		List<AssetReference> FindUnboundResources(List<AssetReference> assetReferences);

		// coroutine way
		IEnumerator Wait(string resourcePath);
		IEnumerator Wait(AssetReference assetReference);
		IEnumerator Wait(List<AssetReference> assetReference);
		IEnumerator WaitAll();

		// Async way
		Task WaitASync(AssetReference assetReference);
		Task WaitASync(List<AssetReference> assetReference);
		
		// extensions
		IEnumerator InstantiateGameObject(DiContainer diContainer, AssetReference assetReference, Vector3 position, Quaternion rotation, Transform parent, GameObjectInstantiationDone done);
	}
}