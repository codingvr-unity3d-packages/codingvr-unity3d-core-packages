﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Scene
{
    public interface ISceneContext
    {
        GameObject SceneUITransition { get; }
    }
}