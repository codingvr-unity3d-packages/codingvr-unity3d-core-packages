﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Zenject;

namespace codingVR.Scene
{
	public class ResourcesLoader : MonoBehaviour, IResourcesLoader
	{
		private Dictionary<string, ResourceRequest> resources = new Dictionary<string, ResourceRequest>();

		private Dictionary<string, AsyncOperationHandle<UnityEngine.Object>> addressables = new Dictionary<string, AsyncOperationHandle<UnityEngine.Object>>();

		/*
		 * 
		 * LoadResource
		 * 
		 */

		public void LoadResource(string resourcePath, bool addressableResource)
		{
			if (addressableResource == false)
			{
				if (resources.ContainsKey(resourcePath) == false)
				{
					resources.Add(resourcePath, Resources.LoadAsync(resourcePath));
				}
				else
				{
					Debug.LogErrorFormat("{0} : resource path {1} already added !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, resourcePath);
				}
			}
			else
			{
				if (addressables.ContainsKey(resourcePath) == false)
				{
					addressables.Add(resourcePath, Addressables.LoadAssetAsync<UnityEngine.Object>(resourcePath));
				}
				else
				{
					Debug.LogErrorFormat("{0} : addressables path {1} already added !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, resourcePath);
				}
			}
		}

		public void LoadResourceAsset(AssetReference assetReference)
		{
			var assetReferencePath = assetReference.AssetGUID;
			if (addressables.ContainsKey(assetReferencePath) == false)
			{
				var async = assetReference.LoadAssetAsync<UnityEngine.Object>();
				addressables.Add(assetReferencePath, async);
			}
			else
			{
				Debug.LogErrorFormat("{0} : addressables path {1} already added !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, assetReference);
			}
		}

		public void LoadResourcesAsset(List<AssetReference> assetReferences)
		{
			foreach (var it in assetReferences)
			{
				LoadResourceAsset(it);
			}
		}

		/*
		 * 
		 * Get Resources
		 * 
		 */

		UnityEngine.Object GetResource(Dictionary<string, ResourceRequest> dictionary, string resourcePath)
		{
			UnityEngine.Object result = null;
			var ret = dictionary.ContainsKey(resourcePath);
			if (ret == true)
			{
				var request = dictionary[resourcePath];
				if (request.isDone == true)
				{
					result = request.asset;
				}
			}
			return result;
		}

		UnityEngine.Object GetResurceAddressable(Dictionary<string, AsyncOperationHandle<UnityEngine.Object>> dictionary, string addressablePath)
		{
			UnityEngine.Object result = null;
			var ret = dictionary.ContainsKey(addressablePath);
			if (ret == true)
			{
				var request = dictionary[addressablePath];
				if (request.IsDone == true)
				{
					switch (request.Status)
					{
						case AsyncOperationStatus.None:
							Debug.LogErrorFormat("{0} : adressable resource loading {1} has failed with unknow status !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, addressablePath);
							break;
						case AsyncOperationStatus.Succeeded:
							result = request.Result;
							//Debug.LogErrorFormat("{0} : adressable resource loading {1}=>{2} has succeded !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, addressablePath, result);
							break;
						case AsyncOperationStatus.Failed:
							Debug.LogErrorFormat("{0} : adressable resource loading {1} has failed !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, addressablePath);
							break;
					}
				}
				else
				{
					//Debug.LogErrorFormat("{0} : adressable resource {1} BREAK 2 !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, addressablePath);
				}
			}
			else
			{
				//Debug.LogErrorFormat("{0} : adressable resource {1} BREAK 1 !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, addressablePath);
			}

			return result;
		}

		public UnityEngine.Object GetResource(string resourcePath)
		{
			var asset = GetResource(resources, resourcePath);
			if (asset == null)
			{
				asset = GetResurceAddressable(addressables, resourcePath);
			}
			if (asset == null)
			{
				Debug.LogErrorFormat("{0} : resource {1} not found !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, resourcePath);
			}
			return asset;
		}

		public UnityEngine.Object GetResource(AssetReference assetReference)
		{
			return GetResource(assetReference.AssetGUID);
		}

		/*
		 * 
		 * Contain Resources
		 * 
		 */

		bool ContainResource(Dictionary<string, ResourceRequest> dictionary, string resourcePath)
		{
			return dictionary.ContainsKey(resourcePath);
		}

		bool ContainResourceAddressable(Dictionary<string, AsyncOperationHandle<UnityEngine.Object>> dictionary, string addressablePath)
		{
			return dictionary.ContainsKey(addressablePath);
		}

		public bool ContainResource(AssetReference assetReference)
		{
			return ContainResource(assetReference.AssetGUID);
		}

		public List<AssetReference> FindUnboundResources(List<AssetReference> assetReferences)
		{
			List <AssetReference> unboundAssetReferences = new List<AssetReference>();
			foreach (var it in assetReferences)
			{
				var b = ContainResource(it);
				if (b == false)
				{
					unboundAssetReferences.Add(it);
				}
			}
			return unboundAssetReferences;
		}

		public bool ContainResource(string resourcePath)
		{
			var ret = ContainResource(resources, resourcePath);
			if (ret == false)
			{
				ret = ContainResourceAddressable(addressables, resourcePath);
			}
			return ret;
		}

		/*
		 * 
		 * Wait - coRoutines
		 * 
		 */

		public IEnumerator Wait(string resourcePath)
		{
			var ret = resources.ContainsKey(resourcePath);
			if (ret == true)
			{
				var request = resources[resourcePath];
				yield return request;
			}
			else
			{
				ret = addressables.ContainsKey(resourcePath);
				if (ret == true)
				{
					var request = addressables[resourcePath];
					yield return request;
				}
			}
			yield return null;
		}

		public IEnumerator Wait(AssetReference assetReference)
		{
			var ret = addressables.ContainsKey(assetReference.AssetGUID);
			if (ret == true)
			{
				var request = addressables[assetReference.AssetGUID];
				switch (request.Status)
				{
					case AsyncOperationStatus.None:
						yield return request;
						break;
					case AsyncOperationStatus.Succeeded:
						break;
					case AsyncOperationStatus.Failed:
						Debug.LogErrorFormat("{0} : adressable resource loading {1} has failed !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, assetReference);
						break;
				}
			}
			else
			{
				Debug.LogErrorFormat("{0} : wait failed on path {1} not bound to db !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, assetReference);
			}
			yield return null;
		}

		public IEnumerator Wait(List<AssetReference> assetReference)
		{
			foreach (var it in assetReference)
			{
				yield return Wait(it);
			}

			yield return null;
		}

		public IEnumerator WaitAll()
		{
			foreach (var it in resources)
			{
				yield return it.Value;
			}
			foreach (var it in addressables)
			{
				yield return it.Value;
			}
			yield return null;
		}

		/*
		 * 
		 * Wait - Async
		 * 
		 */

		public async Task WaitASync(AssetReference assetReference)
		{
			var ret = addressables.ContainsKey(assetReference.AssetGUID);
			if (ret == true)
			{
				var request = addressables[assetReference.AssetGUID];
				await request.Task;
			}
			else
			{
				Debug.LogErrorFormat("{0} : wait failed on path {1} not bound to db !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, assetReference);
			}
		}

		public async Task WaitASync(List<AssetReference> assetReference)
		{
			foreach(var it in assetReference)
			{
				await WaitASync(it);
			}
		}

		/*
		 * 
		 * Extensions
		 * 
		 */

		GameObject ErrorGizmo(string assetRessourcesPath, Vector3 position, Quaternion rotation, Transform parent)
		{
			var _prefab = Resources.Load("GameObjectDrawGizmosError");
			GameObject _gameObject = null;
			if (parent != null)
				_gameObject = GameObject.Instantiate(_prefab, position, rotation, parent) as GameObject;
			else
				_gameObject = GameObject.Instantiate(_prefab, position, rotation) as GameObject;
			codingVR.Core.GameObjectDrawGizmos gameObjectError = _gameObject.GetComponent<codingVR.Core.GameObjectDrawGizmos>();
			gameObjectError.Title = assetRessourcesPath + "\nnot found !!!";
			return _gameObject;
		}
		
		public IEnumerator InstantiateGameObject(DiContainer diContainer, AssetReference assetReference, Vector3 position, Quaternion rotation, Transform parent, GameObjectInstantiationDone done)
		{
			// once the object is loaded; we setp object
			UnityEngine.Object SetupObject()
			{
				var obj = GetResource(assetReference);
				if (obj != null)
				{
					// and instantiate
					if (parent != null)
						obj = GameObject.Instantiate(obj, position, rotation, parent) as GameObject;
					else
						obj = GameObject.Instantiate(obj, position, rotation) as GameObject;
					if (obj is GameObject gameObject)
					{
						var cp = gameObject.GetComponentsInChildren<MonoBehaviour>();
						foreach (var it in cp)
						{
							diContainer.Inject(it);
						}
					}
				}
				else
				{
					obj = ErrorGizmo(name, position, rotation, parent);
				}
				done?.Invoke(obj as GameObject);
				return obj;
			}
			
			// check if resource already loaded
			var has = ContainResource(assetReference);
			if (has == false)
			{
				// otherwide we load the resource
				LoadResourceAsset(assetReference);
				// waiting async loading
				yield return Wait(assetReference);
				// setup object
				SetupObject();
			}
			else
			{
				// waiting async loading
				yield return Wait(assetReference);
				// setup object
				SetupObject();
			}

			yield return null;
		}

	}
}