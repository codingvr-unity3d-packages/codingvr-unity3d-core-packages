﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using Zenject;

namespace codingVR.Scene
{
    public class SceneLoader : MonoBehaviour, ISceneLoader
    {
        public class SceneLoaderSingle
        {
            // === ISceneLoader implementation ===

            private float loadingProgress;
            public float LoadingProgress { get { return loadingProgress; } }

            private bool isDownloading = false;
            public bool IsDownloading { get { return isDownloading; } }

            private bool isFailed = false;
            public bool IsFailed { get { return isFailed; } }

            private string currentScene = "";
            public string CurrentScene { get { return currentScene; } }

            private SceneLoaderGlobalState sceneLoaderGlobalState;

            codingVR.Core.IEventManager eventManager;

            public SceneLoaderSingle(codingVR.Core.IEventManager eventManager, SceneLoaderGlobalState sceneLoaderGlobalState)
            {
                this.sceneLoaderGlobalState = sceneLoaderGlobalState;
                this.eventManager = eventManager;
            }

            public void LoadScene(Core.ICoRoutineManager coRoutineManager, string sceneName, string loaderSceneName, LoadSceneMode mode, bool addressables, LoadingDone done)
            {
                // kick-off the one co-routine to rule them all
                coRoutineManager.StartCoRoutine(sceneName, LoadScenesInOrder(sceneName, loaderSceneName, mode, addressables, done));
            }

            public bool UnloadScene(string sceneName)
            {
                var ret = UnloadAddressablesGameScene(sceneName);
                if (ret == false)
                {
                    ret = UnloadGameScene(sceneName);
                }
                return ret;
            }

            // === SceneLoader implementation ===

            private IEnumerator LoadScenesInOrder(string sceneName, string loaderSceneName, LoadSceneMode mode, bool addressables, LoadingDone done)
            {
                //yield return new WaitWhile(() => sceneLoaderGlobalState.IsBusy != 0);

                // reset failure
                isFailed = false;
                // is loading
                isDownloading = false;
                // current scene
                currentScene = sceneName;

                // LoadSceneAsync() returns an AsyncOperation, 
                // so will only continue past this point when the Operation has finished
                string loadingScene = loaderSceneName;


                var sceneGameIndex = SceneManager.GetSceneByName(sceneName);
                if (sceneGameIndex.isLoaded == false)
                {
                    yield return new WaitWhile(() => sceneLoaderGlobalState.IsBusy != 0);
                    sceneLoaderGlobalState.IsBusy++;
                    sceneLoaderGlobalState.CountLoadingScreenStarted++;
                    if (sceneLoaderGlobalState.CountLoadingScreenStarted == 1)
                    {

                        var sceneLoadingIndex = SceneManager.GetSceneByName(loadingScene);
                        if (sceneLoadingIndex.isLoaded == false)
                        {
                            // load the "loading screen"
                            var asyncScene = SceneManager.LoadSceneAsync(loadingScene, LoadSceneMode.Additive);
                            // Wait until the asynchronous scene fully loads
                            yield return new WaitUntil(() => asyncScene.isDone == true);
                            // IT Seems that asyncScene.isDone is not sufficient enough to determine if the scene is loaded 
                            yield return new WaitUntil(() => SceneManager.GetSceneByName(loadingScene).isLoaded == true);
                        }
                    }
                    /*else
					{
						Debug.LogError("loaded already " + sceneLoaderGlobalState.CountLoadingScreenStarted);
					}*/
                    sceneLoaderGlobalState.IsBusy--;

                    // as soon as we've finished loading the loading screen, start loading the game scene
                    yield return LoadGameScene(sceneName, mode, addressables);

                    if (sceneLoaderGlobalState.CountLoadingScreenStarted > 0)
                    {
                        sceneLoaderGlobalState.CountLoadingScreenStarted--;
                        if (sceneLoaderGlobalState.CountLoadingScreenStarted == 0)
                        {
                            // we unload the "loading scene"
                            yield return new WaitWhile(() => sceneLoaderGlobalState.IsBusy != 0);
                            sceneLoaderGlobalState.IsBusy++;
                            var sceneLoadingIndex = SceneManager.GetSceneByName(loadingScene);
                            if (sceneLoadingIndex.isLoaded == true)
                            {
                                var asyncScene = SceneManager.UnloadSceneAsync(sceneLoadingIndex);
                                if (asyncScene != null)
                                {
                                    yield return new WaitUntil(() => asyncScene.isDone == true);
                                    // IT Seems that asyncScene.isDone is not sufficient enough to determine if the scene is unloaded 
                                    yield return new WaitUntil(() => SceneManager.GetSceneByName(loadingScene).isLoaded == false);
                                }
                                else
                                {
                                    Debug.LogErrorFormat("{0} : can't unload 'Loading' on that scene {1} ", System.Reflection.MethodBase.GetCurrentMethod().Name, sceneName);
                                }
                            }
                            sceneLoaderGlobalState.IsBusy--;
                        }
                    }
                }

                eventManager.QueueEvent(new Scene.Event_SceneLoaded(sceneName));

                // reset failure
                isFailed = false;
                // is loading
                isDownloading = false;
                // current scene
                currentScene = "";

                if (done != null)
                {
                    done(sceneName);
                }


                yield return null;
            }

            // === SceneManager Loader ===

            private Dictionary<string, AsyncOperation> scenes = new Dictionary<string, AsyncOperation>();

            private IEnumerator LoadGameScene(string sceneName, LoadSceneMode mode)
            {
                void OnSceneLoaded(AsyncOperation obj)
                {
                    loadingProgress = 100.0f;
                    scenes.Add(sceneName, obj);
                }

                var asyncScene = SceneManager.LoadSceneAsync(sceneName, mode);

                // delegate on onCompeted
                asyncScene.completed += OnSceneLoaded;

                // this value stops the scene from displaying when it's finished loading
                asyncScene.allowSceneActivation = false;

                while (!asyncScene.isDone)
                {
                    // loading bar progress
                    loadingProgress = Mathf.Clamp01(asyncScene.progress / 0.9f) * 100.0f;

                    // scene has loaded as much as possible, the last 10% can't be multi-threaded
                    if (asyncScene.progress >= 0.9f)
                    {
                        // we finally show the scene
                        asyncScene.allowSceneActivation = true;
                    }
                    yield return null;
                }

                yield return new WaitUntil(() => SceneManager.GetSceneByName(sceneName).isLoaded == true);

                yield return 0;
            }

            private bool UnloadGameScene(string sceneName)
            {
                var ret = scenes.ContainsKey(sceneName);
                if (ret == true)
                {
                    var sceneIndex = SceneManager.GetSceneByName(sceneName);
                    SceneManager.UnloadSceneAsync(sceneIndex);
                    scenes.Remove(sceneName);
                }
                return ret;
            }

            // === Addressables scene Loader ===

            // https://github.com/PixelLifetime/unity-game-development-stack-exchange/tree/master/Assets/%7B%40%23%7D%20Answers/Preload%20multiple%20scenes%20at%20the%20same%20time%20and%20activate%20them%20on%20demand%20in%20Unity.%20%5BUnity%2C%20async%2C%20simultaneous%5D
            private Dictionary<string, AsyncOperationHandle<SceneInstance>> addressablesScenes = new Dictionary<string, AsyncOperationHandle<SceneInstance>>();

            private IEnumerator LoadAddressablesGameScene(string sceneName, LoadSceneMode mode)
            {
                void OnAddressablesSceneLoaded(AsyncOperationHandle<SceneInstance> obj)
                {
                    if (obj.Status == AsyncOperationStatus.Succeeded)
                    {
                        //obj.Result.ActivateAsync();
                        loadingProgress = 100.0f;
                        addressablesScenes.Add(sceneName, obj);
                        isFailed = false;
                    }
                    else
                    {
                        isFailed = true;
                        Debug.LogError("Failed to load scene at address: ");
                    }
                }

                AsyncOperationHandle<long> downloadingSize = Addressables.GetDownloadSizeAsync(sceneName);
                while (!downloadingSize.IsDone)
                {
                    yield return null;
                }
                if (downloadingSize.Result > 0)
                {
                    isDownloading = true;
                }
                else
                {
                    isDownloading = false;
                }


                // see https://docs.unity3d.com/Packages/com.unity.resourcemanager@2.2/api/UnityEngine.ResourceManagement.IAsyncOperation-1.html?_ga=2.141585970.1247589960.1616282392-351060288.1585493398
                AsyncOperationHandle<SceneInstance> asyncScene = Addressables.LoadSceneAsync(key: sceneName, LoadSceneMode.Additive, true);

                // delegate on onCompeted
                asyncScene.Completed += OnAddressablesSceneLoaded;

                while (!asyncScene.IsDone && asyncScene.Status != AsyncOperationStatus.Failed)
                {
                    // loading bar progress
                    loadingProgress = Mathf.Clamp01(asyncScene.PercentComplete / 0.9f) * 100.0f;

                    //Debug.LogError("Progressing " + asyncScene.PercentComplete + ".... ");

                    // scene has loaded as much as possible, the last 10% can't be multi-threaded
                    if (asyncScene.PercentComplete >= 0.9f)
                    {
                        // we finally show the scene
                        //asyncScene.Result.ActivateAsync();
                    }

                    yield return null;
                }

                if (asyncScene.Status == AsyncOperationStatus.Failed)
                {
                    isFailed = true;
                }
                else
                {
                    isFailed = false;
                }

                if (isFailed == false)
                {
                    yield return new WaitUntil(() => SceneManager.GetSceneByName(name: sceneName).isLoaded == true);
                }

                yield return 0;
            }

            private bool UnloadAddressablesGameScene(string sceneName)
            {
                var ret = addressablesScenes.ContainsKey(sceneName);
                if (ret == true)
                {
                    Addressables.UnloadSceneAsync(addressablesScenes[sceneName]);
                    addressablesScenes.Remove(sceneName);
                }
                return ret;
            }

            // === Addressables & Scene loader ===

            // see also some examples
            private IEnumerator LoadGameScene(string sceneName, LoadSceneMode mode, bool addressables)
            {
                if (addressables == false)
                {
                    yield return LoadGameScene(sceneName, mode);
                }
                else
                {
                    yield return LoadAddressablesGameScene(sceneName, mode);
                }
                yield return 0;
            }
        }

        // === ISceneLoader implementation ===

        SceneLoaderSingle sceneLoaderSingle = null;

        public class SceneLoaderGlobalState
        {
            public int CountLoadingScreenStarted { get; set; } = 0;
            public int IsBusy { get; set; } = 0;
        }

        SceneLoaderGlobalState sceneLoaderGlobalState = new SceneLoaderGlobalState();

        private Core.IEventManager eventManager;
        private Core.ICoRoutineManager coRoutineManager;

        [Inject]
        public void Constructor(Core.IEventManager eventManager, Core.ICoRoutineManager coRoutineManager)
        {
            this.eventManager = eventManager;
            this.coRoutineManager = coRoutineManager;
        }

        void Awake()
        {
            sceneLoaderSingle = new SceneLoaderSingle(eventManager, sceneLoaderGlobalState);
        }

        public float LoadingProgress
        {
            get
            {
                return sceneLoaderSingle.LoadingProgress;
            }
        }

        public bool IsDownloading
        {
            get
            {
                return sceneLoaderSingle.IsDownloading;
            }
        }

        public bool IsFailed
        {
            get
            {
                return sceneLoaderSingle.IsFailed;
            }
        }

        public string CurrentScene
        {
            get
            {
                return sceneLoaderSingle.CurrentScene;
            }
        }

        public void LoadScene(string sceneName, string loaderSceneName, LoadSceneMode mode, bool addressables, LoadingDone done)
        {
            // kick-off the one co-routine to rule them all
            sceneLoaderSingle.LoadScene(coRoutineManager, sceneName, loaderSceneName, mode, addressables, done);
            /*
            IEnumerator f()
            {
                yield return coRoutineManager.PopToken();
            }
            StartCoroutine(f());
            */
        }

        public bool UnloadScene(string sceneName)
        {
            return sceneLoaderSingle.UnloadScene(sceneName);
        }
    }
}