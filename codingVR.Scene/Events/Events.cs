﻿using UnityEngine;

namespace codingVR.Scene
{
	// === SceneState =================================================================================================

	#region SceneState

	public class Event_SceneLoaded : Core.IGameEvent
	{
		public int Type
		{
			get;
		}

		public object Data
		{
			get;
		}
		
		public Event_SceneLoaded(string scene)
		{
			this.Data = scene;
		}
	};

	#endregion

	// === SceneState =================================================================================================

	#region Event_SceneSlotReady

	public class Event_SceneSlotLoaded : Core.IGameEvent
	{
		public int Type
		{
			get;
		}

		public object Data
		{
			get;
		}

		public Event_SceneSlotLoaded(Component slotManager, string sceneName)
		{
			this.Data = (slotManager, sceneName);
		}
	};

	#endregion
}
