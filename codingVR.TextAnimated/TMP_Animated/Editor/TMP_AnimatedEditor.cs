﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using TMPro.EditorUtilities;

namespace codingVR.TextAnimated
{
    [CustomEditor(typeof(TMP_Animated), true)]
    [CanEditMultipleObjects]
    public class TMP_AnimatedEditor : TMP_BaseEditorPanel
    {
        SerializedProperty speedProp;
        SerializedProperty animationsExpression;
        SerializedProperty animationsGestures;
        protected override void OnEnable()
        {
            base.OnEnable();
            speedProp = serializedObject.FindProperty("speed");
            animationsExpression = serializedObject.FindProperty("expressions");
            animationsGestures = serializedObject.FindProperty("gestures");

        }
        protected override void OnUndoRedo()
        {
        }
        protected override void DrawExtraSettings()
        {
            EditorGUILayout.LabelField("Animation Settings", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(speedProp, new GUIContent("     Default Speed"));
            EditorGUILayout.PropertyField(animationsExpression);
            EditorGUILayout.PropertyField(animationsGestures);

        }
        protected override bool IsMixSelectionTypes()
        {
            return false;
        }

    }
}