﻿
using UnityEngine;
using TMPro;
using DG.Tweening;

namespace codingVR.TextAnimated
{
    public class DialogueAudio : MonoBehaviour
    {
        private TMP_Animated animatedText;

        public AudioClip[] voices;
        public AudioClip[] punctuations;
        [Space]
        public AudioSource voiceSource;
        public AudioSource punctuationSource;

        //Link the dialogueAudio with a textAnimated to launch a sound
        //each time the text reveal a letter
        public void SetAnimatedText(TMP_Animated text)
        {
            if(this.animatedText != text)
            {
                if(this.animatedText != null)
                {
                    this.animatedText.onTextReveal.RemoveListener((newChar) => ReproduceSound(newChar));
                }

                this.animatedText = text;
                this.animatedText.onTextReveal.AddListener((newChar) => ReproduceSound(newChar));
            }          
        }

        //Launch a sound with a random selection of the sounds available
        public void ReproduceSound(char c)
        {

            if (char.IsPunctuation(c) && !punctuationSource.isPlaying)
            {
                voiceSource.Stop();
                punctuationSource.clip = punctuations[Random.Range(0, punctuations.Length)];
                punctuationSource.Play();
            }

            if (char.IsLetter(c) && !voiceSource.isPlaying)
            {
                punctuationSource.Stop();
                voiceSource.clip = voices[Random.Range(0, voices.Length)];
                voiceSource.Play();
            }

        }

    }

}
