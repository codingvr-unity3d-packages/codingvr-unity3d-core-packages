﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using GameCreator.Characters;

namespace codingVR.TextAnimated
{
	[System.Serializable] public class TextRevealEvent : UnityEvent<char> { }

	[System.Serializable] public class DialogueEvent : UnityEvent { }

	public class TMP_Animated : TextMeshProUGUI
	{
		[SerializeField] private float speed = 10;
		public TextRevealEvent onTextReveal;
		public DialogueEvent onDialogueFinish;

		public GameObject character = null;

		private Coroutine readCoroutine;

		public bool endRead = false;

		[SerializeField] Data.AnimationsScriptable expressions;
		[SerializeField] Data.AnimationsScriptable gestures;

		public void textAnimated(GameEngine.IPlayerStateManager playerStateManager, string newText, bool progressiveDisplay, GameObject character, bool returnTimer)
		{
			this.character = character;

			if (this.character != null)
			{
				DialogueAudio dialogueAudio = this.character.GetComponent<DialogueAudio>();
				// if (dialogueAudio != null) dialogueAudio.SetAnimatedText(this);
			}

			ReadText(playerStateManager, newText, progressiveDisplay, returnTimer);
		}

		private void ReadText(GameEngine.IPlayerStateManager playerStateManager, string newText, bool progressiveDisplay, bool returnTimer)
		{
			text = string.Empty;

			// split the whole text into parts based off the <> tags 
			// even numbers in the array are text, odd numbers are tags
			string[] subTexts = newText.Split('<', '>');

			// textmeshpro still needs to parse its built-in tags, so we only include noncustom tags
			string displayText = "";
			for (int i = 0; i < subTexts.Length; i++)
			{
				if (i % 2 == 0)
					displayText += subTexts[i];
				else if (subTexts[i].StartsWith("print="))
				{
					string textMessage = ParseString(playerStateManager, subTexts[i].Split('=')[1]);
					displayText += textMessage;
					subTexts[i - 1] += textMessage;
				}
				else if (!isCustomTag(subTexts[i].Replace(" ", "")))
					displayText += $"<{subTexts[i]}>";
			}
			// check to see if a tag is our own
			bool isCustomTag(string tag)
			{
				return tag.StartsWith("speed=") || tag.StartsWith("pause=") || tag.StartsWith("gesture=") || tag.StartsWith("expression=");
			}

			// send that string to textmeshpro and hide all of it, then start reading

			text = displayText;


			if (progressiveDisplay)
			{
				maxVisibleCharacters = 0;
				readCoroutine = StartCoroutine(Read());
			}

			IEnumerator Read()
			{
				int subCounter = 0;
				int visibleCounter = 0;
				while (subCounter < subTexts.Length && endRead == false)
				{
					// if the text is a tag it determines what actions to launch
					if (subCounter % 2 == 1)
					{

						yield return new WaitForSeconds(EvaluateTag(subTexts[subCounter].Replace(" ", "")));

					}
					else
					{
						//reveal one by one the characters of the text with the given speed
						while (visibleCounter < subTexts[subCounter].Length)
						{

							onTextReveal.Invoke(subTexts[subCounter][visibleCounter]);
							maxVisibleCharacters++;
							visibleCounter++;
							yield return new WaitForSeconds(1f / speed);

						}
						visibleCounter = 0;
					}
					subCounter++;
				}
				yield return null;

				//Determine and launch the actions depending of the value
				//of the tag given

				float EvaluateTag(string tag)
				{
					if (tag.Length > 0)
					{
						if (tag.StartsWith("speed="))
						{
							speed = float.Parse(tag.Split('=')[1]);
						}
						else if (tag.StartsWith("pause="))
						{
							return float.Parse(tag.Split('=')[1]);
						}

						else if (tag.StartsWith("expression="))
						{
							OnExpressionChange(tag.Split('=')[1]);
						}

						else if (tag.StartsWith("gesture="))
						{
							string content = tag.Split('=')[1];

							//Split again to see if the text must
							//wait the end of the gesture to continu
							string[] split = content.Split('|');

							bool wait = false;

							if (split.Length > 1)
							{
								if (split[1] == "wait") wait = true;
							}

							float waitTime = OnGestureChange(split[0], wait);

							return waitTime;
						}
					}
					return 0;
				}
				Character charTarget = this.character.GetComponentInChildren<Character>();
				CharacterAnimator characterAnimator = charTarget.GetCharacterAnimator();
				characterAnimator.StopGesture(1.0f);
				onDialogueFinish.Invoke();
			}
		}

		public void EndReadText()
		{
			endRead = true;
			maxVisibleCharacters = text.Length;
		}


		public void OnExpressionChange(string nameExpression)
		{

			int index = expressions.GetIndexByName(nameExpression);

			Animator[] animators = this.character.GetComponentsInChildren<Animator>();
			Animator animator = null;

			//Get the animator of the face of the character with his tag "cvrExpression"
			foreach (Animator anim in animators)
			{
				if (anim.gameObject.tag == "cvrExpression") animator = anim;
			}

			if (animator != null && expressions != null)
			{
				string trigger = expressions.GetTriggerByIndex(index);
				animator.SetTrigger(trigger);
			}

		}

		public float OnGestureChange(string nameAnimation, bool wait)
		{
			Character charTarget = this.character.GetComponentInChildren<Character>();

			Data.AnimationsScriptable.DataAnimation dataAnimation = gestures.GetDataAnimationByName(nameAnimation);
			AnimationClip clip = dataAnimation.animationClip;
			AvatarMask avatarMask = dataAnimation.avatarMask;
			int speedAnimation = 1;
			float waitTime = 0;

			//Calculate the time to wait before the text can appear again
			if (wait) waitTime = clip.length / speedAnimation;

			if (clip != null && charTarget != null && charTarget.GetCharacterAnimator() != null)
			{
				CharacterAnimator characterAnimator = charTarget.GetCharacterAnimator();
				characterAnimator.CrossFadeGesture(
					clip, speedAnimation, avatarMask,
					0.1f, 0.1f
				);
			}

			return waitTime;
		}

		//Vérify if the value given is a variable
		//and parse it if it is
		private string ParseString(GameEngine.IPlayerStateManager playerStateManager, string value)
		{
			var activityNestedField = playerStateManager.ActivityNestedField;
			value = activityNestedField.ParseNestedFields(value);
			return value;
		}
	}
}