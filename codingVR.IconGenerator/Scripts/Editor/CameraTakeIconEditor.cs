﻿namespace codingVR.IconGenerator
{
	using UnityEngine;
	using UnityEditor;

	[CustomEditor(typeof(CameraTakeIcon))]
	public class CameraTakeIconEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			//initialize process
			base.OnInspectorGUI();
			CameraTakeIcon camIcon = (CameraTakeIcon)target;
			if (GUILayout.Button("TakePicture"))
			{
				camIcon.generateImage();
			}
		}
	}
}