﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace codingVR.IconGenerator
{
	[ExecuteInEditMode]
	public class CameraTakeIcon : MonoBehaviour
	{
		// Start is called before the first frame update

		//this is where the gameobjects are
		public Transform objective;

		//this is the teta angular position
		[Range(0, 360)]
		public float teta;
		//this is the phi angular position
		[Range(0, 360)]
		public float phi;
		//this is the radii
		[Range(0, 10)]
		public float Radi;

		//this is the background-color
		public Color background;

		//resolution of the picture
		public int resWidth = 512, resHeight = 512;

		//render texture
		RenderTexture rt;

        [SerializeField]
        public string folder = "Assets/Textures";

		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{
			//spherical coordinates
			float x = Radi * Mathf.Sin(phi * Mathf.PI / 180) * Mathf.Sin(teta * Mathf.PI / 180);
			float z = Radi * Mathf.Sin(phi * Mathf.PI / 180) * Mathf.Cos(teta * Mathf.PI / 180);
			float y = Radi * Mathf.Cos(phi * Mathf.PI / 180);

			//set the position and rotation of the camera
			transform.position = new Vector3(x, y, z);
			transform.LookAt(objective);

			//set the color of the camera
			Camera.main.backgroundColor = background;

		}

		//this is called when pressing the button in the editor
		public void generateImage()
		{
			for (int ii = 0; ii < objective.childCount; ii++)
			{
				disableAllChildren(ii);

				//use render texture to capture image
				rt = new RenderTexture(resWidth, resHeight, 32);
				Camera.main.targetTexture = rt;
				Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGBA32, false);
              
				Camera.main.Render();
				RenderTexture.active = rt;
				screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
				Camera.main.targetTexture = null;
				RenderTexture.active = null; // JC: added to avoid errors

				//set the bytes of the image and encode it to png
				byte[] bytes = screenShot.EncodeToPNG();
				string filename = "IM_"+objective.GetChild(ii).name;

				string folderPath = Path.GetFullPath(folder);

				//write to folder with specific name and format
				File.WriteAllBytes(folder + "/" + filename + ".png", bytes);

				//debug
				Debug.Log("Saved" + folder + "/" + filename + ".png");

				//clear render
				DestroyImmediate(rt);
			}
		}

		//dissables all children except the one in position j
		public void disableAllChildren(int j)
		{
			for (int ii = 0; ii < objective.childCount; ii++)
			{
				if (ii != j)
				{
					objective.GetChild(ii).gameObject.SetActive(false);
				}
				else
				{
					objective.GetChild(ii).gameObject.SetActive(true);
				}
			}
		}
	}
}