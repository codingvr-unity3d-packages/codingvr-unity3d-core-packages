﻿namespace codingVR.HexMapTerrain
{
	public interface IHexCell : HexMapNavigation.IHexCellOrigin
	{
		/*
		 * 
		 * Terrain Data
		 * 
		 */

		IHexCellTerrainData TerrainData { get; }
		IHexCellTerrainData TerrainOrDefaultData();

		/*
		 * 
		 * Layout
		 * 
		 */

		// plug terrain
		bool PlugTerrain(HexMapNavigation.HexCellShaderData shaderData, float elevationParam);
		// create hole (remove data)
		bool MakeHole();
	}
}