﻿    using UnityEngine;

namespace codingVR.HexMapTerrain
{
    public interface IHexEnvironmentFeatures
    {
        /*
         * 
         * Features management
         * 
         */

        void AddFeature(IHexCell cell, Vector3 position);
        void AddSpecialFeature(IHexCell cell, Vector3 position);

        /*
         * 
         * Bridge management
         * 
         */

        void AddBridge(Vector3 roadCenter1, Vector3 roadCenter2);

        /*
         * 
         * Wall management
         * 
         */

        void AddWall(EdgeVertices5 near, IHexCell nearCell, EdgeVertices5 far, IHexCell farCell, bool hasRiver, bool hasRoad);
        void AddWall(IHexCell centerCell, Vector3 c1, IHexCell cell1, Vector3 c2, IHexCell cell2, Vector3 c3, IHexCell cell3);
    }
}