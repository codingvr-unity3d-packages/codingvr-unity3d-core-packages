﻿    using UnityEngine;

namespace codingVR.HexMapTerrain
{
    public interface IHexGridChunkGeometry
    {
        /*
         * 
         * test if position is contained by this chunk at geometry level
         * 
         */

        bool GeometryContains(Vector3 position);

        /*
         * 
         * tessellate
         * 
         */

        void Triangulate(IHexEnvironmentManager environment, IHexCell[] cells);
    }
}