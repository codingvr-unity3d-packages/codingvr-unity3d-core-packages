﻿using System.Collections.Generic;
using UnityEngine;

namespace codingVR.HexMapTerrain
{
	public interface IHexGridChunk
	{
		/*
		 * 
		 * Geometry
		 * 
		 */

		// tesselation
		bool GeometryContains(Vector3 position);
	}
}