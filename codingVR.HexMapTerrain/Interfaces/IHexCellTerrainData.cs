﻿using UnityEngine;
using Zenject;

namespace codingVR.HexMapTerrain
{
	public interface IHexCellTerrainData : HexMapNavigation.IHexCellTerrainDataOrigin
	{
		/*
		 * 
		 * Refreshing
		 * 
		 */

		Vector3 MovingCell { get; set; }

		/* 
		 * 
		 * Elevation & water
		 * 
		 */

		// Elevation & water
		float Elevation { get; set; }
		float ElevationPerturbStrength { get; set; }
		float ElevationStep { get; set; }
		float WaterLevel { get; set; }
		float ViewElevation { get; }
		bool IsUnderwater { get; }

		/* 
		 * 
		 * Features
		 * 
		 */

		// river
		bool HasIncomingRiver { get; set; }
		bool HasOutgoingRiver { get; set; }
		bool HasRiver { get; }
		bool HasRiverBeginOrEnd { get; }
		HexMapNavigation.HexDirection RiverBeginOrEndDirection { get; }

		// roads
		bool HasRoads { get; }

		// rivers
		HexMapNavigation.HexDirection IncomingRiver { get; set; }
		HexMapNavigation.HexDirection OutgoingRiver { get; }

		// features
		float StreamBedY { get; }
		float RiverSurfaceY { get; }
		float WaterSurfaceY { get; }

		// features
		int UrbanLevel { get; set; }
		int FarmLevel { get; set; }
		int PlantLevel { get; set; }
		int SpecialIndex { get; set; }
		bool IsSpecial { get; }

		// walls
		bool Walled { get; set; }

		// terrain type
		int TerrainTypeIndex { get; set; }

		/*
		 * 
		 * Features advanced
		 * 
		 */

		// roads & rivers advanced
		bool HasRiverThroughEdge(HexMapNavigation.HexDirection direction);
		void RemoveIncomingRiver();
		void RemoveOutgoingRiver();
		void RemoveRiver();
		void SetOutgoingRiver(HexMapNavigation.HexDirection direction);
		bool HasRoadThroughEdge(HexMapNavigation.HexDirection direction);
		void AddRoad(HexMapNavigation.HexDirection direction);
		void SetRoadState(int index, bool state);
		void RemoveRoads();
		float GetElevationDifference(HexMapNavigation.HexDirection direction);
		bool IsValidRiverDestination(IHexCell neighbor);

		/*
		 * 
		 * Geometry
		 * 
		 */

		HexEdgeType GetEdgeType(HexMapNavigation.HexDirection direction);
		HexEdgeType GetEdgeType(IHexCell otherCell);
	}

	public interface IHexCellTerrainDataFactory : IFactory<IHexCell, IHexCellTerrainData>
	{
	};
}