﻿using System.Collections.Generic;
using UnityEngine;

namespace codingVR.HexMapTerrain
{
	public interface IHexEnvironmentManager
	{
		/*
		 * 
		 * containers
		 * 
		 */

		void MakeChildOfChunkEnvironement(HexMapNavigation.HexEnvironmentManagerIDs containerIdx, Transform child);
		bool DestroyChildOfChunkEnvironement(HexMapNavigation.HexEnvironmentManagerIDs containerIdx, string childIdentifier);
		Transform FindChildOfChunkEnvironement(HexMapNavigation.HexEnvironmentManagerIDs containerIdx, string childIdentifier);
		List<Transform> FindChildsOfChunkEnvironement(HexMapNavigation.HexEnvironmentManagerIDs containerIdx, string childFromParentID);
		List<Transform> FindChildsOfChunkEnvironement(HexMapNavigation.HexEnvironmentManagerIDs containerIdx);

		/*
		 * 
		 * Features
		 * 
		 */

		IHexEnvironmentFeatures Features { get; }
	}
}