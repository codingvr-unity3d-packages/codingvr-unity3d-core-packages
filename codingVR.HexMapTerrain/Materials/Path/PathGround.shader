﻿Shader "Custom/PathGround"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Terrain Texture Array", 2DArray) = "white" {}
		_GridTex("Grid Texture", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Specular("Specular", Color) = (0.2, 0.2, 0.2)
		_BackgroundColor("Background Color", Color) = (0,0,0)
	}
	SubShader
	{
		Tags {"Queue" = "Transparent" "RenderType" = "Transparent" }
		ZWrite Off
		//ZTest Off
		Offset -50, -50
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 200

		CGPROGRAM

		#pragma surface surf StandardSpecular fullforwardshadows alpha:fade vertex:vert
		#pragma target 3.5

		#pragma shader_feature _ GRID_ON
		#pragma shader_feature _ HEX_MAP_EDIT_MODE
		#pragma shader_feature OUTER_RADIUS_IS_10d0 OUTER_RADIUS_IS_2d0 OUTER_RADIUS_IS_1d0 OUTER_RADIUS_IS_0d5

		#include "../HexMetrics.cginc"
		#include "../HexCellData.cginc"

		UNITY_DECLARE_TEX2DARRAY(_MainTex);

		sampler2D _GridTex;

		half _Glossiness;
		fixed3 _Specular;
		fixed4 _Color;
		half3 _BackgroundColor;

		struct Input {
			float4 color : COLOR;
			float3 worldPos;
			float3 terrain;
			float explored;
			float3 normal;
		};

		void vert(inout appdata_full v, out Input data)
		{
			UNITY_INITIALIZE_OUTPUT(Input, data);

			// get cell data
			float4 cell0 = GetCellData(v, 0);
			float4 cell1 = GetCellData(v, 1);
			float4 cell2 = GetCellData(v, 2);

			// get tearrin texture index
			data.terrain.x = cell0.w;
			data.terrain.y = cell1.w;
			data.terrain.z = cell2.w;

			// get map data
			data.explored = cell0.y * v.color.x + cell1.y * v.color.y + cell2.y * v.color.z;
			data.normal = v.normal;
		}

		float4 GetTerrainColor(Input IN, float2 uv, int index)
		{
			float3 uvw = float3(uv * (16 * TILING_SCALE), IN.terrain[index]);
			float4 c = UNITY_SAMPLE_TEX2DARRAY(_MainTex, uvw);
			return c * (IN.color[index]);
		}

		// tri planar mapping
		// https://gamedevelopment.tutsplus.com/articles/use-tri-planar-texture-mapping-for-better-terrain--gamedev-13821
		float4 TriPlanarMapping(Input IN)
		{
			// apply texture on different plan
			fixed4 xaxis = GetTerrainColor(IN, IN.worldPos.yz, 0) + GetTerrainColor(IN, IN.worldPos.yz, 1) + GetTerrainColor(IN, IN.worldPos.yz, 2);
			fixed4 yaxis = GetTerrainColor(IN, IN.worldPos.xz, 0) + GetTerrainColor(IN, IN.worldPos.xz, 1) + GetTerrainColor(IN, IN.worldPos.xz, 2);
			fixed4 zaxis = GetTerrainColor(IN, IN.worldPos.xy, 0) + GetTerrainColor(IN, IN.worldPos.xy, 1) + GetTerrainColor(IN, IN.worldPos.xy, 2);

			// normal is the world-space normal of the fragment
			float3 blending = abs(IN.normal);
			blending = normalize(max(blending, 0.00001)); // Force weights to sum to 1.0
			//blending = pow(blending, float3(0.1, 0.1, 0.1));
			float b = (blending.x + blending.y + blending.z);
			blending /= float3(b, b, b);

			// blend the results of the 3 planar projections.
			return  xaxis * blending.x + yaxis * blending.y + zaxis * blending.z;
		}



		// surface
		void surf(Input IN, inout SurfaceOutputStandardSpecular o)
		{
			fixed4 c = TriPlanarMapping(IN);

			float explored = IN.explored;

			float2 gridUV = IN.worldPos.xz;
			gridUV.x *= 1 / (4 * OUTER_RADIUS * 0.866025404);
			gridUV.y *= 1 / (2 * OUTER_RADIUS * 1.5);
			fixed4 grid = tex2D(_GridTex, gridUV);
			o.Albedo = c.rgb;

			o.Albedo.rgb *= (1.0 - explored);
			o.Albedo.g += explored;
			o.Albedo *= grid;

			//o.Albedo = float3(1.0, 0.0, 0.0);
			o.Specular = _Specular;
			o.Smoothness = _Glossiness;
			o.Occlusion = 1.0;
			o.Emission = _BackgroundColor * 0.0;
			o.Alpha = 0.75;
		}
		ENDCG
		}
}
