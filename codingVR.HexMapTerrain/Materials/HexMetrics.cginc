﻿#define OUTER_TO_INNER 0.866025404

#if defined(OUTER_RADIUS_IS_10d0)
#define OUTER_RADIUS 2
#endif

#if defined(OUTER_RADIUS_IS_2d0)
#define OUTER_RADIUS 2
#endif

#if defined(OUTER_RADIUS_IS_1d0)
#define OUTER_RADIUS 1
#endif

#if defined(OUTER_RADIUS_IS_0d5)
#define OUTER_RADIUS 0.5
#endif

#define CHUNK_SIZE_X 5
#define TILING_SCALE (1 / (CHUNK_SIZE_X * 2 * OUTER_RADIUS / OUTER_TO_INNER))
