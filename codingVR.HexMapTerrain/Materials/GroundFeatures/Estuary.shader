﻿Shader "Custom/Estuary" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Specular ("Specular", Color) = (0.2, 0.2, 0.2)
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf StandardSpecular alpha vertex:vert
		#pragma target 3.0

		#pragma shader_feature _ HEX_MAP_EDIT_MODE
		#pragma shader_feature OUTER_RADIUS_IS_10d0 OUTER_RADIUS_IS_2d0 OUTER_RADIUS_IS_1d0 OUTER_RADIUS_IS_0d5

		#include "Water.cginc"
		#include "../HexCellData.cginc"

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float2 riverUV;
			float3 worldPos;
			float2 visibility;
		};

		half _Glossiness;
		fixed3 _Specular;
		fixed4 _Color;

		void vert (inout appdata_full v, out Input data) {
			UNITY_INITIALIZE_OUTPUT(Input, data);
			data.riverUV = v.texcoord1.xy;
		}

		void surf (Input IN, inout SurfaceOutputStandardSpecular o) {
			float shore = IN.uv_MainTex.y;
			float foam = Foam(shore, IN.worldPos.xz, _MainTex);
			float waves = Waves(IN.worldPos.xz, _MainTex);
			waves *= 1 - shore;
			float shoreWater = max(foam, waves);

			float river = River(IN.riverUV, _MainTex);

			float water = lerp(shoreWater, river, IN.uv_MainTex.x);

			fixed4 c = saturate(_Color + water);
			o.Albedo = c.rgb;
			o.Specular = _Specular;
			o.Smoothness = _Glossiness;
			o.Occlusion = 1.0;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}