﻿Shader "Custom/Road" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Specular ("Specular", Color) = (0.2, 0.2, 0.2)
	}
	SubShader {
		Tags {
			"RenderType"="Opaque"
			"Queue" = "Geometry+1"
		}
		LOD 200
		Offset -1, -1
		
		CGPROGRAM
		#pragma surface surf StandardSpecular fullforwardshadows decal:blend vertex:vert
		#pragma target 3.0

		#pragma shader_feature _ HEX_MAP_EDIT_MODE
		#pragma shader_feature OUTER_RADIUS_IS_10d0 OUTER_RADIUS_IS_2d0 OUTER_RADIUS_IS_1d0 OUTER_RADIUS_IS_0d5

		#include "../HexMetrics.cginc"
		#include "../HexCellData.cginc"

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float3 worldPos;
			float2 visibility;
		};

		half _Glossiness;
		fixed3 _Specular;
		fixed4 _Color;

		void vert (inout appdata_full v, out Input data) {
			UNITY_INITIALIZE_OUTPUT(Input, data);
		}

		void surf (Input IN, inout SurfaceOutputStandardSpecular o) {
			float4 noise =
				tex2D(_MainTex, IN.worldPos.xz * (3 * TILING_SCALE));
			fixed4 c = _Color * ((noise.y * 0.75 + 0.25));
			float blend = IN.uv_MainTex.x;
			blend *= noise.x + 0.5;
			blend = smoothstep(0.4, 0.7, blend);

			o.Albedo = c.rgb;
			o.Specular = _Specular;
			o.Smoothness = _Glossiness;
			o.Occlusion = 1.0;
			o.Alpha = blend;
		}
		ENDCG
	}
	FallBack "Diffuse"
}