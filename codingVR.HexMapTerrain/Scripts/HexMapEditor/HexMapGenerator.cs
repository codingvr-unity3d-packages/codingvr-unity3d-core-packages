﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.HexMapTerrain
{
	public class HexMapGenerator : MonoBehaviour, HexMapNavigation.IHexMapGenerator
	{
		HexMapNavigation.IHexGridManagerOrigin hexGridManager;

		[SerializeField]
		bool useFixedSeed;

		[SerializeField]
		int seed;

		[Range(0f, 0.5f)]
		[SerializeField]
		float jitterProbability = 0.25f;

		[Range(20, 200)]
		[SerializeField]
		int chunkSizeMin = 30;

		[Range(20, 200)]
		[SerializeField]
		int chunkSizeMax = 100;

		[Range(0f, 1f)]
		[SerializeField]
		float highRiseProbability = 0.25f;

		[Range(0f, 0.4f)]
		[SerializeField]
		float sinkProbability = 0.2f;

		[Range(5, 95)]
		[SerializeField]
		int landPercentage = 50;

		[Range(1, 5)]
		[SerializeField]
		int waterLevel = 3;

		[Range(-4, 0)]
		[SerializeField]
		int elevationMinimum = -2;

		[Range(6, 10)]
		[SerializeField]
		int elevationMaximum = 8;

		[Range(0, 10)]
		[SerializeField]
		int mapBorderX = 5;

		[Range(0, 10)]
		[SerializeField]
		int mapBorderZ = 5;

		[Range(0, 10)]
		[SerializeField]
		int regionBorder = 5;

		[Range(1, 4)]
		[SerializeField]
		int regionCount = 1;

		[Range(0, 100)]
		[SerializeField]
		int erosionPercentage = 50;

		[Range(0f, 1f)]
		[SerializeField]
		float startingMoisture = 0.1f;

		[Range(0f, 1f)]
		[SerializeField]
		float evaporationFactor = 0.5f;

		[Range(0f, 1f)]
		[SerializeField]
		float precipitationFactor = 0.25f;

		[Range(0f, 1f)]
		[SerializeField]
		float runoffFactor = 0.25f;

		[Range(0f, 1f)]
		[SerializeField]
		float seepageFactor = 0.125f;

		HexMapNavigation.HexDirection windDirection = HexMapNavigation.HexDirection.NW;

		[Range(1f, 10f)]
		[SerializeField]
		float windStrength = 4f;

		[Range(0, 20)]
		[SerializeField]
		int riverPercentage = 10;

		[Range(0f, 1f)]
		[SerializeField]
		float extraLakeProbability = 0.25f;

		[Range(0f, 1f)]
		[SerializeField]
		float lowTemperature = 0f;

		[Range(0f, 1f)]
		[SerializeField]
		float highTemperature = 1f;

		public enum HemisphereMode
		{
			Both, North, South
		}

		[SerializeField]
		HemisphereMode hemisphere;

		[Range(0f, 1f)]
		[SerializeField]
		float temperatureJitter = 0.1f;

		HexMapNavigation.HexCellPriorityQueue searchFrontier;

		int searchFrontierPhase;

		int cellCount, landCells;

		int temperatureJitterChannel;

		[Inject]
		public void Construct(HexMapNavigation.IHexGridManagerOrigin hexGridManager)
		{
			this.hexGridManager = hexGridManager;
		}

		struct MapRegion
		{
			public int xMin, xMax, zMin, zMax;
		}

		List<MapRegion> regions;

		struct ClimateData
		{
			public float clouds, moisture;
		}

		List<ClimateData> climate = new List<ClimateData>();
		List<ClimateData> nextClimate = new List<ClimateData>();

		List<HexMapNavigation.HexDirection> flowDirections = new List<HexMapNavigation.HexDirection>();

		struct Biome
		{
			public int terrain, plant;

			public Biome(int terrain, int plant)
			{
				this.terrain = terrain;
				this.plant = plant;
			}
		}

		static float[] temperatureBands = { 0.1f, 0.3f, 0.6f };

		static float[] moistureBands = { 0.12f, 0.28f, 0.85f };

		static Biome[] biomes = 
		{
			new Biome(0, 0), new Biome(4, 0), new Biome(4, 0), new Biome(4, 0),
			new Biome(0, 0), new Biome(2, 0), new Biome(2, 1), new Biome(2, 2),
			new Biome(0, 0), new Biome(1, 0), new Biome(1, 1), new Biome(1, 2),
			new Biome(0, 0), new Biome(1, 1), new Biome(1, 2), new Biome(1, 3)
		};

		public bool GenerateMap(int x, int z, int chunkSizeX, int chunkSizeZ, bool wrapping)
		{
			Random.State originalRandomState = Random.state;
			if (!useFixedSeed)
			{
				seed = Random.Range(0, int.MaxValue);
				seed ^= (int)System.DateTime.Now.Ticks;
				seed ^= (int)Time.unscaledTime;
				seed &= int.MaxValue;
			}
			Random.InitState(seed);

			cellCount = x * z;
			bool b = hexGridManager.CreateMap(x, z, chunkSizeX, chunkSizeZ, wrapping, false);
			if (b == true)
			{
				if (searchFrontier == null)
				{
					searchFrontier = new HexMapNavigation.HexCellPriorityQueue();
				}
				for (int i = 0; i < cellCount; i++)
				{
					(hexGridManager.HexGrid.GetCell(i) as IHexCell).TerrainData.WaterLevel = waterLevel;
				}
				CreateRegions(hexGridManager.HexGrid);
				CreateLand(hexGridManager.HexGrid);
				ErodeLand(hexGridManager.HexGrid);
				CreateClimate(hexGridManager.HexGrid);
				CreateRivers(hexGridManager.HexGrid);
				SetTerrainType(hexGridManager.HexGrid);
				for (int i = 0; i < cellCount; i++)
				{
					hexGridManager.HexGrid.GetCell(i).HeuristicData.SearchPhase = 0;
				}
			}
			Random.state = originalRandomState;
			return b;
		}

		void CreateRegions(HexMapNavigation.IHexGrid hexGrid)
		{
			if (regions == null)
			{
				regions = new List<MapRegion>();
			}
			else
			{
				regions.Clear();
			}

			int borderX = hexGrid.Wrapping ? regionBorder : mapBorderX;
			MapRegion region;
			switch (regionCount)
			{
				default:
					if (hexGrid.Wrapping)
					{
						borderX = 0;
					}
					region.xMin = borderX;
					region.xMax = hexGrid.CellCountX - borderX;
					region.zMin = mapBorderZ;
					region.zMax = hexGrid.CellCountZ - mapBorderZ;
					regions.Add(region);
					break;
				case 2:
					if (Random.value < 0.5f)
					{
						region.xMin = borderX;
						region.xMax = hexGrid.CellCountX / 2 - regionBorder;
						region.zMin = mapBorderZ;
						region.zMax = hexGrid.CellCountZ - mapBorderZ;
						regions.Add(region);
						region.xMin = hexGrid.CellCountX / 2 + regionBorder;
						region.xMax = hexGrid.CellCountX - borderX;
						regions.Add(region);
					}
					else
					{
						if (hexGrid.Wrapping)
						{
							borderX = 0;
						}
						region.xMin = borderX;
						region.xMax = hexGrid.CellCountX - borderX;
						region.zMin = mapBorderZ;
						region.zMax = hexGrid.CellCountZ / 2 - regionBorder;
						regions.Add(region);
						region.zMin = hexGrid.CellCountZ / 2 + regionBorder;
						region.zMax = hexGrid.CellCountZ - mapBorderZ;
						regions.Add(region);
					}
					break;
				case 3:
					region.xMin = borderX;
					region.xMax = hexGrid.CellCountX / 3 - regionBorder;
					region.zMin = mapBorderZ;
					region.zMax = hexGrid.CellCountZ - mapBorderZ;
					regions.Add(region);
					region.xMin = hexGrid.CellCountX / 3 + regionBorder;
					region.xMax = hexGrid.CellCountX * 2 / 3 - regionBorder;
					regions.Add(region);
					region.xMin = hexGrid.CellCountX * 2 / 3 + regionBorder;
					region.xMax = hexGrid.CellCountX - borderX;
					regions.Add(region);
					break;
				case 4:
					region.xMin = borderX;
					region.xMax = hexGrid.CellCountX / 2 - regionBorder;
					region.zMin = mapBorderZ;
					region.zMax = hexGrid.CellCountZ / 2 - regionBorder;
					regions.Add(region);
					region.xMin = hexGrid.CellCountX / 2 + regionBorder;
					region.xMax = hexGrid.CellCountX - borderX;
					regions.Add(region);
					region.zMin = hexGrid.CellCountZ / 2 + regionBorder;
					region.zMax = hexGrid.CellCountZ - mapBorderZ;
					regions.Add(region);
					region.xMin = borderX;
					region.xMax = hexGrid.CellCountX / 2 - regionBorder;
					regions.Add(region);
					break;
			}
		}

		void CreateLand(HexMapNavigation.IHexGrid hexGrid)
		{
			int landBudget = Mathf.RoundToInt(cellCount * landPercentage * 0.01f);
			landCells = landBudget;
			for (int guard = 0; guard < 10000; guard++)
			{
				bool sink = Random.value < sinkProbability;
				for (int i = 0; i < regions.Count; i++)
				{
					MapRegion region = regions[i];
					int chunkSize = Random.Range(chunkSizeMin, chunkSizeMax - 1);
					if (sink)
					{
						landBudget = SinkTerrain(hexGrid, chunkSize, landBudget, region);
					}
					else
					{
						landBudget = RaiseTerrain(hexGrid, chunkSize, landBudget, region);
						if (landBudget == 0)
						{
							return;
						}
					}
				}
			}
			if (landBudget > 0)
			{
				Debug.LogWarning("Failed to use up " + landBudget + " land budget.");
				landCells -= landBudget;
			}
		}

		int RaiseTerrain(HexMapNavigation.IHexGrid hexGrid, int chunkSize, int budget, MapRegion region)
		{
			searchFrontierPhase += 1;
			IHexCell firstCell = GetRandomCell(hexGrid, region);
			firstCell.HeuristicData.SearchPhase = searchFrontierPhase;
			firstCell.HeuristicData.Distance = 0;
			firstCell.HeuristicData.SearchHeuristic = 0;
			searchFrontier.Enqueue(firstCell);
			HexMapNavigation.HexCoordinates center = firstCell.Coordinates;

			int rise = Random.value < highRiseProbability ? 2 : 1;
			int size = 0;
			while (size < chunkSize && searchFrontier.Count > 0)
			{
				IHexCell current = searchFrontier.Dequeue() as IHexCell;
				var originalElevation = current.TerrainData.Elevation;
				var newElevation = originalElevation + rise;
				if (newElevation > elevationMaximum)
				{
					continue;
				}
				current.TerrainData.Elevation = newElevation;
				if (
					originalElevation < waterLevel &&
					newElevation >= waterLevel && --budget == 0
				)
				{
					break;
				}
				size += 1;

				for (HexMapNavigation.HexDirection d = HexMapNavigation.HexDirection.NE; d <= HexMapNavigation.HexDirection.NW; d++)
				{
					HexMapNavigation.IHexCellOrigin neighbor = current.GetNeighbor(d);
					if (neighbor != null && neighbor.HeuristicData.SearchPhase < searchFrontierPhase)
					{
						neighbor.HeuristicData.SearchPhase = searchFrontierPhase;
						neighbor.HeuristicData.Distance = neighbor.Coordinates.DistanceTo(center);
						neighbor.HeuristicData.SearchHeuristic =
							Random.value < jitterProbability ? 1 : 0;
						searchFrontier.Enqueue(neighbor);
					}
				}
			}
			searchFrontier.Clear();
			return budget;
		}

		int SinkTerrain(HexMapNavigation.IHexGrid hexGrid, int chunkSize, int budget, MapRegion region)
		{
			searchFrontierPhase += 1;
			IHexCell firstCell = GetRandomCell(hexGrid, region);
			firstCell.HeuristicData.SearchPhase = searchFrontierPhase;
			firstCell.HeuristicData.Distance = 0;
			firstCell.HeuristicData.SearchHeuristic = 0;
			searchFrontier.Enqueue(firstCell);
			HexMapNavigation.HexCoordinates center = firstCell.Coordinates;

			int sink = Random.value < highRiseProbability ? 2 : 1;
			int size = 0;
			while (size < chunkSize && searchFrontier.Count > 0)
			{
				IHexCell current = searchFrontier.Dequeue() as IHexCell;
				var originalElevation = current.TerrainData.Elevation;
				var newElevation = current.TerrainData.Elevation - sink;
				if (newElevation < elevationMinimum)
				{
					continue;
				}
				current.TerrainData.Elevation = newElevation;
				if (
					originalElevation >= waterLevel &&
					newElevation < waterLevel
				)
				{
					budget += 1;
				}
				size += 1;

				for (HexMapNavigation.HexDirection d = HexMapNavigation.HexDirection.NE; d <= HexMapNavigation.HexDirection.NW; d++)
				{
					HexMapNavigation.IHexCellOrigin neighbor = current.GetNeighbor(d);
					if (neighbor != null && neighbor.HeuristicData.SearchPhase < searchFrontierPhase)
					{
						neighbor.HeuristicData.SearchPhase = searchFrontierPhase;
						neighbor.HeuristicData.Distance = neighbor.Coordinates.DistanceTo(center);
						neighbor.HeuristicData.SearchHeuristic =
							Random.value < jitterProbability ? 1 : 0;
						searchFrontier.Enqueue(neighbor);
					}
				}
			}
			searchFrontier.Clear();
			return budget;
		}

		void ErodeLand(HexMapNavigation.IHexGrid hexGrid)
		{
			List<HexMapNavigation.IHexCellOrigin> erodibleCells = Patterns.ListPool<HexMapNavigation.IHexCellOrigin>.Get();
			for (int i = 0; i < cellCount; i++)
			{
				var cell = hexGrid.GetCell(i);
				if (IsErodible(cell as IHexCell))
				{
					erodibleCells.Add(cell);
				}
			}

			int targetErodibleCount =
				(int)(erodibleCells.Count * (100 - erosionPercentage) * 0.01f);

			while (erodibleCells.Count > targetErodibleCount)
			{
				int index = Random.Range(0, erodibleCells.Count);
				var cell = erodibleCells[index];
				var targetCell = GetErosionTarget(cell as IHexCell);

				(cell as IHexCell).TerrainData.Elevation -= 1;
				targetCell.TerrainData.Elevation += 1;

				if (!IsErodible(cell as IHexCell))
				{
					erodibleCells[index] = erodibleCells[erodibleCells.Count - 1];
					erodibleCells.RemoveAt(erodibleCells.Count - 1);
				}

				for (HexMapNavigation.HexDirection d = HexMapNavigation.HexDirection.NE; d <= HexMapNavigation.HexDirection.NW; d++)
				{
					IHexCell neighbor = cell.GetNeighbor(d) as IHexCell;
					if (
						neighbor != null && neighbor.TerrainData.Elevation == (cell as IHexCell).TerrainData.Elevation + 2 &&
						!erodibleCells.Contains(neighbor)
					)
					{
						erodibleCells.Add(neighbor);
					}
				}

				if (IsErodible(targetCell) && !erodibleCells.Contains(targetCell))
				{
					erodibleCells.Add(targetCell);
				}

				for (HexMapNavigation.HexDirection d = HexMapNavigation.HexDirection.NE; d <= HexMapNavigation.HexDirection.NW; d++)
				{
					IHexCell neighbor = targetCell.GetNeighbor(d) as IHexCell;
					if (
						neighbor != null && neighbor != cell &&
						neighbor.TerrainData.Elevation == targetCell.TerrainData.Elevation + 1 &&
						!IsErodible(neighbor)
					)
					{
						erodibleCells.Remove(neighbor);
					}
				}
			}

			Patterns.ListPool<HexMapNavigation.IHexCellOrigin>.Add(erodibleCells);
		}

		bool IsErodible(IHexCell cell)
		{
			var erodibleElevation = cell.TerrainData.Elevation - 2;
			for (HexMapNavigation.HexDirection d = HexMapNavigation.HexDirection.NE; d <= HexMapNavigation.HexDirection.NW; d++)
			{
				IHexCell neighbor = cell.GetNeighbor(d) as IHexCell;
				if (neighbor != null && neighbor.TerrainData.Elevation <= erodibleElevation)
				{
					return true;
				}
			}
			return false;
		}

		IHexCell GetErosionTarget(IHexCell cell)
		{
			var candidates = Patterns.ListPool<IHexCell>.Get();
			var erodibleElevation = cell.TerrainData.Elevation - 2;
			for (HexMapNavigation.HexDirection d = HexMapNavigation.HexDirection.NE; d <= HexMapNavigation.HexDirection.NW; d++)
			{
				IHexCell neighbor = cell.GetNeighbor(d) as IHexCell;
				if (neighbor != null && neighbor.TerrainData.Elevation <= erodibleElevation)
				{
					candidates.Add(neighbor);
				}
			}
			var target = candidates[Random.Range(0, candidates.Count)];
			Patterns.ListPool<IHexCell>.Add(candidates);
			return target;
		}

		void CreateClimate(HexMapNavigation.IHexGrid hexGrid)
		{
			climate.Clear();
			nextClimate.Clear();
			ClimateData initialData = new ClimateData();
			initialData.moisture = startingMoisture;
			ClimateData clearData = new ClimateData();
			for (int i = 0; i < cellCount; i++)
			{
				climate.Add(initialData);
				nextClimate.Add(clearData);
			}

			for (int cycle = 0; cycle < 40; cycle++)
			{
				for (int i = 0; i < cellCount; i++)
				{
					EvolveClimate(hexGrid, i);
				}
				List<ClimateData> swap = climate;
				climate = nextClimate;
				nextClimate = swap;
			}
		}

		void EvolveClimate(HexMapNavigation.IHexGrid hexGrid, int cellIndex)
		{
			var cell = hexGrid.GetCell(cellIndex);
			ClimateData cellClimate = climate[cellIndex];

			if ((cell as IHexCell).TerrainData.IsUnderwater)
			{
				cellClimate.moisture = 1f;
				cellClimate.clouds += evaporationFactor;
			}
			else
			{
				float evaporation = cellClimate.moisture * evaporationFactor;
				cellClimate.moisture -= evaporation;
				cellClimate.clouds += evaporation;
			}

			float precipitation = cellClimate.clouds * precipitationFactor;
			cellClimate.clouds -= precipitation;
			cellClimate.moisture += precipitation;

			float cloudMaximum = 1f - (cell as IHexCell).TerrainData.ViewElevation / (elevationMaximum + 1f);
			if (cellClimate.clouds > cloudMaximum)
			{
				cellClimate.moisture += cellClimate.clouds - cloudMaximum;
				cellClimate.clouds = cloudMaximum;
			}

			HexMapNavigation.HexDirection mainDispersalDirection = windDirection.Opposite();
			float cloudDispersal = cellClimate.clouds * (1f / (5f + windStrength));
			float runoff = cellClimate.moisture * runoffFactor * (1f / 6f);
			float seepage = cellClimate.moisture * seepageFactor * (1f / 6f);
			for (HexMapNavigation.HexDirection d = HexMapNavigation.HexDirection.NE; d <= HexMapNavigation.HexDirection.NW; d++)
			{
				IHexCell neighbor = cell.GetNeighbor(d) as IHexCell;
				if (neighbor == null)
				{
					continue;
				}
				ClimateData neighborClimate = nextClimate[neighbor.Index];
				if (d == mainDispersalDirection)
				{
					neighborClimate.clouds += cloudDispersal * windStrength;
				}
				else
				{
					neighborClimate.clouds += cloudDispersal;
				}

				float elevationDelta = neighbor.TerrainData.ViewElevation - (cell as IHexCell).TerrainData.ViewElevation;
				if (elevationDelta < 0)
				{
					cellClimate.moisture -= runoff;
					neighborClimate.moisture += runoff;
				}
				else if (elevationDelta == 0)
				{
					cellClimate.moisture -= seepage;
					neighborClimate.moisture += seepage;
				}

				nextClimate[neighbor.Index] = neighborClimate;
			}

			ClimateData nextCellClimate = nextClimate[cellIndex];
			nextCellClimate.moisture += cellClimate.moisture;
			if (nextCellClimate.moisture > 1f)
			{
				nextCellClimate.moisture = 1f;
			}
			nextClimate[cellIndex] = nextCellClimate;
			climate[cellIndex] = new ClimateData();
		}

		void CreateRivers(HexMapNavigation.IHexGrid hexGrid)
		{
			List<HexMapNavigation.IHexCellOrigin> riverOrigins = Patterns.ListPool<HexMapNavigation.IHexCellOrigin>.Get();
			for (int i = 0; i < cellCount; i++)
			{
				var cell = hexGrid.GetCell(i);
				if ((cell as IHexCell).TerrainData.IsUnderwater)
				{
					continue;
				}
				ClimateData data = climate[i];
				float weight =
					data.moisture * ((cell as IHexCell).TerrainData.Elevation - waterLevel) /
					(elevationMaximum - waterLevel);
				if (weight > 0.75f)
				{
					riverOrigins.Add(cell);
					riverOrigins.Add(cell);
				}
				if (weight > 0.5f)
				{
					riverOrigins.Add(cell);
				}
				if (weight > 0.25f)
				{
					riverOrigins.Add(cell);
				}
			}

			int riverBudget = Mathf.RoundToInt(landCells * riverPercentage * 0.01f);
			while (riverBudget > 0 && riverOrigins.Count > 0)
			{
				int index = Random.Range(0, riverOrigins.Count);
				int lastIndex = riverOrigins.Count - 1;
				var origin = riverOrigins[index];
				riverOrigins[index] = riverOrigins[lastIndex];
				riverOrigins.RemoveAt(lastIndex);

				if (!(origin as IHexCell).TerrainData.HasRiver)
				{
					bool isValidOrigin = true;
					for (HexMapNavigation.HexDirection d = HexMapNavigation.HexDirection.NE; d <= HexMapNavigation.HexDirection.NW; d++)
					{
						IHexCell neighbor = origin.GetNeighbor(d) as IHexCell;
						if (neighbor != null && (neighbor.TerrainData.HasRiver || neighbor.TerrainData.IsUnderwater))
						{
							isValidOrigin = false;
							break;
						}
					}
					if (isValidOrigin)
					{
						riverBudget -= CreateRiver(origin as IHexCell);
					}
				}
			}

			if (riverBudget > 0)
			{
				Debug.LogWarning("Failed to use up river budget.");
			}

			Patterns.ListPool<HexMapNavigation.IHexCellOrigin>.Add(riverOrigins);
		}

		int CreateRiver(IHexCell origin)
		{
			int length = 1;
			var cell = origin;
			HexMapNavigation.HexDirection direction = HexMapNavigation.HexDirection.NE;
			while (!cell.TerrainData.IsUnderwater)
			{
				var minNeighborElevation = float.MaxValue;
				flowDirections.Clear();
				for (HexMapNavigation.HexDirection d = HexMapNavigation.HexDirection.NE; d <= HexMapNavigation.HexDirection.NW; d++)
				{
					var neighbor = cell.GetNeighbor(d) as IHexCell;
					if (neighbor == null)
					{
						continue;
					}

					if (neighbor.TerrainData.Elevation < minNeighborElevation)
					{
						minNeighborElevation = neighbor.TerrainData.Elevation;
					}

					if (neighbor == origin || neighbor.TerrainData.HasIncomingRiver)
					{
						continue;
					}

					var delta = neighbor.TerrainData.Elevation - cell.TerrainData.Elevation;
					if (delta > 0.0f)
					{
						continue;
					}

					if (neighbor.TerrainData.HasOutgoingRiver)
					{
						cell.TerrainData.SetOutgoingRiver(d);
						return length;
					}

					if (delta < 0)
					{
						flowDirections.Add(d);
						flowDirections.Add(d);
						flowDirections.Add(d);
					}
					if (
						length == 1 ||
						(d != direction.Next2() && d != direction.Previous2())
					)
					{
						flowDirections.Add(d);
					}
					flowDirections.Add(d);
				}

				if (flowDirections.Count == 0)
				{
					if (length == 1)
					{
						return 0;
					}

					if (minNeighborElevation >= cell.TerrainData.Elevation)
					{
						cell.TerrainData.WaterLevel = minNeighborElevation;
						if (minNeighborElevation == cell.TerrainData.Elevation)
						{
							cell.TerrainData.Elevation = minNeighborElevation - 1;
						}
					}
					break;
				}

				direction = flowDirections[Random.Range(0, flowDirections.Count)];
				cell.TerrainData.SetOutgoingRiver(direction);
				length += 1;

				if (
					minNeighborElevation >= cell.TerrainData.Elevation &&
					Random.value < extraLakeProbability
				)
				{
					cell.TerrainData.WaterLevel = cell.TerrainData.Elevation;
					cell.TerrainData.Elevation -= 1;
				}

				cell = cell.GetNeighbor(direction) as IHexCell;
			}
			return length;
		}

		void SetTerrainType(HexMapNavigation.IHexGrid hexGrid)
		{
			temperatureJitterChannel = Random.Range(0, 4);
			int rockDesertElevation =
				elevationMaximum - (elevationMaximum - waterLevel) / 2;

			for (int i = 0; i < cellCount; i++)
			{
				var cell = hexGrid.GetCell(i);
				float temperature = DetermineTemperature(hexGrid, (cell as IHexCell));
				float moisture = climate[i].moisture;
				if (!(cell as IHexCell).TerrainData.IsUnderwater)
				{
					int t = 0;
					for (; t < temperatureBands.Length; t++)
					{
						if (temperature < temperatureBands[t])
						{
							break;
						}
					}
					int m = 0;
					for (; m < moistureBands.Length; m++)
					{
						if (moisture < moistureBands[m])
						{
							break;
						}
					}
					Biome cellBiome = biomes[t * 4 + m];

					if (cellBiome.terrain == 0)
					{
						if ((cell as IHexCell).TerrainData.Elevation >= rockDesertElevation)
						{
							cellBiome.terrain = 3;
						}
					}
					else if ((cell as IHexCell).TerrainData.Elevation == elevationMaximum)
					{
						cellBiome.terrain = 4;
					}

					if (cellBiome.terrain == 4)
					{
						cellBiome.plant = 0;
					}
					else if (cellBiome.plant < 3 && (cell as IHexCell).TerrainData.HasRiver)
					{
						cellBiome.plant += 1;
					}

					(cell as IHexCell).TerrainData.TerrainTypeIndex = cellBiome.terrain;
					(cell as IHexCell).TerrainData.PlantLevel = cellBiome.plant;
				}
				else
				{
					int terrain;
					if ((cell as IHexCell).TerrainData.Elevation == waterLevel - 1)
					{
						int cliffs = 0, slopes = 0;
						for (
							HexMapNavigation.HexDirection d = HexMapNavigation.HexDirection.NE; d <= HexMapNavigation.HexDirection.NW; d++
						)
						{
							var neighbor = cell.GetNeighbor(d) as IHexCell;
							if (neighbor == null)
							{
								continue;
							}
							float delta = neighbor.TerrainData.Elevation - (cell as IHexCell).TerrainData.WaterLevel;
							if (delta == 0)
							{
								slopes += 1;
							}
							else if (delta > 0)
							{
								cliffs += 1;
							}
						}

						if (cliffs + slopes > 3)
						{
							terrain = 1;
						}
						else if (cliffs > 0)
						{
							terrain = 3;
						}
						else if (slopes > 0)
						{
							terrain = 0;
						}
						else
						{
							terrain = 1;
						}
					}
					else if ((cell as IHexCell).TerrainData.Elevation >= waterLevel)
					{
						terrain = 1;
					}
					else if ((cell as IHexCell).TerrainData.Elevation < 0)
					{
						terrain = 3;
					}
					else
					{
						terrain = 2;
					}

					if (terrain == 1 && temperature < temperatureBands[0])
					{
						terrain = 2;
					}
					(cell as IHexCell).TerrainData.TerrainTypeIndex = terrain;
				}
			}
		}

		float DetermineTemperature(HexMapNavigation.IHexGrid hexGrid, IHexCell cell)
		{
			float latitude = (float)cell.Coordinates.Z / hexGrid.CellCountZ;
			if (hemisphere == HemisphereMode.Both)
			{
				latitude *= 2f;
				if (latitude > 1f)
				{
					latitude = 2f - latitude;
				}
			}
			else if (hemisphere == HemisphereMode.North)
			{
				latitude = 1f - latitude;
			}

			float temperature =
				Mathf.LerpUnclamped(lowTemperature, highTemperature, latitude);

			temperature *= 1f - (cell.TerrainData.ViewElevation - waterLevel) /
				(elevationMaximum - waterLevel + 1f);

			float jitter =
				HexMetrics.SampleNoise(cell.LocalPosition * 0.1f)[temperatureJitterChannel];

			temperature += (jitter * 2f - 1f) * temperatureJitter;

			return temperature;
		}

		IHexCell GetRandomCell(HexMapNavigation.IHexGrid hexGrid, MapRegion region)
		{
			return hexGrid.GetCell(
				Random.Range(region.xMin, region.xMax),
				Random.Range(region.zMin, region.zMax)
			) as IHexCell;
		}
	}
}