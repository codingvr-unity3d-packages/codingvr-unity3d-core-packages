﻿using UnityEngine;

namespace codingVR.HexMapTerrain
{
	public class HexMapEditorDependentPlatform : MonoBehaviour
	{
		// Start is called before the first frame update
		void Start()
		{
#if UNITY_EDITOR
			gameObject.SetActive(true);
#else
		gameObject.SetActive(false);
#endif
		}

		// Update is called once per frame
		void Update()
		{

		}
	}
}