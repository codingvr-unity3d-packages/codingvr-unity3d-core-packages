﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace codingVR.HexMapTerrain
{
	public class HexMapEditor : MonoBehaviour
	{
		HexMapNavigation.IHexGridManagerOrigin hexGridManager;

		enum OptionalToggle
		{
			Ignore, Yes, No
		}

		// === DIP Constructor ===

		[Inject]
		public void Construct(HexMapNavigation.IHexGridManagerOrigin hexGridManager)
		{
			this.hexGridManager = hexGridManager;
		}

		// === Version ===

		public Text version;

		// === Terrain ===

		// Terrain widget
		public Slider elevationSlider;
		public Slider elevationPerturbStrengthSlider;
		public Slider elevationStep;
		public TMP_Dropdown terrainDropdown;

		// Terrain slider current values
		float activeElevation;
		float activeElevationPerturbStrength;
		float activeElevationStep;
		int activeWaterLevel;

		int activeTerrainTypeIndex = -1;
		int brushSize;

		// Terrain toggles
		bool applyElevation = false;
		bool applyWaterLevel = false;

		// Terrain toggles
		OptionalToggle riverMode, roadMode, walledMode;


		// Terrain delegate
		public void SetTerrainType(TMP_Dropdown terrainDropdown)
		{
			if (terrainDropdown)
				activeTerrainTypeIndex = terrainDropdown.value - 1;
		}

		public void SetTerrainTypeIndex(int index)
		{
			activeTerrainTypeIndex = index;
		}

		public void SetApplyElevation(bool toggle)
		{
			applyElevation = toggle;
		}

		public void SetElevation(Slider slider)
		{
			activeElevation = slider.value;
		}

		public void SetElevationStep(Slider slider)
		{
			activeElevationStep = slider.value;
		}

		public void SetElevationPerturbStrength(Slider slider)
		{
			activeElevationPerturbStrength = slider.value;
		}

		public void SetApplyWaterLevel(bool toggle)
		{
			applyWaterLevel = toggle;
		}

		public void SetWaterLevel(float level)
		{
			activeWaterLevel = (int)level;
		}

		public void SetBrushSize(float size)
		{
			brushSize = (int)size;
		}

		public void SetRiverMode(int mode)
		{
			riverMode = (OptionalToggle)mode;
		}

		public void SetRoadMode(int mode)
		{
			roadMode = (OptionalToggle)mode;
		}

		public void SetApplyHole(bool toggle)
		{
			applyHole = toggle;
		}

		public void UpdateTerrainFromGround()
		{
			for (int i = 0; i < hexGridManager.HexGrid.GetGridChunkCount(); ++i)
			{
				var chunk = hexGridManager.HexGrid.GetGridChunk(i);
				if (chunk != null)
				{
					chunk.Refresh(); 
				}
			}
		}

		// === Environment ===

		// values
		int activeUrbanLevel, activeFarmLevel, activePlantLevel, activeSpecialIndex;
		// toggles
		bool applyUrbanLevel, applyFarmLevel, applyPlantLevel, applySpecialIndex, applyHole;

		// delegates
		public void SetApplyUrbanLevel(bool toggle)
		{
			applyUrbanLevel = toggle;
		}

		public void SetUrbanLevel(float level)
		{
			activeUrbanLevel = (int)level;
		}

		public void SetApplyFarmLevel(bool toggle)
		{
			applyFarmLevel = toggle;
		}

		public void SetFarmLevel(float level)
		{
			activeFarmLevel = (int)level;
		}

		public void SetApplyPlantLevel(bool toggle)
		{
			applyPlantLevel = toggle;
		}

		public void SetPlantLevel(float level)
		{
			activePlantLevel = (int)level;
		}

		public void SetApplySpecialIndex(bool toggle)
		{
			applySpecialIndex = toggle;
		}

		public void SetSpecialIndex(float index)
		{
			activeSpecialIndex = (int)index;
		}

		public void SetWalledMode(int mode)
		{
			walledMode = (OptionalToggle)mode;
		}

		// === Quest panel ===

		// Tag values
		public InputField activeTagField;
		bool applyTag;
		string activeFollowTag;

		// define tag
		public void SetApplyTag(bool toggle)
		{
			applyTag = toggle;
		}

		// links structure
		public void ShowLinkedTags()
		{
			hexGridManager.ShowLinkedTags(true);
		}

		public void ToggleSelectHexCellLink(IHexCell cellPrev, IHexCell cellNext)
		{
			var tagPair = Data.TagPair.MakePair(cellPrev.TagData.Tag, cellNext.TagData.Tag);
			var selected = hexGridManager.ToggleSelectHexCellLink(tagPair);
			if (selected == null)
			{
				selected = hexGridManager.ToggleSelectHexCellLink(Data.TagPair.Swap(tagPair));
			}
		}

		void OnSelectingHexCellLink(Data.TagPair tagPair)
		{
			displayLink.text = "link unselected";

			bool enableLinkProperties = false;
			if (tagPair != null)
			{
				var hexCellLink = FindHexCellLinkProperties(tagPair);
				if (hexCellLink != null)
				{
					thruUnderWaterToggle.SetIsOnWithoutNotify(hexCellLink.ThruUnderWater);
					thruRiverToggle.SetIsOnWithoutNotify(hexCellLink.ThruRiver);
					thruNotExplorableToggle.SetIsOnWithoutNotify(hexCellLink.ThruNotExplorable);
					thruPlantToggle.SetIsOnWithoutNotify(hexCellLink.ThruPlant);
					waypointsToggle.SetIsOnWithoutNotify(hexCellLink.Waypoints);

					enableLinkProperties = true;
					displayLink.text = tagPair.First.TagName + " / " + tagPair.Second.TagName;
				}
			}

			SetHexCellLinkPropertiesToggleInteractable(enableLinkProperties);
		}

		protected void SetHexCellLinkPropertiesToggleInteractable(bool bInteractable)
		{
			waypointsToggle.interactable = bInteractable;
			bInteractable = waypointsToggle.isOn == false & bInteractable;
			thruUnderWaterToggle.interactable = bInteractable;
			thruRiverToggle.interactable = bInteractable;
			thruNotExplorableToggle.interactable = bInteractable;
			thruPlantToggle.interactable = bInteractable;
		}

		public void RemoveSelectedLink()
		{
			hexGridManager.RemoveSelectedLink();
		}

		// Link properties

		// Widgets
		public Toggle thruUnderWaterToggle;
		public Toggle thruRiverToggle;
		public Toggle thruNotExplorableToggle;
		public Toggle thruPlantToggle;
		public Toggle waypointsToggle;
		public TMP_Text displayLink;
		// explorable toggles
		public Toggle explorableIgnoreToggle;
		public Toggle explorableYesToggle;
		public Toggle explorableNoToggle;
		OptionalToggle explorableMode;


		HexCellLinkProperties FindHexCellLinkProperties(Data.TagPair tag)
		{
			HexMapNavigation.IHexCellLinkProperties link = null;
			link = hexGridManager.FindHexCellLinkProperties(tag);
			return link as HexCellLinkProperties;
		}

		HexCellLinkProperties FindCurrentHexCellLink()
		{
			HexMapNavigation.IHexCellLinkProperties link = null;
			var sel = hexGridManager.SelectedHexCellLink;
			if (null != sel)
				link = FindHexCellLinkProperties(sel);
			return link as HexCellLinkProperties;
		}

		void SetApplyPathLinkThruUnderWater(Toggle toogle)
		{
			var link = FindCurrentHexCellLink();
			if (null != link)
			{
				if (link.ThruUnderWater != toogle.isOn)
				{
					link.ThruUnderWater = toogle.isOn;
					hexGridManager.LinkedHexRebuild = true;
				}
			}
		}

		void SetApplyPathLinkThruUnit(Toggle toogle)
		{
			var link = FindCurrentHexCellLink();
			if (null != link)
			{
				if (link.ThruRiver != toogle.isOn)
				{
					link.ThruRiver = toogle.isOn;
					hexGridManager.LinkedHexRebuild = true;
				}
			}
		}

		void SetApplyPathLinkThruNotExplorable(Toggle toogle)
		{
			var link = FindCurrentHexCellLink();
			if (null != link)
			{
				if (link.ThruNotExplorable != toogle.isOn)
				{
					link.ThruNotExplorable = toogle.isOn;
					hexGridManager.LinkedHexRebuild = true;
				}
			}
		}

		void SetApplyPathLinkThruPlant(Toggle toogle)
		{
			var link = FindCurrentHexCellLink();
			if (null != link)
			{
				if (link.ThruPlant != toogle.isOn)
				{
					link.ThruPlant = toogle.isOn;
					hexGridManager.LinkedHexRebuild = true;
				}
			}
		}

		void SetApplyPathLinkWaypoints(Toggle toogle)
		{
			var link = FindCurrentHexCellLink();
			if (null != link)
			{
				if (link.Waypoints != toogle.isOn)
				{
					link.Waypoints = toogle.isOn;
					hexGridManager.LinkedHexRebuild = true;
				}
				SetHexCellLinkPropertiesToggleInteractable(true);
			}
		}

		void SetNotExplorableMode(OptionalToggle option)
		{
			explorableMode = option;
		}

		// === editor ===

		// short drag
		HexMapNavigation.HexDirection shortDragDirection;
		IHexCell shortDragPreviousCell;

		// long drag
		IHexCell longDragPreviousCell;
		IHexCell longDragFirstCell;

		public void SetEditMode(bool toggle)
		{
			enabled = toggle;
		}

		public void ShowGrid(bool visible)
		{
			Material terrainMaterial = null; ;
			if (hexGridManager.HexGrid.GetGridChunkCount() > 0)
			{
				terrainMaterial = hexGridManager.HexGrid.GetGridChunk(0).TerrainMaterial;
			}

			if (visible)
			{
				terrainMaterial?.EnableKeyword("GRID_ON");
			}
			else
			{
				terrainMaterial?.DisableKeyword("GRID_ON");
			}
		}

		void Awake()
		{
			//terrainMaterial.DisableKeyword("GRID_ON");
			Shader.EnableKeyword("HEX_MAP_EDIT_MODE");
			SetEditMode(false);
		}

		public void Start()
		{
			version.text = Application.version;

			// Terrain listener

			if (terrainDropdown)
			{
				terrainDropdown.onValueChanged.AddListener
				(
					delegate { SetTerrainType(terrainDropdown); }
				);
			}

			if (elevationSlider)
			{
				elevationSlider.onValueChanged.AddListener
				(
					delegate { SetElevation(elevationSlider); }
				);
			}

			if (elevationPerturbStrengthSlider)
			{
				elevationPerturbStrengthSlider.onValueChanged.AddListener
				(
					delegate { SetElevationPerturbStrength(elevationPerturbStrengthSlider); }
				);
			}

			if (elevationStep)
			{
				elevationStep.onValueChanged.AddListener
				(
					delegate { SetElevationStep(elevationStep); }
				);
			}


			// Quests Hex link properties listerner

			if (thruUnderWaterToggle)
			{
				thruUnderWaterToggle.onValueChanged.AddListener
				(
					delegate { SetApplyPathLinkThruUnderWater(thruUnderWaterToggle); }
				);
			}

			if (thruRiverToggle)
			{
				thruRiverToggle.onValueChanged.AddListener
				(
					delegate { SetApplyPathLinkThruUnit(thruRiverToggle); }
				);
			}

			if (thruNotExplorableToggle)
			{
				thruNotExplorableToggle.onValueChanged.AddListener
				(
					delegate { SetApplyPathLinkThruNotExplorable(thruNotExplorableToggle); }
				);
			}

			if (thruPlantToggle)
			{
				thruPlantToggle.onValueChanged.AddListener
				(
					delegate { SetApplyPathLinkThruPlant(thruPlantToggle); }
				);
			}

			if (waypointsToggle)
			{
				waypointsToggle.onValueChanged.AddListener
				(
					delegate { SetApplyPathLinkWaypoints(waypointsToggle); }
				);
			}

			// not explorable toggles
			if (explorableIgnoreToggle)
			{
				explorableIgnoreToggle.onValueChanged.AddListener
				(
					delegate { SetNotExplorableMode(OptionalToggle.Ignore); }
				);
			}
			if (explorableYesToggle)
			{
				explorableYesToggle.onValueChanged.AddListener
				(
					delegate { SetNotExplorableMode(OptionalToggle.Yes); }
				);
			}
			if (explorableNoToggle)
			{
				explorableNoToggle.onValueChanged.AddListener
				(
					delegate { SetNotExplorableMode(OptionalToggle.No); }
				);
			}



			// event when a link is selected
			hexGridManager.HexCellLinkOnSelectingEvent.AddListener(OnSelectingHexCellLink);

			// default cell link properties
			SetHexCellLinkPropertiesToggleInteractable(false);
		}


		// === Click left ===
		int countClickLeft = 0;
		float clickLeftTime = 0.0f;
		readonly float clickLeftDelay = 0.3f;
		IHexCell doubleClickedLeftHexCellPrev = null;
		IHexCell doubleClickedLeftHexCellNext = null;

		static void Swap<T>(ref T lhs, ref T rhs)
		{
			T temp;
			temp = lhs;
			lhs = rhs;
			rhs = temp;
		}

		private void Update()
		{
			// FIX: IsPointerOverGameObject is not compatible with GameCreator,
			// Instead EventSystem.current.currentSelectedGameObject does the job
			//see https://support.gamecreator.io/communities/1/topics/1121-ispointerovergameobject-every-time-true-when-gamecreatorcoreeventsystemmanagersingleton-is
			if (EventSystem.current.currentSelectedGameObject == null)
			//if (!EventSystem.current.IsPointerOverGameObject())
			{
				// ================================
				// mouse button left down
				// ================================
				if (Input.GetMouseButtonDown(0))
				{
					// first click we start timer
					if (countClickLeft == 0)
					{
						clickLeftTime = Time.time;
						countClickLeft++;
						return;
					}

					// second click we check delay & we validate therwise we reset
					if (countClickLeft == 1 && (Time.time - clickLeftTime) < clickLeftDelay)
					{
						countClickLeft++;
					}
					else
					{
						countClickLeft = 0;
					}

					// we launch action on second click
					if (countClickLeft == 2)
					{
						//countClickLeft = 0;
						//Debug.Log("GetMouseButtonDown " + countClickLeft);

						bool b = true;

						doubleClickedLeftHexCellNext = GetCellUnderCursor() as IHexCell;
						if (doubleClickedLeftHexCellPrev != null && doubleClickedLeftHexCellNext != null)
						{
							if (doubleClickedLeftHexCellPrev.TagData.Tag != null && doubleClickedLeftHexCellNext.TagData.Tag != null)
							{
								if (doubleClickedLeftHexCellPrev.TagData.Tag.Equals(doubleClickedLeftHexCellNext.TagData.Tag) == false)
								{
									EditCellOnDoubleClick(doubleClickedLeftHexCellPrev, doubleClickedLeftHexCellNext);
								}
								else
								{
									b = false;
								}
							}
						}

						if (b == true)
						{
							Swap<IHexCell>(ref doubleClickedLeftHexCellPrev, ref doubleClickedLeftHexCellNext);
						}

					}
					return;
				}

				// ================================
				// mouse button left up
				// ================================

				if (Input.GetMouseButtonUp(0) && countClickLeft == 0)
				{
					var cell = GetCellUnderCursor();
					if (cell != null)
					{
						EditCellOnClick(cell as IHexCell);
					}
					return;
				}

				// anyway, if delay is up to 0.5 we reset count click
				if ((Time.time - clickLeftTime) >= clickLeftDelay)
				{
					countClickLeft = 0;
				}

				// ================================
				// mouse button left click
				// ================================

				if (Input.GetMouseButton(0))
				{
					HandleInput();
					return;
				}

				// ================================
				// mouse button right click
				// ================================

				if (Input.GetMouseButton(1))
				{
					// get cell information
					var cell = GetCellUnderCursor();
					if (cell != null)
					{
						GetCellInformationUnderCursor(cell as IHexCell);
					}
					else
					{
						Vector3 point = default(Vector3);
						if (RaycastFromScreen(out point))
						{
							GetElevationFromPoint(point);
						}
					}
				}

				// ================================
				// keydown U
				// ================================

				if (Input.GetKeyDown(KeyCode.U))
				{
					return;
				}

				// ================================
				// keydown F
				// ================================

				if (Input.GetKeyDown(KeyCode.F))
				{
					return;
				}
			}
			if (longDragFirstCell != null)
			{
				EditCellOnLongDragDrop(longDragFirstCell, GetCellUnderCursor() as IHexCell);
			}
			shortDragPreviousCell = null;
			longDragPreviousCell = null;
			longDragFirstCell = null;
		}

		// === raycast from screen ===

		bool RaycastFromScreen(out Vector3 point)
		{
			// make a ray from mouse mouse position screen point 
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			var b = Physics.Raycast(ray, out hit);
			point = hit.point;
			return b;
		}

		// === get cell under cursor ===

		HexMapNavigation.IHexCellOrigin GetCellUnderCursor()
		{
			// make a ray from mouse mouse position screen point 
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			var cell = hexGridManager.HexGrid?.GetCell(ray);
			return cell;
		}
		
		HexMapNavigation.IHexCellOrigin GetDefaultCellUnderCursor()
		{
			// if cell == null we try to find a virtual cell according to current Elevation
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			var cell = hexGridManager.HexGrid?.GetCell(ray, activeElevation, activeElevationStep);
			return cell;
		}

		// === get elevation from point ===

		public struct Elevation
		{
			public float elevation;
			public float elevationPerturbStrength;
			public float elevationStep;
		}

		void GetElevationFromPoint(Vector3 point)
		{
			if (elevationSlider)
			{
				elevationSlider.value = point.y;
				elevationPerturbStrengthSlider.value = 0.0f;
				elevationStep.value = 1.0f / HexMapNavigation.HexMetrics.scale;
			}
		}

		Elevation SaveElevationFromPoint()
		{
			var elevationStruct = new Elevation();
			if (elevationSlider)
			{
				elevationStruct.elevation = elevationSlider.value;
				elevationStruct.elevationPerturbStrength = elevationPerturbStrengthSlider.value;
				elevationStruct.elevationStep = elevationStep.value;
			}
			return elevationStruct;
		}

		void RestoreElevationFromPoint(Elevation elevationStruct)
		{
			elevationSlider.value = elevationStruct.elevation;
			elevationPerturbStrengthSlider.value = elevationStruct.elevationPerturbStrength;
			elevationStep.value = elevationStruct.elevationStep;
		}

		// === get information under cursor ===

		void GetCellInformationUnderCursor(IHexCell cell)
		{
			if (cell != null)
			{
				if (terrainDropdown)
				{
					terrainDropdown.value = cell.TerrainData.TerrainTypeIndex + 1;
				}
				if (elevationSlider)
				{
					elevationSlider.value = cell.TerrainData.Elevation;
					elevationPerturbStrengthSlider.value = cell.TerrainData.ElevationPerturbStrength;
					elevationStep.value = cell.TerrainData.ElevationStep;
				}
				if (activeTagField)
				{
					if (cell.TagData.Tag != null)
					{
						activeTagField.text = cell.TagData.Tag.TagName;
					}
				}
				if (explorableMode != OptionalToggle.Ignore)
				{
					explorableMode = cell.Explorable ? OptionalToggle.Yes : OptionalToggle.No;
					explorableYesToggle.SetIsOnWithoutNotify(explorableMode == OptionalToggle.Yes);
					explorableNoToggle.SetIsOnWithoutNotify(explorableMode == OptionalToggle.No);
				}

				/*
				if (applyWaterLevel)
				{
					cell.WaterLevel = activeWaterLevel;
				}
				if (applySpecialIndex)
				{
					cell.SpecialIndex = activeSpecialIndex;
				}
				if (applyUrbanLevel)
				{
					cell.UrbanLevel = activeUrbanLevel;
				}
				if (applyFarmLevel)
				{
					cell.FarmLevel = activeFarmLevel;
				}
				if (applyPlantLevel)
				{
					cell.PlantLevel = activePlantLevel;
				}
				if (riverMode == OptionalToggle.No)
				{
					cell.RemoveRiver();
				}
				if (roadMode == OptionalToggle.No)
				{
					cell.RemoveRoads();
				}
				if (walledMode != OptionalToggle.Ignore)
				{
					cell.Walled = walledMode == OptionalToggle.Yes;
				}

				if (isShortDrag == true)
				{
					IHexCell otherCell = cell.GetNeighbor(shortDragDirection.Opposite());
					if (otherCell)
					{
						if (riverMode == OptionalToggle.Yes)
						{
							otherCell.SetOutgoingRiver(shortDragDirection);
						}
						if (roadMode == OptionalToggle.Yes)
						{
							otherCell.AddRoad(shortDragDirection);
						}
					}
				}
				*/
			}
		}

		// === handle mouse input ===  

		Vector3 lastMousePosition = Vector3.zero;
		void HandleInput()
		{
			if (Input.mousePosition != lastMousePosition)
			{
				lastMousePosition = Input.mousePosition;
				// get cell under cursor
				var currentCell = GetCellUnderCursor();
				if (null == currentCell)
				{
					// if failed 
					// by example raycast intercepting a structure
					// we use (x, z) of hexamp for deducting the cell 
					Vector3 point;
					if (RaycastFromScreen(out point))
					{
						currentCell = hexGridManager.HexGrid.GetCell(point);
					}
				}
				// if cell == null we try to find a virtual cell according to current Elevation
				// we can't get default cell when applyHole is activated
				if (currentCell == null && applyHole == false)
				{
					currentCell = GetDefaultCellUnderCursor();
				}

				if (currentCell != null)
				{
					bool isShortDrag;

					{
						if (shortDragPreviousCell != null && shortDragPreviousCell != currentCell)
						{
							isShortDrag = ValidateDragShort(shortDragPreviousCell, currentCell as IHexCell);
						}
						else
						{
							isShortDrag = false;
						}
						shortDragPreviousCell = currentCell as IHexCell;
					}
					
					{
						if (longDragPreviousCell != null && longDragPreviousCell != currentCell)
						{
							if (longDragFirstCell == null)
							{
								longDragFirstCell = longDragPreviousCell;
							}
							//Plugins.DebugLine.DrawLine(longDragFirstCell.Position, currentCell.Position, Color.green, 0.25f, 4.0f);
						}
						longDragPreviousCell = currentCell as IHexCell;
					}

					EditCells(currentCell as IHexCell, isShortDrag);
				}
				else
				{
					shortDragPreviousCell = null;
					longDragPreviousCell = null;
					longDragFirstCell = null;
				}
			}
		}

		// === check if drah action has started ===

		bool ValidateDragShort(IHexCell shortDragPreviousCell, IHexCell currentCell)
		{
			for (shortDragDirection = HexMapNavigation.HexDirection.NE; shortDragDirection <= HexMapNavigation.HexDirection.NW; shortDragDirection++)
			{
				if (shortDragPreviousCell.GetNeighbor(shortDragDirection) == currentCell)
				{
					return true;
				}
			}
			return false;
		}

		// === edit cells according to brush size ===

		void EditCellOnDoubleClick(IHexCell cellPrev, IHexCell cellNext)
		{
			// Select / unselect cell Link
			ToggleSelectHexCellLink(cellPrev, cellNext);
		}

		void EditCellOnClick(IHexCell cell)
		{
			if (applyTag)
			{
				if (string.IsNullOrEmpty(activeTagField.text) == false)
				{
					hexGridManager.SetCellTag(cell, Data.Tag.CreateTag(activeTagField.text));
				}
				else
				{
					hexGridManager.SetCellTag(cell, null);
				}
			}
		}

		void EditCellOnLongDragDrop(IHexCell FromCell, IHexCell ToCell)
		{
			var selectedCell = GetCellUnderCursor();
			if (longDragFirstCell.TagData.Tag != null && selectedCell != null)
			{
				if (hexGridManager.LinkHexCell(longDragFirstCell, selectedCell) == true)
				{
					hexGridManager.SelectedHexCellLink = Data.TagPair.MakePair(FromCell.TagData.Tag, ToCell.TagData.Tag);
				}
			}
		}

		void EditCells(IHexCell center, bool isShortDrag)
		{
			int centerX = center.Coordinates.X;
			int centerZ = center.Coordinates.Z;

			for (int r = 0, z = centerZ - brushSize; z <= centerZ; z++, r++)
			{
				for (int x = centerX - r; x <= centerX + brushSize; x++)
				{
					EditCell(hexGridManager.HexGrid.GetCell(new HexMapNavigation.HexCoordinates(x, z)) as IHexCell, isShortDrag);
				}
			}
			for (int r = 0, z = centerZ + brushSize; z > centerZ; z--, r++)
			{
				for (int x = centerX - brushSize; x <= centerX + r; x++)
				{
					EditCell(hexGridManager.HexGrid.GetCell(new HexMapNavigation.HexCoordinates(x, z)) as IHexCell, isShortDrag);
				}
			}
		}

		// === edit cell according to  UI Properties ===

		void EditCell(IHexCell cell, bool isShortDrag)
		{
			if (cell != null)
			{
				if (applyHole == true && cell.TerrainData != null)
				{
					cell.MakeHole();
					hexGridManager.LinkedHexRebuild = true;
					return;
				}

				if (cell.TerrainData == null && applyHole == false)
				{
					HexMapNavigation.HexCellShaderData shaderData = hexGridManager.HexGrid.ShaderData;

					Vector3 point;
					if (RaycastFromScreen(out point))
					{
						cell.PlugTerrain(shaderData, point.y);
					}
					else
					{
						cell.PlugTerrain(shaderData, 0.0f);
					}
					hexGridManager.LinkedHexRebuild = true;
				}

				if (explorableMode != OptionalToggle.Ignore)
				{
					bool isYes = (explorableMode == OptionalToggle.Yes);
					if (cell.Explorable != isYes)
					{
						cell.Explorable = isYes;
						hexGridManager.LinkedHexRebuild = true;
					}
				}

				if (cell.TerrainData != null)
				{
					if (activeTerrainTypeIndex >= 0)
					{
						cell.TerrainData.TerrainTypeIndex = activeTerrainTypeIndex;
					}
					if (applyElevation)
					{
						cell.TerrainData.Elevation = activeElevation;
						cell.TerrainData.ElevationPerturbStrength = activeElevationPerturbStrength;
						cell.TerrainData.ElevationStep = activeElevationStep;
					}

					if (applyWaterLevel)
					{
						cell.TerrainData.WaterLevel = activeWaterLevel;
						hexGridManager.LinkedHexRebuild = true;
					}
					if (applySpecialIndex)
					{
						cell.TerrainData.SpecialIndex = activeSpecialIndex;
					}
					if (applyUrbanLevel)
					{
						cell.TerrainData.UrbanLevel = activeUrbanLevel;
					}
					if (applyFarmLevel)
					{
						cell.TerrainData.FarmLevel = activeFarmLevel;
					}
					if (applyPlantLevel)
					{
						if (cell.TerrainData.PlantLevel != activePlantLevel)
						{
							cell.TerrainData.PlantLevel = activePlantLevel;
							hexGridManager.LinkedHexRebuild = true;
						}
					}
					if (riverMode == OptionalToggle.No)
					{
						cell.TerrainData.RemoveRiver();
					}
					if (roadMode == OptionalToggle.No)
					{
						cell.TerrainData.RemoveRoads();
					}

					if (walledMode != OptionalToggle.Ignore)
					{
						cell.TerrainData.Walled = walledMode == OptionalToggle.Yes;
					}

					if (isShortDrag == true)
					{
						IHexCell otherCell = cell.GetNeighbor(shortDragDirection.Opposite()) as IHexCell;
						if (otherCell != null)
						{
							if (riverMode == OptionalToggle.Yes)
							{
								HexMapNavigation.HexCellShaderData shaderData = hexGridManager.HexGrid.ShaderData;
								otherCell.TerrainData.SetOutgoingRiver(shortDragDirection);
							}
							if (roadMode == OptionalToggle.Yes)
							{
								HexMapNavigation.HexCellShaderData shaderData = hexGridManager.HexGrid.ShaderData;
								otherCell.TerrainData.AddRoad(shortDragDirection);
							}
						}
					}
				}
			}
		}
	}
}