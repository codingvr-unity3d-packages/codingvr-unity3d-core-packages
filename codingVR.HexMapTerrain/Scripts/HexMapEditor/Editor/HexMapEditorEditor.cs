﻿using UnityEditor;
using UnityEngine;

namespace codingVR.HexMapTerrain
{
	[CustomEditor(typeof(HexMapEditor))]
	public class HexMapEditorEditor : UnityEditor.Editor
	{
		private void OnSceneGUI()
		{
			Event e = Event.current;

			if (e.commandName != "")
			{
				Debug.LogError("Command recognized: " + e.commandName + "game object: " + Selection.gameObjects);
			}
		}
	}
}