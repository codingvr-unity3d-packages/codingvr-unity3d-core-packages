﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using Zenject;
using System.Collections;

namespace codingVR.HexMapTerrain
{
	public class SaveLoadMenu : MonoBehaviour
	{
		public Text menuLabel, actionButtonLabel;
		public InputField nameInput;
		public RectTransform listContent;
		public SaveLoadItem itemPrefab;

		HexMapNavigation.IHexGridManagerOrigin hexGridManager;
		Scene.IResourcesLoader resourcesLoader;
		HexMapNavigation.HexMapCamera hexMapCamera;

		bool saveMode;

		[Inject]
		public void Construct(HexMapNavigation.IHexGridManagerOrigin hexGridManager, Scene.IResourcesLoader resourcesLoader, HexMapNavigation.HexMapCamera hexMapCamera)
		{
			this.hexGridManager = hexGridManager;
			this.hexMapCamera = hexMapCamera;
			this.resourcesLoader = resourcesLoader;
		}

		public void Open(bool saveMode)
		{
			this.saveMode = saveMode;
			if (saveMode)
			{
				menuLabel.text = "Save Map";
				actionButtonLabel.text = "Save";
			}
			else
			{
				menuLabel.text = "Load Map";
				actionButtonLabel.text = "Load";
			}
			FillList();
			gameObject.SetActive(true);
			hexMapCamera.Locked = true;
		}

		public void Close()
		{
			gameObject.SetActive(false);
			hexMapCamera.Locked = false;
		}

		public void Action()
		{
			string path = GetSelectedPath();
			if (path == null)
			{
				return;
			}
			if (saveMode)
			{
				Save(path);
			}
			else
			{
				StartCoroutine(Load(path));
			}
			Close();
		}

		public void SelectItem(string name)
		{
			nameInput.text = name;
		}

		public void Delete()
		{
			string path = GetSelectedPath();
			if (path == null)
			{
				return;
			}
			if (File.Exists(path))
			{
				File.Delete(path);
			}
			nameInput.text = "";
			FillList();
		}

		string GetStreaminAddressablesAssets()
		{
			return "Assets/StreamingAddressablesAssets/";
		}

		void FillList()
		{
			for (int i = 0; i < listContent.childCount; i++)
			{
				Destroy(listContent.GetChild(i).gameObject);
			}
			string[] paths = Directory.GetFiles(GetStreaminAddressablesAssets(), "*.json");
			Array.Sort(paths);
			for (int i = 0; i < paths.Length; i++)
			{
				SaveLoadItem item = Instantiate(itemPrefab);
				item.menu = this;
				item.MapName = Path.GetFileNameWithoutExtension(paths[i]);
				item.MapName = Path.GetFileNameWithoutExtension(item.MapName);
				item.transform.SetParent(listContent, false);
			}
		}

		string GetSelectedPath()
		{
			return HexMapNavigation.HexGridLoader.GetSelectedPath(nameInput.text);
		}

		void Save(string path)
		{
			HexMapNavigation.HexGridLoader.Save(hexGridManager, path);
		}

		IEnumerator Load(string path)
		{
			yield return HexMapNavigation.HexGridLoader.Load(hexGridManager, resourcesLoader, path, false);
		}
	}
}