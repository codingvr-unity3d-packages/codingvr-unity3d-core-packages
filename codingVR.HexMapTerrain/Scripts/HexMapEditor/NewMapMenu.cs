﻿using UnityEngine;
using Zenject;

namespace codingVR.HexMapTerrain
{
	public class NewMapMenu : MonoBehaviour
	{
		HexMapNavigation.IHexGridManagerOrigin hexGridManager;
		HexMapNavigation.HexMapCamera hexMapCamera;
		bool generateMaps = true;
		bool wrapping = false;
		bool emptyMaps = false;

		// === DIP Constructor ===

		[Inject]
		public void Construct(HexMapNavigation.IHexGridManagerOrigin hexGridManager, HexMapNavigation.HexMapCamera hexMapCamera)
		{
			this.hexGridManager = hexGridManager;
			this.hexMapCamera = hexMapCamera;
		}

		// === Menu ===

		public void ToggleMapGeneration(bool toggle)
		{
			generateMaps = toggle;
		}

		public void ToggleWrapping(bool toggle)
		{
			wrapping = toggle;
		}

		public void ToggleMapEmpty(bool toggle)
		{
			emptyMaps = toggle;
		}

		public void Open()
		{
			gameObject.SetActive(true);
			hexMapCamera.Locked = true;
		}

		public void Close()
		{
			gameObject.SetActive(false);
			hexMapCamera.Locked = false;
		}

		public void CreateSmallMap()
		{
			CreateMap(20, 15, HexMapNavigation.HexMetrics.chunkSizeX, HexMapNavigation.HexMetrics.chunkSizeZ);
		}

		public void CreateMediumMap()
		{
			CreateMap(40, 30, HexMapNavigation.HexMetrics.chunkSizeX, HexMapNavigation.HexMetrics.chunkSizeZ);
		}

		public void CreateLargeMap()
		{
			CreateMap(80, 60, HexMapNavigation.HexMetrics.chunkSizeX, HexMapNavigation.HexMetrics.chunkSizeZ);
		}

		public void CreateExtraLargeMap()
		{
			CreateMap(120, 90, HexMapNavigation.HexMetrics.chunkSizeX, HexMapNavigation.HexMetrics.chunkSizeZ);
		}

		bool CreateMap(int x, int z, int chunkSizeX, int chunkSizeZ)
		{
			bool b = false;
			if (generateMaps && !emptyMaps)
			{
				b = hexGridManager.GenerateMap(x, z, chunkSizeX, chunkSizeZ, wrapping);
			}
			else
			{
				b = hexGridManager.CreateMap(x, z, chunkSizeX, chunkSizeZ, wrapping, emptyMaps);
			}
			hexMapCamera.ValidatePosition();
			Close();
			return b;
		}
	}
}