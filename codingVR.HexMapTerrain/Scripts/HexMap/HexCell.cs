﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace codingVR.HexMapTerrain
{
	class HexCell : MonoBehaviour, IHexCell
	{
		private void Awake()
		{
		}

		/*
		 * 
		 * Setup
		 * 
		 */

		public void AddCellToChunk(HexMapNavigation.IHexGridChunkOrigin chunk, Transform transformParent, Transform uiRectTransformParent)
		{
			this.Chunk = chunk;
			this.transform.SetParent(transformParent, false);
			this.UIRect.SetParent(uiRectTransformParent, false);
		}

		private void CreateCell(Vector3 position, HexMapNavigation.HexCoordinates coordinates, int index, int columnIndex, HexMapNavigation.HexCellShaderData shaderData)
		{
			this.transform.localPosition = position;
			this.Coordinates = coordinates;
			this.Index = index;
			this.ColumnIndex = columnIndex;
			if (this.terrainData != null)
				this.terrainData.ShaderData = shaderData;
		}
		
		public void CreateCell(int x, int z, int chunkSizeX, int chunkSizeZ, int index, HexMapNavigation.IHexCellLabelFactory hexCellLabelFactory, HexMapNavigation.HexCellShaderData shaderData, IHexCellTerrainDataFactory terrainDataFactory)
		{
			// create component
			terrainData = terrainDataFactory?.Create(this) as HexCellTerrainData;
			tagData = new HexMapNavigation.HexCellTagData(this);
			heuristicData = new HexMapNavigation.HexCellHeuristicData(this);
			unitData = new HexMapNavigation.HexCellUnitData(this);

			// create cell
			var offsetCoordinate = HexMapNavigation.HexCoordinates.FromOffsetCoordinates(x, z);
			Vector3 position = offsetCoordinate.GetPosition();
			CreateCell(position, offsetCoordinate, index, x / chunkSizeX, shaderData);
			UIRect = hexCellLabelFactory.Create(new Vector2(position.x, position.z));

			// set default label 
			MakeDefaultLabel();
		}

		/*
		 * 
		 * Gizmo
		 * 
		 */


		void OnDrawGizmos()
		{
			Image highlight = UIRect.GetChild(0).GetComponent<Image>();
			if (highlight.enabled == true)
			{
				heuristicData.DrawGizmos(highlight.color);
			}
		}
		
		/*
		 * 
		 * Terrain Data
		 * 
		 */

		HexCellTerrainData terrainData = null;
			   
		public IHexCellTerrainData TerrainData
		{
			get
			{
				return terrainData;
			}
		}

		static HexCellTerrainData terrainDefaultData = null;

		public IHexCellTerrainData TerrainOrDefaultData()
		{
			IHexCellTerrainData returnTerrainData = terrainData;
			
			if (null == returnTerrainData)
			{
				returnTerrainData = HexCellTerrainData.MakeDefaultHexCellTerrainData(this);
			}

			return returnTerrainData;
		}

		/*
		 * 
		 * Tag Data
		 * 
		 */

		HexMapNavigation.IHexCellTagData tagData = null;

		public HexMapNavigation.IHexCellTagData TagData
		{
			get
			{
				return tagData;
			}
		}

		/*
		 * 
		 * Heuristic Data
		 * 
		 */

		HexMapNavigation.HexCellHeuristicData heuristicData = null;

		public HexMapNavigation.IHexCellHeuristicData HeuristicData
		{
			get
			{
				return heuristicData;
			}
		}

		/*
		 * 
		 * States
		 * 
		 */

		bool explored = false;

		public bool IsExplored
		{
			get
			{
				return explored && Explorable;
			}
			set
			{
				explored = value;
				terrainData?.ShaderData.RefreshVisibility(this);
			}
		}

		bool explorable = false;

		public bool Explorable
		{
			get
			{
				return explorable;
			}
			set
			{
				if (explorable != value)
				{
					explorable = value;
					RefreshCanvasTextLabel();
				}
			}
		}

		public float Range
		{
			get
			{
				return HexCellTerrainData.MakeDefaultHexCellTerrainData(this).ViewElevation;
			}
		}

		/*
		 * 
		 * Path constraints
		 * 
		 */

		public bool IsValidDestination()
		{
			return IsExplored && !TerrainData.IsUnderwater && UnitData.Unit == null;
		}

		public int GetMoveCost(HexMapNavigation.IHexCellOrigin fromCellOrigin, HexMapNavigation.HexDirection direction)
		{
			var toCell = this;
			var fromCell = fromCellOrigin as IHexCell;

			if (!toCell.IsValidDestination())
			{
				return -1;
			}
			HexEdgeType edgeType = fromCell.TerrainData.GetEdgeType(toCell);
			if (edgeType == HexEdgeType.Cliff)
			{
				return -1;
			}
			int moveCost;
			if (fromCell.TerrainData.HasRoadThroughEdge(direction))
			{
				moveCost = 1;
			}
			else if (fromCell.TerrainData.Walled != toCell.TerrainData.Walled)
			{
				return -1;
			}
			else
			{
				moveCost = edgeType == HexEdgeType.Flat ? 5 : 10;
				moveCost += toCell.TerrainData.UrbanLevel + toCell.TerrainData.FarmLevel + toCell.TerrainData.PlantLevel;
			}
			return moveCost;
		}


		/*
		 * 
		 * Unit Data
		 * 
		 */

		HexMapNavigation.IHexCellUnitData unitData = null;

		public HexMapNavigation.IHexCellUnitData UnitData
		{
			get
			{
				return unitData;
			}
		}

		/*
		 * 
		 * Layout
		 * 
		 */

		public bool PlugTerrain(HexMapNavigation.HexCellShaderData shaderData, float elevationParam)
		{
			bool bPlugged = false;
			if (terrainData == null)
			{
				this.terrainData = new HexCellTerrainData(this);
				this.terrainData.ShaderData = shaderData;
				var localPosition = this.LocalPosition;
				localPosition.y = elevationParam;
				this.LocalPosition = localPosition;
				this.terrainData.Elevation = localPosition.y;
				this.terrainData.ElevationPerturbStrength = 0.0f;
				this.terrainData.ElevationStep = 1.0f / HexMapNavigation.HexMetrics.scale;
				MakeDefaultLabel();
				Refresh();
			}
			return bPlugged ? true : false;
		}

		public bool MakeHole()
		{
			bool bHoleMade = true;

			Image highlight = UIRect.GetChild(0).GetComponent<Image>();

			if (tagData.Tag != null)
				bHoleMade = false;
			if (highlight.enabled == true)
				bHoleMade = false;
			if (Explorable == false)
				bHoleMade = false;
			
			if (bHoleMade == true)
			{
				terrainData = null;
				SetLabel(null);
				Refresh();
				Vector3 pos = transform.localPosition;
				pos.y = 0.0f;
				transform.localPosition = pos;
			}
			return bHoleMade;
		}

		/*
		 * 
		 * Coordinates
		 * 
		 */

		// hex coordinate
		public HexMapNavigation.HexCoordinates Coordinates { get; private set; }
		// cells indexes
		public int Index { get; private set; }
		public int ColumnIndex { get; private set; }

		// position
		public Vector3 LocalPosition
		{
			get
			{
				return transform.localPosition;
			}
			set
			{
				UpdateNewPosition(value);
			}
		}

		public Vector3 Position
		{
			get
			{
				return transform.position;
			}
		}

		public Vector3 ComputeNextLocalPosition(float elevation, float elevationStep, float elevationPerturbStrength)
		{
			Vector3 position = transform.localPosition;
			position.y = elevation * elevationStep * HexMapNavigation.HexMetrics.scale;
			position.y += (HexMetrics.SampleNoise(position).y * 2f - 1f) * elevationPerturbStrength * HexMapNavigation.HexMetrics.scale;
			return position;
		}

		public void UpdateNewPosition(Vector3 newPosition)
		{
			// new postion
			transform.localPosition = newPosition;

			// update text info position
			Vector3 uiPosition = UIRect.localPosition;
			uiPosition.z = -newPosition.y;
			UIRect.localPosition = uiPosition;
		}

		/*
		 * 
		 * Chunks
		 * 
		 */

		// parent chunk (owner of cell)
		public HexMapNavigation.IHexGridChunkOrigin Chunk { get; private set; }

		public void Refresh()
		{
			if (Chunk != null)
			{
				Chunk.Refresh();
				for (int i = 0; i < neighbors.Length; i++)
				{
					HexMapNavigation.IHexCellOrigin neighbor = neighbors[i];
					if (neighbor != null && neighbor.Chunk != Chunk)
					{
						neighbor.Chunk.Refresh();
					}
				}
			}
		}

		public void RefreshChunkContainer()
		{
			Chunk?.Refresh();
		}
		
		/*
		 * 
		 * Neighbhors
		 * 
		 */

		// neighbors
		HexMapNavigation.IHexCellOrigin[] neighbors = new HexMapNavigation.IHexCellOrigin[6];

		public HexMapNavigation.IHexCellOrigin[] Neighbors
		{
			get
			{
				return neighbors;
			}
		}

		public int NeighborCount
		{
			get
			{
				return neighbors.Length;
			}
		}

		public HexMapNavigation.IHexCellOrigin GetNeighbor(HexMapNavigation.HexDirection direction)
		{
			return neighbors[(int)direction];
		}

		public void SetNeighbor(HexMapNavigation.HexDirection direction, HexMapNavigation.IHexCellOrigin cell)
		{
			SetNeighborCell((int)direction, cell);
			cell.SetNeighborCell((int)direction.Opposite(), this);
		}

		public void SetNeighborCell(int index, HexMapNavigation.IHexCellOrigin cell)
		{
			neighbors[index] = cell;
		}
		
		/*
		 * 
		 * UI
		 * 
		 */

		// canvas rect transform    
		public RectTransform UIRect { get; private set; }

		// text & label
		private string textLabel;

		void UpdateUITRect(string text)
		{
			Text label = UIRect.GetComponent<Text>();
			// if text is not a number then a small font size is selected
			label.text = text;
			if (label.text != null)
			{
				if (label.text.Length > 1)
					label.fontSize = 4;
				else
					label.fontSize = 20;
			}
		}

		void MakeDefaultLabel()
		{
			// set default label 
			//string text = "[" + this.Coordinates.X + "," + this.Coordinates.Y + "]";
			//SetLabel(text);
		}

		public void SetLabel(string text)
		{
			Text label = UIRect.GetComponent<Text>();
			textLabel = text;
			if (tagData != null)
				textLabel = tagData.RefreshCanvasTextLabel(label, textLabel);
			if (terrainData != null)
				textLabel = terrainData.RefreshCanvasTextLabel(label, textLabel);
			UpdateUITRect(textLabel);
		}

		public void RefreshCanvasTextLabel()
		{
			Text label = UIRect.GetComponent<Text>();
			if (tagData != null)
				textLabel = tagData.RefreshCanvasTextLabel(label, textLabel);
			if (terrainData != null)
				textLabel = terrainData.RefreshCanvasTextLabel(label, textLabel);
			UpdateUITRect(textLabel);
		}

		public void DisableHighlight(Data.Tag tag)
		{
			Image highlight = UIRect.GetChild(0).GetComponent<Image>();
			highlight.enabled = false;
		}

		public void EnableHighlight(Data.Tag tag, Color color)
		{
			Image highlight = UIRect.GetChild(0).GetComponent<Image>();
			highlight.color = color;
			highlight.enabled = true;
		}
		
		/*
		 * 
		 * Load & Save
		 * 
		 */

		// Streaming
		public void Save(Core.GameDataWriter writer)
		{
			// Cell config
			writer.WriteObjectStart("Cell-Config");
			writer.Write(terrainData == null ? false : true, "hasTerrain");
			writer.WriteObjectEnd();
						
			// Tag data
			tagData.Save(writer);

			// Terrain data
			terrainData?.Save(writer);

			// Cell properties
			writer.WriteObjectStart("Cell-Properties");
			writer.Write(Explorable, "explorable");
			writer.WriteObjectEnd();
		}

		public void Load(Core.GameDataReader reader, int version)
		{
			bool bRefreshUI = false;
			if (version >= 10)
			{
				// Cell config
				if (version >= 20)
				{
					reader.ReadObjectStart("Cell-Config");
					bool hasTerrain = false;
					hasTerrain = reader.ReadBoolean();
					if (hasTerrain == true)
					{
						terrainData = terrainData ?? new HexCellTerrainData(this);
					}
					else
					{
						terrainData = null;
					}
					reader.ReadObjectEnd();
				}

				// Tag data
				tagData.Load(reader, version);

				// Terrain data
				terrainData?.Load(reader, version);

				// States
				if (version < 17)
				{
					reader.ReadObjectStart("Cell-States");
					IsExplored = reader.ReadBoolean();
					reader.ReadObjectEnd();
					terrainData?.ShaderData.RefreshVisibility(this);
				}

				// Cell properties
				if (version >= 18)
				{
					reader.ReadObjectStart("Cell-Properties");
					Explorable = reader.ReadBoolean();
					reader.ReadObjectEnd();
				}

				if (Explorable == false)
				{
					bRefreshUI = true;
				}
			}
			else
			{
				terrainData?.Load(reader, version);
				tagData.Load(reader, version);
			}

			if (true == bRefreshUI)
			{
				RefreshCanvasTextLabel();
			}
		}
	}
}