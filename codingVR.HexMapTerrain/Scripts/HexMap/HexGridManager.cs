﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Zenject;
using UnityEngine.UI;
using System.Threading.Tasks;
using System.Linq;

namespace codingVR.HexMapTerrain
{
	// === Hex grid manager ===============================================================

	using TagManager = Data.TagCollectionManager<HexMapNavigation.IHexGridManagerOrigin, HexMapNavigation.HexCellLinked>;

	class HexGridManager : MonoBehaviour, HexMapNavigation.IHexGridManagerOrigin
	{
		// === Factory ===

		public class HexCellLinkPropertiesFactory : HexMapNavigation.IHexCellLinkPropertiesFactory
		{
			public HexMapNavigation.IHexCellLinkProperties Create(Data.Tag tag)
			{
				return HexCellLinkProperties.Create(tag);
			}

			public HexMapNavigation.IHexCellLinkProperties Load(Core.GameDataReader reader, int version)
			{
				return HexCellLinkProperties.Load(reader, version);
			}
		}
		
		public class HexCellFactory : HexMapNavigation.IHexCellFactory
		{
			UnityEngine.Object hexCellPrefab;
			IHexCellTerrainDataFactory terrainDataFactory;

			public HexCellFactory(UnityEngine.Object hexCellPrefab, IHexCellTerrainDataFactory terrainDataFactory)
			{
				this.hexCellPrefab = hexCellPrefab;
				this.terrainDataFactory = terrainDataFactory;
			}

			public HexMapNavigation.IHexCellOrigin Create(HexMapNavigation.IHexCellLabelFactory hexCellLabelFactory, int x, int z, int chunkSizeX, int chunkSizeZ, int index, HexMapNavigation.HexCellShaderData shaderData)
			{
				var go = Instantiate(hexCellPrefab) as GameObject;
				var cell = go.GetComponent<HexCell>();
				cell.CreateCell(x, z, chunkSizeX, chunkSizeZ, index, hexCellLabelFactory, shaderData, terrainDataFactory);
				return cell;
			}
		}

		public class HexCellLabelFactory : HexMapNavigation.IHexCellLabelFactory
		{
			Object hexCellLabelPrefab;

			public HexCellLabelFactory(Object hexCellLabelPrefab)
			{
				this.hexCellLabelPrefab = hexCellLabelPrefab;
			}

			public RectTransform Create(Vector2 position)
			{
				var go = Instantiate(hexCellLabelPrefab) as GameObject;
				Text label = go.GetComponent<Text>();
				label.rectTransform.anchoredPosition = position;
				label.rectTransform.transform.localScale *= Vector2.one * HexMapNavigation.HexMetrics.scale;
				return label.rectTransform;
			}
		}
		
		public class HexGridFactory : HexMapNavigation.IHexGridFactory
		{
			Object hexGridPrefab;

			public HexGridFactory(Object hexGridPrefab)
			{
				this.hexGridPrefab = hexGridPrefab;
			}

			public HexMapNavigation.IHexGrid Create(Transform parentTranform)
			{
				var go = Instantiate(hexGridPrefab) as GameObject;
				var cell = go.GetComponent<HexMapNavigation.HexGrid>();
				go.transform.SetParent(parentTranform, false);
				return cell;
			}
		}

		public class HexChunkFactory : HexMapNavigation.IHexChunkFactory
		{
			Object chunkPrefab;
			DiContainer diContainer;

			public HexChunkFactory(DiContainer diContainer, Object chunkPrefab)
			{
				this.chunkPrefab = chunkPrefab;
				this.diContainer = diContainer;
			}

			public HexMapNavigation.IHexGridChunkOrigin Create(Transform parentTranform, int chunkSizeX, int chunkSizeZ)
			{
				var gochunk = Instantiate(chunkPrefab) as GameObject;
				gochunk.transform.SetParent(parentTranform, false);
				var chunk = gochunk.GetComponent<HexMapNavigation.IHexGridChunkOrigin>();
				chunk.Inject(diContainer);
				chunk.Setup(chunkSizeX, chunkSizeZ);
				return chunk;
			}
		}

		public class HexCellTerrainDataFactory : HexMapTerrain.IHexCellTerrainDataFactory
		{
			public HexMapTerrain.IHexCellTerrainData Create(IHexCell hexCell)
			{
				return new HexCellTerrainData(hexCell);
			}
		}

		// === Hex prefabs ===

		[SerializeField]
		Object cellPrefab;
		[SerializeField]
		Object cellLabelPrefab;
		[SerializeField]
		Object chunkPrefab;
		[SerializeField]
		Object gridPrefab;

		public Object HexCellPrefab
		{
			get
			{
				return cellPrefab;
			}
			set
			{
				if (value != cellPrefab)
					cellPrefab = value;
			}
		}

		public Object HexCellLabelPrefab
		{
			get
			{
				return cellLabelPrefab;
			}
			set
			{
				if (value != cellLabelPrefab)
					cellLabelPrefab = value;
			}
		}
		public Object HexChunkPrefab
		{
			get
			{
				return chunkPrefab;
			}
			set
			{
				if (value != chunkPrefab)
					chunkPrefab = value;
			}
		}
		public Object HexGridPrefab
		{
			get
			{
				return gridPrefab;
			}
			set
			{
				if (value != gridPrefab)
					gridPrefab = value;
			}
		}

		// === map generator ===

		[SerializeField]
		HexMapNavigation.IHexMapGenerator mapGenerator;
		
		// === PRN ===

		[SerializeField]
		Texture2D noiseSource;

		[SerializeField]
		int seed;

		[SerializeField]
		bool centerMap = false;

		// === Camera ===

		HexMapNavigation.HexMapCamera hexMapCamera;

		// === Resources Loader ===

		Scene.IResourcesLoader resourcesLoader;

		// === Factory ===

		// direct acces to grid
		public HexMapNavigation.IHexGrid HexGrid { get; private set; }

		// === collection of link ===

		TagManager tagManager;

		// === dirty flags ===

		bool presentScreenPlayObjects = false;
		bool linkedHexInvalidDisplay = false;
		bool linkedHexInvalidDisplayAndFind = false;
		bool linkedHexRebuild = false;
		
		public bool LinkedHexInvalidDisplay
		{
			set
			{
				if (value == true)
				{
					linkedHexInvalidDisplay = value;
				}
			}
		}

		public bool LinkedHexInvalidDisplayAndFind
		{
			set
			{
				if (value == true)
				{
					linkedHexInvalidDisplayAndFind = value;
				}
			}
		}

		public bool LinkedHexRebuild
		{
			set
			{
				if (value == true)
				{
					linkedHexRebuild = value;
				}
			}
		}

		public bool PresentScreenPlayObjects
		{
			set
			{
				if (value == true)
				{
					presentScreenPlayObjects = value;
				}
			}
		}
		
		// === Screen play ===

		public GameEngine.IScreenPlay ScreenPlay { get; private set; }

		// === Constructor ===

		DiContainer diContainer;

		// DIP
		[Inject]
		public void Construct(DiContainer diContainer, GameEngine.IScreenPlay screenPlay, Scene.IResourcesLoader resourcesLoader, HexMapNavigation.HexMapCamera hexMapCamera, HexMapNavigation.IHexMapGenerator mapGenerator)
		{
			this.ScreenPlay = screenPlay;
			this.hexMapCamera = hexMapCamera;
			this.mapGenerator = mapGenerator;
			this.resourcesLoader = resourcesLoader;
			this.diContainer = diContainer;
		}

		// === Life cycle ===

		// awake
		void Awake()
		{
			HexMetrics.noiseSource = noiseSource;
			HexMetrics.InitializeHashGrid(seed);

			tagManager = new TagManager(this);
		}

		void OnEnable()
		{
			if (!HexMetrics.noiseSource)
			{
				HexMetrics.noiseSource = noiseSource;
				HexMetrics.InitializeHashGrid(seed);
			}
		}

		// update
		void Update()
		{
			if (linkedHexRebuild == true)
			{
				linkedHexRebuild = false;
				linkedHexInvalidDisplayAndFind = true;
				HexGrid.Net.ClearAllPaths();
			}

			if (linkedHexInvalidDisplay == true)
			{
				linkedHexInvalidDisplay = false;
				ShowLinkedTags(false);
			}

			if (linkedHexInvalidDisplayAndFind == true)
			{
				linkedHexInvalidDisplayAndFind = false;
				ShowLinkedTags(true);
			}

			if (presentScreenPlayObjects == true)
			{
				PresentScreenPlayObjectsFct();
				presentScreenPlayObjects = false;
			}
		}

		// instatiate grid
		void InstansiateGrid()
		{
			var hexGridFactory = new HexGridFactory(gridPrefab);
			HexGrid = hexGridFactory.Create(transform);
		}

		// populate all children
		void PopulateAllChildren(List<GameObject> children, Transform parent)
		{
			foreach (Transform child in parent)
			{
				children.Add(child.gameObject);
				PopulateAllChildren(children, child);
			}
		}

		#region INTERFACE

		// destroy maze
		void DestroyHexGrid()
		{
			// clearing tag manager
			tagManager?.Clear();
			// clearing all childs
			var children = new List<GameObject>();
			PopulateAllChildren(children, transform);
			children.ForEach(child => Destroy(child.gameObject));
		}

		// === hex grid creation ===

		IEnumerable<string> FindAllGridChildObjectPaths()
		{
			var list = new List<Transform>();
			for (int i=0;i< HexGrid.GetGridChunkCount(); ++i)
			{
				var chunk = HexGrid.GetGridChunk(i);
				if (chunk != null)
				{
					for (var it = HexMapNavigation.HexEnvironmentManagerIDs.firstIdx; it != HexMapNavigation.HexEnvironmentManagerIDs.CountContainer; it++)
					{
						list.AddRange( chunk.FindChildsOfChunkEnvironement(it) );
					}
				}
			}

			string SelectPath(Transform obj)
			{
				var persistableObject = obj.gameObject.GetComponent<RuntimeTools.PersistableObject>();
				var assetRessourcesPath = persistableObject.AssetRessourcesPath;
				return assetRessourcesPath;
			}

			var uniqueNames = list.Select(obj => SelectPath(obj)).Distinct();
			return uniqueNames;
		}

		// save/load
		public void Save(Core.GameDataWriter writer)
		{
			// features
			{
				writer.WriteObjectStart("FeaturesConfig");
				writer.Write(HexMetrics.maxPlantCountByCell, "MaxPlantCountByCell");
				writer.Write(HexMetrics.maxUrbanCountByCell, "MaxUrbanCountByCell");
				writer.Write(HexMetrics.maxFarmCountByCell, "MaxFarmCountByCell");
				writer.WriteObjectEnd();
			}
			
			// save prefabs references for all objects
			{
				var completeList = FindAllGridChildObjectPaths();
				writer.WriteObjectStart("AllPrefabRef");
				writer.Write(completeList.Count(), "Count");
				for (int i=0;i< completeList.Count();++i)
				{
					var assetRessourcesPath = completeList.ElementAt(i);
					writer.Write(assetRessourcesPath, "Prefab["+i+"]");
				}
				writer.WriteObjectEnd();
			}
					   
			// Map
			HexGrid.Save(writer);

			// tag save
			writer.WriteObjectStart("Tags");
			tagManager.Save(writer);
			writer.WriteObjectEnd();
		}

		public IEnumerator Load(Core.GameDataReader reader, int version, bool addressables, Core.Done done)
		{
			hexMapCamera.Locked = true;

			// clean map
			DestroyHexGrid();
			InstansiateGrid();

			// features
			if (version >= 19)
			{
				reader.ReadObjectStart("FeaturesConfig");
				HexMetrics.maxPlantCountByCell = reader.ReadInt();
				HexMetrics.maxUrbanCountByCell = reader.ReadInt();
				HexMetrics.maxFarmCountByCell = reader.ReadInt();
				reader.ReadObjectEnd();
			}

			// load prefabs references
			if (version >= 23)
			{
				reader.ReadObjectStart("AllPrefabRef");
				var Count = reader.ReadInt();
				for (int i = 0; i < Count; ++i)
				{
					var prefabPath = reader.ReadString();
					resourcesLoader.LoadResource(prefabPath, addressables);
				}
				reader.ReadObjectEnd();
			}


			// load map
			var hexCellTerrainDataFactory = new HexCellTerrainDataFactory();
			var hexCellFactory = new HexCellFactory(cellPrefab, hexCellTerrainDataFactory);
			var hexChunkFactory = new HexChunkFactory(diContainer, chunkPrefab);
			var hexCellLabelFactory = new HexCellLabelFactory(cellLabelPrefab);
			yield return resourcesLoader.WaitAll();
			HexGrid.Load(hexCellFactory, hexChunkFactory, hexCellLabelFactory, reader, version, centerMap);

			// load tag
			reader.ReadObjectStart("Tags");
			tagManager.Load(reader, version);
			reader.ReadObjectEnd();
			
			hexMapCamera.Locked = false;
			done(true);

			yield return null;
		}

		public bool CreateMap(int x, int z, int chunkSizeX, int chunkSizeZ, bool wrapping, bool emptyMaps)
		{
			bool b = false;
			hexMapCamera.Locked = true;
			DestroyHexGrid();
			InstansiateGrid();
			var hexCellTerrainDataFactory = emptyMaps == false ? new HexCellTerrainDataFactory() : null;
			var hexCellFactory = new HexCellFactory(cellPrefab, hexCellTerrainDataFactory);
			var hexChunkFactory = new HexChunkFactory(diContainer, chunkPrefab);
			var hexCellLabelFactory = new HexCellLabelFactory(cellLabelPrefab);
			b = HexGrid.CreateMap(hexCellFactory, hexChunkFactory, hexCellLabelFactory, x, z, chunkSizeX, chunkSizeZ, wrapping, centerMap);
			hexMapCamera.Locked = false;
			return b;
		}

		public bool GenerateMap(int x, int z, int chunkSizeX, int chunkSizeZ, bool wrapping)
		{
			bool b = false;
			hexMapCamera.Locked = true;
			DestroyHexGrid();
			InstansiateGrid();
			b = mapGenerator.GenerateMap(x, z, chunkSizeX, chunkSizeZ, wrapping);
			hexMapCamera.Locked = false;
			return b;
		}

		public void CloseMap()
		{
			DestroyHexGrid();
		}

		// === cells ===

		public HexMapNavigation.IHexCellOrigin GetCellUnderCursor()
		{
			// make a ray from mouse mouse position screen point 
			return HexGrid.GetCell(Camera.main.ScreenPointToRay(Input.mousePosition));
		}
		
		// === linked cells ===

		public Data.TagPair SelectHexCellLink(Data.TagPair tagPair)
		{
			if (tagPair != null)
			{
				HexMapNavigation.HexCellLinked cellLinked = tagManager.Find(tagPair);
				if (cellLinked != null)
				{
					if (cellLinked.ContainsLink(tagPair.Second) == true)
					{
						tagManager.Selected = tagPair;
					}
					else
					{
						tagManager.Selected = null;
					}
					linkedHexInvalidDisplay = true;
				}
			}
			else
			{
				tagManager.Selected = null;
			}
			return tagManager.Selected;
		}

		public Data.TagPair SelectedHexCellLink
		{
			get
			{
				return tagManager.Selected;
			}
			set
			{
				if (value != null)
				{
					if (value.Equals(tagManager.Selected) == false)
					{
						SelectHexCellLink(value);
					}
				}
				else
				{
					tagManager.Selected = null;
				}
			}
		}

		public HexMapNavigation.IHexCellLinkProperties FindHexCellLinkProperties(Data.TagPair tagPair)
		{
			HexMapNavigation.IHexCellLinkProperties linkProp = null;
			HexMapNavigation.HexCellLinked cellLinked = tagManager.Find(tagPair.First);
			if (cellLinked != null)
			{
				linkProp = cellLinked.FindLinkProperties(tagPair.Second);
			}
			return linkProp;
		}

		public TagManager.Event HexCellLinkOnSelectingEvent
		{
			get
			{
				return tagManager.OnSelecting;
			}
		}

		// === Linked cell properties ===

		public HexMapNavigation.IHexCellLinkPropertiesFactory HexCellLinkPropertiesFactoryImpl
		{
			get
			{
				return new HexCellLinkPropertiesFactory();
			}
		}

		// === Tag management ===

		public void SetCellTag(HexMapNavigation.IHexCellOrigin cell, Data.Tag tag)
		{
			if (cell.TagData.Tag != null)
			{
				if (cell.TagData.Tag.Equals(tag) == false)
				{
					if (tag == null)
					{
						tagManager.Remove(cell.TagData.Tag);
					}
					else
					{
						tagManager.Replace(cell.TagData.Tag, tag);
					}
				}
			}
			else
			{
				tagManager.Set(new HexMapNavigation.HexCellLinked(this, cell, tag));
			}
		}

		/*
		 * According to tag (ex #welcome)
		 * put GameObjects from ScreenPlay hierachy to hewGrid Map
		 * 
		 */
		void PresentScreenPlayObjectsFct()
		{
			List<GameEngine.PresentObjectStruct> presentObjectArray = new List<GameEngine.PresentObjectStruct>(this.tagManager.Items.Count);
			for (var i = 0; i < this.tagManager.Items.Count; ++i)
			{
				var refTag = this.tagManager.Items[i];
				HexMapNavigation.IHexCellOrigin cell = HexGrid.FindCellFromTag(refTag.Tag, true);
				if (cell != null)
				{
					presentObjectArray.Add(new GameEngine.PresentObjectStruct(cell.Position, refTag.Tag));

				}
				else
				{
					Debug.LogWarning("Cell not found from tag " + refTag.Tag.ToString());
				}
			}
			ScreenPlay.PresentObjects(presentObjectArray);
		}
		
		// === Linked cells ===

		public void ShowLinkedTags(bool invalidate)
		{
			HexMapNavigation.IHexCellOrigin selectedFrom = null;
			HexMapNavigation.IHexCellOrigin selectedTo = null;
			HexMapNavigation.IHexCellLinkProperties selectedLinkProperties = null;

			// parse all tags
			foreach (var it in this.tagManager.Items)
			{
				// no parent => not a path
				for (var i = 0; i < it.LinkedTagsCount(); ++i)
				{
					var linkedTag = it.LinkedTagProperties(i);
					if (linkedTag != null)
					{
						// cells from tags
						HexMapNavigation.IHexCellOrigin From = HexGrid.FindCellFromTag(it.Tag, invalidate);
						HexMapNavigation.IHexCellOrigin To = HexGrid.FindCellFromTag(linkedTag.Tag, invalidate);
						if (To != null && From != null)
						{
							Data.TagPair tagPair = Data.TagPair.MakePair(From.TagData.Tag, To.TagData.Tag);
							// selected or not
							bool selected = false;
							if (this.tagManager.Selected != null)
								selected = this.tagManager.Selected.Equals(tagPair) ? true : false;
							if (selected == false)
							{
								// find and draw
								HexGrid.Net.FindPath(tagPair, From, To, null, linkedTag, selected, invalidate);
							}
							else
							{
								// we save selected tags
								selectedFrom = From;
								selectedTo = To;
								selectedLinkProperties = linkedTag;
							}
						}
						else
						{
							Data.TagPair tagPair = Data.TagPair.MakePair(it.Tag, linkedTag.Tag);
							HexGrid.Net.ClearPath(tagPair);
							Debug.LogWarning("ShowLinkedTags() : inconsitent tag <" + it.Tag.TagName + "> from tag manager");
						}
					}
				}
			}

			// the selected path is draw in last
			if (selectedTo != null)
			{
				HexGrid.Net.FindPath(Data.TagPair.MakePair(selectedFrom.TagData.Tag, selectedTo.TagData.Tag), selectedFrom, selectedTo, null, selectedLinkProperties, true, invalidate);
			}
		}

		public Data.TagPair ToggleSelectHexCellLink(Data.TagPair tagPair)
		{
			HexMapNavigation.HexCellLinked cellLinked = tagManager.Find(tagPair);
			if (cellLinked != null)
			{
				if (tagPair.Equals(tagManager.Selected) == true)
				{
					tagManager.Selected = null;
				}
				else
				{
					if (cellLinked.ContainsLink(tagPair.Second) == true)
					{
						tagManager.Selected = tagPair;
					}
					else
					{
						tagManager.Selected = null;
					}
				}
				linkedHexInvalidDisplay = true;
			}
			return tagManager.Selected;
		}

		public void RemoveSelectedLink()
		{
			HexMapNavigation.HexCellLinked cellLinked = tagManager.Find(tagManager.Selected);
			if (cellLinked != null)
			{
				tagManager.RemoveLink(tagManager.Selected);
				tagManager.Selected = null;
			}
		}

		public bool LinkHexCell(HexMapNavigation.IHexCellOrigin from, HexMapNavigation.IHexCellOrigin to)
		{
			bool done = false;
			if (from != null && to != null)
			{
				if (to.TagData.Tag != null)
				{
					if (from.TagData.Tag != null)
					{
						// we create a new link between 2 nodes
						if (from.TagData.Tag.Equals(to.TagData.Tag) == false)
						{
							HexMapNavigation.HexCellLinked tagMetaDataFrom = tagManager.Find(from.TagData.Tag);
							if (tagMetaDataFrom != null)
							{
								tagMetaDataFrom.LinkedTagAdd(to.TagData.Tag);
								done = true;
							}
						}
						else
						{
							Debug.LogWarningFormat("{0} : can't link a tag #{1} from itself !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, from.TagData.Tag.TagName);
						}
					}
					else
					{
						// we move an existing node to another place
						tagManager.Set(new HexMapNavigation.HexCellLinked(this, from, to.TagData.Tag));
					}
				}
			}
			return done;
		}

		public void UnselectHexCellLink(Data.TagPair tagPair)
		{
			HexMapNavigation.HexCellLinked cellLinked = tagManager.Find(tagPair);
			if (cellLinked != null)
			{
				if (tagPair.Equals(tagManager.Selected) == true)
				{
					tagManager.Selected = null;
					linkedHexInvalidDisplay = true;
				}
			}
		}

		HexMapNavigation.HexGridPathDetails GetPathDetails(HexMapNavigation.IHexGridUnit unit, Data.TagPair tagPair)
		{
			var path = HexGrid.Net.GetPathDetails(tagPair);
			if (path == null)
			{
				HexGrid.Net.FindPath(this, SelectedHexCellLink, unit, FindHexCellLinkProperties(tagPair), false, false);
				path = HexGrid.Net.GetPathDetails(tagPair);
			}
			return path;
		}
					 
		// === unit management ===

		public bool Spawn(Data.Tag tag, GameObject player)
		{
			bool b = false;
			HexMapNavigation.IHexCellOrigin hexCell = HexGrid.FindCellFromTag(tag, false);
			HexMapNavigation.HexGridUnit hexUnit = player.GetComponent<HexMapNavigation.HexGridUnit>();
			if (hexCell != null && hexUnit != null)
			{
				hexUnit.transform.position = hexCell.Position;
				(hexCell as IHexCell).Chunk.MakeChildOfChunkEnvironement(HexMapNavigation.HexEnvironmentManagerIDs.CharactersContainerIdx, hexUnit.transform);
				b = true;
			}

			if (b == true)
			{
				// links all tags
				ShowLinkedTags(false);
			}

			if (b == true)
			{
				// fix camera game location
				var camGame = global::GameCreator.Core.Hooks.HookCamera.Instance;
				camGame.gameObject.transform.position = hexUnit.UnitTransform.position + Vector3.up * 30.0f + hexUnit.UnitTransform.forward * 30.0f;
				camGame.gameObject.transform.LookAt(hexUnit.UnitTransform);

				// fix camera editor location
				var camEditor = global::GameCreator.Core.Hooks.HookHexMapCamera.Instance;
				camEditor.gameObject.transform.position = new Vector3(hexUnit.UnitTransform.position.x, 0.0f, hexUnit.UnitTransform.position.z);
			}

			return b;
		}

		public GameEngine.ICharacterMoving UnitMoveOnPathBegin(HexMapNavigation.IHexGridUnit unit)
		{
			GameEngine.ICharacterMoving ret = null;
			if (SelectedHexCellLink != null)
			{
				var path = GetPathDetails(unit, SelectedHexCellLink);
				ret = unit.MoveOnPathBegin(path);
			}
			else
			{
				Debug.LogWarningFormat("{0} : no path selected !!! ", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			return ret;
		}

		public void UnitSelectNextPath(HexMapNavigation.IHexGridUnit unit, Data.TagPair tagPair)
		{
			var path = GetPathDetails(unit, tagPair);
			if (path != null)
				unit.NextPathToTravel = path;
		}

		public void UnitSelectPath(HexMapNavigation.IHexGridUnit unit, Data.TagPair tagPair)
		{
			var path = GetPathDetails(unit, tagPair);
			if (path != null)
				unit.PathToTravel = path;
		}

		#endregion
	}
}