﻿using UnityEngine;
using UnityEngine.UI;

namespace codingVR.HexMapTerrain
{
	class HexCellTerrainData : IHexCellTerrainData
	{
		IHexCell cell;

		static HexCellTerrainData hexCellTerrainData = null;

		/*
		 * 
		 * Constructor
		 * 
		 */

		static public HexCellTerrainData MakeDefaultHexCellTerrainData(HexCell cell)
		{
			// create default instance
			if (hexCellTerrainData == null)
			{
				hexCellTerrainData = new HexCellTerrainData(null);
			}

			// assign current cell
			hexCellTerrainData.cell = cell;
			
			// elevation
			float elevation = 0.0f;
			float countElevation = 0.0f;
			foreach (var neighbor in cell.Neighbors)
			{
				var terrainData = (neighbor as IHexCell)?.TerrainData;
				if (terrainData != null)
				{
					elevation += terrainData.Elevation;
					countElevation += 1.0f;
				}
			}
			elevation /= countElevation;
			hexCellTerrainData.elevation = (int)elevation;

			return hexCellTerrainData;
		}

		public HexCellTerrainData(IHexCell cell)
		{
			this.cell = cell;
			elevation = 0;
		}

		/*
		 * 
		 * UI
		 * 
		 */

		public string RefreshCanvasTextLabel(Text label, string textLabel)
		{
			string text = null;
			Color cellColor = Color.black;

			// set text
			if (textLabel != null)
				text = textLabel;

			// explorable
			if (cell.Explorable == false)
			{
				if (text == null)
				{
					text = "\u2715";
				}
				cellColor = Color.red;
			}
			else
			{
				if (text != null && text == "\u2715")
				{
					text = null;
				}
			}

			// manage cell label
			label.color = cellColor;
			return text;
		}
		
		/*
		 * 
		 * Load & Save
		 * 
		 */

		public void Save(Core.GameDataWriter writer)
		{
			// Environment
			writer.WriteObjectStart("Cell-Terrain");
			writer.Write((byte)terrainTypeIndex, "terrainTypeIndex");
			writer.Write(elevation, "elevation");
			writer.Write(elevationPerturbStrength, "elevationPerturbStrength");
			writer.Write(elevationStep, "elevationStep");
			writer.Write((byte)waterLevel, "waterLevel");
			writer.Write((byte)urbanLevel, "urbanLevel");
			writer.Write((byte)farmLevel, "farmLevel");
			writer.Write((byte)plantLevel, "plantLevel");
			writer.Write((byte)specialIndex, "specialIndex");
			writer.Write(walled, "walled");

			if (hasIncomingRiver)
			{
				writer.Write((byte)(incomingRiver + 128), "incomingRiver");
			}
			else
			{
				writer.Write((byte)0, "incomingRiver");
			}

			if (hasOutgoingRiver)
			{
				writer.Write((byte)(outgoingRiver + 128), "outgoingRiver");
			}
			else
			{
				writer.Write((byte)0, "outgoingRiver");
			}

			int roadFlags = 0;
			for (int i = 0; i < roads.Length; i++)
			{
				if (roads[i])
				{
					roadFlags |= 1 << i;
				}
			}
			writer.Write((byte)roadFlags, "roadFlags");
			writer.WriteObjectEnd();
		}

		public void Load(Core.GameDataReader reader, int version)
		{
			// Environment & Terrain
			if (version >= 20)
				reader.ReadObjectStart("Cell-Terrain");
			else
				reader.ReadObjectStart("Cell-Environment");
			terrainTypeIndex = reader.ReadByte();
			ShaderData.RefreshTerrain(cell, terrainTypeIndex);
			if (version >= 21)
			{
				elevation = reader.ReadFloat();
			}
			else
			{
				int elevationInt = reader.ReadByte();
				elevationInt -= 127;
				elevation = (float)elevationInt;
			}

			if (version >= 2)
				elevationPerturbStrength = reader.ReadFloat();
			if (version >= 3)
				elevationStep = reader.ReadFloat();
			
			RefreshPosition();
			MovingCell = Vector3.zero;
			waterLevel = reader.ReadByte();
			urbanLevel = reader.ReadByte();
			farmLevel = reader.ReadByte();
			plantLevel = reader.ReadByte();
			specialIndex = reader.ReadByte();
			walled = reader.ReadBoolean();
			if (version >= 9 && version <= 17)
			{
				cell.Explorable = reader.ReadBoolean();
			}

			byte riverData = reader.ReadByte();
			if (riverData >= 128)
			{
				hasIncomingRiver = true;
				incomingRiver = (HexMapNavigation.HexDirection)(riverData - 128);
			}
			else
			{
				hasIncomingRiver = false;
			}

			riverData = reader.ReadByte();
			if (riverData >= 128)
			{
				hasOutgoingRiver = true;
				outgoingRiver = (HexMapNavigation.HexDirection)(riverData - 128);
			}
			else
			{
				hasOutgoingRiver = false;
			}

			int roadFlags = reader.ReadByte();
			for (int i = 0; i < roads.Length; i++)
			{
				roads[i] = (roadFlags & (1 << i)) != 0;
			}

			reader.ReadObjectEnd();

			// States
			if (version < 10)
			{
				reader.ReadObjectStart("Cell-States");
				reader.ReadBoolean(); // pass through
				reader.ReadObjectEnd();
			}
		}

		/*
		 * 
		 * Refreshing
		 * 
		 */

		public Vector3 MovingCell { get; set; }

		public void RefreshPosition()
		{
			// compute new position
			Vector3 position = cell.ComputeNextLocalPosition(elevation, elevationStep, elevationPerturbStrength);

			// cell is moving
			MovingCell += position - cell.LocalPosition;

			// update cell position
			cell.UpdateNewPosition(position);
		}

		/*
		 * 
		 * Refreshing
		 * 
		 */

		// refresh position

		public void ValidateAndRefresh()
		{
			RefreshPosition();
			ValidateRivers();
			ValidateRoads();
			cell.Refresh();
		}


		/* 
		 * 
		 * Elevation & water
		 * 
		 */

		// elevation
		float elevation = float.MinValue;

		public float Elevation
		{
			get
			{
				return elevation;
			}
			set
			{
				if (elevation == value)
				{
					return;
				}
				var originalViewElevation = ViewElevation;
				elevation = value;
				if (ViewElevation != originalViewElevation)
				{
					ShaderData?.ViewElevationChanged();
				}
				ValidateAndRefresh();
			}
		}

		float elevationPerturbStrength = 0.0f;
		
		// elevation Perturb Strength
		public float ElevationPerturbStrength
		{
			get
			{
				return elevationPerturbStrength;
			}
			set
			{
				if (elevationPerturbStrength == value)
				{
					return;
				}
				elevationPerturbStrength = value;
				ValidateAndRefresh();
			}
		}

		float elevationStep = 3.0f;

		// elevation Perturb Strength
		public float ElevationStep
		{
			get
			{
				return elevationStep;
			}
			set
			{
				if (elevationStep == value)
				{
					return;
				}
				elevationStep = value;
				ValidateAndRefresh();
			}
		}

		// water
		float waterLevel;

		public float WaterLevel
		{
			get
			{
				return waterLevel;
			}
			set
			{
				if (waterLevel == value)
				{
					return;
				}
				var originalViewElevation = ViewElevation;
				waterLevel = value;
				if (ViewElevation != originalViewElevation)
				{
					ShaderData?.ViewElevationChanged();
				}
				ValidateRivers();
				cell.Refresh();
			}
		}

		public float ViewElevation
		{
			get
			{
				return elevation >= waterLevel ? elevation : waterLevel;
			}
		}

		public bool IsUnderwater
		{
			get
			{
				return waterLevel > elevation;
			}
		}

		/* 
		 * 
		 * Features
		 * 
		 */

		// river 
		public bool HasIncomingRiver
		{
			get
			{
				return hasIncomingRiver;
			}
			set
			{
				hasIncomingRiver = value;
			}
		}

		public bool HasOutgoingRiver
		{
			get
			{
				return hasOutgoingRiver;
			}
			set
			{
				hasOutgoingRiver = value;
			}
		}

		public bool HasRiver
		{
			get
			{
				return hasIncomingRiver || hasOutgoingRiver;
			}
		}

		public bool HasRiverBeginOrEnd
		{
			get
			{
				return hasIncomingRiver != hasOutgoingRiver;
			}
		}

		public HexMapNavigation.HexDirection RiverBeginOrEndDirection
		{
			get
			{
				return hasIncomingRiver ? incomingRiver : outgoingRiver;
			}
		}

		// roads 
		public bool HasRoads
		{
			get
			{
				for (int i = 0; i < roads.Length; i++)
				{
					if (roads[i])
					{
						return true;
					}
				}
				return false;
			}
		}

		// rivers
		public HexMapNavigation.HexDirection IncomingRiver
		{
			get
			{
				return incomingRiver;
			}
			set
			{
				incomingRiver = value;
			}
		}

		public HexMapNavigation.HexDirection OutgoingRiver
		{
			get
			{
				return outgoingRiver;
			}
		}

		// features
		public float StreamBedY
		{
			get
			{
				return (elevation + HexMetrics.streamBedElevationOffset) * elevationStep * HexMapNavigation.HexMetrics.scale;
			}
		}

		public float RiverSurfaceY
		{
			get
			{
				return (elevation + HexMetrics.waterElevationOffset) * elevationStep * HexMapNavigation.HexMetrics.scale;
			}
		}

		public float WaterSurfaceY
		{
			get
			{
				return (waterLevel + HexMetrics.waterElevationOffset) * elevationStep * HexMapNavigation.HexMetrics.scale;
			}
		}

		// features
		int urbanLevel, farmLevel, plantLevel;
		public int UrbanLevel
		{
			get
			{
				return urbanLevel;
			}
			set
			{
				if (urbanLevel != value)
				{
					urbanLevel = value;
					cell.RefreshChunkContainer();
				}
			}
		}

		public int FarmLevel
		{
			get
			{
				return farmLevel;
			}
			set
			{
				if (farmLevel != value)
				{
					farmLevel = value;
					cell.RefreshChunkContainer();
				}
			}
		}

		public int PlantLevel
		{
			get
			{
				return plantLevel;
			}
			set
			{
				if (plantLevel != value)
				{
					plantLevel = value;
					cell.RefreshChunkContainer();
				}
			}
		}

		int specialIndex;

		public int SpecialIndex
		{
			get
			{
				return specialIndex;
			}
			set
			{
				if (specialIndex != value && !HasRiver)
				{
					specialIndex = value;
					RemoveRoads();
					cell.RefreshChunkContainer();
				}
			}
		}

		public bool IsSpecial
		{
			get
			{
				return specialIndex > 0;
			}
		}

		// walls
		bool walled;

		public bool Walled
		{
			get
			{
				return walled;
			}
			set
			{
				if (walled != value)
				{
					walled = value;
					cell.Refresh();
				}
			}
		}

		// terrain type
		int terrainTypeIndex;

		public int TerrainTypeIndex
		{
			get
			{
				return terrainTypeIndex;
			}
			set
			{
				if (terrainTypeIndex != value)
				{
					terrainTypeIndex = value;
					ShaderData?.RefreshTerrain(cell, terrainTypeIndex);
				}
			}
		}

		/* 
		 * 
		 * Tag
		 * 
		 */

		public Data.Tag Tag
		{
			get
			{
				return tag;
			}
			set
			{
				if (tag != value)
				{
					tag = value;
					cell.RefreshCanvasTextLabel();
				}
			}
		}

		private Data.Tag tag;

		/*
		 * 
		 * Shader
		 * 
		 */

		public HexMapNavigation.HexCellShaderData ShaderData {  get; set; }

		/*
		 * 
		 * Features advanced
		 * 
		 */

		bool hasIncomingRiver, hasOutgoingRiver;
		HexMapNavigation.HexDirection incomingRiver, outgoingRiver;

		public bool HasRiverThroughEdge(HexMapNavigation.HexDirection direction)
		{
			return
				hasIncomingRiver && incomingRiver == direction ||
				hasOutgoingRiver && outgoingRiver == direction;
		}

		public void RemoveIncomingRiver()
		{
			if (!hasIncomingRiver)
			{
				return;
			}
			hasIncomingRiver = false;
			cell.RefreshChunkContainer();

			IHexCell neighbor = cell.GetNeighbor(incomingRiver) as IHexCell;
			neighbor.TerrainData.HasOutgoingRiver = false;
			neighbor.RefreshChunkContainer();
		}

		public void RemoveOutgoingRiver()
		{
			if (!hasOutgoingRiver)
			{
				return;
			}
			hasOutgoingRiver = false;
			cell.RefreshChunkContainer();

			IHexCell neighbor = cell.GetNeighbor(outgoingRiver) as IHexCell;
			neighbor.TerrainData.HasIncomingRiver = false;
			neighbor.RefreshChunkContainer();
		}

		public void RemoveRiver()
		{
			RemoveOutgoingRiver();
			RemoveIncomingRiver();
		}

		public void SetOutgoingRiver(HexMapNavigation.HexDirection direction)
		{
			if (hasOutgoingRiver && outgoingRiver == direction)
			{
				return;
			}

			IHexCell neighbor = cell.GetNeighbor(direction) as IHexCell;
			if (!IsValidRiverDestination(neighbor))
			{
				return;
			}

			RemoveOutgoingRiver();
			if (hasIncomingRiver && incomingRiver == direction)
			{
				RemoveIncomingRiver();
			}
			hasOutgoingRiver = true;
			outgoingRiver = direction;
			specialIndex = 0;

			neighbor.TerrainData.RemoveIncomingRiver();
			neighbor.TerrainData.HasIncomingRiver = true;
			neighbor.TerrainData.IncomingRiver = direction.Opposite();
			neighbor.TerrainData.SpecialIndex = 0;

			SetRoad((int)direction, false);
		}

		// roads
		bool[] roads = new bool[6];

		public bool HasRoadThroughEdge(HexMapNavigation.HexDirection direction)
		{
			return roads[(int)direction];
		}

		public void AddRoad(HexMapNavigation.HexDirection direction)
		{
			var cellNeighbor = cell.GetNeighbor(direction) as IHexCell;
			if (!roads[(int)direction] && !HasRiverThroughEdge(direction) && !IsSpecial && !cellNeighbor.TerrainData.IsSpecial && GetElevationDifference(direction) <= 1)
			{
				SetRoad((int)direction, true);
			}
		}

		public void RemoveRoads()
		{
			for (int i = 0; i < cell.NeighborCount; i++)
			{
				if (roads[i])
				{
					SetRoad(i, false);
				}
			}
		}

		public void ValidateRoads()
		{
			for (int i = 0; i < roads.Length; i++)
			{
				if (roads[i] && GetElevationDifference((HexMapNavigation.HexDirection)i) > 1)
				{
					SetRoad(i, false);
				}
			}
		}

		void SetRoad(int index, bool state)
		{
			roads[index] = state;
			HexMapNavigation.HexDirection direction = (HexMapNavigation.HexDirection)index;
			var cellNeighbor = cell.GetNeighbor(direction) as IHexCell;
			cellNeighbor.TerrainData.SetRoadState((int)direction.Opposite(), state);
			cellNeighbor.RefreshChunkContainer();
			cell.RefreshChunkContainer();
		}

		public void SetRoadState(int index, bool state)
		{
			roads[index] = state;
		}

		// others
		public float GetElevationDifference(HexMapNavigation.HexDirection direction)
		{
			var cellNeighbor = cell.GetNeighbor(direction) as IHexCell;
			var difference = Elevation - cellNeighbor.TerrainData.Elevation;
			return difference >= 0 ? difference : -difference;
		}

		public bool IsValidRiverDestination(IHexCell neighbor)
		{
			return neighbor != null && (
				elevation >= neighbor.TerrainData.Elevation || waterLevel == neighbor.TerrainData.Elevation
			);
		}

		public void ValidateRivers()
		{
			if (
				hasOutgoingRiver &&
				!IsValidRiverDestination(cell.GetNeighbor(outgoingRiver) as IHexCell)
			)
			{
				RemoveOutgoingRiver();
			}
			if (
				hasIncomingRiver &&
				!(cell.GetNeighbor(incomingRiver) as IHexCell).TerrainData.IsValidRiverDestination(cell)
			)
			{
				RemoveIncomingRiver();
			}
		}

		/*
		 * 
		 * Geometry
		 * 
		 */

		public HexEdgeType GetEdgeType(HexMapNavigation.HexDirection direction)
		{
			var cellNeighbor = cell.GetNeighbor(direction) as IHexCell;
			return HexMetrics.GetEdgeType(Elevation, cellNeighbor.TerrainOrDefaultData().Elevation);
		}

		public HexEdgeType GetEdgeType(IHexCell otherCell)
		{
			return HexMetrics.GetEdgeType(Elevation, otherCell.TerrainOrDefaultData().Elevation);
		}
	}
}