﻿namespace codingVR.HexMapTerrain
{
    public enum HexEdgeType
    {
        Flat, Slope, Cliff
    }
}