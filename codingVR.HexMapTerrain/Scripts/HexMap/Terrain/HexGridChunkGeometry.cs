﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace codingVR.HexMapTerrain
{
	public class HexGridChunkGeometry : MonoBehaviour, IHexGridChunkGeometry
	{
		// all meshes
		[SerializeField]
		HexMesh terrain, rivers, roads, water, waterShore, estuaries;

		/*
		 * 
		 * weights
		 * 
		 */

		static Color weights1 = new Color(1f, 0f, 0f);
		static Color weights2 = new Color(0f, 1f, 0f);
		static Color weights3 = new Color(0f, 0f, 1f);

		/*
		 *
		 * life cycle
		 * 
		 */

		void Awake()
		{
		}

		void LateUpdate()
		{
		}


		/*
		 * 
		 * test if position is conatined by this chunk at geometry level
		 * 
		 */

		public bool GeometryContains(Vector3 position)
		{
			HexMesh hexMesh = gameObject.GetComponentInChildren<HexMesh>();
			Bounds bounds = hexMesh.GetComponent<MeshFilter>().sharedMesh.bounds;
			Bounds bounds2d = new Bounds(new Vector3(bounds.center.x, 0.0f, bounds.center.z), new Vector3(bounds.size.x, 1.0f, bounds.size.z));
			Vector3 position2d = new Vector3(position.x, 0.5f, position.z);
			bool contains = false;
			if (bounds2d.Contains(position2d) == true)
			{
				contains = terrain.GeometryContains(position);
			}
			return contains;
		}

		/*
		 * 
		 * tessellate
		 * 
		 */

		public void Triangulate(IHexEnvironmentManager environment, IHexCell[] cells)
		{
			terrain.Clear();
			rivers.Clear();
			roads.Clear();
			water.Clear();
			waterShore.Clear();
			estuaries.Clear();
			for (int i = 0; i < cells.Length; i++)
			{
				if (cells[i].TerrainData != null)
				{
					Triangulate(environment, cells[i]);
				}
			}
			terrain.Apply();
			rivers.Apply();
			roads.Apply();
			water.Apply();
			waterShore.Apply();
			estuaries.Apply();
		}

		void Triangulate(IHexEnvironmentManager environment, IHexCell cell)
		{
			for (HexMapNavigation.HexDirection d = HexMapNavigation.HexDirection.NE; d <= HexMapNavigation.HexDirection.NW; d++)
			{
				Triangulate(environment, d, cell);
			}
			if (!cell.TerrainData.IsUnderwater)
			{
				if (!cell.TerrainData.HasRiver && !cell.TerrainData.HasRoads)
				{
					environment.Features?.AddFeature(cell, cell.LocalPosition);
				}
				if (cell.TerrainData.IsSpecial)
				{
					environment.Features?.AddSpecialFeature(cell, cell.LocalPosition);
				}
			}
		}

		// we triangulate one direction
		void Triangulate(IHexEnvironmentManager environment, HexMapNavigation.HexDirection direction, IHexCell cell)
		{
			// create 4 vertex along the edge between direction & direction+1
			Vector3 center = cell.LocalPosition;
			EdgeVertices5 e = new EdgeVertices5( center + HexMetrics.GetFirstSolidCorner(direction),  center + HexMetrics.GetSecondSolidCorner(direction) );

			if (cell.TerrainData.HasRiver)
			{
				if (cell.TerrainData.HasRiverThroughEdge(direction))
				{
					e.v3.y = cell.TerrainData.StreamBedY;
					if (cell.TerrainData.HasRiverBeginOrEnd)
					{
						TriangulateWithRiverBeginOrEnd(direction, cell, center, e);
					}
					else
					{
						TriangulateWithRiver(direction, cell, center, e);
					}
				}
				else
				{
					TriangulateAdjacentToRiver(environment, direction, cell, center, e);
				}
			}
			else
			{
				TriangulateWithoutRiver(direction, cell, center, e);

				if (!cell.TerrainData.IsUnderwater && !cell.TerrainData.HasRoadThroughEdge(direction))
				{
					environment.Features?.AddFeature(cell, (center + e.v1 + e.v5) * (1f / 3f));
				}
			}

			// each cell is reponsible to manage connection for the first directions
			if (direction <= HexMapNavigation.HexDirection.SE)
			{
				TriangulateConnection(environment, direction, cell, e);
			}

			if (cell.TerrainData.IsUnderwater)
			{
				TriangulateWater(direction, cell, center);
			}
		}

		void TriangulateWater(HexMapNavigation.HexDirection direction, IHexCell cell, Vector3 center)
		{
			center.y = cell.TerrainData.WaterSurfaceY;

			IHexCell neighbor = cell.GetNeighbor(direction) as IHexCell;
			if (neighbor != null && !neighbor.TerrainOrDefaultData().IsUnderwater)
			{
				TriangulateWaterShore(direction, cell, neighbor, center);
			}
			else
			{
				TriangulateOpenWater(direction, cell, neighbor, center);
			}
		}

		void TriangulateOpenWater(HexMapNavigation.HexDirection direction, IHexCell cell, IHexCell neighbor, Vector3 center)
		{
			Vector3 c1 = center + HexMetrics.GetFirstWaterCorner(direction);
			Vector3 c2 = center + HexMetrics.GetSecondWaterCorner(direction);

			water.AddTriangle(center, c1, c2);
			Vector3 indices;
			indices.x = indices.y = indices.z = cell.Index;
			water.AddTriangleCellData(indices, weights1);

			if (direction <= HexMapNavigation.HexDirection.SE && neighbor != null)
			{
				Vector3 bridge = HexMetrics.GetWaterBridge(direction);
				Vector3 e1 = c1 + bridge;
				Vector3 e2 = c2 + bridge;

				water.AddQuad(c1, c2, e1, e2);
				indices.y = neighbor.Index;
				water.AddQuadCellData(indices, weights1, weights2);

				if (direction <= HexMapNavigation.HexDirection.E)
				{
					IHexCell nextNeighbor = cell.GetNeighbor(direction.Next()) as IHexCell;
					if (nextNeighbor == null || !nextNeighbor.TerrainOrDefaultData().IsUnderwater)
					{
						return;
					}
					water.AddTriangle(
						c2, e2, c2 + HexMetrics.GetWaterBridge(direction.Next())
					);
					indices.z = nextNeighbor.Index;
					water.AddTriangleCellData(
						indices, weights1, weights2, weights3
					);
				}
			}
		}

		void TriangulateWaterShore(HexMapNavigation.HexDirection direction, IHexCell cell, IHexCell neighbor, Vector3 center)
		{
			EdgeVertices5 e1 = new EdgeVertices5(
				center + HexMetrics.GetFirstWaterCorner(direction),
				center + HexMetrics.GetSecondWaterCorner(direction)
			);
			water.AddTriangle(center, e1.v1, e1.v2);
			water.AddTriangle(center, e1.v2, e1.v3);
			water.AddTriangle(center, e1.v3, e1.v4);
			water.AddTriangle(center, e1.v4, e1.v5);
			Vector3 indices;
			indices.x = indices.z = cell.Index;
			indices.y = neighbor.Index;
			water.AddTriangleCellData(indices, weights1);
			water.AddTriangleCellData(indices, weights1);
			water.AddTriangleCellData(indices, weights1);
			water.AddTriangleCellData(indices, weights1);

			Vector3 center2 = neighbor.LocalPosition;
			if (neighbor.ColumnIndex < cell.ColumnIndex - 1)
			{
				center2.x += HexMapNavigation.HexMetrics.wrapSize * HexMapNavigation.HexMetrics.innerDiameter;
			}
			else if (neighbor.ColumnIndex > cell.ColumnIndex + 1)
			{
				center2.x -= HexMapNavigation.HexMetrics.wrapSize * HexMapNavigation.HexMetrics.innerDiameter;
			}
			center2.y = center.y;
			EdgeVertices5 e2 = new EdgeVertices5(
				center2 + HexMetrics.GetSecondSolidCorner(direction.Opposite()),
				center2 + HexMetrics.GetFirstSolidCorner(direction.Opposite())
			);

			if (cell.TerrainData.HasRiverThroughEdge(direction))
			{
				TriangulateEstuary(
					e1, e2,
					cell.TerrainData.HasIncomingRiver && cell.TerrainData.IncomingRiver == direction, indices
				);
			}
			else
			{
				waterShore.AddQuad(e1.v1, e1.v2, e2.v1, e2.v2);
				waterShore.AddQuad(e1.v2, e1.v3, e2.v2, e2.v3);
				waterShore.AddQuad(e1.v3, e1.v4, e2.v3, e2.v4);
				waterShore.AddQuad(e1.v4, e1.v5, e2.v4, e2.v5);
				waterShore.AddQuadUV(0f, 0f, 0f, 1f);
				waterShore.AddQuadUV(0f, 0f, 0f, 1f);
				waterShore.AddQuadUV(0f, 0f, 0f, 1f);
				waterShore.AddQuadUV(0f, 0f, 0f, 1f);
				waterShore.AddQuadCellData(indices, weights1, weights2);
				waterShore.AddQuadCellData(indices, weights1, weights2);
				waterShore.AddQuadCellData(indices, weights1, weights2);
				waterShore.AddQuadCellData(indices, weights1, weights2);
			}

			IHexCell nextNeighbor = cell.GetNeighbor(direction.Next()) as IHexCell;
			if (nextNeighbor != null)
			{
				Vector3 center3 = nextNeighbor.LocalPosition;
				if (nextNeighbor.ColumnIndex < cell.ColumnIndex - 1)
				{
					center3.x += HexMapNavigation.HexMetrics.wrapSize * HexMapNavigation.HexMetrics.innerDiameter;
				}
				else if (nextNeighbor.ColumnIndex > cell.ColumnIndex + 1)
				{
					center3.x -= HexMapNavigation.HexMetrics.wrapSize * HexMapNavigation.HexMetrics.innerDiameter;
				}
				Vector3 v3 = center3 + (nextNeighbor.TerrainOrDefaultData().IsUnderwater ?
					HexMetrics.GetFirstWaterCorner(direction.Previous()) :
					HexMetrics.GetFirstSolidCorner(direction.Previous()));
				v3.y = center.y;
				waterShore.AddTriangle(e1.v5, e2.v5, v3);
				waterShore.AddTriangleUV(
					new Vector2(0f, 0f),
					new Vector2(0f, 1f),
					new Vector2(0f, nextNeighbor.TerrainOrDefaultData().IsUnderwater ? 0f : 1f)
				);
				indices.z = nextNeighbor.Index;
				waterShore.AddTriangleCellData(
					indices, weights1, weights2, weights3
				);
			}
		}

		void TriangulateEstuary(EdgeVertices5 e1, EdgeVertices5 e2, bool incomingRiver, Vector3 indices)
		{
			waterShore.AddTriangle(e2.v1, e1.v2, e1.v1);
			waterShore.AddTriangle(e2.v5, e1.v5, e1.v4);
			waterShore.AddTriangleUV(
				new Vector2(0f, 1f), new Vector2(0f, 0f), new Vector2(0f, 0f)
			);
			waterShore.AddTriangleUV(
				new Vector2(0f, 1f), new Vector2(0f, 0f), new Vector2(0f, 0f)
			);
			waterShore.AddTriangleCellData(indices, weights2, weights1, weights1);
			waterShore.AddTriangleCellData(indices, weights2, weights1, weights1);

			estuaries.AddQuad(e2.v1, e1.v2, e2.v2, e1.v3);
			estuaries.AddTriangle(e1.v3, e2.v2, e2.v4);
			estuaries.AddQuad(e1.v3, e1.v4, e2.v4, e2.v5);

			estuaries.AddQuadUV(
				new Vector2(0f, 1f), new Vector2(0f, 0f),
				new Vector2(1f, 1f), new Vector2(0f, 0f)
			);
			estuaries.AddTriangleUV(
				new Vector2(0f, 0f), new Vector2(1f, 1f), new Vector2(1f, 1f)
			);
			estuaries.AddQuadUV(
				new Vector2(0f, 0f), new Vector2(0f, 0f),
				new Vector2(1f, 1f), new Vector2(0f, 1f)
			);
			estuaries.AddQuadCellData(
				indices, weights2, weights1, weights2, weights1
			);
			estuaries.AddTriangleCellData(indices, weights1, weights2, weights2);
			estuaries.AddQuadCellData(indices, weights1, weights2);

			if (incomingRiver)
			{
				estuaries.AddQuadUV2(
					new Vector2(1.5f, 1f), new Vector2(0.7f, 1.15f),
					new Vector2(1f, 0.8f), new Vector2(0.5f, 1.1f)
				);
				estuaries.AddTriangleUV2(
					new Vector2(0.5f, 1.1f),
					new Vector2(1f, 0.8f),
					new Vector2(0f, 0.8f)
				);
				estuaries.AddQuadUV2(
					new Vector2(0.5f, 1.1f), new Vector2(0.3f, 1.15f),
					new Vector2(0f, 0.8f), new Vector2(-0.5f, 1f)
				);
			}
			else
			{
				estuaries.AddQuadUV2(
					new Vector2(-0.5f, -0.2f), new Vector2(0.3f, -0.35f),
					new Vector2(0f, 0f), new Vector2(0.5f, -0.3f)
				);
				estuaries.AddTriangleUV2(
					new Vector2(0.5f, -0.3f),
					new Vector2(0f, 0f),
					new Vector2(1f, 0f)
				);
				estuaries.AddQuadUV2(
					new Vector2(0.5f, -0.3f), new Vector2(0.7f, -0.35f),
					new Vector2(1f, 0f), new Vector2(1.5f, -0.2f)
				);
			}
		}

		void TriangulateWithoutRiver(HexMapNavigation.HexDirection direction, IHexCell cell, Vector3 center, EdgeVertices5 e)
		{
			// triangle fan in the middle of hex
			TriangulateEdgeFan(center, e, cell.Index);

			if (cell.TerrainData.HasRoads)
			{
				Vector2 interpolators = GetRoadInterpolators(direction, cell);
				TriangulateRoad(center,  Vector3.Lerp(center, e.v1, interpolators.x), Vector3.Lerp(center, e.v5, interpolators.y), e, cell.TerrainData.HasRoadThroughEdge(direction), cell.Index);
			}
		}

		Vector2 GetRoadInterpolators(HexMapNavigation.HexDirection direction, IHexCell cell)
		{
			Vector2 interpolators;
			if (cell.TerrainData.HasRoadThroughEdge(direction))
			{
				interpolators.x = interpolators.y = 0.5f;
			}
			else
			{
				interpolators.x =
					cell.TerrainData.HasRoadThroughEdge(direction.Previous()) ? 0.5f : 0.25f;
				interpolators.y =
					cell.TerrainData.HasRoadThroughEdge(direction.Next()) ? 0.5f : 0.25f;
			}
			return interpolators;
		}

		void TriangulateAdjacentToRiver(IHexEnvironmentManager environment, HexMapNavigation.HexDirection direction, IHexCell cell, Vector3 center, EdgeVertices5 e)
		{
			if (cell.TerrainData.HasRoads)
			{
				TriangulateRoadAdjacentToRiver(environment, direction, cell, center, e);
			}

			if (cell.TerrainData.HasRiverThroughEdge(direction.Next()))
			{
				if (cell.TerrainData.HasRiverThroughEdge(direction.Previous()))
				{
					center += HexMetrics.GetSolidEdgeMiddle(direction) *
						(HexMapNavigation.HexMetrics.innerToOuter * 0.5f);
				}
				else if (
					cell.TerrainData.HasRiverThroughEdge(direction.Previous2())
				)
				{
					center += HexMetrics.GetFirstSolidCorner(direction) * 0.25f;
				}
			}
			else if (
				cell.TerrainData.HasRiverThroughEdge(direction.Previous()) &&
				cell.TerrainData.HasRiverThroughEdge(direction.Next2())
			)
			{
				center += HexMetrics.GetSecondSolidCorner(direction) * 0.25f;
			}

			EdgeVertices5 m = new EdgeVertices5(
				Vector3.Lerp(center, e.v1, 0.5f),
				Vector3.Lerp(center, e.v5, 0.5f)
			);

			TriangulateEdgeStrip(
				m, weights1, cell.Index,
				e, weights1, cell.Index
			);
			TriangulateEdgeFan(center, m, cell.Index);

			if (!cell.TerrainData.IsUnderwater && !cell.TerrainData.HasRoadThroughEdge(direction))
			{
				environment.Features?.AddFeature(cell, (center + e.v1 + e.v5) * (1f / 3f));
			}
		}

		void TriangulateRoadAdjacentToRiver(IHexEnvironmentManager environment, HexMapNavigation.HexDirection direction, IHexCell cell, Vector3 center, EdgeVertices5 e)
		{
			bool hasRoadThroughEdge = cell.TerrainData.HasRoadThroughEdge(direction);
			bool previousHasRiver = cell.TerrainData.HasRiverThroughEdge(direction.Previous());
			bool nextHasRiver = cell.TerrainData.HasRiverThroughEdge(direction.Next());
			Vector2 interpolators = GetRoadInterpolators(direction, cell);
			Vector3 roadCenter = center;

			if (cell.TerrainData.HasRiverBeginOrEnd)
			{
				roadCenter += HexMetrics.GetSolidEdgeMiddle(
					cell.TerrainData.RiverBeginOrEndDirection.Opposite()
				) * (1f / 3f);
			}
			else if (cell.TerrainData.IncomingRiver == cell.TerrainData.OutgoingRiver.Opposite())
			{
				Vector3 corner;
				if (previousHasRiver)
				{
					if (
						!hasRoadThroughEdge &&
						!cell.TerrainData.HasRoadThroughEdge(direction.Next())
					)
					{
						return;
					}
					corner = HexMetrics.GetSecondSolidCorner(direction);
				}
				else
				{
					if (
						!hasRoadThroughEdge &&
						!cell.TerrainData.HasRoadThroughEdge(direction.Previous())
					)
					{
						return;
					}
					corner = HexMetrics.GetFirstSolidCorner(direction);
				}
				roadCenter += corner * 0.5f;
				if (cell.TerrainData.IncomingRiver == direction.Next() && (
					cell.TerrainData.HasRoadThroughEdge(direction.Next2()) ||
					cell.TerrainData.HasRoadThroughEdge(direction.Opposite())
				))
				{
					environment.Features?.AddBridge(roadCenter, center - corner * 0.5f);
				}
				center += corner * 0.25f;
			}
			else if (cell.TerrainData.IncomingRiver == cell.TerrainData.OutgoingRiver.Previous())
			{
				roadCenter -= HexMetrics.GetSecondCorner(cell.TerrainData.IncomingRiver) * 0.2f;
			}
			else if (cell.TerrainData.IncomingRiver == cell.TerrainData.OutgoingRiver.Next())
			{
				roadCenter -= HexMetrics.GetFirstCorner(cell.TerrainData.IncomingRiver) * 0.2f;
			}
			else if (previousHasRiver && nextHasRiver)
			{
				if (!hasRoadThroughEdge)
				{
					return;
				}
				Vector3 offset = HexMetrics.GetSolidEdgeMiddle(direction) *
					HexMapNavigation.HexMetrics.innerToOuter;
				roadCenter += offset * 0.7f;
				center += offset * 0.5f;
			}
			else
			{
				HexMapNavigation.HexDirection middle;
				if (previousHasRiver)
				{
					middle = direction.Next();
				}
				else if (nextHasRiver)
				{
					middle = direction.Previous();
				}
				else
				{
					middle = direction;
				}
				if (
					!cell.TerrainData.HasRoadThroughEdge(middle) &&
					!cell.TerrainData.HasRoadThroughEdge(middle.Previous()) &&
					!cell.TerrainData.HasRoadThroughEdge(middle.Next())
				)
				{
					return;
				}
				Vector3 offset = HexMetrics.GetSolidEdgeMiddle(middle);
				roadCenter += offset * 0.25f;
				if (
					direction == middle &&
					cell.TerrainData.HasRoadThroughEdge(direction.Opposite())
				)
				{
					environment.Features?.AddBridge(roadCenter, center - offset * (HexMapNavigation.HexMetrics.innerToOuter * 0.7f));
				}
			}

			Vector3 mL = Vector3.Lerp(roadCenter, e.v1, interpolators.x);
			Vector3 mR = Vector3.Lerp(roadCenter, e.v5, interpolators.y);
			TriangulateRoad(roadCenter, mL, mR, e, hasRoadThroughEdge, cell.Index);
			if (previousHasRiver)
			{
				TriangulateRoadEdge(roadCenter, center, mL, cell.Index);
			}
			if (nextHasRiver)
			{
				TriangulateRoadEdge(roadCenter, mR, center, cell.Index);
			}
		}

		void TriangulateWithRiverBeginOrEnd(HexMapNavigation.HexDirection direction, IHexCell cell, Vector3 center, EdgeVertices5 e)
		{
			EdgeVertices5 m = new EdgeVertices5(Vector3.Lerp(center, e.v1, 0.5f),  Vector3.Lerp(center, e.v5, 0.5f));
			m.v3.y = e.v3.y;

			TriangulateEdgeStrip(m, weights1, cell.Index, e, weights1, cell.Index);
			TriangulateEdgeFan(center, m, cell.Index);

			if (!cell.TerrainData.IsUnderwater)
			{
				bool reversed = cell.TerrainData.HasIncomingRiver;
				Vector3 indices;
				indices.x = indices.y = indices.z = cell.Index;
				TriangulateRiverQuad(
					m.v2, m.v4, e.v2, e.v4,
					cell.TerrainData.RiverSurfaceY, 0.6f, reversed, indices
				);
				center.y = m.v2.y = m.v4.y = cell.TerrainData.RiverSurfaceY;
				rivers.AddTriangle(center, m.v2, m.v4);
				if (reversed)
				{
					rivers.AddTriangleUV(
						new Vector2(0.5f, 0.4f),
						new Vector2(1f, 0.2f), 
						new Vector2(0f, 0.2f)
					);
				}
				else
				{
					rivers.AddTriangleUV(
						new Vector2(0.5f, 0.4f),
						new Vector2(0f, 0.6f), 
						new Vector2(1f, 0.6f)
					);
				}
				rivers.AddTriangleCellData(indices, weights1);
			}
		}

		void TriangulateWithRiver(HexMapNavigation.HexDirection direction, IHexCell cell, Vector3 center, EdgeVertices5 e)
		{
			Vector3 centerL, centerR;
			if (cell.TerrainData.HasRiverThroughEdge(direction.Opposite()))
			{
				centerL = center +
					HexMetrics.GetFirstSolidCorner(direction.Previous()) * 0.25f;
				centerR = center +
					HexMetrics.GetSecondSolidCorner(direction.Next()) * 0.25f;
			}
			else if (cell.TerrainData.HasRiverThroughEdge(direction.Next()))
			{
				centerL = center;
				centerR = Vector3.Lerp(center, e.v5, 2f / 3f);
			}
			else if (cell.TerrainData.HasRiverThroughEdge(direction.Previous()))
			{
				centerL = Vector3.Lerp(center, e.v1, 2f / 3f);
				centerR = center;
			}
			else if (cell.TerrainData.HasRiverThroughEdge(direction.Next2()))
			{
				centerL = center;
				centerR = center +
					HexMetrics.GetSolidEdgeMiddle(direction.Next()) *
					(0.5f * HexMapNavigation.HexMetrics.innerToOuter);
			}
			else
			{
				centerL = center +
					HexMetrics.GetSolidEdgeMiddle(direction.Previous()) *
					(0.5f * HexMapNavigation.HexMetrics.innerToOuter);
				centerR = center;
			}
			center = Vector3.Lerp(centerL, centerR, 0.5f);

			EdgeVertices5 m = new EdgeVertices5(
				Vector3.Lerp(centerL, e.v1, 0.5f),
				Vector3.Lerp(centerR, e.v5, 0.5f),
				1f / 6f
			);
			m.v3.y = center.y = e.v3.y;

			TriangulateEdgeStrip(
				m, weights1, cell.Index,
				e, weights1, cell.Index
			);

			terrain.AddTriangle(centerL, m.v1, m.v2);
			terrain.AddQuad(centerL, center, m.v2, m.v3);
			terrain.AddQuad(center, centerR, m.v3, m.v4);
			terrain.AddTriangle(centerR, m.v4, m.v5);

			Vector3 indices;
			indices.x = indices.y = indices.z = cell.Index;
			terrain.AddTriangleCellData(indices, weights1);
			terrain.AddQuadCellData(indices, weights1);
			terrain.AddQuadCellData(indices, weights1);
			terrain.AddTriangleCellData(indices, weights1);

			if (!cell.TerrainData.IsUnderwater)
			{
				bool reversed = cell.TerrainData.IncomingRiver == direction;
				TriangulateRiverQuad(
					centerL, centerR, m.v2, m.v4,
					cell.TerrainData.RiverSurfaceY, 0.4f, reversed, indices
				);
				TriangulateRiverQuad(
					m.v2, m.v4, e.v2, e.v4,
					cell.TerrainData.RiverSurfaceY, 0.6f, reversed, indices
				);
			}
		}

		// === triangulate all connection between hew cells ===

		// main function

		void TriangulateConnection(IHexEnvironmentManager environment, HexMapNavigation.HexDirection direction, IHexCell cell, EdgeVertices5 e1)
		{
			// get neighbor according to direction
			IHexCell neighbor = cell.GetNeighbor(direction) as IHexCell;
			if (neighbor == null)
			{
				return;
			}
			// bridge eq to the normal direction to the edge
			Vector3 bridge = HexMetrics.GetBridge(direction);
			// offset between the elevation from 2 cells
			bridge.y = neighbor.LocalPosition.y - cell.LocalPosition.y;
			// we create new edge 
			/*              0
			 *             ***
			 *          **     **   e1    e2
			 *      5**           **1 *** 1
			 *      *               *     *
			 *      *               *     *
			 *      *               *     *
			 *      4**           **2 *** 2
			 *          **     **   
			 *             ***
			 *              3
			 */
			EdgeVertices5 e2 = new EdgeVertices5(
				e1.v1 + bridge,
				e1.v5 + bridge
			);

			bool hasRiver = cell.TerrainData.HasRiverThroughEdge(direction);
			bool hasRoad = cell.TerrainData.HasRoadThroughEdge(direction);

			if (hasRiver)
			{
				e2.v3.y = neighbor.TerrainOrDefaultData().StreamBedY;
				Vector3 indices;
				indices.x = indices.z = cell.Index;
				indices.y = neighbor.Index;

				if (!cell.TerrainData.IsUnderwater)
				{
					if (!neighbor.TerrainOrDefaultData().IsUnderwater)
					{
						TriangulateRiverQuad(
							e1.v2, e1.v4, e2.v2, e2.v4,
							cell.TerrainData.RiverSurfaceY, neighbor.TerrainOrDefaultData().RiverSurfaceY, 0.8f,
							cell.TerrainData.HasIncomingRiver && cell.TerrainData.IncomingRiver == direction,
							indices
						);
					}
					else if (cell.TerrainData.Elevation > neighbor.TerrainOrDefaultData().WaterLevel)
					{
						TriangulateWaterfallInWater(
							e1.v2, e1.v4, e2.v2, e2.v4,
							cell.TerrainData.RiverSurfaceY, neighbor.TerrainOrDefaultData().RiverSurfaceY,
							neighbor.TerrainOrDefaultData().WaterSurfaceY, indices
						);
					}
				}
				else if (
					!neighbor.TerrainOrDefaultData().IsUnderwater &&
					neighbor.TerrainOrDefaultData().Elevation > cell.TerrainOrDefaultData().WaterLevel
				)
				{
					TriangulateWaterfallInWater(
						e2.v4, e2.v2, e1.v4, e1.v2,
						neighbor.TerrainOrDefaultData().RiverSurfaceY, cell.TerrainData.RiverSurfaceY,
						cell.TerrainData.WaterSurfaceY, indices
					);
				}
			}

			if (cell.TerrainData.GetEdgeType(direction) == HexEdgeType.Slope)
			{
				// several strips between 2 neighbor
				TriangulateEdgeTerraces(e1, cell, e2, neighbor, hasRoad);
			}
			else
			{
				// one strip
				TriangulateEdgeStrip(
					e1, weights1, cell.Index,
					e2, weights2, neighbor.Index, hasRoad
				);
			}

			environment.Features?.AddWall(e1, cell, e2, neighbor, hasRiver, hasRoad);

			// Add triangle between direction 
			IHexCell nextNeighbor = cell.GetNeighbor(direction.Next()) as IHexCell;
			if (direction <= HexMapNavigation.HexDirection.E && nextNeighbor != null)
			{
				Vector3 v5 = e1.v5 + HexMetrics.GetBridge(direction.Next());
				v5.y = nextNeighbor.LocalPosition.y;

				if (cell.TerrainData.Elevation <= neighbor.TerrainOrDefaultData().Elevation)
				{
					if (cell.TerrainData.Elevation <= nextNeighbor.TerrainOrDefaultData().Elevation)
					{
						TriangulateCorner(cell, environment, e1.v5, cell, e2.v5, neighbor, v5, nextNeighbor);
					}
					else
					{
						TriangulateCorner(cell, environment, v5, nextNeighbor, e1.v5, cell, e2.v5, neighbor);
					}
				}
				else if (neighbor.TerrainOrDefaultData().Elevation <= nextNeighbor.TerrainOrDefaultData().Elevation)
				{
					TriangulateCorner(cell, environment, e2.v5, neighbor, v5, nextNeighbor, e1.v5, cell);
				}
				else
				{
					TriangulateCorner(cell, environment, v5, nextNeighbor, e1.v5, cell, e2.v5, neighbor);
				}
			}
		}

		void TriangulateWaterfallInWater(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, float y1, float y2, float waterY, Vector3 indices)
		{
			v1.y = v2.y = y1;
			v3.y = v4.y = y2;
			v1 = HexMetrics.Perturb(v1);
			v2 = HexMetrics.Perturb(v2);
			v3 = HexMetrics.Perturb(v3);
			v4 = HexMetrics.Perturb(v4);
			float t = (waterY - y2) / (y1 - y2);
			v3 = Vector3.Lerp(v3, v1, t);
			v4 = Vector3.Lerp(v4, v2, t);
			rivers.AddQuadUnperturbed(v1, v2, v3, v4);
			rivers.AddQuadUV(0f, 1f, 0.8f, 1f);
			rivers.AddQuadCellData(indices, weights1, weights2);
		}

		// triangulate corners between connection (bridge)
		void TriangulateCorner(IHexCell centerCell, IHexEnvironmentManager environment, Vector3 bottom, IHexCell bottomCell, Vector3 left, IHexCell leftCell, Vector3 right, IHexCell rightCell)
		{
			HexEdgeType leftEdgeType = bottomCell.TerrainOrDefaultData().GetEdgeType(leftCell);
			HexEdgeType rightEdgeType = bottomCell.TerrainOrDefaultData().GetEdgeType(rightCell);

			if (leftEdgeType == HexEdgeType.Slope)
			{
				if (rightEdgeType == HexEdgeType.Slope)
				{
					TriangulateCornerTerraces(
						bottom, bottomCell, left, leftCell, right, rightCell
					);
				}
				else if (rightEdgeType == HexEdgeType.Flat)
				{
					TriangulateCornerTerraces(left, leftCell, right, rightCell, bottom, bottomCell);
				}
				else
				{
					TriangulateCornerTerracesCliff(centerCell, bottom, bottomCell, left, leftCell, right, rightCell);
				}
			}
			else if (rightEdgeType == HexEdgeType.Slope)
			{
				if (leftEdgeType == HexEdgeType.Flat)
				{
					TriangulateCornerTerraces(right, rightCell, bottom, bottomCell, left, leftCell);
				}
				else
				{
					TriangulateCornerCliffTerraces(centerCell, bottom, bottomCell, left, leftCell, right, rightCell);
				}
			}
			else if (leftCell.TerrainOrDefaultData().GetEdgeType(rightCell) == HexEdgeType.Slope)
			{
				if (leftCell.TerrainOrDefaultData().Elevation < rightCell.TerrainOrDefaultData().Elevation)
				{
					TriangulateCornerCliffTerraces(centerCell, right, rightCell, bottom, bottomCell, left, leftCell);
				}
				else
				{
					TriangulateCornerTerracesCliff(centerCell, left, leftCell, right, rightCell, bottom, bottomCell);
				}
			}
			else
			{
				terrain.AddTriangle(bottom, left, right);
				Vector3 indices;
				indices.x = bottomCell.Index;
				indices.y = leftCell.Index;
				indices.z = rightCell.Index;
				terrain.AddTriangleCellData(indices, weights1, weights2, weights3);
			}

			environment.Features?.AddWall(centerCell, bottom, bottomCell, left, leftCell, right, rightCell);
		}

		// *** Terraces

		// create several strips between hex cells        


		// one strip by terrace
		void TriangulateEdgeTerraces(EdgeVertices5 begin, IHexCell beginCell, EdgeVertices5 end, IHexCell endCell,  bool hasRoad)
		{
			EdgeVertices5 e2 = EdgeVertices5.TerraceLerp(begin, end, 1);
			Color w2 = HexMetrics.TerraceLerp(weights1, weights2, 1);
			float i1 = beginCell.Index;
			float i2 = endCell.Index;

			// we draw first vertical strip
			TriangulateEdgeStrip(begin, weights1, i1, e2, w2, i2, hasRoad);
		   
			// we draw each steps
			for (int i = 2; i < HexMetrics.terraceSteps; i++)
			{
				EdgeVertices5 e1 = e2;
				Color w1 = w2;
				e2 = EdgeVertices5.TerraceLerp(begin, end, i);
				w2 = HexMetrics.TerraceLerp(weights1, weights2, i);
				TriangulateEdgeStrip(e1, w1, i1, e2, w2, i2, hasRoad);
			}

			// we draw last step
			TriangulateEdgeStrip(e2, w2, i1, end, weights2, i2, hasRoad);
		}

		// terrace corners
		void TriangulateCornerTerraces(Vector3 begin, IHexCell beginCell, Vector3 left, IHexCell leftCell, Vector3 right, IHexCell rightCell)
		{
			Vector3 v3 = HexMetrics.TerraceLerp(begin, left, 1);
			Vector3 v4 = HexMetrics.TerraceLerp(begin, right, 1);
			Color w3 = HexMetrics.TerraceLerp(weights1, weights2, 1);
			Color w4 = HexMetrics.TerraceLerp(weights1, weights3, 1);
			Vector3 indices;
			indices.x = beginCell.Index;
			indices.y = leftCell.Index;
			indices.z = rightCell.Index;

			terrain.AddTriangle(begin, v3, v4);
			terrain.AddTriangleCellData(indices, weights1, w3, w4);

			for (int i = 2; i < HexMetrics.terraceSteps; i++)
			{
				Vector3 v1 = v3;
				Vector3 v2 = v4;
				Color w1 = w3;
				Color w2 = w4;
				v3 = HexMetrics.TerraceLerp(begin, left, i);
				v4 = HexMetrics.TerraceLerp(begin, right, i);
				w3 = HexMetrics.TerraceLerp(weights1, weights2, i);
				w4 = HexMetrics.TerraceLerp(weights1, weights3, i);
				terrain.AddQuad(v1, v2, v3, v4);
				terrain.AddQuadCellData(indices, w1, w2, w3, w4);
			}

			terrain.AddQuad(v3, v4, left, right);
			terrain.AddQuadCellData(indices, w3, w4, weights2, weights3);
		}

		// terrace corners terraces cliff
		void TriangulateCornerTerracesCliff(IHexCell centerCell, Vector3 begin, IHexCell beginCell, Vector3 left, IHexCell leftCell, Vector3 right, IHexCell rightCell)
		{
			float b = 1f / (rightCell.TerrainOrDefaultData().Elevation - beginCell.TerrainOrDefaultData().Elevation);
			if (b < 0)
			{
				b = -b;
			}
			Vector3 boundary = Vector3.Lerp(
				HexMetrics.Perturb(begin), HexMetrics.Perturb(right), b
			);
			Color boundaryWeights = Color.Lerp(weights1, weights3, b);
			Vector3 indices;
			indices.x = beginCell.Index;
			indices.y = leftCell.Index;
			indices.z = rightCell.Index;

			TriangulateBoundaryTriangle(
				begin, weights1, left, weights2, boundary, boundaryWeights, indices
			);

			if (leftCell.TerrainOrDefaultData().GetEdgeType(rightCell) == HexEdgeType.Slope)
			{
				TriangulateBoundaryTriangle(
					left, weights2, right, weights3,
					boundary, boundaryWeights, indices
				);
			}
			else
			{
				terrain.AddTriangleUnperturbed(HexMetrics.Perturb(left), HexMetrics.Perturb(right), boundary);
				terrain.AddTriangleCellData(
					indices, weights2, weights3, boundaryWeights
				);
			}
		}

		// terrace corner cliff terraces
		void TriangulateCornerCliffTerraces(IHexCell centerCell, Vector3 begin, IHexCell beginCell, Vector3 left, IHexCell leftCell, Vector3 right, IHexCell rightCell)
		{
			float b = 1f / (leftCell.TerrainOrDefaultData().Elevation - beginCell.TerrainOrDefaultData().Elevation);
			if (b < 0)
			{
				b = -b;
			}
			Vector3 boundary = Vector3.Lerp(HexMetrics.Perturb(begin), HexMetrics.Perturb(left), b);
			Color boundaryWeights = Color.Lerp(weights1, weights2, b);
			Vector3 indices;
			indices.x = beginCell.Index;
			indices.y = leftCell.Index;
			indices.z = rightCell.Index;

			TriangulateBoundaryTriangle(
				right, weights3, begin, weights1, boundary, boundaryWeights, indices
			);

			if (leftCell.TerrainOrDefaultData().GetEdgeType(rightCell) == HexEdgeType.Slope)
			{
				TriangulateBoundaryTriangle(left, weights2, right, weights3, boundary, boundaryWeights, indices);
			}
			else
			{
				terrain.AddTriangleUnperturbed(HexMetrics.Perturb(left), HexMetrics.Perturb(right), boundary);
				terrain.AddTriangleCellData(indices, weights2, weights3, boundaryWeights);
			}
		}

		void TriangulateBoundaryTriangle(Vector3 begin, Color beginWeights, Vector3 left, Color leftWeights, Vector3 boundary, Color boundaryWeights, Vector3 indices)
		{
			Vector3 v2 = HexMetrics.Perturb(HexMetrics.TerraceLerp(begin, left, 1));
			Color w2 = HexMetrics.TerraceLerp(beginWeights, leftWeights, 1);

			terrain.AddTriangleUnperturbed(HexMetrics.Perturb(begin), v2, boundary);
			terrain.AddTriangleCellData(indices, beginWeights, w2, boundaryWeights);

			for (int i = 2; i < HexMetrics.terraceSteps; i++)
			{
				Vector3 v1 = v2;
				Color w1 = w2;
				v2 = HexMetrics.Perturb(HexMetrics.TerraceLerp(begin, left, i));
				w2 = HexMetrics.TerraceLerp(beginWeights, leftWeights, i);
				terrain.AddTriangleUnperturbed(v1, v2, boundary);
				terrain.AddTriangleCellData(indices, w1, w2, boundaryWeights);
			}

			terrain.AddTriangleUnperturbed(v2, HexMetrics.Perturb(left), boundary);
			terrain.AddTriangleCellData(indices, w2, leftWeights, boundaryWeights);
		}

		void TriangulateEdgeFan(Vector3 center, EdgeVertices5 edge, float index)
		{
			terrain.AddTriangle(center, edge.v1, edge.v2);
			terrain.AddTriangle(center, edge.v2, edge.v3);
			terrain.AddTriangle(center, edge.v3, edge.v4);
			terrain.AddTriangle(center, edge.v4, edge.v5);

			Vector3 indices;
			indices.x = indices.y = indices.z = index;
			terrain.AddTriangleCellData(indices, weights1);
			terrain.AddTriangleCellData(indices, weights1);
			terrain.AddTriangleCellData(indices, weights1);
			terrain.AddTriangleCellData(indices, weights1);
		}

		void TriangulateEdgeStrip(EdgeVertices5 e1, Color w1, float index1, EdgeVertices5 e2, Color w2, float index2, bool hasRoad = false)
		{
			terrain.AddQuad(e1.v1, e1.v2, e2.v1, e2.v2);
			terrain.AddQuad(e1.v2, e1.v3, e2.v2, e2.v3);
			terrain.AddQuad(e1.v3, e1.v4, e2.v3, e2.v4);
			terrain.AddQuad(e1.v4, e1.v5, e2.v4, e2.v5);

			Vector3 indices;
			indices.x = indices.z = index1;
			indices.y = index2;
			terrain.AddQuadCellData(indices, w1, w2);
			terrain.AddQuadCellData(indices, w1, w2);
			terrain.AddQuadCellData(indices, w1, w2);
			terrain.AddQuadCellData(indices, w1, w2);

			if (hasRoad)
			{
				TriangulateRoadSegment(
					e1.v2, e1.v3, e1.v4, e2.v2, e2.v3, e2.v4, w1, w2, indices
				);
			}
		}

		void TriangulateRiverQuad(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, float y, float v, bool reversed, Vector3 indices)
		{
			TriangulateRiverQuad(v1, v2, v3, v4, y, y, v, reversed, indices);
		}

		void TriangulateRiverQuad(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, float y1, float y2, float v, bool reversed, Vector3 indices)
		{
			v1.y = v2.y = y1;
			v3.y = v4.y = y2;
			rivers.AddQuad(v1, v2, v3, v4);
			if (reversed)
			{
				rivers.AddQuadUV(1f, 0f, 0.8f - v, 0.6f - v);
			}
			else
			{
				rivers.AddQuadUV(0f, 1f, v, v + 0.2f);
			}
			rivers.AddQuadCellData(indices, weights1, weights2);
		}

		void TriangulateRoad(Vector3 center, Vector3 mL, Vector3 mR, EdgeVertices5 e, bool hasRoadThroughCellEdge, float index)
		{
			if (hasRoadThroughCellEdge)
			{
				Vector3 indices;
				indices.x = indices.y = indices.z = index;
				Vector3 mC = Vector3.Lerp(mL, mR, 0.5f);
				TriangulateRoadSegment(
					mL, mC, mR, e.v2, e.v3, e.v4,
					weights1, weights1, indices
				);
				roads.AddTriangle(center, mL, mC);
				roads.AddTriangle(center, mC, mR);
				roads.AddTriangleUV(
					new Vector2(1f, 0f), new Vector2(0f, 0f), new Vector2(1f, 0f)
				);
				roads.AddTriangleUV(
					new Vector2(1f, 0f), new Vector2(1f, 0f), new Vector2(0f, 0f)
				);
				roads.AddTriangleCellData(indices, weights1);
				roads.AddTriangleCellData(indices, weights1);
			}
			else
			{
				TriangulateRoadEdge(center, mL, mR, index);
			}
		}

		void TriangulateRoadEdge(Vector3 center, Vector3 mL, Vector3 mR, float index)
		{
			roads.AddTriangle(center, mL, mR);
			roads.AddTriangleUV(
				new Vector2(1f, 0f), new Vector2(0f, 0f), new Vector2(0f, 0f)
			);
			Vector3 indices;
			indices.x = indices.y = indices.z = index;
			roads.AddTriangleCellData(indices, weights1);
		}

		void TriangulateRoadSegment(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, Vector3 v5, Vector3 v6, Color w1, Color w2, Vector3 indices)
		{
			roads.AddQuad(v1, v2, v4, v5);
			roads.AddQuad(v2, v3, v5, v6);
			roads.AddQuadUV(0f, 1f, 0f, 0f);
			roads.AddQuadUV(1f, 0f, 0f, 0f);
			roads.AddQuadCellData(indices, w1, w2);
			roads.AddQuadCellData(indices, w1, w2);
		}
	}
}