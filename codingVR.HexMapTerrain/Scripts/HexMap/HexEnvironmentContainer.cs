﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.HexMapTerrain
{
	public class HexEnvironementContainer
	{
		/*
		 * 
		 * ctor
		 * 
		 */

		public HexEnvironementContainer(Transform parentTransform, string containerName, string tagName)
		{
			// manage props container construction
			// persistent container
			var propsContainerGameObject = new GameObject(containerName);
			propsContainerGameObject.tag = tagName;
			TransformObject = propsContainerGameObject.transform;
			TransformObject.SetParent(parentTransform, false);
		}

		public Transform TransformObject { get; private set; }

		/*
		 * 
		 * Moving object inside this container
		 * 
		 */

		public void MovingTo(IHexCell cell)
		{
			foreach (Transform child in TransformObject)
			{
				if (child.GetComponent<RuntimeTools.PersistableObject>() != null)
				{
					var pose = child.GetComponent<HexMapNavigation.HexGridObjectPoseBase>();
					bool staticObject = pose?.StaticPosition ?? false;
					if (staticObject == false)
					{
						if (HexMetrics.IsInsideHexCell(child.position, cell.Position) == true)
						{
							child.position += cell.TerrainData.MovingCell;
						}
					}
				}
			}
		}

		/*
		 * 
		 * Destroy child
		 * 
		 */

		public bool DestroyChild(string childIdentifier)
		{
			bool b = false;
			foreach (Transform child in TransformObject)
			{
				var obj = child.GetComponent<RuntimeTools.PersistableObject>();
				if (obj != null)
				{
					if (obj.Identifier.Equals(childIdentifier) == true)
					{
						GameObject.DestroyImmediate(obj.gameObject);
						b = true;
						break;
					}
				}
			}
			return b;
		}


		// find child identified by childIdentifier
		// containerIdx :  see HexEnvironmentManager.***ContainerIdx
		public Transform FindChild(string childIdentifier)
		{
			Transform transform = null;
			foreach (Transform child in TransformObject)
			{
				var obj = child.GetComponent<RuntimeTools.PersistableObject>();
				if (obj != null)
				{
					if (obj.Identifier.Equals(childIdentifier) == true)
					{
						transform = obj.transform;
						break;
					}
				}
			}
			return transform;
		}

		// find child identified by childFromParentIdentifier
		// containerIdx :  see HexEnvironmentManager.***ContainerIdx
		public List<Transform> FindChilds(string childFromParentIdentifier)
		{
			List<Transform> list = new List<Transform>();
			foreach (Transform child in TransformObject)
			{
				var obj = child.GetComponent<RuntimeTools.PersistableObject>();
				if (obj != null)
				{
					if (obj.IdentifierParent.Equals(childFromParentIdentifier) == true)
					{
						list.Add(obj.transform);
					}
				}
			}
			return list;
		}


		// find all childs
		// containerIdx :  see HexEnvironmentManager.***ContainerIdx
		public List<Transform> FindChilds()
		{
			List<Transform> list = new List<Transform>();
			foreach (Transform child in TransformObject)
			{
				var obj = child.GetComponent<RuntimeTools.PersistableObject>();
				if (obj != null)
				{
					list.Add(obj.transform);
				}
			}
			return list;
		}

		/*
		 * 
		 * Save & Load
		 * 
		 */

		public void Save(Core.GameDataWriter writer, string headerName)
		{
			// props
			writer.WriteObjectStart(headerName);
			// array of persistable objects
			var allPersistableObjects = TransformObject.GetComponentsInChildren<RuntimeTools.PersistableObject>();

			// make an array of valid object
			var allPersistableValidObjects = new List<RuntimeTools.PersistableObject>();
			foreach (var it in allPersistableObjects)
			{
				if (it.IsRessourceValid() == true)
				{
					allPersistableValidObjects.Add(it);
				}
				else
				{
					Debug.LogErrorFormat("{0} : the game object {1} is not valid !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, it.name);
				}
			}

			// write the count
			writer.Write(allPersistableValidObjects.Count);


			// save each ones
			int num = 0;
			foreach (var it in allPersistableValidObjects)
			{
				// start
				writer.WriteObjectStart("Object[" + num++ + "]");
				// write asset resource path 
				string AssetRessourcesPath = it.AssetRessourcesPath;
				writer.Write(AssetRessourcesPath, "Prefab");
				// save objects
				it.Save(writer);
				// game object properties
				writer.WriteObjectStart("Properties");
				writer.Write(it.gameObject.activeSelf, "Active");

				// get place holder
				int IsPlaceHolder = 0;
				RuntimeTools.IPersistableObject placeHolder = it.GetComponent<RuntimeTools.IPersistableObject>();
				if (placeHolder != null)
				{
					// save type of place holder
					var placeHolderType = it.GetComponent<HexMapNavigation.IHexPlaceHolder>();
					IsPlaceHolder = placeHolderType.Type;
				}

				// static position
				var pose = it.GetComponent<HexMapNavigation.HexGridObjectPoseBase>();
				bool staticObject = pose?.StaticPosition ?? false;
				writer.Write(staticObject, "StaticObject");
				writer.Write(IsPlaceHolder, "IsPlaceHolder");
				writer.WriteObjectEnd();

				// if place Holder delected the load place holder
				if (IsPlaceHolder > 0)
				{
					placeHolder.Save(writer);
				}

				// end
				writer.WriteObjectEnd();
			}

			writer.WriteObjectEnd();
		}

		public void Load(Core.GameDataReader reader, Scene.IResourcesLoader resourcesLoader, int version, string headerName)
		{
			// props
			reader.ReadObjectStart(headerName);
			// read the count
			int length = reader.ReadInt();
			// load each ones
			for (int i = 0; i < length; ++i)
			{
				// start
				reader.ReadObjectStart("Object[" + i + "]");
				// read prefabs asset id
				string assetId = reader.ReadString();
				// instantiate resources
				RuntimeTools.PersistableObject o = null;
				if (version >= 23)
				{
					o = RuntimeTools.PersistableObject.InstantiateFromRessources(resourcesLoader, assetId, TransformObject);
				}
				else
				{
					o = RuntimeTools.PersistableObject.InstantiateFromRessources(null, assetId, TransformObject);
				}
				// and load data
				o.Load(reader, assetId, version);
				if (version >= 4)
				{
					// game object properties
					reader.ReadObjectStart("Properties");
					var active = reader.ReadBoolean();
					// apply properties
					o.gameObject.SetActive(active);
					// static position
					if (version >= 24)
					{
						var pose = o.GetComponent<HexMapNavigation.HexGridObjectPoseBase>();

						// static position
						var staticPosition = reader.ReadBoolean();
						if (pose != null)
							pose.StaticPosition = staticPosition;
						// place holder
						var IsPlaceHolder = reader.ReadInt();
						reader.ReadObjectEnd();

						// if place Holder delected the load place holder
						if (IsPlaceHolder > 0)
						{
							RuntimeTools.IPersistableObject placeHolder = o.GetComponent<RuntimeTools.IPersistableObject>();
							if (placeHolder == null)
							{
								// place holder detected; but there is no interface
								// probably the targeted gameobject is not loaded; and we have gameobject gizmo error as replacemment
								// so we inject the right script able to manage place holder function
								// the goal is to continue the loading without execption
								HexMapNavigation.HexPlaceHolder.AddComponent(o.gameObject, IsPlaceHolder);
								placeHolder = o.GetComponent<RuntimeTools.IPersistableObject>();
							}
							placeHolder.Load(reader, version);
						}
					}
					else
					{
						if (version >= 22)
						{
							var pose = o.GetComponent<HexMapNavigation.HexGridObjectPoseBase>();
							var staticPosition = reader.ReadBoolean();
							if (pose != null)
								pose.StaticPosition = staticPosition;
						}
						reader.ReadObjectEnd();

						// if place Holder delected the load place holder
						RuntimeTools.IPersistableObject placeHolder = o.GetComponent<RuntimeTools.IPersistableObject>();
						if (null != placeHolder)
						{
							placeHolder.Load(reader, version);
						}
					}
				}
				// end
				reader.ReadObjectEnd();
			}
			reader.ReadObjectEnd();

			return;
		}
	}
}