﻿using UnityEngine;

namespace codingVR.HexMapTerrain
{
	public struct	EdgeVertices2
	{
		// vertices along the edge

		/*
         *         v1      
         *          *
         *           * 
         *            *  
         *   center    * 
         *              *
         *               * 
         *                *
         *                 v2
         */

		public Vector3 v1, v2;

		public EdgeVertices2(Vector3 corner1, Vector3 corner2)
		{
			v1 = corner1;
			v2 = corner2;
		}
	}

}