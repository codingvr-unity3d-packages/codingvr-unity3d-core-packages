﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace codingVR.HexMapTerrain
{
	public class HexGridChunkGeometryPath : MonoBehaviour, IHexGridChunkGeometry
	{
		// all meshes
		[SerializeField]
		HexMesh terrain;

		/*
		 * 
		 * weights
		 * 
		 */

		static Color weights1 = new Color(1f, 0f, 0f);
		static Color weights2 = new Color(0f, 1f, 0f);
		static Color weights3 = new Color(0f, 0f, 1f);

		/*
		 *
		 * life cycle
		 * 
		 */

		void Awake()
		{
		}

		void LateUpdate()
		{
		}
		
		/*
		 * 
		 * test if position is conatined by this chunk at geometry level
		 * 
		 */

		public bool GeometryContains(Vector3 position)
		{
			HexMesh hexMesh = gameObject.GetComponentInChildren<HexMesh>();
			Bounds bounds = hexMesh.GetComponent<MeshFilter>().sharedMesh.bounds;
			Bounds bounds2d = new Bounds(new Vector3(bounds.center.x, 0.0f, bounds.center.z), new Vector3(bounds.size.x, 1.0f, bounds.size.z));
			Vector3 position2d = new Vector3(position.x, 0.5f, position.z);
			bool contains = false;
			if (bounds2d.Contains(position2d) == true)
			{
				contains = terrain.GeometryContains(position);
			}
			return contains;
		}

		/*
		 * 
		 * tessellate
		 * 
		 */

		public void Triangulate(IHexEnvironmentManager environment, IHexCell[] cells)
		{
			terrain.Clear();
			for (int i = 0; i < cells.Length; i++)
			{
				if (cells[i].TerrainData != null)
				{
					Triangulate(environment, cells[i]);
				}
			}
			terrain.Apply();
		}

		void Triangulate(IHexEnvironmentManager environment, IHexCell cell)
		{
			float maxY = float.MinValue;
			for (HexMapNavigation.HexDirection d = HexMapNavigation.HexDirection.NE; d <= HexMapNavigation.HexDirection.NW; d++)
			{
				float localMaxY = maxY;
				Triangulate(environment, d, cell, out localMaxY);
				if (localMaxY > maxY)
					maxY = localMaxY;
			}
			Vector3 pos = cell.LocalPosition;
			pos.y = maxY;
			cell.LocalPosition = pos;

		}

		// we triangulate one direction
		void Triangulate(IHexEnvironmentManager environment, HexMapNavigation.HexDirection direction, IHexCell cell, out float localMaxY)
		{
			// create 4 vertex along the edge between direction & direction+1
			Vector3 center = cell.LocalPosition;
			EdgeVertices2 e = new EdgeVertices2( center + HexMetrics.GetFirstCorner(direction),  center + HexMetrics.GetSecondCorner(direction) );
			bool done = TriangulateEdgeFan(center, e, cell.Index, out localMaxY);
			if (done == false)
			{
				// each cell is reponsible to manage connection for the first directions
				IHexCell neighbor = cell.GetNeighbor(direction) as IHexCell;
				if (direction <= HexMapNavigation.HexDirection.SE || neighbor == null || neighbor.TerrainData == null)
				{
					TriangulateConnection(environment, neighbor, cell, e);
				}
			}
		}

		// === triangulate all connection between hew cells ===

		// main function

		void TriangulateConnection(IHexEnvironmentManager environment, IHexCell neighbor, IHexCell cell, EdgeVertices2 e1)
		{
			void Triangulate(float neighborY, float index)
			{
				if (Mathf.Approximately(neighborY, cell.LocalPosition.y))
				{
					return;
				}

				// bridge eq to the normal direction to the edge
				Vector3 bridge = Vector3.zero;
				// offset between the elevation from 2 cells
				bridge.y = neighborY - cell.LocalPosition.y;
				// we create new edge 
				/*              0
				 *             ***
				 *          **     **   e1    e2
				 *      5**           **1 *** 1
				 *      *               *     *
				 *      *               *     *
				 *      *               *     *
				 *      4**           **2 *** 2
				 *          **     **   
				 *             ***
				 *              3
				 */
				EdgeVertices2 e2 = new EdgeVertices2(e1.v1 + bridge, e1.v2 + bridge);
				// one strip
				TriangulateEdgeStrip(e1, weights1, cell.Index, e2, weights2, index);
			}

			// get neighbor according to direction
			if (neighbor == null)
			{
				Triangulate(0.0f, cell.Index);
			}
			else
			{
				Triangulate(neighbor.LocalPosition.y, neighbor.Index);
			}
		}

		bool TriangulateEdgeFan(Vector3 center, EdgeVertices2 edge, float index, out float maxY)
		{
			bool bRaycastDone = terrain.AddFallingTriangleUnperturbed(center, edge.v1, edge.v2, out maxY);
			Vector3 indices;
			indices.x = indices.y = indices.z = index;
			terrain.AddTriangleCellData(indices, weights1);
			return bRaycastDone;
		}

		void TriangulateEdgeStrip(EdgeVertices2 e1, Color w1, float index1, EdgeVertices2 e2, Color w2, float index2)
		{
			terrain.AddQuadUnperturbed(e1.v1, e1.v2, e2.v1, e2.v2);
			Vector3 indices;
			indices.x = indices.z = index1;
			indices.y = index2;
			terrain.AddQuadCellData(indices, w1, w2);
		}
	}
}