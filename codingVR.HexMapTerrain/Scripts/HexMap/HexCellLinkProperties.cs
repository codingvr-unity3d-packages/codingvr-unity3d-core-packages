﻿namespace codingVR.HexMapTerrain
{
	// === HexCellLinkProperties implementation ===============================================================

	public class HexCellLinkProperties : HexMapNavigation.IHexCellLinkProperties
	{
		// === HexCellLinkProperties ===

		private HexCellLinkProperties(Data.Tag tag)
		{
			this.Tag = tag;
		}

		private HexCellLinkProperties()
		{}

		public static HexCellLinkProperties Create(Data.Tag tag)
		{
			return new HexCellLinkProperties(tag);
		}

		public static HexCellLinkProperties Load(Core.GameDataReader reader, int version)
		{
			var hexCellSingleLink = new HexCellLinkProperties();
			hexCellSingleLink.LoadInternal(reader, version);
			return hexCellSingleLink;
		}

		// === Save / load ===

		public void Save(Core.GameDataWriter writer)
		{
			writer.WriteObjectStart("Link");
			Tag.Save(writer);
			writer.WriteObjectEnd();

			writer.WriteObjectStart("PathProperties");
			writer.Write(ThruUnderWater, "thruUnderWater");
			writer.Write(ThruRiver, "thruUnit");
			writer.Write(ThruNotExplorable, "thruNotExplorable");
			writer.Write(ThruPlant, "thruPlant");
			writer.WriteObjectEnd();

			writer.WriteObjectStart("PathMode");
			writer.Write(Waypoints, "wayPoints");
			writer.WriteObjectEnd();
		}

		private void LoadInternal(Core.GameDataReader reader, int version)
		{
			if (version >= 14)
			{
				reader.ReadObjectStart("Link");
				Tag = Data.Tag.Load(reader, version);
				reader.ReadObjectEnd();

				reader.ReadObjectStart("PathProperties");
				ThruUnderWater = reader.ReadBoolean();
				ThruRiver = reader.ReadBoolean();
				ThruNotExplorable = reader.ReadBoolean();
				ThruPlant = reader.ReadBoolean();
				reader.ReadObjectEnd();

				reader.ReadObjectStart("PathMode");
				Waypoints = reader.ReadBoolean();
				reader.ReadObjectEnd();
			}
			else
			{
				reader.ReadObjectStart("Link");
				Tag = Data.Tag.Load(reader, version);
				ThruUnderWater = reader.ReadBoolean();
				ThruRiver = reader.ReadBoolean();
				ThruNotExplorable = reader.ReadBoolean();
				ThruPlant = reader.ReadBoolean();
				reader.ReadObjectEnd();
			}
		}

		public Data.Tag Tag { get; set; }
		public bool ThruUnderWater { get; set; } = true;
		public bool ThruRiver { get; set; } = true;
		public bool ThruNotExplorable { get; set; } = false;
		public bool ThruPlant { get; set; } = true;
		public bool Waypoints { get; set; } = false;

		public bool IsValidDestination(HexMapNavigation.IHexCellOrigin cellOrigin)
		{
			var cell = cellOrigin as IHexCell;

			// in way point path mode the destnation is already valid
			bool b = Waypoints;
			if (cell.TerrainData != null && b == false)
			{
				b = (cell.Explorable || ThruNotExplorable) && (!cell.TerrainData.IsUnderwater || ThruUnderWater) && (cell.TerrainData.PlantLevel == 0 || ThruPlant) && (!cell.TerrainData.HasRiver || ThruRiver);
			}
			return b;
		}
	}
}