﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace codingVR.HexMapTerrain
{
	public class HexEnvironmentFeatures : MonoBehaviour, IHexEnvironmentFeatures
	{
		/*
		 * 
		 * wall bridge ...
		 * 
		 */

		[SerializeField]
		HexMesh walls;
		[SerializeField]
		Transform wallTower, bridge;
		[SerializeField]
		Transform[] special;

		/*
		 * 
		 * features data
		 * 
		 */

		[SerializeField]
		HexFeatureCollection[] urbanCollections, farmCollections, plantCollections;

		enum FeatureType
		{
			none = -1,
			plant = 0,
			urban = 1,
			farm = 2,
			count
		};


		Transform featuresContainer;

		/*
		 * 
		 * deferred features
		 * features are recorded then applied at the end
		 * 
		 */

		Dictionary<IHexCell, FeaturePool> defferedFeatures = new Dictionary<IHexCell, FeaturePool>();

		class FeaturePool : codingVR.Patterns.ObjectBase
		{
			List<Vector3> positions;
			int[] featureRemainingCount = new int[(int)FeatureType.count];

			void InitFeatureRemaining()
			{
				featureRemainingCount[(int)FeatureType.plant] = HexMetrics.maxPlantCountByCell;
				featureRemainingCount[(int)FeatureType.urban] = HexMetrics.maxUrbanCountByCell;
				featureRemainingCount[(int)FeatureType.farm] = HexMetrics.maxFarmCountByCell;
			}

			public void Init()
			{
				InitFeatureRemaining();
				positions.Clear();
			}

			public FeaturePool()
			{
				Core.RandomContinuous.InitState(17);
				InitFeatureRemaining();
				positions =  Patterns.ListPool<Vector3>.Get();
			}

			public void ApplyFeatures(HexEnvironmentFeatures features, IHexCell cell)
			{
				// choose randomly the feature within the distribution
				while (positions.Count > 0)
				{
					int v = Core.RandomContinuous.Range(0, positions.Count - 1);
					features.AddDefferedFeature(cell, positions[v], featureRemainingCount);
					positions.RemoveAt(v);
				}
			}

			public void AddPosition(Vector3 position)
			{
				positions.Add(position);
			}
		}

		void ApplyDeferredFeatures()
		{
			foreach (var it in defferedFeatures)
			{
				it.Value.ApplyFeatures(this, it.Key);
			}
			defferedFeatures.Clear();
		}
						
		/*
		 * 
		 * lifecycle
		 * 
		 */

		void Awake()
		{
			
		}
			   
		/*
		 * 
		 * Data
		 * 
		 */
		 
		public void Clear()
		{
			// manage features container construction
			if (featuresContainer)
			{
				Destroy(featuresContainer.gameObject);
			}
			featuresContainer = new GameObject("Features Container").transform;
			featuresContainer.SetParent(transform, false);

			// wall cleanup
			walls.Clear();
		}


		// Apply cached modifciation
		public void Apply()
		{
			// apply walls
			walls.Apply();

			// apply deferred features
			ApplyDeferredFeatures();
		}

		/*
		 * 
		 * Features management
		 * 
		 */

		Transform PickFeaturePrefab(HexFeatureCollection[] collection, int level, float hash, float choice)
		{
			if (level > 0)
			{
				float[] thresholds = HexMetrics.GetFeatureThresholds(level - 1);
				for (int i = 0; i < thresholds.Length; i++)
				{
					if (hash < thresholds[i])
					{
						return collection[i].Pick(choice);
					}
				}
			}
			return null;
		}
		
		bool AddDefferedFeature(IHexCell cell, Vector3 position, int[] remainingFeature)
		{
			if (cell.TerrainData.IsSpecial)
			{
				return false;
			}

			HexMapNavigation.HexHash hash = HexMetrics.SampleHashGrid(position);

			Transform CreateInstance(Transform _prefab, bool _scale)
			{
				Transform instance = Instantiate(_prefab);
				instance.localPosition = HexMetrics.Perturb(position);
				instance.localRotation = Quaternion.Euler(0f, 360f * hash.e, 0f);
				if (_scale)
					instance.localScale = Vector3.one * HexMapNavigation.HexMetrics.scale;
				instance.SetParent(featuresContainer, false);
				return instance;
			}

			bool isFeatureAvailable(FeatureType type)
			{
				bool valid = false;
				if (remainingFeature[(int)type] > 0)
				{
					remainingFeature[(int)type]--;
					valid = true;
				}
				return valid;
			}

			Transform prefab = PickFeaturePrefab(urbanCollections, cell.TerrainData.UrbanLevel, hash.a, hash.d);
			Transform otherPrefab = PickFeaturePrefab(farmCollections, cell.TerrainData.FarmLevel, hash.b, hash.d);
			float usedHash = hash.a;
			bool scale = true;
			if (prefab)
			{
				if (otherPrefab && hash.b < hash.a)
				{
					if (isFeatureAvailable(FeatureType.farm) == true)
					{
						prefab = otherPrefab;
						usedHash = hash.b;
					}
					else
					{
						return false; 
					}
				}
				else
				{
					if (isFeatureAvailable(FeatureType.urban) == false)
					{
						return false;
					}
				}
			}
			else if (otherPrefab)
			{
				if (isFeatureAvailable(FeatureType.farm) == true)
				{
					prefab = otherPrefab;
					usedHash = hash.b;
				}
			}
			otherPrefab = PickFeaturePrefab(plantCollections, cell.TerrainData.PlantLevel, hash.c, hash.d);
			if (prefab)
			{
				if (otherPrefab && hash.c < usedHash)
				{
					if (isFeatureAvailable(FeatureType.plant) == true)
					{
						prefab = otherPrefab;
						scale = false;
					}
					else
					{
						return false;
					}
				}
			}
			else if (otherPrefab)
			{
				if (isFeatureAvailable(FeatureType.plant) == true)
				{
					prefab = otherPrefab;
					scale = false;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}

			CreateInstance(prefab, scale);
			return true;
		}

		public void AddFeature(IHexCell cell, Vector3 position)
		{
			if (defferedFeatures.ContainsKey(cell) == false)
			{
				defferedFeatures.Add(cell, Patterns.Object<FeaturePool>.Get());
			}
			defferedFeatures[cell].AddPosition(position);
		}
				
		public void AddSpecialFeature(IHexCell cell, Vector3 position)
		{
			HexMapNavigation.HexHash hash = HexMetrics.SampleHashGrid(position);
			Transform instance = Instantiate(special[cell.TerrainData.SpecialIndex - 1]);
			instance.localPosition = HexMetrics.Perturb(position);
			instance.localRotation = Quaternion.Euler(0f, 360f * hash.e, 0f);
			instance.localScale = Vector3.one * HexMapNavigation.HexMetrics.scale;
			instance.SetParent(featuresContainer, false);
		}

		/*
		 * 
		 * Bridge management
		 * 
		 */

		public void AddBridge(Vector3 roadCenter1, Vector3 roadCenter2)
		{
			roadCenter1 = HexMetrics.Perturb(roadCenter1);
			roadCenter2 = HexMetrics.Perturb(roadCenter2);
			Transform instance = Instantiate(bridge);
			instance.localPosition = (roadCenter1 + roadCenter2) * 0.5f;
			instance.forward = roadCenter2 - roadCenter1;
			float length = Vector3.Distance(roadCenter1, roadCenter2);
			instance.localScale = new Vector3(1f, 1f, length * (1f / HexMetrics.bridgeDesignLength)) * HexMapNavigation.HexMetrics.scale;
			instance.SetParent(featuresContainer, false);
		}

		/*
		 * 
		 * Wall management
		 * 
		 */

		public void AddWall(EdgeVertices5 near, IHexCell nearCell, EdgeVertices5 far, IHexCell farCell, bool hasRiver, bool hasRoad)
		{
			if (nearCell.TerrainData.Walled != farCell.TerrainOrDefaultData().Walled && !nearCell.TerrainData.IsUnderwater && !farCell.TerrainOrDefaultData().IsUnderwater &&  nearCell.TerrainData.GetEdgeType(farCell) != HexEdgeType.Cliff)
			{
				AddWallSegment(near.v1, far.v1, near.v2, far.v2);
				if (hasRiver || hasRoad)
				{
					AddWallCap(near.v2, far.v2);
					AddWallCap(far.v4, near.v4);
				}
				else
				{
					AddWallSegment(near.v2, far.v2, near.v3, far.v3);
					AddWallSegment(near.v3, far.v3, near.v4, far.v4);
				}
				AddWallSegment(near.v4, far.v4, near.v5, far.v5);
			}
		}

		public void AddWall(IHexCell centerCell, Vector3 c1, IHexCell cell1, Vector3 c2, IHexCell cell2, Vector3 c3, IHexCell cell3)
		{
			if (cell1.TerrainOrDefaultData().Walled)
			{
				if (cell2.TerrainData.Walled)
				{
					if (!cell3.TerrainData.Walled)
					{
						AddWallSegment(c3, cell3, c1, cell1, c2, cell2);
					}
				}
				else if (cell3.TerrainOrDefaultData().Walled)
				{
					AddWallSegment(c2, cell2, c3, cell3, c1, cell1);
				}
				else
				{
					AddWallSegment(c1, cell1, c2, cell2, c3, cell3);
				}
			}
			else if (cell2.TerrainOrDefaultData().Walled)
			{
				if (cell3.TerrainData.Walled)
				{
					AddWallSegment(c1, cell1, c2, cell2, c3, cell3);
				}
				else
				{
					AddWallSegment(c2, cell2, c3, cell3, c1, cell1);
				}
			}
			else if (cell3.TerrainOrDefaultData().Walled)
			{
				AddWallSegment(c3, cell3, c1, cell1, c2, cell2);
			}
		}

		void AddWallSegment(Vector3 nearLeft, Vector3 farLeft, Vector3 nearRight, Vector3 farRight, bool addTower = false)
		{
			nearLeft = HexMetrics.Perturb(nearLeft);
			farLeft = HexMetrics.Perturb(farLeft);
			nearRight = HexMetrics.Perturb(nearRight);
			farRight = HexMetrics.Perturb(farRight);

			// far left        far right
			// -------------------------
			// *                       *
			// -------------------------
			// near left      near right


			// compute middle of wall
			Vector3 left = HexMetrics.WallLerp(nearLeft, farLeft);
			Vector3 right = HexMetrics.WallLerp(nearRight, farRight);

			// far left        far right
			// -------------------------
			// * left            right *
			// -------------------------
			// near left      near right

			// compute vector middle to border
			Vector3 leftThicknessOffset = HexMetrics.WallThicknessOffset(nearLeft, farLeft);
			Vector3 rightThicknessOffset = HexMetrics.WallThicknessOffset(nearRight, farRight);

			// compute wall ceiling
			float leftTop = left.y + HexMetrics.wallHeight;
			float rightTop = right.y + HexMetrics.wallHeight;

			Vector3 v1, v2, v3, v4;
			v1 = v3 = left - leftThicknessOffset;
			v2 = v4 = right - rightThicknessOffset;
			v3.y = leftTop;
			v4.y = rightTop;

			// -------------------------
			// * left            right *
			// -------------------------
			// v1 v3              v2 v4
			//
			// v3---------------------v4
			// *                       *  
			// v1---------------------v2


			// bottom side
			walls.AddQuadUnperturbed(v1, v2, v3, v4);

			Vector3 t1 = v3, t2 = v4;

			v1 = v3 = left + leftThicknessOffset;
			v2 = v4 = right + rightThicknessOffset;
			v3.y = leftTop;
			v4.y = rightTop;

			// v1 v3              v2 v4
			// -------------------------
			// * left            right *
			// -------------------------
			//    t1                 t2 

			// v3---------------------v4
			// *                       *  
			// v1---------------------v2


			// top side
			walls.AddQuadUnperturbed(v2, v1, v4, v3);

			// ceiling side
			walls.AddQuadUnperturbed(t1, t2, v3, v4);

			if (addTower)
			{
				Transform towerInstance = Instantiate(wallTower);
				towerInstance.transform.localPosition = (left + right) * 0.5f;
				towerInstance.transform.localScale = Vector3.one * HexMapNavigation.HexMetrics.scale;
				Vector3 rightDirection = right - left;
				rightDirection.y = 0f;
				towerInstance.transform.right = rightDirection;
				towerInstance.SetParent(featuresContainer, false);
			}
		}

		void AddWallSegment(Vector3 pivot, IHexCell pivotCell, Vector3 left, IHexCell leftCell, Vector3 right, IHexCell rightCell)
		{
			if (pivotCell.TerrainData.IsUnderwater)
			{
				return;
			}

			bool hasLeftWall = !leftCell.TerrainData.IsUnderwater && pivotCell.TerrainData.GetEdgeType(leftCell) != HexEdgeType.Cliff;
			bool hasRighWall = !rightCell.TerrainData.IsUnderwater && pivotCell.TerrainData.GetEdgeType(rightCell) != HexEdgeType.Cliff;

			if (hasLeftWall)
			{
				if (hasRighWall)
				{
					bool hasTower = false;
					if (leftCell.TerrainData.Elevation == rightCell.TerrainData.Elevation)
					{
						HexMapNavigation.HexHash hash = HexMetrics.SampleHashGrid(
							(pivot + left + right) * (1f / 3f)
						);
						hasTower = hash.e < HexMetrics.wallTowerThreshold;
					}
					AddWallSegment(pivot, left, pivot, right, hasTower);
				}
				else if (leftCell.TerrainData.Elevation < rightCell.TerrainData.Elevation)
				{
					AddWallWedge(pivot, left, right);
				}
				else
				{
					AddWallCap(pivot, left);
				}
			}
			else if (hasRighWall)
			{
				if (rightCell.TerrainData.Elevation < leftCell.TerrainData.Elevation)
				{
					AddWallWedge(right, pivot, left);
				}
				else
				{
					AddWallCap(right, pivot);
				}
			}
		}

		void AddWallCap(Vector3 near, Vector3 far)
		{
			near = HexMetrics.Perturb(near);
			far = HexMetrics.Perturb(far);

			Vector3 center = HexMetrics.WallLerp(near, far);
			Vector3 thickness = HexMetrics.WallThicknessOffset(near, far);

			Vector3 v1, v2, v3, v4;
			v1 = v3 = center - thickness;
			v2 = v4 = center + thickness;
			v3.y = v4.y = center.y + HexMetrics.wallHeight;
			walls.AddQuadUnperturbed(v1, v2, v3, v4);
		}

		void AddWallWedge(Vector3 near, Vector3 far, Vector3 point)
		{
			near = HexMetrics.Perturb(near);
			far = HexMetrics.Perturb(far);
			point = HexMetrics.Perturb(point);

			Vector3 center = HexMetrics.WallLerp(near, far);
			Vector3 thickness = HexMetrics.WallThicknessOffset(near, far);

			Vector3 v1, v2, v3, v4;
			Vector3 pointTop = point;
			point.y = center.y;

			v1 = v3 = center - thickness;
			v2 = v4 = center + thickness;
			v3.y = v4.y = pointTop.y = center.y + HexMetrics.wallHeight;

			walls.AddQuadUnperturbed(v1, point, v3, pointTop);
			walls.AddQuadUnperturbed(point, v2, pointTop, v4);
			walls.AddTriangleUnperturbed(pointTop, v3, v4);
		}
	}
}