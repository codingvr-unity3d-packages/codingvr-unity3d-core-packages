﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Zenject;

namespace codingVR.HexMapTerrain
{
	public class HexEnvironmentManager : MonoBehaviour, IHexEnvironmentManager
	{
		/*
		 * 
		 * ctor
		 * 
		 */

		Scene.IResourcesLoader resourcesLoader;

		// DIP
		[Inject]
		public void Construct(Scene.IResourcesLoader resourcesLoader)
		{
			this.resourcesLoader = resourcesLoader;
		}
		
		/*
		 * 
		 * features
		 * 
		 */

		HexEnvironmentFeatures features;

		public IHexEnvironmentFeatures Features
		{
			get
			{

				return features;
			}
		}

		/*
		 * 
		 * containers
		 * 
		 */


		HexEnvironementContainer[] Containers = new HexEnvironementContainer[(int)HexMapNavigation.HexEnvironmentManagerIDs.CountContainer];

		// Get container
		HexEnvironementContainer Container(HexMapNavigation.HexEnvironmentManagerIDs idx)
		{
			return Containers[(int)idx];
		}

		// Moving object inside all containers
		public void MovingTo(IHexCell cell)
		{
			foreach (var container in Containers)
			{
				container.MovingTo(cell);
			}
		}

		// make child of environment
		// containerIdx :  see HexEnvironmentManager.***ContainerIdx
		public void MakeChildOfChunkEnvironement(HexMapNavigation.HexEnvironmentManagerIDs containerIdx, Transform child)
		{
			var transform = Container(containerIdx).TransformObject;
			child.SetParent(transform, true);
		}

		// destroy child identified by childIdentifier
		// containerIdx :  see HexEnvironmentManager.***ContainerIdx
		public bool DestroyChildOfChunkEnvironement(HexMapNavigation.HexEnvironmentManagerIDs containerIdx, string childIdentifier)
		{
			return Container(containerIdx).DestroyChild(childIdentifier);
		}

		// find child identified by childIdentifier
		// containerIdx :  see HexEnvironmentManager.***ContainerIdx
		public Transform FindChildOfChunkEnvironement(HexMapNavigation.HexEnvironmentManagerIDs containerIdx, string childIdentifier)
		{
			return Container(containerIdx).FindChild(childIdentifier);
		}

		// find childs identified by childFromParentIdentifier
		// containerIdx :  see HexEnvironmentManager.***ContainerIdx
		public List<Transform> FindChildsOfChunkEnvironement(HexMapNavigation.HexEnvironmentManagerIDs containerIdx, string childFromParentIdentifier)
		{
			return Container(containerIdx).FindChilds(childFromParentIdentifier);
		}

		// find childs
		// containerIdx :  see HexEnvironmentManager.***ContainerIdx
		public List<Transform> FindChildsOfChunkEnvironement(HexMapNavigation.HexEnvironmentManagerIDs containerIdx)
		{
			return Container(containerIdx).FindChilds();
		}

		/*
		 * 
		 * Features
		 * 
		 */

		public void ClearFeatures()
		{
			features?.Clear();
		}

		// apply cached modification
		public void ApplyFeatures()
		{
			features?.Apply();
		}


		/*
		 * 
		 * Environment life cycle
		 * 
		 */

		void Awake()
		{
			// get features
			features = GetComponent<HexEnvironmentFeatures>();
			
			/* persistent container */
			// manage props container construction
			Containers[(int)HexMapNavigation.HexEnvironmentManagerIDs.PropsContainerIdx] = new HexEnvironementContainer(transform, "Props Container", "HexGridObjectsContainer");

			// manage functionals container construction
			Containers[(int)HexMapNavigation.HexEnvironmentManagerIDs.FunctionsContainerIdx] = new HexEnvironementContainer(transform, "Functions Container", "HexGridObjectsContainer");

			// manage functionals container construction
			Containers[(int)HexMapNavigation.HexEnvironmentManagerIDs.CharactersContainerIdx] = new HexEnvironementContainer(transform, "Characters Container", "HexGridObjectsContainer");
		}

		/*
		 * 
		 * Save & Load
		 * 
		 */

		public void Save(codingVR.Core.GameDataWriter writer)
		{
			// Chunk object start
			writer.WriteObjectStart("Chunk-Environment");

			// Save props container
			Containers[(int)HexMapNavigation.HexEnvironmentManagerIDs.PropsContainerIdx].Save(writer, "Environment-Props");

			// Save functionals container
			Containers[(int)HexMapNavigation.HexEnvironmentManagerIDs.FunctionsContainerIdx].Save(writer, "Environment-Functions");

			// Save functionals container
			Containers[(int)HexMapNavigation.HexEnvironmentManagerIDs.CharactersContainerIdx].Save(writer, "Environment-Characters");

			// Chunk object end
			writer.WriteObjectEnd();
		}

		public void Load(codingVR.Core.GameDataReader reader, int version)
		{
			if (version >= 1.0)
			{
				// Environment
				reader.ReadObjectStart("Chunk-Environment");

				// Load props
				Containers[(int)HexMapNavigation.HexEnvironmentManagerIDs.PropsContainerIdx].Load(reader, resourcesLoader, version, "Environment-Props");

				// Load functionals
				if (version >= 6.0)
				{
					Containers[(int)HexMapNavigation.HexEnvironmentManagerIDs.FunctionsContainerIdx].Load(reader, resourcesLoader, version, "Environment-Functions");
				}

				// Load functionals
				if (version >= 7.0)
				{
					Containers[(int)HexMapNavigation.HexEnvironmentManagerIDs.CharactersContainerIdx].Load(reader, resourcesLoader, version, "Environment-Characters");
				}

				// Environment
				reader.ReadObjectEnd();
			}
			return;
		}
	}
}