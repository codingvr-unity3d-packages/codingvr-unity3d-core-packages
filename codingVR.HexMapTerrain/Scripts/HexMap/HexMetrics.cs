﻿using UnityEngine;

namespace codingVR.HexMapTerrain
{
	public static class HexMetrics
	{
		// max features by cell
		public static int maxPlantCountByCell = 7;
		public static int maxUrbanCountByCell = 7;
		public static int maxFarmCountByCell = 7;
		
		/*
		 *  terracesPerSlope = 2 => 4 strips     
		 *  terraceSteps = 5  => 5 strips
		 *  
		 *        _/  
		 *      _/
		 *     /
		 *     
		 */

		public const int terracesPerSlope = 4; // count of terrace
		public const int terraceSteps = terracesPerSlope * 2 + 1; // count of steps (2 by terrace)
		public const float horizontalTerraceStepSize = 1f / terraceSteps;
		public const float verticalTerraceStepSize = 1f / (terracesPerSlope + 1);

		// hex cell slope
		public const float solidFactor = 0.7f;
		public const float blendFactor = 1f - solidFactor;

		// cell config
		public static float cellPerturbStrength => 2.0f * HexMapNavigation.HexMetrics.scale;

		public const float streamBedElevationOffset = -1.75f;

		// hex cell water
		public const float waterFactor = 0.6f;
		public const float waterBlendFactor = 1f - waterFactor;
		public const float waterElevationOffset = -0.5f;

		// hex cell wall
		public static float wallHeight => 4f * HexMapNavigation.HexMetrics.scale;
		public static float wallYOffset => -1f * HexMapNavigation.HexMetrics.scale;
		public static float wallThickness => 0.75f * HexMapNavigation.HexMetrics.scale;
		public const float wallElevationOffset = verticalTerraceStepSize;
		public const float wallTowerThreshold = 0.5f;

		public const float bridgeDesignLength = 7f;

		// hash
		public static float noiseScale => 0.003f / HexMapNavigation.HexMetrics.scale;
		public const int hashGridSize = 256;
		public const float hashGridScale = 0.25f;

		
		static HexMapNavigation.HexHash[] hashGrid;


		static float[][] featureThresholds = 
		{
			new float[] {0.0f, 0.0f, 0.4f},
			new float[] {0.0f, 0.4f, 0.6f},
			new float[] {0.4f, 0.6f, 0.8f}
		};

		public static Texture2D noiseSource;

		public static Vector4 SampleNoise(Vector3 position)
		{
			Vector4 sample = noiseSource.GetPixelBilinear(position.x * noiseScale, position.z * noiseScale);

			if (HexMapNavigation.HexMetrics.Wrapping && position.x < HexMapNavigation.HexMetrics.innerDiameter * 1.5f)
			{
				Vector4 sample2 = noiseSource.GetPixelBilinear(
					(position.x + HexMapNavigation.HexMetrics.wrapSize * HexMapNavigation.HexMetrics.innerDiameter) * noiseScale,
					position.z * noiseScale
				);
				sample = Vector4.Lerp(
					sample2, sample, position.x * (1f / HexMapNavigation.HexMetrics.innerDiameter) - 0.5f
				);
			}

			return sample;
		}

		public static void InitializeHashGrid(int seed)
		{
			hashGrid = new HexMapNavigation.HexHash[hashGridSize * hashGridSize];
			Random.State currentState = Random.state;
			Random.InitState(seed);
			for (int i = 0; i < hashGrid.Length; i++)
			{
				hashGrid[i] = HexMapNavigation.HexHash.Create();
			}
			Random.state = currentState;
		}

		public static HexMapNavigation.HexHash SampleHashGrid(Vector3 position)
		{
			int x = (int)(position.x * hashGridScale) % hashGridSize;
			if (x < 0)
			{
				x += hashGridSize;
			}
			int z = (int)(position.z * hashGridScale) % hashGridSize;
			if (z < 0)
			{
				z += hashGridSize;
			}
			return hashGrid[x + z * hashGridSize];
		}

		public static float[] GetFeatureThresholds(int level)
		{
			return featureThresholds[level];
		}

		public static Vector3 GetFirstCorner(HexMapNavigation.HexDirection direction)
		{
			return HexMapNavigation.HexMetrics.corners[(int)direction];
		}

		public static Vector3 GetSecondCorner(HexMapNavigation.HexDirection direction)
		{
			return HexMapNavigation.HexMetrics.corners[(int)direction + 1];
		}

		public static Vector3 GetFirstSolidCorner(HexMapNavigation.HexDirection direction)
		{
			return HexMapNavigation.HexMetrics.corners[(int)direction] * solidFactor;
		}

		public static Vector3 GetSecondSolidCorner(HexMapNavigation.HexDirection direction)
		{
			return HexMapNavigation.HexMetrics.corners[(int)direction + 1] * solidFactor;
		}

		public static Vector3 GetSolidEdgeMiddle(HexMapNavigation.HexDirection direction)
		{
			return (HexMapNavigation.HexMetrics.corners[(int)direction] + HexMapNavigation.HexMetrics.corners[(int)direction + 1]) * (0.5f * solidFactor);
		}
		
		public static bool IsInsideHexCell(Vector3 point, Vector3 hexCenter)
		{
			bool b = point.IsInConvexPolygonXZ(hexCenter, HexMapNavigation.HexMetrics.corners);
			return b;
		}

		public static bool IsInsidePerturbedHexCell(Vector3 point, Vector3 hexCenter)
		{
			Vector3[] newCorners = new Vector3[HexMapNavigation.HexMetrics.corners.Length];
			for (int i = 0; i < HexMapNavigation.HexMetrics.corners.Length; i++)
				newCorners[i] = Perturb(HexMapNavigation.HexMetrics.corners[i] + hexCenter);
			bool b = point.IsInConvexPolygonXZ(newCorners);
			return b;
		}

		public static Vector3 GetFirstWaterCorner(HexMapNavigation.HexDirection direction)
		{
			return HexMapNavigation.HexMetrics.corners[(int)direction] * waterFactor;
		}

		public static Vector3 GetSecondWaterCorner(HexMapNavigation.HexDirection direction)
		{
			return HexMapNavigation.HexMetrics.corners[(int)direction + 1] * waterFactor;
		}
		// by adding 2 neighbors (consecutive) vectors we obtain the perpendicular vector to the edge (between the 2 vectors)
		public static Vector3 GetBridge(HexMapNavigation.HexDirection direction)
		{
			return (HexMapNavigation.HexMetrics.corners[(int)direction] + HexMapNavigation.HexMetrics.corners[(int)direction + 1]) *  blendFactor;  // (1.0 - solidFactor)
		}

		// by adding 2 neighbors (consecutive) vectors we obtain the perpendicular vector to the edge (between the 2 vectors)
		public static Vector3 GetNormalDirection(HexMapNavigation.HexDirection direction)
		{
			return (HexMapNavigation.HexMetrics.corners[(int)direction] + HexMapNavigation.HexMetrics.corners[(int)direction + 1]).normalized;
		}

		public static Vector3 GetWaterBridge(HexMapNavigation.HexDirection direction)
		{
			return (HexMapNavigation.HexMetrics.corners[(int)direction] + HexMapNavigation.HexMetrics.corners[(int)direction + 1]) * waterBlendFactor;
		}

		public static Vector3 TerraceLerp(Vector3 a, Vector3 b, int step)
		{
			float h = step * HexMetrics.horizontalTerraceStepSize;
			a.x += (b.x - a.x) * h;
			a.z += (b.z - a.z) * h;
			float v = ((step + 1) / 2) * HexMetrics.verticalTerraceStepSize;
			a.y += (b.y - a.y) * v;
			return a;
		}

		public static Color TerraceLerp(Color a, Color b, int step)
		{
			float h = step * HexMetrics.horizontalTerraceStepSize;
			return Color.Lerp(a, b, h);
		}

		public static Vector3 WallLerp(Vector3 near, Vector3 far)
		{
			near.x += (far.x - near.x) * 0.5f;
			near.z += (far.z - near.z) * 0.5f;
			float v =
				near.y < far.y ? wallElevationOffset : (1f - wallElevationOffset);
			near.y += (far.y - near.y) * v + wallYOffset;
			return near;
		}

		public static Vector3 WallThicknessOffset(Vector3 near, Vector3 far)
		{
			Vector3 offset;
			offset.x = far.x - near.x;
			offset.y = 0f;
			offset.z = far.z - near.z;
			return offset.normalized * (wallThickness * 0.5f);
		}

		public static HexEdgeType GetEdgeType(float elevation1, float elevation2)
		{
			if (elevation1 == elevation2)
			{
				return HexEdgeType.Flat;
			}
			var delta = Mathf.FloorToInt(Mathf.Abs(elevation2 - elevation1));
			if (delta == 1)
			{
				return HexEdgeType.Slope;
			}
			return HexEdgeType.Cliff;
		}

		public static Vector3 Perturb(Vector3 position)
		{
			Vector4 sample = SampleNoise(position);
			position.x += (sample.x * 2f - 1f) * cellPerturbStrength;
			position.z += (sample.z * 2f - 1f) * cellPerturbStrength;
			return position;
		}
	}
}