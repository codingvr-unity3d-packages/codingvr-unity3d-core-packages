﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.HexMapTerrain
{
	public class HexGridChunk : MonoBehaviour, IHexGridChunk, HexMapNavigation.IHexGridChunkOrigin
	{
		/*
		 * 
		 * ctor
		 * 
		 */

		public void Inject(DiContainer diContainer)
		{
			// instantiate and attach the component in once function 
			diContainer.Inject(EnvironmentRaw);
		}

		// cell list
		IHexCell[] cells;
		// canvas for text
		Canvas gridCanvas;


		/*
		 *
		 * gridcells
		 * 
		 */

		[SerializeField]
		GameObject gridCells;

		/*
		 *
		 * environment
		 * 
		 */

		[SerializeField]
		HexEnvironmentManager environmentLOD0;

		public IHexEnvironmentManager Environment => EnvironmentRaw;
		
		public HexEnvironmentManager EnvironmentRaw
		{
			get { return environmentLOD0; }
		}

		/*
		 *
		 * setup
		 * 
		 */

		int chunkSizeX = HexMapNavigation.HexMetrics.chunkSizeX;
		int chunkSizeZ = HexMapNavigation.HexMetrics.chunkSizeZ;

		public void Setup(int chunkSizeX, int chunkSizeZ)
		{
			this.chunkSizeX = chunkSizeX;
			this.chunkSizeZ = chunkSizeZ;
			// initialize hex cells for this chunk
			cells = new IHexCell[chunkSizeX * chunkSizeZ];
		}

		/*
		 *
		 * geometry
		 * 
		 */

		[SerializeField]
		GameObject geometryLOD0;

		IHexGridChunkGeometry Geometry { get; set; }

		/*
		 *
		 * life cycle
		 * 
		 */

		void Awake()
		{
			gridCanvas = GetComponentInChildren<Canvas>();
			Geometry = geometryLOD0.GetComponent<IHexGridChunkGeometry>();
		}

		void LateUpdate()
		{
			Triangulate();
			foreach (var it in cells)
			{
				MovingTo(it);
			}
			enabled = false;
		}

		/*
		 * 
		 * Refreshing chunk
		 * 
		 */

		public void Refresh()
		{
			// this component is enabled on Refresh 
			// so the function Update 
			enabled = true;
		}

		/*
		 * 
		 * UI
		 * 
		 */

		public void ShowUI(bool visible)
		{
#if !UNITY_EDITOR
			gridCanvas.gameObject.SetActive(visible);
#endif
		}

		/*
		 *
		 * cell management
		 * 
		 */

		public void AddCell(int index, HexMapNavigation.IHexCellOrigin cell)
		{
			// bind cell to chunk
			cells[index] = cell as IHexCell;
			// cell setup
			cell.AddCellToChunk(this, gridCells.transform, gridCanvas.transform);
		}

		/*
		 * 
		 * Environment
		 * 
		 */

		// environment moving
		void MovingTo(IHexCell cell)
		{
			if (cell.TerrainData != null)
			{
				if (cell.TerrainData.MovingCell.HasMagnitude())
				{
					// props must move
					EnvironmentRaw.MovingTo(cell);
					// cell has moved
					cell.TerrainData.MovingCell = Vector3.zero;
				}
			}
		}

		// make child of environment
		// containerIdx :  see HexEnvironmentManager.***ContainerIdx
		public void MakeChildOfChunkEnvironement(HexMapNavigation.HexEnvironmentManagerIDs containerIdx, Transform child)
		{
			Environment.MakeChildOfChunkEnvironement(containerIdx, child);
		}

		// destroy child identified by childIdentifier
		// containerIdx :  see HexEnvironmentManager.***ContainerIdx
		public bool DestroyChildOfChunkEnvironement(HexMapNavigation.HexEnvironmentManagerIDs containerIdx, string childIdentifier)
		{
			return Environment.DestroyChildOfChunkEnvironement(containerIdx, childIdentifier);
		}

		// find child identified by childIdentifier
		// containerIdx :  see HexEnvironmentManager.***ContainerIdx
		public Transform FindChildOfChunkEnvironement(HexMapNavigation.HexEnvironmentManagerIDs containerIdx, string childIdentifier)
		{
			return Environment.FindChildOfChunkEnvironement(containerIdx, childIdentifier);
		}

		// find child identified by childFromParentIdentifier
		// containerIdx :  see HexEnvironmentManager.***ContainerIdx
		public List<Transform> FindChildsOfChunkEnvironement(HexMapNavigation.HexEnvironmentManagerIDs containerIdx, string childFromParentID)
		{
			return Environment.FindChildsOfChunkEnvironement(containerIdx, childFromParentID);
		}

		// find childs
		// containerIdx :  see HexEnvironmentManager.***ContainerIdx
		public List<Transform> FindChildsOfChunkEnvironement(HexMapNavigation.HexEnvironmentManagerIDs containerIdx)
		{
			return Environment.FindChildsOfChunkEnvironement(containerIdx);
		}


		public Material TerrainMaterial
		{
			get
			{
				var go = geometryLOD0.transform.Find("Ground");
				List<Material> materials = new List<Material>();
				go.GetComponent<MeshRenderer>().GetSharedMaterials(materials);
				return materials[0];
			}
		}

		/*
		 * 
		 * Geometry
		 * 
		 */

		public void Triangulate()
		{
			EnvironmentRaw.ClearFeatures();
			Geometry.Triangulate(Environment, cells);
			EnvironmentRaw.ApplyFeatures();
		}

		public bool GeometryContains(Vector3 position)
		{
			return Geometry.GeometryContains(position);
		}

		/*
		 * 
		 * save / load
		 * 
		 */

		public void Save(codingVR.Core.GameDataWriter writer)
		{
			// environement
			EnvironmentRaw.Save(writer);
		}

		public void Load(codingVR.Core.GameDataReader reader, int version)
		{
			// environement
			EnvironmentRaw.Load(reader, version);
		}
	}
}