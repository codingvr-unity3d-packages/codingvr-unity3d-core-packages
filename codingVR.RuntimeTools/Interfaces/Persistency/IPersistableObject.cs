﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.RuntimeTools
{
    public interface IPersistableObject
    {
        void Save(codingVR.Core.GameDataWriter writer);
        void Load(codingVR.Core.GameDataReader reader, int version);
    }
}
