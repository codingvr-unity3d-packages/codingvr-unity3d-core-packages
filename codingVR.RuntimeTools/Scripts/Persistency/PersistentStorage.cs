﻿using System.IO;
using UnityEngine;

namespace codingVR.RuntimeTools
{
    public class PersistentStorage : MonoBehaviour
    {
        string savePath;
        string savePathJson;

        void Awake()
        {
            savePath = Path.Combine(Application.persistentDataPath, "saveFile.bin");
            savePathJson = Path.Combine(Application.persistentDataPath, "saveFile.json");
        }

        public void Save(PersistableObject o)
        {
            using (
                var writer = new BinaryWriter(File.Open(savePath, FileMode.Create))
            )
            {
                codingVR.Core.GameDataWriter gameDataWriter = new codingVR.Core.GameDataWriter(writer);
                gameDataWriter.Open();
                o.Save(gameDataWriter);
                gameDataWriter.Close();
            }

            {
                var writerJson = new LitJson.JsonWriter(new System.Text.StringBuilder());
                {
                    codingVR.Core.GameDataWriter gameDataWriter = new codingVR.Core.GameDataWriter(writerJson);
                    gameDataWriter.Open();
                    o.Save(gameDataWriter);
                    gameDataWriter.Close();

                }
                string json = writerJson.TextWriter.ToString();
                Debug.Log(json);

                using (
                    var writer = new StreamWriter(File.Open(savePathJson, FileMode.Create))
                )
                {
                    writer.Write(json);
                }
            }
        }

        public void Load(PersistableObject o)
        {
            /*
            using (
                var reader = new BinaryReader(File.Open(savePath, FileMode.Open))
            ) {
                o.Load(new GameDataReader(reader));
            }
            */

            {
                string json;
                using (var reader = new StreamReader(File.Open(savePathJson, FileMode.Open)))
                {
                    json = reader.ReadToEnd();
                }

                if (json.Length > 0)
                {
                    var readerJson = new LitJson.JsonReader(json);
                    var gameDataReader = new codingVR.Core.GameDataReader(readerJson);
                    gameDataReader.Open();
                    o.Load(gameDataReader);
                    gameDataReader.Close();
                }
            }
        }
    }
}