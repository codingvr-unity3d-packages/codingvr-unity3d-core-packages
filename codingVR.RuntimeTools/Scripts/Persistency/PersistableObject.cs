﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Zenject;

namespace codingVR.RuntimeTools
{
	[ExecuteInEditMode]
	public class PersistableObject : MonoBehaviour
	{
		class List
		{
			List<PersistableObject> list = new List<PersistableObject>();

			public List(PersistableObject p)
			{
				Add(p);
			}

			public void Add(PersistableObject p)
			{
				if (list.Count > 0)
				{
					var go = list[list.Count - 1];
					go.identifierDuplicated = true;
					p.identifierDuplicated = true;
				}
				else
				{
					p.identifierDuplicated = false;
				}
				list.Add(p);
			}

			public int Remove(PersistableObject p)
			{
				var f = list.Find(x => string.Compare(x.Identifier, p.Identifier) == 0 );
				if (f != null)
				{
					list.Remove(f);
					if (list.Count == 1)
					{
						list[0].identifierDuplicated = false;
					}
				}
				return list.Count;
			}
		};

		static Dictionary<string, List> identifierSet = new Dictionary<string, List>();

		/*
		 * 
		 * Unique Identifier
		 * 
		 */

		[SerializeField]
		[OnChangingCall("OnIdentifierChanging")]
		string identifier="";
		
		private bool identifierDuplicated = false;

		private void IdentifierSet(string newIdentifier)
		{
			// remove last identifier
			if (identifierSet.ContainsKey(identifier))
			{
				if (identifierSet[identifier].Remove(this) == 0)
				{
					identifierSet.Remove(identifier);
				}
			}

			// insert new identifier
			if (string.IsNullOrEmpty(newIdentifier) == false)
			{
				if (identifierSet.ContainsKey(newIdentifier) == false)
				{
					identifierSet.Add(newIdentifier, new List(this));
				}
				else
				{
					identifierSet[newIdentifier].Add(this);
					Debug.LogErrorFormat("{0} : identifier {1} is duplicated on game object {2}", System.Reflection.MethodBase.GetCurrentMethod().Name, newIdentifier, gameObject.name);
				}
			}
			identifier = newIdentifier;
		}

		public string Identifier
		{
			get => identifier;
			set
			{
				if (value != identifier)
				{
					IdentifierSet(value);
				}
			}
		}

		public void OnIdentifierChanging(string identifier)
		{
			//IdentifierSet(identifier);
			Identifier = identifier;
		}
		
		public bool MakeIdentifier(bool forceNewOne = false)
		{
			bool b = false;
			if (identifier == "" || forceNewOne == true)
			{
				Identifier = System.Guid.NewGuid().ToString();
				b = true;
			}
			return b;
		}

		[ReadOnly]
		[SerializeField]
		string identifierParent;
		public string IdentifierParent { get => identifierParent; set => identifierParent = value; }

		/* 
		 * 
		 * Asset ressources path
		 * 
		 */

		[ReadOnly]
		[SerializeField]
		string assetRessourcesPath;

		public string AssetRessourcesPath
		{
			get => assetRessourcesPath;
			set => assetRessourcesPath = value;
		}

		// how to find a gameobjects prefab during runtime
		// https://stackoverflow.com/questions/53992675/how-to-find-a-gameobjects-prefab-during-runtime-with-unity-version-2018-3
		// This function is called when the script is loaded or a value is changed in the Inspector(Called in the editor only).
		void OnValidate() // Which will be called on build or editor play
		{
#if UNITY_EDITOR
			// Deposit UnityEditor API dependent into some field
			// asset resources path
			//if (assetRessourcesPath.Length == 0)
			{
				var prefab = PrefabUtilityExtension.GetCorrespondingObjectFromSource(gameObject);
				if (prefab != null)
					assetRessourcesPath = ResourcesExtensions.MakeResourcesPathFromAssetPath(UnityEditor.AssetDatabase.GetAssetPath(prefab));
			}

			if (Event.current != null && Event.current.commandName != null)
			{
				if (
					Event.current.commandName == "Duplicate" || 
					Event.current.commandName == "Paste"
					)
				{
					MakeIdentifier(true);
				}
				else
				{
					MakeIdentifier();
				}
			}
			else
			{
				MakeIdentifier();
			}
#endif
		}

		void drawGizmo(int scale, FontStyle fontStyle)
		{
			if (identifierDuplicated == true)
			{
				var text = string.Format("{0}\nID '{1}' duplicated", name, Identifier);
				DrawGizmos.DrawString(text, transform.position, scale, Color.red, fontStyle);
			}
		}

		void OnDrawGizmos()
		{
#if UNITY_EDITOR
			if (Selection.activeGameObject == null || Selection.activeGameObject.GetInstanceID() != gameObject.GetInstanceID())
			{
				drawGizmo(25, FontStyle.Bold);
			}
#endif
		}

		void OnDrawGizmosSelected()
		{
			drawGizmo(40, FontStyle.BoldAndItalic);
		}

		void OnDestroy()
		{
			IdentifierSet("");
		}

		void Start()
		{
#if UNITY_EDITOR

			// asset resources path
			if (assetRessourcesPath.Length == 0)
			{
				var prefab = PrefabUtilityExtension.GetCorrespondingObjectFromSource(gameObject);
				assetRessourcesPath = ResourcesExtensions.MakeResourcesPathFromAssetPath(UnityEditor.AssetDatabase.GetAssetPath(prefab));
			}

			// Make identifier
			{
				if (MakeIdentifier() == false)
				{
					IdentifierSet(identifier);
				}
			}
#endif
		}

		/* 
		 * 
		 * Asset ressources instanting
		 * 
		 */

		static GameObject ErrorGizmo(string assetRessourcesPath, Transform parent)
		{
			var _prefab = Resources.Load("GameObjectDrawGizmosError");
			var _gameObject = GameObject.Instantiate(_prefab, parent) as GameObject;
			codingVR.Core.GameObjectDrawGizmos gameObjectError = _gameObject.GetComponent<codingVR.Core.GameObjectDrawGizmos>();
			gameObjectError.Title = "prefab\n" + assetRessourcesPath + "\nnot found or issue on setup!!!";
			_gameObject.AddComponent<PersistableObject>();
			return _gameObject;
		}

		static private PersistableObject SetupPersistableObject(Scene.IResourcesLoader resourcesLoader, string assetRessourcesPath, Transform parent)
		{
			PersistableObject obj = null;
			GameObject gameObject = null;

			/*
			var async_ = Resources.LoadAsync(assetRessourcesPath); //to Addressables.LoadAssetAsync<GameObject>("desert/tank.prefab");.
			while (!async_.isDone) { }
			UnityEngine.Object prefab = async_.asset;
			*/

			// try to load prefab
			//UnityEngine.Object prefab = Resources.Load(assetRessourcesPath); // note: not .prefab!
			UnityEngine.Object prefab = null;
			if (resourcesLoader != null)
				prefab = resourcesLoader.GetResource(assetRessourcesPath);
			else
				prefab = Resources.Load(assetRessourcesPath); // note: not .prefab!

			if (prefab != null)
			{
				// instantiate prefab
				gameObject = (GameObject)GameObject.Instantiate(prefab, parent);
			}
			
			// if no game object we draw a error desc
			if (gameObject == null)
			{
				gameObject = ErrorGizmo(assetRessourcesPath, parent);
			}
			// game object null again
			if (gameObject == null)
			{
				Debug.LogError("Intantiation of game object " + assetRessourcesPath + " has failed !!!");
			}
			else
			{
				// return PersistableObject
				obj = gameObject.GetComponent<PersistableObject>();
				if (obj == null)
					obj = gameObject.AddComponent<PersistableObject>();
				obj = gameObject.GetComponent<PersistableObject>();
				if (obj == null)
					obj = ErrorGizmo(assetRessourcesPath, parent).GetComponent<PersistableObject>();
			}
			return obj;
		}

		public bool IsRessourceValid()
		{
			bool isValid = false;
			if (string.IsNullOrEmpty(this.assetRessourcesPath) == false)
			{
				isValid = true;
			}
			return isValid;
		}

		static public PersistableObject InstantiateFromRessources(Scene.IResourcesLoader resourcesLoader, string assetRessourcesPath)
		{
			return SetupPersistableObject(resourcesLoader, assetRessourcesPath, null);
		}

		static public PersistableObject InstantiateFromRessources(Scene.IResourcesLoader resourcesLoader, string assetRessourcesPath, Transform parent)
		{
			return SetupPersistableObject(resourcesLoader, assetRessourcesPath, parent);
		}

#if UNITY_EDITOR
		static public PersistableObject InstantiateFromPrefab(GameObject prefab, string identifierParent, Transform parent)
		{
			PersistableObject persistableObject = null;

			// get Outermost prefab insatnce
			var outermostPrefabInstanceRoot = PrefabUtility.GetOutermostPrefabInstanceRoot(prefab as GameObject);

			// if outermostPrefabInstanceRoot is null then the client prefab is not bound to its own prefab
			if (outermostPrefabInstanceRoot == null)
			{
				// return error
				var gameObject = ErrorGizmo(prefab.name, parent);
				persistableObject = gameObject.GetComponent<PersistableObject>();
			}
			else
			{
				var gameObjectTarget = GameObject.Instantiate(prefab, parent.position, parent.rotation);
				persistableObject = gameObjectTarget.GetComponent<PersistableObject>();
				persistableObject.IdentifierParent = identifierParent;
			}
			return persistableObject;
		}
#endif

		public Object CloneObject()
		{
			var clonedObject = Object.Instantiate(this);
			clonedObject.transform.parent = gameObject.transform.parent;
			return clonedObject;
			/*
			PersistableObject gameObjectCloned = PersistableObject.InstantiateFromRessources(assetRessourcesPath, gameObject.transform.parent);
			gameObjectCloned.transform.localPosition = gameObject.transform.localPosition;
			return gameObjectCloned;
			*/
		}
		
		/* 
		 * 
		 * Save / Load object
		 * 
		 */

		public virtual void Save(codingVR.Core.GameDataWriter writer)
		{
			// identifier
			writer.WriteObjectStart("Object");
			writer.Write(identifier, "ID");
			writer.Write(identifierParent == null ? "" : identifierParent, "IDParent");
			writer.WriteObjectEnd();

			// transform
			writer.WriteObjectStart("Transform");
			writer.Write(transform.localPosition, "Position");
			writer.Write(transform.localRotation, "Rotation");
			writer.Write(transform.localScale, "Scale");
			writer.WriteObjectEnd();
		}

		public virtual void Load(codingVR.Core.GameDataReader reader, int version)
		{
			// identifier
			if (version >= 13)
			{
				reader.ReadObjectStart("Object");
				IdentifierSet(reader.ReadString());
				if (version >= 15)
					identifierParent = reader.ReadString();
				reader.ReadObjectEnd();
			}

			// transform
			reader.ReadObjectStart("Transform");
			transform.localPosition = reader.ReadVector3();
			transform.localRotation = reader.ReadQuaternion();
			transform.localScale = reader.ReadVector3();
			reader.ReadObjectEnd();
		}

		public virtual void Load(codingVR.Core.GameDataReader reader)
		{
			Load(reader, 4);
		}
		
		public virtual void Load(codingVR.Core.GameDataReader reader, string assetRessourcesPath, int version)
		{
			this.assetRessourcesPath = assetRessourcesPath;
			Load(reader, version);
		}
	}
} 