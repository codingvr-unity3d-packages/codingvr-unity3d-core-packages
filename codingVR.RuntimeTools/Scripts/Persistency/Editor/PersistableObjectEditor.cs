﻿using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(codingVR.RuntimeTools.PersistableObject))]
public class PersistableObjectEditor : Editor
{
    protected void CloneButtonInspectorGUI(string buttonName)
    {
        if (GUILayout.Button(buttonName))
        {
            foreach (var obj in targets)
            {
                codingVR.RuntimeTools.PersistableObject persistableObject = obj as codingVR.RuntimeTools.PersistableObject;
                if (persistableObject != null)
                {
                    persistableObject.CloneObject();
                }
            }
        }
    }

    protected void NewIDButtonInspectorGUI(string buttonName)
    {
        if (GUILayout.Button(buttonName))
        {
            foreach (var obj in targets)
            {
                codingVR.RuntimeTools.PersistableObject persistableObject = obj as codingVR.RuntimeTools.PersistableObject;
                if (persistableObject != null)
                {
                    persistableObject.MakeIdentifier(true);
                }
            }
        }
    }

    public override void OnInspectorGUI()
    {
        CloneButtonInspectorGUI("Clone on map");
        NewIDButtonInspectorGUI("New ID");
        base.OnInspectorGUI();
    }
}
