﻿using System;

namespace codingVR.GameEngine
{
	// === Event_ScreenPlayScene... ===========================================================================================

	#region Event_ScreenPlayScene

	// Event_ScreenPlaySceneEnter

	public class Event_ScreenPlaySceneEnter : Core.IGameEvent
	{
		public int Type
		{
			get { return 0; }
		}

		public object Data
		{
			set; get;
		}


		public Event_ScreenPlaySceneEnter(string themeName)
		{
			Data = themeName;
		}
	};

    // Event_ScreenPlaySceneLeave
    // WARNING: this message not to be received by the scene which is leaving the game

    public class Event_ScreenPlaySceneLeave : Core.IGameEvent
	{
		public int Type
		{
			get { return 0; }
		}

		public object Data
		{
			set; get;
		}


		public Event_ScreenPlaySceneLeave(string themeName, bool switchOrReplaceScene)
		{
			Data = (themeName, switchOrReplaceScene);
		}
	};

	#endregion

	// === Event_ScreenPlayTag... ===========================================================================================

	#region Event_ScreenPlayTag

	public class Event_ScreenPlayTagBase : Core.IGameEvent
	{
		public int Type
		{
			get { return 0; }
		}

		public object Data
		{
			set; get;
		}
		
		public Event_ScreenPlayTagBase(Tuple<string, Data.Tag> data)
		{
			Data = data;
		}
	}
	
	// Event_ScreenPlayTagEnter

	public class Event_ScreenPlayTagEnter : Event_ScreenPlayTagBase
	{
		public Event_ScreenPlayTagEnter(Tuple<string, Data.Tag> data) : base(data)
		{
		}
	};

	// Event_ScreenPlayTagLeave

	public class Event_ScreenPlayTagLeave : Event_ScreenPlayTagBase
	{
		public Event_ScreenPlayTagLeave(Tuple<string, Data.Tag> data) : base(data)
		{
		}
	};

	// Event_ScreenPlayTagGroupEnter

	public class Event_ScreenPlayTagGroupEnter : Event_ScreenPlayTagBase
	{
		public Event_ScreenPlayTagGroupEnter(Tuple<string, Data.Tag> data) : base(data)
		{
		}
	};

	// Event_ScreenPlayTagGroupLeave

	public class Event_ScreenPlayTagGroupLeave : Event_ScreenPlayTagBase
	{

		public Event_ScreenPlayTagGroupLeave(Tuple<string, Data.Tag> data) : base(data)
		{
		}
	};

	#endregion

	// === Event_ScreenPlayGroupActivity(Open|Close)... ===========================================================================================

	#region Event_ScreenPlayGroupActivityOpenClose

	// Event_ScreenPlayOpen

	public class Event_ScreenPlayGroupActivityOpen : Event_ScreenPlayTagBase
	{
		public Event_ScreenPlayGroupActivityOpen(Tuple<string, Data.Tag> data) : base(data)
		{
		}
	};

	// Event_ScreenPlayClose

	public class Event_ScreenPlayGroupActivityClose : Event_ScreenPlayTagBase
	{

		public Event_ScreenPlayGroupActivityClose(Tuple<string, Data.Tag> data) : base(data)
		{
		}
	};

	#endregion


	// === Event_ScreenPlay(Push|Pop) ===========================================================================================

	#region Event_ScreenPlayPushPop

	// Event_ScreenPlayPush

	public class Event_ScreenPlayPush : Core.IGameEvent
	{
		public int Type
		{
			get { return 0; }
		}

		public object Data
		{
			set; get;
		}


		public Event_ScreenPlayPush(ScreenPlayPropertiesStruct screenPlayPropertiesStruct, Core.EventDelegate done) : base(done)
		{
			Data = screenPlayPropertiesStruct;
		}
	};
	
	// Event_ScreenPlayPop

	public class Event_ScreenPlayPop : Core.IGameEvent
	{
		public int Type
		{
			get { return 0; }
		}

		public object Data
		{
			set; get;
		}

		public Event_ScreenPlayPop(ScreenPlayPropertiesStruct screenPlayPropertiesStruct, Core.EventDelegate done) : base(done)
		{
			Data = screenPlayPropertiesStruct;
		}
	};

	#endregion

	// === Event_ScreenPlaySceneStarted ===========================================================================================

	#region Event_ScreenPlaySceneStarted

	public class Event_ScreenPlaySceneStarted : Core.IGameEvent
	{
		public enum ScreenPlayMode
		{
			none = 0,
			startGame = 1,
			startEditor = 2,
			pushScreenPlayScene = 3,
			popScreenPlayScene = 4,
			switchScreenPlayScene = 5
		}
		public int Type
		{
			get; private set;
		}

		public object Data
		{
			set; get;
		}
		
		public Event_ScreenPlaySceneStarted(ScreenPlayMode mode)
		{
			Type = (int)mode;
		}
	};

	#endregion

	// === Event_LoginCompleted ===========================================================================================

	#region Event_LoginCompleted

	public class Event_LoginCompleted : Core.IGameEvent
	{
		public object Data
		{
			set; get;
		}

		public Event_LoginCompleted(string idPlayer)
		{
			Data = idPlayer;
		}
	};

	#endregion

	// === Event_GameUI... ===========================================================================================

	#region Event_GameUI

	public class Event_GameUIActiveItem : Core.IGameEvent
	{
		public int Index
		{
			set; get;
		}

		public bool Active
		{
			set; get;
		}

		// tuple (int index, activityData data)
		public Event_GameUIActiveItem(int index, bool active)
		{
			Index = index;
			Active = active;
		}
	};

	#endregion

	// === Event_RuntimeObjects... ===========================================================================================

	#region Event_RuntimeObjects

	public class Event_RuntimeObjectsChange : Core.IGameEvent
	{
		public enum Operation
		{
			insert,
			destroy,
		}

		public int Type
		{
			set; get;
		}

		public object Data
		{
			set; get;
		}

		// tuple (int index, activityData data)
		public Event_RuntimeObjectsChange((string idGroup, string idObject, string objectName) id, Operation operation)
		{
			Data = id;
			Type = (int)operation; 
		}
	};

	#endregion
}