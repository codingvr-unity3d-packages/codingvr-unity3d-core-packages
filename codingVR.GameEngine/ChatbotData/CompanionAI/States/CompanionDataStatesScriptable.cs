﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

namespace codingVR.GameEngine
{
	[System.Serializable]
	public class CompanionDataStatesData : Data.ActivityData
	{
		[SerializeField]
		public string description = "Comapnion data states";

		[SerializeField]
		[DefaultValue(false)]
		public bool companionRunning = false;
	}

	[CreateAssetMenu(fileName = "#CompanionDataStates", menuName = "CodingVR/Data/Activities/MonAmiPotoAI/CompanionDataStatesScriptable")]
	[System.Serializable]
	public class CompanionDataStatesScriptable : Data.ActivityDataScriptable
	{
		[SerializeField]
		CompanionDataStatesData rawData;

		public override Data.IActivityFieldServer Data
		{
			get
			{
				return rawData as Data.IActivityFieldServer;
			}
			set
			{
				if (value != rawData)
				{
					rawData = value as CompanionDataStatesData;
				}
			}
		}

		public void OnDestroy()
		{
		}
	}
}
