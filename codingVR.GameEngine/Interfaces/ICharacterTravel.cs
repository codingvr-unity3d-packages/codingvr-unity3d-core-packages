﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	public delegate void TravelDone();

	public interface ICharacterMoving
	{
		// update moving fro one iteration according to speed
		(bool, Vector3, int) UpdateIteration(int findNode);
		// execute complete iteration
		IEnumerator ExecuteCompleteIteration(bool cancelable, global::GameCreator.Characters.Character character, float stopThreshold, float cancelDelay, GameObject target);
		// force stop
		bool ForceStop { set; }
	};

	public interface ICharacterTravel
	{
		// Teleportation
		void Teleport(Data.Tag tag);
		void Teleport(Data.TagPath tagPath);
		// Path selection
		void SelectPath(Data.TagPair tagPair);
		void SelectPath(Data.TagPathPair tagPathPair);
		[Obsolete("PreviousPathToTravel is obsolete.", false)]
		void SelectNextPath(Data.TagPair tagPair);
		// Move on path according to selection & user specification
		ICharacterMoving MoveOnPathBegin(bool reverse);
		// Game object
		GameObject GameObject { get; }
	};
}
