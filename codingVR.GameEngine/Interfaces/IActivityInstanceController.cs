﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	public interface IActivityInstanceController
	{
		Data.IActivityInstance ActivityInstance { get; }
	}
}
