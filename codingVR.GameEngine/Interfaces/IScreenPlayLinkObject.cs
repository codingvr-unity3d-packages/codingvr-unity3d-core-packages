﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	public interface IScreenPlayLinkObject
	{
		// tag pair
		Data.TagPathPair TagPathPair { get; }
		Data.TagPair TagPair { get; }

		// Gameobject
		GameObject GameObject { get; }

		// compute min & max distance from any pos
		(float, float) ComputeMinMaxDistanceFrom(Vector3 position);
	}
}
