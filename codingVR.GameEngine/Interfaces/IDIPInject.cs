﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.GameEngine
{
	public interface IDIPInject
	{
		DiContainer DiContainer { get; }
	}
}
