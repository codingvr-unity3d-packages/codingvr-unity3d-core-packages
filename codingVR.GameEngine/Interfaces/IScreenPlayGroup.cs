﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	public interface IScreenPlayGroup
	{
		IEnumerator ExecuteScreenPlayGroup(bool active, GameObject target);
		bool Contains(GameObject target);
		bool IsBusy { get; }
		Data.Tag TagTriggerOnEnter { get; }

		// Status group
		// group is activated when any of these properties among [isActivityCreated, isActivityStarted, isActivityCompleted] is enabled
		bool ActiveGroup { get; set; }
		bool Started { get; }
	}
}
