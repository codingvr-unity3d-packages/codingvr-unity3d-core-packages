﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace codingVR.GameEngine
{
    [SerializeField]
    public enum TransitionStatus
    {
        isWaitingForStart,
        isStarting,
        isPaused,
        isPlaying,
        isCompleted
    }

    public interface ITransition
    {
        TransitionStatus StatusTransition { get; set; }
    }

}