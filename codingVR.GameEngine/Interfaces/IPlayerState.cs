﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	public interface IPlayerState
	{
		// Reset Game
		void Reset();

		// Id player
		string IdPlayer { get; }
	};

	public interface IPlayerStateTheme
	{
		// Theme management
		ICollection<string> GetThemeStateKeys();
		bool ContainsThemeState(string themeNameAsKey);
		Data.IThemeState FindThemeState(string themeNameAsKey);
		string FindKey(Data.IThemeState themeState);
		Data.IThemeConfig FindThemeConfig(string themeNameAsKey);
		Data.IThemeState FindCurrentThemeState();
	};
	
	public interface IPlayerStateHistory
	{
		// Game history management
		void UpdateRelevantGameStateHistory(Data.GraphDataNodeScriptable graphData);
		void BackToRelevantGameStateHistory();
	};

	public interface IPlayerStateActivityFields
	{
		Data.IActivityField ActivityField { get; }
		Data.IActivityNestedField ActivityNestedField { get; }
		GameObject GameObject { get; }
	}

	[Flags]
	public enum RelevantActivityFlags
	{
		none = 0,
		all = none,
		managedInCurrentScene = 1,
		visibleFromUI = 2,
		startedOrCompleted = 4,
	}

	public interface IPlayerStateActivityController : IPlayerStateActivityFields
	{
		bool BuildActivity(string idActivityInstance, string idActivityInstanceParent, string idClass);
		bool RegisterActivity(string idActivityInstance, bool bForce);
		bool CreateActivity(string idActivityInstance, bool bForce);
		bool StartActivity(string idActivityInstance, bool bForce);
		bool CompleteActivity(string idActivityInstance, bool bForce);
		bool StopActivity(string idActivityInstance, bool bForce);
		void ResetActivityGameplay(string idActivityInstance);
		void AddActivityClass(string className, string description, Data.ActivityClassImportance importance, Data.ActivityClassRegularity regularity, Data.ActivityClassSize size, int duration, int delay, int points, GameObject activityClassBehavior);
		Data.IActivityClass FindActivityClass(string idClass);
		Data.IActivityInstance FindActivityInstance(string idActivityInstance);
		bool IsActivityRelevant(string idActivityInstance, GameEngine.RelevantActivityFlags flags);
		ICollection<string> GetActivityInstanceKeys(GameEngine.RelevantActivityFlags flags, ICollection<string> collection = null);
	}

	public interface IPlayerStateManager : IPlayerStateActivityFields
	{
	}

	public interface IPlayerStateActivityPlanner
	{
	}
}
