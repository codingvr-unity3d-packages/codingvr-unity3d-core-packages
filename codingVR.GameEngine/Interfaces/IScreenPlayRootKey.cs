﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	// IScreenPlayRootKey works by combining Tag with a specific identifier such as 'identifier.#tag' => TagPath
	// the goal is to manage several non unique tag name (i.e #guidance among several screen play)
	// (screenPlay1 with #guidance and screenPlay2 with #guidance)
	// by identifying a screenPlay with a specific identifier (screenPlay1 identified by companion)
	// we can select a specificaly companion.#guidance which references #guidance from screenPlay1
	public interface IScreenPlayRootKey
	{
		string Identifier { get; set; }
	}
}
