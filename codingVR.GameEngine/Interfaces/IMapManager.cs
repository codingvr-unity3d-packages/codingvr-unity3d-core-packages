﻿using UnityEngine;

namespace codingVR.GameEngine
{
	// === IMapManager =========================================================================================

	public interface IMapManager
	{
		// === game object on map ===

		bool Spawn(Data.Tag tag, GameObject player);
	};
}
