﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.GameEngine
{
	public interface ILocationBasedServicesLink
	{
		bool IsValid();
		bool IsDirty(IScreenPlayLinkObject screenPlayLinkObject);
		Transform Start { get; }
		Transform End { get; }
	};

	public interface ILocationBasedServicesItem
	{

		void Update(float maxDistanceToPath);
		void Release();
	}

	public interface ILocationBasedServices
	{
		bool InstantiatePath(string tagIdentifier, DiContainer diContainer, GameEngine.IScreenPlay screenPlay, Data.IActivityInstance activityInstance, ILocationBasedServicesLink link, GameObject gameObjectOwner, GameObject screenPlayGuidanceControllerPrefab, GameObject tagGameObjectActivity);
		bool DestroyPath(string tagIdentifier);
		void UpdateAll(float maxDistanceToPath);
	}
}
