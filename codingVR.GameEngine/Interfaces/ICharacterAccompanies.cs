﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	public class IWaypointSettings
	{
		public static readonly float _radiusMin = 1.0f;
		public static readonly float _radiusMax = 6.0f;
		public static readonly bool _teleportationOverMaxRadius = false;
		public static readonly float _runSpeedMin = 4.0f;
		public static readonly float _runSpeedMax = 5.0f;
		public static readonly Vector3 _directionalOffset = new Vector3(2.0f, 0.0f, 2.0f);

		public float radiusMin = _radiusMin;
		public float radiusMax = _radiusMax;
		public bool teleportationOverMaxRadius = _teleportationOverMaxRadius;
		public float runSpeedMin = _runSpeedMin;
		public float runSpeedMax = _runSpeedMax;
		public Vector3 directionalOffset = _directionalOffset;

		public IWaypointSettings(float radiusMin, float radiusMax, bool teleportationOverMaxRadius, float runSpeedMin, float runSpeedMax, Vector3 directionalOffset)
		{
			this.radiusMin = radiusMin;
			this.radiusMax = radiusMax;
			this.teleportationOverMaxRadius = teleportationOverMaxRadius;
			this.runSpeedMin = runSpeedMin;
			this.runSpeedMax = runSpeedMax;
			this.directionalOffset = directionalOffset;
		}
	}

	public interface ICharacterAccompanies
	{
		// new waypoint
		void AddWaypoint(Transform waypoint, float radiusMin, float radiusMax, bool teleportationOverMaxRadius, float runSpeedMin, float runSpeedMax, Vector3 directionalOffset);

		// last waypoint
		bool HasReachedLastWaypoint { get; }
		void ReplaceLastWaypoint(Transform waypoint, float radiusMin, float radiusMax, bool teleportationOverMaxRadius, float runSpeedMin, float runSpeedMax, Vector3 directionalOffset);
		void RemoveLastWaypoint();
		void ChangeLastWaypoint(float radiusMin, float radiusMax, bool teleportationOverMaxRadius, float runSpeedMin,  float runSpeedMax, Vector3 directionalOffset);

		// life cycle
		bool Continue { get; set; }
	}
}
