﻿using codingVR.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
    public interface IEconomyManager
    {
        int MoneyRewardByID(string idInstance);
        ItemScriptable GetItemRewardByID(string idInstance);
    }
}