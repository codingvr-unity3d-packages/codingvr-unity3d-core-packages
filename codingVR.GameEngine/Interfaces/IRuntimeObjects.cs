﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	public interface IRuntimeObjects
	{
		bool AddNewObject(GameObject newObject);
		bool DestroyObject(GameObject newObject);
		void DestroyAll();
	}
}
