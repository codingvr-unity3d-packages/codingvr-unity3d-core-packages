﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameCreator.Core;

namespace codingVR.GameEngine
{

    [SerializeField]
    public enum GameLoopStatus
    {
        isWaitingForStart,
        isStarting,
        isPaused,
        isLooping,
        isLoopingAction,
        isCompleted
    }

    public interface IWidgetLoop
    {
       void StartLoop(Actions[] actions, Data.GraphDataNodeGroupScriptable dataGroup,Data.GraphDataNodeScriptable data);
       IEnumerator StopLoop();

        // Game status
        GameLoopStatus Status { get; set; }
    }
}