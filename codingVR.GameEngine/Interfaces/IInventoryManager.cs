﻿using codingVR.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	public enum TypeItem
	{
		Tête,
		Torse,
		Jambes,
		Pieds,
		Cheveux,
		Pilosité,
		Peau
	}

	public enum TypeStore
	{
		Clothes,
		Body
	}

	public enum TagObject
	{
		cvrPlayerClothes,
		cvrPlayerHairs,
		cvrPlayerPilosity

	}

	[System.Serializable]
	public  class DataTypeItem
	{
		public TypeItem typeItem;
		public TypeStore typeStore;
		public Sprite typeImage;
	}
	
	[System.Serializable]
	public class DataTypeStore
	{
		public TypeStore typeStore;
		public Sprite typeImage;
	}
	public interface IInventoryManager
	{
		InventoryScriptable GetInventory();

		// create token
		int CreateToken();

		// money
		bool CheckMoney(int charge);
		int GetMoney();
		void AddMoney(int token, int money);
		void RemoveMoney(int token, int money);

		// items
		void RemoveItem(int token, ItemScriptable item);
		void AddItem(int token, ItemScriptable item);
		bool HasItem(ItemScriptable item);

		// try to equiq with a item from store and return the new item store equiped
		ItemScriptable TryEquipItem(int token, ItemScriptable previousItemStoreEquiped, ItemScriptable itemToEquip, TypeItem itemToDesequip);
		// equip item
		void EquipItem(int token, ItemScriptable itemToEquip, TypeItem itemToDesequip);
		// revert item
		void RevertEquipItem(int token, TypeItem itemToEquipType, ItemScriptable itemToDesequip);
		// change color of body item
		void ChangeMaterialBodyItem(int token, Material material, TypeItem typeItem);
	}
}

