﻿using System.Collections;
using UnityEngine;

namespace codingVR.GameEngine
{
	public interface IGameRootManager
	{
		// Save any game object from theme or scenes
		void SaveGameObject(string key, GameObject gameObject);
		void RestoreGameObject(string key);

		// game object
		GameObject GameObject { get; }
	}
};
