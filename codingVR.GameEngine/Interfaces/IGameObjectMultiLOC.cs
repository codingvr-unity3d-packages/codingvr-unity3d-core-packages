﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	public interface IGameObjectMultiLOC
	{
		int CurrentLOC { get; set; }
		int CountLOC { get; }
	}
}
