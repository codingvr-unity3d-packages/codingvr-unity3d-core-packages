﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	[SerializeField]
	public enum GameSessionStatus
	{
		isWaitingForStart,
		isStarting,
		isPaused,
		isPlaying,
		isCompleted
	}

	public interface IGameSession<Structure> where Structure : ScriptableObject
	{
		// Select Data
		Structure SelectData(Structure data = null);
		// Start is called before the first frame update
		IEnumerator StartSession();
		IEnumerator StopSession();
		// Update is called once per frame
		Structure Reply { get; }
		// Game status
		GameSessionStatus Status { get; set;  }
	}
}
