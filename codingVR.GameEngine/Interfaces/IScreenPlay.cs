﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace codingVR.GameEngine
{
	public struct PresentObjectStruct
	{
		public Vector3 positionMapTag { get; private set; }
		public Data.Tag tag { get; private set; }
		public Transform transformScreenPlayTag { get; set; }

		public PresentObjectStruct(Vector3 positionMapTag, Data.Tag tag)
		{
			this.positionMapTag = positionMapTag;
			this.transformScreenPlayTag = null;
			this.tag = tag;
		}

		public PresentObjectStruct(Vector3 positionMapTag, Data.Tag tag, Transform transformScreenPlayTag)
		{
			this.positionMapTag = positionMapTag;
			this.transformScreenPlayTag = transformScreenPlayTag;
			this.tag = tag;
		}
	}

	[Serializable]
	public struct ScreenPlayPropertiesStruct
	{
		// current screen play game object
		public string currentScreenPlaySceneName;
		// loading scene screen
		public string loadingSceneName;
		// last screen play tag
		[FormerlySerializedAs("lastScreenPlayTag")]
		public string lastScreenPlayObjectTag;

		public ScreenPlayPropertiesStruct(string currentScreenPlaySceneName, string loadingSceneName, string lastScreenPlayTag)
		{
			this.currentScreenPlaySceneName = currentScreenPlaySceneName;
			this.loadingSceneName = loadingSceneName;
			this.lastScreenPlayObjectTag = lastScreenPlayTag;
		}

		public ScreenPlayPropertiesStruct(string currentScreenPlaySceneName, string lastScreenPlayTag)
		{
			this.currentScreenPlaySceneName = currentScreenPlaySceneName;
			this.loadingSceneName = "Loading";
			this.lastScreenPlayObjectTag = lastScreenPlayTag;
		}
	}

	public struct PathTagStruct
	{
		public Vector3 start;
		public Vector3? mid;
		public Vector3? end;
		public IScreenPlayLinkObject linkObject;

		public PathTagStruct(IScreenPlayLinkObject linkObject, Vector3 start, Vector3? mid, Vector3? end)
		{
			this.start = start;
			this.mid = mid;
			this.end = end;
			this.linkObject = linkObject;
		}
	};

	public interface IScreenPlay
	{
		/*
		 * 
		 * LifeCycle
		 * 
		 */

		// launch screen play in game mode; the data passed in the function are managed by the game manager
		bool LaunchScreenPlayGameMode(ScreenPlayPropertiesStruct[] theme);
		// launch screen play with some data edited in the screen play UI interface for test purpose
		bool LaunchScreenPlayEditMode();

		/*
		 * 
		 * Object management
		 * 
		 */

		// in order ot manager geometric trigger, place all object to final location
		void PresentObjects(List<PresentObjectStruct> presentObjectArray);
		// we find tag among all screen play
		GameObject FindTag(Data.Tag tag);
		// we find tag restricted to specific screenPlay identified by TagPath
		GameObject FindTagPath(Data.TagPath tagPath);
		// find screen play from parent
		GameObject FindScreenPlay(GameObject fromParent);
		// Find child from parent
		List<GameObject> FindChildrenTagFrom(GameObject fromParent, Data.Tag tag);
		GameObject FindChildTagFrom(GameObject fromParent, Data.Tag tag);
		// find parent
		GameObject FindParentTagFrom(GameObject gameObject);
		// position tag 
		(Vector3, bool) PositionTag(Data.Tag tag);
		(Vector3, bool) PositionTagPath(Data.TagPath tagPath);
		// path tag
		PathTagStruct? PathTag(Data.TagPair tagPair);
		PathTagStruct? PathFromTagPath(Data.TagPathPair tagPathPair);
		// SwitchingObject ScreenPlay
		IEnumerator SwitchingObject(Data.Tag fromTag, Data.Tag toTag, Core.Done done);
		// activate ScreenPlay Object from LastScreenPlayObject
		IEnumerator SwitchingObjectLastScreenPlayObject(Data.Tag tag, Core.Done done);
		// activate/deactivate ScreenPlay Object
		IEnumerator ActivateObject(Data.Tag tag, Core.Done done);
		IEnumerator DeactivateObject(Data.Tag tag, Core.Done done);
		// runtime objects
		IRuntimeObjects FindRuntimeObjects(GameObject gameObject);

		/*
		 * 
		 * Scene management
		 * 
		 */

		// addressables scene
		bool AddressablesScene { get; }
		// switching Scene
		IEnumerator SwitchingScreenPlay(ScreenPlayPropertiesStruct theme);
		// push/pop screen play
		IEnumerator PushScreenPlay(ScreenPlayPropertiesStruct theme, bool closeParent);
		IEnumerator PopScreenPlay(bool openParent);
		// configure next tag for next time
		void SetupScreenPlayNextTag(string nextTag);
		// current screen play scene
		string CurrentScreenPlaySceneName();
		//  last tag
		string LastScreenPlayObjectTag { get; }
	}
}