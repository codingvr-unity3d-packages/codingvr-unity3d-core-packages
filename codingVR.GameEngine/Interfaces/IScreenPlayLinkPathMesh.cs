﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	public interface IScreenPlayLinkPathMesh
	{
		// update path
		void UpdatePathMesh();

		// Distance point to path
		float DistancePointToPath { get; }
	}
}
