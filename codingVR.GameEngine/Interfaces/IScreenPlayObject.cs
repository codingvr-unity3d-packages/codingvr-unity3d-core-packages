﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	public abstract class IScreenPlayObject : MonoBehaviour
	{
		// Is Busy
		public abstract bool IsBusy { get; }

		// Activate screen play object
		public abstract IEnumerator Activate(bool b);
		// Reset screen play object
		public abstract void Reset();
		// Stop screen play object
		public abstract void Stop();

		// Group
		public abstract IScreenPlayGroup Group { get; }

		// Runtime objects
		public abstract IRuntimeObjects RuntimeObjects { get; }

		// Gameobject
		public abstract GameObject GameObject { get; }
	}
}
