﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	public static class Layers
	{
		static public readonly int cvrTerrain = 10;
		static public readonly int cvrPathFinding = 13;
		static public readonly int cvrBuilding = 14;
	}
}
