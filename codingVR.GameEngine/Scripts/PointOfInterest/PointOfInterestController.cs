﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.GameEngine
{
	public class PointOfInterestController : MonoBehaviour
	{
		// === DATA-UI ===========================================================================

		#region DATA-UI

		[Header("Screen Play Group")]
		[Tooltip
			(
			"Screen Play Group identifies the owner of this point of interest.\n"
			)]
		[SerializeField]
		GameEngine.ScreenPlayGroup screenPlayGroup;

		[Header("Gfx")]
		[Tooltip
			(
			"The grpahic of this point of interest.\n"
			)]
		[SerializeField]
		GameObject gameObjectGfx;

		#endregion

		// === DATA-PRIVATE ===========================================================================

		#region DATA-PRIVATE

		Core.IEventManager eventManager;
		GameEngine.IPlayerStateActivityController playerStateActivityController;
		Data.IActivityInstance activityInstance;

		// is selected
		bool? isSelectedPointOfInterest = null;

		#endregion

		// === LIFE-CYCLE ============================================================

		#region LIFE-CYCLE

		[Inject]
		public void Constructor(Core.IEventManager eventManager, GameEngine.IPlayerStateActivityController playerStateActivityController)
		{
			this.eventManager = eventManager;
			this.playerStateActivityController = playerStateActivityController;
		}

		void OnEnable()
		{
			EnableListeners();
		}

		void OnDisable()
		{
			DisableListeners();
		}

		private void Start()
		{
		}

		#endregion

		// === IMPLEMENTATION-UTILS ============================================================

		#region IMPLEMENTATION-UTILS

		bool ContainsIdInstanceActivity(string groupTagName)
		{
			return screenPlayGroup?.name.Contains(groupTagName) ?? false;
		}

		bool IsThisGroup(Data.Event_PlayerStateActivityInstanceChange ev)
		{
			var groupData = ((string, Data.IActivityFieldServer))ev.Data;
			return ContainsIdInstanceActivity(groupData.Item1);
		}

		#endregion

		// === TRIGGERING ============================================================

		#region TRIGGERING

		/*
		*
		* Triggering
		* 
		*/

		private void EnableListeners()
		{
			// add listener
			// activity instance change
			eventManager.AddListener<Data.Event_PlayerStateActivityInstanceChange>(OnActivityInstanceChange);

			// add listener on player state activity event
			eventManager.AddListener<Data.Event_PlayerStateActivitiesJustStarting>(OnActivitiesJustStarting);
		}

		private void DisableListeners()
		{
			// remove listener
			// activity instance change
			eventManager.RemoveListener<Data.Event_PlayerStateActivityInstanceChange>(OnActivityInstanceChange);

			// add listener on player state activity event
			eventManager.RemoveListener<Data.Event_PlayerStateActivitiesJustStarting>(OnActivitiesJustStarting);
		}

		void OnActivitiesJustStarting(Data.Event_PlayerStateActivitiesJustStarting ev)
		{
			if (screenPlayGroup != null)
			{
				activityInstance = playerStateActivityController.FindActivityInstance(screenPlayGroup.name);
			}

			SelectPointOfInterest();
		}

		private void OnActivityInstanceChange(Data.Event_PlayerStateActivityInstanceChange ev)
		{
			if (IsThisGroup(ev))
			{
				SelectPointOfInterest();
			}
		}

		#endregion

		// === IMPLEMENTATION ============================================================

		#region IMPLEMENTATION

		private void SelectPointOfInterest()
		{
			if (activityInstance != null)
			{
				if (isSelectedPointOfInterest == null || isSelectedPointOfInterest != activityInstance.IsSelected)
				{
					isSelectedPointOfInterest = activityInstance.IsSelected;
				}

				gameObjectGfx?.SetActive(isSelectedPointOfInterest.Value);
			}
		}

		#endregion

	}
}
