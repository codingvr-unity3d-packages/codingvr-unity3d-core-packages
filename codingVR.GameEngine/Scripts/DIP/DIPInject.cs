﻿//#define TEST_NAV_MESH

using System;
using System.Collections;
using System.Collections.Generic;
using codingVR.Data;
using UnityEngine;
using Zenject;

namespace codingVR.GameEngine
{
		/* 
		 * 
		 * When the container for this object is a scriptable object of this HACK is used to inject dependency 
		 * 
		 */

	public class DIPInject : MonoBehaviour, IDIPInject
	{
		// === DATA-PRIVATE ============================================================

		#region DATA-PRIVATE

		DiContainer diContainer;
		
		#endregion
		
	// === UNITY-LIFECYCLE ============================================================

		#region UNITY-LIFECYCLE

		[Inject]
		public void Constructor(DiContainer diContainer)
		{
			this.diContainer = diContainer;
		}

		#endregion

		void Start()
		{
			var behavior = GetComponent<global::GameCreator.Behavior.Behavior>();
			diContainer.Inject(behavior.behaviorGraph);
		}

		// === IActivityInstanceController ====================================================================================

		#region IDIPInject

		public DiContainer DiContainer => diContainer;

		#endregion
	}
}
