﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.GameEngine
{
	public class ActivityInstancePathNGuidanceObject : ILocationBasedServicesItem
	{
		// === PRIVATE-DATA  =============================

		#region PRIVATE-DATA

		// interfaces
		GameEngine.IScreenPlay screenPlay = null;
		DiContainer diContainer = null;

		// activity instance
		Data.IActivityInstance activityInstance;

		// prefabs & gameobjects
		GameObject screenPlayGuidanceControllerPrefab;
		GameObject tagGameObjectActivity;
		GameObject gameObjectOwner;
		ILocationBasedServicesLink link = null;

		// cache variable
		GameObject guidanceGameObject;

		// path & guidance implemntation
		IScreenPlayLinkObject screenPlayLinkObject;
		IScreenPlayLinkPathMesh screenPlayLinkPathMesh;

		// tagIdentifier
		string tagIdentifier;

		#endregion

		// === LIFE-CYCLE ========================================

		#region LIFE-CYCLE

		public ActivityInstancePathNGuidanceObject
		(
			string tagIdentifier,
			DiContainer diContainer,
			GameEngine.IScreenPlay screenPlay,
			Data.IActivityInstance activityInstance,
			ILocationBasedServicesLink link,
			GameObject gameObjectOwner,
			GameObject screenPlayGuidanceControllerPrefab,
			GameObject tagGameObjectActivity
		)
		{
			// tag identifier
			this.tagIdentifier = tagIdentifier;

			// interfaces
			this.diContainer = diContainer;
			this.screenPlay = screenPlay;

			// activity class & instance
			this.activityInstance = activityInstance;

			// game objects & prefabs
			this.screenPlayGuidanceControllerPrefab = screenPlayGuidanceControllerPrefab;
			this.tagGameObjectActivity = tagGameObjectActivity;
			this.screenPlay = screenPlay;
			this.gameObjectOwner = gameObjectOwner;
			this.link = link;

			ApplyPath(this.link);
		}

		#endregion

		// === IMPLEMENTATION ===============================================================

		#region IMPLEMENTATION

		void ApplyPath(ILocationBasedServicesLink link)
		{
			InstantiatePath(this.screenPlayGuidanceControllerPrefab, this.tagIdentifier, link);
		}

		// === Update ========================================================================

		void Update(IScreenPlayLinkObject spLinkObject, IScreenPlayLinkPathMesh spLinkPathMesh, ILocationBasedServicesLink link)
		{
			// set up tags placement
			SetupTagsPlacement(spLinkObject, link);

			// update the mesh linked to path
			spLinkPathMesh.UpdatePathMesh();
		}

		// Update is called once per frame
		void Update(ILocationBasedServicesLink link, float maxDistanceToPath)
		{
			if (screenPlayLinkObject != null)
			{
				if (link.IsDirty(screenPlayLinkObject) == true)
				{
					Update(screenPlayLinkObject, screenPlayLinkPathMesh, link);
				}
			}
		}
		
		// === Manage Tags ==================================================================

		void SetupTagsPlacement(IScreenPlayLinkObject spLinkObject, ILocationBasedServicesLink link)
		{
			// get tag pair
			var tagPathPair = spLinkObject.TagPathPair;

			// path tag
			var tagGameObjectFirst = screenPlay.FindTagPath(tagPathPair.First);
			var tagGameObjectSecond = screenPlay.FindTagPath(tagPathPair.Second);
			var tagActivityGameObject = tagGameObjectActivity;

			// assign new position 
			if (link.Start != null)
				tagGameObjectFirst.transform.position = link.Start.position;
			if (link.End != null)
				tagGameObjectSecond.transform.position = link.End.position;
		}

		void SelectGuidanceTags(string tagIdentifier)
		{
			var screenPlayObject = this.screenPlay.FindScreenPlay(this.gameObjectOwner);
			if (screenPlayObject != null)
			{
				screenPlayObject.GetComponent<IScreenPlayRootKey>().Identifier = tagIdentifier;
			}
		}

		void UnselectGuidanceTags()
		{
			var screenPlayObject = this.screenPlay.FindScreenPlay(this.gameObjectOwner);
			if (screenPlayObject != null)
			{
				screenPlayObject.GetComponent<IScreenPlayRootKey>().Identifier = "";
			}
		}

		// === Instantiate Path =================================================================

		void InstantiatePath(GameObject screenPlayGuidanceControllerPrefab, string tagIdentifier, ILocationBasedServicesLink link)
		{
			if (this.guidanceGameObject == null)
			{
				// instantiate guidance object
				this.guidanceGameObject = GameObject.Instantiate(screenPlayGuidanceControllerPrefab, Vector3.zero, Quaternion.identity, this.gameObjectOwner.transform);
				// inject dependencies to component
				diContainer.Inject(this.guidanceGameObject.GetComponentInChildren<GameEngine.IScreenPlayLinkObject>());
				// select guidance tags				
				SelectGuidanceTags(tagIdentifier);

				// get link object interface
				screenPlayLinkObject = this.guidanceGameObject.GetComponentInChildren<IScreenPlayLinkObject>();
				screenPlayLinkPathMesh = this.guidanceGameObject.GetComponentInChildren<IScreenPlayLinkPathMesh>();

				// make path
				Update(screenPlayLinkObject, screenPlayLinkPathMesh, link);

				// set flags
				this.activityInstance.SubStatusFlags |= Data.ActivityInstanceSubStatusFlags.geoLocalisationIsRunning;
			}
		}

		void DestroyPath()
		{
			UnselectGuidanceTags();
			GameObject.Destroy(guidanceGameObject);
			this.screenPlayLinkObject = null;
			this.screenPlayLinkPathMesh = null;
			this.guidanceGameObject = null;

			// set flags
			this.activityInstance.SubStatusFlags &= ~Data.ActivityInstanceSubStatusFlags.geoLocalisationIsRunning;
		}

		#endregion

		// === Interface ===

		#region INTERFACE

		public void Update(float maxDistanceToPath)
		{
			// Update
			Update(this.link, maxDistanceToPath);

			// Manage selection according to distance point to path
			if (activityInstance.IsSelected == true)
			{
				if (screenPlayLinkPathMesh.DistancePointToPath > 12.0)
				{
					activityInstance.IsSelected = false;
				}
			}
		}

		public void Release()
		{
			DestroyPath();
		}

		#endregion
	}
}
