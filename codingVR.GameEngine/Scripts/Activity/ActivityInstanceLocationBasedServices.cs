﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.GameEngine
{
	public class ActivityInstanceLocationBasedServices : ILocationBasedServices
	{
		Dictionary<string, ILocationBasedServicesItem> localizations = new Dictionary<string, ILocationBasedServicesItem>();

		public ActivityInstanceLocationBasedServices()
		{

		}

		// === ILocationBasedServices ===

		#region ILocationBasedServices

		public bool InstantiatePath(string tagIdentifier, DiContainer diContainer, GameEngine.IScreenPlay screenPlay, Data.IActivityInstance activityInstance, ILocationBasedServicesLink link, GameObject gameObjectOwner, GameObject screenPlayGuidanceControllerPrefab, GameObject tagGameObjectActivity)
		{
			bool builtPath = false;
			if (link != null)
			{
				ActivityInstancePathNGuidanceObject path = new ActivityInstancePathNGuidanceObject(tagIdentifier, diContainer, screenPlay, activityInstance, link, gameObjectOwner, screenPlayGuidanceControllerPrefab, tagGameObjectActivity);
				localizations.Add(tagIdentifier, path);
				builtPath = true;
			}
			return builtPath;
		}

		public bool DestroyPath(string tagIdentifier)
		{
			bool removedPath = false;
			if (localizations.ContainsKey(tagIdentifier))
			{
				localizations[tagIdentifier].Release();
				localizations.Remove(tagIdentifier);
				removedPath = true;
			}
			return removedPath;
		}

		public void UpdateAll(float maxDistanceToPath)
		{
			foreach (var it in localizations)
			{
				it.Value.Update(maxDistanceToPath);
			}
		}

		#endregion
	}
}
