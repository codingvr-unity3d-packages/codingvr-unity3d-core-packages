﻿//#define TEST_NAV_MESH

using System;
using System.Collections;
using System.Collections.Generic;
using codingVR.Data;
using UnityEngine;
using Zenject;

namespace codingVR.GameEngine
{
	public class ActivityInstanceControllerData : MonoBehaviour, Data.IActivityField, IActivityInstanceController
	{
		// === DATA-PRIVATE ============================================================

		#region DATA-PRIVATE

		// interfaces bound
		GameEngine.IScreenPlay screenPlay = null;
		Core.IEventManager eventManager = null;
		DiContainer diContainer = null;
		GameEngine.IPlayerStateActivityController playerStateActivity;
		// listening
		int listeningInProgress = 0;

		// activity instance
		Data.IActivityInstance activityInstance;

		// group
		IScreenPlayGroup screenPlayGroup;
		

		#endregion

		// === UNITY-LIFECYCLE ============================================================

		#region UNITY-LIFECYCLE

		[Inject]
		public void Constructor(DiContainer diContainer, GameEngine.IScreenPlay screenPlay, Core.IEventManager eventManager, GameEngine.IPlayerStateActivityController playerStateActivity)
		{
			this.screenPlay = screenPlay;
			this.eventManager = eventManager;
			this.diContainer = diContainer;
			this.playerStateActivity = playerStateActivity;

			EnableListeners();
		}

		private void OnEnable()
		{
			EnableListeners();
		}

		private void OnDisable()
		{
			DisableListeners();
		}

		// Start is called before the first frame update
		private void Start()
		{
			//SetupTagGroup();
		}

		// Update is called once per frame
		void Update()
		{
		}

		#endregion

		// === TRIGGERING ============================================================

		#region TRIGGERING

		/*
		*
		* Triggering
		* 
		*/

		private void EnableListeners()
		{
			// add listener
			if (listeningInProgress == 0 && eventManager != null)
			{
				// add listener on game event
				eventManager.AddListener<GameEngine.Event_ScreenPlaySceneStarted>(OnScreenPlaySceneStarted);
				// tag group
				eventManager.AddListener<GameEngine.Event_ScreenPlayTagGroupEnter>(OnScreenPlayTagGroupEnter);
				eventManager.AddListener<GameEngine.Event_ScreenPlayTagGroupLeave>(OnScreenPlayTagGroupLeave);
				// scene enter
				eventManager.AddListener<GameEngine.Event_ScreenPlaySceneEnter>(OnScreenPlaySceneEnter);
				// activity instance
				eventManager.AddListener<Data.Event_PlayerStateActivityRegister>(OnActivityRegister);
				eventManager.AddListener<Data.Event_PlayerStateActivityCreate>(OnActivityCreate);
				eventManager.AddListener<Data.Event_PlayerStateActivityStart>(OnActivityStart);
				eventManager.AddListener<Data.Event_PlayerStateActivityComplete>(OnActivityComplete);
				eventManager.AddListener<Data.Event_PlayerStateActivityStop>(OnActivityStop);

				// add listener on activity group Open|Close
				eventManager.AddListener<GameEngine.Event_ScreenPlayGroupActivityOpen>(OnScreenPlayGroupActivityOpen);
				eventManager.AddListener<GameEngine.Event_ScreenPlayGroupActivityClose>(OnScreenPlayGroupActivityClose);

				// listener initialized
				listeningInProgress++;
			}
		}

		private void DisableListeners()
		{
			// remove listener
			if (listeningInProgress > 0)
			{
				// remove listener on game event
				eventManager.RemoveListener<GameEngine.Event_ScreenPlaySceneStarted>(OnScreenPlaySceneStarted);
				// tag group
				eventManager.RemoveListener<GameEngine.Event_ScreenPlayTagGroupEnter>(OnScreenPlayTagGroupEnter);
				eventManager.RemoveListener<GameEngine.Event_ScreenPlayTagGroupLeave>(OnScreenPlayTagGroupLeave);
				// scene enter
				eventManager.RemoveListener<GameEngine.Event_ScreenPlaySceneEnter>(OnScreenPlaySceneEnter);
				// activity instance
				eventManager.RemoveListener<Data.Event_PlayerStateActivityRegister>(OnActivityRegister);
				eventManager.RemoveListener<Data.Event_PlayerStateActivityCreate>(OnActivityCreate);
				eventManager.RemoveListener<Data.Event_PlayerStateActivityStart>(OnActivityStart);
				eventManager.RemoveListener<Data.Event_PlayerStateActivityComplete>(OnActivityComplete);
				eventManager.RemoveListener<Data.Event_PlayerStateActivityStop>(OnActivityStop);

				// add listener on activity group Open|Close
				eventManager.RemoveListener<GameEngine.Event_ScreenPlayGroupActivityOpen>(OnScreenPlayGroupActivityOpen);
				eventManager.RemoveListener<GameEngine.Event_ScreenPlayGroupActivityClose>(OnScreenPlayGroupActivityClose);

				// listener desinitialized
				listeningInProgress--;
			}
		}

		/*
		 * Screen Play
		 * 
		 */

		// Screen Play Started

		void OnScreenPlaySceneStarted(GameEngine.Event_ScreenPlaySceneStarted ev)
		{
			var screenPlayMode = (GameEngine.Event_ScreenPlaySceneStarted.ScreenPlayMode)ev.Type;
			switch (screenPlayMode)
			{
				case GameEngine.Event_ScreenPlaySceneStarted.ScreenPlayMode.startEditor:
				case GameEngine.Event_ScreenPlaySceneStarted.ScreenPlayMode.startGame:
					break;
				case GameEngine.Event_ScreenPlaySceneStarted.ScreenPlayMode.pushScreenPlayScene:
				case GameEngine.Event_ScreenPlaySceneStarted.ScreenPlayMode.popScreenPlayScene:
					break;
				case GameEngine.Event_ScreenPlaySceneStarted.ScreenPlayMode.switchScreenPlayScene:
					break;
				case GameEngine.Event_ScreenPlaySceneStarted.ScreenPlayMode.none:
					break;
			}
		}
		
		void OnScreenPlayTagGroupEnter(GameEngine.Event_ScreenPlayTagGroupEnter ev)
		{
			if (IsThisGroup(ev))
			{
			}
		}

		void OnScreenPlayTagGroupLeave(GameEngine.Event_ScreenPlayTagGroupLeave ev)
		{
			if (IsThisGroup(ev))
			{
			}
		}

		void OnScreenPlaySceneEnter(GameEngine.Event_ScreenPlaySceneEnter ev)
		{
			//SetupTagGroup();
		}
		
		/*
		 * 
		 * Life cycle
		 * 
		 */

		private void OnActivityRegister(Data.Event_PlayerStateActivityRegister ev)
		{
			OnActivityChange(ev);
		}

		private void OnActivityCreate(Data.Event_PlayerStateActivityCreate ev)
		{
			OnActivityChange(ev);
		}

		private void OnActivityStart(Data.Event_PlayerStateActivityStart ev)
		{
			OnActivityChange(ev);
		}

		private void OnActivityComplete(Data.Event_PlayerStateActivityComplete ev)
		{
			OnActivityChange(ev);
		}

		private void OnActivityStop(Data.Event_PlayerStateActivityStop ev)
		{
			OnActivityChange(ev);
		}

		private void OnActivityChange(Data.Event_PlayerStateActivityBase ev)
		{
			if (IsThisGroup(ev))
			{
				//SetupTagGroup();
			}
		}

		// GameEngine.Event_ScreenPlayGroupActivity...

		void OnScreenPlayGroupActivityOpen(GameEngine.Event_ScreenPlayGroupActivityOpen ev)
		{
			var groupData = ev.Data as Tuple<string, Data.Tag>;
		}

		void OnScreenPlayGroupActivityClose(GameEngine.Event_ScreenPlayGroupActivityClose ev)
		{
			var groupData = ev.Data as Tuple<string, Data.Tag>;
		}

		#endregion

		// === IMPLEMENTATION-UTILS ============================================================

		#region IMPLEMENTATION-UTILS

		bool ContainsIdInstanceActivity(string groupTagName)
		{
			return gameObject.name.Contains(groupTagName);
		}
		
		GameObject TagActivityGameObject
		{
			get
			{
				// can't place result to cache since we suppose that the target can be unloaded at any time
				// in this case the cahed reference will be lost
				return screenPlay.FindTag(Data.Tag.CreateTag(activityInstance.IdInstance));
			}
		}

		bool IsThisGroup(GameEngine.Event_ScreenPlayTagBase ev)
		{
			var groupData = ev.Data as Tuple<string, Data.Tag>;
			return ContainsIdInstanceActivity(groupData.Item2.ToString());
		}

		bool IsThisGroup(Data.Event_PlayerStateActivityBase ev)
		{
			var groupData = ev.Data as string;
			return ContainsIdInstanceActivity(groupData);
		}

		#endregion

		// === IMPLEMENTATION-DATA-SYNCING ============================================================

		#region IMPLEMENTATION-DATA-SYNCING

		private IActivityField[] ActivityFields
		{
			get
			{
				return TagActivityGameObject.GetComponentsInChildren<IActivityFieldServer>();
			}
		}

		public delegate RET Operation<RET>(IActivityField function, FieldSearchMethod method);

		RET ParseAndExecute<RET>(IActivityField[] activityFields, FieldSearchMethod method, Operation<RET> func, RET failed)
		{
			switch (method)
			{
				case FieldSearchMethod.defaultMethod:
					method = FieldSearchMethod.any;
					break;
				case FieldSearchMethod.any:
					break;
				case FieldSearchMethod.fullPath:
					break;
				case FieldSearchMethod.localPath:
					break;
			}

			RET b = default(RET);
			foreach (var it in activityFields)
			{
				b = func(it, method);
				if (b?.Equals(failed) == false)
				{
					break;
				}
			}
			return b;
		}

		public bool SetValue(string name, object value, FieldSearchMethod method = FieldSearchMethod.defaultMethod)
		{
			bool Op(IActivityField _activityField, FieldSearchMethod _method)
			{
				return _activityField.SetValue(name, value, _method);
			}
			return ParseAndExecute(ActivityFields, method, Op, false);
		}

		public bool AddValue(string name, object value, FieldSearchMethod method = FieldSearchMethod.defaultMethod)
		{
			bool Op(IActivityField _activityField, FieldSearchMethod _method)
			{
				return _activityField.AddValue(name, value, _method);
			}
			return ParseAndExecute(ActivityFields, method, Op, false);
		}

		public bool SubValue(string name, object value, FieldSearchMethod method = FieldSearchMethod.defaultMethod)
		{
			bool Op(IActivityField _activityField, FieldSearchMethod _method)
			{
				return _activityField.SubValue(name, value, _method);
			}
			return ParseAndExecute(ActivityFields, method, Op, false);
		}

		public bool DivValue(string name, object value, FieldSearchMethod method = FieldSearchMethod.defaultMethod)
		{
			bool Op(IActivityField _activityField, FieldSearchMethod _method)
			{
				return _activityField.DivValue(name, value, _method);
			}
			return ParseAndExecute(ActivityFields, method, Op, false);
		}

		public bool MulValue(string name, object value, FieldSearchMethod method = FieldSearchMethod.defaultMethod)
		{
			bool Op(IActivityField _activityField, FieldSearchMethod _method)
			{
				return _activityField.MulValue(name, value, _method);
			}
			return ParseAndExecute(ActivityFields, method, Op, false);
		}

		public bool ResetValue(string name, FieldSearchMethod method = FieldSearchMethod.defaultMethod)
		{
			bool Op(IActivityField _activityField, FieldSearchMethod _method)
			{
				return _activityField.ResetValue(name, _method);
			}
			return ParseAndExecute(ActivityFields, method, Op, false);
		}

		public object GetValue(string name, FieldSearchMethod method = FieldSearchMethod.defaultMethod)
		{
			object Op(IActivityField _activityField, FieldSearchMethod _method)
			{
				return _activityField.GetValue(name, _method);
			}
			return ParseAndExecute(ActivityFields, method, Op, null);
		}

		public (bool, bool) CompareValue(string name, object value, FieldComparison comparison, FieldSearchMethod method = FieldSearchMethod.defaultMethod)
		{
			(bool, bool) Op(IActivityField _activityField, FieldSearchMethod _method)
			{
				return _activityField.CompareValue(name, value, comparison, _method);
			}
			return ParseAndExecute<(bool, bool)>(ActivityFields, method, Op, (false, false));
		}

		#endregion

		// === IActivityInstanceController ====================================================================================

		#region IActivityInstanceController

		public Data.IActivityInstance ActivityInstance
		{
			get
			{
				return activityInstance;
			}
			set
			{
				if (value != activityInstance)
				{
					activityInstance = value;
				}
			}
		}

		#endregion
	}
}
