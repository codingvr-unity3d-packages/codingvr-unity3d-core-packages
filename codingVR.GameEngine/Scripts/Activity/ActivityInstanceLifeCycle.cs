﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.GameEngine
{
	public class ActivityInstanceLifeCycle : MonoBehaviour
	{
		// === DATA-UI ============================================================

		#region DATA-UI

		[SerializeField]
		ScreenPlayGroup screenPlayGroup;

		#endregion

		// === DATA-PRIVATE ============================================================

		#region DATA-PRIVATE

		// interfaces bound
		Core.IEventManager eventManager = null;
		DiContainer diContainer = null;
		
		// listening
		int listeningInProgress = 0;

		// actions
		static readonly string __action_register__ = "__action_register__";
		static readonly string __action_registering__ = "__action_registering__";
		static readonly string __action_registered__ = "__action_registered__";

		static readonly string __action_create__ = "__action_create__";
		static readonly string __action_created__ = "__action_created__";
		static readonly string __action_creating__ = "__action_creating__";

		static readonly string __action_start__ = "__action_start__";
		static readonly string __action_started__ = "__action_started__";
		static readonly string __action_starting__ = "__action_starting__";

		static readonly string __action_complete__ = "__action_complete__";
		static readonly string __action_completed__ = "__action_completed__";
		static readonly string __action_completing__ = "__action_completing__";

		static readonly string __action_stop__ = "__action_stop__";
		static readonly string __action_stopping__ = "__action_stopping__";

		static readonly string __action_ready__ = "__action_ready__";

		Dictionary<string, (string, string)> actionsChoices = new Dictionary<string, (string, string)>()
		{
			{ __action_register__, (__action_registering__, __action_registered__)},
			{ __action_create__, (__action_creating__, __action_created__)},
			{ __action_start__, (__action_starting__, __action_started__)},
			{ __action_complete__, (__action_completing__, __action_completed__)},
			{ __action_stop__, (__action_stopping__, null)},
			{ __action_ready__, (null, __action_ready__)}
		};

		#endregion

		// === UNITY-LIFECYCLE ============================================================

		#region UNITY-LIFECYCLE

		[Inject]
		public void Constructor(DiContainer diContainer, Core.IEventManager eventManager)
		{
			this.eventManager = eventManager;
			this.diContainer = diContainer;

			EnableListeners();
		}

		private void OnEnable()
		{
			EnableListeners();
		}

		private void OnDisable()
		{
			DisableListeners();
		}


		// Start is called before the first frame update
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{

		}

		#endregion

		// === IMPLEMENTATION-UTILS ============================================================

		#region IMPLEMENTATION-UTILS

		bool ContainsIdInstanceActivity(string groupTagName)
		{
			bool b = false;
			if (screenPlayGroup != null)
			{
				b = screenPlayGroup.name.Contains(groupTagName);
			}
			else
			{
				Debug.LogErrorFormat("{0} : screenPlayGroup is null on {1}", System.Reflection.MethodBase.GetCurrentMethod().Name, groupTagName);
			}
			return b;
		}

		bool IsThisGroup(GameEngine.Event_ScreenPlayTagBase ev)
		{
			var groupData = ev.Data as Tuple<string, Data.Tag>;
			return ContainsIdInstanceActivity(groupData.Item2.ToString());
		}

		bool IsThisGroup(Data.Event_PlayerStateActivityBase ev)
		{
			var groupData = ev.Data as string;
			return ContainsIdInstanceActivity(groupData);
		}

		#endregion

		// === IMPLEMENTATION-CORE ============================================================

		#region IMPLEMENTATION-CORE 

		void ExecuteGameObjectTag(string actionName)
		{
			if (string.IsNullOrEmpty(actionName) == false)
			{
				var gameObjectAction = gameObject.transform.Find(actionName);
				gameObjectAction.gameObject.SetActive(true);
				var trigger = gameObjectAction.GetComponent<global::GameCreator.Core.Trigger>();
				var action = gameObjectAction.GetComponent<global::GameCreator.Core.Actions>();
				if (null != trigger)
				{
					// Events form GameCreator Actions
					void OnFinish()
					{
						gameObjectAction.gameObject.SetActive(false);
					}

					void OnExecute(GameObject gameObject)
					{
					}

					//Events form GameCreator Actions
					action.onFinish.AddListener(OnFinish);
					action.onExecute.AddListener(OnExecute);

					trigger.Execute();
				}
				else
				{
					Debug.LogErrorFormat("{0} : the game object tagged '{1}' has no trigger !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, tag);
				}
			}
		}

		#endregion
		

		// === TRIGGERING ============================================================

		#region TRIGGERING

		/*
		*
		* Triggering
		* 
		*/

		private void EnableListeners()
		{
			// add listener
			if (listeningInProgress == 0 && eventManager != null)
			{
				// activity instance
				eventManager.AddListener<Data.Event_PlayerStateActivityRegister>(OnActivityRegister);
				eventManager.AddListener<Data.Event_PlayerStateActivityCreate>(OnActivityCreate);
				eventManager.AddListener<Data.Event_PlayerStateActivityStart>(OnActivityStart);
				eventManager.AddListener<Data.Event_PlayerStateActivityComplete>(OnActivityComplete);
				eventManager.AddListener<Data.Event_PlayerStateActivityStop>(OnActivityStop);

				// activities
				eventManager.AddListener<Data.Event_PlayerStateActivitiesStartedAndReady>(OnActivitiesStartedAndReady);


				// listener initialized
				listeningInProgress++;
			}
		}

		private void DisableListeners()
		{
			// remove listener
			if (listeningInProgress > 0)
			{
				// activity instance
				eventManager.RemoveListener<Data.Event_PlayerStateActivityRegister>(OnActivityRegister);
				eventManager.RemoveListener<Data.Event_PlayerStateActivityCreate>(OnActivityCreate);
				eventManager.RemoveListener<Data.Event_PlayerStateActivityStart>(OnActivityStart);
				eventManager.RemoveListener<Data.Event_PlayerStateActivityComplete>(OnActivityComplete);
				eventManager.RemoveListener<Data.Event_PlayerStateActivityStop>(OnActivityStop);

				// activities
				eventManager.RemoveListener<Data.Event_PlayerStateActivitiesStartedAndReady>(OnActivitiesStartedAndReady);

				// listener desinitialized
				listeningInProgress--;
			}
		}
		
		private void OnActivityRegister(Data.Event_PlayerStateActivityRegister ev)
		{
			OnActivityChange(ev, __action_register__);
		}

		private void OnActivityCreate(Data.Event_PlayerStateActivityCreate ev)
		{
			OnActivityChange(ev, __action_create__);
		}

		private void OnActivityStart(Data.Event_PlayerStateActivityStart ev)
		{
			OnActivityChange(ev, __action_start__);
		}

		private void OnActivityComplete(Data.Event_PlayerStateActivityComplete ev)
		{
			OnActivityChange(ev, __action_complete__);
		}

		private void OnActivityStop(Data.Event_PlayerStateActivityStop ev)
		{
			OnActivityChange(ev, __action_stop__);
		}

		private void ExecuteGameObjectAction(string action, bool isProgressing)
		{
			var actionCouple = actionsChoices[action];
			if (isProgressing == true)
			{
				// Renewal is false then the activity is changing
				ExecuteGameObjectTag(actionCouple.Item1);
			}
			else
			{
				// Renewal is false then the activity is changed
				ExecuteGameObjectTag(actionCouple.Item2);
			}
		}

		private void OnActivityChange(Data.Event_PlayerStateActivityBase ev, string action)
		{
			if (IsThisGroup(ev))
			{
				ExecuteGameObjectAction(action, ev.IsProgressing);
			}
		}

		private void OnActivitiesStartedAndReady(Data.Event_PlayerStateActivitiesStartedAndReady ev)
		{
			ExecuteGameObjectAction(__action_ready__, false);
		}

		#endregion
	}
}