﻿//#define TEST_NAV_MESH

using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.GameEngine
{
	public class ActivityInstanceController : MonoBehaviour, IActivityInstanceController
	{
		// === DATA-UI ============================================================

		#region DATA-UI

		[SerializeField]
		GameObject ScreenPlayGuidanceControllerPrefab;

		[SerializeField]
		float maxDistanceToPath = 7.5f;


		[ShowNativeProperty]
		bool IsSelected
		{
			get
			{
				if (isSelectedPointOfInterest != null)
				{
					return isSelectedPointOfInterest.Value;
				}
				return false;
			}
		}

		#endregion

		// === DATA-PRIVATE ============================================================

		#region DATA-PRIVATE

		// interfaces bound
		GameEngine.IScreenPlay screenPlay = null;
		Core.IEventManager eventManager = null;
		DiContainer diContainer = null;
		GameEngine.IPlayerStateActivityController playerStateActivity;
		// listening
		int listeningInProgress = 0;

		// is selected
		bool? isSelectedPointOfInterest = null;


		// activity instance
		Data.IActivityInstance activityInstance;

		// path & guidance implemntation
		ActivityInstanceLocationBasedServices locationBasedServices = new ActivityInstanceLocationBasedServices();

		public class PointOfInterestLink : ILocationBasedServicesLink
		{
			readonly float maxDistanceToPath;
			Transform start;
			Transform end;

			public PointOfInterestLink(Transform start, Transform end, float maxDistanceToPath)
			{
				this.maxDistanceToPath = maxDistanceToPath;
				this.start = start;
				this.end = end;
			}

			// === ILocationBasedServicesLink ===

			public bool IsDirty(IScreenPlayLinkObject screenPlayLinkObject)
			{
				bool isDirty = false;
				/*if (screenPlayLinkObject != null)
				{
					var posPlayer = global::GameCreator.Core.Hooks.HookPlayer.Instance.transform.position;
					(float min, float max) = screenPlayLinkObject.ComputeMinMaxDistanceFrom(posPlayer);
					if (min > maxDistanceToPath)
					{
						isDirty = true;
					}
				}*/
				return isDirty;
			}
			
			public bool IsValid()
			{
				return start != null & this.end != null;

			}

			public Transform Start => start;
			public Transform End => end;
		};


		public class CompanionAILink : ILocationBasedServicesLink
		{
			Transform start;
			Transform end;

			public CompanionAILink(Transform start, Transform end)
			{
				this.start = start;
				this.end = end;
			}

			// === ILocationBasedServicesLink ===

			public bool IsDirty(IScreenPlayLinkObject screenPlayLinkObject)
			{
				bool isDirty = false;
				return isDirty;
			}

			public bool IsValid()
			{
				return start != null & this.end != null;

			}

			public Transform Start => start;
			public Transform End => end;
		};


		// group
		IScreenPlayGroup screenPlayGroup;

		// activity class behavior
		Data.IActivityClassBehavior activityClassBehavior;

		#endregion

		// === UNITY-LIFECYCLE ============================================================

		#region UNITY-LIFECYCLE

		[Inject]
		public void Constructor(DiContainer diContainer, GameEngine.IScreenPlay screenPlay, Core.IEventManager eventManager, GameEngine.IPlayerStateActivityController playerStateActivity)
		{
			this.screenPlay = screenPlay;
			this.eventManager = eventManager;
			this.diContainer = diContainer;
			this.playerStateActivity = playerStateActivity;

			// 
			EnableListeners();
		}

		private void OnEnable()
		{
			EnableListeners();
		}

		private void OnDisable()
		{
			DisableListeners();
		}

		// Start is called before the first frame update
		private void Start()
		{
			SetupTagGroup();
			SelectPointOfInterest();
		}

		// Update is called once per frame
		void Update()
		{
			// update location based services
			locationBasedServices.UpdateAll(maxDistanceToPath);
		}

		#endregion

		// === TRIGGERING ============================================================

		#region TRIGGERING

		/*
		*
		* Triggering
		* 
		*/

		private void EnableListeners()
		{
			// add listener
			if (listeningInProgress == 0 && eventManager != null)
			{
				// tag group
				eventManager.AddListener<GameEngine.Event_ScreenPlayTagGroupEnter>(OnScreenPlayTagGroupEnter);
				eventManager.AddListener<GameEngine.Event_ScreenPlayTagGroupLeave>(OnScreenPlayTagGroupLeave);
				
				// scene enter
				eventManager.AddListener<GameEngine.Event_ScreenPlaySceneEnter>(OnScreenPlaySceneEnter);
				
				// activity instance
				eventManager.AddListener<Data.Event_PlayerStateActivityRegister>(OnActivityRegister);
				eventManager.AddListener<Data.Event_PlayerStateActivityCreate>(OnActivityCreate);
				eventManager.AddListener<Data.Event_PlayerStateActivityStart>(OnActivityStart);
				eventManager.AddListener<Data.Event_PlayerStateActivityComplete>(OnActivityComplete);
				eventManager.AddListener<Data.Event_PlayerStateActivityStop>(OnActivityStop);

				// activity instance change
				eventManager.AddListener<Data.Event_PlayerStateActivityInstanceChange>(OnActivityInstanceChange);

				// listener initialized
				listeningInProgress++;
			}
		}

		private void DisableListeners()
		{
			// remove listener
			if (listeningInProgress > 0)
			{
				// tag group
				eventManager.RemoveListener<GameEngine.Event_ScreenPlayTagGroupEnter>(OnScreenPlayTagGroupEnter);
				eventManager.RemoveListener<GameEngine.Event_ScreenPlayTagGroupLeave>(OnScreenPlayTagGroupLeave);
				
				// scene enter
				eventManager.RemoveListener<GameEngine.Event_ScreenPlaySceneEnter>(OnScreenPlaySceneEnter);
				
				// activity instance
				eventManager.RemoveListener<Data.Event_PlayerStateActivityRegister>(OnActivityRegister);
				eventManager.RemoveListener<Data.Event_PlayerStateActivityCreate>(OnActivityCreate);
				eventManager.RemoveListener<Data.Event_PlayerStateActivityStart>(OnActivityStart);
				eventManager.RemoveListener<Data.Event_PlayerStateActivityComplete>(OnActivityComplete);
				eventManager.RemoveListener<Data.Event_PlayerStateActivityStop>(OnActivityStop);

				// activity instance change
				eventManager.RemoveListener<Data.Event_PlayerStateActivityInstanceChange>(OnActivityInstanceChange);

				// listener desinitialized
				listeningInProgress--;
			}
		}

		/*
		 * Screen Play
		 * 
		 */
		
		void OnScreenPlayTagGroupEnter(GameEngine.Event_ScreenPlayTagGroupEnter ev)
		{
			if (IsThisGroup(ev))
			{
				// Setup get localized support
				locationBasedServices.InstantiatePath("Companion", diContainer, screenPlay, ActivityInstance, GetActivityCompanionAI(), this.gameObject, ScreenPlayGuidanceControllerPrefab, TagActivityGameObject);
			}
		}

		void OnScreenPlayTagGroupLeave(GameEngine.Event_ScreenPlayTagGroupLeave ev)
		{
			if (IsThisGroup(ev))
			{
				// Release get localized support
				locationBasedServices.DestroyPath("Companion");
			}
		}

		void OnScreenPlaySceneEnter(GameEngine.Event_ScreenPlaySceneEnter ev)
		{
			SetupTagGroup();
		}

		/*
		 * 
		 * Life cycle
		 * 
		 */

		private void OnActivityRegister(Data.Event_PlayerStateActivityRegister ev)
		{
			OnActivityChange(ev);
		}

		private void OnActivityCreate(Data.Event_PlayerStateActivityCreate ev)
		{
			OnActivityChange(ev);
		}

		private void OnActivityStart(Data.Event_PlayerStateActivityStart ev)
		{
			OnActivityChange(ev);
		}

		private void OnActivityComplete(Data.Event_PlayerStateActivityComplete ev)
		{
			OnActivityChange(ev);
		}

		private void OnActivityStop(Data.Event_PlayerStateActivityStop ev)
		{
			OnActivityChange(ev);
		}

		private void OnActivityChange(Data.Event_PlayerStateActivityBase ev)
		{
			if (IsThisGroup(ev))
			{
				SetupTagGroup();
			}
		}

		private void OnActivityInstanceChange(Data.Event_PlayerStateActivityInstanceChange ev)
		{
			if (IsThisGroup(ev))
			{
				SelectPointOfInterest();
			}
		}

		#endregion

		// === IMPLEMENTATION-UTILS ============================================================

		#region IMPLEMENTATION-UTILS

		bool ContainsIdInstanceActivity(string groupTagName)
		{
			return gameObject.name.Contains(groupTagName);
		}


		GameObject TagActivityGameObject
		{
			get
			{
				// can't place result to cache since we suppose that the target can be unloaded at any time
				// in this case the cahed reference will be lost
				return screenPlay.FindTag(Data.Tag.CreateTag(ActivityInstance.IdInstance));
			}
		}

		bool IsThisGroup(GameEngine.Event_ScreenPlayTagBase ev)
		{
			var groupData = ev.Data as Tuple<string, Data.Tag>;
			return ContainsIdInstanceActivity(groupData.Item2.ToString());
		}

		bool IsThisGroup(Data.Event_PlayerStateActivityBase ev)
		{
			var groupData = ev.Data as string;
			return ContainsIdInstanceActivity(groupData);
		}

		bool IsThisGroup(Data.Event_PlayerStateActivityInstanceChange ev)
		{
			var groupData = ((string, Data.IActivityFieldServer))ev.Data;
			return ContainsIdInstanceActivity(groupData.Item1);
		}

		Data.IActivityClass ActivityClass
		{
			get
			{
				var activityClass = playerStateActivity.FindActivityClass(ActivityInstance.IdClass);
				return activityClass;
			}
		}
				
		#endregion

		// === IMPLEMENTATION-TAG-GROUP-LIFECYCLE ============================================================

		#region IMPLEMENTATION-TAG-GROUP-LIFECYCLE
			
		void SetupTagGroup()
		{
			var activityClass = ActivityClass;

			if ((activityClass.PropertyFlags & Data.ActivityClassPropertyFlags.geoLocalisedByTag) != 0)
			{
				GameObject tagObject = screenPlay.FindTag(Data.Tag.CreateTag(gameObject.name));
				if (tagObject != null)
				{
					IScreenPlayGroup screenPlayGroup = tagObject.GetComponent<IScreenPlayGroup>();
					screenPlayGroup.ActiveGroup =
						ActivityInstance.StatusFlags == Data.ActivityInstanceLifeCycleStatus.isActivityCreated ||
						ActivityInstance.StatusFlags == Data.ActivityInstanceLifeCycleStatus.isActivityStarted ||
						ActivityInstance.StatusFlags == Data.ActivityInstanceLifeCycleStatus.isActivityCompleted;
				}
			}

			if (activityClassBehavior == null && activityClass.ActivityClassBehavior != null)
			{
				var behavior = GameObject.Instantiate(activityClass.ActivityClassBehavior, Vector3.zero, Quaternion.identity, transform);
				activityClassBehavior = behavior.GetComponent<Data.IActivityClassBehavior>();
				diContainer.Inject(activityClassBehavior);
				activityClassBehavior.Setup(ActivityInstance);
			}
		}

		private void SelectPointOfInterest()
		{
			if (isSelectedPointOfInterest == null || isSelectedPointOfInterest != ActivityInstance.IsSelected)
			{
				isSelectedPointOfInterest = ActivityInstance.IsSelected;

				if ((this.ActivityClass.PropertyFlags & Data.ActivityClassPropertyFlags.geoLocalisationSupport) != 0)
				{
					if (isSelectedPointOfInterest == true)
					{
						// Setup get localized support
						locationBasedServices.InstantiatePath("PointOfInterest", diContainer, screenPlay, ActivityInstance, GetActivityPointOfInterest(), this.gameObject, ScreenPlayGuidanceControllerPrefab, TagActivityGameObject);
					}
					else
					{
						locationBasedServices.DestroyPath("PointOfInterest");
					}
				}
			}
		}

		#endregion

		// === IMPLEMENTATION-PATH ============================================================

		#region IMPLEMENTATION-PATH

		ILocationBasedServicesLink GetActivityCompanionAI()
		{
			Transform start = null;
			Transform end = null;
			ILocationBasedServicesLink link = null;

			if (TagActivityGameObject != null)
			{
				var companionAI = TagActivityGameObject.GetComponentInChildren<CompanionAIController>();
				if (companionAI != null)
				{
					start = companionAI.transform.Find("Start");
					end = companionAI.transform.Find("End");
				}
			}

			if (start != null && end != null)
			{
				link = new CompanionAILink(start, end);
			}
            else
            {
                Debug.LogErrorFormat("{0} : Start or End from {1} are not found !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, TagActivityGameObject);
            }

            return link;
		}

		ILocationBasedServicesLink GetActivityPointOfInterest()
		{
			Transform start = null;
			Transform end = null;
			ILocationBasedServicesLink link = null;

			if (TagActivityGameObject != null)
			{
				var pointOfInterest = TagActivityGameObject.GetComponentInChildren<PointOfInterestController>();
				if (pointOfInterest != null)
				{
					start = global::GameCreator.Core.Hooks.HookPlayer.Instance.transform;
					end = pointOfInterest.transform.Find("Target");
				}
                else
                {
                    Debug.LogErrorFormat("{0} : PointOfInterestController is not found within Activity GameObject {1} !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, TagActivityGameObject);
                }
            }

            if (start != null && end != null)
			{
				link = new PointOfInterestLink(start, end, this.maxDistanceToPath);
			}

			return link;
		}

		#endregion

		// === IActivityInstanceController ====================================================================================

		#region IActivityInstanceController

		public Data.IActivityInstance ActivityInstance
		{
			get
			{
				return activityInstance;
			}
			set
			{
				if (value != activityInstance)
				{
					activityInstance = value;
				}
			}
		}

		#endregion
	}
}
