﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using codingVR.Data;
using Zenject;
using codingVR.Core;
using NaughtyAttributes;
using System;

namespace codingVR.GameEngine
{
	public class InventoryManager : MonoBehaviour, IInventoryManager
	{
		[SerializeField]
		InventoryScriptable inventory;

		private IEventManager eventManager;
		private Scene.IResourcesLoader resourcesLoader;
		private GameEngine.IPlayerState playerState;
		private DiContainer diContainer;
		bool isLoaded = false;

		public static readonly string domain = "inventory";

		// === LIEFCYCLE =========================================================

		#region LIEFCYCLE

		[Inject]
		public void Constructor(DiContainer diContainer, IEventManager eventManager, Scene.IResourcesLoader resourcesLoader, GameEngine.IPlayerState playerState)
		{
			this.eventManager = eventManager;
			this.resourcesLoader = resourcesLoader;
			this.playerState = playerState;
			this.diContainer = diContainer;
			if (inventory != null)
			{
				diContainer.Inject(inventory);
			}
		}

		void OnEnable()
		{
			EnableListeners();
		}

		void OnDisable()
		{
			EnableListeners();
		}

		public void OnApplicationPause(bool pauseStatus)
		{
			if (pauseStatus == true)
			{
				Save();
			}
		}

		public void OnApplicationQuit()
		{
			Save();
		}

		void Update()
		{
			if (this.inventory != null && this.inventory.IsDirty == true && isLoaded == true /* to prevent saving empty data ; we need to have loaded first */)
			{
				//this.eventManager.QueueEvent(new Event_PlayerStateActivityDataChange((activityDataGroupId, this.dataActvity.Data)));
				// TODO : ActivityData : ActivityPersistentDataManager : can't save data at each frame
				// https://gitlab.com/monamipoto/potomaze/-/issues/516
				Save();
			}
		}

		#endregion

		// === TRIGGERING ============================================================

		#region TRIGGERING

		/*
		*
		* Triggering
		* 
		*/

		private void EnableListeners()
		{
			// add listener on player state activity event
			eventManager.AddListener<Data.Event_PlayerStateActivitiesJustStarting>(OnActivitiesJustStarting);

			// add listener on screen play scene leave
			eventManager.AddListener<GameEngine.Event_ScreenPlaySceneLeave>(OnScreenPlaySceneLeave);
			
		}

		private void DisableListeners()
		{
			// listener on player state activity event
			eventManager.RemoveListener<Data.Event_PlayerStateActivitiesJustStarting>(OnActivitiesJustStarting);

			// add listener on screen play scene leave
			eventManager.RemoveListener<GameEngine.Event_ScreenPlaySceneLeave>(OnScreenPlaySceneLeave);
		}

		void OnActivitiesJustStarting(Data.Event_PlayerStateActivitiesJustStarting ev)
		{
			Load();
		}

		void OnScreenPlaySceneLeave(GameEngine.Event_ScreenPlaySceneLeave ev)
		{
			Save();
		}

		#endregion

		// === IMPLEMENTATION-NETWORK =========================================================

		#region IMPLEMENTATION-NETWORK

		/*
		 * 
		 * Network
		 * 
		 */

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		public void Load()
		{
			if (inventory != null)
			{
				void Instantiate(Data.InventoryScriptable data)
				{
					this.isLoaded = true;
					DesequipCompletly();
					foreach (var it in data.ItemsEquiped)
					{
						EquipItem(it);
					}
				}
				StartCoroutine(inventory.GetData(playerState.IdPlayer, domain, Instantiate));
			}
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		public void Save()
		{
			if (inventory != null)
			{
				StartCoroutine(inventory.PutData(playerState.IdPlayer, domain));
			}
		}

		#endregion

		// === IMPLEMENTATION =========================================================

		#region IMPLEMENTATION

		/*
		 * 
		 * Item on player
		 * 
		 */

		GameObject GetClotheObject(ItemScriptable item, GameObject player)
		{
			GameObject g = null;

			g = GetChildWithName(player, item.NameGameObject);

			return g;
		}

		GameObject GetChildWithName(GameObject obj, string name)
		{
			Transform trans = obj.transform;
			Transform childTrans = trans.Find(name);
			// Debug.Log("Trans name : " + trans.name);
			// Debug.Log("ChildTrans name: " + childTrans.name);
			if (childTrans != null && childTrans.name != trans.name)
			{
				return childTrans.gameObject;
			}
			else
			{
				// Debug.Log("No child");
				return null;
			}
		}

		/*
		 * 
		 * Item according to Type
		 * 
		 */

		ItemScriptable GetItemEquipedByType(TypeItem typeItem)
		{
			ItemScriptable item = null;

			foreach (ItemScriptable it in inventory.ItemsEquiped)
			{
				if (it.typeItem == typeItem)
				{
					item = it;
				}
			}

			return item;
		}

		void ReplaceItemEquipedByType(ItemScriptable item, TypeItem typeItem)
		{
			int i = 0;
			int index = -1;

			foreach (ItemScriptable it in inventory.ItemsEquiped)
			{
				if (it.typeItem == typeItem)
				{
					index = i;
				}
				i++;
			}

			inventory.SetItemEquiped(index, item);
		}

		// get item inventory by type
		ItemScriptable GetItemInventoryByType(TypeItem typeItem)
		{
			ItemScriptable item = null;

			foreach (ItemScriptable it in inventory.InventoryItems)
			{
				if (it.typeItem == typeItem)
				{
					item = it;
				}
			}

			return item;
		}

		/*
		 * 
		 * Equip Item
		 * 
		 */

			// equip with new item but withoud desequip
		void EquipItem(ItemScriptable itemToEquip)
		{
			GameObject player = GameObject.FindGameObjectWithTag(itemToEquip.tagObject.ToString());
			GameObject objectToEquip = GetClotheObject(itemToEquip, player);
			Material materialBody = null;

			if (itemToEquip.typeStore == TypeStore.Body)
			{
				ItemScriptable itemEquiped = GetItemEquipedByType(itemToEquip.typeItem);
				
				if(itemEquiped.materialsItem.Count > 0)
				{
					materialBody = itemEquiped.materialsItem[0];
				}
			}

			if (objectToEquip != null)
			{
				objectToEquip.SetActive(true);

				
				if (itemToEquip.materialsItem.Count != 0 && objectToEquip != null)
				{
					//If it's clothes, apply the material of the item to equip on the object
					if (itemToEquip.typeStore == TypeStore.Clothes)
					{
						SkinnedMeshRenderer skinMeshRender = objectToEquip.GetComponent<SkinnedMeshRenderer>();
						Debug.Log("MeshRenderer : " + skinMeshRender);
						Material[] materials = new Material[itemToEquip.materialsItem.Count];

						int i = 0;
						foreach (Material m in itemToEquip.materialsItem)
						{
							materials[i] = m;
							i++;
						}
						skinMeshRender.materials = materials;
					}
					
					//If it's the body, apply the color of the precedent item on the object and change the material on the item to equip
					else if(itemToEquip.typeStore == TypeStore.Body)
					{
						if(itemToEquip.typeItem == TypeItem.Cheveux || itemToEquip.typeItem == TypeItem.Pilosité)
						{
							if(itemToEquip.materialsItem.Count > 0)
							{
								itemToEquip.materialsItem[0] = materialBody;
								MeshRenderer meshRender = objectToEquip.GetComponent<MeshRenderer>();
								meshRender.material = materialBody;
							}
						}
					}
				}
			}
		}
		
		// Revert to previous
		public void RevertEquipItem(ItemScriptable itemToEquip, ItemScriptable itemToDesequip)
		{
			GameObject player = GameObject.FindGameObjectWithTag(itemToEquip.tagObject.ToString());
			// Desequip item
			{
				GameObject objectToDesequip = null;
				objectToDesequip = GetClotheObject(itemToDesequip, player);
				objectToDesequip?.SetActive(false);
			}

			// Equiq item
			{
				EquipItem(itemToEquip);
			}

			return;
		}

		// Desequip Completly
		private void DesequipCompletly()
		{
			var player = GameObject.FindGameObjectWithTag("cvrPlayerClothes");
			foreach (Transform it in player.transform)
			{
				it.gameObject.SetActive(false);
			}
		}

		#endregion
		
		// === IInventoryManager ===========================================

		#region IIventoryManager

		public InventoryScriptable GetInventory()
		{
			return inventory;
		}

		/*
		 * 
		 * Create token
		 * 
		 */

		public int CreateToken()
		{
			return Guid.NewGuid().ToString().GetHashCode();
		}
		
		/*
		 * 
		 * Money
		 * 
		 */

		public bool CheckMoney(int charge)
		{
			return inventory.Money >= charge;
		}

		public int GetMoney()
		{
			return inventory.Money;
		}

		public void AddMoney(int token, int money)
		{
			inventory.Money = inventory.Money + money;
			eventManager.QueueEvent(new Event_InventoryChange(token, Event_InventoryChange.EventType.addMoney));
		}

		public void RemoveMoney(int token, int money)
		{
			inventory.Money = inventory.Money - money;
			eventManager.QueueEvent(new Event_InventoryChange(token, Event_InventoryChange.EventType.removeMoney));
		}

		/*
		 * 
		 * Items
		 * 
		 */

		public void AddItem(int token, ItemScriptable item)
		{
			inventory.AddInventoryItem(item);
			eventManager.QueueEvent(new Event_InventoryChange(token, Event_InventoryChange.EventType.addItem));
		}

		public void RemoveItem(int token, ItemScriptable item)
		{
			inventory.RemoveInventoryItem(item);
			eventManager.QueueEvent(new Event_InventoryChange(token, Event_InventoryChange.EventType.removeItem));
		}

		public bool HasItem(ItemScriptable item)
		{
			return inventory.InventoryItems.Contains(item);
		}

		/*
		 * 
		 * Equip item
		 * 
		 */

		// equip with and desequip
		public void EquipItem(int token, ItemScriptable itemToEquip, TypeItem itemToDesequip)
		{
			GameObject player = GameObject.FindGameObjectWithTag(itemToEquip.tagObject.ToString());

			// desequip
			{
				GameObject objectToDesequip = null;
				objectToDesequip = GetClotheObject(GetItemEquipedByType(itemToDesequip), player);
				objectToDesequip?.SetActive(false);
			}

			// equip
			{
				EquipItem(itemToEquip);
			}

			// manage inventory
			{
				if (GetInventory().InventoryItems.Contains(itemToEquip))
				{
					ReplaceItemEquipedByType(itemToEquip, itemToEquip.typeItem);
				}
			}

			eventManager.QueueEvent(new Event_InventoryChange(token, Event_InventoryChange.EventType.equipItem));

		}

		public ItemScriptable TryEquipItem(int token, ItemScriptable previousItemStoreEquiped, ItemScriptable itemToEquip, TypeItem itemToDesequip)
		{
			GameObject player = GameObject.FindGameObjectWithTag(itemToEquip.tagObject.ToString());

			// desequip
			{
				GameObject objectToDesequip = null;
				if (previousItemStoreEquiped != null)
				{
					if (previousItemStoreEquiped.typeItem == itemToEquip.typeItem)
					{
						objectToDesequip = GetClotheObject(previousItemStoreEquiped, player);
						objectToDesequip?.SetActive(false);
					}
					else
					if (previousItemStoreEquiped.typeItem != itemToEquip.typeItem)
					{
						var previousItem = GetClotheObject(GetItemInventoryByType(previousItemStoreEquiped.typeItem), player);
						previousItem.SetActive(false);
						var currentItem = GetItemEquipedByType(previousItemStoreEquiped.typeItem);
						EquipItem(currentItem);
						objectToDesequip = GetClotheObject(GetItemEquipedByType(itemToDesequip), player);
						objectToDesequip?.SetActive(false);
					}
				}
				else
				{
					objectToDesequip = GetClotheObject(GetItemEquipedByType(itemToDesequip), player);
					objectToDesequip?.SetActive(false);
				}

			}

			// equip
			{
				EquipItem(itemToEquip);
			}

			// manage inventory
			{
				if (GetInventory().InventoryItems.Contains(itemToEquip))
				{
					ReplaceItemEquipedByType(itemToEquip, itemToEquip.typeItem);
				}
				else
				{
					Debug.Log("Test equip :" + itemToEquip.NameGameObject);
					previousItemStoreEquiped = itemToEquip;
				}
			}

			eventManager.QueueEvent(new Event_InventoryChange(token, Event_InventoryChange.EventType.tryEquipItem));

			return previousItemStoreEquiped;
		}

		public void RevertEquipItem(int token, TypeItem itemToEquipType, ItemScriptable itemToDesequip)
		{
			RevertEquipItem(GetItemEquipedByType(itemToEquipType), itemToDesequip);

			eventManager.QueueEvent(new Event_InventoryChange(token, Event_InventoryChange.EventType.revertEquipItem));
		}

		public void ChangeMaterialBodyItem(int token, Material material, TypeItem typeItem)
		{
			ItemScriptable itemEquiped = GetItemEquipedByType(typeItem);

			if (itemEquiped != null)
			{
				GameObject player = GameObject.FindGameObjectWithTag(itemEquiped.tagObject.ToString());

				if (player != null)
				{
					GameObject objectToEquip = GetClotheObject(itemEquiped, player);

					if (objectToEquip != null)
					{
						if (typeItem == TypeItem.Cheveux || typeItem == TypeItem.Pilosité)
						{
							if (itemEquiped.materialsItem.Count > 0)
							{
								itemEquiped.materialsItem[0] = material;
								MeshRenderer meshRender = objectToEquip.GetComponent<MeshRenderer>();
								meshRender.material = material;

								eventManager.QueueEvent(new Event_InventoryChange(token, Event_InventoryChange.EventType.changeMaterialBodyItem));
							}
						}
					}

					else Debug.Log("No object to equip found for : " + typeItem.ToString());
				}

				else Debug.Log("No object in player found for : " + typeItem.ToString());
			}

			else Debug.Log("No item Equiped for : " + typeItem.ToString());
		}

		#endregion
	}
}