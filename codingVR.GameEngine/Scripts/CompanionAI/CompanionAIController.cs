﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Scripting.APIUpdating;
using Zenject;

namespace codingVR.GameEngine
{
	[MovedFrom(false, null, null, "ActivityInstanceCompanion")]
	public class CompanionAIController : MonoBehaviour
	{
		// === DATA-UI ============================================================

		#region DATA-UI

		[SerializeField]
		ScreenPlayGroup screenPlayGroup;

		#endregion

		// === DATA-PRIVATE ============================================================

		#region DATA-PRIVATE

		// interfaces bound
		Core.IEventManager eventManager = null;
		DiContainer diContainer = null;
		
		// listening
		int listeningInProgress = 0;

		// actions
		static readonly string __action_enter__ = "__action_enter__";
		static readonly string __action_leave__ = "__action_leave__";

		#endregion

		// === UNITY-LIFECYCLE ============================================================

		#region UNITY-LIFECYCLE

		[Inject]
		public void Constructor(DiContainer diContainer, Core.IEventManager eventManager)
		{
			this.eventManager = eventManager;
			this.diContainer = diContainer;

			EnableListeners();
		}

		private void OnEnable()
		{
			EnableListeners();
		}

		private void OnDisable()
		{
			DisableListeners();
		}


		// Start is called before the first frame update
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{

		}

		#endregion

		// === IMPLEMENTATION-UTILS ============================================================

		#region IMPLEMENTATION-UTILS

		bool ContainsIdInstanceActivity(string groupTagName)
		{
			bool b = false;
			if (screenPlayGroup != null)
			{
				b = screenPlayGroup.name.Contains(groupTagName);
			}
			return b;

		}

		bool IsThisGroup(GameEngine.Event_ScreenPlayTagBase ev)
		{
			var groupData = ev.Data as Tuple<string, Data.Tag>;
			return ContainsIdInstanceActivity(groupData.Item2.ToString());
		}

		bool IsThisGroup(Data.Event_PlayerStateActivityBase ev)
		{
			var groupData = ev.Data as string;
			return ContainsIdInstanceActivity(groupData);
		}

		#endregion

		// === IMPLEMENTATION-CORE ============================================================

		#region IMPLEMENTATION-CORE 

		void ExecuteGameObjectTag(string actionName)
		{
			var gameObjectAction = gameObject.transform.Find(actionName);
			gameObjectAction.gameObject.SetActive(true);
			var trigger = gameObjectAction.GetComponent<global::GameCreator.Core.Trigger>();
			var action = gameObjectAction.GetComponent<global::GameCreator.Core.Actions>();
			if (null != trigger)
			{
				// Events form GameCreator Actions
				void OnFinish()
				{
					gameObjectAction.gameObject.SetActive(false);
				}

				void OnExecute(GameObject gameObject)
				{
				}

				//Events form GameCreator Actions
				action.onFinish.AddListener(OnFinish);
				action.onExecute.AddListener(OnExecute);

				trigger.Execute();
			}
			else
			{
				Debug.LogErrorFormat("{0} : the game object tagged '{1}' has no trigger !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, tag);
			}
		}

		#endregion
		
		// === TRIGGERING ============================================================

		#region TRIGGERING

		/*
		*
		* Triggering
		* 
		*/

		private void EnableListeners()
		{
			// add listener
			if (listeningInProgress == 0 && eventManager != null)
			{
				// tag group
				eventManager.AddListener<GameEngine.Event_ScreenPlayTagGroupEnter>(OnScreenPlayTagGroupEnter);
				eventManager.AddListener<GameEngine.Event_ScreenPlayTagGroupLeave>(OnScreenPlayTagGroupLeave);

				// listener initialized
				listeningInProgress++;
			}
		}

		private void DisableListeners()
		{
			// remove listener
			if (listeningInProgress > 0)
			{
				// tag group
				eventManager.RemoveListener<GameEngine.Event_ScreenPlayTagGroupEnter>(OnScreenPlayTagGroupEnter);
				eventManager.RemoveListener<GameEngine.Event_ScreenPlayTagGroupLeave>(OnScreenPlayTagGroupLeave);

				// listener desinitialized
				listeningInProgress--;
			}
		}

		void OnScreenPlayTagGroupEnter(GameEngine.Event_ScreenPlayTagGroupEnter ev)
		{
			if (IsThisGroup(ev))
			{
				// Execute companion AI
				ExecuteGameObjectTag(__action_enter__);
			}
		}

		void OnScreenPlayTagGroupLeave(GameEngine.Event_ScreenPlayTagGroupLeave ev)
		{
			if (IsThisGroup(ev))
			{
				// Leave companion AI
				ExecuteGameObjectTag( __action_leave__);
			}
		}

		#endregion
	}
}