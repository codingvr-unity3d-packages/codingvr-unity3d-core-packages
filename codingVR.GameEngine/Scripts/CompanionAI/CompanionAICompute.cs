﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.GameEngine
{
	public class CompanionAICompute : MonoBehaviour, Data.IActivityDataBehavior
	{
		// === DATA-PRIVATE =================================================

		#region DATA-PRIVATE

		Data.IActivityFieldServer activityData;
		Core.IEventManager eventManager;
		int listernerInstalled = 0;

		#endregion

		// === LIFE-CYCLE ============================================================

		#region LIFE-CYCLE

		[Inject]
		void Construct(Core.IEventManager eventManager)
		{
			// global interfaces
			this.eventManager = eventManager;

			// enable listeners
			this.EnableListeners();
		}

		// Start is called before the first frame update
		void Start()
		{
		}

		// Update is called once per frame
		void Update()
		{

		}

		private void OnEnable()
		{
			EnableListeners();
		}

		private void OnDisable()
		{
			DisableListeners();
		}

		#endregion

		// === IActivityDataBehavior ======================================================================================

		#region IActivityDataBehavior

		public void Setup(string activityInstanceId, Data.IActivityFieldServer activityData, GameObject activityDataGroupId)
		{
			this.activityData = activityData;

			// the setup is called after the loading of data
			// here we can reset data than we can't keep
			this.activityData.SetValue("companionRunning", false);
		}

		#endregion

		// === IMPLEMENTATION-UTILS ============================================================

		#region IMPLEMENTATION-UTILS

		bool ContainsIdInstanceActivity(string groupTagName)
		{
			return activityData.Id.Contains(groupTagName);
		}

		bool IsThisGroup(GameEngine.Event_ScreenPlayTagBase ev)
		{
			var activity = ev.Data as Tuple<string, Data.Tag>;
			return ContainsIdInstanceActivity(activity.Item2.ToString());
		}

		bool IsThisGroup(Data.Event_PlayerStateActivityBase ev)
		{
			var activityId = ev.Data as string;
			return ContainsIdInstanceActivity(activityId);
		}

		#endregion

		// === TRIGGERING ============================================================

		#region TRIGGERING

		private void EnableListeners()
		{
			if (eventManager != null && listernerInstalled == 0)
			{
				// add listener on player state activity event
				eventManager.AddListener<Data.Event_PlayerStateActivityStop>(OnActivityStop);

				listernerInstalled++;
			}
		}

		private void DisableListeners()
		{
			if (eventManager != null && listernerInstalled > 0)
			{
				// remove listener on player state activity event
				eventManager.RemoveListener<Data.Event_PlayerStateActivityStop>(OnActivityStop);

				listernerInstalled = 0;
			}
		}

		void OnActivityStop(Data.Event_PlayerStateActivityStop ev)
		{
			if (IsThisGroup(ev))
			{
				
			}
		}

		#endregion
	}
}