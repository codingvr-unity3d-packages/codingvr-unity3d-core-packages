﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.GameEngine
{
	public class GameObjectMultiLOC : MonoBehaviour, IGameObjectMultiLOC
	{
		// === DATA-UI ===========================================================================

		#region DATA-UI

		[Header("LOCs")]

		[SerializeField]
		List<GameObject> gameObjectLOCs =  new List<GameObject>();

		[SerializeField]
		[OnValueChanged("OnChangeLOC")]
		int currentLOC = 0;

		[Header("Activty Data")]
		[SerializeField]
		Data.ActivityDataScriptable dataActvity;
		
		#endregion

		// === DATA-PRIVATE ===========================================================================

		#region DATA-PRIVATE

		// events
		Core.IEventManager eventManager;
		bool listernerInstalled = false;

		// player
		GameEngine.IPlayerStateActivityController playerStateAcitivty;

		#endregion

		// === LIFECYCLE ===========================================================================

		#region LIFECYCLE

		[Inject]
		public void Constructor(GameEngine.IPlayerStateActivityController playerStateAcitivty, Core.IEventManager eventManager)
		{
			this.playerStateAcitivty = playerStateAcitivty;
			this.eventManager = eventManager;
			EnableListeners();
		}

		void OnEnable()
		{
			EnableListeners();
		}

		void OnDisable()
		{
			DisableListeners();
		}

		// Start is called before the first frame update
		void Start()
		{
			OnChangeLOC();
		}

		// Update is called once per frame
		void Update()
		{

		}

		#endregion

		// === TRIGGERING ==============================================================================================================

		#region TRIGGERING

		/*
		 *
		 * Triggering
		 * 
		 */

		private void EnableListeners()
		{
			// listener on game event
			if (eventManager != null && listernerInstalled == false)
			{
				// activity data
				eventManager.AddListener<Data.Event_PlayerStateActivityDataChange>(OnActivityDataChange);
				listernerInstalled = true;
			}
		}

		private void DisableListeners()
		{
			// remove listener on game event
			if (eventManager != null && listernerInstalled == true)
			{
				// activity data
				eventManager.RemoveListener<Data.Event_PlayerStateActivityDataChange>(OnActivityDataChange);
				listernerInstalled = false;
			}
		}

		private void OnActivityDataChange(Data.Event_PlayerStateActivityDataChange ev)
		{
			var data = ((UnityEngine.Object, Data.IActivityFieldServer))ev.Data;

			SyncActivityDataFields(data.Item1, data.Item2);
		}

		private void SyncActivityDataFields(UnityEngine.Object obj, Data.IActivityFieldServer activityFieldServer)
		{
			if (dataActvity != null)
			{
				if (activityFieldServer.Id.Equals(dataActvity.Data.Id) == true)
				{
					CurrentLOC = (int)activityFieldServer.GetValue("LOC", Data.FieldSearchMethod.localPath);
				}
			}
			else
			{
				Debug.LogErrorFormat("{0} : data activity is null on gameobject {1}!!!", System.Reflection.MethodBase.GetCurrentMethod().Name, name);
			}
		}

		#endregion

		// === IMPLEMENTATION ===========================================================================

		#region IMPLEMENTATION

		void OnChangeLOC()
		{
			foreach (Transform tr in transform)
			{
				Destroy(tr.gameObject);
			}
			Instantiate(gameObjectLOCs[currentLOC], transform);
		}
		
		#endregion

		// === IGameObjectMultiLOC ===========================================================================

		#region IGameObjectMultiLOC

		public int CurrentLOC
		{
			get
			{
				return currentLOC;
			}
			set
			{
				if (value != currentLOC)
				{
					currentLOC = value;
					OnChangeLOC();
				}
			}
		}

		
		public int CountLOC
		{
			get
			{
				return gameObjectLOCs.Count;
			}
		}

		#endregion
	}
}
