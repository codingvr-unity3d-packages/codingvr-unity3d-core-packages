﻿using UnityEngine;
using Zenject;
using System.Collections;
using System;
using System.Collections.Generic;

namespace codingVR.GameEngine
{
	public class GameRootManager : MonoBehaviour, IGameRootManager
	{
		// === Game Object Def ===

		struct GameObjectDef
		{
			public GameObject gameObject;
			public string gameObjectParentName;

			public GameObjectDef(GameObject gameObject, string gameObjectParentName)
			{
				this.gameObject = gameObject;
				this.gameObjectParentName = gameObjectParentName;
			}
		}

		Dictionary<string, GameObjectDef> gameObjectDefs = new Dictionary<string, GameObjectDef>();

		// === U3D life cycle ===

		/*
		 * 
		 * Starting
		 * 
		 */

		void Awake()
		{
			UnityEngine.Object.DontDestroyOnLoad(this.gameObject);
		}

		// Start is called before the first frame update
		void Start()
		{
			Application.wantsToQuit += WantsToQuit;
		}

		/*
		 * 
		 * Final splash management
		 * 
		 */

		float showSplashTimeout = 2.0f;
		bool allowQuitting = false;

		bool WantsToQuit()
		{
			bool b = true;	
			
			// Don't allow the user to exit until we got permission in
			if (!allowQuitting)
			{
				b = false;
			}
			return b;
		}

		// On Application Quit
		void OnApplicationQuit()
		{
			// If we haven't already load up the final splash screen level
			//if (Application.loadedLevelName.ToLower() != "finalsplash")
			{
				StartCoroutine(DelayedQuit());
			}
		}

		IEnumerator DelayedQuit()
		{
			//Application.LoadLevel("finalsplash");

			// Wait for showSplashTimeout
			yield return new WaitForSeconds(showSplashTimeout);
			
			// then quit for real
			allowQuitting = true;
			Application.Quit();
		}

		// === GameEngine.IGameRootManager ===

		public void SaveGameObject(string key, GameObject gameObject)
		{
			gameObject.SetActive(false);
			GameObjectDef god = new GameObjectDef(gameObject, gameObject.transform.parent.name);
			gameObjectDefs.Add(key, god);
			gameObject.transform.parent = transform;
		}

		public void RestoreGameObject(string key)
		{
			if (gameObjectDefs.ContainsKey(key))
			{
				var gameObjectDef = gameObjectDefs[key];

				GameObject parent = GameObject.Find(gameObjectDef.gameObjectParentName);
				GameObject target = gameObjectDef.gameObject;

				if (parent != null && target != null)
				{
					target.transform.parent = parent.transform;
				}

				gameObjectDefs.Remove(key);
				target.SetActive(true);
			}
		}

		// game object
		public GameObject GameObject
		{
			get
			{
				return gameObject;
			}
		}
	}
}
