﻿using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TypeReferences;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace codingVR.GameEngine
{
	public class RuntimeObjects : MonoBehaviour, IRuntimeObjects
	{
		// === PRIVATE-DATA ===================================================================

		#region UNITY-LIFECYCLE

		private codingVR.Core.IEventManager eventManager;

		#endregion

		// === DATA-UI ===========================================================================

		#region DATA-UI

		[Header("Screen Play Group")]
		[Tooltip
			(
			"Screen Play Group identifies the owner of this pesistent runtime objects manager.\n"
			)]
		[SerializeField]
		GameEngine.ScreenPlayGroup screenPlayGroup;

		#endregion

		// === UNITY-LIFECYCLE ===================================================================

		#region UNITY-LIFECYCLE

		[Inject]
		public void Contruct(codingVR.Core.IEventManager eventManager)
		{
			this.eventManager = eventManager;
		}


		// Start is called before the first frame update
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{

		}

		#endregion

		// === IRuntimeObjects ===================================================================

		#region IRuntimeObjects

		void SendMessage(Event_RuntimeObjectsChange.Operation type, string objectName)
		{
			if (screenPlayGroup != null)
			{
				this.eventManager.QueueEvent(new Event_RuntimeObjectsChange((screenPlayGroup.name, name, objectName), type));
			}
			else
			{
				Debug.LogErrorFormat("{0} : can't send message for object {1} since screenPlayGroup is null !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, objectName);
			}
		}

		public bool AddNewObject(GameObject newObject)
		{
			bool b = false;
			var foundObject = gameObject.FindObjectEvenInactive(newObject.name);
			if (foundObject == null)
			{
				newObject.transform.parent = this.transform;
				SendMessage(Event_RuntimeObjectsChange.Operation.insert, newObject.name);
				b = true;
			}
			return b;
		}

		public bool DestroyObject(GameObject newObject)
		{
			bool b = false;
			var foundObject = gameObject.FindObjectEvenInactive(newObject.name);
			if (newObject.CompareTo(foundObject) == true)
			{
				Destroy(newObject);
				SendMessage(Event_RuntimeObjectsChange.Operation.destroy, newObject.name);
				b = true;
			}
			return b;
		}

		public void DestroyAll()
		{
			gameObject.DestroyChildren(false);
		}

		#endregion
	}
}
