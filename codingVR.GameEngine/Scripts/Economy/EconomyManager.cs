﻿using codingVR.Core;
using codingVR.Data;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using Zenject;

namespace codingVR.GameEngine
{
	public class EconomyManager : MonoBehaviour , IEconomyManager
	{
		[SerializeField]
		int experience = 1;

		[SerializeField]
		int context = 1;

		[SerializeField]
		int baseValue = 5;

		[System.Serializable]
		public struct BaseValue
		{
			public string nameValue;
			public int baseValue;
		}

		[SerializeField]
		public List<BaseValue> listBaseValue = new List<BaseValue>();

		IInventoryManager inventoryManager;
		private Core.IEventManager eventManager;
		private IPlayerStateActivityController playerStateActivity;

		private RandomHash randomHash;

		// === UNITY-LIFE-CYCLE ==========================================================

		#region UNITY-LIFE-CYCLE

		[Inject]
		public void Constructor(IInventoryManager inventoryManager, IEventManager eventManager, IPlayerStateActivityController playerStateActivity)
		{
			this.inventoryManager = inventoryManager;
			this.eventManager = eventManager;
			this.playerStateActivity = playerStateActivity;
		}

		private void OnEnable()
		{
			// add listener on event and activity planner
			eventManager.AddListener<Data.Event_PlayerStateEventAndActiviyPlannerRequestActivityRewards>(OnEventAndActiviyPlannerRequestActivityRewards);

			randomHash = new RandomHash(10);
		}

		private void OnDisable()
		{
			// remove listener on event and activity planner
			eventManager.RemoveListener<Data.Event_PlayerStateEventAndActiviyPlannerRequestActivityRewards>(OnEventAndActiviyPlannerRequestActivityRewards);
		}

		#endregion

		// === GAME-LIFE-CYCLE ==========================================================

		#region GAME-LIFE-CYCLE

		void OnEventAndActiviyPlannerRequestActivityRewards(Data.Event_PlayerStateEventAndActiviyPlannerRequestActivityRewards ev)
		{
			var activityInstance = playerStateActivity.FindActivityInstance(ev.Data as string);
			if ((activityInstance.SubStatusFlags & ActivityInstanceSubStatusFlags.activityRewarded) != ActivityInstanceSubStatusFlags.activityRewarded)
			{
				activityInstance.SubStatusFlags |= ActivityInstanceSubStatusFlags.activityRewarded;
				RewardActivity(playerStateActivity.FindActivityInstance(ev.Data as string));
			}
			else
			{
				Debug.LogErrorFormat("{0} : ActivityInstanceSubStatusFlags.activityRewarded already signaled !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
		}

		#endregion

		// === IMPLEMENTATION ==========================================================

		#region IMPLEMENTATION

		void RewardActivity(Data.IActivityInstance iActivityInstance)
		{
			List<ItemScriptable> itemsGift = iActivityInstance.ItemsGift;
			

			float activityInstanceHash = (float)iActivityInstance.IdInstance.GetHashCode();
			float passageValue = iActivityInstance.InstancePassage;

			if (itemsGift.Count > 0)
			{
				// create a list o new items not yet present in the inventory
				List<ItemScriptable> itemsGiftRemain = new List<ItemScriptable>();
				foreach (ItemScriptable it in itemsGift)
				{
					if (inventoryManager.HasItem(it) == false)
					{
						itemsGiftRemain.Add(it);
					}
				}

				// request for item gift
				ItemScriptable requestedGiftItem = null;
				if (itemsGiftRemain.Count > 0)
				{
					var pos = randomHash.Range(activityInstanceHash, passageValue, 0, itemsGiftRemain.Count);
					requestedGiftItem = itemsGiftRemain[pos];
					if (requestedGiftItem != null)
					{
						RequestForGiftItem(iActivityInstance, requestedGiftItem);
					}
					else
					{

						Debug.LogErrorFormat("{0} : requested gift at position {1} is null !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, pos);
					}
				}

				// request for money; calculate if a supplementary reward of money must be given
				{
					IActivityClass iactivityClass = playerStateActivity.FindActivityClass(iActivityInstance.IdClass);
					int money = fMoney((int)iactivityClass.Importance + (int)iactivityClass.Regularity + (int)iactivityClass.Size);
					int moneyItem = requestedGiftItem?.GetPrice() ?? 0;
					money = money - moneyItem;
					Debug.Log("Money credited : " + money);
					if (money > 0)
					{
						RequestForCredit(iActivityInstance, money);
					}
				}
			}

			else
			{
				IActivityClass iactivityClass = playerStateActivity.FindActivityClass(iActivityInstance.IdClass);
				int money = fMoney((int)iactivityClass.Importance + (int)iactivityClass.Regularity + (int)iactivityClass.Size);

				if (money > 0)
				{
					RequestForCredit(iActivityInstance, money);
				}
			}
		}

		/*
		 * compute money according to inout criteria
		 * 
		 */

		int fPrice()
		{
			return experience + context;
		}

		int fMoney(int points)
		{
			return baseValue * fPrice();
		}

		/*
		 * request fro new credit
		 * 
		 */

		void RequestForCredit(Data.IActivityInstance iActivityInstance, int money)
		{
			var token = inventoryManager.CreateToken();
			inventoryManager.AddMoney(token, money);
			iActivityInstance.LastMoneyCredited = money;
		}

		/*
		 * 
		 * ???????????????????????????????????????????
		 * 
		 */

		bool RequestForCharge(int money)
		{

			bool b = RequestForCheck(money);
			if(b == true)
			{
				var token = inventoryManager.CreateToken();
				inventoryManager.RemoveMoney(token, money);
			}

			return b;
		}

		bool RequestForCheck(int money)
		{
			bool check = false;

			if (inventoryManager.CheckMoney(money)) check = true;

			return check;
		}


		/*
		 * 
		 * request for gift item
		 * 
		 */

		bool RequestForGiftItem(Data.IActivityInstance iActivityInstance, ItemScriptable item)
		{
			bool b = inventoryManager.HasItem(item);

			if (!b)
			{
				var token = inventoryManager.CreateToken();
				inventoryManager.AddItem(token, item);
				iActivityInstance.LastItemGiftRewared = item;
			}

			return !b;
		}

		/*
		 * 
		 * ???????????????????????????????????????????
		 * 
		 */

		public int CalculateValue(int points, string nameValue)
		{
			int finalValue = 0;
			int baseValue = 0;

			foreach (BaseValue bv in listBaseValue)
			{
				if (bv.nameValue == nameValue) baseValue = bv.baseValue;
			}

			finalValue = baseValue * points;

			return finalValue;
		}

		#endregion
		
		// === IEconomyManager ==========================================================

		#region IEconomyManager
			
		public int MoneyRewardByID(string idInstance)
		{
			int money = 0;
			IActivityInstance iActivityInstance = playerStateActivity.FindActivityInstance(idInstance);
			if (iActivityInstance != null)
			{
				money = iActivityInstance.LastMoneyCredited;
			}
			return money;
		}

		public ItemScriptable GetItemRewardByID(string idInstance)
		{
			ItemScriptable item = null;
			IActivityInstance iActivityInstance = playerStateActivity.FindActivityInstance(idInstance);
			if (iActivityInstance != null)
			{
				item = iActivityInstance.LastItemGiftRewared;
			}
			return item;
		}

		#endregion
	}
}