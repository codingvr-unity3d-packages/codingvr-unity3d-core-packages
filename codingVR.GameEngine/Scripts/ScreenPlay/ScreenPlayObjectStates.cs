﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
    [RequireComponent(typeof(ScreenPlay))]
    public class ScreenPlayObjectStates : MonoBehaviour
    {
        [OnChangingCall("NewStateChanging")]
        public string newState = null;
        ScreenPlay screenPlay;

        private void Awake()
        {
            screenPlay = FindObjectOfType<ScreenPlay>();
        }

        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            if (newState != null)
            {
                bool bFound = false;
                var array = GameObject.FindObjectsOfType<ScreenPlayObject>();
                foreach (var it in array)
                {
                    if (it.gameObject.name == newState)
                    {
                        //screenPlay.SwitchingObject(screenPlay. GameObject from, it.gameObject);
                    }
                }
                newState = null;
            }
        }

        public void NewStateChanging()
        {
            Debug.Log("I have changed to" + newState);
        }
    }
}
