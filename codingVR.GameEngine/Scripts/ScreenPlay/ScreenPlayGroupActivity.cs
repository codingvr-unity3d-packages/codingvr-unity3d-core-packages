﻿using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.GameEngine
{
	[RequireComponent(typeof(ScreenPlayGroupDebug))]
	[RequireComponent(typeof(ScreenPlayGroup))]
	public class ScreenPlayGroupActivity : MonoBehaviour
	{
		// === DATA-INTERNAL ============================================================

		#region DATA-INTERNAL

		GameEngine.IPlayerStateActivityController playerStateActivityController;
		Core.IEventManager eventManager;

		#endregion

		// === DATA-UI ============================================================

		#region DATA-UI

		[SerializeField]
		string idClass;

		[SerializeField]
		string idActivityInstanceParent;

		void ActivityInstanceSubStatusChange()
		{
			var instance = playerStateActivityController.FindActivityInstance(gameObject.name);
			if (instance != null)
			{
				Debug.LogErrorFormat("{0} : Can't change class from activity instance {1} after registration !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, instance.IdInstance);
			}
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		void BuildActivity()
		{
			playerStateActivityController.BuildActivity(gameObject.name, idActivityInstanceParent, idClass);
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		public void RegisterActivity()
		{
			playerStateActivityController.RegisterActivity(gameObject.name, true);
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		public void CreateActivity()
		{
			playerStateActivityController.CreateActivity(gameObject.name, true);
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		public void StartActivity()
		{
			playerStateActivityController.StartActivity(gameObject.name, true);
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		public void CompleteActivity()
		{
			playerStateActivityController.CompleteActivity(gameObject.name, true);
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		public void StopActivity()
		{
			playerStateActivityController.StopActivity(gameObject.name, true);
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		private void ResetActivityGamePlay()
		{
			playerStateActivityController.ResetActivityGameplay(gameObject.name);
		}

		#endregion

		// === LIFE-CYCLE ============================================================

		#region LIFE-CYCLE


		/*
		 *
		 * life cycle
		 * 
		 */

		[Inject]
		public void Constructor(GameEngine.IPlayerStateActivityController playerStateAcitivty, Core.IEventManager eventManager)
		{
			this.playerStateActivityController = playerStateAcitivty;
			this.eventManager = eventManager;
		}

		void Awake()
		{
			var screenPlayGroup = GetComponent<IScreenPlayGroup>();
			if (screenPlayGroup != null)
			{
				if (screenPlayGroup.ActiveGroup == true)
				{
					Debug.LogErrorFormat("{0} : By default the ActiveGroup property should be set to false when ScreenPlayGroupActivity is bound on {1} !!", System.Reflection.MethodBase.GetCurrentMethod().Name, name);
					screenPlayGroup.ActiveGroup = false;
				}
			}
		}

		// Start is called before the first frame update
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{

		}

		private void OnEnable()
		{
			EnableListeners();
		}

		private void OnDisable()
		{
			DisableListeners();
		}

		#endregion

		// === IMPLEMENTATION-UTILS ============================================================

		#region IMPLEMENTATION-UTILS

		bool ContainsIdInstanceActivity(string groupTagName)
		{
			return gameObject.name.Contains(groupTagName);
		}
		
		bool IsThisGroup(GameEngine.Event_ScreenPlayTagBase ev)
		{
			var groupData = ev.Data as Tuple<string, Data.Tag>;
			return ContainsIdInstanceActivity(groupData.Item2.ToString());
		}

		bool IsThisGroup(Data.Event_PlayerStateActivityBase ev)
		{
			var groupData = ev.Data as string;
			return ContainsIdInstanceActivity(groupData);
		}

		#endregion

		// === TRIGGERING ============================================================

		#region TRIGGERING

		/*
		*
		* Triggering
		* 
		*/

		private void EnableListeners()
		{
			// fire event on open activity group 
			eventManager.QueueEvent(new Event_ScreenPlayGroupActivityOpen(new Tuple<string, Data.Tag>(this.gameObject.scene.name, Data.Tag.CreateTag(name))));

			// add listener on player state activity event
			eventManager.AddListener<Data.Event_PlayerStateActivitiesJustStarting>(OnActivitiesJustStarting);
			eventManager.AddListener<Data.Event_PlayerStateActivityRegister>(OnActivityRegister);
			eventManager.AddListener<Data.Event_PlayerStateActivityCreate>(OnActivityCreate);
			eventManager.AddListener<Data.Event_PlayerStateActivityStart>(OnActivityStart);
			eventManager.AddListener<Data.Event_PlayerStateActivityComplete>(OnActivityComplete);
			eventManager.AddListener<Data.Event_PlayerStateActivityStop>(OnActivityStop);

			// setup debug color
			SetupDebugColorAcoordingToStatus();
		}

		private void DisableListeners()
		{
			// fire event close activity group 
			eventManager.QueueEvent(new Event_ScreenPlayGroupActivityClose(new Tuple<string, Data.Tag>(this.gameObject.scene.name, Data.Tag.CreateTag(name))));

			// listener on player state activity event
			eventManager.RemoveListener<Data.Event_PlayerStateActivitiesJustStarting>(OnActivitiesJustStarting);
			eventManager.RemoveListener<Data.Event_PlayerStateActivityRegister>(OnActivityRegister);
			eventManager.RemoveListener<Data.Event_PlayerStateActivityCreate>(OnActivityCreate);
			eventManager.RemoveListener<Data.Event_PlayerStateActivityStart>(OnActivityStart);
			eventManager.RemoveListener<Data.Event_PlayerStateActivityComplete>(OnActivityComplete);
			eventManager.RemoveListener<Data.Event_PlayerStateActivityStop>(OnActivityStop);
		}
		
		void OnActivitiesJustStarting(Data.Event_PlayerStateActivitiesJustStarting ev)
		{
			SetupDebugColorAcoordingToStatus();
		}
		
		void OnActivityCreate(Data.Event_PlayerStateActivityCreate ev)
		{
			if (IsThisGroup(ev))
			{
				SetupDebugColorAcoordingToStatus();
			}
		}

		void OnActivityStart(Data.Event_PlayerStateActivityStart ev)
		{
			if (IsThisGroup(ev))
			{
				SetupDebugColorAcoordingToStatus();
			}
		}

		void OnActivityStop(Data.Event_PlayerStateActivityStop ev)
		{
			if (IsThisGroup(ev))
			{
				SetupDebugColorAcoordingToStatus();
			}
		}

		void OnActivityComplete(Data.Event_PlayerStateActivityComplete ev)
		{
			if (IsThisGroup(ev))
			{
				SetupDebugColorAcoordingToStatus();
			}
		}

		void OnActivityRegister(Data.Event_PlayerStateActivityRegister ev)
		{
			if (IsThisGroup(ev))
			{
				SetupDebugColorAcoordingToStatus();
			}
		}

		#endregion

		// === IMPLEMENTATION ============================================================

		#region IMPLEMENTATION

		void SetupDebugColorAcoordingToStatus()
		{
			var debug = GetComponent<ScreenPlayGroupDebug>();

			Color ColorAlpha(Color color)
			{
				color.a = 0.5f;
				return color;
			}

			var activityInstance = playerStateActivityController.FindActivityInstance(gameObject.name);
			if (activityInstance != null)
			{
				switch (activityInstance.StatusFlags)
				{

					case Data.ActivityInstanceLifeCycleStatus.isActivityRegistered:
						{
							debug.GroupColor = ColorAlpha(Color.red);
						}
						break;

					case Data.ActivityInstanceLifeCycleStatus.isActivityCreated:
						{
							debug.GroupColor = ColorAlpha(new Color(255.0f / 255.0f, 165.0f / 255.0f, 0.0f)); // orange
						}
						break;


					case Data.ActivityInstanceLifeCycleStatus.isActivityStarted:
						{
							debug.GroupColor = ColorAlpha(Color.green);
						}
						break;

					case Data.ActivityInstanceLifeCycleStatus.isActivityCompleted:
						{
							debug.GroupColor = ColorAlpha(Color.blue);
						}
						break;

					case Data.ActivityInstanceLifeCycleStatus.isActivityStopped:
					default:
						{
							debug.GroupColor = ColorAlpha(Color.gray);
						}
						break;
				}
			}
		}
		#endregion
	}
}
