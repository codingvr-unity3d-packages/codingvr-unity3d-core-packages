﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	using global::GameCreator.Core;
	using NaughtyAttributes.Editor;
	using System;
	using UnityEditor;
	using UnityEditor.AI;
	using UnityEditor.IMGUI.Controls;
	using UnityEditor.SceneManagement;
	using UnityEditor.UIElements;
	using UnityEngine.AI;
	using UnityEngine.UIElements;

	[CustomEditor(typeof(ScreenPlayObjectArea))]
	public class ScreenPlayObjectAreaEditor : Editor
	{
		// handle
		BoxBoundsHandle boxBoundsHandle = new BoxBoundsHandle();

		// vector3 field
		Vector3Field sizeUI;

		// utility method
		static void HorizontalLine(Color color)
		{
			GUIStyle horizontalLine;
			horizontalLine = new GUIStyle();
			horizontalLine.normal.background = EditorGUIUtility.whiteTexture;
			horizontalLine.margin = new RectOffset(0, 0, 4, 4);
			horizontalLine.fixedHeight = 1;

			var c = GUI.color;
			GUI.color = color;
			GUILayout.Box(GUIContent.none, horizontalLine);
			GUI.color = c;
		}
		
		// inspector gui
		public override void OnInspectorGUI()
		{
			ScreenPlayObjectArea Manager = (ScreenPlayObjectArea)target;

			// update
			serializedObject.Update();

			// navigation mesh setup
			Manager.UseNavigation = EditorGUILayout.Toggle("Use navigation", Manager.UseNavigation);
			if (Manager.UseNavigation)
			{
				NavMeshComponentsGUIUtility.AgentTypePopup("Agent id", serializedObject.FindProperty("agentID"));
				NavMeshComponentsGUIUtility.AreaPopup("Area type", serializedObject.FindProperty("areaType"));
			}

			// trigger setup
			HorizontalLine(Color.black);
			Manager.UseTrigger = EditorGUILayout.Toggle("Use trigger", Manager.UseTrigger);
			if (Manager.UseTrigger)
			{
				// trigger type
				Manager.TriggerType = (ScreenPlayObjectArea.TriggerTypeEnum)EditorGUILayout.EnumPopup(Manager.TriggerType);
				// target game object
				EditorGUILayout.PropertyField(serializedObject.FindProperty("targetGameObject"));
			}

			// box setup
			if (Manager.UseNavigation || Manager.UseTrigger)
			{
				HorizontalLine(Color.black);

				Manager.Center = EditorGUILayout.Vector3Field("Center Area", Manager.Center);
				Manager.Size = EditorGUILayout.Vector3Field("Size Area", Manager.Size);
			}

			// make scene dirty
			if (GUI.changed)
			{
				EditorUtility.SetDirty(target);
				EditorSceneManager.MarkSceneDirty(Manager.gameObject.scene);
			}

			// apply modified properties
			serializedObject.ApplyModifiedProperties();
		}
		
		// scene gui
		public void OnSceneGUI()
		{
			// target
			var screenPlayObjectArea = (target as ScreenPlayObjectArea);

			// specify handle
			boxBoundsHandle.size = screenPlayObjectArea.Size;
			boxBoundsHandle.center = screenPlayObjectArea.Center + screenPlayObjectArea.transform.position;
			boxBoundsHandle.handleColor = Color.red;
			boxBoundsHandle.wireframeColor = Color.black;
		
			// begin edition
			EditorGUI.BeginChangeCheck();
			boxBoundsHandle.DrawHandle();
			if (EditorGUI.EndChangeCheck())
			{
				// record the target object before setting new values so changes can be undone/redone
				Undo.RecordObject(screenPlayObjectArea, "Change Bounds");

				// update UI
				var center = boxBoundsHandle.center - screenPlayObjectArea.transform.position;
				var size = boxBoundsHandle.size;

				// czntzer / size
				screenPlayObjectArea.Center = center;
				screenPlayObjectArea.Size = size;
			}
		}
	}
}