﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace codingVR.GameEngine
{
	public class ScreenPlayGroup : MonoBehaviour, IScreenPlayGroup
	{
		// === DATA-PRIVATE ============================================================

		#region DATA-PRIVATE

		GameObject gameObjectActions;

		Data.Tag tagTriggerOnEnter = null;

		#endregion

		// === DATA-UI ============================================================

		#region DATA-UI

		// enter
		[Header("Actions")]
		[SerializeField]
		global::GameCreator.Core.Actions __action_enter__;

		// leave
		[SerializeField]
		global::GameCreator.Core.Actions __action_leave__;

		[SerializeField]
		[Header("Group life cycle")]
		[ReadOnly]
		bool isBusy = false;

		[Serializable]
		public class PlayerStateCollection : Core.cvrSerializableDictionary<int, GameObject> { }
		[SerializeField]
		PlayerStateCollection tagSet = new PlayerStateCollection();

		[Header("Group state at starting")]
		[SerializeField]
		bool activeGroup = true;

		#endregion

		// === UNITY-LIFE-CYCLE ============================================================

		#region UNITY-LIFE-CYCLE

		DiContainer diContainer;
		GameEngine.IScreenPlay screenPlay;
		Core.IEventManager eventManager;
			

		[Inject]
		public void Constructor(DiContainer diContainer, GameEngine.IScreenPlay screenPlay, Core.IEventManager eventManager)
		{
			this.screenPlay = screenPlay;
			this.eventManager = eventManager;
			this.diContainer = diContainer;
		}


		// Start is called before the first frame update
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{

		}

		#endregion

		// === IMPLEMENTATION ===============================================================

		#region IMPLEMENTATION

		// === Actions ==================================================================

		void ExecuteActions(global::GameCreator.Core.Actions actions, UnityAction onFinish, UnityAction<GameObject> onExecute)
		{
			if (actions != null)
			{
				//Events form GameCreator Actions
				actions.onFinish.AddListener(onFinish);
				actions.onExecute.AddListener(onExecute);
				actions.Execute();
			}
		}

		#endregion

		// === IScreenPlayGroup =====================================================

		#region IScreenPlayGroup


		public bool IsBusy
		{
			get
			{
				return isBusy;
			}
		}

		public bool Started
		{
			get
			{
				return tagSet.Count > 0;
			}
		}

		public bool Contains(GameObject target)
		{
			return tagSet.Contains(target.GetInstanceID());
		}

		public Data.Tag TagTriggerOnEnter => tagTriggerOnEnter;

		public bool ActiveGroup
		{
			get
			{
				return activeGroup;
			}
			set
			{
				activeGroup = value;
			}
		}
		
		public IEnumerator ExecuteScreenPlayGroup(bool active, GameObject target)
		{
			if (ActiveGroup == true || Started == true)
			{
				if (isBusy == true && active == false)
				{
					__action_enter__.Stop();
					yield return new WaitWhile(() => __action_enter__.actionsList.isExecuting == true);
				}

				/*		


					yield return new WaitUntil(() => __action_enter__.actionsList.isExecuting == false);
					yield return new WaitUntil(() => __action_leave__.actionsList.isExecuting == false);
					yield return new WaitUntil(() => busy == false); 


				*/

				isBusy = true;

				/*
				if (target.name == "#minigame-connect" && active == true)
					Debug.LogError("#minigame-connect true" );
				*/

				if (active == true)
				{
					if (tagSet.ContainsKey(target.GetInstanceID()) == false)
					{
						tagSet.Add(target.GetInstanceID(), target);

						if (tagSet.Count == 1)
						{
							// tag trigger;
							tagTriggerOnEnter = Data.Tag.CreateTag(target.name);

							// Events form GameCreator Actions
							void OnFinish()
							{
								__action_enter__.gameObject.SetActive(false);
								isBusy = false;
							}

							void OnExecute(GameObject gameObject)
							{
								isBusy = true;
							}

							__action_enter__.gameObject.SetActive(true);
							ExecuteActions(__action_enter__, OnFinish, OnExecute);
							yield return new WaitUntil(() => isBusy == false);

							// tag event start
							eventManager.QueueEvent(new Event_ScreenPlayTagGroupEnter(new Tuple<string, Data.Tag>(this.gameObject.scene.name, Data.Tag.CreateTag(name))));
						}
					}
				}
				else
				{
					if (tagSet.ContainsKey(target.GetInstanceID()) == true)
					{
						tagSet.Remove(target.GetInstanceID());

						/*
						if (target.name == "#minigame-camera")
							Debug.LogError("ExecuteScreenPlayGroup #minigame-camera tagSet count" + tagSet.Count);
							*/

						if (tagSet.Count == 0)
						{
							// stop and wait
							target.GetComponent<IScreenPlayObject>().Stop();
							yield return new WaitUntil(() => target.GetComponent<IScreenPlayObject>().IsBusy == false);

							// Events form GameCreator Actions
							void OnFinish()
							{
								__action_leave__.gameObject.SetActive(false);
								isBusy = false;
							}
							void OnExecute(GameObject gameObject)
							{
								isBusy = true;

							}
							// execute actions
							__action_leave__.gameObject.SetActive(true);
							ExecuteActions(__action_leave__, OnFinish, OnExecute);
							yield return new WaitUntil(() => isBusy == false);

							// tag event leave
							eventManager.QueueEvent(new Event_ScreenPlayTagGroupLeave(new Tuple<string, Data.Tag>(this.gameObject.scene.name, Data.Tag.CreateTag(name))));

							// reset tag trigger enter
							tagTriggerOnEnter = null;
						}
					}
				}
			}
			yield return null;
		}

		#endregion
	}
}