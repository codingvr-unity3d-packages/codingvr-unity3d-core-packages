﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCreator.GameEngine
{
    public class ScreenPlayObjectDebug : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        private void Awake()
        {
            Debug.LogWarning("Awake " + name + " from " + transform.parent.name);
        }

        private void OnEnable()
        {
            Debug.LogWarning("On Enable object " + name + " from " + transform.parent.name);
        }
    }
}
