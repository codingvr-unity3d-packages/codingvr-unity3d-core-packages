﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	public class ScreenPlayRootKey : MonoBehaviour, IScreenPlayRootKey
	{
		// enter
		[SerializeField]
		string identifier;

		public string Identifier
		{
			get
			{
				return identifier;
			}
			set
			{
				if (identifier != value)
				{
					identifier = value;
				}
			}
		}

		public string LinkToRoot(string tag)
		{
			return Identifier + "." + tag;
		}
	}
}
