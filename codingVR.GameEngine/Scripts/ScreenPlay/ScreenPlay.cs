﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Zenject;

namespace codingVR.GameEngine
{
    public class ScreenPlay : MonoBehaviour, IScreenPlay
    {
        [Serializable]
        public class ScreenPlaySub
        {
            // screen play properties
            [SerializeField]
            ScreenPlayPropertiesStruct screenPlayProperties = new ScreenPlayPropertiesStruct("", "Loading", Data.Tags.Welcome.ToString());

            // first tag triggered
            private const string startTag = "#__start__";

            // ** ScreenPlay Object Tag **

            // caching gameobject according to lastScreenPlayObjectTag
            private GameObject lastScreenPlayObjectGameObject;
            public string LastScreenPlayObjectTag { get { return screenPlayProperties.lastScreenPlayObjectTag; } }

            // ** ScreenPlay Master **

            // screen play master
            private ScreenPlay screenPlayMaster;

            /*
			 * 
			 * init
			 * 
			 */

            public void Setup(ScreenPlay screenPlayMaster)
            {
                this.screenPlayMaster = screenPlayMaster;
            }

            public void Setup(ScreenPlay screenPlayMaster, ScreenPlayPropertiesStruct screenPlayProperties)
            {
                this.screenPlayMaster = screenPlayMaster;
                this.screenPlayProperties = screenPlayProperties;
            }

            /*
			 * 
			 * launching screen play
			 * 
			 */

            // Switching Scene
            public IEnumerator LaunchScreenPlayScene(GameEngine.Event_ScreenPlaySceneStarted.ScreenPlayMode screenPlayMode, string toTag = "")
            {
                SetupScreenPlayNextTag(toTag);
                yield return LoadingScreenPlayScene(screenPlayProperties);

                screenPlayMaster.eventManager.QueueEvent(new GameEngine.Event_ScreenPlaySceneStarted(screenPlayMode));

                yield return null;
            }

            // configure next tag for next time
            public void SetupScreenPlayNextTag(string toTag)
            {
                if (String.IsNullOrEmpty(toTag) == false)
                    screenPlayProperties.lastScreenPlayObjectTag = toTag;
            }

            // Switching Scene
            public IEnumerator SwitchingScreenPlay(GameEngine.Event_ScreenPlaySceneStarted.ScreenPlayMode screenPlayMode, ScreenPlayPropertiesStruct screenPlayProperties)
            {
                var oldScreenPlayProperties = this.screenPlayProperties;
                this.screenPlayProperties = screenPlayProperties;
                yield return LoadingAndUnloadScreenPlayScene(oldScreenPlayProperties, screenPlayProperties, true);

                screenPlayMaster.eventManager.QueueEvent(new GameEngine.Event_ScreenPlaySceneStarted(screenPlayMode));

                yield return null;
            }

            // Close screen play
            public IEnumerator CloseScreenPlay(bool switching)
            {
                yield return UnloadCurrentScreenPlayScene(screenPlayProperties, switching);
            }

            // current scene play state
            public ScreenPlayPropertiesStruct CurrentScreenPlayProperties()
            {
                return screenPlayProperties;
            }


            /*
			 * 
			 * Tag utilities
			 * 
			 */

            public GameObject FindScreenPlay(GameObject fromParent)
            {
                var array = fromParent.RecursiveFindChildrenByTag(new string[] { "ScreenPlay" });
                return array.Count > 0 ? array[0] : null;
            }

            // find screen play object from tag
            public GameObject FindTag(Data.Tag tag)
            {
                GameObject screenPlayObject = null;
                // tag name
                if (tag != null)
                {
                    var tagName = tag.ToString();
                    screenPlayObject = GameObjectExtensions.FindObjectFromTag("ScreenPlay", tagName); // gameObject.FindObject(tagName);
                }
                else
                {
                    Debug.LogErrorFormat("{0} : tag is null !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
                }
                return screenPlayObject;
            }

            public GameObject FindTagPath(Data.TagPath tagPath)
            {
                bool predicate(GameObject go)
                {
                    bool b = false;
                    var c = go.GetComponent<IScreenPlayRootKey>();
                    if (c != null)
                    {
                        if (c.Identifier == tagPath.identifier)
                        {
                            b = true;
                        }
                    }
                    return b;
                }

                GameObject tagGameObject = null;
                // tag name
                if (string.IsNullOrEmpty(tagPath.identifier) == false)
                {
                    var screenPlays = GameObject.FindGameObjectsWithTag("ScreenPlay");
                    var screenPlay = screenPlays.FirstOrDefault(predicate);
                    if (screenPlay != null)
                    {
                        tagGameObject = FindChildTagFrom(screenPlay, tagPath.tag);
                    }
                }
                else
                {
                    tagGameObject = FindTag(tagPath.tag);
                }
                return tagGameObject;
            }

            public List<GameObject> FindChildrenTagFrom(GameObject fromParent, Data.Tag tag)
            {
                string[] s = { tag.ToString() };
                return fromParent.RecursiveFindChildren(s);
            }

            public GameObject FindChildTagFrom(GameObject fromParent, Data.Tag tag)
            {
                GameObject gameObject = null;
                string[] s = { tag.ToString() };
                var children = fromParent.RecursiveFindChildren(s);
                if (children.Count > 0)
                {
                    gameObject = children[0];
                }
                return gameObject;
            }

            // find first screen play object parent  from object
            public GameObject FindParentTagFrom(GameObject fromGameObject)
            {
                IScreenPlayObject screenPlayObject = null;
                Transform parent = fromGameObject.transform;
                bool bParentNotFound = true;
                while (bParentNotFound == true && parent != null)
                {
                    parent = parent.parent;
                    if (parent != null)
                    {
                        var so = parent.GetComponent<IScreenPlayObject>();
                        if (so != null || parent.tag == "ScreenPlay")
                        {
                            bParentNotFound = false;
                            screenPlayObject = so;
                        }
                    }
                }
                return screenPlayObject != null ? screenPlayObject.gameObject : null;
            }

            // Find runtime objects
            public IRuntimeObjects FindRuntimeObjects(GameObject gameObject)
            {
                var parentTag = FindParentTagFrom(gameObject);
                var screenPlayObject = parentTag?.GetComponent<IScreenPlayObject>();
                return screenPlayObject?.RuntimeObjects;
            }

            // find screen play object from tag
            public (Vector3, bool) PositionTag(Data.Tag tag)
            {
                Vector3 pos = Vector3.zero;
                bool b = false;
                // tag name
                var goTag = FindTag(tag);
                if (goTag != null)
                {
                    pos = goTag.transform.position;
                    b = true;
                }
                return (pos, b);
            }

            // find object from tag & name
            public static IScreenPlayLinkObject FindTagPairFromUnityTag(string tag, Data.TagPair tagPair)
            {
                var screenPlays = GameObject.FindGameObjectsWithTag(tag);
                foreach (var it in screenPlays)
                {
                    IScreenPlayLinkObject[] trs = it.GetComponentsInChildren<IScreenPlayLinkObject>(true);
                    foreach (var t in trs)
                    {
                        if (t.TagPair?.Equals(tagPair) == true)
                        {
                            return t;
                        }
                    }
                }
                return null;
            }

            // find screen play object from tag
            public IScreenPlayLinkObject FindTagPair(Data.TagPair tagPair)
            {
                IScreenPlayLinkObject screenPlayLinkObject = null;
                // tag name
                if (tagPair != null)
                {
                    screenPlayLinkObject = FindTagPairFromUnityTag("ScreenPlay", tagPair); // gameObject.FindObject(tagName);
                }
                else
                {
                    Debug.LogErrorFormat("{0} : tag is null !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
                }
                return screenPlayLinkObject;
            }

            // find screen play object from tag
            public PathTagStruct? PathTag(Data.TagPair tagPair)
            {
                Vector3 start = Vector3.zero;
                bool b = false;

                // tag start
                var goTagFirst = FindTag(tagPair.First);
                if (goTagFirst != null)
                {
                    start = goTagFirst.transform.position;
                    b = true;
                }

                // tag mid
                Vector3? mid = null;
                // tag end
                Vector3? end = null;

                // we get a ScreenPlayLinkObject
                IScreenPlayLinkObject goTagSecond = FindTagPair(tagPair);
                if (goTagSecond != null)
                {
                    // we found a ScreenPlayLinkObject
                    mid = goTagSecond.GameObject.transform.position;
                }
                else
                {
                    // ScreenPlayLinkObject not found, we make a path from tag
                    (Vector3 pos, bool findEnd) = PositionTag(tagPair.Second);
                    if (findEnd)
                    {
                        end = pos;
                    }
                }

                return b == true ? (PathTagStruct?)new PathTagStruct(goTagSecond, start, mid, end) : null;
            }

            // find screen play object from tag path
            public (Vector3, bool) PositionTagPath(Data.TagPath tagPath)
            {
                Vector3 pos = Vector3.zero;
                bool b = false;
                // tag name
                var goTag = FindTagPath(tagPath);
                if (goTag != null)
                {
                    pos = goTag.transform.position;
                    b = true;
                }
                return (pos, b);
            }

            // find object from tag path & name
            public static IScreenPlayLinkObject FindTagPathPairFromUnityTag(string tag, Data.TagPathPair tagPathPair)
            {
                var screenPlays = GameObject.FindGameObjectsWithTag(tag);
                foreach (var it in screenPlays)
                {
                    IScreenPlayLinkObject[] trs = it.GetComponentsInChildren<IScreenPlayLinkObject>(true);
                    foreach (var t in trs)
                    {
                        if (t.TagPathPair?.Equals(tagPathPair) == true)
                        {
                            return t;
                        }
                    }
                }
                return null;
            }

            // find screen play object from tag path
            public IScreenPlayLinkObject FindTagPathPair(Data.TagPathPair tagPathPair)
            {
                IScreenPlayLinkObject screenPlayLinkObject = null;
                // tag name
                if (tagPathPair != null)
                {
                    screenPlayLinkObject = FindTagPathPairFromUnityTag("ScreenPlay", tagPathPair); // gameObject.FindObject(tagName);
                }
                else
                {
                    Debug.LogErrorFormat("{0} : tag is null !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
                }
                return screenPlayLinkObject;
            }


            // find screen play object from tag
            public PathTagStruct? PathFromTagPath(Data.TagPathPair tagPathPair)
            {
                Vector3 start = Vector3.zero;
                bool b = false;

                // tag start
                var goTagFirst = FindTagPath(tagPathPair.First);
                if (goTagFirst != null)
                {
                    start = goTagFirst.transform.position;
                    b = true;
                }

                // tag mid
                Vector3? mid = null;
                // tag end
                Vector3? end = null;

                // we get a ScreenPlayLinkObject
                IScreenPlayLinkObject goTagSecond = FindTagPathPair(tagPathPair);
                if (goTagSecond != null)
                {
                    // we found a ScreenPlayLinkObject
                    mid = goTagSecond.GameObject.transform.position;
                }
                else
                {
                    // ScreenPlayLinkObject not found, we make a path from tag
                    (Vector3 pos, bool findEnd) = PositionTagPath(tagPathPair.Second);
                    if (findEnd)
                    {
                        end = pos;
                    }
                }

                return b == true ? (PathTagStruct?)new PathTagStruct(goTagSecond, start, mid, end) : null;
            }

            /*
			*
			* last screen play game object
			* 
			*/

            private GameObject GetLastScreenPlayGameObject()
            {
                return GetScreenPlayGameObjectFromTag(screenPlayProperties.lastScreenPlayObjectTag);
            }

            private GameObject GetScreenPlayGameObjectFromTag(string tag)
            {
                if (lastScreenPlayObjectGameObject != null)
                {
                    if (tag.ToString() == lastScreenPlayObjectGameObject.name)
                    {
                        return lastScreenPlayObjectGameObject;
                    }
                }

                if (tag != null)
                {
                    lastScreenPlayObjectGameObject = GameObject.Find(tag.ToString());
                }

                return lastScreenPlayObjectGameObject;
            }

            private void SetLastScreenPlayGameObject(GameObject gameObject)
            {
                if (lastScreenPlayObjectGameObject != null)
                {
                    screenPlayMaster.EventManager.QueueEvent(new Event_ScreenPlayTagLeave(new Tuple<string, Data.Tag>(screenPlayProperties.currentScreenPlaySceneName, Data.Tag.CreateTag(lastScreenPlayObjectGameObject.name))));
                }

                lastScreenPlayObjectGameObject = gameObject;

                if (lastScreenPlayObjectGameObject != null)
                {
                    screenPlayProperties.lastScreenPlayObjectTag = lastScreenPlayObjectGameObject.name;
                    screenPlayMaster.EventManager.QueueEvent(new Event_ScreenPlayTagEnter(new Tuple<string, Data.Tag>(CurrentScreenPlayProperties().currentScreenPlaySceneName, Data.Tag.CreateTag(screenPlayProperties.lastScreenPlayObjectTag))));
                }
                else
                {
                    screenPlayProperties.lastScreenPlayObjectTag = null;
                }
            }

            /*
			 * 
			 * Object management
			 * 
			 */

            // populate all children
            private List<GameObject> PopulateAllScreenPlayObjects(string sceneName)
            {
                List<GameObject> children = new List<GameObject>();
                void PopulateAllChildrenFromParent(GameObject parent)
                {
                    foreach (Transform tag in parent.transform)
                    {
                        var tagGameObject = tag.gameObject;
                        if (tagGameObject.scene.name == sceneName)
                        {
                            if (Data.Tag.IsTag(tagGameObject.name) == true && tagGameObject.name != startTag)
                            {
                                children.Add(tagGameObject);
                            }
                            PopulateAllChildrenFromParent(tagGameObject);
                        }
                        else
                        {
                            Debug.LogErrorFormat("{0} : game object {1} is not in the scene {2} !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, tagGameObject.name, sceneName);
                        }
                    }
                }

                var all = GameObject.FindGameObjectsWithTag("ScreenPlay");
                foreach (var parent in all)
                {
                    if (parent.scene.name == sceneName)
                    {
                        PopulateAllChildrenFromParent(parent);
                    }
                }
                return children;
            }


            // in order ot manager geometric trigger, place all object to final location
            public void PresentObjects(List<PresentObjectStruct> presentObjectArray)
            {
                // Correct the object array
                List<PresentObjectStruct> CorrectObjectArray(List<PresentObjectStruct> _presentObjectArray)
                {
                    // fill transformScreenPlayTag if possible
                    for (int i = 0; i < _presentObjectArray.Count; ++i)
                    {
                        bool b = false;
                        var o = FindTag(_presentObjectArray[i].tag);
                        if (o != null)
                        {
                            _presentObjectArray[i] = new PresentObjectStruct(_presentObjectArray[i].positionMapTag, _presentObjectArray[i].tag, o.transform);
                            b = true;
                        }
                        else
                        {
                            Debug.LogFormat("ScreenPlay0 object {0} not found", _presentObjectArray[i].tag);
                        }
                    }

                    // remove items which is not bound to screen play object
                    bool match(PresentObjectStruct element)
                    {
                        bool b = false;
                        var o = element.transformScreenPlayTag;
                        if (o == null)
                        {
                            b = true;
                        }
                        return b;
                    }

                    _presentObjectArray.RemoveAll(match);

                    return _presentObjectArray;
                }

                // Find the firsts parents
                (List<PresentObjectStruct>, List<PresentObjectStruct>) FindFirstParents(List<PresentObjectStruct> _presentObjectArray)
                {
                    // check if element has parent tagged "ScreenPlay"
                    bool match(PresentObjectStruct element)
                    {
                        bool b = false;
                        Transform parent = element.transformScreenPlayTag;
                        bool bParentNotFound = true;
                        while (bParentNotFound == true && parent != null)
                        {
                            parent = parent.parent;
                            if (parent != null)
                            {
                                if (parent.GetComponent<IScreenPlayObject>() != null || parent.tag == "ScreenPlay")
                                {
                                    bParentNotFound = false;
                                }
                            }
                        }

                        if (parent != null)
                        {
                            if (parent.tag == "ScreenPlay")
                            {
                                b = true;
                            }
                        }
                        return b;
                    }

                    // find parents
                    List<PresentObjectStruct> _parents = _presentObjectArray.FindAll(match);
                    _presentObjectArray = _presentObjectArray.Except(_parents).ToList();
                    return (_parents, _presentObjectArray);
                }

                // sorting 
                List<PresentObjectStruct> SortTransformAccordingToHierarchyyDeep(List<PresentObjectStruct> _parents, int _indexParent, List<PresentObjectStruct> _presentObjectArray)
                {
                    if (_indexParent < _parents.Count)
                    {
                        if (_presentObjectArray.Count > 0)
                        {
                            Transform curParent;

                            bool match(PresentObjectStruct element)
                            {
                                bool b = false;
                                var transform = element.transformScreenPlayTag;
                                b = curParent.GetInstanceID() == transform.parent.GetInstanceID();
                                return b;
                            }


                            for (; _indexParent < _parents.Count; ++_indexParent)
                            {
                                curParent = FindTag(_parents[_indexParent].tag).transform;
                                var newParents = _presentObjectArray.FindAll(match);
                                _parents.AddRange(newParents);
                                _presentObjectArray = _presentObjectArray.Except(_parents).ToList();

                            }
                            SortTransformAccordingToHierarchyyDeep(_parents, _indexParent, _presentObjectArray);
                        }
                    }
                    else
                    {
                        if (_presentObjectArray.Count > 0)
                        {
                            Debug.LogErrorFormat("{0} : failure during presenting object tag !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
                        }
                    }
                    return _parents;
                }

                // Clean object array
                presentObjectArray = CorrectObjectArray(presentObjectArray);
                // Find first parents
                List<PresentObjectStruct> parents;
                (parents, presentObjectArray) = FindFirstParents(presentObjectArray);
                // then from these parents; we order children according to its parents
                var newPresentObjectArray = SortTransformAccordingToHierarchyyDeep(parents, 0, presentObjectArray);

                // we apply position on screen play tags
                foreach (var it in newPresentObjectArray)
                {
                    // tag name
                    var transformScreenPlayTag = it.transformScreenPlayTag;
                    // screen play object 
                    if (transformScreenPlayTag != null)
                    {
                        transformScreenPlayTag.position = it.positionMapTag;
                    }
                    else
                    {
                        Debug.LogWarning("screenPlay " + transformScreenPlayTag.ToString() + " not found !!!");
                    }
                }
            }

            // switching objects
            IEnumerator SwitchingObject(GameObject from, GameObject to, Core.Done done)
            {
                bool b = false;
                {
                    IScreenPlayGroup fromScreenPlayGroup = null;
                    IScreenPlayGroup toScreenPlayGroup = null;
                    IScreenPlayObject toSreenPlayObject = null;
                    IScreenPlayObject fromScreenPlayObject = null;

                    // Get & Activate Screen Play Object From
                    if (null != from)
                    {
                        // get screen play object
                        fromScreenPlayObject = from.GetComponent<IScreenPlayObject>();
                        // Find Screen Play Group
                        fromScreenPlayGroup = FindGroupFromObject(fromScreenPlayObject);
                    }

                    // Get Screen Play Object To
                    if (to != null)
                    {
                        toSreenPlayObject = to.GetComponent<IScreenPlayObject>();
                        // Find Screen Play Group
                        toScreenPlayGroup = FindGroupFromObject(toSreenPlayObject);
                    }

                    // Activate from
                    {
                        yield return fromScreenPlayObject?.Activate(false);
                    }

                    // Activate Screen Play Group
                    if (toScreenPlayGroup != null || fromScreenPlayGroup != null)
                    {
                        if (toScreenPlayGroup == null && fromScreenPlayGroup != null)
                        {
                            yield return fromScreenPlayGroup.ExecuteScreenPlayGroup(false, from);
                        }
                        else
                        if (toScreenPlayGroup != null && fromScreenPlayGroup == null)
                        {
                            yield return toScreenPlayGroup.ExecuteScreenPlayGroup(true, to);
                        }
                        else
                        if (toScreenPlayGroup.Equals(fromScreenPlayGroup) == false)
                        {
                            yield return fromScreenPlayGroup.ExecuteScreenPlayGroup(false, from);
                            yield return toScreenPlayGroup.ExecuteScreenPlayGroup(true, to);
                        }
                        else
                        if (toScreenPlayGroup.Equals(fromScreenPlayGroup) == true && fromScreenPlayGroup != null)
                        {
                            yield return toScreenPlayGroup.ExecuteScreenPlayGroup(true, to);
                            yield return fromScreenPlayGroup.ExecuteScreenPlayGroup(false, from);
                        }
                    }

                    // Activate Screen Play Object To (only when group is not busy)
                    //if (busy == 0)

                    if (toSreenPlayObject != null)
                    {
                        if (toScreenPlayGroup != null)
                        {
                            if (toScreenPlayGroup.Contains(to))
                            {
                                yield return toSreenPlayObject?.Activate(true);

                            }
                        }
                        else
                        {
                            yield return toSreenPlayObject.Activate(true);
                        }
                    }

                    SetLastScreenPlayGameObject(to);

                }
                b = true;
                done?.Invoke(b);
                yield return null;
            }

            // switching objects
            IEnumerator AddSwitchingObject(GameObject from, GameObject to, Core.Done done)
            {
                /*if (screenPlayMaster.BusySwitch > 0)
					Debug.LogError(" coroutines.Count + " + screenPlayMaster.BusySwitch);
				yield return new WaitWhile(() => screenPlayMaster.BusySwitch > 0);*/
                //Debug.LogError(" AddSwitchingObject enter");
                screenPlayMaster.BusySwitch++;
                yield return SwitchingObject(from, to, done);
                //Debug.LogError(" AddSwitchingObject leave");
                screenPlayMaster.BusySwitch--;
                yield return null;
            }

            // switching tag objects
            public IEnumerator SwitchingObject(Data.Tag fromTag, Data.Tag toTag, Core.Done done)
            {
                var from = FindTag(fromTag);
                var to = FindTag(toTag);
                if (from != null && to != null)
                {
                    yield return AddSwitchingObject(from, to, done);
                }
                yield return null;
            }

            // switching ScreenPlay Object
            public IEnumerator SwitchingObject(Data.Tag tag, Core.Done done)
            {
                IEnumerator action(GameObject screenPlayGameObjectTag)
                {
                    yield return AddSwitchingObject(GetLastScreenPlayGameObject(), screenPlayGameObjectTag, done);
                }

                var screenPlayGameObject = FindTag(tag);
                yield return action(screenPlayGameObject);
            }

            // activate ScreenPlay Object
            public IEnumerator ActivateObject(Data.Tag tag, Core.Done done)
            {
                IEnumerator action(GameObject screenPlayGameObjectTag)
                {
                    yield return AddSwitchingObject(null, screenPlayGameObjectTag, done);
                }

                var screenPlayGameObject = FindTag(tag);
                yield return action(screenPlayGameObject);
            }

            // activate ScreenPlay Object
            public IEnumerator DeactivateObject(Data.Tag tag, Core.Done done)
            {
                IEnumerator action(GameObject screenPlayGameObjectTag)
                {
                    yield return AddSwitchingObject(screenPlayGameObjectTag, null, done);
                }

                var screenPlayGameObject = FindTag(tag);
                yield return action(screenPlayGameObject);
            }

            /*
			 * 
			 * Group management
			 * 
			 */

            public IScreenPlayGroup FindGroupFromObject(IScreenPlayObject fromTag)
            {
                var group = fromTag.Group;
                return group;
            }

            /*
			* 
			* Screen play overall management
			* 
			*/

            // Execute screen play on __Start__
            private IEnumerator ExecuteScreenPlay(ScreenPlayPropertiesStruct screenPlayProperties)
            {
                screenPlayMaster.CoRoutineManager.PushToken(startTag);

                // get all children
                List<GameObject> children = PopulateAllScreenPlayObjects(screenPlayProperties.currentScreenPlaySceneName);

                // setup all screen play objects as desactivated
                {
                    foreach (var obj in children)
                    {
                        var screenPlayObject = obj.GetComponent<IScreenPlayObject>();
                        yield return screenPlayObject?.Activate(false);
                    }

                    yield return null;
                }

                // start initial screen play object
                // execute #__start__
                {
                    // Find all go containing a __start__
                    IScreenPlayObject[] allScreenPlayObjects = GameObject.FindObjectsOfType<IScreenPlayObject>();
                    var startTags = Enumerable.Range(0, allScreenPlayObjects.Count()).Where(i => allScreenPlayObjects[i].GameObject.name == startTag).ToList();

                    // Find the firts go containing a __start__ within the current scene
                    GameObject gameObjectWithStart = null;
                    foreach (var indexGo in startTags)
                    {
                        var go = allScreenPlayObjects[indexGo];
                        UnityEngine.SceneManagement.Scene scene = go.GameObject.scene;
                        if (scene.name == screenPlayProperties.currentScreenPlaySceneName)
                        {
                            gameObjectWithStart = go.GameObject;
                            break;
                        }
                    }

                    if (gameObjectWithStart != null)
                    {
                        IScreenPlayObject screenPlayObject = gameObjectWithStart.GetComponent<IScreenPlayObject>();
                        gameObjectWithStart.SetActive(true);
                        yield return screenPlayObject.Activate(true);
                        yield return new WaitWhile(() => screenPlayObject.IsBusy == true);
                    }
                    else
                    {
                        Debug.LogWarningFormat("{0} : #__start__ not defined !!! ", System.Reflection.MethodBase.GetCurrentMethod().Name);
                    }

                    yield return null;

                    if (gameObjectWithStart != null)
                    {
                        IScreenPlayObject fromScreenPlayObject = gameObjectWithStart.GetComponent<IScreenPlayObject>();
                        yield return fromScreenPlayObject?.Activate(false);
                    }

                    yield return null;
                }

                yield return screenPlayMaster.CoRoutineManager.PopToken();

                yield return null;
            }

            // loading screen play controller (__start__)
            private IEnumerator LoadingScreenPlayScene(ScreenPlayPropertiesStruct screenPlayProperties)
            {
                // Load Unity 
                {
                    bool loadingDone = false;

                    void LoadingDone(string sceneName)
                    {
                        loadingDone = true;
                    }

                    screenPlayMaster.SceneLoader.LoadScene(screenPlayProperties.currentScreenPlaySceneName, screenPlayProperties.loadingSceneName, LoadSceneMode.Additive, screenPlayMaster.AddressablesScene, LoadingDone);
                    yield return new WaitWhile(() => loadingDone == false);
                }

                // Boot Screen Play
                {
                    yield return ExecuteScreenPlay(screenPlayProperties);
                }

                // invoke event
                {
                    screenPlayMaster.EventManager.QueueEvent(new Event_ScreenPlaySceneEnter(screenPlayProperties.currentScreenPlaySceneName));
                    screenPlayMaster.EventManager.QueueEvent(new Event_ScreenPlayTagEnter(new Tuple<string, Data.Tag>(screenPlayProperties.currentScreenPlaySceneName, Data.Tag.CreateTag(screenPlayProperties.lastScreenPlayObjectTag))));
                }

                yield return null;
            }

            // Unload Current Screen Play Scene (switchScene as replace)
            private IEnumerator UnloadCurrentScreenPlayScene(ScreenPlayPropertiesStruct screenPlayProperties, bool switchScene)
            {
                if (IsValidScene(screenPlayProperties.currentScreenPlaySceneName))
                {
                    // Unload scene
                    {
                        yield return screenPlayMaster.sceneLoader.UnloadScene(screenPlayProperties.currentScreenPlaySceneName);
                    }

                    // When the scene is switched then we invoke scene leaving
                    if (String.IsNullOrEmpty(screenPlayProperties.currentScreenPlaySceneName) == false)
                    {
                        screenPlayMaster.EventManager.QueueEvent(new Event_ScreenPlaySceneLeave(screenPlayProperties.currentScreenPlaySceneName, switchScene));
                    }
                }
                yield return null;
            }

            // Loading new screen play controller
            private IEnumerator LoadingAndUnloadScreenPlayScene(ScreenPlayPropertiesStruct previousScreenPlayProperties, ScreenPlayPropertiesStruct newScreenPlayProperties, bool SwitchScene)
            {
                if (previousScreenPlayProperties.currentScreenPlaySceneName != newScreenPlayProperties.currentScreenPlaySceneName)
                {
                    if (IsValidScene(newScreenPlayProperties.currentScreenPlaySceneName) == true)
                    {
                        yield return UnloadCurrentScreenPlayScene(previousScreenPlayProperties, SwitchScene);
                        yield return screenPlayMaster.StartCoroutine(LoadingScreenPlayScene(newScreenPlayProperties));
                    }
                }
                yield return null;
            }

            // Is Valid Scene
            private bool IsValidScene(string sceneName)
            {
                bool isValidScene = true;
                // can't check if scene is in the build settings when we use addressables
                if (screenPlayMaster.AddressablesScene == false)
                {
                    List<string> scenesInBuild = new List<string>();
                    for (int i = 1; i < SceneManager.sceneCountInBuildSettings; i++)
                    {
                        string scenePath = SceneUtility.GetScenePathByBuildIndex(i);
                        int lastSlash = scenePath.LastIndexOf("/");
                        scenesInBuild.Add(scenePath.Substring(lastSlash + 1, scenePath.LastIndexOf(".") - lastSlash - 1));
                    }
                    isValidScene = scenesInBuild.Contains(sceneName) ? true : false;
                }
                return isValidScene;
            }
        }

        /*
		 * 
		 * Data
		 * 
		 */

        // interfaces

        private Scene.ISceneLoader sceneLoader;
        public Scene.ISceneLoader SceneLoader { get => sceneLoader; }
        private Core.IEventManager eventManager;
        public Core.IEventManager EventManager { get => eventManager; }
        private Core.ICoRoutineManager coRoutineManager;
        public Core.ICoRoutineManager CoRoutineManager { get => coRoutineManager;}


        // is addressable scene

        [SerializeField]
		private bool addressablesScene = false;

		// sub Screen play

		[SerializeField]
		private List<ScreenPlaySub> subScreenPlayArray;
				
		/*
		 *
		 * life cycle
		 * 
		 */

		[Inject]
		public void Constructor(Scene.ISceneLoader sceneLoader, Core.IEventManager eventManager, Core.ICoRoutineManager coRoutineManager)
		{
			this.sceneLoader = sceneLoader;
			this.eventManager = eventManager;
            this.coRoutineManager = coRoutineManager;

        }

		// Start is called before the first frame update
		void Awake()
		{
		}

		void OnEnable()
		{
		}

		// Start is called before the first frame update
		void Start()
		{
		}

		/*
		 * 
		 * screen play stack management
		 * 
		 */

		int CurrentScreenPlaySubIndex
		{
			get
			{
				return subScreenPlayArray.Count - 1;
			}
		}

		ScreenPlaySub CurrentScreenPlaySub
		{
			get
			{
				return subScreenPlayArray[CurrentScreenPlaySubIndex];
			}
		}

		ScreenPlaySub ParentScreenPlaySub
		{
			get
			{
				return subScreenPlayArray[CurrentScreenPlaySubIndex-1];
			}
		}

		int CurrentScreenPlaySubCount
		{
			get
			{
				return subScreenPlayArray.Count;
			}
		}

		/*
		 * 
		 * LifeCycle
		 * 
		 */

		public bool LaunchScreenPlayEditMode()
		{
			bool b = false;
			if (CurrentScreenPlaySubCount > 0)
			{
				CurrentScreenPlaySub.Setup(this);
				StopAllCoroutines();
				StartCoroutine(CurrentScreenPlaySub.LaunchScreenPlayScene(Event_ScreenPlaySceneStarted.ScreenPlayMode.startEditor));
				b = true;
			}
			return b;
		}

		public bool LaunchScreenPlayGameMode(ScreenPlayPropertiesStruct[] screenPlayProperties)
		{
			bool b = false;
			if (CurrentScreenPlaySubCount == 0 && screenPlayProperties.Length > 0)
			{
				foreach (var it in screenPlayProperties)
				{
					var screenPlaySub = new ScreenPlaySub();
					screenPlaySub.Setup(this, it);
					subScreenPlayArray.Add(screenPlaySub);
				}
							   
				StopAllCoroutines();
				StartCoroutine(CurrentScreenPlaySub.LaunchScreenPlayScene(Event_ScreenPlaySceneStarted.ScreenPlayMode.startGame));
				b = true;
			}
			return b;
		}


		/*
		 * 
		 * Object management
		 * 
		 */

		// in order ot manager geometric trigger, place all object to final location
		public void PresentObjects(List<PresentObjectStruct> presentObjectArray)
		{
			CurrentScreenPlaySub.PresentObjects(presentObjectArray);
		}
	
		// find screen play object from tag
		public GameObject FindTag(Data.Tag tag)
		{
			return CurrentScreenPlaySub.FindTag(tag);
		}

		// we find tag among all screen play
		public GameObject FindTagPath(Data.TagPath tagPath)
		{
			return CurrentScreenPlaySub.FindTagPath(tagPath);
		}
		
		public GameObject FindScreenPlay(GameObject fromParent)
		{
			return CurrentScreenPlaySub.FindScreenPlay(fromParent);
		}

		public List<GameObject> FindChildrenTagFrom(GameObject fromParent, Data.Tag tag)
		{
			return CurrentScreenPlaySub.FindChildrenTagFrom(fromParent, tag);
		}

		public GameObject FindChildTagFrom(GameObject fromParent, Data.Tag tag)
		{
			return CurrentScreenPlaySub.FindChildTagFrom(fromParent, tag);
		}
		
		public GameObject FindParentTagFrom(GameObject gameObject)
		{
			return CurrentScreenPlaySub.FindParentTagFrom(gameObject);
		}

		public IRuntimeObjects FindRuntimeObjects(GameObject gameObject)
		{
			return CurrentScreenPlaySub.FindRuntimeObjects(gameObject);
		}

		public (Vector3, bool) PositionTag(Data.Tag tag)
		{
			return CurrentScreenPlaySub.PositionTag(tag);
		}

		public (Vector3, bool) PositionTagPath(Data.TagPath tagPath)
		{
			return CurrentScreenPlaySub.PositionTagPath(tagPath);
		}

		public PathTagStruct? PathTag(Data.TagPair tagPair)
		{
			return CurrentScreenPlaySub.PathTag(tagPair);
		}

		public PathTagStruct? PathFromTagPath(Data.TagPathPair tagPathPair)
		{
			return CurrentScreenPlaySub.PathFromTagPath(tagPathPair);
		}

		[SerializeField]
		[ReadOnly]
		int busySwitch = 0;

		public int BusySwitch
		{
			get
			{
				return busySwitch;
			}
			set
			{
				busySwitch = value;
			}
		}

		// SwitchingObject ScreenPlay
		public IEnumerator SwitchingObject(Data.Tag fromTag, Data.Tag toTag, Core.Done done)
		{
			yield return CurrentScreenPlaySub.SwitchingObject(fromTag, toTag, done);
		}

		// activate ScreenPlay Object
		public IEnumerator SwitchingObjectLastScreenPlayObject(Data.Tag tag, Core.Done done)
		{
			yield return CurrentScreenPlaySub.SwitchingObject(tag, done);
		}

		// activate ScreenPlay Object
		public IEnumerator ActivateObject(Data.Tag tag, Core.Done done)
		{
			yield return CurrentScreenPlaySub.ActivateObject(tag, done);
		}

		// activate ScreenPlay Object
		public IEnumerator DeactivateObject(Data.Tag tag, Core.Done done)
		{
			yield return CurrentScreenPlaySub.DeactivateObject(tag, done);
		}

		// Get string of last tag
		public string LastScreenPlayObjectTag
		{
			get
			{
				return CurrentScreenPlaySub.LastScreenPlayObjectTag;
			}
		}

		/*
		 * 
		 * Scene management
		 * 
		 */

		// addressables scene
		public bool AddressablesScene => addressablesScene;

		// switching Scene
		public IEnumerator SwitchingScreenPlay(ScreenPlayPropertiesStruct screenPlayProperties)
		{
			yield return CurrentScreenPlaySub.SwitchingScreenPlay(Event_ScreenPlaySceneStarted.ScreenPlayMode.switchScreenPlayScene, screenPlayProperties);
		}

		// push screen play
		public IEnumerator PushScreenPlay(ScreenPlayPropertiesStruct screenPlayProperties, bool closeParent)
		{
			// Fire event
			{
				bool popDone = false;
				eventManager.QueueEvent(new Event_ScreenPlayPush(screenPlayProperties, (Core.IGameEvent evt) => { popDone = true; }));
				yield return new WaitUntil(() => popDone == true);
			}

			if (closeParent == true)
			{
				// close the parent physically but doens't remove from player state
				yield return CurrentScreenPlaySub.CloseScreenPlay(false);
			}

			// push a new screen play
			{
				var screenPlaySub = new ScreenPlaySub();
				screenPlaySub.Setup(this, screenPlayProperties);
				subScreenPlayArray.Add(screenPlaySub);
			}

			// launch the new screen play
			{
				yield return CurrentScreenPlaySub.LaunchScreenPlayScene(Event_ScreenPlaySceneStarted.ScreenPlayMode.pushScreenPlayScene);
			}
		}

		// configure next tag for next time
		public void SetupScreenPlayNextTag(string nextTag)
		{
			CurrentScreenPlaySub.SetupScreenPlayNextTag(nextTag);
		}

		// pop screen play
		public IEnumerator PopScreenPlay(bool openParent)
		{
			// Fire event
			{
				bool popDone = false;
				eventManager.QueueEvent(new Event_ScreenPlayPop(ParentScreenPlaySub.CurrentScreenPlayProperties(), (Core.IGameEvent evt) => { popDone = true; }));
				yield return new WaitUntil(() => popDone == true);
			}
			
			// Close the current screen play & remove from player state
			{
				yield return CurrentScreenPlaySub.CloseScreenPlay(true);
			}
			// pop screen play 
			{
				subScreenPlayArray.RemoveAt(subScreenPlayArray.Count - 1);
			}
			

			// lauchn the current screen play
			if (openParent == true)
			{
				yield return CurrentScreenPlaySub.LaunchScreenPlayScene(Event_ScreenPlaySceneStarted.ScreenPlayMode.popScreenPlayScene);
			}

			yield return null;
		}

		// current screen play scene name
		public string CurrentScreenPlaySceneName()
		{
			return CurrentScreenPlaySub.CurrentScreenPlayProperties().currentScreenPlaySceneName;
		}
	}
}