﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
    public class ScreenPlayObjectEnabled : MonoBehaviour
    {
        // Start is called before the first frame update
        void Awake()
        {
        }

        void Start()
        {
            var ScreenPlayObject = this.GetComponent<ScreenPlayObject>();
            StartCoroutine(ScreenPlayObject.Activate(true));
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
