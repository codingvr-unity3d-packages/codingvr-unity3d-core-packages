﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Zenject;

namespace codingVR.GameEngine
{
	[ExecuteAlways]
	public class ScreenPlayObject : IScreenPlayObject
	{
		// === DATA-INTERNAL =======================================================================================

		#region DATA-INTERNAL

		// game object actions
		GameObject gameObjectActions;

		// Screen play
		GameEngine.IScreenPlay screenPlay;

		#endregion

		// === DATA-INTERNAL =======================================================================================

		#region DATA-INTERNAL-UI

		// busy
		[SerializeField]
		[ReadOnly]
		bool busy = false;
		
		#endregion

		// === UNITY-LIFE-CYCLE ====================================================================================

		#region UNITY-LIFE-CYCLE

		// === Constructor ===

		[Inject]
		protected void Construct(GameEngine.IScreenPlay screenPlay)
		{
			this.screenPlay = screenPlay;
		}

		// Awake
		void Awake()
		{

		}

		// Start is called before the first frame update
		void Start()
		{
			BindDebugObject();
		}

		// Debugging 
		void BindDebugObject()
		{
			Core.GameObjectDrawGizmos gameObjectDesc = GetComponentInChildren<Core.GameObjectDrawGizmos>();
			if (gameObjectDesc != null)
			{
				gameObjectDesc.Title = name;
			}
		}

		#endregion

		// === IMPLEMENTATION ======================================================================================

		#region IMPLEMENTATION

		// Events form GameCreator Actions
		void OnFinish()
		{
			busy = false;
			GameObjectActions.SetActive(false);
		}

		void OnExecute(GameObject gameObject)
		{
			busy = true;
		}

		// return Actions game object
		private GameObject GameObjectActions
		{
			get
			{
				if (gameObjectActions == null)
				{
					var actions = this.GetComponentInChildren<global::GameCreator.Core.Actions>(true);
					if (actions != null)
					{
						actions.onFinish.AddListener(OnFinish);
						actions.onExecute.AddListener(OnExecute);
						gameObjectActions = actions.gameObject;
						gameObjectActions.SetActive(false);
					}
					else
					{
						Debug.LogErrorFormat("{0} : there is no action entry in ScreenPlayObject {1}", System.Reflection.MethodBase.GetCurrentMethod().Name, name);
					}
				}
				return gameObjectActions;
			}
		}

		// Find the first parents
		IScreenPlayGroup FindGroupParent()
		{
			IScreenPlayGroup group = null;
			Transform parent = transform;
			while (group == null && parent != null)
			{
				parent = parent.parent;
				if (parent != null)
				{
					var screenPlayGroup = parent.GetComponent<IScreenPlayGroup>();
					if (screenPlayGroup != null)
					{
						group = screenPlayGroup;
					}
				}
			}
			return group;
		}

		// Find the first parents
		IRuntimeObjects FindRuntimeObjects()
		{
			var runtImeObject = GetComponentInChildren<IRuntimeObjects>();
			return runtImeObject;
		}
		
		#endregion

		// === IScreenPlayObject ===================================================================================

		#region IScreenPlayObject

		// is busy
		public override bool IsBusy
		{
			get
			{
				return busy;
			}
		}

		// Execute actions
		IEnumerator ExecuteActions(bool bActivated)
		{
			if (busy == false)
			{
				if (GameObjectActions != null)
				{
					bool bActivateRequired = bActivated;
					if (GameObjectActions.activeSelf != bActivateRequired)
					{
						var actions = GameObjectActions.GetComponent<global::GameCreator.Core.Actions>();
						if (bActivateRequired)
						{
							void Activate()
							{
								// game object activation is needed before execution launching
								// othewise the coroutine are not executed correctly
								GameObjectActions.SetActive(bActivateRequired);
								// explicit action execution
								actions.Execute();
							}
							// bu is explictly called here; do not change it
							busy = true;
							// execute action with a delay of one frame
							Activate();
						}
						else
						{
							// disbale game object action
							GameObjectActions.SetActive(bActivateRequired);
						}
					}
				}
			}

			yield return null;
		}

		// Enqueue actions
		IEnumerator EnqueueNextActions(bool bActivate)
		{
			// stop actions
			if (bActivate == false)
			{
				Stop();
				busy = false;
			}
			yield return null;
		}
			   
		// Activate screen play object
		public override IEnumerator Activate(bool b)
		{
			// enqueue next actions
			yield return EnqueueNextActions(b);
			// execute actions
			yield return ExecuteActions(b);
		}

		// Group
		public override IScreenPlayGroup Group
		{
			get
			{
				return FindGroupParent();
			}
		}

		// Runtime object
		public override IRuntimeObjects RuntimeObjects
		{
			get
			{
				return FindRuntimeObjects();
			}
		}
		
		// Gameobject
		public override GameObject GameObject => gameObject;

		// Reset
		public override void Stop()
		{
			// stop game creator actions
			var actions = GameObjectActions.GetComponent<global::GameCreator.Core.Actions>();
			actions.Stop();
			
			if (actions.actionsList != null)
			{
				actions.actionsList.executingIndex = -1;
				actions.actionsList.isExecuting = false;
				actions.actionsList.StopAllCoroutines();
			}
			actions.StopAllCoroutines();

			// Deactivate
			GameObjectActions.SetActive(false);

			/*
			global::GameCreator.Core.IActionsList[] actionList = GetComponentsInChildren<global::GameCreator.Core.IActionsList>();
			foreach (global::GameCreator.Core.IActionsList action in actionList)
			{
				action.Stop();
				foreach (global::GameCreator.Core.IAction it in action.actions)
				{
					if (it != null)
					{
						it.Stop();
						it.StopAllCoroutines();
					}
				}

				action.StopAllCoroutines();
			}
			*/
		}

		// Reset
		public override void Reset()
		{
			// stop game creator actions
			Stop();

			// reset runtimes object
			var runtimeObjectsArray = GetComponentsInChildren<IRuntimeObjects>();
			foreach (var it in runtimeObjectsArray)
			{
				it.DestroyAll();
			}
		}

		#endregion
	}
}