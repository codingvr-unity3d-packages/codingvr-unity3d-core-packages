﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace codingVR.GameEngine
{
	public class ScreenPlayGroupDebug : MonoBehaviour
	{
		// === DATA-UI ============================================================

		#region DATA-UI

		[SerializeField]
		Color groupColor = new Color(192, 255, 128, 64);

		[SerializeField]
		float groupRadius = 4.0f;

		#endregion

		// === LIFE-CYCLE ============================================================

		#region LIFE-CYCLE

		// Start is called before the first frame update
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{

		}

		#endregion

		// === INTERFACE ============================================================

		#region INTERFACE

		public Color GroupColor
		{
			get
			{
				return groupColor;
			}

			set
			{
				if (groupColor != value)
				{
					groupColor = value;
				}
			}
		}

		#endregion

		// === IMPLEMENTATION ============================================================

		#region IMPLEMENTATION

		(int, Vector3, Vector3) ComputeBox()
		{
			Collider[] boxCollider = GetComponentsInChildren<Collider>();
			Transform[] transforms = GetComponentsInChildren<Transform>();

			if (boxCollider.Length > 0)
			{
				Bounds bounds = new Bounds(transform.position, Vector3.zero);
				foreach (Collider bc in boxCollider)
				{
					Bounds boundsLocal = new Bounds(bc.bounds.center, bc.bounds.size);
					bounds.Encapsulate(boundsLocal);
				}
				return (boxCollider.Length, bounds.center - 0.5f * Vector3.up, bounds.size);
			}
			else
			{
				return (0, transform.position, Vector3.one * groupRadius);
			}
		}
		
		void drawGizmos(int scale, FontStyle fontStyle)
		{
			(int len, var pos, var size) = ComputeBox();
			Color gizmoColorNoAlpha = new Color(groupColor.r, groupColor.g, groupColor.b, groupColor.a);
			if (len > 0)
				DrawGizmos.DrawCube(pos, size, gizmoColorNoAlpha);
			else
				DrawGizmos.DrawWireCircle(pos, Math.Max(Math.Max(size.x, size.y), size.z) , gizmoColorNoAlpha, 40);
			DrawGizmos.DrawString(name, transform.position + Vector3.up * 1.0f, 30, groupColor, FontStyle.Bold);
		}
		
		void OnDrawGizmos()
		{
#if UNITY_EDITOR
			if (Selection.Contains(gameObject) == false)
			{
				drawGizmos(25, FontStyle.Bold);
			}
#endif
		}

		void OnDrawGizmosSelected()
		{
#if UNITY_EDITOR
			if (Selection.Contains(gameObject) == true)
			{
				drawGizmos(40, FontStyle.BoldAndItalic);
			}
#endif
		}

		#endregion
	}
}