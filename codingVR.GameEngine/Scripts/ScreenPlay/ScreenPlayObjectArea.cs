﻿using GameCreator.Core;
using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;
using Zenject;

namespace codingVR.GameEngine
{
	[RequireComponent(typeof(ScreenPlayObject))]
	[ExecuteAlways]
	public class ScreenPlayObjectArea : MonoBehaviour
	{
		// === DATA-INTERNAL-UI ====================================================

		#region DATA-INTERNAL-UI

		// data
		[SerializeField]
		//[HideInInspector]
		[FormerlySerializedAs("center")]
		Vector3 centerArea = Vector3.zero;
		[SerializeField]
		//[HideInInspector]
		[FormerlySerializedAs("size")]
		Vector3 sizeArea = Vector3.one;
		[SerializeField]
		[HideInInspector]
		int agentID = 0;
		[SerializeField]
		[HideInInspector]
		int areaType = 1;
		[SerializeField]
		[HideInInspector]
		TargetGameObject targetGameObject = new TargetGameObject(TargetGameObject.Target.Player);

		public enum TriggerTypeEnum
		{
			box,
			sphere,
			capsule,
		};

		[SerializeField]
		[HideInInspector]
		TriggerTypeEnum triggerType = TriggerTypeEnum.box;

		#endregion

		// === DATA-INTERNAL =======================================================================================

		#region DATA-INTERNAL

		// Screen play
		GameEngine.IScreenPlay screenPlay;

		#endregion

		// === UNITY-LIFE-CYCLE ====================================================================================

		#region UNITY-LIFE-CYCLE

		// === Constructor ===

		[Inject]
		protected void Construct(GameEngine.IScreenPlay screenPlay)
		{
			this.screenPlay = screenPlay;
		}

		// Awake
		void Awake()
		{
		}

		// Start is called before the first frame update
		void Start()
		{
		}

		private void Update()
		{
			var collider = gameObject.GetComponent<Collider>();
			if (collider)
				collider.hideFlags = HideFlags.HideInInspector;
				//collider.hideFlags = 0;
		}

		bool checkCollider(Collider other)
		{
			bool check = false;
			var target = targetGameObject.GetGameObject(this.gameObject);
			if (target != null)
			{
				check = other.gameObject.GetInstanceID() == target.GetInstanceID();
			}
			else
			{
				Debug.LogErrorFormat("{0} : targetGameObject is null on {1}; impossible to setup OnTriggerEnter !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, gameObject.name);
			}
			return check;
		}

		private void OnTriggerEnter(Collider other)
		{
			if (checkCollider(other))
			{
				void Done(bool status)
				{

				}
				StartCoroutine(screenPlay.SwitchingObjectLastScreenPlayObject(Data.Tag.CreateTag(name), Done));
			}
		}

		private void OnTriggerExit(Collider other)
		{
			if (checkCollider(other))
			{
				void Done(bool status)
				{

				}
				//gameObject.GetComponent<IScreenPlayObject>().Stop();
				StartCoroutine(screenPlay.DeactivateObject(Data.Tag.CreateTag(name), Done));
			}
		}

		#endregion

		// === IMPLEMENTATION ====================================================================================

		#region IMPLEMENTATION

		// ** Collider management **

		void UpdateTrigger(TriggerTypeEnum colliderType, Vector3 center, Vector3 size)
		{
			Collider compound = null;
			switch (colliderType)
			{
				case TriggerTypeEnum.box:
					{
						var collider = gameObject.GetComponent<BoxCollider>();
						collider.center = center;
						collider.size = size;
						compound = collider;
					}
					break;

				case TriggerTypeEnum.sphere:
					{
						var collider = gameObject.GetComponent<SphereCollider>();
						collider.center = center;
						collider.radius = size.x * 0.5f;
						compound = collider;
					}
					break;

				case TriggerTypeEnum.capsule:
					{
						var collider = gameObject.GetComponent<CapsuleCollider>();
						collider.center = center;
						collider.radius = size.x * 0.5f;
						collider.height = size.y;
						compound = collider;
					}
					break;
			}

			if (compound != null)
			{
				compound.isTrigger = true;
			}
		}

		void CreateTrigger(TriggerTypeEnum colliderType)
		{
			Collider compound = null;
			switch (colliderType)
			{
				case TriggerTypeEnum.box:
					{
						compound = gameObject.AddComponent<BoxCollider>();
					}
					break;

				case TriggerTypeEnum.sphere:
					{
						compound = gameObject.AddComponent<SphereCollider>();
					}
					break;

				case TriggerTypeEnum.capsule:
					{
						compound = gameObject.AddComponent<CapsuleCollider>();
					}
					break;
			}

			if (compound != null)
			{
				compound.hideFlags = HideFlags.HideInInspector;
				compound.isTrigger = true;
			}
		}

		// ** Trigger **
		void UpdateTrigger()
		{
			UpdateTrigger(triggerType, centerArea, sizeArea);
		}

		void CreateTrigger()
		{
			CreateTrigger(triggerType);
			UpdateTrigger(triggerType, centerArea, sizeArea);
		}

		void RemoveTrigger()
		{
			var collider = gameObject.GetComponent<Collider>();
			DestroyImmediate(collider);
		}

		void ManageTrigger(bool newValue)
		{
			if (newValue == true && UseTrigger == false)
			{
				CreateTrigger();
			}
			else if (newValue == true && UseTrigger == true)
			{
				UpdateTrigger();
			}
			else if (newValue == false && UseTrigger == true)
			{
				RemoveTrigger();
			}
			else if (newValue == false && UseTrigger == false)
			{

			}
		}

		// ** Nav mesh modifier **
		void BuildNavMeshes()
		{
			var goAction = GameObject.FindGameObjectsWithTag("cvrNavMesh");
			foreach (var it in goAction)
			{
				var navMeshSurface = it.GetComponent<NavMeshSurface>();
				navMeshSurface.BuildNavMesh();
			}
		}

        #if UNITY_EDITOR
        void UpdateNavMeshModifierVolume()
		{
			var navMesh = gameObject.GetComponent<NavMeshModifierVolume>();
			UnityEditorInternal.InternalEditorUtility.SetIsInspectorExpanded(navMesh, false);
			navMesh.center = centerArea;
			navMesh.size = sizeArea;
			navMesh.area = areaType;
			navMesh.AffectsAgentType(agentID);
			BuildNavMeshes();
		}
        #endif

        #if UNITY_EDITOR
        void CreateNavMeshModifierVolume()
		{
			var navMesh = gameObject.AddComponent<NavMeshModifierVolume>();
			navMesh.hideFlags = HideFlags.NotEditable;
			UnityEditorInternal.InternalEditorUtility.SetIsInspectorExpanded(navMesh, false);
			UpdateNavMeshModifierVolume();
		}
        #endif

        void RemoveNavMeshModifierVolume()
		{
			var navMeshModifier = gameObject.GetComponent<NavMeshModifierVolume>();
			var goAction = GameObject.FindGameObjectsWithTag("cvrNavMesh");
			DestroyImmediate(navMeshModifier);
			BuildNavMeshes();
		}

        
        void ManageNavMeshModifierVolume(bool newValue)
		{
            #if UNITY_EDITOR
            if (newValue == true && UseNavigation == false)
			{
				CreateNavMeshModifierVolume();
			}
			else if (newValue == true && UseNavigation == true)
			{
				UpdateNavMeshModifierVolume();
			}
			else if (newValue == false && UseNavigation == true)
			{
				RemoveNavMeshModifierVolume();
			}
			else if (newValue == false && UseNavigation == false)
			{

			}
            #endif
		}
      

#endregion

        // === INTERFACE ====================================================================================

        #region INTERFACE

        public bool UseTrigger
		{
			get
			{
				var collider = gameObject.GetComponent<Collider>();
				return collider == null ? false : true;
			}

			set
			{
				ManageTrigger(value);
			}
		}

		public bool UseNavigation
		{
            get
            {
				var navMeshModifier = gameObject.GetComponent<NavMeshModifierVolume>();
				return navMeshModifier == null ? false : true;
			}

			set
			{
				ManageNavMeshModifierVolume(value);
			}
        }

		public Vector3 Center
		{
			get
			{
				return centerArea;
			}

			set
			{
				if (centerArea != value)
				{
					centerArea = value;
					ManageNavMeshModifierVolume(UseNavigation);
					ManageTrigger(UseTrigger);
				}
			}
		}

		public Vector3 Size
		{
			get
			{
				return sizeArea;
			}

			set
			{
				if (sizeArea != value)
				{
					sizeArea = value;
					ManageNavMeshModifierVolume(UseNavigation);
					ManageTrigger(UseTrigger);
				}
			}
		}

		public int AreaType
		{
			get
			{
				return areaType;
			}

			set
			{
				if (areaType != value)
				{
					areaType = value;
					var navMeshModifier = gameObject.GetComponent<NavMeshModifierVolume>();
					if (navMeshModifier != null)
					{
						ManageNavMeshModifierVolume(UseNavigation);
						ManageTrigger(UseTrigger);
					}
				}
			}
		}

		public int AgentID
		{
			get
			{
				return agentID;
			}

			set
			{
				if (agentID != value)
				{
					agentID = value;
					var navMeshModifier = gameObject.GetComponent<NavMeshModifierVolume>();
					if (navMeshModifier != null)
					{
						ManageNavMeshModifierVolume(UseNavigation);
						ManageTrigger(UseTrigger);
					}
				}
			}
		}

		public TriggerTypeEnum TriggerType
		{
			get
			{
				return triggerType;
			}

			set
			{
				if (triggerType != value)
				{
					triggerType = value;
					BuildTrigger();
				}
			}
		}

		public void BuildTrigger()
		{
			RemoveTrigger();
			CreateTrigger();
			var navMeshModifier = gameObject.GetComponent<NavMeshModifierVolume>();
			if (navMeshModifier != null)
			{
				ManageNavMeshModifierVolume(UseNavigation);
				ManageTrigger(UseTrigger);
			}
		}

		#endregion
	}
}