﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameEngine
{
	public class ScreenPlayObjectDisabled : MonoBehaviour
	{
		// Start is called before the first frame update
		void Awake()
		{
			var ScreenPlayObject = this.GetComponent<ScreenPlayObject>();
			StartCoroutine(ScreenPlayObject.Activate(false));
		}
	}
}
