# codingVR Unity3d Core Packages

codingVR Unity3d Core Packages


- integration by git subtree
- integration by UPM

# Adding Packages & Dependencies From UPM

* dependencies below has to be added to manifest.json file

```
{
  "dependencies": {
  "com.codingvr.core": "https://gitlab.com/codingvr-unity3d-packages/codingvr-unity3d-core-packages.git",
  "com.dbrizov.naughtyattributes": "https://github.com/dbrizov/NaughtyAttributes.git#upm",  
  "com.unity.addressables": "1.19.11",
  "com.solidalloy.util": "https://github.com/SolidAlloy/SolidUtilities.git#1.30.0",
  "com.solidalloy.type-references": "https://github.com/SolidAlloy/ClassTypeReference-for-Unity.git#2.9.0",
```

# Adding Packages By Git SubTree (for dev usage)

1. dependencies below has to be added to manifest.json file

```
"com.dbrizov.naughtyattributes": "https://github.com/dbrizov/NaughtyAttributes.git#upm",  
```

2. add project in Assets

```
git subtree add --prefix Assets/codingVR/ https://gitlab.com/codingvr-unity3d-packages/codingvr-unity3d-core-packages.git main --squash
git subtree pull --prefix Assets/codingVR https://gitlab.com/codingvr-unity3d-packages/codingvr-unity3d-core-packages.git main --squash
```

3. update unity3d core package

```
git subtree push --prefix Assets/codingVR https://gitlab.com/codingvr-unity3d-packages/codingvr-unity3d-core-packages.git main
```

# SubTree documentation

http://manpages.ubuntu.com/manpages/trusty/man1/git-subtree.1.html  
https://gauthier.frama.io/post/git-subtree/  
https://gist.github.com/SKempin/b7857a6ff6bddb05717cc17a44091202


# Package UPM documentation

see https://nagachiang.github.io/tutorial-working-with-custom-package-in-unity-2019-2/#  
see https://medium.com/@FredericRP/your-assets-in-the-unity-package-manager-from-a-git-subfolder-1339880dd09f
