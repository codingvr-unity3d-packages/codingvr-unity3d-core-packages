// https://gist.github.com/kimsama/ff69cca140468f92d755

using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace codingVR.Editor
{
	public static class GetAllFilesUnderSelectedFolder
	{
		/// <summary>
		/// Retrieves selected folder on Project view.
		/// </summary>
		/// <returns></returns>
		public static string GetSelectedPathOrFallback()
		{
			string path = "Assets";

			foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets))
			{
				path = AssetDatabase.GetAssetPath(obj);
				if (!string.IsNullOrEmpty(path) && File.Exists(path))
				{
					path = Path.GetDirectoryName(path);
					break;
				}
			}
			return path;
		}

		/// <summary>
		/// Recursively gather all files under the given path including all its subfolders.
		/// </summary>
		public static IEnumerable<string> GetFiles(string path)
		{
			Queue<string> queue = new Queue<string>();
			queue.Enqueue(path);
			while (queue.Count > 0)
			{
				path = queue.Dequeue();
				try
				{
					foreach (string subDir in Directory.GetDirectories(path))
					{
						queue.Enqueue(subDir);
					}
				}
				catch (System.Exception ex)
				{
					Debug.LogError(ex.Message);
				}
				string[] files = null;
				try
				{
					files = Directory.GetFiles(path);
				}
				catch (System.Exception ex)
				{
					Debug.LogError(ex.Message);
				}
				if (files != null)
				{
					for (int i = 0; i < files.Length; i++)
					{
						yield return files[i];
					}
				}
			}
		}
	}
}	
/*
	
	=== Example ===


	// You can either filter files to get only neccessary files by its file extension using LINQ.
	// It excludes .meta files from all the gathers file list.
	var assetFiles = GetFiles(GetSelectedPathOrFallback()).Where(s => s.Contains(".meta") == false);

	foreach (string f in assetFiles)
	{
	  Debug.Log("Files: " + f);
	}
*/    