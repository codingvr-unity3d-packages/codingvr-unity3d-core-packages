using System.Linq;
using UnityEngine;
using UnityEditor;
using System.Reflection;

/*
 *
 * When a serialized value changes, 
 * this script allows to send an event to MonoBehavior 
 * you can test tha by using the script
 * SimpleOnChangingCallTester
 * Thanks to https://stackoverflow.com/questions/37958136/unity-c-how-script-know-when-public-variablenot-property-changed
 */

    public class OnChangingCallAttribute : PropertyAttribute
    {
        public string methodName;
        public OnChangingCallAttribute(string methodWithMax1Arguments)
        {
            methodName = methodWithMax1Arguments;
        }
    }

#if UNITY_EDITOR

    [CustomPropertyDrawer(typeof(OnChangingCallAttribute))]
    public class OnChangingCallAttributePropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginChangeCheck();
            EditorGUI.PropertyField(position, property);

            if (EditorGUI.EndChangeCheck())
            {
                OnChangingCallAttribute at = attribute as OnChangingCallAttribute;
                MethodInfo method = property.serializedObject.targetObject.GetType().GetMethods().Where(m => m.Name == at.methodName).First();

                if (method != null && method.GetParameters().Count() <= 1)// Only instantiate methods with max 1 parameters
                {
                    void callMethod<value>(value val)
                    {
                        if (method.GetParameters().Count() == 0)
                        {
                            method.Invoke(property.serializedObject.targetObject, null);
                        }
                        else
                        {
                            object[] obj = { val };
                            method.Invoke(property.serializedObject.targetObject, obj);
                        }
                    }


                    switch (property.propertyType)
                    {
                        case SerializedPropertyType.Integer:
                        {
                            callMethod(property.intValue);
                        }
                        break;

                    case SerializedPropertyType.String:
                        {
                            callMethod(property.stringValue);
                        }
                        break;

                    default:
                        {
                            method.Invoke(property.serializedObject.targetObject, null);
                        }
                        break;
                    }
                }
            }
        }
    }

#endif

