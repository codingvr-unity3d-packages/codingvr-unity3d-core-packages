﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true);
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        using (var scope = new EditorGUI.DisabledGroupScope(true))
        {
            EditorGUI.PropertyField(position, property, label, true);
        }
    }
}

[CustomPropertyDrawer(typeof(BeginReadOnlyGroupAttribute))]
public class BeginReadOnlyGroupDrawer : DecoratorDrawer
{

    public override float GetHeight() { return 0; }

    public override void OnGUI(Rect position)
    {
        EditorGUI.BeginDisabledGroup(true);
    }

}

[CustomPropertyDrawer(typeof(EndReadOnlyGroupAttribute))]
public class EndReadOnlyGroupDrawer : DecoratorDrawer
{

    public override float GetHeight() { return 0; }

    public override void OnGUI(Rect position)
    {
        EditorGUI.EndDisabledGroup();
    }

}

/*
 * 
 * example
 * 
 */

#if EXAMPLE

 using UnityEngine;
 
 public class ReadOnlyExample : MonoBehaviour
{
    [BeginReadOnlyGroup] // tag a group of fields as ReadOnly
    public string a;
    public int b;
    public Material c;
    public List<int> d = new List<int>();
    public CustomTypeWithPropertyDrawer e; // Works!
    [EndReadOnlyGroup]
    [ReadOnly] public string a2;
    [ReadOnly] public CustomTypeWithPropertyDrawer e2; // DOES NOT USE CustomPropertyDrawer!
    [BeginReadOnlyGroup]
    public int b2;
    public Material c2;
    public List<int> d2 = new List<int>();
    // Attribute tags apply to the next field of which there are no more so Unity/C# complains.
    // Since there are no more fields, we can omit the closing tag.
    // [EndReadOnlyGroup]

}

#endif