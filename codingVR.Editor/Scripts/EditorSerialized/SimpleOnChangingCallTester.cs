using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace codingVR.Core
{
    public class SimpleOnChangingCallTester : MonoBehaviour
    {
        [OnChangingCall("ImChanging")]
        public int TestMeForChanges = 0;

        private void Start()
        {

        }

        public void ImChanging(int newValue)
        {
            this.DelayedFunction(() => {
                Debug.Log("I have changed to" + TestMeForChanges);
            });
        }
    }
}