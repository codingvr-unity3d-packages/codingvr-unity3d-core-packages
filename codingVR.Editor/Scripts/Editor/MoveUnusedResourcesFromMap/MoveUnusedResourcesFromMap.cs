using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using System.Collections.Generic;
using System;
using System.IO;
using System.Linq;

namespace codingVR.Editor
{
	public class MoveUnusedResourcesFromMap : EditorWindow
	{
		static List<string> ReadFile(Core.GameDataReader reader, int version)
		{
			List<string> localPrefabList = new List<string>();

			// features
			if (version >= 19)
			{
				reader.ReadObjectStart("FeaturesConfig");
				int maxPlantCountByCell = reader.ReadInt();
				int maxUrbanCountByCell = reader.ReadInt();
				int maxFarmCountByCell = reader.ReadInt();
				reader.ReadObjectEnd();
			}

			// load prefabs references
			if (version >= 23)
			{
				reader.ReadObjectStart("AllPrefabRef");
				var Count = reader.ReadInt();
				for (int i = 0; i < Count; ++i)
				{
					var prefabPath = reader.ReadString() + ".prefab";
					localPrefabList.Add(NormalizePath(prefabPath));
				}
			}

			return localPrefabList;
		}


		static HashSet<string> LoadFile(string realPath)
		{
			HashSet<string> prefabList = new HashSet<string>();
			
			// read file text
			{
				Debug.LogWarning("Opening json file " + realPath + " ...");

				string json;
				using (var reader = new StreamReader(File.Open(realPath, FileMode.Open)))
				{
					json = reader.ReadToEnd();
				}

				if (json.Length > 0)
				{
					var readerJson = new LitJson.JsonReader(json);
					var gameDataReader = new Core.GameDataReader(readerJson);
					gameDataReader.Open();
					int version = gameDataReader.ReadInt();
					if (version <= codingVR.HexMapNavigation.HexGridLoader.mapFileVersion)
					{
						var localList = ReadFile(gameDataReader, version);
						prefabList.UnionWith(localList);
					}
					else
					{
						Debug.LogWarning("Unknown map format " + version);
					}
					gameDataReader.Close();
				}
			}
			return prefabList;
		}

		[MenuItem("Assets/CodingVR/Move Unused Resources From map.json Files")]
		public static void ShowExample()
		{

			MoveUnusedResourcesFromMap wnd = GetWindow<MoveUnusedResourcesFromMap>();
			wnd.titleContent = new GUIContent("MoveUnusedResourcesFromMap");
		}

		private void OnGUI()
		{
			// Set the container height to the window
			rootVisualElement.Q<ListView>().style.height = new StyleLength(position.height);
		}
		
		void FillListView(List<string> list)
		{
			// Each editor window contains a root VisualElement object
			VisualElement root = rootVisualElement;

			// The "makeItem" function will be called as needed
			// when the ListView needs more items to render
			Func<VisualElement> makeItem = () => new Label();

			// As the user scrolls through the list, the ListView object
			// will recycle elements created by the "makeItem"
			// and invoke the "bindItem" callback to associate
			// the element with the matching data item (specified as an index in the list)
			Action<VisualElement, int> bindItem = (e, i) => (e as Label).text = list[i];

			var listView = root.Q<ListView>();
			listView.makeItem = makeItem;
			listView.bindItem = bindItem;
			listView.itemsSource = list;
			listView.selectionType = SelectionType.Multiple;

			// Callback invoked when the user double clicks an item
			listView.onItemChosen += obj => Debug.Log(obj);

			// Callback invoked when the user changes the selection inside the ListView
			listView.onSelectionChanged += objects => Debug.Log(objects);
		}

		private static string GetApplicationPath()
		{
			string projectFolder = Path.Combine(Application.dataPath, "../");
			projectFolder = Path.GetFullPath(projectFolder);
			return projectFolder;
		}

		private static string NormalizePath(string path)
		{
			return path.Replace('\\', '/');
		}

		public void OnEnable()
		{
			// Create the list of unused prefabs 
			List<string> referencingPaths = new List<string>();
			List<string> resources = new List<string>();
			{
				DirectoryInfo dir = new DirectoryInfo(Application.streamingAssetsPath);
				FileInfo[] info = dir.GetFiles("*.map.json");
				HashSet<string> prefabList = new HashSet<string>();
				foreach (var it in info)
				{
					var localList = LoadFile(it.FullName);
					prefabList.UnionWith(localList);
				}
				var selectedPath = GetAllFilesUnderSelectedFolder.GetSelectedPathOrFallback();
				var allResourcesPath = GetAllFilesUnderSelectedFolder.GetFiles(GetAllFilesUnderSelectedFolder.GetSelectedPathOrFallback()).Where(s => s.Contains(".meta") == false);
				
				int count = allResourcesPath.Count();

				foreach (var path in allResourcesPath)
				{
					string subPath = path.Remove(0, selectedPath.Length);
					if (subPath[0] == '/' || subPath[0] == '\\')
						subPath = subPath.Remove(0, 1);
					//subPath = subPath.Replace(".prefab", "");
					resources.Add(NormalizePath(subPath));
				}
				foreach (var path in resources)
				{
					if (prefabList.Contains(path) == false)
					{
						referencingPaths.Add(path);
					}
				}
			}

			// Each editor window contains a root VisualElement object
			VisualElement root = rootVisualElement;

			// Import UXML
			var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/codingVR/codingVR.Editor/Scripts/Editor/MoveUnusedResourcesFromMap/MoveUnusedResourcesFromMap.uxml");
			VisualElement labelFromUXML = visualTree.CloneTree();
			root.Add(labelFromUXML);

			// count of item total
			var itemCount = root.Q<IntegerField>("CountItemTotal");
			itemCount.value = resources.Count;

			// count of item whcih well moved
			var removedItemCount = root.Q<IntegerField>("CountItemUnused");
			removedItemCount.value = referencingPaths.Count;
			
			// Fill list view
			FillListView(referencingPaths);

			// Apply button
			var applyButton = root.Q<Button>("Apply");
			applyButton.clicked += () =>
			{
				var destPath = GetAllFilesUnderSelectedFolder.GetSelectedPathOrFallback();
				destPath = destPath.Replace(destPath, destPath + "_NotInJsonFiles");
				var sourcePath = GetAllFilesUnderSelectedFolder.GetSelectedPathOrFallback();
				foreach (var it in referencingPaths)
				{
					var sourceFile = Path.Combine(sourcePath, it);
					var destFile = Path.Combine(destPath, it);
					sourceFile = Path.Combine(GetApplicationPath(), sourceFile);
					destFile = Path.Combine(GetApplicationPath(), destFile);
					destFile = Path.GetFullPath(destFile);
					sourceFile = Path.GetFullPath(sourceFile);
					//Debug.LogError("sourceFile " + sourceFile + "destFile " + destFile);
					var destDirectory = Path.GetDirectoryName(destFile);
					if (Directory.Exists(destDirectory) == false)
						Directory.CreateDirectory(destDirectory);
					FileUtil.MoveFileOrDirectory(sourceFile, destFile);
				}
				this.Close();
			};
		}
	}
}