﻿using UnityEngine;
using UnityEditor;
using Tymski;
using UnityEngine.SceneManagement;

public class ReplaceWithPrefab : EditorWindow
{
	[SerializeField] private GameObject prefab;

	[MenuItem("CodingVR/GameObject/Replace With Prefab", false, 0)]
	static void CreateReplaceWithPrefab()
	{
		EditorWindow.GetWindow<ReplaceWithPrefab>();
	}

	private void MoveChilds(Transform from, Transform to)
	{
		// all child froms
		Transform[] childFroms = new Transform[from.childCount];
		for (int iChildFrom = 0; iChildFrom < from.childCount; ++iChildFrom)
		{
			childFroms[iChildFrom] = from.GetChild(iChildFrom);
		}
		// parse child froms
		foreach (var childFrom in childFroms)
		{
			bool foundChild = false;

			foreach (Transform childTo in to)
			{
				if (childTo.name.Equals(childFrom.name))
				{
					var componentsTo = childTo.GetComponents<Component>();
					var componentsFrom = childFrom.GetComponents<Component>();
					foreach (var cmpFrom in componentsFrom)
					{
						var componentFromType = cmpFrom.GetType();
						bool foundComponent = false;
						foreach (var cmpTo in componentsTo)
						{
							var componentToType = cmpTo.GetType();
							if (componentFromType == componentToType)
							{
								if (componentFromType == typeof(GameCreator.Core.Actions))
								{
									// Do Nothing if Actions;
								}
								else
								if (componentFromType == typeof(GameCreator.Core.IActionsList))
								{
									UnityEditorInternal.ComponentUtility.CopyComponent(cmpFrom);
									UnityEditorInternal.ComponentUtility.PasteComponentAsNew(childTo.gameObject);
									var listFrom = cmpFrom.GetComponent<GameCreator.Core.IActionsList>();
									var listTo = childTo.GetComponent<GameCreator.Core.IActionsList>();
									listTo.actions = new GameCreator.Core.IAction[listFrom.actions.Length];

									for (int i = 0;i < listFrom.actions.Length; ++i)
									{
										GameCreator.Core.IAction c = cmpTo.gameObject.AddComponent(listFrom.actions[i].GetType()) as GameCreator.Core.IAction;
										UnityEditorInternal.ComponentUtility.CopyComponent(listFrom.actions[i]);
										UnityEditorInternal.ComponentUtility.PasteComponentValues(c);
										listTo.actions[i] = c;
									}
								}
								else
								{
									UnityEditorInternal.ComponentUtility.CopyComponent(cmpFrom);
									UnityEditorInternal.ComponentUtility.PasteComponentValues(cmpTo);
								}
								foundComponent = true;
								break;
							}
						}
						if (foundComponent == false)
						{
							UnityEditorInternal.ComponentUtility.CopyComponent(cmpFrom);
							UnityEditorInternal.ComponentUtility.PasteComponentAsNew(childTo.gameObject);
						}
					}
					foundChild = true;
					MoveChilds(childFrom.transform, childTo.transform);
					break;
				}
			}

			if (foundChild == false)
			{
				childFrom.parent = to;
			}
		}
	}

	private void OnGUI()
	{
		prefab = (GameObject)EditorGUILayout.ObjectField("Prefab", prefab, typeof(GameObject), false);

		if (GUILayout.Button("Replace"))
		{
			var selection = Selection.gameObjects;

			for (var i = selection.Length - 1; i >= 0; --i)
			{
				var selected = selection[i];
				var prefabType = PrefabUtility.GetPrefabType(prefab);
				GameObject newObject;

				if (prefabType == PrefabType.Prefab)
				{
					newObject = (GameObject)PrefabUtility.InstantiatePrefab(prefab, selected.scene);
				}
				else
				{
					newObject = Instantiate(prefab);
				}

				if (newObject == null)
				{
					Debug.LogError("Error instantiating prefab");
					break;
				}

				Undo.RegisterCreatedObjectUndo(newObject, "Replace With Prefabs");
				newObject.transform.parent = selected.transform.parent;
				newObject.transform.localPosition = selected.transform.localPosition;
				newObject.transform.localRotation = selected.transform.localRotation;
				newObject.transform.localScale = selected.transform.localScale;
				newObject.transform.SetSiblingIndex(selected.transform.GetSiblingIndex());
				newObject.name = selected.name;

				// move childs from selected to new object
				MoveChilds(selected.transform, newObject.transform);

				// destroy selected
				Undo.DestroyObjectImmediate(selected);
			}
		}

		GUI.enabled = false;
		EditorGUILayout.LabelField("Selection count: " + Selection.objects.Length);
	}
}