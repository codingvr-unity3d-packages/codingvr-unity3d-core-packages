﻿using UnityEngine;
using UnityEditor;
using System.Reflection;

//https://answers.unity.com/questions/15225/how-do-i-remove-null-components-ie-missingmono-scr.html

public class FindMissingScriptsRecursively : EditorWindow
{
	#region gui

	public void OnGUI()
	{
		if (GUILayout.Button("Find Missing Scripts in selected GameObjects"))
		{
			FindInSelected(this);
		}

		if (GUILayout.Button("Remove Missing Scripts in selected GameObjects"))
		{
			FindAndRemoveMissingInSelected();
		}
	}
	
	[MenuItem("CodingVR/Missing Scripts/Find Missing Scripts Recursively")]
	public static void ShowWindow()
	{
		EditorWindow.GetWindow(typeof(FindMissingScriptsRecursively));
	}

	#endregion

	// === CodingVR/Missing Scripts/Remove Missing Scripts Recursively ===

	#region FindAndRemoveMissingInSelected

	[MenuItem("CodingVR/Missing Scripts/Remove Missing Scripts Recursively")]
	private static void FindAndRemoveMissingInSelected()
	{
		var deepSelection = EditorUtility.CollectDeepHierarchy(Selection.gameObjects);
		int compCount = 0;
		int goCount = 0;
		foreach (var o in deepSelection)
		{
			if (o is GameObject go)
			{
				int count = GameObjectUtility.GetMonoBehavioursWithMissingScriptCount(go);
				if (count > 0)
				{
					// Edit: use undo record object, since undo destroy wont work with missing
					Undo.RegisterCompleteObjectUndo(go, "Remove missing scripts");
					GameObjectUtility.RemoveMonoBehavioursWithMissingScript(go);
					compCount += count;
					goCount++;
				}
			}
		}
		Debug.Log($"Found and removed {compCount} missing scripts from {goCount} GameObjects");
	}

	#endregion

	// === CodingVR/Missing Scripts/Find Missing Scripts Recursively ===

	#region FindMissingScripts

	static int go_count = 0, components_count = 0, missing_count = 0;
	   
	private static void FindInSelected(EditorWindow ed)
	{
		GameObject[] go = Selection.gameObjects;
		go_count = 0;
		components_count = 0;
		missing_count = 0;
		foreach (GameObject g in go)
		{
			FindInGO(g);
		}
		Debug.Log(string.Format("Searched {0} GameObjects, {1} components, found {2} missing", go_count, components_count, missing_count));
	}

	private static PropertyInfo cachedInspectorModeInfo;

	// https://forum.unity.com/threads/how-to-get-the-local-identifier-in-file-for-scene-objects.265686/
	private static long LocalIdentifieInFileOrObject(Object unityObject)
	{
		/*
		PropertyInfo inspectorModeInfo = typeof(SerializedObject).GetProperty("inspectorMode", BindingFlags.NonPublic | BindingFlags.Instance);

		SerializedObject serializedObject = new SerializedObject(unityObject);
		inspectorModeInfo.SetValue(serializedObject, InspectorMode.Debug, null);

		SerializedProperty localIdProp = serializedObject.FindProperty("m_LocalIdentfierInFile");   //note the misspelling!
		int localId = localIdProp.intValue;
		return localId;
		*/

		long id = -1;

		if (unityObject == null) return id;

		if (cachedInspectorModeInfo == null)
		{
			cachedInspectorModeInfo = typeof(SerializedObject).GetProperty("inspectorMode", BindingFlags.NonPublic | BindingFlags.Instance);
		}

		SerializedObject serializedObject = new SerializedObject(unityObject);
		cachedInspectorModeInfo.SetValue(serializedObject, InspectorMode.Debug, null);
		SerializedProperty serializedProperty = serializedObject.FindProperty("m_LocalIdentfierInFile");
#if UNITY_5_PLUS
			id = serializedProperty.longValue;
#else
		id = serializedProperty.intValue;
#endif
		if (id <= 0)
		{
			PrefabType prefabType = PrefabUtility.GetPrefabType(unityObject);
			if (prefabType != PrefabType.None)
			{
				id = LocalIdentifieInFileOrObject(PrefabUtility.GetPrefabObject(unityObject));
			}
			else
			{
				// this will work for the new objects in scene which weren't saved yet
				id = unityObject.GetInstanceID();
			}
		}
		return id;
	}

	private static void FindInGO(GameObject g)
	{
		go_count++;
		bool found = false;
		Component[] components = g.GetComponents<Component>();
		for (int i = 0; i < components.Length; i++)
		{
			components_count++;
			if (components[i] == null)
			{
				missing_count++;
				string s = g.name;
				Transform t = g.transform;
				while (t.parent != null)
				{
					s = t.parent.name + "/" + s;
					t = t.parent;
				}
				found = true;
				
			}
		}

		if (found)
		{
			for (int i = 0; i < components.Length; i++)
			{
				components_count++;
				if (components[i] == null)
				{
					missing_count++;
					string s = g.name;
					Transform t = g.transform;
					while (t.parent != null)
					{
						s = t.parent.name + "/" + s;
						t = t.parent;
					}
					Debug.LogError(s + "empty script attached in position : " + i + " on game object : " + g.name, g);

					Selection.activeGameObject = g;
				}
				else
				{
					string s = g.name;
					Transform t = g.transform;
					while (t.parent != null)
					{
						s = t.parent.name + "/" + s;
						t = t.parent;
					}
				
					Debug.Log(s + "script attached in position: " + i + " name: " + components[i] + " id file: " + LocalIdentifieInFileOrObject(components[i]), g);
				}
			}
		}

		// Now recurse through each child GO (if there are any):
		foreach (Transform childT in g.transform)
		{
			//Debug.Log("*** Searching " + childT.name  + " ***" );
			FindInGO(childT.gameObject);
		}
	}

	#endregion
}