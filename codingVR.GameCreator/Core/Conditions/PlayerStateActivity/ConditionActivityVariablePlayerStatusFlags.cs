﻿namespace GameCreator.Core
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;
    using GameCreator.Variables;

    #if UNITY_EDITOR
    using UnityEditor;
    #endif

    [AddComponentMenu("")]
	public class ConditionActivityVariablePlayerStatusFlags : ConditionActivityVariable<codingVR.Data.ActivityInstancePlayerStatusFlags>
    {
		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

        #if UNITY_EDITOR

		public static new string NAME = "CodingVR/Variables/Activity Condition Variable Player Status Flags";

        public override string GetNodeTitle()
        {
            return string.Format(
                NODE_TITLE,
                this.variable,
                this.compareTo
            );
        }

		protected override bool ShowComparison()
		{
			return true;
		}

		#endif
	}
}