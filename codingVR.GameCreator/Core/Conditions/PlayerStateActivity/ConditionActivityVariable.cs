﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Variables;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public abstract class ConditionActivityVariable<TYPE> : ICondition
	{
		public TargetActivityVariable target = new TargetActivityVariable(TargetActivityVariable.Target.Invoker);
		
		public codingVR.Data.FieldComparison comparison = codingVR.Data.FieldComparison.Equal;

		public string variable;
		public TYPE compareTo;

		// PRIVATE-DATA: -------------------------------------------------------------------------

		codingVR.GameEngine.IPlayerStateActivityController playerStateActivityController;
		codingVR.GameEngine.IPlayerStateManager playerStateMananger;

		// LIFECYCLE: ----------------------------------------------------------------------------

		[Inject]
		public void Constructor(codingVR.GameEngine.IPlayerStateActivityController playerStateActivityController, codingVR.GameEngine.IPlayerStateManager playerStateMananger)
		{
			this.playerStateActivityController = playerStateActivityController;
			this.playerStateMananger = playerStateMananger;
		}

		/* 
		 * 
		 * https://gitlab.com/monamipoto/potomaze/-/issues/590
		* When the container for this object is a scriptable object; this HACK is used to inject dependency 
		 * 
		 */

		public void DIPInject(GameObject invoker)
		{
			if (this.playerStateActivityController == null)
			{
				if (invoker != null)
				{
					var dipInject = invoker.GetComponent<codingVR.GameEngine.IDIPInject>();
					if (dipInject != null)
						dipInject.DiContainer.Inject(this);
				}
			}
		}

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool Check(GameObject target)
		{
			// https://gitlab.com/monamipoto/potomaze/-/issues/590
			DIPInject(target);

			bool bCheck = false;
			(var activityField, var activityNestedField) = this.target.GetActivityField(playerStateActivityController, playerStateMananger, target);
		
			if (activityField != null)
			{
				if (activityNestedField != null)
				{
					var value = activityField.CompareValue(activityNestedField.ParseNestedFields(variable), compareTo, comparison);
					bCheck = value.Item1 && value.Item2;
				}
				else
				{
					var value = activityField.CompareValue(variable, compareTo, comparison);
					bCheck = value.Item1 && value.Item2;
				}
			}
			return bCheck;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

		#if UNITY_EDITOR

		public static new string NAME = "CodingVR/Variables/Activity Variable";
		protected const string NODE_TITLE = "Activity Variable Compare {0} with {1}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spTarget;
		private SerializedProperty spVariable;
		private SerializedProperty spCompareTo;
		private SerializedProperty spComparison;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return "unknown";
		}

		protected override void OnEnableEditorChild ()
		{
			this.spTarget = this.serializedObject.FindProperty("target");
			this.spVariable = this.serializedObject.FindProperty("variable");
			this.spCompareTo = this.serializedObject.FindProperty("compareTo");
			this.spComparison = this.serializedObject.FindProperty("comparison");
		}

		protected virtual bool ShowComparison()
		{
			return false;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spTarget);

			EditorGUILayout.PropertyField(this.spVariable);

			if (this.ShowComparison())
			{
				EditorGUILayout.PropertyField(this.spComparison);
			}

			EditorGUILayout.PropertyField(this.spCompareTo);
			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}