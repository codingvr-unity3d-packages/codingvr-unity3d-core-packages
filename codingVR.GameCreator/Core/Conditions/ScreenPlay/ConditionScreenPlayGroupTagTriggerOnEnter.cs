﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Core;

	#if UNITY_EDITOR
	using UnityEditor;
	#endif

	[AddComponentMenu("")]
	public class ConditionScreenPlayGroupTagTriggerOnEnter : ICondition
	{
		public codingVR.GameEngine.ScreenPlayGroup screenPlayGroup;
		public string tag;
		
		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool Check(GameObject target)
		{
			bool bCheck = false;
			if (screenPlayGroup != null)
			{
				codingVR.GameEngine.IScreenPlayGroup screnPlayGroupInterface = screenPlayGroup.GetComponent<codingVR.GameEngine.IScreenPlayGroup>();
				bCheck = screnPlayGroupInterface.TagTriggerOnEnter.Equals(tag);
			}
			return bCheck;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

		#if UNITY_EDITOR

	    public static new string NAME = "CodingVR/ScreenPlay/Screen Play Group Tag Trigger On Enter";
		private const string NODE_TITLE = "Is Group {0} Has Entered By Tag {1}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spScreenPlayGroup;
		private SerializedProperty spTag;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(NODE_TITLE, screenPlayGroup?.name ?? "(null)",  tag);
		}

		protected override void OnEnableEditorChild ()
		{
			this.spScreenPlayGroup = this.serializedObject.FindProperty("screenPlayGroup");
			this.spTag = this.serializedObject.FindProperty("tag");
		}

		protected override void OnDisableEditorChild ()
		{
			this.screenPlayGroup = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spScreenPlayGroup);
			EditorGUILayout.PropertyField(this.spTag);

			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}
