﻿
namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
    using codingVR.Data;
	using System.Collections.ObjectModel;

#if UNITY_EDITOR
    using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ConditionGraphDataNode : ICondition
	{
        public bool satisfied = false;
        //public string[] test = new string[] { "test1", "test2", "test3" };
        public int index = 0;
        public GraphDataNodeScriptable graphDataNode;

        public override bool Check(GameObject target)
        {
            if (graphDataNode.Options[index] == graphDataNode.Answer)
            {
                satisfied = true;
            }
            else
            {
                satisfied = false;
            }
            return this.satisfied;
        }

#if UNITY_EDITOR
        public static new string NAME = "Custom/ConditionGraphDataNode";

        // PROPERTIES: ----------------------------------------------------------------------------

        private SerializedProperty spGraphDataNode;
        //private string[] test2 = new string[] { "test1", "test2", "test3" };
        private ReadOnlyCollection<string> options;
        private string[] answerList;

        protected override void OnEnableEditorChild()
        {
            this.spGraphDataNode = this.serializedObject.FindProperty("graphDataNode");
            //this.spTest = this.serializedObject.FindProperty("test");
        }

        protected override void OnDisableEditorChild()
        {
            this.spGraphDataNode = null;
            // this.spTest = null;
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            EditorGUILayout.PropertyField(this.spGraphDataNode);

            if (graphDataNode != null)
            {
                options = graphDataNode.Options;
                answerList = new string[options.Count];
                int i = 0;
                foreach (string option in options)
                {
                    answerList[i] = option;
                    i++;
                }

                //EditorGUILayout.PropertyField(this.spTest);
                index = EditorGUILayout.Popup("Test", index, answerList);
            }

            this.serializedObject.ApplyModifiedProperties();

        }
#endif
    }
}

