﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;

	[AddComponentMenu("")]
	public class IgniterOnScreenPlayObjectEnable : Igniter 
	{
		private static IEnumerator delayedAction(UnityAction action)
		{
			yield return null; // optional
			yield return new WaitForEndOfFrame(); // Wait for the next frame
			action.Invoke(); // execute a delegate
		}

		private static IEnumerator delayedAction<T>(T gameObject, System.Action<T> action)
		{
			yield return null; // optional
			yield return new WaitForEndOfFrame(); // Wait for the next frame
			action.Invoke(gameObject); // execute a delegate
		}

		public static void DelayedFunction(MonoBehaviour mono, UnityAction action)
		{
			mono.StartCoroutine(delayedAction(action));
		}

#if UNITY_EDITOR
		public new static string NAME = "CodingVR/ScreenPlay/On Enable Screen Play Object";
		#endif

		private new void OnEnable()
		{
			base.OnEnable();
			//this.ExecuteTrigger(gameObject);
			// see https://gitlab.com/monamipoto/potomaze/-/issues/186
						
						DelayedFunction(this, () =>
						{
							this.ExecuteTrigger(gameObject);
						});
						

		}
	}
}