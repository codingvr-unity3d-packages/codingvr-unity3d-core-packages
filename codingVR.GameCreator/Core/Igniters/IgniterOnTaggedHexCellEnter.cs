﻿namespace GameCreator.Core
{
    using UnityEngine;
    using GameCreator.Variables;
    using codingVR.Core;

    [AddComponentMenu("")]
    public class IgniterOnTaggedHexCellEnter : Igniter
    {
#if UNITY_EDITOR
        public new static string NAME = "CodingVR/Quests/On Tagged Hex Cell Enter";
        public new static string COMMENT = "Leave empty 'to Tag' to trigger any tagged Hex Cell";
#endif

        [VariableFilter(Variable.DataType.GameObject)]
        public VariableProperty storeInvoker = new VariableProperty(Variable.VarType.GlobalVariable);
        public TargetGameObject gameObjectTarget = new TargetGameObject(TargetGameObject.Target.GameObject);
        [OnChangingCall("ToTagChanging")]
        public string toTag;
        private codingVR.Data.Tag toTagEntity = null;
        private codingVR.Data.Tag ToTagEntity
        {
            get
            {
                if (toTagEntity == null)
                {
                    toTagEntity = codingVR.Data.Tag.CreateTag(toTag);
                }
                return toTagEntity;
            }
        }

        private void Start()
        {
            GameObject target = this.gameObjectTarget.GetGameObject(null);
            if (target != null)
            {
                var pose = target.GetComponent<codingVR.HexMapNavigation.HexGridObjectPoseCharacter>();
                if (null != pose)
                {
                    pose.OnCellEnter.AddListener(this.OnCellEnter);
                }
            }
        }

        private void OnCellEnter(codingVR.HexMapNavigation.IHexCellOrigin cell)
        {
            if (cell.TagData.Tag != null)
            {
                if (ToTagEntity == null || ToTagEntity.Equals(cell.TagData.Tag))
                {
                    storeInvoker.Set(cell);
                    base.ExecuteTrigger(this.gameObjectTarget.GetGameObject(null));
                }
            }
            else
            {
                Debug.LogErrorFormat("{0} this cell is not tagged !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        public void ToTagChanging()
        {
            // ivalidate concrete tag
            toTagEntity = null;
        }
    }
}