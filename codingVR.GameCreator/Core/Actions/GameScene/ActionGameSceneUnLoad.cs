﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using UnityEngine.SceneManagement;
	using GameCreator.Variables;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionGameSceneUnLoad : IAction
	{
		public StringProperty sceneName = new StringProperty();
		private codingVR.Scene.ISceneLoader sceneLoader;
		

		[Inject]
		public void Contruct(codingVR.Scene.ISceneLoader sceneLoader)
		{
			this.sceneLoader = sceneLoader;
		}
		
		// EXECUTABLE: ----------------------------------------------------------------------------

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			sceneLoader.UnloadScene(sceneName.ToString());
			yield return 0;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/GameScene/Unload Scene";
		private const string NODE_TITLE = "Unload scene {0}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spSceneName;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				this.sceneName
			);
		}

		protected override void OnEnableEditorChild()
		{
			this.spSceneName = this.serializedObject.FindProperty("sceneName");
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();
			EditorGUILayout.PropertyField(this.spSceneName);
			this.serializedObject.ApplyModifiedProperties();
		}

#endif
	}
}

