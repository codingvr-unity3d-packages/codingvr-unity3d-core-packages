﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using UnityEngine.SceneManagement;
	using GameCreator.Variables;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionGameSceneLoad : IAction
	{
		public StringProperty sceneName = new StringProperty();
		private codingVR.Scene.ISceneLoader sceneLoader;
		public bool addressablesScene = false;
		public StringProperty loadingSceneName = new StringProperty("Loading");

		[Inject]
		public void Contruct(codingVR.Scene.ISceneLoader sceneLoader)
		{
			this.sceneLoader = sceneLoader;
		}
		
		// EXECUTABLE: ----------------------------------------------------------------------------

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			bool loadingDone = false;

			void LoadingDone(string sceneName)
			{
				loadingDone = true;
			}
			sceneLoader.LoadScene(sceneName.ToString(), loadingSceneName.ToString(), LoadSceneMode.Additive, addressablesScene, LoadingDone);
			yield return new WaitWhile(() => loadingDone == false);

			yield return 0;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/GameScene/Load additive Scene";
		private const string NODE_TITLE = "Load additive scene {0}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spSceneName;
		private SerializedProperty spAddressablesScene;
		private SerializedProperty spLoadingSceneName;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				this.sceneName
			);
		}

		protected override void OnEnableEditorChild()
		{
			this.spSceneName = this.serializedObject.FindProperty("sceneName");
			this.spAddressablesScene = this.serializedObject.FindProperty("addressablesScene");
			this.spLoadingSceneName = this.serializedObject.FindProperty("loadingSceneName");
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();
			EditorGUILayout.PropertyField(this.spSceneName);
			EditorGUILayout.PropertyField(this.spAddressablesScene);
			EditorGUILayout.PropertyField(this.spLoadingSceneName);
			this.serializedObject.ApplyModifiedProperties();
		}

#endif
	}
}

