﻿namespace GameCreator.Core
{
    using System.Collections;
    using UnityEngine;
    using Zenject;

#if UNITY_EDITOR
    using UnityEditor;
    using UnityEngine.Scripting.APIUpdating;
#endif

    [AddComponentMenu("")]
    //[MovedFrom(false, null, "ActionGameSceneState")]
    public class ActionGameSceneState : IAction
    {
        public TargetGameObject target = new TargetGameObject();

        [Space]
        public codingVR.GameEngine.GameSessionStatus gameSceneState = codingVR.GameEngine.GameSessionStatus.isWaitingForStart;

        public string gameSceneName;

        public override bool InstantExecute(GameObject target, IAction[] actions, int index)
        {
            // bind game scene
            var gameScene = codingVR.Patterns.Interface<codingVR.Data.IGameSessionDataLink>.Bind();
            // Start game
            if (gameScene != null)
            {
                gameScene.Status = gameSceneState;
            }
            codingVR.Patterns.Interface<codingVR.Data.IGameSessionDataLink>.Unbind();

            return true;
        }

#if UNITY_EDITOR

        public static new string NAME = "CodingVR/GameScene/State";
        private const string NODE_TITLE = "Game scene state changes to {0}";

        // PROPERTIES: ----------------------------------------------------------------------------

        private SerializedProperty spGameSceneState;

        // INSPECTOR METHODS: ---------------------------------------------------------------------

        public override string GetNodeTitle()
        {
            return string.Format(
                NODE_TITLE,
                this.gameSceneState
            );
        }

        protected override void OnEnableEditorChild()
        {
            this.spGameSceneState = this.serializedObject.FindProperty("gameSceneState");
        }

        protected override void OnDisableEditorChild()
        {
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            EditorGUILayout.PropertyField(this.spGameSceneState, new GUIContent("Game Scene State"));

            this.serializedObject.ApplyModifiedProperties();
        }
#endif
    }
}