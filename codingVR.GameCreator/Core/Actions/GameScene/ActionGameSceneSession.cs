﻿namespace GameCreator.Core
{
	using System;
	using System.Collections;
	using UnityEngine;
	using UnityEngine.SceneManagement;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionGameSceneSession : GameQuestionTarget
	{
		public string gameSceneName;
		private DiContainer diContainer;
		private codingVR.Scene.ISceneLoader sceneLoader;
		public bool addressablesScene = false;
		public string loadingSceneName = "Loading";
		public codingVR.GameEngine.IScreenPlay screenPlay;
		private bool forceStop = false;
		
		[Inject]
		public void Contruct(DiContainer diContainer, codingVR.Scene.ISceneLoader sceneLoader, codingVR.GameEngine.IScreenPlay screenPlay)
		{
			this.diContainer = diContainer;
			this.sceneLoader = sceneLoader;
			this.screenPlay = screenPlay;
		}

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			return false;
		}

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			this.forceStop = false;

			// Load scene
			var savedPlayer = GameCreator.Core.Hooks.HookPlayer.Instance;
			var savedCamera = GameCreator.Core.Hooks.HookCamera.Instance;

			bool loadingDone = false;

			void LoadingDone(string sceneName)
			{
				loadingDone = true;
			}

			sceneLoader.LoadScene(gameSceneName, loadingSceneName, LoadSceneMode.Additive, addressablesScene, LoadingDone);
			yield return new WaitWhile(() => loadingDone == false);
			
		   // we pass a frame to complete initialisation
		   yield return null;

			// bind game scene
			var gameScene = codingVR.Patterns.Interface<codingVR.Data.IGameSessionDataLink>.Bind();
			// Start and execute game scene
			if (gameScene != null)
			{
				// game scene session
				var sessionData = gameScene.SelectData(SelectedData());
				if (sessionData != null)
				{
					// load data 
					diContainer.Inject(sessionData);
					yield return sessionData.GetDataSpec();
					yield return sessionData.GetData(true);
				}
				// start session
				yield return gameScene.StartSession();
				yield return new WaitUntil(() => gameScene.Status == codingVR.GameEngine.GameSessionStatus.isCompleted || this.forceStop == true);
				yield return gameScene.StopSession();

				// network post
				var reply = gameScene.Reply;
				if (reply != null)
				{
					diContainer.Inject(reply);
					yield return reply.PutData();
				}

				this.forceStop = false;
			}

			// we unload scene
			sceneLoader.UnloadScene(gameSceneName);


			// all right
#if UNITY_EDITOR
			Debug.Log(GetNodeTitle() + " Done !!!");
#endif

			// unbind game scene
			codingVR.Patterns.Interface<codingVR.Data.IGameSessionDataLink>.Unbind();

			GameCreator.Core.Hooks.HookPlayer.Instance = savedPlayer;
			GameCreator.Core.Hooks.HookCamera.Instance = savedCamera;

			yield return 0;
		}

		public override void Stop()
		{
			this.forceStop = true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/GameScene/Session";
		private const string NODE_TITLE = "Game scene session running at {0} is wating for an answer";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spGameSceneName;
		private SerializedProperty spAddressablesScene;
		private SerializedProperty spLoadingSceneName;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			var questionTitle = base.TitleTarget();
			return string.Format(NODE_TITLE, questionTitle);
		}

		protected override void OnEnableEditorChild ()
		{
			base.EnableEditorChild();
			
			this.spGameSceneName = this.serializedObject.FindProperty("gameSceneName");
			this.spAddressablesScene = this.serializedObject.FindProperty("addressablesScene");
			this.spLoadingSceneName = this.serializedObject.FindProperty("loadingSceneName");
		}

		protected override void OnDisableEditorChild ()
		{
			base.DisableEditorChild();
			
			this.spGameSceneName = null;
			this.spAddressablesScene = null;
			this.spLoadingSceneName = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spGameSceneName, new GUIContent("Scene Name Value"));
			EditorGUILayout.PropertyField(this.spAddressablesScene, new GUIContent("Addressables Scene"));
			EditorGUILayout.PropertyField(this.spLoadingSceneName, new GUIContent("Loading Scene Name"));

			base.InspectorGUI();

			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}
