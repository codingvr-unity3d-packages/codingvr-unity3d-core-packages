﻿namespace GameCreator.Core
{
	using System.Collections;
	using UnityEngine;
	using Zenject;
	using System;
	using System.Linq;

#if UNITY_EDITOR
	using UnityEditor;
#endif
	[AddComponentMenu("")]
	public class ActionFireCVREvent : IAction
	{
		public enum Events
		{
			none = 0,
			playerExitingGame,
		};

		public Events indexEvent = Events.none;
		public bool wait = false;
		private bool forceStop = false;

		private codingVR.Core.IEventManager eventManager;

		[Inject]
		public void Contruct(codingVR.Core.IEventManager eventManager)
		{
			this.eventManager = eventManager;
		}

		/* 
		 * 
		 * https://gitlab.com/monamipoto/potomaze/-/issues/590
		 * When the container for this object is a scriptable object; this HACK is used to inject dependency 
		 * 
		 */

		public void DIPInject(GameObject invoker)
		{
			if (this.eventManager == null)
			{
				if (invoker != null)
				{
					var dipInject = invoker.GetComponent<codingVR.GameEngine.IDIPInject>();
					if (dipInject != null)
						dipInject.DiContainer.Inject(this);
				}
			}
		}

		codingVR.Core.IGameEvent GetGameEvent(Events evt)
		{
			codingVR.Core.IGameEvent result = null;
			switch (evt)
			{
				case Events.playerExitingGame:
					result = new codingVR.Data.Event_PlayerExitingGame();
					break;
			}
			return result;
		}

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			bool eventSent = false;
			var gameEvent = GetGameEvent(indexEvent);
			if (gameEvent != null)
			{
				eventManager.QueueEvent(gameEvent);
				eventSent = true;
			}
			return !(eventSent && wait); //false => we continue
		}

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			yield return new WaitWhile(() => eventManager.HasEvent(GetGameEvent(indexEvent)) == true);
		}


#if UNITY_EDITOR

		public static new string NAME = "CodingVR/GameState/ActionFireCVREvent";
		private const string NODE_TITLE = "Fire {0} Events {1}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spEvents;
		private SerializedProperty spActive;
		private SerializedProperty spIndexEvent;
		private SerializedProperty spWait;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				this.wait ? "and Wait" : "",
				this.indexEvent.ToString()
			);
		}

		protected override void OnEnableEditorChild()
		{
			this.spIndexEvent = this.serializedObject.FindProperty("indexEvent");
			this.spWait = this.serializedObject.FindProperty("wait");
		}

		protected override void OnDisableEditorChild()
		{
			this.spIndexEvent = null;
			this.spWait = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			var events = System.Enum.GetNames(typeof(Events));

			this.spIndexEvent.intValue = EditorGUILayout.Popup(this.spIndexEvent.intValue, events);
			EditorGUILayout.PropertyField(this.spWait, new GUIContent("Wait To Finish"));

			this.serializedObject.ApplyModifiedProperties();
		}
#endif
	}
}

