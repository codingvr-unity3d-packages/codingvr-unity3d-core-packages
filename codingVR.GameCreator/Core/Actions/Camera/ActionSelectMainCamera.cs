﻿namespace GameCreator.Camera
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using GameCreator.Core;

	#if UNITY_EDITOR
	using UnityEditor;
	#endif

	[AddComponentMenu("")]
	public class ActionSelectMainCamera : IAction
	{
		public CameraMotor cameraMotor;

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			CameraMotor.MAIN_MOTOR = cameraMotor;
			return true;
		}

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			yield return 0;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

		#if UNITY_EDITOR

		public static new string NAME = "CodingVR/Camera/Select Main Camera";
		private const string NODE_TITLE = "Select {0} As Main Camera";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spCameraMotor;

		
		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			string motor = (this.cameraMotor == null ? "none" : this.cameraMotor.gameObject.name);
			return string.Format(NODE_TITLE, motor);
		}

		protected override void OnEnableEditorChild ()
		{
			this.spCameraMotor = this.serializedObject.FindProperty("cameraMotor");
		}

		protected override void OnDisableEditorChild ()
		{
			this.spCameraMotor = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spCameraMotor);

			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}
