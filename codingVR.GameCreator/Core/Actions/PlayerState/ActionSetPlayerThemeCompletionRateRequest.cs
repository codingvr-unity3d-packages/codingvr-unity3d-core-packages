﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Core;
	using GameCreator.Variables;
	using UnityEngine.SceneManagement;
    using Zenject;

#if UNITY_EDITOR
    using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionSetPlayerThemeCompletionRateRequest : IAction 
	{
		public NumberProperty completionRate = new NumberProperty(0.0f);
		codingVR.GameEngine.IPlayerStateTheme playerStateTheme;


		// CONSTRUCT: ---------------------------------------------------------------------------- 

		[Inject]
		public void Construct(codingVR.GameEngine.IPlayerStateTheme playerStateTheme)
		{
			this.playerStateTheme = playerStateTheme;
		}
		
		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			codingVR.Data.IThemeState themeState = playerStateTheme.FindCurrentThemeState();
			themeState.CompletionRateRequest = completionRate.GetValue();
			return true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

		#if UNITY_EDITOR

		public static new string NAME = "CodingVR/PlayerState/Set Player Theme Completion Rate Request";
		private const string NODE_TITLE = "Set Completion Rate Request {0}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spCompletionRate;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				this.completionRate
			);
		}

		protected override void OnEnableEditorChild ()
		{
			this.spCompletionRate = this.serializedObject.FindProperty("completionRate");
		}

		protected override void OnDisableEditorChild ()
		{
			this.spCompletionRate = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spCompletionRate);

			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}