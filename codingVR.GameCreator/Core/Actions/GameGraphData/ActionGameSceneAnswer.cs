﻿namespace GameCreator.Core
{
	using System.Collections;
	using UnityEngine;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionGameSceneAnswer : GameQuestionTarget
	{
		public string answer;
		DiContainer diContainer;

		// LIFECYCLE: ----------------------------------------------------------------------------

		[Inject]
		public void Constructor(DiContainer diContainer)
		{
			this.diContainer = diContainer;
		}

		/* 
		 * 
		 * https://gitlab.com/monamipoto/potomaze/-/issues/590
		 * When the container for this object is a scriptable object; this HACK is used to inject dependency 
		 * 
		 */

		public void DIPInject(GameObject invoker, object injectable)
		{
			if (this.diContainer == null)
			{
				if (invoker != null)
				{
					var dipInject = invoker.GetComponent<codingVR.GameEngine.IDIPInject>();
					if (dipInject != null)
					{
						dipInject.DiContainer.Inject(injectable);
						this.diContainer = dipInject.DiContainer;
					}
				}
			}
		}

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			return false;
		}

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			DIPInject(target, this);

			var data = SelectedData();
			if (data != null)
			{
				// load data 
				diContainer.Inject(data);
				yield return data.GetDataSpec();
				yield return data.GetData(true);

				// set data
				data.Answer = answer;

				// network post
				yield return data.PutData();
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/GameData/Answer";
		private const string NODE_TITLE = "At the question {0}, the answer is {1}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spAnswer;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			var questionTitle = base.TitleTarget();

			return string.Format(
				NODE_TITLE,
				questionTitle,
				answer
			);
		}

		protected override void OnEnableEditorChild()
		{
			base.EnableEditorChild();

			this.spAnswer = this.serializedObject.FindProperty("answer");
		}

		protected override void OnDisableEditorChild()
		{
			base.DisableEditorChild();

			this.spAnswer = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			base.InspectorGUI();

			EditorGUILayout.PropertyField(this.spAnswer, new GUIContent(Prefix + " Output"));
			this.serializedObject.ApplyModifiedProperties();
		}
#endif
	}
}