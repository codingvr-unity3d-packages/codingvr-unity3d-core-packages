﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameCreator.Core
{
	public class GameGraphDataBaseTarget<GraphDataScriptable, GraphDataStruct> : IAction where GraphDataScriptable : codingVR.Data.IGraphDataBaseScriptable<GraphDataScriptable, GraphDataStruct>
	{
		// DATA: ----------------------------------------------------------------------------

		[FormerlySerializedAs("graphDataNodeScriptable")]
		[FormerlySerializedAs("questionDataScript")]
		[SerializeField]
		GraphDataScriptable graphDataScript;
		
		// get selected data (implicit or explicit)
		public GraphDataScriptable SelectedData()
		{
			GraphDataScriptable ret = null;
			switch (questionDataTarget)
			{
				case QuestionDataTarget.Explicit:
						ret = graphDataScript ?? codingVR.Data.IGraphDataBaseScriptable<GraphDataScriptable, GraphDataStruct>.Selected;
					break;

				case QuestionDataTarget.Selected:
						ret = codingVR.Data.IGraphDataBaseScriptable<GraphDataScriptable, GraphDataStruct>.Selected;
					break;
			}
			
			return ret;
		}

		public void SelectExplicitData(GraphDataScriptable data)
		{
			this.questionDataTarget = QuestionDataTarget.Explicit;
			this.graphDataScript = data;
		}

		public enum QuestionDataTarget
		{
			Explicit = 0,
			Selected = 1
		}
		public QuestionDataTarget questionDataTarget;

#if UNITY_EDITOR

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spQuestion;
		private SerializedProperty spQuestionDataTarget;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public void EnableEditorChild()
		{
			this.spQuestion = this.serializedObject.FindProperty("graphDataScript");
			this.spQuestionDataTarget = this.serializedObject.FindProperty("questionDataTarget");
		}

		public void DisableEditorChild()
		{
			this.spQuestion = null;
			this.spQuestionDataTarget = null;
		}
		
		public void InspectorGUI()
		{
			EditorGUILayout.PropertyField(this.spQuestionDataTarget, new GUIContent(Prefix + " Target"));

			if (questionDataTarget == QuestionDataTarget.Explicit)
			{
				EditorGUILayout.PropertyField(this.spQuestion, new GUIContent(Prefix + " Input"));
			}
		}

		public string TagTarget()
		{
			string title = "Selected";
			switch (questionDataTarget)
			{
				case QuestionDataTarget.Explicit:
					if (graphDataScript != null)
						title = graphDataScript.Tag;
					break;

				case QuestionDataTarget.Selected:
					if (codingVR.Data.IGraphDataBaseScriptable<GraphDataScriptable, GraphDataStruct>.Selected != null)
						title = codingVR.Data.IGraphDataBaseScriptable<GraphDataScriptable, GraphDataStruct>.Selected.Tag;
					break;
			}
			return title;
		}


		public string TitleTarget()
		{
			string title = "Selected";
			switch (questionDataTarget)
			{
				case QuestionDataTarget.Explicit:
					if (graphDataScript != null)
						title = graphDataScript.Title;
				break;

				case QuestionDataTarget.Selected:
					if (codingVR.Data.IGraphDataBaseScriptable<GraphDataScriptable, GraphDataStruct>.Selected != null)
						title = codingVR.Data.IGraphDataBaseScriptable<GraphDataScriptable, GraphDataStruct>.Selected.Title;
				break;
			}
			return title;
		}

		public string Answer()
		{
			string title = "Selected";
			switch (questionDataTarget)
			{
				case QuestionDataTarget.Explicit:
					if (graphDataScript != null)
						title = graphDataScript.Answer;
					break;

				case QuestionDataTarget.Selected:
					if (codingVR.Data.IGraphDataBaseScriptable<GraphDataScriptable, GraphDataStruct>.Selected != null)
						title = codingVR.Data.IGraphDataBaseScriptable<GraphDataScriptable, GraphDataStruct>.Selected.Answer;
					break;
			}
			return title;
		}

		public string Prefix => typeof(GraphDataScriptable).Name;
#endif
	}
}
