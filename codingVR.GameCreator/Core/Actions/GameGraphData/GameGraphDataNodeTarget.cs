﻿using UnityEditor;
using UnityEngine;

namespace GameCreator.Core
{
    public class GameGraphDataNodeTarget : GameGraphDataBaseTarget<codingVR.Data.GraphDataNodeScriptable, codingVR.Data.GraphDataNodeScriptable.DataStruct>
    {
#if UNITY_EDITOR
        public static new string NAME = "CodingVR/GameGraphData/GameGraphDataNodeTargetAction";
#endif
    }
}
