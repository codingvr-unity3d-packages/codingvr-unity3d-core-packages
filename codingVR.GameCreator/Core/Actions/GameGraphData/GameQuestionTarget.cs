﻿using UnityEditor;
using UnityEngine;

namespace GameCreator.Core
{
    public class GameQuestionTarget : GameGraphDataBaseTarget<codingVR.Data.QuestionDataScriptable, codingVR.Data.QuestionDataScriptable.DataStruct>
    {
#if UNITY_EDITOR
        public static new string NAME = "CodingVR/GameGraphData/QuestionTargetAction";
#endif
    }
}
