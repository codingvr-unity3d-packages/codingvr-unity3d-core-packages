﻿namespace GameCreator.Core
{
	using System.Collections;
	using UnityEngine;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionGameQuestionSelect : IAction
	{
		public TargetGameObject target = new TargetGameObject();
		public codingVR.Data.QuestionDataScriptable questionDataScript;
		private DiContainer diContainer;

		// CONSTRUCTOR: ----------------------------------------------------------------------------

		/* 
		 * 
		 * https://gitlab.com/monamipoto/potomaze/-/issues/590
		 * When the container for this object is a scriptable object; this HACK is used to inject dependency 
		 * 
		 */

		public void DIPInject(GameObject invoker, object injectable)
		{
			if (this.diContainer == null)
			{
				if (invoker != null)
				{
					var dipInject = invoker.GetComponent<codingVR.GameEngine.IDIPInject>();
					if (dipInject != null)
					{
						dipInject.DiContainer.Inject(injectable);
						this.diContainer = dipInject.DiContainer;
					}
				}
			}
		}

		[Inject]
		public void Constructor(DiContainer diContainer)
		{
			this.diContainer = diContainer;
		}

		// EXECUTE: ----------------------------------------------------------------------------

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			DIPInject(target, this);

			// selection link
			codingVR.Data.QuestionDataScriptable.Selected = questionDataScript;

			// get data from link spec and result
			var data = codingVR.Data.QuestionDataScriptable.Selected;
			if (data != null)
			{
				// load data 
				diContainer.Inject(data);
				yield return data.GetDataSpec();
				yield return data.GetData(true);
			}

			yield return 0;
		}
		
#if UNITY_EDITOR

		public static new string NAME = "CodingVR/GameData/Select Question";
		private const string NODE_TITLE = "The Selected Question is {0}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spQuestion;
		
		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			string questionInfo = "???";
			if (questionDataScript != null)
			{
				questionInfo = questionDataScript.Tag;
			}

			return string.Format(
				NODE_TITLE,
				questionInfo
			);
		}

		protected override void OnEnableEditorChild()
		{
			this.spQuestion = this.serializedObject.FindProperty("questionDataScript");
		}

		protected override void OnDisableEditorChild()
		{
			this.spQuestion = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();
			EditorGUILayout.PropertyField(this.spQuestion, new GUIContent(typeof(codingVR.Data.QuestionDataScriptable).Name + " Input"));
			this.serializedObject.ApplyModifiedProperties();
		}
#endif
	}
}