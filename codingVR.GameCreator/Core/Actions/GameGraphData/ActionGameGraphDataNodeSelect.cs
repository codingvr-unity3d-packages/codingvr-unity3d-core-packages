﻿namespace GameCreator.Core
{
	using System.Collections;
	using UnityEngine;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionGameGraphDataNodeSelect : IAction
	{
		public TargetGameObject target = new TargetGameObject();

		public codingVR.Data.GraphDataNodeScriptable graphDataNodeScriptable;

		private DiContainer diContainer;

		// CONSTRUCTOR: ----------------------------------------------------------------------------

		/* 
		 * 
		 * https://gitlab.com/monamipoto/potomaze/-/issues/590
		 * When the container for this object is a scriptable object; this HACK is used to inject dependency 
		 * 
		 */

		public void DIPInject(GameObject invoker, object injectable)
		{
			if (this.diContainer == null)
			{
				if (invoker != null)
				{
					var dipInject = invoker.GetComponent<codingVR.GameEngine.IDIPInject>();
					if (dipInject != null)
					{
						dipInject.DiContainer.Inject(injectable);
						this.diContainer = dipInject.DiContainer;
					}
				}
			}
		}

		[Inject]
		public void Constructor(DiContainer diContainer)
		{
			this.diContainer = diContainer;
		}

		// EXECUTE: ----------------------------------------------------------------------------

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			DIPInject(target, this);

			// selection link
			codingVR.Data.GraphDataNodeScriptable.Selected = graphDataNodeScriptable;

			// get data from link spec and result
			var data = codingVR.Data.GraphDataNodeScriptable.Selected;
			if (data != null)
			{
				// load data 
				diContainer.Inject(data);
				yield return data.GetDataSpec();
				yield return data.GetData(true);
			}

			yield return 0;
		}

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/GameData/Select Graph Data Node";
		private const string NODE_TITLE = "The Selected Graph Data Node is {0}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spGraphDataNode;
		
		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			string questionInfo = "???";
			if (graphDataNodeScriptable != null)
			{
				questionInfo = graphDataNodeScriptable.Data.question;
			}

			return string.Format(
				NODE_TITLE,
				questionInfo
			);
		}

		protected override void OnEnableEditorChild()
		{
			this.spGraphDataNode = this.serializedObject.FindProperty("graphDataNodeScriptable");
		}

		protected override void OnDisableEditorChild()
		{
			this.spGraphDataNode = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();
			EditorGUILayout.PropertyField(this.spGraphDataNode, new GUIContent(typeof(codingVR.Data.GraphDataNodeScriptable).Name + " Input"));
			this.serializedObject.ApplyModifiedProperties();
		}
#endif
	}
}