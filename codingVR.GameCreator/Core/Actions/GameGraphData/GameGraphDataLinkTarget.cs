﻿using UnityEditor;
using UnityEngine;

namespace GameCreator.Core
{
    public class GameGraphDataLinkTarget : GameGraphDataBaseTarget<codingVR.Data.QuestionDataScriptable, codingVR.Data.QuestionDataScriptable.DataStruct>
    {
#if UNITY_EDITOR
        public static new string NAME = "CodingVR/GameGraphData/GameGraphDataLinkTargetAction";
#endif
    }
}
