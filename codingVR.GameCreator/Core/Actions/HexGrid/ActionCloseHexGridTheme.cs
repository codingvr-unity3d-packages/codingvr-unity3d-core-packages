﻿namespace GameCreator.Core
{
	using System.Collections;
	using UnityEngine;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionCloseHexGridTheme : IAction
	{
		public TargetGameObject target = new TargetGameObject();

		// hex grid manager
		private codingVR.HexMapNavigation.IHexGridManagerOrigin hexGridManager;
		/// theme			
		public string theme;

		[Inject]
		public void Contruct(codingVR.HexMapNavigation.IHexGridManagerOrigin hexGridManager)
		{
			this.hexGridManager = hexGridManager;
		}

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			return false;
		}

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			hexGridManager.CloseMap();
			yield return 0;
		}

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/HexGrid/CloseHexGridTheme";
		private const string NODE_TITLE = "Close hex grid theme : {0}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spQuestion;
		private SerializedProperty spTheme;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				theme
			);
		}

		protected override void OnEnableEditorChild()
		{
			this.spTheme = this.serializedObject.FindProperty("theme");
		}

		protected override void OnDisableEditorChild()
		{
			this.spQuestion = null;
			this.spTheme = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spTheme, new GUIContent("Theme Value"));

			this.serializedObject.ApplyModifiedProperties();
		}

#endif
	}
}
