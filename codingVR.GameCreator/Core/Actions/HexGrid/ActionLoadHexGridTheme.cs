﻿namespace GameCreator.Core
{
	using System.Collections;
	using UnityEngine;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionLoadHexGridTheme : IAction
	{
		public TargetGameObject target = new TargetGameObject();

		// ScreenPlay
		private codingVR.GameEngine.IScreenPlay screenPlay;
		// hex grid manager
		private codingVR.HexMapNavigation.IHexGridManagerOrigin hexGridManager;
		// resourced loader
		private codingVR.Scene.IResourcesLoader resourcesLoader;
		// theme
		public string theme;

		[Inject]
		public void Contruct(codingVR.GameEngine.IScreenPlay screenPlay, codingVR.HexMapNavigation.IHexGridManagerOrigin hexGridManager, codingVR.Scene.IResourcesLoader resourcesLoader)
		{
			this.screenPlay = screenPlay;
			this.hexGridManager = hexGridManager;
			this.resourcesLoader = resourcesLoader;
		}

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			return false;
		}

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			var path = codingVR.HexMapNavigation.HexGridLoader.GetSelectedPath(theme);
			yield return codingVR.HexMapNavigation.HexGridLoader.Load(hexGridManager, resourcesLoader, path, screenPlay.AddressablesScene);
			yield return 0;
		}

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/HexGrid/LoadHexGridTheme";
		private const string NODE_TITLE = "Load hex grid theme : {0}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spTheme;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				theme
			);
		}

		protected override void OnEnableEditorChild()
		{
			this.spTheme = this.serializedObject.FindProperty("theme");
		}

		protected override void OnDisableEditorChild()
		{
			this.spTheme = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spTheme, new GUIContent("Theme Value"));

			this.serializedObject.ApplyModifiedProperties();
		}

#endif
	}
}