﻿namespace GameCreator.Core
{
	using UnityEngine;
	using Zenject;
#if UNITY_EDITOR
	using UnityEditor;
	#endif

	[AddComponentMenu("")]
	public class ActionConfigureHexGridManager : IAction 
	{
		private codingVR.HexMapNavigation.IHexGridManagerOrigin hexGridManager;

		// === Hex Grid prefabs ===

		[SerializeField]
		Object cellPrefab;
		[SerializeField]
		Object cellLabelPrefab;
		[SerializeField]
		Object chunkPrefab;
		[SerializeField]
		Object gridPrefab;

		[Inject]
		public void Construct(codingVR.HexMapNavigation.IHexGridManagerOrigin hexGridManager)
		{
			this.hexGridManager = hexGridManager;
		}

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			hexGridManager.HexChunkPrefab = chunkPrefab;
			hexGridManager.HexCellLabelPrefab = cellLabelPrefab;
			hexGridManager.HexCellPrefab = cellPrefab;
			hexGridManager.HexGridPrefab = gridPrefab;

			return true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

		#if UNITY_EDITOR

		public static new string NAME = "CodingVR/HexGrid/Manager";
		private const string NODE_TITLE = "Hex Grid Manager Setup";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spCellPrefab;
		private SerializedProperty spCellLabelPrefab;
		
		private SerializedProperty spChunkPrefab;
		private SerializedProperty spGridPrefab;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE
			);
		}

		protected override void OnEnableEditorChild ()
		{
			this.spCellPrefab = this.serializedObject.FindProperty("cellPrefab");
			this.spCellLabelPrefab = this.serializedObject.FindProperty("cellLabelPrefab");
			this.spChunkPrefab = this.serializedObject.FindProperty("chunkPrefab");
			this.spGridPrefab = this.serializedObject.FindProperty("gridPrefab");
		}

		protected override void OnDisableEditorChild ()
		{
			return;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spCellPrefab);
			EditorGUILayout.PropertyField(this.spCellLabelPrefab);
			EditorGUILayout.PropertyField(this.spChunkPrefab);
			EditorGUILayout.PropertyField(this.spGridPrefab);

			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}