﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Camera;
	using GameCreator.Core.Hooks;
	using GameCreator.Messages;
	using GameCreator.Localization;
	using GameCreator.Variables;
	using GameCreator.Characters;
    using Zenject;
	using System;
	using NaughtyAttributes;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionCharacterConversation : IAction
	{

		private const string TOOLTIP_TRANS_TIME = "0: No transition. Values between 0.5 and 1.5 are recommended";

		// PROPERTIES: ----------------------------------------------------------------------------

		// TargetGameObject characters
		[Header("Characters")]
		public TargetGameObject interlocutor = new TargetGameObject(TargetGameObject.Target.GameObject);
		public TargetGameObject speaker = new TargetGameObject(TargetGameObject.Target.GameObject);

		//LookAt properties
		[Header("Lookat")]
		public bool hasLookAt = false;
		[RotationConstraint]
		public Vector3 freezeRotation = new Vector3(1, 0, 1);

		//Camera properties
		[Header("Camera")]
		public bool hasCameraChange = false;
		public TargetGameObject cameraMotorObject = new TargetGameObject(TargetGameObject.Target.GameObject);
		[Obsolete("cameraMotor is deprecated. Use cameraMotorObject instead.", false)]
		public CameraMotor cameraMotor;
		public CameraMotorTypeTarget cameraMotorTarget;

		[Header("Transition")]
		[Tooltip(TOOLTIP_TRANS_TIME)]
		[Range(0.0f, 60.0f)]
		public float transitionTime = 0.0f;

		//Animation properties
		[Header("Animation")]
		public bool hasAnimation = false;
		public AnimationClip clip;
		public NumberProperty speed = new NumberProperty(1.0f);
		public float fadeIn = 0.1f;
		public float fadeOut = 0.1f;
		public bool waitTillComplete = false;
		private CharacterAnimator characterAnimator;

		//Message properties
		[Header("Message")]
		public bool hasMessage = false;
		[LocStringNoPostProcess][TextArea]
		public LocString message = new LocString();
		[TextArea(3, 7)]
		public string textArea = "";
		public float timeMessage = 2.0f;
		public Vector3 offset = new Vector3(0, 2, 0);
		public Color color = Color.black;
		public bool isAnimated = false;

		// DATA: ----------------------------------------------------------------------------

		private bool forceStop = false;
		private codingVR.Core.IEventManager eventManager;

		public int nbClick = 0;
		private ActionFacingFloatingMessage actionFloatingMessage;

		// CONSTRUCTOR: ----------------------------------------------------------------------------

		[Inject]
        public void Contruct(codingVR.Core.IEventManager eventManager)
        {
            this.eventManager = eventManager;
        }

		/* 
		 * 
		 * https://gitlab.com/monamipoto/potomaze/-/issues/590
		 * When the container for this object is a scriptable object; this HACK is used to inject dependency 
		 * 
		 */

		public void DIPInject(GameObject invoker)
		{
			if (this.eventManager == null)
			{
				if (invoker != null)
				{
					var dipInject = invoker.GetComponent<codingVR.GameEngine.IDIPInject>();
					if (dipInject != null)
						dipInject.DiContainer.Inject(this);
				}
			}
		}

        private void OnClickMouse(codingVR.Data.Event_OnClickMouse ev)
        {
			Debug.Log("Onclick mouse conversation nbClick : " + nbClick);
			if (isAnimated == true && actionFloatingMessage != null)
			{
				if (nbClick > 0 || actionFloatingMessage.endAnimated == true)
				{
					Stop();
				}

				else
				{
					nbClick++;
				}
			}
			else
			{			
				Stop();
			}
		}

        // EXECUTABLE: ----------------------------------------------------------------------------

        public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			// https://gitlab.com/monamipoto/potomaze/-/issues/590
			DIPInject(target);

			this.forceStop = false;
           
            
			//LookAt
            if (hasLookAt)
			{
				LookAtInterlocutor(target, interlocutor, speaker);
				yield return new WaitForSeconds(0.2f);
				LookAtInterlocutor(target, speaker, interlocutor);
			}
		  
			//Camera change
			if (hasCameraChange)
			{
				// get camera motor from game object
				var cameraMotorObjectInstance = cameraMotorObject.GetGameObject(target);
				if (cameraMotorObjectInstance != null)
					cameraMotor = cameraMotorObjectInstance.GetComponent<CameraMotor>();

				// setup camera from camera motor
				ActionCameraChange actionCameraChange = this.gameObject.AddComponent(typeof(ActionCameraChange)) as ActionCameraChange;
				TargetPosition targetPos = new TargetPosition(TargetPosition.Target.Transform);
				targetPos.targetTransform = speaker.GetGameObject(target).transform;
				cameraMotorTarget = (CameraMotorTypeTarget)cameraMotor.cameraMotorType;
				cameraMotorTarget.anchor = interlocutor;
				targetPos.offset = cameraMotorTarget.target.offset;
				cameraMotorTarget.target = targetPos;
				cameraMotor.cameraMotorType = cameraMotorTarget;
				actionCameraChange.cameraMotor = cameraMotor;
				actionCameraChange.InstantExecute(target, null, -1);
				yield return new WaitForSeconds(transitionTime);
			}

			//Animation
			if (hasAnimation)
			{
				ActionCharacterGesture actionCharacterGesture = this.gameObject.AddComponent(typeof(ActionCharacterGesture)) as ActionCharacterGesture;

				actionCharacterGesture.clip = this.clip;
				actionCharacterGesture.speed = this.speed;
				actionCharacterGesture.fadeIn = this.fadeIn;
				actionCharacterGesture.fadeOut = this.fadeOut;
				actionCharacterGesture.waitTillComplete = this.waitTillComplete;
				TargetCharacter targetCharacter = new TargetCharacter(TargetCharacter.Target.Character);
				targetCharacter.character = this.speaker.GetComponent<Character>(target);
				actionCharacterGesture.character = targetCharacter;

				actionCharacterGesture.InstantExecute(target, null, -1);
			}

			//Message
			if (hasMessage)
			{
				this.actionFloatingMessage = this.gameObject.AddComponent(typeof(ActionFacingFloatingMessage)) as ActionFacingFloatingMessage;

				actionFloatingMessage.message = this.message;
				actionFloatingMessage.time = this.timeMessage;
				actionFloatingMessage.offset = this.offset;
				actionFloatingMessage.color = this.color;
				actionFloatingMessage.audioClip = null;
				actionFloatingMessage.target = speaker;
				actionFloatingMessage.isAnimated = this.isAnimated;
                actionFloatingMessage.SetEventManager(this.eventManager);

                StartCoroutine(actionFloatingMessage.Execute(target, null, -1));

				if (eventManager != null)
					eventManager.AddListener<codingVR.Data.Event_OnClickMouse>(OnClickMouse);

				if (this.isAnimated)
				{
					yield return new WaitUntil(() => actionFloatingMessage.endAnimated || this.forceStop);
				}

				//yield return new WaitForSeconds(timeMessage);
				float waitTime = Time.time + this.timeMessage;
                WaitUntil waitUntil = new WaitUntil(() => Time.time > waitTime || this.forceStop);
                yield return waitUntil;

			}

			if (eventManager != null)
				eventManager.RemoveListener<codingVR.Data.Event_OnClickMouse>(OnClickMouse);

			yield return 0;
		}

		public void LookAtInterlocutor(GameObject target,TargetGameObject interlocutorWhoLook, TargetGameObject interlocutorToLook)
		{  
			ActionLookAt actionLookAt = this.gameObject.AddComponent(typeof(ActionLookAt)) as ActionLookAt;
			actionLookAt.target = interlocutorWhoLook;
			TargetPosition targetPos = new TargetPosition(TargetPosition.Target.Transform);
			targetPos.targetTransform = interlocutorToLook.GetGameObject(target).transform;
			actionLookAt.lookAtPosition = targetPos;
			actionLookAt.InstantExecute(target, null, -1);
		}
			   
		public override void Stop()
		{
			this.forceStop = true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR
		public static new string NAME = "Custom/ActionCharacterConversation";
		private const string NODE_TITLE = "{0} start conv with {1}";

		// PROPERTIES: ----------------------------------------------------------------------------

		//TargetGameObject characters
		private SerializedProperty spInterlocutor;
		private SerializedProperty spSpeaker;

		//LookAt properties
		private SerializedProperty spHasLookAt;

		//Camera properties
		private SerializedProperty spHasCameraChange;
		private SerializedProperty spCameraMotor;
		private SerializedProperty spCameraMotorObject;
		private SerializedProperty spTransitionTime;

		//Animation properties
		private SerializedProperty spHasAnimation;
		private SerializedProperty spClip;
		private SerializedProperty spWaitTillComplete;
		private SerializedProperty spSpeed;
		private SerializedProperty spFadeIn;
		private SerializedProperty spFadeOut;

		//Message properties
		private SerializedProperty spHasMessage;
		private SerializedProperty spMessage;
		private SerializedProperty spTextArea;
		private SerializedProperty spColor;
		private SerializedProperty spTime;
		private SerializedProperty spOffset;
		private SerializedProperty spIsAnimated;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				interlocutor.ToString(),
				speaker.ToString()
			);
		}

		protected override void OnEnableEditorChild()
		{
			//TargetGameObject characters
			this.spInterlocutor = this.serializedObject.FindProperty("interlocutor");
			this.spSpeaker = this.serializedObject.FindProperty("speaker");

			//LookAt
			this.spHasLookAt = this.serializedObject.FindProperty("hasLookAt");

			//Camera
			this.spHasCameraChange = this.serializedObject.FindProperty("hasCameraChange");
			this.spCameraMotor = this.serializedObject.FindProperty("cameraMotor");
			this.spCameraMotorObject = this.serializedObject.FindProperty("cameraMotorObject");
			this.spTransitionTime = this.serializedObject.FindProperty("transitionTime");

			//Animation
			this.spHasAnimation = this.serializedObject.FindProperty("hasAnimation");
			this.spClip = this.serializedObject.FindProperty("clip");
			this.spWaitTillComplete = this.serializedObject.FindProperty("waitTillComplete");
			this.spSpeed = this.serializedObject.FindProperty("speed");
			this.spFadeIn = this.serializedObject.FindProperty("fadeIn");
			this.spFadeOut = this.serializedObject.FindProperty("fadeOut");

			//Message properties
			this.spHasMessage = this.serializedObject.FindProperty("hasMessage");
			this.spMessage = this.serializedObject.FindProperty("message");
			this.spTextArea = this.serializedObject.FindProperty("textArea");
			this.spColor = this.serializedObject.FindProperty("color");
			this.spTime = this.serializedObject.FindProperty("timeMessage");
			this.spOffset = this.serializedObject.FindProperty("offset");
			this.spIsAnimated = this.serializedObject.FindProperty("isAnimated");

		}

		protected override void OnDisableEditorChild()
		{
			//TargetGameObject characters
			this.spInterlocutor = null;
			this.spSpeaker = null;

			//Camera
			this.spCameraMotor = null;
			this.spCameraMotorObject = null;
			this.spTransitionTime = null;

			//Animation
			this.spClip = null;
			this.spWaitTillComplete = null;
			this.spSpeed = null;
			this.spFadeIn = null;
			this.spFadeOut = null;

			//Message properties
			this.spMessage = null;
			this.spTextArea = null;
			this.spColor = null;
			this.spTime = null;
			this.spOffset = null;
			this.spIsAnimated = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			//TargetGameObject characters
			EditorGUILayout.PropertyField(this.spInterlocutor);
			EditorGUILayout.Space();
			EditorGUILayout.PropertyField(this.spSpeaker);

			EditorGUILayout.PropertyField(this.spHasLookAt);

			//Camera
			EditorGUILayout.PropertyField(this.spHasCameraChange);
			if (this.spHasCameraChange.boolValue)
			{
				EditorGUILayout.Space();
				EditorGUILayout.PropertyField(this.spCameraMotorObject);
				EditorGUILayout.Space();
				EditorGUILayout.PropertyField(this.spCameraMotor, new GUIContent("DEPRECATED Camera Motor"));
				EditorGUILayout.Space();
				EditorGUILayout.PropertyField(this.spTransitionTime);
			}
			EditorGUILayout.Space();

			//Animation
			EditorGUILayout.PropertyField(this.spHasAnimation);
			if (this.spHasAnimation.boolValue)
			{
				EditorGUILayout.PropertyField(this.spClip);
				EditorGUI.indentLevel++;
				EditorGUILayout.PropertyField(this.spSpeed);
				EditorGUILayout.PropertyField(this.spFadeIn);
				EditorGUILayout.PropertyField(this.spFadeOut);
			  //  EditorGUILayout.PropertyField(this.spWaitTillComplete);
				EditorGUI.indentLevel--;
			}
			
			EditorGUILayout.Space();
			//Message properties
			EditorGUILayout.PropertyField(this.spHasMessage);

			if (spHasMessage.boolValue)
			{
				EditorGUILayout.PropertyField(this.spMessage);
				EditorGUILayout.PropertyField(this.spTextArea);

				if (GUILayout.Button("Appliquer texte au message"))
				{
					this.message.content = this.textArea;
				}

				EditorGUILayout.PropertyField(this.spColor);

				EditorGUILayout.Space();
				EditorGUILayout.PropertyField(this.spTime);
				EditorGUILayout.Space();
				EditorGUILayout.PropertyField(this.spOffset);
				EditorGUILayout.PropertyField(this.spIsAnimated);
			}
			
			this.serializedObject.ApplyModifiedProperties();
		}
#endif
	}
}

