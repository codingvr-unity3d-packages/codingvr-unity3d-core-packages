﻿namespace GameCreator.Characters
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Core;
    using GameCreator.Variables;

	#if UNITY_EDITOR
	using UnityEditor;
	#endif

	[AddComponentMenu("")]
	public class ActionCharacterDynamicScale : IAction 
	{
        public TargetCharacter target = new TargetCharacter(TargetCharacter.Target.Player);
        public NumberProperty scale = new NumberProperty(5.0f);
        public NumberProperty speed = new NumberProperty(0.5f);

        // EXECUTABLE: ----------------------------------------------------------------------------

        public override bool InstantExecute(GameObject target, IAction[] actions, int index)
        {
            Character charTarget = this.target.GetCharacter(target);
            /*
            if (charTarget != null)
            {
                switch (this.changeProperty)
                {
                    case CHANGE_PROPERTY.IsControllable:
                        bool isControllable = this.valueBool.GetValue(target);
                        charTarget.characterLocomotion.SetIsControllable(isControllable);
                        break;

                    case CHANGE_PROPERTY.CanRun:
                        charTarget.characterLocomotion.canRun = this.valueBool.GetValue(target);
                        break;

                    case CHANGE_PROPERTY.SetRunSpeed:
                        charTarget.characterLocomotion.runSpeed = this.valueNumber.GetValue(target);
                        break;

                    case CHANGE_PROPERTY.Height:
                        charTarget.characterLocomotion.ChangeHeight(this.valueNumber.GetValue(target));
                        break;

                    case CHANGE_PROPERTY.JumpForce:
                        charTarget.characterLocomotion.jumpForce = this.valueNumber.GetValue(target);
                        break;

                    case CHANGE_PROPERTY.JumpTimes:
                        charTarget.characterLocomotion.jumpTimes = this.valueNumber.GetInt(target);
                        break;

                    case CHANGE_PROPERTY.Gravity:
                        charTarget.characterLocomotion.gravity = this.valueNumber.GetValue(target);
                        break;

                    case CHANGE_PROPERTY.MaxFallSpeed:
                        charTarget.characterLocomotion.maxFallSpeed = this.valueNumber.GetValue(target);
                        break;

                    case CHANGE_PROPERTY.CanJump:
                        charTarget.characterLocomotion.canJump = this.valueBool.GetValue(target);
                        break;
                }
            }
            */


            return false;
        }

        delegate float linear(float scale);

        public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
        {
            Character charTarget = this.target.GetCharacter(target);
            if (charTarget != null)
            {
                if (charTarget.characterLocomotion != null)
                {
                    if (charTarget.characterLocomotion.characterController != null)
                    {
                        if (charTarget.characterLocomotion.characterController.enabled)
                        {

                            /*
                             * 
                             * computation speed according to scale
                             * =====================================
                             * speed(scale) = a * scale + b
                             * we consider a linerar interpolation
                             * speed(1) = 5
                             * speed(5) = 24
                             * =>
                             * a = 19/4
                             * b = 1/4
                             *                      
                             * computation speed according to scale
                             * =====================================
                             * height(scale) = a * scale + b
                             * we consider a linerar interpolation
                             * height(1) = 2
                             * height(5) = 2.19
                             * =>
                             * 
                             * a * 1.0 + b = 2
                             * a * 5.0 + b = 2.19
                             * 2.0 - a * 1.0 = 2.19 - a * 5.0
                             * a * 4.0 = 0.19
                             * a = 0.19 / 4.0
                             * b = (8 - 0.19) / 4
                             * 
                             * a = 0.2/4
                             * b = 7.8/4
                             *                      
                             */

                            void setLocomotion(float factor)
                            {
                                // speed
                                linear runSpeed = scale => (19.0f * scale - 1.0f) / 4.0f;
                                float newScale = scale.GetValue() * factor + charTarget.gameObject.transform.localScale.y * (1.0f - factor);
                                charTarget.characterLocomotion.runSpeed = runSpeed(newScale);
                                // height
                                linear runHeight = scale => (0.19f * scale + (8.0f - 0.19f)) / 4.0f;
                                float newHeight = scale.GetValue() * factor + charTarget.gameObject.transform.localScale.y * (1.0f - factor);

                                var h0 = runHeight(1.0f);
                                var h1 = runHeight(5.0f);

                                charTarget.characterLocomotion.characterController.height = runHeight(newScale);
                                // set scale
                                charTarget.gameObject.transform.localScale = new Vector3(newScale, newScale, newScale);
                            };

                            for (float f = 0.0f; f < 1.0f; f += Time.deltaTime * speed.GetValue())
                            {
                                setLocomotion(f);
                                yield return null;
                            }
                            setLocomotion(1.0f);
                        }
                    }
                }
            }
            yield return 0;
        }

        // +--------------------------------------------------------------------------------------+
        // | EDITOR                                                                               |
        // +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

        public static new string NAME = "CodingVR/Character/Dynamic Scaling";
		private const string NODE_TITLE = "Dynamic Scaling {0} to {1} at {2} m/s";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spTarget;
		private SerializedProperty spScale;
        private SerializedProperty spSpeed;


        // INSPECTOR METHODS: ---------------------------------------------------------------------

        public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
                this.target,
				this.scale,
                this.speed
            );
		}

		protected override void OnEnableEditorChild ()
		{
			this.spTarget = this.serializedObject.FindProperty("target");
            this.spScale = this.serializedObject.FindProperty("scale");
            this.spSpeed = this.serializedObject.FindProperty("speed");
        }

		protected override void OnDisableEditorChild ()
		{
            this.spTarget = null;
            this.spScale = null;
            this.spSpeed = null;
        }

        public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

            EditorGUILayout.PropertyField(this.spTarget);
			EditorGUILayout.PropertyField(this.spScale);
            EditorGUILayout.PropertyField(this.spSpeed);

            this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}