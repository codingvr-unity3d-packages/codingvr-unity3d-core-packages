﻿namespace GameCreator.Characters
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Core;

	#if UNITY_EDITOR
	using UnityEditor;
	#endif

	[AddComponentMenu("")]
	public class ActionCharacterAccompanies : IAction 
	{
		public enum ActionType
		{
			Follow,
			Stop,
			Suspend,
			Continue,
		}

		// Character Target & Actions	
		public TargetCharacter character = new TargetCharacter();
		public ActionType actionType = ActionType.Follow;
		public TargetCharacter followTarget = new TargetCharacter();

		// Waypoint properties
		public float followMinRadius = codingVR.GameEngine.IWaypointSettings._radiusMin;
		public float followMaxRadius = codingVR.GameEngine.IWaypointSettings._radiusMax;
		public bool teleportationOverMaxRadius = codingVR.GameEngine.IWaypointSettings._teleportationOverMaxRadius;
		public float minRunSpeed = codingVR.GameEngine.IWaypointSettings._runSpeedMin;
		public float maxRunSpeed = codingVR.GameEngine.IWaypointSettings._runSpeedMax;
		public Vector3 directionalOffset = codingVR.GameEngine.IWaypointSettings._directionalOffset;

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			Character charTarget = this.character.GetCharacter(target);

			if (charTarget == null)
			{
				return true;
			}


			switch (actionType)
			{
				case ActionType.Follow:
					{
						codingVR.GameEngine.ICharacterAccompanies charactersAccompaniesPlayer = charTarget.gameObject.GetComponent<codingVR.GameEngine.ICharacterAccompanies>();
						if (charactersAccompaniesPlayer == null)
						{
							charactersAccompaniesPlayer = charTarget.gameObject.AddComponent<CharacterAccompanies>();
							charactersAccompaniesPlayer.AddWaypoint(followTarget.GetCharacter(target).transform, followMinRadius, followMaxRadius, teleportationOverMaxRadius, minRunSpeed, maxRunSpeed, directionalOffset);
						}
						else
						{
							Debug.LogErrorFormat("{0} : CharacterAccompanies already bound; impossible to add a new waypoint {1} !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, followTarget.GetCharacter(target));
						}
					}
					break;

				case ActionType.Stop:
					{
						var listCharacterAccompanies = charTarget.gameObject.GetComponents<codingVR.GameEngine.ICharacterAccompanies>();
						if (listCharacterAccompanies.Length > 0)
						{
							foreach (CharacterAccompanies characterAccompanies in listCharacterAccompanies)
							{
								DestroyImmediate(characterAccompanies);
							}
						}
						else
						{
							Debug.LogErrorFormat("{0} : No CharacterAccompanies bound !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
						}
					}
					break;

				case ActionType.Suspend:
					{
						codingVR.GameEngine.ICharacterAccompanies charactersAccompaniesPlayer = charTarget.gameObject.GetComponent<codingVR.GameEngine.ICharacterAccompanies>();
						if (charactersAccompaniesPlayer != null)
						{
							charactersAccompaniesPlayer.Continue = false;
						}
						else
						{
							Debug.LogErrorFormat("{0} : CharacterAccompanies not bound !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
						}
					}
					break;

				case ActionType.Continue:
					{
						codingVR.GameEngine.ICharacterAccompanies charactersAccompaniesPlayer = charTarget.gameObject.GetComponent<codingVR.GameEngine.ICharacterAccompanies>();
						if (charactersAccompaniesPlayer != null)
						{
							charactersAccompaniesPlayer.Continue = true;
						}
						else
						{
							Debug.LogErrorFormat("{0} : CharacterAccompanies not bound !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
						}
					}
					break;
			}

			return true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/Character/Character Accompagnies";
		private const string NODE_TITLE = "{0} {1} {2}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spCharacter;
		private SerializedProperty spActionType;
		private SerializedProperty spFollowTarget;

		private SerializedProperty spMinRadius;
		private SerializedProperty spMaxRadius;
		private SerializedProperty spTeleportationOverMaxRadius;
		private SerializedProperty spMinRunSpeed;
		private SerializedProperty spMaxRunSpeed;
		private SerializedProperty spDirectionalOffset;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				this.character,
				"Accompanies", //ObjectNames.NicifyVariableName(this.actionType.ToString()),
				(this.actionType == ActionType.Follow ? this.followTarget.ToString() : "Stop")
			);
		}

		protected override void OnEnableEditorChild ()
		{
			this.spCharacter = this.serializedObject.FindProperty("character");
			this.spActionType = this.serializedObject.FindProperty("actionType");
			this.spFollowTarget = this.serializedObject.FindProperty("followTarget");

			this.spMinRadius = this.serializedObject.FindProperty("followMinRadius");
			this.spMaxRadius = this.serializedObject.FindProperty("followMaxRadius");
			this.spTeleportationOverMaxRadius = this.serializedObject.FindProperty("teleportationOverMaxRadius");
			this.spMinRunSpeed = this.serializedObject.FindProperty("minRunSpeed");
			this.spMaxRunSpeed = this.serializedObject.FindProperty("maxRunSpeed");
			this.spDirectionalOffset = this.serializedObject.FindProperty("directionalOffset");
		}

		protected override void OnDisableEditorChild ()
		{
			this.spCharacter = null;
			this.spActionType = null;
			this.spFollowTarget = null;

			this.spMinRadius = null;
			this.spMaxRadius = null;
			this.spTeleportationOverMaxRadius = null;
			this.spMinRunSpeed = null;
			this.spMaxRunSpeed = null;
			this.spDirectionalOffset = null;
			return;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spCharacter);
			EditorGUILayout.PropertyField(this.spActionType);

			if ((ActionType)this.spActionType.intValue == ActionType.Follow)
			{
				EditorGUILayout.Space();
				EditorGUILayout.PropertyField(this.spFollowTarget);
				EditorGUILayout.Space();
				EditorGUILayout.PropertyField(this.spMinRadius);  
				EditorGUILayout.PropertyField(this.spMaxRadius);
				EditorGUILayout.PropertyField(this.spTeleportationOverMaxRadius);
				EditorGUILayout.PropertyField(this.spMinRunSpeed);
				EditorGUILayout.PropertyField(this.spMaxRunSpeed);
				EditorGUILayout.PropertyField(this.spDirectionalOffset);
			}

			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}