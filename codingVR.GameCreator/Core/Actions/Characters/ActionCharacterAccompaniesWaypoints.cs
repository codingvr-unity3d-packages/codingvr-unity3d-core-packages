﻿namespace GameCreator.Characters
{
	using UnityEngine;
	using GameCreator.Core;

	#if UNITY_EDITOR
	using UnityEditor;
	#endif

	[AddComponentMenu("")]
	public class ActionCharacterAccompaniesWaypoints : IAction 
	{
		// character, target & actions
		public TargetCharacter character = new TargetCharacter();
		public enum Operation
		{
			add,
			replaceLast,
			removeLast,
		}
		public Operation operation;
		public TargetGameObject target = new TargetGameObject();

		// Waypoint properties
		public float followMinRadius = codingVR.GameEngine.IWaypointSettings._radiusMin;
		public float followMaxRadius = codingVR.GameEngine.IWaypointSettings._radiusMax;
		public bool teleportationOverMaxRadius = codingVR.GameEngine.IWaypointSettings._teleportationOverMaxRadius;
		public float minRunSpeed = codingVR.GameEngine.IWaypointSettings._runSpeedMin;
		public float maxRunSpeed = codingVR.GameEngine.IWaypointSettings._runSpeedMax;
		public Vector3 directionalOffset = codingVR.GameEngine.IWaypointSettings._directionalOffset;
		
		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			Character charTarget = this.character.GetCharacter(target);
			if (charTarget == null)
			{
				return true;
			}

			codingVR.GameEngine.ICharacterAccompanies charactersAccompaniesPlayer = charTarget.gameObject.GetComponent<codingVR.GameEngine.ICharacterAccompanies>();
			if (charactersAccompaniesPlayer != null)
			{
				switch(operation)
				{
					case Operation.add:
						charactersAccompaniesPlayer.AddWaypoint(this.target.GetGameObject(target).transform, followMinRadius, followMaxRadius, teleportationOverMaxRadius, minRunSpeed, maxRunSpeed, directionalOffset);
					break;
					case Operation.replaceLast:
						charactersAccompaniesPlayer.ReplaceLastWaypoint(this.target.GetGameObject(target).transform, followMinRadius, followMaxRadius, teleportationOverMaxRadius, minRunSpeed, maxRunSpeed, directionalOffset);
						break;
					case Operation.removeLast:
						charactersAccompaniesPlayer.RemoveLastWaypoint();
					break;
				}

			}
			else
			{
				Debug.LogErrorFormat("{0} : CharacterAccompanies not bound !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}

			return true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/Character/Character Accompagnies goes through waypoint steps";
		private const string NODE_TITLE = "{0} {1} {2}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spCharacter;
		private SerializedProperty spOperation;
		private SerializedProperty spTarget;

		private SerializedProperty spMinRadius;
		private SerializedProperty spMaxRadius;
		private SerializedProperty spTeleportationOverMaxRadius;
		private SerializedProperty spMinRunSpeed;
		private SerializedProperty spMaxRunSpeed;
		private SerializedProperty spDirectionalOffset;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				this.character,
				"Accompanies",
				" Goes Through Waypoint Steps"
			);
		}

		protected override void OnEnableEditorChild ()
		{
			this.spCharacter = this.serializedObject.FindProperty("character");
			this.spOperation = this.serializedObject.FindProperty("operation");
			this.spTarget = this.serializedObject.FindProperty("target");

			this.spMinRadius = this.serializedObject.FindProperty("followMinRadius");
			this.spMaxRadius = this.serializedObject.FindProperty("followMaxRadius");
			this.spTeleportationOverMaxRadius = this.serializedObject.FindProperty("teleportationOverMaxRadius");
			this.spMinRunSpeed = this.serializedObject.FindProperty("minRunSpeed");
			this.spMaxRunSpeed = this.serializedObject.FindProperty("maxRunSpeed");
			this.spDirectionalOffset = this.serializedObject.FindProperty("directionalOffset");
		}

		protected override void OnDisableEditorChild ()
		{
			this.spCharacter = null;
			this.spOperation = null;
			this.spTarget = null;

			this.spMinRadius = null;
			this.spMaxRadius = null;
			this.spTeleportationOverMaxRadius = null;
			this.spMinRunSpeed = null;
			this.spMaxRunSpeed = null;
			this.spDirectionalOffset = null;
			return;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spCharacter);
			EditorGUILayout.PropertyField(this.spOperation);

			switch (operation)
			{
				case Operation.add:
				case Operation.replaceLast:
					EditorGUILayout.Space();
					EditorGUILayout.PropertyField(this.spTarget);
					EditorGUILayout.Space();
					EditorGUILayout.PropertyField(this.spMinRadius);
					EditorGUILayout.PropertyField(this.spMaxRadius);
					EditorGUILayout.PropertyField(this.spTeleportationOverMaxRadius);
					EditorGUILayout.PropertyField(this.spMinRunSpeed);
					EditorGUILayout.PropertyField(this.spMaxRunSpeed);
					EditorGUILayout.PropertyField(this.spDirectionalOffset);
					break;
				case Operation.removeLast:
					break;
			}
		
			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}