﻿namespace GameCreator.Characters
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Core;

	#if UNITY_EDITOR
	using UnityEditor;
	#endif

	[AddComponentMenu("")]
	public class ActionCharacterAccompaniesDefaultParameters : IAction 
	{
		public TargetCharacter character = new TargetCharacter();
		public TargetCharacter followTarget = new TargetCharacter();

		// Waypoint properties
		public float followMinRadius = codingVR.GameEngine.IWaypointSettings._radiusMin;
		public float followMaxRadius = codingVR.GameEngine.IWaypointSettings._radiusMax;
		public bool teleportationOverMaxRadius = codingVR.GameEngine.IWaypointSettings._teleportationOverMaxRadius;
		public float minRunSpeed = codingVR.GameEngine.IWaypointSettings._runSpeedMin;
		public float maxRunSpeed = codingVR.GameEngine.IWaypointSettings._runSpeedMax;
		public Vector3 directionalOffset = codingVR.GameEngine.IWaypointSettings._directionalOffset;

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			Character charTarget = this.character.GetCharacter(target);

			if (charTarget == null)
			{
				return true;
			}
			
			codingVR.GameEngine.ICharacterAccompanies charactersAccompaniesPlayer = charTarget.gameObject.GetComponent<codingVR.GameEngine.ICharacterAccompanies>();
			if (charactersAccompaniesPlayer != null)
			{
				charactersAccompaniesPlayer.ChangeLastWaypoint(followMinRadius, followMaxRadius, teleportationOverMaxRadius, minRunSpeed, maxRunSpeed, directionalOffset);
			}
			else
			{
				Debug.LogErrorFormat("{0} : CharacterAccompanies already bound !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}

			return true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/Character/Character Accompagnies And Set Default Parameters";
		private const string NODE_TITLE = "{0} {1} {2}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spCharacter;
				
		private SerializedProperty spFollowTarget;
		private SerializedProperty spMinRadius;
		private SerializedProperty spMaxRadius;
		private SerializedProperty spTeleportationOverMaxRadius;

		private SerializedProperty spMinRunSpeed;
		private SerializedProperty spMaxRunSpeed;

		private SerializedProperty spDirectionalOffset;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				this.character,
				"Accompanies", //ObjectNames.NicifyVariableName(this.actionType.ToString()),
				this.followTarget.ToString(),
				" And Set Default Parameters"
			);
		}

		protected override void OnEnableEditorChild ()
		{
			this.spCharacter = this.serializedObject.FindProperty("character");
			this.spFollowTarget = this.serializedObject.FindProperty("followTarget");
			this.spMinRadius = this.serializedObject.FindProperty("followMinRadius");
			this.spMaxRadius = this.serializedObject.FindProperty("followMaxRadius");
			this.spTeleportationOverMaxRadius = this.serializedObject.FindProperty("teleportationOverMaxRadius");
			this.spMinRunSpeed = this.serializedObject.FindProperty("minRunSpeed");
			this.spDirectionalOffset = this.serializedObject.FindProperty("directionalOffset");
			this.spMaxRunSpeed = this.serializedObject.FindProperty("maxRunSpeed");
		}

		protected override void OnDisableEditorChild ()
		{
			return;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spCharacter);

			EditorGUILayout.Space();
			EditorGUILayout.PropertyField(this.spFollowTarget);

			EditorGUILayout.PropertyField(this.spMinRadius);  
			EditorGUILayout.PropertyField(this.spMaxRadius);
			EditorGUILayout.PropertyField(this.spTeleportationOverMaxRadius);

			EditorGUILayout.PropertyField(this.spMinRunSpeed);
			EditorGUILayout.PropertyField(this.spMaxRunSpeed);

			EditorGUILayout.PropertyField(this.spDirectionalOffset);
		
			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}