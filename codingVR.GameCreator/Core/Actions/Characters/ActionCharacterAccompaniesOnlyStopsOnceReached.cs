﻿namespace GameCreator.Characters
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Core;

	#if UNITY_EDITOR
	using UnityEditor;
	#endif

	[AddComponentMenu("")]
	public class ActionCharacterAccompaniesOnlyStopsOnceReached : IAction 
	{
		public enum ActionType
		{
			Follow,
			StopFollow
		}

		public TargetCharacter character = new TargetCharacter();
		public TargetCharacter followTarget = new TargetCharacter();
		
		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			return false;
		}

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			Character charTarget = this.character.GetCharacter(target);

			if (charTarget == null)
			{
				yield return 0;
			}

			var accompaniesPlayer = charTarget.gameObject.GetComponent<CharacterAccompanies>();
			if (accompaniesPlayer != null)
			{
				// wait while last waypoint is not reached
				yield return new WaitUntil(() => accompaniesPlayer.HasReachedLastWaypoint);
				// remove last way point
				accompaniesPlayer.RemoveLastWaypoint();
			}
			else
			{
				Debug.LogErrorFormat("{0} : the component CharacterAccompanies is not bound !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/Character/The Accompanying Stops Moving Once Reached";
		private const string NODE_TITLE = "{0} {1} {2}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spCharacter;
		
		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				"The Accompanying",
				this.character,
				"Stops Moving Once Reached"
			);
		}

		protected override void OnEnableEditorChild ()
		{
			this.spCharacter = this.serializedObject.FindProperty("character");
		}

		protected override void OnDisableEditorChild ()
		{
			return;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spCharacter);
			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}