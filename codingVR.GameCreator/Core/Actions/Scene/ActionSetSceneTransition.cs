﻿namespace GameCreator.Core
{
	using System;
	using System.Collections;
	using UnityEngine;
	using UnityEngine.SceneManagement;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionSetSceneTransition : IAction
	{
		public string gameSceneName;
		private ZenjectSceneLoader loader;
		private DiContainer diContainer;
		private codingVR.Scene.ISceneContext sceneTransition;
		public bool active = true;

		[Inject]
		public void Contruct(DiContainer diContainer, ZenjectSceneLoader loader, codingVR.Scene.ISceneContext sceneTransition)
		{
			this.loader = loader;
			this.diContainer = diContainer;
			this.sceneTransition = sceneTransition;
		}

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			GameObject sceneUITransition = sceneTransition.SceneUITransition;
			if (sceneUITransition != null)
				sceneUITransition.SetActive(active);

			return true;
		}

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			yield return 0;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/Scene/Set Scene Transition";
		private const string NODE_TITLE = "Set scene transition {0}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spActive;
		
		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				(this.active ? "active" : "inactive")
			);
		}

		protected override void OnEnableEditorChild ()
		{
			this.spActive = this.serializedObject.FindProperty("active");
		}

		protected override void OnDisableEditorChild ()
		{
			this.spActive = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spActive);

			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}
