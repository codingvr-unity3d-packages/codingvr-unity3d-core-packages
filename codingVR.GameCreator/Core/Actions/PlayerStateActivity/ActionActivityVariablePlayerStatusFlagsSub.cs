﻿namespace GameCreator.Core
{
	using UnityEngine;

	[AddComponentMenu("")]
	public class ActionActivityVariablePlayerStatusFlagsSub : ActionActivityVariableOperationBase<codingVR.Data.ActivityInstancePlayerStatusFlags>
	{
		// EXECUTABLE: ----------------------------------------------------------------------------

		override protected bool InstantExecuteOperation(codingVR.Data.IActivityField activityField, string name, object value)
		{
			return activityField.SubValue(name, value);
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/Activity/Variable Player Status Flags Sub";
		private const string NODE_TITLE = "Activity Variable Player Status Flags Sub {0} to {1}";

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				base.variable,
				base.value
			);
		}
#endif
	}
}