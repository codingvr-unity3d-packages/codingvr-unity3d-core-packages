﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Core;
	using GameCreator.Variables;
	using UnityEngine.SceneManagement;
	using GameCreator.Behavior;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public abstract class ActionActivityVariableOperationBase<TYPE> : IAction
	{
		public TargetActivityVariable target;

		public string variable;
		public TYPE value;

		// PRIVATE-DATA: -------------------------------------------------------------------------

		public codingVR.GameEngine.IPlayerStateActivityController playerStateActivityController;
		codingVR.GameEngine.IPlayerStateManager playerStateMananger;

		// LIFECYCLE: ----------------------------------------------------------------------------

		[Inject]
		public void Constructor(codingVR.GameEngine.IPlayerStateActivityController playerStateActivityController, codingVR.GameEngine.IPlayerStateManager playerStateMananger)
		{
			this.playerStateActivityController = playerStateActivityController;
			this.playerStateMananger = playerStateMananger;
		}

		/* 
		 * 
		 * https://gitlab.com/monamipoto/potomaze/-/issues/590
		 * When the container for this object is a scriptable object; this HACK is used to inject dependency 
		 * 
		 */

		public void DIPInject(GameObject invoker)
		{
			if (this.playerStateActivityController == null)
			{
				if (invoker != null)
				{
					var dipInject = invoker.GetComponent<codingVR.GameEngine.IDIPInject>();
					if (dipInject != null)
						dipInject.DiContainer.Inject(this);
				}
			}
		}

		// EXECUTABLE: ----------------------------------------------------------------------------

		abstract protected bool InstantExecuteOperation(codingVR.Data.IActivityField activityField, string name, object value);

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			// https://gitlab.com/monamipoto/potomaze/-/issues/590
			DIPInject(target);

			(var activityField, var activityNestedField) = this.target.GetActivityField(playerStateActivityController, playerStateMananger, target);
			if (activityField != null)
			{
				bool bResult = false;

				if (activityNestedField != null)
				{
					bResult = InstantExecuteOperation(activityField, activityNestedField.ParseNestedFields(variable), value);
				}
				else
				{
					bResult = InstantExecuteOperation(activityField, variable, value);
				}

				if (bResult == false)
				{ 
					Debug.LogErrorFormat("{0} : field {1} from {2} not found !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, variable, activityField);
				}
			}
			return false;
		}

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			yield return new WaitForEndOfFrame();
			yield return new WaitForEndOfFrame();
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spTarget;
		private SerializedProperty spVariable;
		private SerializedProperty spValue;


		protected override void OnEnableEditorChild()
		{
			this.spTarget = this.serializedObject.FindProperty("target");
			this.spVariable = this.serializedObject.FindProperty("variable");
			this.spValue = this.serializedObject.FindProperty("value");
		}

		protected override void OnDisableEditorChild()
		{
			this.spTarget = null;
			this.spVariable = null;
			this.spValue = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spTarget);
			EditorGUILayout.PropertyField(this.spVariable);
			EditorGUILayout.PropertyField(this.spValue);

			this.serializedObject.ApplyModifiedProperties();
		}

#endif
	}
}