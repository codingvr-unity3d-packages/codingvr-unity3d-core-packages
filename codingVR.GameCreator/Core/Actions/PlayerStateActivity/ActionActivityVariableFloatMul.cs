﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Core;
	using GameCreator.Variables;
	using UnityEngine.SceneManagement;
	using GameCreator.Behavior;

#if UNITY_EDITOR
	using UnityEditor;
	using System;

#endif

	[AddComponentMenu("")]
	public class ActionActivityVariableFloatMul : ActionActivityVariableOperationBase<float>
	{
		// EXECUTABLE: ----------------------------------------------------------------------------

		override protected bool InstantExecuteOperation(codingVR.Data.IActivityField activityField, string name, object value)
		{
			return activityField.MulValue(name, value);
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/Activity/Variable Float Mul";
		private const string NODE_TITLE = "Activity Variable Float Mul {0} to {1}";

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				base.variable,
				base.value
			);
		}
#endif
	}
}