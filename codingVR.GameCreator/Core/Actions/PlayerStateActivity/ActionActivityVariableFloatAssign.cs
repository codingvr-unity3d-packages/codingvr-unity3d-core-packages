﻿namespace GameCreator.Core
{
	using UnityEngine;
	using System;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionActivityVariableFloatAssign : ActionActivityVariableOperationBase<float>
	{
		// EXECUTABLE: ----------------------------------------------------------------------------

		override protected bool InstantExecuteOperation(codingVR.Data.IActivityField activityField, string name, object value)
		{
			return activityField.SetValue(name, value);
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/Activity/Variable Float Assign";
		private const string NODE_TITLE = "Activity Variable Float Assign {0} to {1}";

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				base.variable,
				base.value
			);
		}
#endif
	}
}