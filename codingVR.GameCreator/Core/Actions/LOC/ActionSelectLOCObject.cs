﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Core;
	using GameCreator.Variables;
	using UnityEngine.SceneManagement;
	using codingVR.Core;
    using Zenject;

#if UNITY_EDITOR
    using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionSelectLOCObject : IAction 
	{
		public NumberProperty completionRate = new NumberProperty(0.0f);
		public StringProperty sceneName = new StringProperty();

		codingVR.GameEngine.IPlayerState playerState;


		// CONSTRUCT: ---------------------------------------------------------------------------- 

		[Inject]
		public void Construct(codingVR.GameEngine.IPlayerState playerState)
		{
			this.playerState = playerState;
		}
		
		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			var sceneIndex = SceneManager.GetSceneByName(sceneName.value);
			var rootObjects = sceneIndex.GetRootGameObjects();
			foreach(var it in rootObjects)
			{
				var locManager = it.GetComponentInChildren<codingVR.PotoGame.HubThemeSlots.ILOCManager>();
				if (locManager != null)
				{
					var subRootObjects = this.gameObject.scene.GetRootGameObjects();
					foreach (var i1t in subRootObjects)
					{
						var subLocManager = i1t.GetComponentInChildren<codingVR.PotoGame.HubThemeSlots.ILOCManager>();
						if (subLocManager != null)
						{
							var worldTransform = subLocManager.SelectedTransform.SaveWorld();
							locManager.SelectedTransform.LoadWorld(worldTransform);
							it.gameObject.SetActive(true);
						}
					}


				}
			}
			return true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

		#if UNITY_EDITOR

		public static new string NAME = "CodingVR/LOC/Select LOC Object";
		private const string NODE_TITLE = "Select LOC Object In {0}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spSceneName;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				this.sceneName
			);
		}

		protected override void OnEnableEditorChild ()
		{
			this.spSceneName = this.serializedObject.FindProperty("sceneName");
		}

		protected override void OnDisableEditorChild ()
		{
			this.spSceneName = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spSceneName);

			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}