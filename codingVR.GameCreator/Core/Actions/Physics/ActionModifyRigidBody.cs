﻿namespace GameCreator.Core
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

#if UNITY_EDITOR
    using UnityEditor;
#endif

    [AddComponentMenu("")]
    public class ActionModifyRigidBody : IAction
    {
        public TargetGameObject target = new TargetGameObject();
        public bool freezePosition = false;
        public bool freezeRotation = false;

        public override bool InstantExecute(GameObject target, IAction[] actions, int index)
        {
            GameObject targetValue = this.target.GetGameObject(target);
            if (targetValue != null)
            {
                Rigidbody rigidBody = target.GetComponent<Rigidbody>();

                var constr = rigidBody.constraints;

                if (freezePosition)
                {
                    constr = RigidbodyConstraints.FreezePosition;
                }
                if(freezeRotation)
                {
                    constr = RigidbodyConstraints.FreezeRotation;
                }
                
            }

            return true;
        }

#if UNITY_EDITOR
        public static new string NAME = "CodingVR/Physics/ModifyRigidBody";
        private const string NODE_TITLE = "Modifify Rigidbody of object {0}";

        // PROPERTIES: ----------------------------------------------------------------------------

        private SerializedProperty spTarget;
        private SerializedProperty spFreezePosition;
        private SerializedProperty spFreezeRotation;

        // INSPECTOR METHODS: ---------------------------------------------------------------------

        public override string GetNodeTitle()
        {
            return string.Format(
                NODE_TITLE,
                this.target
            );
        }

        protected override void OnEnableEditorChild()
        {
            this.spTarget = this.serializedObject.FindProperty("target");
            this.spFreezePosition = this.serializedObject.FindProperty("freezePosition");
            this.spFreezeRotation = this.serializedObject.FindProperty("freezeRotation");
        }

        protected override void OnDisableEditorChild()
        {
            this.spTarget = null;
            this.spFreezePosition = null;
            this.spFreezeRotation = null;
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            EditorGUILayout.PropertyField(this.spTarget);
            EditorGUILayout.PropertyField(this.spFreezePosition);
            EditorGUILayout.PropertyField(this.spFreezeRotation);


            this.serializedObject.ApplyModifiedProperties();
        }

#endif
    }
}