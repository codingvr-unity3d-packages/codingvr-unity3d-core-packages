﻿namespace GameCreator.Core
{
	using System.Collections;
	using UnityEngine;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionGameManagerSpawnPlayer : IAction
	{
		private codingVR.GameEngine.IScreenPlay screenPlay;
		private codingVR.GameEngine.IMapManager mapManager;
		private IFactory<UnityEngine.Object, codingVR.GameEngine.ICharacterTravel> playerFactory;
		private codingVR.Scene.IResourcesLoader resourcesLoader;
		public TargetGameObject prefabModel = new TargetGameObject(TargetGameObject.Target.GameObject);
		public bool addressables = true;

		[Inject]
		public void Contruct(codingVR.GameEngine.IScreenPlay screenPlay, codingVR.GameEngine.IMapManager mapManager, codingVR.Scene.IResourcesLoader resourcesLoader, IFactory<UnityEngine.Object, codingVR.GameEngine.ICharacterTravel> playerFactory)
		{
			this.screenPlay = screenPlay;
			this.mapManager = mapManager;
			this.playerFactory = playerFactory;
			this.resourcesLoader = resourcesLoader;
		}

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			return false;
		}

		public IEnumerator SpawnPlayer(GameObject target, codingVR.Data.Tag tag)
		{
			bool bResult = false;
			GameObject playerPrefab = this.prefabModel.GetGameObject(target);
			if (playerPrefab != null)
			{
				Object obj = playerPrefab;
				// with addressable we can't share a static referenced prefab form scene to a another scene 
				// so we have to load explicitelythis prefab
				if (addressables)
				{
					resourcesLoader.LoadResource(playerPrefab.name, true);
					yield return resourcesLoader.Wait(playerPrefab.name);
					obj = resourcesLoader.GetResource(playerPrefab.name);
				}
				var playerTravel = playerFactory.Create(obj);
				var player = playerTravel.GameObject;
				if (player != null)
				{
					bResult = mapManager.Spawn(codingVR.Data.Tag.CreateTag(tag), player);
				}
				if (bResult == true)
				{
					// we activate the tag associated to character
					yield return screenPlay.ActivateObject(codingVR.Data.Tag.CreateTag(tag), null);
				}
				else
				{
					GameObject.Destroy(player);
					Debug.LogError("Impossible to create a new Player !!!");
				}
			}
			else
			{
				Debug.LogError("Player prefab is null !!!");
			}
			yield return null;
		}

		public IEnumerator TeleportPlayer(codingVR.Data.Tag tag)
		{
			var playerCharacter = Hooks.HookPlayer.Instance.Get<GameCreator.Characters.PlayerCharacter>();
			codingVR.GameEngine.ICharacterTravel characterTravel = playerCharacter.GetComponentInChildren<codingVR.GameEngine.ICharacterTravel>();
			characterTravel.Teleport(tag);
			// we activate the tag associated to character
			yield return screenPlay.ActivateObject(codingVR.Data.Tag.CreateTag(tag), null);
		}
			   
		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			if (GameCreator.Core.Hooks.HookPlayer.Instance == null)
			{
				yield return SpawnPlayer(target, codingVR.Data.Tag.CreateTag(screenPlay.LastScreenPlayObjectTag));
			}
			else
			{
				yield return TeleportPlayer(codingVR.Data.Tag.CreateTag(screenPlay.LastScreenPlayObjectTag));
			}

			yield return 0;
		}

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/GameManager/SpawnPlayer";
		private const string NODE_TITLE = "Spawn player";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spPrefabModel;
		private SerializedProperty spAddressables;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE
			);
		}

		protected override void OnEnableEditorChild()
		{
			this.spPrefabModel = this.serializedObject.FindProperty("prefabModel");
			this.spAddressables = this.serializedObject.FindProperty("addressables");
		}

		protected override void OnDisableEditorChild()
		{
			this.spPrefabModel = null;
			this.spAddressables = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spPrefabModel);
			EditorGUILayout.PropertyField(this.spAddressables);

			this.serializedObject.ApplyModifiedProperties();
		}

#endif
	}
}