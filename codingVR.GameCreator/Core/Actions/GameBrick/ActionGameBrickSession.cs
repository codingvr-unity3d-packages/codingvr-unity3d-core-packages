﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Variables;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionGameBrickSession : GameGraphDataNodeTarget
	{
		public enum Source
		{
			Actions,
			Variable
		}
		public Source source = Source.Actions;
		public Actions actions;
		private static int bound = 0;

		private codingVR.Core.IEventManager eventManager;
		private DiContainer diContainer;

		private codingVR.Patterns.ISingleInstanceRunning<ActionGameBrickSession> singleInstanceRunning = new codingVR.Patterns.ISingleInstanceRunning<ActionGameBrickSession>();

		[ReadOnly]
		public string answer;

		[VariableFilter(Variable.DataType.GameObject)]
		public VariableProperty variable = new VariableProperty(Variable.VarType.LocalVariable);

		public bool waitToFinish = false;

		private bool actionsComplete = false;
		private bool forceStop = false;

		// CONSTRUCTOR: ----------------------------------------------------------------------------

		/* 
		 * 
		 * https://gitlab.com/monamipoto/potomaze/-/issues/590
		 * When the container for this object is a scriptable object; this HACK is used to inject dependency 
		 * 
		 */

		public void DIPInject(GameObject invoker, object injectable)
		{
			if (this.diContainer == null)
			{
				if (invoker != null)
				{
					var dipInject = invoker.GetComponent<codingVR.GameEngine.IDIPInject>();
					if (dipInject != null)
					{
						dipInject.DiContainer.Inject(injectable);
						this.diContainer = dipInject.DiContainer;
					}
				}
			}
		}

		[Inject]
		public void Constructor(DiContainer diContainer, codingVR.Core.IEventManager eventManager)
		{
			this.eventManager = eventManager;
			this.diContainer = diContainer;
		}

		//Issue #577 Is called when the player has clicked on the exit game button and the gameBrickSession
		//get the Event_PlayerExitingGame
		private void OnPlayerExitingGame(codingVR.Data.Event_PlayerExitingGame ev)
		{
			Stop();
		}

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			DIPInject(target, this);

			yield return singleInstanceRunning.Lock();
			{
				this.forceStop = false;

				Actions actionsToExecute = null;

				switch (this.source)
				{
					case Source.Actions:
						actionsToExecute = this.actions;
						break;

					case Source.Variable:
						GameObject value = this.variable.Get(target) as GameObject;
						if (value != null) actionsToExecute = value.GetComponent<Actions>();
						break;
				}

				if (actionsToExecute != null)
				{
					this.actionsComplete = false;

					var gameBrick = actionsToExecute.gameObject.GetComponent<codingVR.Data.IGameSessionDataNode>();
					if (gameBrick != null)
					{
						var data = SelectedData();
						if (data != null)
						{
							// Select data
							var sessionData = gameBrick.SelectData(data);
							if (sessionData != null)
							{
								// load data 
								diContainer.Inject(sessionData);
								yield return sessionData.GetDataSpec();
								yield return sessionData.GetData(true);
							}

							// Start and execute game scene
							yield return gameBrick.StartSession();
							// execute actions
							actionsToExecute.actionsList.Execute(target, this.OnCompleteActions);

							// Enable History UI 
							if (eventManager != null)
							{
								eventManager.QueueEvent(new codingVR.Data.Event_GameStateHistoryEnableUI(true));
								//Issue #577 Add a listener to the Event_OnExitGame to know when to stop the session
								eventManager.AddListener<codingVR.Data.Event_PlayerExitingGame>(OnPlayerExitingGame);
							}
							// waiting completion
							yield return new WaitUntil(() => gameBrick.Status == codingVR.GameEngine.GameSessionStatus.isCompleted || this.forceStop == true);

							// Disable History UI
							if (eventManager != null)
							{
								eventManager.QueueEvent(new codingVR.Data.Event_GameStateHistoryEnableUI(false));
								//Issue #577 remove the listener OnExitGame
								eventManager.RemoveListener<codingVR.Data.Event_PlayerExitingGame>(OnPlayerExitingGame);
							}

							// manage special case force stop
							if (this.forceStop == true)
							{
								actionsToExecute.actionsList.Stop();
							}

							// answer
							answer = gameBrick.Reply == null ? "" : gameBrick.Reply.Answer;
							Debug.Log("BeforeStopSession");
							// Stop game scene
							yield return gameBrick.StopSession();
							Debug.Log("AfterStopSession");

							// network post
							var reply = gameBrick.Reply;
							if (reply != null)
							{
								diContainer.Inject(reply);
								yield return reply.PutData();
							}

							// reset internal state
							forceStop = false;
						}
						else
						{
							Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
						}
					}
				}
				else
				{
					Debug.LogWarningFormat("{0} : the game object {1} doesn't support IGameSessionDataNode !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, actionsToExecute.gameObject.name);
				}
			}
			Debug.Log("Unlock");
			singleInstanceRunning.Unlock();
			yield return 0;
		}

		private void OnCompleteActions()
		{
			this.actionsComplete = true;
		}

		public override void Stop()
		{
			Debug.Log("Stop");
			this.forceStop = true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/GameBrick/Session";
		private const string NODE_TITLE = "Game brick session runs {0} {1}; input #{2}, answer '{3}'";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spSource;
		private SerializedProperty spActions;
		private SerializedProperty spVariable;

		private SerializedProperty spWaitToFinish;

		private SerializedProperty spAnswer;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			var questionTitle = base.TagTarget();

			string actionsName = (this.source == Source.Actions
				? (this.actions == null ? "none" : this.actions.name)
				: this.variable.ToString()
			);

			return string.Format(
				NODE_TITLE,
				actionsName,
				(this.waitToFinish ? "and Wait" : ""),
				questionTitle,
				answer
			);
		}

		protected override void OnEnableEditorChild()
		{
			base.EnableEditorChild();

			this.spSource = this.serializedObject.FindProperty("source");
			this.spVariable = this.serializedObject.FindProperty("variable");
			this.spActions = this.serializedObject.FindProperty("actions");
			this.spWaitToFinish = this.serializedObject.FindProperty("waitToFinish");

			this.spAnswer = this.serializedObject.FindProperty("answer");
		}

		protected override void OnDisableEditorChild()
		{
			base.DisableEditorChild();

			this.spSource = null;
			this.spVariable = null;
			this.spActions = null;
			this.spWaitToFinish = null;

			this.spAnswer = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spSource);
			switch (this.spSource.enumValueIndex)
			{
				case (int)Source.Actions:
					EditorGUILayout.PropertyField(this.spActions);
					break;

				case (int)Source.Variable:
					EditorGUILayout.PropertyField(this.spVariable);
					break;
			}

			base.InspectorGUI();

			EditorGUILayout.PropertyField(this.spAnswer, new GUIContent(Prefix + " Output"));
			EditorGUILayout.PropertyField(this.spWaitToFinish);

			this.serializedObject.ApplyModifiedProperties();
		}
#endif
	}
}