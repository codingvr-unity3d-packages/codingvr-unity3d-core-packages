﻿namespace GameCreator.Core
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;
    using GameCreator.Variables;
    using Zenject;

#if UNITY_EDITOR
    using UnityEditor;
#endif

    [AddComponentMenu("")]
    public class ActionGameBrickSessionGroup : IAction
    {
        // EXECUTABLE: ----------------------------------------------------------------------------

        public codingVR.Data.GraphDataNodeGroupScriptable graphDataNodeGroup;
        public codingVR.Data.GraphDataNodeScriptable graphDataNode;
        public List<Actions> actionsGroup;
        public List<Actions> actionsTransition;
        public Actions actions;

        public bool waitToFinish = false;

        private bool actionsComplete = false;
        private bool forceStop = false;

        private codingVR.Core.IEventManager eventManager;
        private codingVR.Patterns.ISingleInstanceRunning<ActionGameBrickSessionGroup> singleInstanceRunning = new codingVR.Patterns.ISingleInstanceRunning<ActionGameBrickSessionGroup>();

        [Inject]
        public void Constructor(codingVR.Core.IEventManager eventManager)
        {
            this.eventManager = eventManager;
        }

        public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
        {
			yield return singleInstanceRunning.Lock();

			this.forceStop = false;

			Actions actionsToExecute = this.actions;
            if (actionsToExecute != null)
            {
                this.actionsComplete = false;

                var gameLoop = actionsToExecute.gameObject.GetComponent<codingVR.GameEngine.IWidgetLoop>();
                if (gameLoop != null)
                {
                    var data = graphDataNodeGroup;
                    if (data != null)
                    {
                        // Start and execute loop
                        Actions[] actionsGroup = new Actions[this.actionsGroup.Count];
                        Actions[] actionsTransition = new Actions[this.actionsGroup.Count];

                        int i = 0;
                        foreach (Actions action in this.actionsGroup)
                        {
                            actionsGroup[i] = action;
                            i++;
                        }

                        graphDataNodeGroup.actions = this.actionsTransition;
  
                        gameLoop.StartLoop(actionsGroup, graphDataNodeGroup, graphDataNode);
                        // execute actions
                        actionsToExecute.actionsList.Execute(target, this.OnCompleteActions);
                       
                        // Enable History UI 
                        eventManager.QueueEvent(new codingVR.Data.Event_GameStateHistoryEnableUI(true));

                        //waiting completion
                        yield return new WaitUntil(() => gameLoop.Status == codingVR.GameEngine.GameLoopStatus.isCompleted || this.forceStop == true);

                        // Disable History UI 
                        eventManager.QueueEvent(new codingVR.Data.Event_GameStateHistoryEnableUI(false));

                        // manage special case force stop
                        if (this.forceStop == true)
                        {
                            actionsToExecute.actionsList.Stop();
                        }

                        // Stop game scene
                        yield return gameLoop.StopLoop();

                        // reset internal state
                        forceStop = false;

                    }
                    else
                    {
                        Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            else
            {
                Debug.LogWarningFormat("{0} : the game object {1} doesn't support IGameSessionDataNode !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, actionsToExecute.gameObject.name);
            }

           singleInstanceRunning.Unlock();
            yield return 0;
        }

        private void OnCompleteActions()
        {
            this.actionsComplete = true;
        }

        public override void Stop()
        {
            this.forceStop = true;
        }

        // +--------------------------------------------------------------------------------------+
        // | EDITOR                                                                               |
        // +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

        public static new string NAME = "CodingVR/GameBrick/Session group";
        private const string NODE_TITLE = "GameBrick Group Session";

        // PROPERTIES: ----------------------------------------------------------------------------

        private SerializedProperty spGraphDataNodeGroup;
        private SerializedProperty spGraphDataNode;
        private SerializedProperty spActionsGroup;
        private SerializedProperty spActions;
        private SerializedProperty spWaitToFinish;
        private SerializedProperty spActionsTransition;

        // INSPECTOR METHODS: ---------------------------------------------------------------------

        public override string GetNodeTitle()
        {
            return string.Format(
                NODE_TITLE
            );
        }

        protected override void OnEnableEditorChild()
        {
            this.spGraphDataNodeGroup = this.serializedObject.FindProperty("graphDataNodeGroup");
            this.spActionsGroup = this.serializedObject.FindProperty("actionsGroup");
            this.spActions = this.serializedObject.FindProperty("actions");
            this.spActionsTransition = this.serializedObject.FindProperty("actionsTransition");
            this.spWaitToFinish = this.serializedObject.FindProperty("waitToFinish");
            this.spGraphDataNode = this.serializedObject.FindProperty("graphDataNode");
        }

        protected override void OnDisableEditorChild()
        {
            return;
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            EditorGUILayout.LabelField("Widget loop", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(this.spActions);
            EditorGUILayout.PropertyField(this.spGraphDataNode);
            EditorGUILayout.LabelField("Group widgets", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(this.spGraphDataNodeGroup);
            EditorGUILayout.PropertyField(this.spActionsGroup);
            EditorGUILayout.PropertyField(this.spActionsTransition);

            EditorGUILayout.PropertyField(this.spWaitToFinish);

            this.serializedObject.ApplyModifiedProperties();


        }

#endif
    }



}
