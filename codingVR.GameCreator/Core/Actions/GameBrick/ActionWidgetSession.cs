﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Variables;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionWidgetSession : IAction
	{
		// DATA: ----------------------------------------------------------------------------

		public TargetGameObject widgetGameObject = new TargetGameObject(TargetGameObject.Target.GameObject);
		private codingVR.Patterns.ISingleInstanceRunning<ActionWidgetSession> singleInstanceRunning = new codingVR.Patterns.ISingleInstanceRunning<ActionWidgetSession>();
		public bool waitToFinish = false;
		private bool actionsComplete = false;
		private bool forceStop = false;
		private DiContainer diContainer;
			   
		// CONSTRUCTOR: ----------------------------------------------------------------------------

		/* 
		 * 
		 * https://gitlab.com/monamipoto/potomaze/-/issues/590
		 * When the container for this object is a scriptable object; this HACK is used to inject dependency 
		 * 
		 */

		public void DIPInject(GameObject invoker, object injectable)
		{
			if (this.diContainer == null)
			{
				if (invoker != null)
				{
					var dipInject = invoker.GetComponent<codingVR.GameEngine.IDIPInject>();
					if (dipInject != null)
					{
						dipInject.DiContainer.Inject(injectable);
						this.diContainer = dipInject.DiContainer;
					}
				}
			}
		}

		[Inject]
		public void Constructor(DiContainer diContainer)
		{
			this.diContainer = diContainer;
		}

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			DIPInject(target, this);

			yield return singleInstanceRunning.Lock();
			{
				this.forceStop = false;
				
				if (widgetGameObject != null)
				{

					this.actionsComplete = false;

					var widget = widgetGameObject.gameObject.GetComponent<codingVR.Data.IGameSessionDataNode>();
					if (widget != null)
					{
						// Select data
						var sessionData = widget.SelectData();
						if (sessionData != null)
						{
							// load data 
							diContainer.Inject(sessionData);
							yield return sessionData.GetDataSpec();
							yield return sessionData.GetData(true);
						}

						// Start and execute game scene
						yield return widget.StartSession();

						// waiting completion
						yield return new WaitUntil(() => widget.Status == codingVR.GameEngine.GameSessionStatus.isCompleted || this.forceStop == true);

						// Stop game scene
						yield return widget.StopSession();

						// reset internal state
						forceStop = false;
						
						// network post
						var reply = widget.Reply;
						if (reply != null)
						{
							diContainer.Inject(reply);
							yield return reply.PutData();
						}
					}
				}
				else
				{
					Debug.LogWarningFormat("{0} : the game object {1} doesn't support IGameSessionDataNode !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, widgetGameObject.gameObject.name);
				}
			}
			singleInstanceRunning.Unlock();
			yield return 0;
		}

		private void OnCompleteActions()
		{
			this.actionsComplete = true;
		}

		public override void Stop()
		{
			this.forceStop = true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/GameBrick/Widget";
		private const string NODE_TITLE = "Widgetsession runs {0} {1};";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spWidget;
		private SerializedProperty spWaitToFinish;


		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			string actionsName = "";
			if (widgetGameObject != null) actionsName = widgetGameObject.ToString();

			return string.Format(
				NODE_TITLE,
				actionsName,
				(this.waitToFinish ? "and Wait" : "")
			);
		}

		protected override void OnEnableEditorChild()
		{
			this.spWidget = this.serializedObject.FindProperty("widgetGameObject");
			this.spWaitToFinish = this.serializedObject.FindProperty("waitToFinish");
		}

		protected override void OnDisableEditorChild()
		{
			this.spWidget = null;
			this.spWaitToFinish = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();
			
			EditorGUILayout.PropertyField(this.spWidget);
			EditorGUILayout.PropertyField(this.spWaitToFinish);
			
			this.serializedObject.ApplyModifiedProperties();
		}
#endif
	}
}