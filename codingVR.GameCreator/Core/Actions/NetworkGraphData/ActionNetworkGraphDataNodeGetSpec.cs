﻿namespace GameCreator.Core
{
    using System.Collections;
    using UnityEngine;
    using Zenject;

#if UNITY_EDITOR
    using UnityEditor;
#endif

    [AddComponentMenu("")]
    public class ActionNetworkGraphDataNodeGetSpec : GameGraphDataNodeTarget
    {
        private codingVR.Data.INetworkSession<codingVR.Data.GetGraphDataNodeSpec, codingVR.Data.SetGraphDataNodeResult> networkSession;

        [Inject]
        public void Contruct(codingVR.Data.INetworkSession<codingVR.Data.GetGraphDataNodeSpec, codingVR.Data.SetGraphDataNodeResult> networkSession)
        {
            this.networkSession = networkSession;
        }
        
        public override bool InstantExecute(GameObject target, IAction[] actions, int index)
        {
            return false;
        }

        public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
        {
            var data = SelectedData();
            if (data != null)
            {
                bool done = false;
                void DoneSet(codingVR.Data.GetGraphDataNodeSpec dataStruct)
                {
					if (dataStruct != null)
						dataStruct.Make(data);
					else
						Debug.LogErrorFormat("{0} : data is null !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
					done = true;
                }

                networkSession.GetSpec(codingVR.Data.Tag.ToIDString(data.Tag), DoneSet);
                yield return new WaitUntil(() => done == true);
            }
            else
            {
                Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            yield return 0;
        }

#if UNITY_EDITOR

        public static new string NAME = "CodingVR/NetworkGraphData/NodeGetSpecification";
        private const string NODE_TITLE = "On the network, we get the graph data node specification #{0}";


        // INSPECTOR METHODS: ---------------------------------------------------------------------

        public override string GetNodeTitle()
        {
            var questionTitle = base.TagTarget();

            return string.Format(
                NODE_TITLE,
                questionTitle
            );
        }

        protected override void OnEnableEditorChild()
        {
            base.EnableEditorChild();
        }

        protected override void OnDisableEditorChild()
        {
            base.DisableEditorChild();
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            base.InspectorGUI();

            this.serializedObject.ApplyModifiedProperties();
        }
#endif
    }
}