﻿namespace GameCreator.Core
{
    using System.Collections;
    using UnityEngine;
    using Zenject;

#if UNITY_EDITOR
    using UnityEditor;
#endif

    [AddComponentMenu("")]
    public class ActionNetworkGraphDataNodePostResult : GameGraphDataNodeTarget
    {
        private codingVR.Data.INetworkSession<codingVR.Data.GetGraphDataNodeSpec, codingVR.Data.SetGraphDataNodeResult> networkSession;

        [Inject]
        public void Contruct(codingVR.Data.INetworkSession<codingVR.Data.GetGraphDataNodeSpec, codingVR.Data.SetGraphDataNodeResult> networkSession)
        {
            this.networkSession = networkSession;
        }


        public override bool InstantExecute(GameObject target, IAction[] actions, int index)
        {
            return false;
        }

        public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
        {
            bool done = false;
            void DoneSet(codingVR.Data.SetGraphDataNodeResult dataStruct)
            {
                done = true;
            }

            var data = SelectedData();
            if (data != null)
            {
                var dataStuct = codingVR.Data.SetGraphDataNodeResult.Create(data);
                networkSession.Post(dataStuct, DoneSet);
                yield return new WaitUntil(() => done == true);
            }
            else
            {
                Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            yield return 0;
        }

#if UNITY_EDITOR

        public static new string NAME = "CodingVR/NetworkGraphData/NodePostResult";
        private const string NODE_TITLE = "On the network, at the question #{0}, we post the graph data node result {1}";

        // INSPECTOR METHODS: ---------------------------------------------------------------------

        public override string GetNodeTitle()
        {
            var tag = base.TagTarget();
            var answer = base.Answer();

            return string.Format(
                NODE_TITLE,
				tag,
				"'" + answer.Truncate(8) + "'"
			);
        }

        protected override void OnEnableEditorChild()
        {
            base.EnableEditorChild();
        }

        protected override void OnDisableEditorChild()
        {
            base.DisableEditorChild();
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            base.InspectorGUI();

            this.serializedObject.ApplyModifiedProperties();
        }
#endif
    }
}