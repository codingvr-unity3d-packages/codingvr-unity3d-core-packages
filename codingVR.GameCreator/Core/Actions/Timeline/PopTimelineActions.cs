﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Variables;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class PopTimelineActions : IAction 
	{
		public bool waitToFinish = false;

		private bool actionsComplete = false;
		private bool forceStop = false;

		// domaon Id
		public string domainId;

		// interface
		private codingVR.GameCreator.ITimelineManager timelineManager;

		// UNITY-LIFECYCLE: ----------------------------------------------------------------------------

		#region UNITY-LIFECYCLE

		[Inject]
		public void Constructor(codingVR.GameCreator.ITimelineManager timelineManager)
		{
			this.timelineManager = timelineManager;
		}

		#endregion
		
		// EXECUTABLE: ----------------------------------------------------------------------------

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			this.forceStop = false;

			Actions[] actionsToExecuteArray = timelineManager.Pop(this.domainId);

			if (actionsToExecuteArray != null)
			{
				this.actionsComplete = false;

				foreach (var actionsToExecute in actionsToExecuteArray)
				{
					actionsToExecute.actionsList.Execute(target, this.OnCompleteActions);

					if (this.waitToFinish)
					{
						WaitUntil wait = new WaitUntil(() =>
						{
							if (actionsToExecute == null)
							{
								return true;
							}
							if (this.forceStop)
							{
								actionsToExecute.actionsList.Stop();
								return true;
							}
							return this.actionsComplete;
						});

						yield return wait;

						this.actionsComplete = false;
					}
				}
			}
			else
			{
				Debug.LogErrorFormat("{0} : actions {1} not found !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, this.domainId);
			}

			yield return 0;
		}

		private void OnCompleteActions()
		{
			this.actionsComplete = true;
		}

		public override void Stop()
		{
			this.forceStop = true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

		#if UNITY_EDITOR

		public static new string NAME = "codingVR/Timeline/Pop Timeline Actions";
		private const string NODE_TITLE = "Pop timeline actions {0}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spWaitToFinish;
		private SerializedProperty spDomainId;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				domainId
			);
		}

		protected override void OnEnableEditorChild ()
		{
			this.spWaitToFinish = this.serializedObject.FindProperty("waitToFinish");
			this.spDomainId = this.serializedObject.FindProperty("domainId");
		}

		protected override void OnDisableEditorChild ()
		{
			this.spWaitToFinish = null;
			this.spDomainId = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spDomainId);
			EditorGUILayout.PropertyField(this.spWaitToFinish);
			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}