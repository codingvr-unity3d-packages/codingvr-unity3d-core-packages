﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Variables;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class PushTimelineActions : IAction 
	{
		public enum Source
		{
			Actions,
			Variable
		}

		public Source source = Source.Actions;
		public Actions actions;

		[VariableFilter(Variable.DataType.GameObject)]
		public VariableProperty variable = new VariableProperty(Variable.VarType.LocalVariable);

		// domaon Id
		public string domainId;
		
		// action priority
		public int actionPriority;

		// action weight
		public float actionWeight = 0.0f;

		// interface
		private codingVR.GameCreator.ITimelineManager timelineManager;

		// UNITY-LIFECYCLE: ----------------------------------------------------------------------------

		#region UNITY-LIFECYCLE

		[Inject]
		public void Constructor(codingVR.GameCreator.ITimelineManager timelineManager)
		{
			this.timelineManager = timelineManager;
		}

		#endregion

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			Actions actionsToExecute = null;

			switch (this.source)
			{
				case Source.Actions:
					actionsToExecute = this.actions;
					break;

				case Source.Variable:
					GameObject value = this.variable.Get(target) as GameObject;
					if (value != null) actionsToExecute = value.GetComponent<Actions>();
					break;
			}

			// push actions
			if (timelineManager.Push(domainId, /*actionId, */actionPriority, actionWeight, actionsToExecute) == false)
			{
				Debug.LogErrorFormat("{0} : push actions {1} has failed !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, this.domainId);
			}

			yield return 0;
		}

		private void OnCompleteActions()
		{
		}

		public override void Stop()
		{
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

		#if UNITY_EDITOR

		public static new string NAME = "codingVR/Timeline/Push Timeline Actions";
		private const string NODE_TITLE = "Push timeline actions {0}:{1}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spSource;
		private SerializedProperty spActions;
		private SerializedProperty spVariable;
		private SerializedProperty spDomainId;
		//private SerializedProperty spActionId;
		private SerializedProperty spActionPriority;
		private SerializedProperty spActionWeight;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			string actionsName = (this.source == Source.Actions
				? (this.actions == null ? "none" : this.actions.name)
				: this.variable.ToString()
			);

			return string.Format(
				NODE_TITLE,
				domainId,
				actionsName
			);
		}

		protected override void OnEnableEditorChild ()
		{
			this.spSource = this.serializedObject.FindProperty("source");
			this.spVariable = this.serializedObject.FindProperty("variable");
			this.spActions = this.serializedObject.FindProperty("actions");
			this.spDomainId = this.serializedObject.FindProperty("domainId");
			//this.spActionId = this.serializedObject.FindProperty("actionId");
			this.spActionPriority = this.serializedObject.FindProperty("actionPriority");
			this.spActionWeight = this.serializedObject.FindProperty("actionWeight");
		}

		protected override void OnDisableEditorChild ()
		{
			this.spSource = null;
			this.spVariable = null;
			this.spActions = null;
			this.spDomainId = null;
			//this.spActionId = null;
			this.spActionPriority = null;
			this.spActionWeight = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();
			
			EditorGUILayout.PropertyField(this.spSource);
			switch (this.spSource.enumValueIndex)
			{
				case (int)Source.Actions:
					EditorGUILayout.PropertyField(this.spActions);
					break;

				case (int)Source.Variable:
					EditorGUILayout.PropertyField(this.spVariable);
					break;
			}

			EditorGUILayout.PropertyField(this.spDomainId);
			//EditorGUILayout.PropertyField(this.spActionId);
			EditorGUILayout.PropertyField(this.spActionPriority);
			EditorGUILayout.PropertyField(this.spActionWeight);

			this.serializedObject.ApplyModifiedProperties();
		}
		#endif
	}
}