﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
    using GameCreator.Characters;
    using Hooks;
    
    [AddComponentMenu("")]
	public class ActionMAPToPlayer : IAction
	{
        public override bool InstantExecute(GameObject target, IAction[] actions, int index)
        {
            GameObject map = GameObject.Find("MonAmiPotoNPC");

            if(map != null)
            {
                CharacterController characterController = map.GetComponent<CharacterController>();

                PlayerCharacter playerCharacter = HookPlayer.Instance.Get<PlayerCharacter>();

                characterController.enabled = false;
                map.transform.position = new Vector3(playerCharacter.transform.position.x + 2.0f, playerCharacter.transform.position.y, playerCharacter.transform.position.z);
                characterController.enabled = true;
            }
           
            return true;
        }

		#if UNITY_EDITOR
        public static new string NAME = "Custom/ActionMAPToPlayer";
		#endif
	}
}

