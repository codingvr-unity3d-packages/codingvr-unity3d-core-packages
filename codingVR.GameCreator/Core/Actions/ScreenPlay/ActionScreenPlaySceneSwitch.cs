﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using UnityEngine.SceneManagement;
	using GameCreator.Variables;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
	using System;
#endif

	[AddComponentMenu("")]
	public class ActionScreenPlaySceneSwitch : IAction
	{
		public StringProperty newSceneName = new StringProperty();
		public string loadingSceneName = "Loading";
		private codingVR.GameEngine.IScreenPlay screenPlayController;
		private codingVR.GameEngine.IPlayerStateTheme playerStateTheme;
		public string screenPlayTag = codingVR.Data.Tags.Welcome.ToString();
		
		[Inject]
		public void Contruct(codingVR.GameEngine.IScreenPlay sceenPlayController, codingVR.GameEngine.IPlayerStateTheme playerState)
		{
			this.screenPlayController = sceenPlayController;
			this.playerStateTheme = playerState;
		}
		
		// EXECUTABLE: ----------------------------------------------------------------------------

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			var themeState = playerStateTheme.FindThemeState(newSceneName.ToString());

			string tag;
			if (string.IsNullOrEmpty(screenPlayTag) == true)
				tag = themeState.LastTag.ToString();
			else
				tag = screenPlayTag;

			yield return screenPlayController.SwitchingScreenPlay(new codingVR.GameEngine.ScreenPlayPropertiesStruct(newSceneName.ToString(), tag));
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/ScreenPlay/Scene/Switch";
		private const string NODE_TITLE = "Screen play scene controller switch to {0}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spNewSceneName;
		private SerializedProperty spScreenPlayTag;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				this.newSceneName
			);
		}

		protected override void OnEnableEditorChild()
		{
			this.spNewSceneName = this.serializedObject.FindProperty("newSceneName");
			this.spScreenPlayTag = this.serializedObject.FindProperty("screenPlayTag");
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spNewSceneName);
			EditorGUILayout.PropertyField(this.spScreenPlayTag);

			this.serializedObject.ApplyModifiedProperties();
		}

#endif
	}
}

