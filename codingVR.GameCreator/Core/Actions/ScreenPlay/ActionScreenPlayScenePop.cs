﻿namespace GameCreator.Core
{
	using GameCreator.Core.Hooks;
	using System.Collections;
	using UnityEngine;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
	using System;
#endif

	[AddComponentMenu("")]
	public class ActionScreenPlayScenePop : IAction
	{
		private codingVR.GameEngine.IScreenPlay screenPlayController;
		private codingVR.GameEngine.IGameRootManager gameManager;

		[Inject]
		public void Contruct(codingVR.GameEngine.IScreenPlay sceenPlayController, codingVR.GameEngine.IGameRootManager gameManager)
		{
			this.screenPlayController = sceenPlayController;
			this.gameManager = gameManager;
		}
		
		// EXECUTABLE: ----------------------------------------------------------------------------

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			GameObject player = HookPlayer.Instance.gameObject;

			gameManager.SaveGameObject("player", player);
			yield return screenPlayController.PopScreenPlay(true);
			gameManager.RestoreGameObject("player");

			yield return 0;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/ScreenPlay/Scene/Pop";
		private const string NODE_TITLE = "Screen play scene controller pop";

		// PROPERTIES: ----------------------------------------------------------------------------


		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE
			);
		}

		protected override void OnEnableEditorChild()
		{
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			this.serializedObject.ApplyModifiedProperties();
		}
#endif
	}
}

