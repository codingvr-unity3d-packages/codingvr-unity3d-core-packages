﻿namespace GameCreator.Core
{
	using GameCreator.Core.Hooks;
	using System.Collections;
	using UnityEngine;
	using GameCreator.Variables;
	using Zenject;
	

#if UNITY_EDITOR
	using UnityEditor;
	using System;
#endif

	[AddComponentMenu("")]
	public class ActionScreenPlaySceneNextTag : IAction
	{
		private codingVR.GameEngine.IScreenPlay screenPlayController;
		public string screenPlayNextTag = "";

		[Inject]
		public void Contruct(codingVR.GameEngine.IScreenPlay sceenPlayController)
		{
			this.screenPlayController = sceenPlayController;
		}

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			screenPlayController.SetupScreenPlayNextTag(screenPlayNextTag);
			return true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/ScreenPlay/Scene/NextTag";
		private const string NODE_TITLE = "Screen play scene controller next tag to {0}";

		// PROPERTIES: ----------------------------------------------------------------------------
		private SerializedProperty spScreenPlayNextTag;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				this.screenPlayNextTag
			);
		}

		protected override void OnEnableEditorChild()
		{
			this.spScreenPlayNextTag = this.serializedObject.FindProperty("screenPlayNextTag");
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spScreenPlayNextTag);

			this.serializedObject.ApplyModifiedProperties();
		}

#endif
	}
}

