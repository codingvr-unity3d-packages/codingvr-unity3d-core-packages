﻿namespace GameCreator.Core
{
    using System.Collections;
    using UnityEngine;
    using Zenject;

    [AddComponentMenu("")]
    public class ActionScreenPlayObjectSwitch : GameQuestionTarget
    {
        private DiContainer diContainer;
        private codingVR.GameEngine.IScreenPlay screenPlay;

        // LIFECYCLE: ----------------------------------------------------------------------------

        [Inject]
        public void Contruct(DiContainer diContainer, codingVR.GameEngine.IScreenPlay screenPlay)
        {
            this.diContainer = diContainer;
            this.screenPlay = screenPlay;
        }

        // EXECUTABLE: ----------------------------------------------------------------------------

        public override bool InstantExecute(GameObject target, IAction[] actions, int index)
        {
            return false;
        }

        public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
        {
			var data = SelectedData();
			if (data != null)
			{
				bool bReturn = false;
				void Done(bool bStatus)
				{
					bReturn = true;
				}

				yield return screenPlay.SwitchingObject(codingVR.Data.Tag.CreateTag(data.Tag), codingVR.Data.Tag.CreateTag(data.Answer), Done);
				yield return new WaitUntil(() => bReturn == true);

				Debug.AssertFormat(bReturn == true, "impossible to switch bewteen tags from {0} to {1} ", data.Tag, data.Answer);
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
        }

        // +--------------------------------------------------------------------------------------+
        // | EDITOR                                                                               |
        // +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

        public static new string NAME = "CodingVR/ScreenPlay/Object/Switch";
        private const string NODE_TITLE = "Screen play object switch from {0} to {1}";

        // INSPECTOR METHODS: ---------------------------------------------------------------------

        public override string GetNodeTitle()
        {
            string answer = "???";
            var data = SelectedData();
            if (data != null)
            {
                answer = data.Answer;
            }
            return string.Format(NODE_TITLE, base.TitleTarget(), answer);
        }

        protected override void OnEnableEditorChild()
        {
            base.EnableEditorChild();
        }

        protected override void OnDisableEditorChild()
        {
            base.DisableEditorChild();
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            base.InspectorGUI();

            this.serializedObject.ApplyModifiedProperties();
        }
#endif
    }
}

