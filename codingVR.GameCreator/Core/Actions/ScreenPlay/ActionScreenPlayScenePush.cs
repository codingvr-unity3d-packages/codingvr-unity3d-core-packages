﻿namespace GameCreator.Core
{
	using GameCreator.Core.Hooks;
	using System.Collections;
	using UnityEngine;
	using GameCreator.Variables;
	using Zenject;
	

#if UNITY_EDITOR
	using UnityEditor;
	using System;
#endif

	[AddComponentMenu("")]
	public class ActionScreenPlayScenePush : IAction
	{
		public StringProperty newSceneName = new StringProperty();
		public string loadingSceneName = "Loading";
		private codingVR.GameEngine.IScreenPlay screenPlayController;
		private codingVR.GameEngine.IPlayerStateTheme playerStateTheme;
		private codingVR.GameEngine.IGameRootManager gameManager;
		public string screenPlayTag = codingVR.Data.Tags.Welcome.ToString();

		[Inject]
		public void Contruct(codingVR.GameEngine.IScreenPlay sceenPlayController, codingVR.GameEngine.IPlayerStateTheme playerState, codingVR.GameEngine.IGameRootManager gameManager)
		{
			this.screenPlayController = sceenPlayController;
			this.playerStateTheme = playerState;
			this.gameManager = gameManager;
		}
		
		// EXECUTABLE: ----------------------------------------------------------------------------

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			var themeState = playerStateTheme.FindThemeState(newSceneName.ToString());

			string tag;
			if (string.IsNullOrEmpty(screenPlayTag) == true)
				tag = themeState.LastTag.ToString();
			else
				tag = screenPlayTag;
					   
			GameObject player = HookPlayer.Instance.gameObject;

			gameManager.SaveGameObject("player", player);
			yield return screenPlayController.PushScreenPlay(new codingVR.GameEngine.ScreenPlayPropertiesStruct(newSceneName.ToString(), tag), true);
			gameManager.RestoreGameObject("player");

			yield return 0;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/ScreenPlay/Scene/Push";
		private const string NODE_TITLE = "Screen play scene controller push to {0}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spNewSceneName;
		private SerializedProperty spScreenPlayTag;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				this.newSceneName
			);
		}

		protected override void OnEnableEditorChild()
		{
			this.spNewSceneName = this.serializedObject.FindProperty("newSceneName");
			this.spScreenPlayTag = this.serializedObject.FindProperty("screenPlayTag");
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spNewSceneName);
			EditorGUILayout.PropertyField(this.spScreenPlayTag);

			this.serializedObject.ApplyModifiedProperties();
		}

#endif
	}
}

