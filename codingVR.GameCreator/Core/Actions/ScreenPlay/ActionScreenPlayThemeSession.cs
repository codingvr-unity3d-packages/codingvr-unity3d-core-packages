﻿namespace GameCreator.Core
{
	using System;
	using System.Collections;
	using UnityEngine;
	using UnityEngine.SceneManagement;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionScreenPlayThemeSession : GameQuestionTarget
	{
		private DiContainer diContainer;
		private codingVR.Scene.ISceneLoader sceneLoader;
		public codingVR.GameEngine.IScreenPlay screenPlay;
		public codingVR.GameEngine.ScreenPlayPropertiesStruct screenPlayProperties = new codingVR.GameEngine.ScreenPlayPropertiesStruct("EmptyTheme", "Loading", codingVR.Data.Tags.Welcome.ToString());
		private bool forceStop = false;

		[Inject]
		public void Contruct(DiContainer diContainer, codingVR.Scene.ISceneLoader sceneLoader, codingVR.GameEngine.IScreenPlay screenPlay)
		{
			this.diContainer = diContainer;
			this.sceneLoader = sceneLoader;
			this.screenPlay = screenPlay;
		}

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			return false;
		}

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			this.forceStop = false;

			// Load scene
			var savedPlayer = GameCreator.Core.Hooks.HookPlayer.Instance;
			var savedCamera = GameCreator.Core.Hooks.HookCamera.Instance;

			bool loadingDone = false;

			void LoadingDone(string sceneName)
			{
				loadingDone = true;
			}

			sceneLoader.LoadScene(screenPlayProperties.currentScreenPlaySceneName, screenPlayProperties.loadingSceneName, LoadSceneMode.Additive, true, LoadingDone);
			yield return new WaitWhile(() => loadingDone == false);
			
			// we pass a frame to complete initialisation
			yield return null;

			// bind game scene
			var gameScene = codingVR.Patterns.Interface<codingVR.Data.IGameSessionDataLink>.Bind();
			// Start and execute game scene
			if (gameScene != null)
			{
				// push screen play
				yield return screenPlay.PushScreenPlay(screenPlayProperties, false);
				
				// we pass a frame to complete initialisation
				yield return null;

				// select data
				var sessionData = gameScene.SelectData(SelectedData());
				if (sessionData != null)
				{
					// load data 
					diContainer.Inject(sessionData);
					yield return sessionData.GetDataSpec();
					yield return sessionData.GetData(true);
				}
				// start session
				yield return gameScene.StartSession();

				// wait response
				yield return new WaitUntil(() => gameScene.Status == codingVR.GameEngine.GameSessionStatus.isCompleted || this.forceStop == true);

				// stop session
				yield return gameScene.StopSession();

				// pop screen play
				yield return screenPlay.PopScreenPlay(false);

				// reset internal state
				forceStop = false;

				// network post
				var reply = gameScene.Reply;
				if (reply != null)
				{
					diContainer.Inject(reply);
					yield return reply.PutData();
				}
			}

			// we unload scene
			sceneLoader.UnloadScene(screenPlayProperties.currentScreenPlaySceneName);

			// all right
#if UNITY_EDITOR
			Debug.Log(GetNodeTitle() + " Done !!!");
#endif

			// unbind game scene
			codingVR.Patterns.Interface<codingVR.Data.IGameSessionDataLink>.Unbind();

			GameCreator.Core.Hooks.HookPlayer.Instance = savedPlayer;
			GameCreator.Core.Hooks.HookCamera.Instance = savedCamera;

			yield return 0;
		}

		public override void Stop()
		{
			this.forceStop = true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/ScreenPlay/Scene/ThemeSession";
		private const string NODE_TITLE = "Screen play theme session running at {0} is wating for an answer";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spScreenPlayProperties;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			var questionTitle = base.TitleTarget();
			return string.Format(NODE_TITLE, questionTitle);
		}

		protected override void OnEnableEditorChild ()
		{
			base.EnableEditorChild();
			
			this.spScreenPlayProperties = this.serializedObject.FindProperty("screenPlayProperties");
		}

		protected override void OnDisableEditorChild ()
		{
			base.DisableEditorChild();
			
			this.spScreenPlayProperties = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spScreenPlayProperties, new GUIContent("Screen play properties"));

			base.InspectorGUI();

			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}
