﻿namespace GameCreator.Core
{
	using GameCreator.Variables;
	using UnityEngine;

	#if UNITY_EDITOR
	using UnityEditor;
	#endif

    [AddComponentMenu("")]
	public class ActionFindGameObject : IAction
	{
		public StringProperty gameobjectName = new StringProperty();
		public bool searchFromParent = false;
		public GameObjectProperty parent = new GameObjectProperty();

		[VariableFilter(Variable.DataType.GameObject)]
		public VariableProperty output = new VariableProperty();

        // EXECUTABLE: ----------------------------------------------------------------------------

        public override bool InstantExecute(GameObject target, IAction[] actions, int index)
        {
			if (searchFromParent) output.Set(parent.GetValue(target).transform.Find(gameobjectName.GetValue(target))?.gameObject);
			else output.Set(GameObject.Find(gameobjectName.GetValue(target)));

            return true;
        }

        // +--------------------------------------------------------------------------------------+
        // | EDITOR                                                                               |
        // +--------------------------------------------------------------------------------------+

        #if UNITY_EDITOR

        public static new string NAME = "CodingVR/Object/Find GameObject";
		private const string NODE_TITLE = "Find GameObject {0}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spGameObjectName;
		private SerializedProperty spSearchFromParent;
		private SerializedProperty spParent;
		private SerializedProperty spOutput;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(NODE_TITLE, this.gameobjectName?.value);
		}

		protected override void OnEnableEditorChild ()
		{
			this.spGameObjectName = this.serializedObject.FindProperty(nameof(this.gameobjectName));
			this.spSearchFromParent = this.serializedObject.FindProperty(nameof(this.searchFromParent));
			this.spParent = this.serializedObject.FindProperty(nameof(this.parent));
			this.spOutput = this.serializedObject.FindProperty(nameof(this.output));
		}

		protected override void OnDisableEditorChild ()
		{
			this.spGameObjectName = null;
			this.spSearchFromParent = null;
			this.spParent = null;
			this.spOutput = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spGameObjectName);

			EditorGUILayout.Space();

			EditorGUILayout.PropertyField(this.spSearchFromParent);

			if (this.searchFromParent)
            {
				EditorGUILayout.PropertyField(this.spParent);
            }

			EditorGUILayout.Space();

			EditorGUILayout.PropertyField(this.spOutput);

			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}
