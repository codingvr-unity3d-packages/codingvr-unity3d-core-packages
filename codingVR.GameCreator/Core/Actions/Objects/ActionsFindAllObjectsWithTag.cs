﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Variables;

	[AddComponentMenu("")]
	public class CustomFindAllObjectsWithTag : IAction
	{
        [TagSelector] public string tagName = "";
		[Space]
        public HelperGetListVariable listVariables = new HelperGetListVariable();

        public override bool InstantExecute(GameObject target, IAction[] actions, int index)
        {
            ListVariables list = this.listVariables.GetListVariables(target);
            if (list == null) return true;

            GameObject[] goList = GameObject.FindGameObjectsWithTag(this.tagName);

			for (int i = 0; i < goList.Length; ++i) {
                list.Push(goList[i], this.listVariables.select, this.listVariables.index);
			}

            return true;
        }

        #if UNITY_EDITOR

        public static new string NAME = "CodingVR/Object/CustomFindAllObjectsWithTag";
		private const string NODE_TITLE = "Find objects with tag {0}, Add to {1}";

        public override string GetNodeTitle()
        {
            return string.Format(NODE_TITLE, this.tagName, this.listVariables);
        }

		#endif
    }
}
