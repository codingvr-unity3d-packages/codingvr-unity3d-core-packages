﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Core;
	using GameCreator.Variables;
	using UnityEngine.SceneManagement;

#if UNITY_EDITOR
	using UnityEditor;
	#endif

	[AddComponentMenu("")]
	public class ActionSetActiveByName : IAction 
	{
		public string target;
		public bool active = true;

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			for (var i=0;i<SceneManager.sceneCount;++i)
			{
				var scene = SceneManager.GetSceneAt(i);
				var objs = scene.GetAllGameObjects(active); // we use the requirement (it true; then we need to find also the hidden objects; if false we need to find only the visible objects)
				foreach (var it in objs)
				{
					if (it.name == this.target)
					{
						GameObject targetValue = it;
						if (targetValue != null)
						{
							targetValue.SetActive(this.active);
						}
					}
				}
			}
			return true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

		#if UNITY_EDITOR

		public static new string NAME = "CodingVR/Object/Set Active By Name";
		private const string NODE_TITLE = "Set {0} object {1}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spTarget;
		private SerializedProperty spActive;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE, 
				(this.active ? "active" : "inactive"),
				this.target
			);
		}

		protected override void OnEnableEditorChild ()
		{
			this.spTarget = this.serializedObject.FindProperty("target");
			this.spActive = this.serializedObject.FindProperty("active");
		}

		protected override void OnDisableEditorChild ()
		{
			this.spTarget = null;
			this.spActive = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spTarget);
			EditorGUILayout.PropertyField(this.spActive);

			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}