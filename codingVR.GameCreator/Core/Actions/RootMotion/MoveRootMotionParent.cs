﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRootMotionParent : MonoBehaviour
{
	public bool Running = false;

	private Animator _animator;

	void OnAnimatorMove()
	{
		Animator animator = GetAnimator();

		if(animator && Running == true) {
			transform.parent.rotation = animator.rootRotation;
			transform.parent.position += animator.deltaPosition;
		}
	}

	public void Run()
	{
		Running = true;
		Animator animator = GetAnimator();
		if(animator) {
			animator.applyRootMotion = true;
		}
	}

	public void Stop()
	{
		Running = false;
		Animator animator = GetAnimator();
		if(animator) {
			animator.applyRootMotion = false;
		}
	}

	private Animator GetAnimator()
	{
		if(_animator == null) {
			_animator = GetComponent<Animator>();
		}

		return _animator;
	}
}
