﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Characters;

	[AddComponentMenu("")]
	public class ActionRootMotionMovement : IAction
	{
		public TargetCharacter target = new TargetCharacter();
		public bool Enable = true;

        public override bool InstantExecute(GameObject target, IAction[] actions, int index)
        {
			Character charTarget = this.target.GetCharacter(target);
			if(charTarget != null && charTarget.transform.childCount > 0)
			{
				Transform firstChild = charTarget.transform.GetChild(0);
				MoveRootMotionParent mrmp = firstChild.GetComponent<MoveRootMotionParent>();
				if(Enable == true) {
					if(mrmp == null) { mrmp = firstChild.gameObject.AddComponent<MoveRootMotionParent>(); }
					mrmp.Run();
				} else {
					if(mrmp != null) { mrmp.Stop(); }
				}
			}
			
			return true;
        }

		#if UNITY_EDITOR
        public static new string NAME = "Character/Root Motion Movement";
		private const string NODE_TITLE = "{0} Root Motion Movement";

		public override string GetNodeTitle()
		{
			string enabledText = (this.Enable == true) ? "Enable" : "Disable";
			return string.Format(NODE_TITLE, enabledText);
		}
		#endif
	}
}
