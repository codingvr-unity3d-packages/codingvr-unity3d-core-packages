﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCreator.GameObject
{

    public class GameObjectAttachment : MonoBehaviour
    {

        public Transform gameObjectFromAttach;

        private Vector3 _positionOffset;
        private Quaternion _rotationOffset;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (gameObjectFromAttach == null)
                return;

            var targetPos = gameObjectFromAttach.position - _positionOffset;
            var targetRot = gameObjectFromAttach.localRotation * _rotationOffset;

            transform.position = RotatePointAroundPivot(targetPos, gameObjectFromAttach.position, targetRot);
            //transform.localRotation = targetRot;
        }

        public void SetAttachment(Transform gameObjectAttach)
        {
            //Offset vector
            _positionOffset = gameObjectAttach.position - transform.position;
            //Offset rotation
            _rotationOffset = Quaternion.Inverse(gameObjectAttach.localRotation * transform.localRotation);
            //Our gameObjectToAttach
            gameObjectFromAttach = gameObjectAttach;
        }

        public Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Quaternion rotation)
        {
            //Get a direction from the pivot to the point
            Vector3 dir = point - pivot;
            //Rotate vector around pivot
            dir = rotation * dir;
            //Calc the rotated vector
            point = dir + pivot;
            //Return calculated vector
            return point;
        }
    }
}