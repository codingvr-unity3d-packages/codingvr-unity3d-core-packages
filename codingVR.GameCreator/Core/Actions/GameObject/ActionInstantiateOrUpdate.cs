﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Core.Hooks;
	using GameCreator.Variables;
	using UnityEngine.AddressableAssets;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionInstantiateOrUpdate : IAction
	{
		public string nameInstance;
		public bool addressable = false;
		public AssetReference assetReference = new AssetReference();
		public TargetGameObject prefab = new TargetGameObject();
		public TargetPosition initLocation = new TargetPosition();
		public TargetGameObject parent = new TargetGameObject();

		[Space]
		public VariableProperty storeInstance = new VariableProperty();

		// DATA: ----------------------------------------------------------------------------

		private DiContainer diContainer;
		codingVR.Scene.IResourcesLoader resourcesLoader;

		// CONSTRUCTOR: ----------------------------------------------------------------------------

		[Inject]
		public void Contruct(DiContainer diContainer, codingVR.Scene.IResourcesLoader resourcesLoader)
		{
			this.diContainer = diContainer;
			this.resourcesLoader = resourcesLoader;
		}

		/* 
		 * 
		 * https://gitlab.com/monamipoto/potomaze/-/issues/590
		 * When the container for this object is a scriptable object; this HACK is used to inject dependency 
		 * 
		 */

		public void DIPInject(GameObject invoker, object injectable)
		{
			if (this.diContainer == null)
			{
				if (invoker != null)
				{
					var dipInject = invoker.GetComponent<codingVR.GameEngine.IDIPInject>();
					if (dipInject != null)
						dipInject.DiContainer.Inject(injectable);
				}
			}
		}

		// dip inject all mono from gameobject
		void DIPInjectGameObject(GameObject _instance)
		{
			var cp = _instance.GetComponentsInChildren<MonoBehaviour>();
			foreach (var it in cp)
			{
				this.diContainer.Inject(it);
			}
		}

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			// https://gitlab.com/monamipoto/potomaze/-/issues/590
			DIPInject(target, this);

			if (addressable == false)
			{
				GameObject prefabValue = this.prefab.GetGameObject(target);
				if (prefabValue != null)
				{
					Vector3 position = this.initLocation.GetPosition(target, Space.Self);
					Quaternion rotation = this.initLocation.GetRotation(target);

					GameObject instance = null;
					GameObject instanceParent = this.parent.GetGameObject(target);


					// instantiate or update
					if (instanceParent != null)
					{
						var instanceTransform = instanceParent.transform.Find(this.nameInstance);
						if (instanceTransform == null)
						{
							instance = Instantiate(prefabValue, position, rotation, instanceParent.transform);
							instance.name = nameInstance;
							DIPInjectGameObject(instance);
						}
						else
						{
							instance = instanceTransform.gameObject;
							instanceTransform.position = position;
							instanceTransform.rotation = rotation;
						}
					}
					else
					{
						instance = Instantiate(prefabValue, position, rotation);
						instance.name = nameInstance;
						DIPInjectGameObject(instance);
					}
					if (instance != null) this.storeInstance.Set(instance, target);
				}
			}

			return addressable == false;
		}

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			Vector3 position = this.initLocation.GetPosition(target, Space.Self);
			Quaternion rotation = this.initLocation.GetRotation(target);

			GameObject instance = null;
			GameObject instanceParent = this.parent.GetGameObject(target);

			// game object instantiated
			void Done(GameObject go)
			{
				go.name = nameInstance;
			}

			// instantiate or update
			if (instanceParent != null)
			{
				var instanceTransform = instanceParent.transform.Find(this.nameInstance);
				if (instanceTransform == null)
				{
					yield return resourcesLoader.InstantiateGameObject(diContainer, this.assetReference, position, rotation, instanceParent.transform, Done);
					instance = resourcesLoader.GetResource(this.assetReference) as GameObject;
				}
				else
				{
					instance = instanceTransform.gameObject;
					instanceTransform.position = position;
					instanceTransform.rotation = rotation;
				}
			}
			else
			{
				yield return resourcesLoader.InstantiateGameObject(diContainer, this.assetReference, position, rotation, instanceParent.transform, Done);
				instance = resourcesLoader.GetResource(this.assetReference) as GameObject;
			}
			if (instance != null) this.storeInstance.Set(instance, target);
			yield return 0;
		}


		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/GameObject/InstantiateOrUpdate";
		private const string NODE_TITLE = "Instantiate or update {0}";

		private static readonly GUIContent GC_STORE = new GUIContent("Store (optional)");

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spNameInstance;
		private SerializedProperty spAddressable;
		private SerializedProperty spAssetReference;
		private SerializedProperty spPrefab;
		private SerializedProperty spInitLocation;
		private SerializedProperty spParent;
		private SerializedProperty spStore;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(NODE_TITLE, this.nameInstance);
		}

		protected override void OnEnableEditorChild()
		{
			this.spNameInstance = this.serializedObject.FindProperty("nameInstance");
			this.spAddressable = this.serializedObject.FindProperty("addressable");
			this.spAssetReference = this.serializedObject.FindProperty("assetReference");
			this.spPrefab = this.serializedObject.FindProperty("prefab");
			this.spInitLocation = this.serializedObject.FindProperty("initLocation");
			this.spParent = this.serializedObject.FindProperty("parent");
			this.spStore = this.serializedObject.FindProperty("storeInstance");
		}

		protected override void OnDisableEditorChild()
		{
			this.spNameInstance = null;
			this.spAddressable = null;
			this.spParent = null;
			this.spAssetReference = null;
			this.spNameInstance = null;
			this.spPrefab = null;
			this.spInitLocation = null;
			this.spStore = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spNameInstance);
			EditorGUILayout.PropertyField(this.spAddressable);

			EditorGUILayout.Space();
			if (addressable)
				EditorGUILayout.PropertyField(this.spAssetReference);
			else
				EditorGUILayout.PropertyField(this.spPrefab);
			EditorGUILayout.PropertyField(this.spInitLocation);
			EditorGUILayout.PropertyField(this.spParent);

			EditorGUILayout.Space();
			EditorGUILayout.PropertyField(this.spStore, GC_STORE);

			this.serializedObject.ApplyModifiedProperties();
		}

#endif
	}
}