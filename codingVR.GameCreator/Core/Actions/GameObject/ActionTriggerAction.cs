﻿
namespace GameCreator.GameObject
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using GameCreator.Core;

	#if UNITY_EDITOR
	using UnityEditor;
	#endif

	[AddComponentMenu("")]
	public class ActionTriggerAction : IAction
	{
		public string tagAction;

        public override bool InstantExecute(GameObject target, IAction[] actions, int index)
        {
			var goAction = GameObject.FindGameObjectWithTag(tagAction);
			if (goAction != null)
			{
				var trigger = goAction.GetComponent<Trigger>();
				if (null != trigger)
				{
					trigger.Execute();
				}
				else
				{
					Debug.LogErrorFormat("{0} : the game object tagget has no trigger !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
				}
			}
			else
			{
				Debug.LogErrorFormat("{0} : the game object tagged no found !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}

			return true;
        }

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

		#if UNITY_EDITOR
		public static new string NAME = "Custom/ActionTriggerAction";
		private const string NODE_TITLE = "{0} {1}";
		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spAction;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				"Trigger",
				this.tagAction
			);
		}

		protected override void OnEnableEditorChild()
		{
			this.spAction = this.serializedObject.FindProperty("tagAction");
		}

		protected override void OnDisableEditorChild()
		{
			return;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spAction);

			this.serializedObject.ApplyModifiedProperties();
		}

#endif
	}
}
