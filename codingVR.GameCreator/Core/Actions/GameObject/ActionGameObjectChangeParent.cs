﻿namespace GameCreator.GameObject
{

	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using GameCreator.Core;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionGameObjectChangeParent : IAction
	{
		public enum AttachType
		{
			Attach,
			Detach
		}

		public TargetGameObject targetGameObject = new TargetGameObject();
		public TargetGameObject targetGameObjectParent = new TargetGameObject();
		public bool worldPositionStays = false;

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			if (targetGameObject == null)
			{
				return true;
			}

			if (targetGameObjectParent == null)
			{
				return true;
			}

			targetGameObject.GetGameObject(this.gameObject).transform.SetParent(targetGameObjectParent.GetGameObject(this.gameObject).transform, worldPositionStays);

			return true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/GameObject/GameObject change parent";
		private const string NODE_TITLE = "{0} {1} {2}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spTargetGameObject;
		private SerializedProperty spTargetGameObjectParent;
		private SerializedProperty spWorldPositionStays;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				this.targetGameObject,
				"parented to", //ObjectNames.NicifyVariableName(this.actionType.ToString()),
				this.targetGameObjectParent
			);
		}

		protected override void OnEnableEditorChild()
		{
			this.spTargetGameObject = this.serializedObject.FindProperty("targetGameObject");
			this.spTargetGameObjectParent = this.serializedObject.FindProperty("targetGameObjectParent");
			this.spWorldPositionStays = this.serializedObject.FindProperty("worldPositionStays");
		}

		protected override void OnDisableEditorChild()
		{
			this.spTargetGameObject = null;
			this.spTargetGameObjectParent = null;
			this.spWorldPositionStays = null;
			return;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();
			EditorGUILayout.PropertyField(this.spTargetGameObject);
			EditorGUILayout.Space();
			EditorGUILayout.PropertyField(this.spWorldPositionStays);
			EditorGUILayout.PropertyField(this.spTargetGameObjectParent);
			this.serializedObject.ApplyModifiedProperties();
		}
#endif
	}

}