﻿namespace GameCreator.GameObject
{

	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using GameCreator.Core;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionGameObjectAttachment : IAction
	{
		public enum AttachType
		{
			Attach,
			Detach
		}

		public TargetGameObject targetGameObjectToAttach = new TargetGameObject();
		public TargetGameObject targetGameObjectFromAttach = new TargetGameObject();
		public AttachType attachType = AttachType.Attach;

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			//targetGameObjectToAttach.GetType.
			if (targetGameObjectToAttach == null) return true;

			if (attachType == AttachType.Attach)
			{
				if (targetGameObjectFromAttach == null)
				{
					return true;
				}
				if (targetGameObjectToAttach.GetGameObject(this.gameObject).GetComponent<GameObjectAttachment>() != null)
				{
					Destroy(targetGameObjectToAttach.GetGameObject(this.gameObject).GetComponent<GameObjectAttachment>());
				}

				GameObjectAttachment gameObjectAttachment = targetGameObjectToAttach.GetGameObject(this.gameObject).AddComponent<GameObjectAttachment>();
				gameObjectAttachment.SetAttachment(targetGameObjectFromAttach.GetGameObject(this.gameObject).transform);
			}
			else if (attachType == AttachType.Detach)
			{
				if (targetGameObjectToAttach.GetGameObject(this.gameObject).GetComponent<GameObjectAttachment>() != null)
				{
					Destroy(targetGameObjectToAttach.GetGameObject(this.gameObject).GetComponent<GameObjectAttachment>());
				}
			}
			return true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/GameObject/GameObject attach";
		private const string NODE_TITLE = "{0} {1} {2}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spTargetGameObjectToAttach;
		private SerializedProperty spTargetGameObjectFromAttach;

		private SerializedProperty spAttachType;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				this.targetGameObjectToAttach,
				"attach to", //ObjectNames.NicifyVariableName(this.actionType.ToString()),
				(this.attachType == AttachType.Attach ? this.targetGameObjectFromAttach.ToString() : "")
			);
		}

		protected override void OnEnableEditorChild()
		{
			this.spTargetGameObjectToAttach = this.serializedObject.FindProperty("targetGameObjectToAttach");
			this.spTargetGameObjectFromAttach = this.serializedObject.FindProperty("targetGameObjectFromAttach");
			this.spAttachType = this.serializedObject.FindProperty("attachType");
		}

		protected override void OnDisableEditorChild()
		{
			return;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();
			EditorGUILayout.PropertyField(this.spTargetGameObjectToAttach);
			EditorGUILayout.PropertyField(this.spAttachType);
			if ((AttachType)this.spAttachType.intValue == AttachType.Attach)
			{
				EditorGUILayout.Space();
				EditorGUILayout.PropertyField(this.spTargetGameObjectFromAttach);
			}
			this.serializedObject.ApplyModifiedProperties();
		}
#endif
	}

}