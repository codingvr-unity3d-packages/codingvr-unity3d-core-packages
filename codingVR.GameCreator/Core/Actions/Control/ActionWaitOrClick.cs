﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionWaitOrClick : IAction
	{
		public float waitTime = 0.0f;
		public bool waitTimer = true;
		public bool waitClick = false;

		private bool forceStop = false;

		private codingVR.Core.IEventManager eventManager;

		[Inject]
        public void Contruct(codingVR.Core.IEventManager eventManager)
        {
            this.eventManager = eventManager;
        }

		/* 
		 * 
		 * https://gitlab.com/monamipoto/potomaze/-/issues/590
		 * When the container for this object is a scriptable object; this HACK is used to inject dependency 
		 * 
		 */

		public void DIPInject(GameObject invoker)
		{
			if (this.eventManager == null)
			{
				if (invoker != null)
				{
					var dipInject = invoker.GetComponent<codingVR.GameEngine.IDIPInject>();
					if (dipInject != null)
						dipInject.DiContainer.Inject(this);
				}
			}
		}

		private void OnClickMouse(codingVR.Data.Event_OnClickMouse ev)
		{
			Stop();
		}

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			this.forceStop = false;

			if (waitClick)
			{
				if (eventManager != null)
					eventManager.AddListener<codingVR.Data.Event_OnClickMouse>(OnClickMouse);
			}

			WaitUntil waitUntil;

			if (waitTimer)
			{
				float stopTime = Time.time + this.waitTime;
				waitUntil = new WaitUntil(() => Time.time > stopTime || this.forceStop);
			}

			else
			{
				waitUntil = new WaitUntil(() => this.forceStop);
			}

			yield return waitUntil;

			if (waitClick)
			{
				if (eventManager != null)
					eventManager.RemoveListener<codingVR.Data.Event_OnClickMouse>(OnClickMouse);
			}

			yield return 0;
		}

		public override void Stop()
		{
			this.forceStop = true;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/Control/ActionWaitOrClick";
		private const string NODE_TITLE = "Wait {0} second{1}";

		private static readonly GUIContent GUICONTENT_WAITTIME = new GUIContent("Time to wait (s)");

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spWaitTime;
		private SerializedProperty spWaitTimer;
		private SerializedProperty spWaitClick;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(NODE_TITLE, this.waitTime, (this.waitTime == 1f ? "" : "s"));
		}

		protected override void OnEnableEditorChild()
		{
			this.spWaitTime = this.serializedObject.FindProperty("waitTime");
			this.spWaitTimer = this.serializedObject.FindProperty("waitTimer");
			this.spWaitClick = this.serializedObject.FindProperty("waitClick");
		}

		protected override void OnDisableEditorChild()
		{
			this.spWaitTime = null;
			this.spWaitTimer = null;
			this.spWaitClick = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();
			
			EditorGUILayout.PropertyField(this.spWaitTimer);
			if (this.spWaitTimer.boolValue)
			{
				EditorGUILayout.PropertyField(this.spWaitTime, GUICONTENT_WAITTIME);
			}

			EditorGUILayout.PropertyField(this.spWaitClick);

			this.serializedObject.ApplyModifiedProperties();
		}

#endif
	}
}

