﻿namespace GameCreator.Characters
{
	using System.Collections;
	using UnityEngine;
	using GameCreator.Core;
	using GameCreator.Core.Hooks;
    using Zenject;
#if UNITY_EDITOR
    using UnityEditor;
#endif

    [AddComponentMenu("")]
	public class ActionPlayerMoveOnPath : IAction 
	{
        //public float fromTime = 0.0f;
        //public float toTime = 1.0f;
        private DiContainer diContainer;
        private bool forceStop = false;

        [Inject]
        public void Contruct(DiContainer diContainer)
        {
            this.diContainer = diContainer;
        }

        // EXECUTABLE: ----------------------------------------------------------------------------

        public override bool InstantExecute(GameObject target, IAction[] actions, int index)
        {
            if (HookPlayer.Instance != null)
            {
            }

            return false;
        }


        public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
        {
			this.forceStop = false;

			if (HookPlayer.Instance != null)
            {
                GameCreator.Characters.ILocomotionSystem.TargetRotation cRotation = null;
                PlayerCharacterCodingVR playerCharacterCodingVR = HookPlayer.Instance.Get<PlayerCharacterCodingVR>();
				var playerCharacter = HookPlayer.Instance.Get<GameCreator.Characters.PlayerCharacter>();
				codingVR.GameEngine.ICharacterTravel playerStates = playerCharacter.GetComponentInChildren<codingVR.GameEngine.ICharacterTravel>();
				this.forceStop = false;
                Debug.AssertFormat(playerStates != null && playerCharacterCodingVR != null, "{0} : component validate has failed !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
                bool reached = false;
                Vector3 velocity;
				int findNode = -1;
                var moving = playerStates.MoveOnPathBegin(false);
                while (reached == false && !forceStop)
                {
                    // we execute map travel
                    if (playerCharacterCodingVR.CurrentSpeed > 0.0f)
                    {
                        (reached, velocity, findNode) = moving.UpdateIteration(findNode);
                        if (reached == false)
                        {
                            playerCharacterCodingVR.characterLocomotion.SetDirectionalDirection(velocity, cRotation);
                        }
                    }
                    else
                    {
                        playerCharacterCodingVR.characterLocomotion.SetDirectionalDirection(Vector3.zero, cRotation);
                    }
                    // all right
                    yield return null;
                }

                // stop travel
                playerCharacterCodingVR.characterLocomotion.Stop(cRotation, CharacterArrivedCallback);
            }

            yield return 0;
        }

        public override void Stop()
        {
            this.forceStop = true;
            PlayerCharacterCodingVR playerCharacter = HookPlayer.Instance.Get<PlayerCharacterCodingVR>();
            if (playerCharacter != null)
            {
                playerCharacter.characterLocomotion.Stop();
            }
        }

        public void CharacterArrivedCallback()
        {
        }

        // +--------------------------------------------------------------------------------------+
        // | EDITOR                                                                               |
        // +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

        public static new string NAME = "CodingVR/Quests/Character/Player Move On Path";
		private const string NODE_TITLE = "Player Move On The Selected Path {0}";

		// PROPERTIES: ----------------------------------------------------------------------------

		//private SerializedProperty spFromTime;
        //private SerializedProperty spToTime;

        // INSPECTOR METHODS: ---------------------------------------------------------------------

        public override string GetNodeTitle()
		{
            //string value = string.Format("from time {0} to time {1} ", fromTime, toTime);
            string value = string.Format("");
            return string.Format(
                NODE_TITLE,
                value
            );
		}

		protected override void OnEnableEditorChild ()
		{
			//this.spFromTime = this.serializedObject.FindProperty("fromTime");
            //this.spToTime = this.serializedObject.FindProperty("toTime");
        }

		protected override void OnDisableEditorChild ()
		{
			//this.spFromTime = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

            EditorGUILayout.Space();
            //EditorGUILayout.PropertyField(this.spFromTime);
            //EditorGUILayout.PropertyField(this.spToTime);

            this.serializedObject.ApplyModifiedProperties();
        }

#endif
    }
}