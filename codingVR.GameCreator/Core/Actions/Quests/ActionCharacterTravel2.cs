﻿namespace GameCreator.Characters
{
	using System;
	using System.Collections;
	using UnityEngine;
	using UnityEngine.SceneManagement;
	using Zenject;
	using GameCreator.Core;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionCharacterTravel2 : GameQuestionTarget
	{
		// character
		public CVRTargetCharacter characterTarget = new CVRTargetCharacter(CVRTargetCharacter.Target.Player);

		// cancellation
		public bool cancelable = false;
		public float cancelDelay = 2.0f;

		// follow path in reverse mode
		public bool reversePath = false;
		
		// stop criteria
		[Range(0.0f, 5.0f)]
		[Tooltip("Threshold distance from the target that is considered as reached")]
		public float stopThreshold = 0.005f;

		// force stop
		bool forceStop = false;
			   		 
		// charatcter moving in progress
		codingVR.GameEngine.ICharacterMoving characterMoving = null;
		bool wasControllable = false;
		Character character = null;

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			return false;
		}

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			var data = SelectedData();
			if (data != null)
			{
				Character charTarget = this.characterTarget.GetCharacter(target);
				if (charTarget == null)
				{
					// by default player is selected
					characterTarget = new CVRTargetCharacter(CVRTargetCharacter.Target.Player);
					charTarget = this.characterTarget.GetCharacter(target);
				}

				// assign character
				character = charTarget;

				// initial state
				wasControllable = charTarget.characterLocomotion.isControllable;
				charTarget.characterLocomotion.SetIsControllable(false);

				// we execute map travel
				codingVR.GameEngine.ICharacterTravel charTargetTravel = charTarget.gameObject.GetComponent<codingVR.GameEngine.ICharacterTravel>();
				Debug.AssertFormat(charTargetTravel != null, "{0} : CharacterStates for GameObject {1} required", System.Reflection.MethodBase.GetCurrentMethod().Name, name);
				codingVR.Data.TagPathPair tagPair = codingVR.Data.TagPathPair.MakePair(SelectedData().Tag, SelectedData().Answer);

				charTargetTravel.SelectPath(tagPair);
				characterMoving = charTargetTravel.MoveOnPathBegin(reversePath);
				yield return characterMoving.ExecuteCompleteIteration(cancelable, charTarget, stopThreshold, cancelDelay, target);

				// stop travel
				ILocomotionSystem.TargetRotation cRotation = null;
				charTarget.characterLocomotion.Stop(cRotation, CharacterArrivedCallback);

				// restore character states
				charTarget.characterLocomotion.SetIsControllable(wasControllable);

				// detach hexUnitMoveOnPath
				characterMoving = null;

				// all right
#if UNITY_EDITOR
				Debug.Log(GetNodeTitle() + " Done !!!");
#endif
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}

			yield return 0;
		}

		public override void Stop()
		{
			if (characterMoving != null)
				characterMoving.ForceStop = true;

			if (this.character == null) return;
			
			this.character.characterLocomotion.SetIsControllable(this.wasControllable);
			this.character.characterLocomotion.Stop();
		}

		public void CharacterArrivedCallback()
		{
		}


		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/Quests/Character/Travel character v2";
		private const string NODE_TITLE = "Character {0} travels from {1} to {2}";

		private static readonly GUIContent GC_CANCEL = new GUIContent("Cancelable");

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spCharacter;
		private SerializedProperty spCancelable;
		private SerializedProperty spCancelDelay;
		private SerializedProperty spReversePath;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(NODE_TITLE, this.characterTarget, base.TitleTarget(), base.SelectedData()?.Answer);
		}

		protected override void OnEnableEditorChild()
		{
			this.spCharacter = this.serializedObject.FindProperty("characterTarget");

			base.EnableEditorChild();

			this.spCancelable = this.serializedObject.FindProperty("cancelable");
			this.spCancelDelay = this.serializedObject.FindProperty("cancelDelay");

			this.spReversePath = this.serializedObject.FindProperty("reversePath");
		}

		protected override void OnDisableEditorChild()
		{
			this.spCharacter = null;

			base.DisableEditorChild();

			this.spCancelable = null;
			this.spCancelDelay = null;

			this.spReversePath = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spCharacter);

			EditorGUILayout.PropertyField(this.spReversePath);

			base.InspectorGUI();

			EditorGUILayout.Space();
			Rect rect = GUILayoutUtility.GetRect(GUIContent.none, EditorStyles.textField);
			Rect rectLabel = new Rect(
				rect.x,
				rect.y,
				EditorGUIUtility.labelWidth,
				rect.height
			);
			Rect rectCanceable = new Rect(
				rectLabel.xMax,
				rectLabel.y,
				20f,
				rectLabel.height
			);
			Rect rectDelay = new Rect(
				rectCanceable.x + rectCanceable.width,
				rectCanceable.y,
				rect.width - (rectLabel.width + rectCanceable.width),
				rectCanceable.height
			);


			EditorGUI.LabelField(rectLabel, GC_CANCEL);
			EditorGUI.PropertyField(rectCanceable, this.spCancelable, GUIContent.none);
			EditorGUI.BeginDisabledGroup(!this.spCancelable.boolValue);
			EditorGUI.PropertyField(rectDelay, this.spCancelDelay, GUIContent.none);
			EditorGUI.EndDisabledGroup();

			this.serializedObject.ApplyModifiedProperties();
		}

#endif
	}
}
