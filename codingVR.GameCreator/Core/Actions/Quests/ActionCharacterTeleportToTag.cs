﻿namespace GameCreator.Core
{
	using System;
	using System.Collections;
	using UnityEngine;
	using UnityEngine.SceneManagement;
	using Zenject;
	using GameCreator.Characters;
	using UnityEngine.Serialization;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionCharacterTeleportToTag : IAction
	{
		[FormerlySerializedAs("toTag")]
		public String toTagPath;
		private DiContainer diContainer;
		private bool forceStop = false;

		public CVRTargetCharacter characterTarget = new CVRTargetCharacter(CVRTargetCharacter.Target.Player);

		[Inject]
		public void Contruct(DiContainer diContainer)
		{
			this.diContainer = diContainer;
		}

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			return false;
		}

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			this.forceStop = false;

			Character charTarget = this.characterTarget.GetCharacter(target);
			if (charTarget == null)
			{
				// by default player is selected
				characterTarget = new CVRTargetCharacter(CVRTargetCharacter.Target.Player);
				charTarget = this.characterTarget.GetCharacter(target);
			}

			codingVR.GameEngine.ICharacterTravel characterTravel = charTarget.GetComponentInChildren<codingVR.GameEngine.ICharacterTravel>();
			characterTravel.Teleport(codingVR.Data.TagPath.Parse( toTagPath ));

			// all right
#if UNITY_EDITOR
			Debug.Log(GetNodeTitle() + " Done !!!");
#endif

			yield return 0;
		}

		public override void Stop()
		{
			forceStop = true;
			return;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/Quests/Character/Teleport character to tag";
		private const string NODE_TITLE = "Character {0} teleports to {1}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spCharacter;
		private SerializedProperty spQuestion;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(NODE_TITLE, this.characterTarget, toTagPath);
		}

		protected override void OnEnableEditorChild ()
		{
			this.spCharacter = this.serializedObject.FindProperty("characterTarget");
			this.spQuestion = this.serializedObject.FindProperty("toTagPath");
		}

		protected override void OnDisableEditorChild ()
		{
			this.spQuestion = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spCharacter);

			EditorGUILayout.PropertyField(this.spQuestion, new GUIContent("To Tag Path"));
			
			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}
