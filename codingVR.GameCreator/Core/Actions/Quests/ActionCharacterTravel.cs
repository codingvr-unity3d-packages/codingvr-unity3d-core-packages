﻿namespace GameCreator.Characters
{
	using System;
	using System.Collections;
	using UnityEngine;
	using UnityEngine.SceneManagement;
	using Zenject;
	using GameCreator.Core;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionCharacterTravel : GameQuestionTarget
	{
		private bool forceStop = false;
		public TargetCharacter characterTarget = new TargetCharacter(TargetCharacter.Target.Player);
		Character character = null;

		[Range(0.0f, 5.0f)]
		[Tooltip("Threshold distance from the target that is considered as reached")]
		private float stopThreshold = 0.005f;

		// cancellation
		public bool cancelable = false;
		public float cancelDelay = 2.0f;

		// charatcter moving in progress
		codingVR.GameEngine.ICharacterMoving characterMoving = null;
		private bool wasControllable = false;

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			return false;
		}

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			var data = SelectedData();
			if (data != null)
			{
				Character charTarget = this.characterTarget.GetCharacter(target);
				TargetCharacter character = null;
				if (charTarget == null)
				{
					// by default player is selected
					character = new TargetCharacter(TargetCharacter.Target.Player);
					charTarget = this.characterTarget.GetCharacter(target);
				}

				// initial state
				wasControllable = charTarget.characterLocomotion.isControllable;
				charTarget.characterLocomotion.SetIsControllable(false);

				// we execute map travel
				codingVR.GameEngine.ICharacterTravel player = charTarget.gameObject.GetComponent<codingVR.GameEngine.ICharacterTravel>();
				Debug.AssertFormat(player != null, "{0} : CharacterStates for GameObject {1} required", System.Reflection.MethodBase.GetCurrentMethod().Name, name);
				codingVR.Data.TagPair tagPair = codingVR.Data.TagPair.MakePair(SelectedData().Tag, SelectedData().Answer);

				player.SelectPath(tagPair);
				characterMoving = player.MoveOnPathBegin(false);
				yield return characterMoving.ExecuteCompleteIteration(cancelable, charTarget, stopThreshold, cancelDelay, target);

				// stop travel
				ILocomotionSystem.TargetRotation cRotation = null;
				charTarget.characterLocomotion.Stop(cRotation, CharacterArrivedCallback);

				// restore character states
				charTarget.characterLocomotion.SetIsControllable(wasControllable);

				// detach hexUnitMoveOnPath
				characterMoving = null;

				// all right
#if UNITY_EDITOR
				Debug.Log(GetNodeTitle() + " Done !!!");
#endif
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}

			yield return 0;
		}

		public override void Stop()
		{
			if (characterMoving != null)
				characterMoving.ForceStop = true;

			if (this.character == null) return;


			this.character.characterLocomotion.SetIsControllable(this.wasControllable);
			this.character.characterLocomotion.Stop();
		}

		public void CharacterArrivedCallback()
		{
		}


		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/Quests/Character/Travel character";
		private const string NODE_TITLE = "Character {0} travels from {1} to {2}";

		private static readonly GUIContent GC_CANCEL = new GUIContent("Cancelable");

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spCharacter;
		private SerializedProperty spCancelable;
		private SerializedProperty spCancelDelay;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(NODE_TITLE, this.characterTarget, base.TitleTarget(), base.SelectedData()?.Answer);
		}

		protected override void OnEnableEditorChild()
		{
			this.spCharacter = this.serializedObject.FindProperty("characterTarget");

			base.EnableEditorChild();

			this.spCancelable = this.serializedObject.FindProperty("cancelable");
			this.spCancelDelay = this.serializedObject.FindProperty("cancelDelay");
		}

		protected override void OnDisableEditorChild()
		{
			this.spCharacter = null;

			base.DisableEditorChild();

			this.spCancelable = null;
			this.spCancelDelay = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spCharacter);

			base.InspectorGUI();

			EditorGUILayout.Space();
			Rect rect = GUILayoutUtility.GetRect(GUIContent.none, EditorStyles.textField);
			Rect rectLabel = new Rect(
				rect.x,
				rect.y,
				EditorGUIUtility.labelWidth,
				rect.height
			);
			Rect rectCancenable = new Rect(
				rectLabel.x + rectLabel.width,
				rectLabel.y,
				20f,
				rectLabel.height
			);
			Rect rectDelay = new Rect(
				rectCancenable.x + rectCancenable.width,
				rectCancenable.y,
				rect.width - (rectLabel.width + rectCancenable.width),
				rectCancenable.height
			);

			EditorGUI.LabelField(rectLabel, GC_CANCEL);
			EditorGUI.PropertyField(rectCancenable, this.spCancelable, GUIContent.none);
			EditorGUI.BeginDisabledGroup(!this.spCancelable.boolValue);
			EditorGUI.PropertyField(rectDelay, this.spCancelDelay, GUIContent.none);
			EditorGUI.EndDisabledGroup();

			this.serializedObject.ApplyModifiedProperties();
		}

#endif
	}
}
