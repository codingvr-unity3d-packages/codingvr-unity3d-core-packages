﻿namespace GameCreator.Core
{
	using System;
	using System.Collections;
	using UnityEngine;
	using UnityEngine.SceneManagement;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionCharacterTeleport : GameQuestionTarget
	{
		private DiContainer diContainer;
		private bool forceStop = false;

		[Inject]
		public void Contruct(DiContainer diContainer)
		{
			this.diContainer = diContainer;
		}

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
		{
			return false;
		}

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			// we execute map travel
			this.forceStop = false;
			var playerCharacter = Hooks.HookPlayer.Instance.Get<GameCreator.Characters.PlayerCharacter>();
			codingVR.GameEngine.ICharacterTravel characterTravel = playerCharacter.GetComponentInChildren<codingVR.GameEngine.ICharacterTravel>();
			codingVR.Data.TagPair tagPair = codingVR.Data.TagPair.MakePair(SelectedData().Tag, SelectedData().Answer);
			characterTravel.Teleport(tagPair.Second);

			// all right
#if UNITY_EDITOR
			Debug.Log(GetNodeTitle() + " Done !!!");
#endif

			yield return 0;
		}

		public override void Stop()
		{
			forceStop = true;
			return;
		}

		// +--------------------------------------------------------------------------------------+
		// | EDITOR                                                                               |
		// +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

		public static new string NAME = "CodingVR/Quests/Character/Teleport character";
		private const string NODE_TITLE = "Character teleport from {0} to {1}";

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			string answer = "???";
			var data = SelectedData();
			if (data != null)
			{
				answer = data.Answer;
			}
			return string.Format(NODE_TITLE, base.TitleTarget(), answer);
		}

		protected override void OnEnableEditorChild ()
		{
			base.EnableEditorChild();
		}

		protected override void OnDisableEditorChild ()
		{
			base.DisableEditorChild();
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			base.InspectorGUI();
			
			this.serializedObject.ApplyModifiedProperties();
		}

		#endif
	}
}
