﻿namespace GameCreator.Core
{
    using System;
    using System.Collections;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using Zenject;

#if UNITY_EDITOR
    using UnityEditor;
#endif

    [AddComponentMenu("")]
    public class ActionCharacterSelectPath : GameQuestionTarget
    {
        public bool nextPath = false;
        private DiContainer diContainer;

        [Inject]
        public void Contruct(DiContainer diContainer)
        {
            this.diContainer = diContainer;
        }

        // EXECUTABLE: ----------------------------------------------------------------------------

        public override bool InstantExecute(GameObject target, IAction[] actions, int index)
        {
            // we execute map travel
            {
                codingVR.Data.TagPair tagPair = codingVR.Data.TagPair.MakePair(SelectedData().Tag, SelectedData().Answer);

				var playerCharacter = Hooks.HookPlayer.Instance.Get<GameCreator.Characters.PlayerCharacter>();
				codingVR.GameEngine.ICharacterTravel characterTravel = playerCharacter.GetComponentInChildren<codingVR.GameEngine.ICharacterTravel>();
                if (nextPath == true)
                {
					characterTravel.SelectNextPath(tagPair);
                }
                else
                {
					characterTravel.SelectPath(tagPair);
                }
            }

            // all right
#if UNITY_EDITOR
            Debug.Log(GetNodeTitle() + " Done !!!");
#endif
            return true;
        }

        public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
        {
            yield return null;
        }

        // +--------------------------------------------------------------------------------------+
        // | EDITOR                                                                               |
        // +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

        public static new string NAME = "CodingVR/Quests/Character/Select path";
        private const string NODE_TITLE = "Character {0} path selected from {1} to {2}";

        // PROPERTIES: ----------------------------------------------------------------------------

        private SerializedProperty spNextPath;

        // INSPECTOR METHODS: ---------------------------------------------------------------------

        public override string GetNodeTitle()
        {
            string answer = "???";
            string next = "";
            var data = SelectedData();
            if (data != null)
            {
                answer = data.Answer;
            }

            if (nextPath == true)
            {
                next = "next";
            }

            return string.Format(NODE_TITLE, next, base.TitleTarget(), answer);
        }

        protected override void OnEnableEditorChild()
        {
            base.EnableEditorChild();
            this.spNextPath = this.serializedObject.FindProperty("nextPath");
        }

        protected override void OnDisableEditorChild()
        {
            base.DisableEditorChild();
            this.spNextPath = null;
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            base.InspectorGUI();

            EditorGUILayout.PropertyField(this.spNextPath, new GUIContent("Next path"));

            this.serializedObject.ApplyModifiedProperties();
        }
#endif
    }
}
