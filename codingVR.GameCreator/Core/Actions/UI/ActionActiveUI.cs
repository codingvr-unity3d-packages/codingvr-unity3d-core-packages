﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using Zenject;
	using codingVR.GameEngine;

#if UNITY_EDITOR
	using UnityEditor;
#endif
	[AddComponentMenu("")]
	public class ActionActiveUI : IAction
	{

		private string[] panelsUI = new[] { "Progression", "Menu","Debug","Money" };
		public int indexUI = 0;
		public bool active = false;

		private bool forceStop = false;

		private codingVR.Core.IEventManager eventManager;

		[Inject]
		public void Contruct(codingVR.Core.IEventManager eventManager)
		{
			this.eventManager = eventManager;
		}

		/* 
		 * 
		 * https://gitlab.com/monamipoto/potomaze/-/issues/590
		 * When the container for this object is a scriptable object; this HACK is used to inject dependency 
		 * 
		 */

		public void DIPInject(GameObject invoker)
		{
			if (this.eventManager == null)
			{
				if (invoker != null)
				{
					var dipInject = invoker.GetComponent<codingVR.GameEngine.IDIPInject>();
					if (dipInject != null)
						dipInject.DiContainer.Inject(this);
				}
			}
		}

		public override bool InstantExecute(GameObject target, IAction[] actions, int index)
        {
			eventManager.QueueEvent(new Event_GameUIActiveItem(this.indexUI, this.active));
			
			return true;
        }

		#if UNITY_EDITOR

        public static new string NAME = "CodingVR/UI/ActionActiveUI";
		private const string NODE_TITLE = "Active UI";

		// PROPERTIES: ----------------------------------------------------------------------------

		//private SerializedProperty spPanelsUI;
		private SerializedProperty spActive;
		private SerializedProperty spIndex;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return NODE_TITLE;
				//string.Format(NODE_TITLE));
		}

		protected override void OnEnableEditorChild()
		{
			//this.spPanelsUI = this.serializedObject.FindProperty("panelsUI");
			this.spActive = this.serializedObject.FindProperty("active");
			this.spIndex = this.serializedObject.FindProperty("indexUI");
		}

		protected override void OnDisableEditorChild()
		{
			this.spActive = null;
			this.spIndex = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spActive);
			
			if (this.panelsUI.Length > 0 && indexUI <= this.panelsUI.Length-1)
			{
				this.spIndex.intValue = EditorGUILayout.Popup(this.spIndex.intValue, panelsUI);
			}

			this.serializedObject.ApplyModifiedProperties();
		}
#endif
	}
}
