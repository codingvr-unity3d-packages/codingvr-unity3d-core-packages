﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameCreator
{
	public interface ITimelineManager
	{
		bool Push(string domainId, int actionPriority, float actionWeight, global::GameCreator.Core.Actions actions);
		global::GameCreator.Core.Actions[] Pop(string domainId);
	}
}
