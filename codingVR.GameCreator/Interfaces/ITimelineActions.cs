﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.GameCreator
{
	public interface ITimelineActions
	{
		bool Push(string domainId, string actionId, int actionPriority, global::GameCreator.Core.Actions actions);
		global::GameCreator.Core.Actions Pop(string domainId, string actionId);
		global::GameCreator.Core.Actions[] PopAll(string domainId);
	}
}
