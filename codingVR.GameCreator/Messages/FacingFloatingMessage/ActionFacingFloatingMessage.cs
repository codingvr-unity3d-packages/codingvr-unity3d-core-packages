﻿namespace GameCreator.Messages
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using UnityEngine.Audio;
	using GameCreator.Core;
	using GameCreator.Localization;
	using Zenject;
	using codingVR.TextAnimated;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionFacingFloatingMessage : IAction
	{
		public AudioClip audioClip;

		[LocStringNoPostProcess]
		public LocString message = new LocString();
		[TextArea(3, 7)]
		public string textArea = "";
		public Color color = Color.black;
		public float time = 2.0f;

		public TargetGameObject target = new TargetGameObject(TargetGameObject.Target.GameObject);
		public Vector3 offset = new Vector3(0, 2, 0);

		//Permet de lancer les animations du message. Remettre à true pour les tests,
		//puis rajouter dans l'action la possibilité de le changer
		public bool isAnimated = false;

		private bool forceStop = false;
		public bool endAnimated = false;

		private int nbClick = 0;
		private TMP_Animated textAnimated;

		// interface
		private DiContainer diContainer;
		private codingVR.Core.IEventManager eventManager;
		private codingVR.GameEngine.IPlayerStateManager playerStateManager;

		// LIFECYCLE: ----------------------------------------------------------------------------------

		[Inject]
		public void Contruct(DiContainer diContainer, codingVR.Core.IEventManager eventManager, codingVR.GameEngine.IPlayerStateManager playerStateManager)
		{
			this.eventManager = eventManager;
			this.diContainer = diContainer;
			this.playerStateManager = playerStateManager;
		}

		private void OnClickMouse(codingVR.Data.Event_OnClickMouse ev)
		{
			Debug.Log("Onclick mouse conversation nbClick : " + nbClick);

			if (isAnimated == true)
			{
				if(nbClick > 0 || endAnimated == true)
				{
					Stop();
				}

				else
				{
					textAnimated.EndReadText();
					nbClick++;
				}
			}
			else
			{
				Stop();
			}
			
		}

		private void OnEndTextAnimated()
		{
			endAnimated = true;
		}

		public void SetEventManager(codingVR.Core.IEventManager eventManager)
		{
			if (this.eventManager == null)
			{
				this.eventManager = eventManager;
			}
		}

		private void DIPInject(GameObject invoker)
		{
			if (invoker != null && diContainer == null)
			{
				var dipInject = invoker.GetComponent<codingVR.GameEngine.IDIPInject>();
				if (dipInject != null)
				{
					dipInject.DiContainer.Inject(this);
					diContainer = dipInject.DiContainer;
				}
			}
		}


		// EXECUTABLE: ----------------------------------------------------------------------------

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			DIPInject(target);
			this.forceStop = false;
			if (eventManager != null) eventManager.AddListener<codingVR.Data.Event_OnClickMouse>(OnClickMouse);
			Transform targetTransform = this.target.GetTransform(target);
			GameObject instance = null;

			if (targetTransform != null)
			{
				if (this.audioClip != null)
				{
					AudioMixerGroup voiceMixer = DatabaseGeneral.Load().voiceAudioMixer;
					AudioManager.Instance.PlayVoice(this.audioClip, 0f, 1f, voiceMixer);
				}

				instance = FacingFloatingMessageManager.ShowAndReturn(this.playerStateManager, this.message.GetText(), this.color, targetTransform, this.offset, this.time, this.isAnimated);
				float timeMessage = 0.0f;

				if (isAnimated)
				{
					if (instance != null)
					{
						endAnimated = false;
						textAnimated = instance.GetComponentInChildren<TMP_Animated>();
						textAnimated.onDialogueFinish.AddListener(() => OnEndTextAnimated());
						yield return new WaitUntil(() => endAnimated || this.forceStop);
						textAnimated.onDialogueFinish.RemoveListener(() => OnEndTextAnimated());
					}
				}

				float waitTime = Time.time + this.time;
				WaitUntil waitUntil = new WaitUntil(() => Time.time > waitTime || this.forceStop);
				yield return waitUntil;

				if (this.audioClip != null)
				{
					AudioManager.Instance.StopVoice(this.audioClip);
				}
			}

			if (this.forceStop == true && instance != null) GameObject.Destroy(instance);
			if (eventManager != null) eventManager.RemoveListener<codingVR.Data.Event_OnClickMouse>(OnClickMouse);
			yield return 0;
		}

		public override void Stop()
		{
			this.forceStop = true;
		}

#if UNITY_EDITOR
		public static new string NAME = "CodingVR/Messages/Floating Message";
		private const string NODE_TITLE = "Show message: {0}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spAudioClip;
		private SerializedProperty spMessage;
		private SerializedProperty spTextArea;
		private SerializedProperty spColor;
		private SerializedProperty spTime;
		private SerializedProperty spTarget;
		private SerializedProperty spOffset;
		private SerializedProperty spIsAnimated;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				(this.message.content.Length > 23
					? this.message.content.Substring(0, 20) + "..."
					: this.message.content
				)
			);
		}

		protected override void OnEnableEditorChild()
		{
			this.spAudioClip = this.serializedObject.FindProperty("audioClip");
			this.spMessage = this.serializedObject.FindProperty("message");
			this.spTextArea = this.serializedObject.FindProperty("textArea");
			this.spColor = this.serializedObject.FindProperty("color");
			this.spTime = this.serializedObject.FindProperty("time");

			this.spTarget = this.serializedObject.FindProperty("target");
			this.spOffset = this.serializedObject.FindProperty("offset");
			this.spIsAnimated = this.serializedObject.FindProperty("isAnimated");
		}

		protected override void OnDisableEditorChild()
		{
			this.spAudioClip = null;
			this.spMessage = null;
			this.spTextArea = null;
			this.spColor = null;
			this.spTime = null;

			this.spTarget = null;
			this.spOffset = null;
			this.spIsAnimated = null;
		}

		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spMessage);
			EditorGUILayout.PropertyField(this.spTextArea);

			if (GUILayout.Button("Appliquer texte au message"))
			{
				this.message.content = this.textArea;
			}

			EditorGUILayout.PropertyField(this.spAudioClip);
			EditorGUILayout.PropertyField(this.spColor);

			EditorGUILayout.Space();
			EditorGUILayout.PropertyField(this.spTime);

			if (this.spAudioClip.objectReferenceValue != null)
			{
				AudioClip clip = (AudioClip)this.spAudioClip.objectReferenceValue;
				if (!Mathf.Approximately(clip.length, this.spTime.floatValue))
				{
					Rect btnRect = GUILayoutUtility.GetRect(GUIContent.none, EditorStyles.miniButton);
					btnRect = new Rect(
						btnRect.x + EditorGUIUtility.labelWidth,
						btnRect.y,
						btnRect.width - EditorGUIUtility.labelWidth,
						btnRect.height
					);

					if (GUI.Button(btnRect, "Use Audio Length", EditorStyles.miniButton))
					{
						this.spTime.floatValue = clip.length;
					}
				}
			}

			EditorGUILayout.Space();
			EditorGUILayout.PropertyField(this.spTarget);
			EditorGUILayout.PropertyField(this.spOffset);
			EditorGUILayout.PropertyField(this.spIsAnimated);

			this.serializedObject.ApplyModifiedProperties();
		}
#endif
	}
}
