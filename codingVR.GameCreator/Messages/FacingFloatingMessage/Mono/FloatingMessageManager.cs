﻿namespace GameCreator.Messages
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;
	using UnityEngine.Animations;
	using GameCreator.Core;
	using GameCreator.Core.Hooks;
	using TMPro;
	using codingVR.TextAnimated;
	using Zenject;

	public static class FacingFloatingMessageManager
	{
		private const string CANVAS_ASSET_PATH = "GameCreator/Messages/TMP_FacingFloatingMessage";
		private const float TRANSITION_TIME = 0.3f;
		private static int ANIMATOR_HASH_CLOSE = -1;

		private static bool INITIALIZED = false;
		private static GameObject PREFAB;
		private static bool endAnimated = false;

		// INITIALIZE: ----------------------------------------------------------------------------

		private static void RequireInit()
		{
			if (INITIALIZED) return;
			EventSystemManager.Instance.Wakeup();
			ANIMATOR_HASH_CLOSE = Animator.StringToHash("Close");

			PREFAB = Resources.Load<GameObject>(CANVAS_ASSET_PATH);
		}

		// PUBLIC METHODS: ------------------------------------------------------------------------

		public static void Show(codingVR.GameEngine.IPlayerStateManager playerStateManager, string message, Color color, Transform target, Vector3 offset, float duration)
		{
			RequireInit();
			GameObject instance = GameObject.Instantiate(PREFAB, target);
			instance.transform.localPosition = offset;

			CoroutinesManager.Instance.StartCoroutine(CoroutineShow(playerStateManager, message, color, instance, duration, target.gameObject,false));
		}

        public static GameObject ShowAndReturn(codingVR.GameEngine.IPlayerStateManager playerStateManager, string message, Color color, Transform target, Vector3 offset, float duration, bool isAnimated)
        {
            RequireInit();
			GameObject instance = GameObject.Instantiate(PREFAB, target);
            instance.transform.localPosition = offset;

            CoroutinesManager.Instance.StartCoroutine(CoroutineShow(playerStateManager, message, color, instance, duration, target.gameObject, isAnimated));

            return instance;
        }

		// PRIVATE METHODS: -----------------------------------------------------------------------

		
		private static IEnumerator CoroutineShow(codingVR.GameEngine.IPlayerStateManager playerStateManager, string message, Color color, 
			GameObject instance, float duration,GameObject interlocutor, bool isAnimated)
		{
			Canvas canvas = instance.GetComponent<Canvas>();
			Camera camera = HookCamera.Instance != null ? HookCamera.Instance.Get<Camera>() : null;
			if (!camera) camera = GameObject.FindObjectOfType<Camera>();

			if (canvas != null) canvas.worldCamera = camera;

			Animator animator = instance.GetComponentInChildren<Animator>();
			var pro_text = instance.GetComponentInChildren<TMP_Text>();
			var pro_textAnimated = instance.GetComponentInChildren<TMP_Animated>();

			float timeMessage = 0.0f;

			if (pro_textAnimated == null)
			{
				if (pro_text == null)
				{
					var text = instance.GetComponentInChildren<Text>();
					if (text != null)
					{

						text.text = message;
						text.color = color;
					}
				}
				else
				{
					pro_text.text = message;
					pro_text.color = color;
				}
			}

			else
			{
				endAnimated = false;

				if (isAnimated)
				{
					pro_textAnimated.onDialogueFinish.AddListener(() => OnEndTextAnimated());
				}

				pro_textAnimated.textAnimated(playerStateManager, message, isAnimated, interlocutor, false);
				pro_textAnimated.color = color;

				if (isAnimated)
				{
					yield return new WaitUntil(() => endAnimated);
					pro_textAnimated.onDialogueFinish.RemoveListener(() => OnEndTextAnimated());
				}
			}
			
			LookAtConstraint constraint = instance.GetComponent<LookAtConstraint>();
			if (constraint != null)
			{
				constraint.SetSources(new List<ConstraintSource>()
				{
					new ConstraintSource()
					{
						sourceTransform = HookCamera.Instance.transform,
						weight = 1.0f
					}
				});

				constraint.constraintActive = true;
			}

			WaitForSecondsRealtime wait = new WaitForSecondsRealtime(duration + timeMessage - TRANSITION_TIME);
			yield return wait;

			if (animator != null) animator.SetTrigger(ANIMATOR_HASH_CLOSE);

			wait = new WaitForSecondsRealtime(TRANSITION_TIME);
			yield return wait;

			if (instance != null) GameObject.Destroy(instance);
		}

		private static void OnEndTextAnimated()
		{
			endAnimated = true;
		}
	}
}