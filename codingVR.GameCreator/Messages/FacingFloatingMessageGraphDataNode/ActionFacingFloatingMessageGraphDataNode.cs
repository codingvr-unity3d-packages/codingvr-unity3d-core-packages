﻿namespace GameCreator.Messages
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
	using UnityEngine.Audio;
	using GameCreator.Core;
	using GameCreator.Localization;
	using Zenject;

#if UNITY_EDITOR
	using UnityEditor;
#endif

	[AddComponentMenu("")]
	public class ActionFacingFloatingMessageGraphDataNode : GameGraphDataNodeTarget
	{
		public AudioClip audioClip;

		public Color color = Color.black;
		public float time = 2.0f;

		public TargetGameObject target = new TargetGameObject(TargetGameObject.Target.GameObject);
		public Vector3 offset = new Vector3(0, 2, 0);

		private bool forceStop = false;

		// interfaces
		DiContainer diContainer;
		codingVR.GameEngine.IPlayerStateManager playerStateManager;

		// LIFECYCLE: ----------------------------------------------------------------------------

		[Inject]
		public void Constructor(DiContainer diContainer, codingVR.GameEngine.IPlayerStateManager playerStateManager)
		{
			this.diContainer = diContainer;
			this.playerStateManager = playerStateManager;
		}

		private void DIPInject(GameObject invoker)
		{
			if (invoker != null && diContainer == null)
			{
				var dipInject = invoker.GetComponent<codingVR.GameEngine.IDIPInject>();
				if (dipInject != null)
				{
					dipInject.DiContainer.Inject(this);
					diContainer = dipInject.DiContainer;
				}
			}
		}

		// EXECUTABLE: ----------------------------------------------------------------------------

		public override IEnumerator Execute(GameObject target, IAction[] actions, int index)
		{
			DIPInject(target);

			this.forceStop = false;

			var data = SelectedData();
			if (data != null)
			{
				// load data 
				diContainer.Inject(data);
				yield return data.GetDataSpec();
				yield return data.GetData(true);
			}

			Transform targetTransform = this.target.GetTransform(target);
			if (targetTransform != null && data != null)
			{
				if (this.audioClip != null)
				{
					AudioMixerGroup voiceMixer = DatabaseGeneral.Load().voiceAudioMixer;
					AudioManager.Instance.PlayVoice(this.audioClip, 0f, 1f, voiceMixer);
				}

				FacingFloatingMessageManager.Show(playerStateManager, data.Answer, this.color,	targetTransform, this.offset, this.time);

				float waitTime = Time.time + this.time;
				WaitUntil waitUntil = new WaitUntil(() => Time.time > waitTime || this.forceStop);
				yield return waitUntil;

				if (this.audioClip != null)
				{
					AudioManager.Instance.StopVoice(this.audioClip);
				}
			}

			yield return 0;
		}

		public override void Stop()
		{
			this.forceStop = true;
		}

#if UNITY_EDITOR
		public static new string NAME = "CodingVR/Messages/Floating Message GraphDataNode";
		private const string NODE_TITLE = "Show message: #{0}:{1}";

		// PROPERTIES: ----------------------------------------------------------------------------

		private SerializedProperty spAudioClip;
		private SerializedProperty spColor;
		private SerializedProperty spTime;
		private SerializedProperty spTarget;
		private SerializedProperty spOffset;

		// INSPECTOR METHODS: ---------------------------------------------------------------------

		public override string GetNodeTitle()
		{
			return string.Format(
				NODE_TITLE,
				base.TagTarget(),
				(base.Answer().Length > 23 ? base.Answer().Substring(0, 20) + "..."	: base.Answer())
			);
		}

		protected override void OnEnableEditorChild()
		{
			base.EnableEditorChild();

			this.spAudioClip = this.serializedObject.FindProperty("audioClip");
			this.spColor = this.serializedObject.FindProperty("color");
			this.spTime = this.serializedObject.FindProperty("time");

			this.spTarget = this.serializedObject.FindProperty("target");
			this.spOffset = this.serializedObject.FindProperty("offset");
		}

		protected override void OnDisableEditorChild()
		{
			base.OnDisableEditorChild();

			this.spAudioClip = null;
			this.spColor = null;
			this.spTime = null;

			this.spTarget = null;
			this.spOffset = null;
		}
		
		public override void OnInspectorGUI()
		{
			this.serializedObject.Update();

			EditorGUILayout.PropertyField(this.spAudioClip);
			EditorGUILayout.PropertyField(this.spColor);

			EditorGUILayout.Space();
			EditorGUILayout.PropertyField(this.spTime);

			if (this.spAudioClip.objectReferenceValue != null)
			{
				AudioClip clip = (AudioClip)this.spAudioClip.objectReferenceValue;
				if (!Mathf.Approximately(clip.length, this.spTime.floatValue))
				{
					Rect btnRect = GUILayoutUtility.GetRect(GUIContent.none, EditorStyles.miniButton);
					btnRect = new Rect(
						btnRect.x + EditorGUIUtility.labelWidth,
						btnRect.y,
						btnRect.width - EditorGUIUtility.labelWidth,
						btnRect.height
					);

					if (GUI.Button(btnRect, "Use Audio Length", EditorStyles.miniButton))
					{
						this.spTime.floatValue = clip.length;
					}
				}
			}

			base.InspectorGUI();

			EditorGUILayout.Space();
			EditorGUILayout.PropertyField(this.spTarget);
			EditorGUILayout.PropertyField(this.spOffset);

			this.serializedObject.ApplyModifiedProperties();
		}
#endif
	}
}
