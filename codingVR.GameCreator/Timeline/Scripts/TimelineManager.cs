﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameCreator.Core;
using NaughtyAttributes;
using UnityEngine;
using Zenject;

namespace codingVR.GameCreator
{
	// https://stackoverflow.com/questions/22539581/linq-select-one-field-from-list-of-dto-objects-to-array
	public class TimelineManager : MonoBehaviour, ITimelineManager
	{
		struct ActionPriority
		{
			public int priority;
			public float weight;
			public global::GameCreator.Core.Actions actions;
		}

		[Serializable]
		class ActionsDomain : Core.cvrSerializableDictionary<string, List<ActionPriority>> { }

		[SerializeField]
		[ReadOnly]
		ActionsDomain actionDomains = new ActionsDomain();

		[ShowNativeProperty]
		private int CountDomain
		{
			get
			{
				return actionDomains.Count;
			}
		}

		DiContainer diContainer;

		// === LifeCycle ======================================================================================

		[Inject]
		public void Constructor(DiContainer diContainer)
		{
			this.diContainer = diContainer;
		}

		// === Implementation ===============================================================================

		IEnumerator PopAuto(string domainId)
		{
			var actionGameSceneSession = gameObject.AddComponent<global::GameCreator.Core.PopTimelineActions>() as global::GameCreator.Core.PopTimelineActions;
			// start login transaction
			diContainer.Inject(actionGameSceneSession);
			actionGameSceneSession.domainId = domainId;
			actionGameSceneSession.waitToFinish = true;
			yield return actionGameSceneSession.Execute(gameObject, null, 0);
		}

		// === ITimelineManager ===============================================================================

		#region ITimelineManager

		public global::GameCreator.Core.Actions[] Pop(string domainId)
		{
			global::GameCreator.Core.Actions[] list = null;
			bool b = actionDomains.ContainsKey(domainId);
			if (b == true)
			{
				list = actionDomains[domainId]
						.OrderBy(l => l.priority)
						.Select(l => l.actions)
						.Select(a => { a.runInBackground = false; return a; })
						.ToArray();
				actionDomains[domainId].Clear();
				actionDomains.Remove(domainId);
			}
			else
			{
				Debug.LogErrorFormat("{0} : domain {1} not found !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, domainId);
			}

			return list;
		}

		public bool Push(string domainId, int actionsPriority, float actionWeight, Actions actionsObject)
		{
			bool b = actionDomains.ContainsKey(domainId);
			if (b == false)
			{
				actionDomains.Add(domainId, new List<ActionPriority>());
				b = true;
			}

			if (b == true)
			{
				actionDomains[domainId].Add(new ActionPriority { actions = actionsObject, weight = actionWeight, priority = actionsPriority });
			}

			if (b == true)
			{
				float totalWeight = 0.0f;
				foreach(var it in actionDomains[domainId])
				{
					totalWeight += it.weight;
				}

				if (totalWeight >= 1.0f)
				{
					StartCoroutine(PopAuto(domainId));
				}
			}

			return b;
		}

		#endregion
	}
}
