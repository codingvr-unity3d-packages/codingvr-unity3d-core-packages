﻿using UnityEngine;
using GameCreator.Variables;

namespace GameCreator.Behavior
{
	public class SetupNodeBehaviorGraph : LocalVariables
	{
		// === DATA-UI ============================================================

		#region DATA-UI

		[SerializeField]
		BehaviorGraph behaviorGraph = null;

		[SerializeField]
		NodeBehaviorGraph behaviorGraphOwner = null;

		#endregion

		// === IMPLEMENTATION-CORE ============================================================

		#region IMPLEMENTATION-CORE 

		// sub behavior graph		
		public BehaviorGraph BehaviorGraph => behaviorGraph;

		// node behavior graph owner 
		// from GC behavior decorator such NodeDecoratorSetupNodeBehaviorGraph
		public NodeBehaviorGraph BehaviorGraphOwner
		{
			private get
			{
				return behaviorGraphOwner;
			}
			set
			{
				behaviorGraphOwner = value;
			}
		}

		// reset behavior graph owner since the owner is probably a scriptable object
		private void ResetBehaviorGraphOwner()
		{
			if (BehaviorGraphOwner != null)
			{
				BehaviorGraphOwner.behaviorGraph = null;
			}
		}

		#endregion

		// === UNITY-LIFECYCLE ============================================================

		#region UNITY-LIFECYCLE

		// Start is called before the first frame update
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{

		}

		public void OnEnable()
		{
			ResetBehaviorGraphOwner();
		}

		public void OnDisable()
		{
			ResetBehaviorGraphOwner();
		}

		public void OnDestroy()
		{
			ResetBehaviorGraphOwner();
		}

		#endregion
	}
}