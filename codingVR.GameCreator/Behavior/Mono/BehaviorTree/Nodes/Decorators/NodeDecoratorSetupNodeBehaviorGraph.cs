﻿namespace GameCreator.Behavior
{
	using GameCreator.Variables;
	using System;
	using UnityEngine;

	[Serializable]
	public class NodeDecoratorSetupNodeBehaviorGraph : NodeIDecorator
	{
		public new static string NAME = "Setup Node Behavior Graph";

		[VariableFilter(Variable.DataType.GameObject)]
		public VariableProperty variable = new VariableProperty(Variable.VarType.LocalVariable);

		NodeBehaviorGraph currentNodeBehaviorGraphNode = null;
			   
		// Reset temp current node behavior graph node
		void ResetCurrentNodeBehaviorGraphNode()
		{
			if (currentNodeBehaviorGraphNode != null)
			{
				currentNodeBehaviorGraphNode.behaviorGraph = null;
			}
		}

		// VIRTUAL METHODS: -----------------------------------------------------------------------

		public override Node.Return Execute(Node node, GameObject invoker, Behavior behavior)
		{
			GameObject value = this.variable.Get(invoker) as GameObject;
			if (value != null)
			{
				var setupNodeBehaviorGraph = value.GetComponent<SetupNodeBehaviorGraph>();
				if (setupNodeBehaviorGraph != null)
				{
					foreach (var it in node.outputs)
					{
						currentNodeBehaviorGraphNode = (it as NodeBehaviorGraph);
						if (null != currentNodeBehaviorGraphNode)
						{
							if (currentNodeBehaviorGraphNode.behaviorGraph == null)
							{
								currentNodeBehaviorGraphNode.behaviorGraph = setupNodeBehaviorGraph.BehaviorGraph;
								setupNodeBehaviorGraph.BehaviorGraphOwner = currentNodeBehaviorGraphNode;
								break;
							}
						}
					}
				}
				else
				{
					Debug.LogErrorFormat("{0} : Variable {1} expects a component SetupNodeBehaviorGraph", System.Reflection.MethodBase.GetCurrentMethod().Name, variable.ToString());
				}

				if (currentNodeBehaviorGraphNode == null)
				{
					Debug.LogErrorFormat("{0} : Decorator NodeDecoratorSetupNodeBehavior expects a sub Task using a least one NodeBehaviorGraph", System.Reflection.MethodBase.GetCurrentMethod().Name);
				}
			}
			
			Node.Return result = base.Execute(node, invoker, behavior);

			if (currentNodeBehaviorGraphNode == null || currentNodeBehaviorGraphNode.behaviorGraph == null)
			{
				result = Node.Return.Fail;
			}

			if (result != Node.Return.Running)
			{
				ResetCurrentNodeBehaviorGraphNode();
			}

			return result;
		}

		public override void Abort(Node node, GameObject invoker, Behavior behavior)
		{
			ResetCurrentNodeBehaviorGraphNode();
			return;
		}

		public override string GetName()
		{
			return NAME;
		}
	}
}