﻿using UnityEngine;

namespace GameCreator.Core.Hooks
{
    [AddComponentMenu("CodingVR/Hooks/HookHexMapCamera", 100)]
    public class HookHexMapCamera : IHook<HookHexMapCamera> { }
}
