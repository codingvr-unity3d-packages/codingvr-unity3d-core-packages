﻿using UnityEngine;

namespace GameCreator.Core.Hooks
{
    [AddComponentMenu("CodingVR/Hooks/HookCompanion", 100)]
    public class HookCompanion : IHook<HookCompanion> { }
}
