﻿namespace GameCreator.Core
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEditor;
	using GameCreator.Variables;

	[CustomPropertyDrawer(typeof(TargetActivityVariable))]
	public class TargetActivityVariablePD : TargetGenericPD
	{
		public const string PROP_GAMEOBJECT = "gameObject";
		public const string PROP_SCRIPTABLEOBJECT = "activityDataScriptableObject";

		// PAINT METHODS: -------------------------------------------------------------------------

		protected override SerializedProperty GetProperty(int option, SerializedProperty property)
		{
			TargetActivityVariable.Target optionTyped = (TargetActivityVariable.Target)option;
			switch (optionTyped)
			{
				case TargetActivityVariable.Target.GameObject:
					return property.FindPropertyRelative(PROP_GAMEOBJECT);

				case TargetActivityVariable.Target.ScriptableObject:
					return property.FindPropertyRelative(PROP_SCRIPTABLEOBJECT);

				case TargetActivityVariable.Target.Invoker:
				case TargetActivityVariable.Target.ActivityFieldsDatabase:
				case TargetActivityVariable.Target.ActivityNestedFieldsDatabase:
					break;
			}

			return null;
		}

		protected override void Initialize(SerializedProperty property)
		{
		}
	}
}