﻿namespace GameCreator.Core
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using GameCreator.Core.Hooks;
	using GameCreator.Variables;
	using UnityEngine.Events;
	using Zenject;

	[System.Serializable]
	public class TargetActivityVariable
	{
		public enum Target
		{
			Invoker,
			GameObject,
			ScriptableObject,
			ActivityFieldsDatabase,
			ActivityNestedFieldsDatabase,
			FullDatabase,
		}

		codingVR.GameEngine.IPlayerStateActivityController playerStateActivityController;
		codingVR.GameEngine.IPlayerStateManager playerStateMananger;

		[Serializable]
		public class ChangeEvent : UnityEvent { }

		// PROPERTIES: ----------------------------------------------------------------------------

		public Target target = Target.Invoker;

		public GameObject gameObject;
		public codingVR.Data.ActivityFieldScriptable activityDataScriptableObject;

		public ChangeEvent eventChangeVariable = new ChangeEvent();

		// INITIALIZERS: --------------------------------------------------------------------------

		public TargetActivityVariable() { }

		public TargetActivityVariable(TargetActivityVariable.Target target)
		{
			this.target = target;
		}

		// PUBLIC METHODS: ------------------------------------------------------------------------

		public UnityEngine.Object GetObject(codingVR.GameEngine.IPlayerStateActivityController playerStateActivityController, codingVR.GameEngine.IPlayerStateManager playerStateMananger, GameObject invoker)
		{
			UnityEngine.Object result = null;

			switch (this.target)
			{
				case Target.Invoker:
					result = invoker;
					break;

				case Target.GameObject:
					result = this.gameObject;
					break;

				case Target.ScriptableObject:
					result = activityDataScriptableObject;
					break;

				case Target.ActivityFieldsDatabase:
					result = playerStateActivityController.GameObject;
					break;

				case Target.ActivityNestedFieldsDatabase:
					result = playerStateActivityController.GameObject;
					break;

				case Target.FullDatabase:
					result = playerStateMananger.GameObject;
					break;
			}

			return result;
		}

		public (codingVR.Data.IActivityField, codingVR.Data.IActivityNestedField) GetActivityField(codingVR.GameEngine.IPlayerStateActivityController playerStateActivityController, codingVR.GameEngine.IPlayerStateManager playerStateMananger, GameObject invoker)
		{
			codingVR.Data.IActivityField activityField = null;
			codingVR.Data.IActivityNestedField activityNestedField = null;
			UnityEngine.Object targetObject = this.GetObject(playerStateActivityController, playerStateMananger, invoker);

			switch (this.target)
			{
				case Target.Invoker:
					{
						if (targetObject is GameObject targetGo)
						{
							activityField = targetGo.GetComponent<codingVR.Data.IActivityField>();
						}
					}
					break;

				case Target.GameObject:
					{
						if (targetObject is GameObject targetGo)
						{
							activityField = targetGo.GetComponent<codingVR.Data.IActivityField>();
						}
					}
					break;

				case Target.ScriptableObject:
					{
						if (targetObject is codingVR.Data.ActivityFieldScriptable targetSO)
						{
							activityField = targetSO;
						}
					}
					break;

				case Target.ActivityFieldsDatabase:
					{
						if (targetObject is GameObject targetGo)
						{
							activityField = targetGo.GetComponent<codingVR.Data.IActivityField>();
						}
					}
					break;

				case Target.ActivityNestedFieldsDatabase:
					{
						if (targetObject is GameObject targetGo)
						{
							activityField = targetGo.GetComponent<codingVR.Data.IActivityField>();
							activityNestedField = targetGo.GetComponent<codingVR.Data.IActivityNestedField>();
						}
					}
					break;

				case Target.FullDatabase:
					{
						if (targetObject is GameObject targetGo)
						{
							activityField = targetGo.GetComponent<codingVR.Data.IActivityField>();
							activityNestedField = targetGo.GetComponent<codingVR.Data.IActivityNestedField>();
						}
					}
					break;

			}
			return (activityField, activityNestedField);
		}

		// UTILITIES: -----------------------------------------------------------------------------

		public override string ToString()
		{
			string result = "(unknown)";
			switch (this.target)
			{
				case Target.Invoker:
					result = "Invoker";
					break;
				case Target.GameObject:
					result = this.gameObject != null ? this.gameObject.name : "(null)";
					break;
				case Target.ScriptableObject:
					result = this.activityDataScriptableObject != null ? this.activityDataScriptableObject.name : "(null)";
					break;
				case Target.ActivityFieldsDatabase:
					result = "ActivityFieldsDatabase";
					break;
				case Target.ActivityNestedFieldsDatabase:
					result = "ActivityNestedFieldsDatabase";
					break;
				case Target.FullDatabase:
					result = "FullDatabase";
					break;
			}
			return result;
		}
	}
}