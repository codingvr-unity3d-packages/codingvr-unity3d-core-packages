﻿namespace GameCreator.Characters
{
	using System.IO;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.AI;
	using UnityEditor;
	using GameCreator.Core;

	[CustomEditor(typeof(CharacterAccompanies))]
	public class CharacterAccompaniesEditor : Editor 
	{
		// INSPECTOR GUI: -------------------------------------------------------------------------

		public override void OnInspectorGUI ()
		{
		}

		// SCENE GUI: -----------------------------------------------------------------------------

		void OnSceneGUI()
		{
		}
	}
}