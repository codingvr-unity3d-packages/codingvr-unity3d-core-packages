﻿namespace GameCreator.Characters
{
    using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;
    using GameCreator.Core;

#if UNITY_EDITOR
    using UnityEditor;
#endif

    [AddComponentMenu("")]
    public class ActionCharacterExpression : IAction
    {
        public TargetGameObject character = new TargetGameObject(TargetGameObject.Target.GameObject);
        public codingVR.Data.AnimationsScriptable animations;
        public List<string> listAnimations = new List<string>();
        
        public int selected;

        public override bool InstantExecute(GameObject target, IAction[] actions, int index)
        {
            Animator[] animators = this.character.GetComponentsInChildren<Animator>(target);
            Animator animator = null;

            foreach(Animator anim in animators)
            {
                if (anim.gameObject.tag == "cvrExpression") animator = anim;
            }

            if(animator != null && animations != null)
            {
                string trigger = animations.GetTriggerByIndex(selected);
                animator.SetTrigger(trigger);
            }

            return true;
        }

        // +--------------------------------------------------------------------------------------+
        // | EDITOR                                                                               |
        // +--------------------------------------------------------------------------------------+

#if UNITY_EDITOR

        public static new string NAME = "CodingVR/Character/Character expression";
        private const string NODE_TITLE = "{0}";

        // PROPERTIES: ----------------------------------------------------------------------------

        private SerializedProperty spCharacter;
        private SerializedProperty spAnimations;
        private SerializedProperty spAnimationTrigger;
        private SerializedProperty spSelected; 

        // INSPECTOR METHODS: ---------------------------------------------------------------------

        public override string GetNodeTitle()
        {
            return string.Format(
                    NODE_TITLE,
                    "Character expression"
                );
        }

        protected override void OnEnableEditorChild()
        {
            this.spCharacter = this.serializedObject.FindProperty("character");
            this.spAnimations = this.serializedObject.FindProperty("animations");
            this.spAnimationTrigger = this.serializedObject.FindProperty("animationTrigger");
            this.spSelected = this.serializedObject.FindProperty("selected");
            this.spSelected.intValue = 0;
        }

        protected override void OnDisableEditorChild()
        {
            this.spCharacter = null;
            this.spAnimations = null;
            this.spAnimationTrigger = null;
            this.spSelected = null;
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();
            EditorGUILayout.PropertyField(this.spCharacter);
            EditorGUILayout.PropertyField(this.spAnimations);

            if (animations != null)
            {
                string[] listAnimations = new string[animations.dataAnimations.Count];
                int i = 0;  
                          
               foreach (codingVR.Data.AnimationsScriptable.DataAnimation data in animations.dataAnimations)
                {
                    listAnimations[i] = data.nameAnimation;
                    i++;
                }

                this.spSelected.intValue = EditorGUILayout.Popup(this.spSelected.intValue, listAnimations);
            }
            
            this.serializedObject.ApplyModifiedProperties();
        }

    #endif
    }
}




