﻿namespace GameCreator.Characters
{
    using UnityEngine;
    using GameCreator.Core;
    using GameCreator.Core.Hooks;

    [AddComponentMenu("CodingVR/Characters/Player Character Coding VR", 100)]
    public class PlayerCharacterCodingVR : PlayerCharacter
    {
        bool forceDisplayTouchstickPrivate = false;

        public float CurrentSpeed { get; private set; } = 0.0f;

        // INITIALIZERS: --------------------------------------------------------------------------

        protected override void Awake()
        {
            base.Awake();

#if UNITY_EDITOR
            DatabaseGeneral general = DatabaseGeneral.Load();
            if (general.forceDisplayInEditor)
            {
                this.forceDisplayTouchstickPrivate = general.forceDisplayInEditor;
            }
#endif
        }

        // UPDATE: --------------------------------------------------------------------------------

        Vector3 ComputeTargetDirection(Vector3 axis)
        {
            bool bAxisX = (Mathf.Abs(axis.x) > Mathf.Abs(axis.z));
            Vector3 targetDirection = Vector3.zero;

            if (Application.isMobilePlatform ||
                TouchStickManager.FORCE_USAGE ||
                this.forceDisplayTouchstickPrivate)
            {
                Vector2 touchDirection = TouchStickManager.Instance.GetDirection(this);
                if (bAxisX == true)
                {
                    targetDirection = axis * touchDirection.x;
                }
                else
                {
                    targetDirection = axis * touchDirection.y;
                }
            }
            else
            {
                if (bAxisX == true)
                {
                    targetDirection = axis * Input.GetAxis(AXIS_H);
                }
                else
                {
                    targetDirection = axis * Input.GetAxis(AXIS_V);
                }
            }
            return targetDirection;
        }

        protected override void UpdateInputSideScroll(Vector3 axis)
        {
            Vector3 targetDirection = Vector3.zero;
            if (!this.IsControllable()) return;

            targetDirection = ComputeTargetDirection(axis);

            Camera maincam = this.GetMainCamera();
            if (maincam == null) return;

            this.ComputeMovement(targetDirection);

            float invertValue = (this.invertAxis ? -1 : 1);
            Vector3 moveDirection = Vector3.Scale(this.direction, axis * invertValue);

            moveDirection.Normalize();
            moveDirection *= this.direction.magnitude;

            CurrentSpeed = this.direction.magnitude;
        }

        protected virtual void Update()
        {
            base.Update();
        }
    }
}