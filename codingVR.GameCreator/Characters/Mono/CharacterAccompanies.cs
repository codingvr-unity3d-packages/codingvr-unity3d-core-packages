﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Scripting.APIUpdating;

namespace GameCreator.Characters
{
	[DisallowMultipleComponent]
	public class CharacterAccompanies : MonoBehaviour, codingVR.GameEngine.ICharacterAccompanies
	{
		// === ICharacterAccompanies =====================================================================================

		#region ICharacterAccompanies
		// last waypoint
		public bool HasReachedLastWaypoint
		{
			get
			{
				return waypointArray[waypointArray.Count - 1].HasReached;
			}
		}

		int FindItem(Transform waypoint)
		{
			return waypointArray.FindIndex(item => item.Transform.name == waypoint.name);
		}

		public void AddWaypoint(Transform waypoint, float radiusMin, float radiusMax, bool teleportationOverMaxRadius, float runSpeedMin, float runSpeedMax, Vector3 directionalOffset)
		{
			int index = FindItem(waypoint);
			if (index == -1)
			{
				WaypointStruct waypointStruct = new WaypointStruct(waypoint, radiusMin, radiusMax, teleportationOverMaxRadius, runSpeedMin, runSpeedMax, directionalOffset);
				waypointArray.Add(waypointStruct);
			}
		}

		public void ReplaceLastWaypoint(Transform waypoint, float radiusMin, float radiusMax, bool teleportationOverMaxRadius, float runSpeedMin, float runSpeedMax, Vector3 directionalOffset)
		{
			int index = FindItem(waypoint);
			if (index == -1)
			{
				WaypointStruct waypointStruct = new WaypointStruct(waypoint, radiusMin, radiusMax, teleportationOverMaxRadius, runSpeedMin, runSpeedMax, directionalOffset);
				LastWayPoint = waypointStruct;
			}
		}

		public void RemoveLastWaypoint()
		{
			if (waypointArray.Count > 1)
			{
				waypointArray.RemoveAt(waypointArray.Count - 1);
				if (waypointArray.Count > 1)
				{
					LastWayPoint.Reset();
				}
			}
		}

		public void ChangeLastWaypoint(float radiusMin, float radiusMax, bool teleportationOverMaxRadius, float runSpeedMin, float runSpeedMax, Vector3 directionalOffset)
		{
			if (waypointArray.Count > 1)
			{
				LastWayPoint.Set(radiusMin, radiusMax, teleportationOverMaxRadius, runSpeedMin, runSpeedMax, directionalOffset);
			}
		}

		// life cycle
		public bool Continue { get; set; } = true;


		#endregion

		// === WaypointStruct =====================================================================================

		#region WaypointStruct

		class WaypointStruct
		{
			bool initialized;
			bool hasReached; 
			Transform transform;
			Vector3 lastPosition;

			public float radiusMax;
			public float radiusMin;
			public bool teleportationOverMaxRadius;

			public float runSpeedMax;
			public float runSpeedMin;

			public Vector3 directionalOffset;

			public bool HasReached => hasReached;

			public Transform Transform => transform;

			public float RadiusMax => radiusMax;
			public float RadiusMin => radiusMin;

			public bool TeleportationOverMaxRadius => teleportationOverMaxRadius;

			public float RunSpeedMax => runSpeedMax;
			public float RunSpeedMin => runSpeedMin;

			public Vector3 DirectionalOffset => directionalOffset;

			public WaypointStruct(Transform transform, float radiusMin, float radiusMax, bool teleportationOverMaxRadius, float runSpeedMin, float runSpeedMax, Vector3 directionalOffset)
			{
				this.initialized = false;
				this.hasReached = false;
				this.transform = transform;
				this.lastPosition = Vector3.zero;

				this.radiusMin = radiusMin;
				this.radiusMax = radiusMax;
				this.teleportationOverMaxRadius = teleportationOverMaxRadius;

				this.runSpeedMin = runSpeedMin;
				this.runSpeedMax = runSpeedMax;

				this.directionalOffset = directionalOffset;
			}

			public void Reset()
			{
				this.initialized = false;
				this.hasReached = false;
				this.lastPosition = Vector3.zero;
			}

			public void Set(float radiusMin, float radiusMax, bool teleportationOverMaxRadius, float runSpeedMin, float runSpeedMax, Vector3 directionalOffset)
			{
				this.radiusMin = radiusMin;
				this.radiusMax = radiusMax;
				this.teleportationOverMaxRadius = teleportationOverMaxRadius;

				this.runSpeedMin = runSpeedMin;
				this.runSpeedMax = runSpeedMax;

				this.directionalOffset = directionalOffset;
			}

			Character GetCharacterFromWaypoint(Transform waypoint)
			{
				Character character = waypoint.GetComponent<Character>();
				return character;
			}

			public void Move(CharacterAccompanies characterAccompanies)
			{
				// compute speed from last pos
				var speed = transform.position - lastPosition;
				lastPosition = transform.position;

				// reset has rechaed
				hasReached = false;

				// first pass or continue
				if (initialized == true)
				{
					// this character
					var thisCharacter = characterAccompanies.GetComponent<Character>();
					// character to player vector
					var targetWaypointRight = transform.position.xz() + transform.right.xz() * DirectionalOffset.x + transform.forward.xz() * DirectionalOffset.z;
					Vector3 character2Waypoint = Vector3Extension.fromXZ(targetWaypointRight - characterAccompanies.transform.position.xz());
					// character to player vector direction
					Vector3 directionCharacter2Waypoint = character2Waypoint.normalized;

					/*
												player ---- minRadius --------- maxRadius ----------------------------------------
													|                    |                      |
											walking or stopping   running or walking    running or high speed running or teleport 

												player -- minRadius --------- maxRadius ------------------------------------------
													|                    |                      |
													-neg                 0                      1
					*/

					// character is in player area ?
					float characterIsOutsidePlayerArea = (character2Waypoint.magnitude - RadiusMin) / (RadiusMax - RadiusMin);
					characterIsOutsidePlayerArea = Mathf.Clamp(characterIsOutsidePlayerArea, -1.0f, 1.0f);

					// player is moving ?
					bool playerMoving = speed.magnitude > 0.0001 ? true : false;

					// by default character is waiting
					var speedFactor = Mathf.Clamp(characterIsOutsidePlayerArea, 0.0f, 1.0f);
					thisCharacter.characterLocomotion.runSpeed = speedFactor * RunSpeedMax + (1.0f - speedFactor) * RunSpeedMin;
					var selectedCharacter = GetCharacterFromWaypoint(transform);
					if (selectedCharacter != null)
						thisCharacter.characterLocomotion.canRun = selectedCharacter.characterLocomotion.canRun;
					thisCharacter.characterLocomotion.SetDirectionalDirection(Vector3.zero, null);

					// select direction
					void SelectDirection()
					{
						thisCharacter.characterLocomotion.SetDirectionalDirection(directionCharacter2Waypoint, null);
					}

					// behavior tree
					if (characterIsOutsidePlayerArea >= 1.0f)
					{
						// player is beyond max radius
						thisCharacter.characterLocomotion.canRun = true;
						// we run
						SelectDirection();
						// we teleport
						if (TeleportationOverMaxRadius == true)
						{
							thisCharacter.transform.position = thisCharacter.transform.position + directionCharacter2Waypoint * (character2Waypoint.magnitude);
						}
					}
					else
					{
						if (character2Waypoint.magnitude > 0.0f)
						{
							if (characterIsOutsidePlayerArea > 0.0f && playerMoving == true)
							{
								// char is beyond the minium radius and player is moving
								thisCharacter.characterLocomotion.canRun = true;
								// we are running
								SelectDirection();

								//Debug.LogError("running " + character2Player.magnitude + "run" + thisCharacter.characterLocomotion.canRun);
							}
							else
							if (characterIsOutsidePlayerArea > 0.0f && playerMoving == false)
							{
								// char is beyond the minium radius and player is sleeping
								// we are walking
								SelectDirection();

							}
							else if (characterIsOutsidePlayerArea <= 0.0f && playerMoving == true)
							{
								// char is beyond the minium radius and player is moving
								// we are walking
								thisCharacter.characterLocomotion.SetDirectionalDirection(directionCharacter2Waypoint, null);
								// we are stopping
								hasReached = true;
							}
							else
							if (characterIsOutsidePlayerArea <= 0.0f && playerMoving == false)
							{
								// char is below the minium radius and player is sleeping
								// we are stopping
								hasReached = true;
							}
						}
					}
				}
				else
				{
					initialized = true;
				}
			}

		}

		List<WaypointStruct> waypointArray = new List<WaypointStruct>();

		private WaypointStruct LastWayPoint
		{
			get
			{
				return waypointArray[waypointArray.Count - 1];
			}
			set
			{
				waypointArray[waypointArray.Count - 1] = value;
			}
		}

		#endregion

		// === UNITY_LIFECYCLE =====================================================================================

		#region UNITY_LIFECYCLE

		// Start is called before the first frame update
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{
			if (Continue == true)
			{
				if (waypointArray != null & waypointArray.Count > 0)
				{
					LastWayPoint.Move(this);
				}
			}
		}

		void OnDestroy()
		{
			var thisCharacter = GetComponent<Character>();
			thisCharacter.characterLocomotion.SetDirectionalDirection(Vector3.zero, null);
		}

        #if UNITY_EDITOR
        private void OnDrawGizmos()
		{
			DrawDebugGizmos();
		}
        #endif

#endregion

        // === IMPLEMENTATION =====================================================================================

        #region IMPLEMENTATION

#if UNITY_EDITOR
        void DrawDebugGizmos()
		{
			if (waypointArray.Count > 0)
			{
				var characterAccompanies = gameObject;

				// radius max
				{
					// solid
					{
						var alphaColor = Color.green;
						alphaColor.a = 0.25f;
						Handles.color = alphaColor;

						Handles.color = alphaColor;
						Handles.DrawSolidArc(
							characterAccompanies.transform.position,
							Vector3.up,
							characterAccompanies.transform.forward,
							360f,
							LastWayPoint.RadiusMax
						);
					}

					// wire
					{
						var alphaColor = Color.red;
						alphaColor.a = 0.5f;
						Handles.color = alphaColor;
						Handles.DrawWireArc(
							characterAccompanies.transform.position,
							Vector3.up,
							characterAccompanies.transform.forward,
							360f,
							LastWayPoint.RadiusMax
						);
					}
				}

				// radius min
				{
					// solid
					{
						var alphaColor = Color.blue;
						alphaColor.a = 0.25f;
						Handles.color = alphaColor;

						Handles.color = alphaColor;
						Handles.DrawSolidArc(
							characterAccompanies.transform.position,
							Vector3.up,
							characterAccompanies.transform.forward,
							360f,
							LastWayPoint.RadiusMin
						);
					}

					// wire
					{
						var alphaColor = Color.red;
						alphaColor.a = 0.5f;
						Handles.color = alphaColor;
						Handles.DrawWireArc(
							characterAccompanies.transform.position,
							Vector3.up,
							characterAccompanies.transform.forward,
							360f,
							LastWayPoint.RadiusMin
						);
					}
				}

				// link between waypoint and character
				{
					DrawGizmos.DrawArrow(characterAccompanies.transform.position, LastWayPoint.Transform.position, Color.red);
				}
			}
		}
        #endif

#endregion
    }
}
