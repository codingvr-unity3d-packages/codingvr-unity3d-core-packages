﻿
using FluidHTN;
using UnityEngine;

namespace codingVR.AI.HTN
{
	public abstract class AIDomainDefinition<AIContext> : ScriptableObject where AIContext : IContext
	{
		public abstract Domain<AIContext> Create();
	}
}