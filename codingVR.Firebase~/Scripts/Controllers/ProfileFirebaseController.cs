﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Storage;
using Firebase;
using Firebase.Extensions;
using Firebase.Firestore;

namespace EasyProfile
{
    public interface IProfileFirebaseController
    {
        /// <summary>
        /// Check if Firebase is inited
        /// </summary>
        /// <returns>Return true if Firebase is already inited</returns>
        bool IsFirebaseInited();

        /// <summary>
        /// Check if user is loggined
        /// </summary>
        /// <returns>Return true if user is already loggined</returns>
        bool IsUserLoggined();

        /// <summary>
        /// Initialization Firebase SDK
        /// </summary>
        /// <param name="_callback">Add initialization completion method</param>
        void InitFirebase(Action<CallbackInitFirebaseMessage> _callback = null);

        /// <summary>
        /// User authorization. Use users email and password to sign in
        /// </summary>
        /// <param name="_login">Users email</param>
        /// <param name="_password">Users password</param>
        /// <param name="_callback">Add sign in completion method</param> 
        void LogIn(string _login, string _password, Action<CallbackLoginMessage> _callback = null);

        /// <summary>
        /// User authorization. Use custom tokenID to sign in
        /// </summary>
        /// <param name="_tokenID">Custom token ID</param>
        /// <param name="_callback">Add sign in completion method</param>
        void LogInWithCustomTokenID(string _tokenID, Action<CallbackLoginMessage> _callback = null);
        /// <summary>
        /// User authorization. Use custom auth system to sign in
        /// </summary>
        /// <param name="_userID">Custom user ID</param>
        /// <param name="_callback">Add sign in completion method</param>
        void LogInWithCustomAuthSystem(string _userID, Action<CallbackLoginMessage> _callback = null);

        /// <summary>
        /// Log out from Firebase
        /// </summary>
        /// <param name="_callback">Add completion method</param>
        void LogOut(Action<CallbackLogOut> _callback = null);

        /// <summary>
        /// User registration. Use users email and password to register new user
        /// </summary>
        /// <param name="_email">User email</param>
        /// <param name="_password">User email</param>
        /// <param name="_callback">Add completion method</param>
        void RegisterNewUser(string _email, string _password, Action<CallbackRegistrationMessage> _callback);

        /// <summary>
        /// User registration. Use users custom tiken id to register new user
        /// </summary>
        /// <param name="_tokenID">Your custom token ID</param>
        /// <param name="_user">User data</param>
        /// <param name="_callback">Add completion method</param>
        void RegisterNewUserWithCustomTokenID(string _tokenID, User _user, Action<CallbackRegistrationMessage> _callback);

        /// <summary>
        /// User registration. Use custom auth system
        /// </summary>
        /// <param name="_userID">Custom User ID</param>
        /// <param name="_user">User data</param>
        /// <param name="_callback">Add completion method</param>
        void RegisterNewUserWithCustomAuthSystem(string _userID, User _user, Action<CallbackRegistrationMessage> _callback);

        /// <summary>
        /// Sends an account activation letter to the user in the mail. Use this method after successful user registration.
        /// </summary>
        /// <param name="_callback">Add completion method</param>
        void SendVerifcationEmail(Action<CallbackSendVerificationEmail> _callback = null);

        /// <summary>
        /// Sends a password reset email to the user.
        /// </summary>
        /// <param name="_mail">User email</param>
        /// <param name="_callback">Add completion method</param>
        void SendResetPasswordEmail(string _mail, Action<CallbackSendResetPasswordEmail> _callback = null);

        /// <summary>
        /// Create or set user data
        /// </summary>
        /// <param name="_user">User object</param>
        /// <param name="_callback">Add completion method</param>
        void SetUserData(User _user, Action<CallbackSetUserDataMessage> _callback = null);

        /// <summary>
        /// Get user data
        /// </summary>
        /// <param name="_userID">User ID</param>
        /// <param name="_callback">Add completion method</param>
        void GetUserData(string _userID, Action<CallbackGetUserDataMessage> _callback);

        /// <summary>
        /// Upload user avatar image to Firebase Storage. 
        /// </summary>
        /// <param name="_request">Request object. Use RequestUploadImage _request = new RequestUploadImage()</param>
        /// <param name="_callback">Add upload completion method</param>
        void UploadAvatar(RequestUploadImage _request, Action<CallBackUploadImage> _callback = null);

        /// <summary>
        /// Get dowload url of user avatar
        /// </summary>
        /// <param name="_userID">User ID</param>
        /// <param name="_size">Avatar size</param>
        /// <param name="_callback">Add completion method</param>
        void GetProfileImageURL(string _userID, ImageSize _size, Action<CallbackGetProfileImageURL> _callback);

        /// <summary>
        /// Get user avatar image from Firebase Storage.
        /// </summary>
        /// <param name="_request">Request object. Use RequestGetProfileImage _request = new RequestGetProfileImage()</param>
        /// <param name="_callback">Add completion method</param>
        void GetProfileImage(RequestGetProfileImage _request, Action<CallbackGetProfileImage> _callback = null);

        /// <summary>
        /// Get the full name of user by id.
        /// </summary>
        /// <param name="_userID">User ID</param>
        /// <param name="_callback">Add completion method</param>
        void GetUserFullName(string _userID, Action<string> _callback);

        /// <summary>
        /// Set user full name by user ID.
        /// </summary>
        /// <param name="_userID">User ID</param>
        /// <param name="_value">Full name value</param>
        /// <param name="_callback">Add completion method</param>
        void SetUserFullName(string _userID, string _value, Action<CallbackSetUserValue> _callback = null);

        /// <summary>
        /// Get users last name by user ID.
        /// </summary>
        /// <param name="_callback">Add completion method</param>
        void GetUserLastName(string _userID, Action<string> _callback);

        /// <summary>
        /// Set users last name by user ID
        /// </summary>
        /// <param name="_userID">User ID</param>
        /// <param name="_value">Last name value</param>
        /// <param name="_callback">Add completion method</param>
        void SetUserLastName(string _userID, string _value, Action<CallbackSetUserValue> _callback = null);

        /// <summary>
        /// Get the first name of user by id.
        /// </summary>
        /// <param name="_userID">User ID</param>
        /// <param name="_callback">Add completion method</param>
        void GetUserFirstName(string _userID, Action<string> _callback);

        /// <summary>
        /// Set users first name by user ID
        /// </summary>
        /// <param name="_userID">User ID</param>
        /// <param name="_value">First name value</param>
        /// <param name="_callback"></param>
        void SetUserFirstName(string _userID, string _value, Action<CallbackSetUserValue> _callback = null);

        /// <summary>
        /// Get the nick name of user by id.
        /// </summary>
        /// <param name="_userID">User ID</param>
        /// <param name="_callback">Add completion method</param>
        void GetUserNickName(string _userID, Action<string> _callback);

        /// <summary>
        /// Set user nick name by user ID
        /// </summary>
        /// <param name="_userID">User ID</param>
        /// <param name="_value">Nick name value</param>
        /// <param name="_callback">Add completion method</param>
        void SetUserNickName(string _userID, string _value, Action<CallbackSetUserValue> _callback = null);

        /// <summary>
        /// Get user registration date by user ID
        /// </summary>
        /// <param name="_userID">User ID</param>
        /// <param name="_callback">Add completion method</param>
        void GetUserRegistrationDate(string _userID, Action<string> _callback);

        /// <summary>
        /// Get user phone number date by user ID
        /// </summary>
        /// <param name="_userID">User ID</param>
        /// <param name="_callback">Add completion method</param>
        void GetUserPhone(string _userID, Action<string> _callback);

        /// <summary>
        /// Set user phone
        /// </summary>
        /// <param name="_userID">User ID</param>
        /// <param name="_value">Phone value</param>
        /// <param name="_callback">Add completion method</param>
        void SetUserPhone(string _userID, string _value, Action<CallbackSetUserValue> _callback = null);

        /// <summary>
        /// Get all user of Firebase Database
        /// </summary>
        /// <param name="_callback">Add completion method</param>
        void GetAllUsers(Action<CallbackGetAllUsers> _callback);

        /// <summary>
        /// Search users by string value. You can search for users by name, nickname or phone number.
        /// </summary>
        /// <param name="_searchValue">Search Value</param>
        /// <param name="_callback">Add completion method</param>
        void SearchUsers(string _search, Action<CallbackSearchUsers> _callback);

        /// <summary>
        /// Set user custom value by user ID. All custom values must be configured in plugin settings (EasySystems->EasyProfile->Settings)
        /// </summary>
        /// <param name="_userID">User ID</param>
        /// <param name="_valueID">Custom value id</param>
        /// <param name="_value">Custom value</param>
        /// <param name="_callback">Add completion method</param>
        void SetUserCustomValue(string _userID, string _valueID, object _value, Action<CallbackSetUserCustomValue> _callback = null);

        /// <summary>
        /// Get user custom value by user ID. All custom values must be configured in plugin settings (EasySystems->EasyProfile->Settings)
        /// </summary>
        /// <param name="_userID">User ID</param>
        /// <param name="_valueID">Value ID</param>
        /// <param name="_callback">Add completion method</param>
        void GetUserCustomValue(string _userID, string _valueID, Action<CallbackGetUserCustomValue> _callback);

        /// <summary>
        /// Update user last activity value
        /// </summary>
        /// <param name="_callback">Add completion method</param>
        void UpdateUserActivity(Action<CallbackSetUserActivity> _callback = null);

        /// <summary>
        /// Get current server time. Availbale when user is logined
        /// </summary>
        /// <param name="_callback">Add completion method</param>
        void GetServerTimestamp(Action<CallbackGetServerTimestamp> _callback);

        /// <summary>
        /// Get user last activity by user id.
        /// </summary>
        /// <param name="_userID">User ID</param>
        /// <param name="_callback">Add completion method</param>
        void GetUserLastActivity(string _userID, Action<CallbackGetUserActivity> _callback);
    }
}

