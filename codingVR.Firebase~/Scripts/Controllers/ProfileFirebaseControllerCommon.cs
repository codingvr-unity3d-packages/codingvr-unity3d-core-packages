﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Storage;
using Firebase;
using Firebase.Extensions;

namespace EasyProfile
{
    /// <summary>
    /// Login callback class
    /// </summary>
    public class CallbackLoginMessage
    {
        public bool IsSuccess;
        public string ErrorMessage;
        public ProfileMessageCode MessageCode;
        public string UserID;
        public UserCredentials Gredentials;
        public FirebaseUser FUser;
    }

    /// <summary>
    /// Registration callback class
    /// </summary>
    public class CallbackRegistrationMessage
    {
        public bool IsComplete;
        public string ErrorMessage;
        public string UserID;
    }

    /// <summary>
    /// Set user data callback class
    /// </summary>
    public class CallbackSetUserDataMessage
    {
        public bool IsSuccess;
        public string ErrorMessage;
        public string UserID;
    }

    /// <summary>
    /// Set user custom valuse callback class
    /// </summary>
    public class CallbackSetUserValue
    {
        public bool IsSuccess;
        public string ErrorMessage;
    }

    /// <summary>
    /// Get user data callback class
    /// </summary>
    public class CallbackGetUserDataMessage
    {
        public bool IsSuccess;
        public User UserData;
    }

    /// <summary>
    /// Init Firebase callback class
    /// </summary>
    public class CallbackInitFirebaseMessage
    {
        public bool IsSuccess;
    }

    /// <summary>
    /// Upload image request class
    /// </summary>
    public class RequestUploadImage
    {
        public Texture2D Texture;
        public string OwnerId;
    }

    /// <summary>
    /// Upload image callback class
    /// </summary>
    public class CallBackUploadImage
    {
        public bool IsFinish = false;
        public bool IsSuccess = false;
    }

    /// <summary>
    /// Get profile image request class
    /// </summary>
    public class RequestGetProfileImage
    {
        public string Id;
        public ImageSize Size;
    }

    /// <summary>
    /// Get profile image callback class
    /// </summary>
    public class CallbackGetProfileImage
    {
        public bool IsSuccess;
        public byte[] ImageBytes;
        public string DownloadUrl;
    }

    /// <summary>
    /// Get profile image callback class
    /// </summary>
    public class CallbackGetProfileImageURL
    {
        public bool IsSuccess;
        public string DownloadUrl;
    }

    /// <summary>
    /// Log out callback class
    /// </summary>
    public class CallbackLogOut
    {
        public bool IsSuccess;
    }

    /// <summary>
    /// Get all user callback class
    /// </summary>
    public class CallbackGetAllUsers
    {
        public bool IsSuccess;
        public List<User> Users;
    }

    /// <summary>
    /// Search users callback class
    /// </summary>
    public class CallbackSearchUsers
    {
        public bool IsSuccess;
        public List<User> Users;
    }

    /// <summary>
    /// Send verification email class
    /// </summary>
    public class CallbackSendVerificationEmail
    {
        public bool IsSuccess;
    }

    /// <summary>
    /// Send reset password email
    /// </summary>
    public class CallbackSendResetPasswordEmail
    {
        public bool IsSuccess;
    }

    /// <summary>
    /// Set user custom value callback class
    /// </summary>
    public class CallbackSetUserCustomValue
    {
        public bool IsSuccess;
        public string ErrorMessage;
        public string UserID;
        public string ValueID;
    }

    /// <summary>
    /// Get user custom value callback class
    /// </summary>
    public class CallbackGetUserCustomValue
    {
        public bool IsSuccess;
        public string ErrorMessage;
        public string UserID;
        public string ValueID;
        public CustomValue CustomValue;
    }

    /// <summary>
    /// Set user activity callback class
    /// </summary>
    public class CallbackSetUserActivity
    {
        public bool IsSuccess;
        public string ErrorMessage;
    }

    /// <summary>
    /// Get server time stamp callback class
    /// </summary>
    public class CallbackGetServerTimestamp
    {
        public bool IsSuccess;
        public string ErrorMessage;
        public string Data;
    }

    /// <summary>
    /// Get user activity callback class
    /// </summary>
    public class CallbackGetUserActivity
    {
        public bool IsSuccess;
        public string ErrorMessage;
        public string Data;
    }

    /// <summary>
    /// Custom value serializable class
    /// </summary>
    [System.Serializable]
    public class CustomValue
    {
        public string ValueID;
        public CustomValueType ValueType;

        [HideInInspector]
        public int IntValue;
        [HideInInspector]
        public string StringValue;
        [HideInInspector]
        public float FloatValue;
    }

    /// <summary>
    /// Enum for custom user value type
    /// </summary>
    public enum CustomValueType
    {
        String,
        Int,
        Float
    }

    /// <summary>
    /// Enum for texture resizing 
    /// </summary>
    public enum ImageSize
    {
        Origin = 0,
        Size_1024 = 1024,
        Size_512 = 512,
        Size_256 = 256,
        Size_128 = 128
    }

}

