﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using TypeReferences;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Zenject;

namespace codingVR.Data
{
	// ===== InventoryScriptable =============================================

	#region InventoryScriptable

	[CreateAssetMenu(fileName = "Inventory", menuName = "CodingVR/Data/Inventory", order = 10)]
	public class InventoryScriptable : ScriptableObject
	{
		// === DATA-UI ===

		#region DATA-UI

		[SerializeField]
		string idPlayer;

		[SerializeField]
		int money;

		[SerializeField]
		List<ItemScriptable> inventoryItems = new List<ItemScriptable>();

		[SerializeReference]
		List<ItemScriptable> itemsEquiped;

		#endregion

		// === DATA-INTERNAL =========================================================

		#region DATA-INTERNAL

		Scene.IResourcesLoader resourcesLoader;
		Data.INetworkSession<Data.InventoryScriptable.InventoryStrucSpec, Data.InventoryScriptable.InventoryStrucSpec> networkSession;

		bool isDirty = false;
		bool preventDirtyFlag = false;

		#endregion
		
		// === INTERFACE ===

		#region INTERFACE

		[SerializeField]
		public string IdPlayer
		{
			get { return idPlayer; }
			set { idPlayer = value; }
		}

		public bool IsDirty
		{
			get
			{
				return isDirty;
			}

			set
			{
				if (preventDirtyFlag == false)
				{
					isDirty = value;
				}
			}
		}

		public int Money
		{
			get
			{
				return money;
			}
			set
			{
				money = value;
				IsDirty = true;
			}
		}

		public void AddInventoryItem(ItemScriptable i)
		{
			inventoryItems.Add(i);
			IsDirty = true;
		}

		public void RemoveInventoryItem(ItemScriptable i)
		{
			inventoryItems.Remove(i);
			IsDirty = true;
		}

		public void AddItemEquiped(ItemScriptable i)
		{
			itemsEquiped.Add(i);
			IsDirty = true;
		}

		public void SetInventoryItems(List<ItemScriptable> inventoryItems)
		{
			this.inventoryItems = inventoryItems;
			IsDirty = true;
		}

		public void SetItemsEquiped(List<ItemScriptable> equipedItems)
		{
			this.itemsEquiped = inventoryItems;
			IsDirty = true;
		}

		public void SetItemEquiped(int index, ItemScriptable i)
		{
			itemsEquiped[index] = i;
		}

		public ReadOnlyCollection<ItemScriptable> ItemsEquiped => itemsEquiped.AsReadOnly();

		public ReadOnlyCollection<ItemScriptable> InventoryItems => inventoryItems.AsReadOnly();

		#endregion

		// === UNITY-LIFECYCLE =========================================================

		#region UNITY-LIFECYCLE

		[Inject]
		public void Constructor(Scene.IResourcesLoader resourcesLoader, Data.INetworkSession<Data.InventoryScriptable.InventoryStrucSpec, Data.InventoryScriptable.InventoryStrucSpec> networkSession)
		{
			// interface
			this.resourcesLoader = resourcesLoader;
			this.networkSession = networkSession;
		}

		public void OnValidate()
		{
			IsDirty = true;
		}

		#endregion


		// === IMPLEMENTATION =========================================================

		#region IMPLEMENTATION

		/*
		 * 
		 * Id
		 * 
		 */

		static public string GetIdPlayer(string id)
		{
			return id.Split('.')[0];
		}

		static public string MakeId(string idPlayer, string domain)
		{
			return idPlayer + "." + domain;
		}
			   
		/*
		 * 
		 * Network
		 * 
		 */

		IEnumerator PostData(string idPlayer, string domain)
		{
			bool done = false;
			void DoneSet(InventoryStrucSpec d)
			{
				done = true;
				IsDirty = false;
			}
			// create data ready for serialisation
			this.IdPlayer = idPlayer;
			var set = InventoryStrucSpec.Create(domain, this);
			networkSession.Post(set, DoneSet);
			yield return new WaitUntil(() => done == true);
		}

		public IEnumerator PutData(string idPlayer, string domain, Data.DoneGet<Data.InventoryScriptable> doneSet = null)
		{
			bool done = false;
			void DoneSet(InventoryStrucSpec d)
			{
				doneSet?.Invoke(this);
				IsDirty = false;
				done = true;
			}

			// create data ready for serialisation
			this.IdPlayer = idPlayer;
			var set = InventoryStrucSpec.Create(domain, this);
			networkSession.Put(set, DoneSet);
			// wait 
			yield return new WaitUntil(() => done == true);
		}

		public IEnumerator GetData(string idPlayer, string domain, Data.DoneGet<Data.InventoryScriptable> doneSet)
		{
			int done = 0;

			InventoryStrucSpec dataRef = null;
			void LocalDoneSet(InventoryStrucSpec _dataRef)
			{
				// check if user found !!!
				if (_dataRef != null)
				{
					done = 1;
					dataRef = _dataRef;
				}
				else
				{
					done = -1;
				}
			}

			// load data asynchrnously
			bool b = networkSession.GetResult(MakeId(idPlayer, domain), LocalDoneSet);
			yield return new WaitUntil(() => done != 0);
			if (done == -1)
			{
				yield return PostData(idPlayer, domain);
			}
			else
			{
				// make items asynchrnously
				var task = dataRef.Make(resourcesLoader, this);
				yield return new WaitUntil(() => task.IsCompleted);
			}
			doneSet?.Invoke(this);
			yield return 0;
		}

		#endregion
		
		// ===== Data stucture used for managing transfert by client/server ======

		#region InventoryStrucSpec

		[System.Serializable]
		public class InventoryStrucSpec : IGraphDataBase, ISerializationCallbackReceiver
		{
			static string currentVersion = "1";

			[SerializeField]
			string version = currentVersion;

			[SerializeField]
			int money;

			// asset references
			[SerializeField]
			List<AssetReference> assetInventoryItemReferences = new List<AssetReference>();

			[SerializeField]
			List<AssetReference> assetItemsEquipedReferences = new List<AssetReference>();

			/*
			 * 
			 * Gateway to convert items from serveur / client
			 * 
			 */


			/*
				   // NATIVE VERSION	

				   public async static Task<ItemScriptable> LoadAssetItem(AssetReference asset)
				   {
					   var res = asset.LoadAssetAsync<ItemScriptable>();
					   return await res.Task;
				   }

				   static async Task<List<ItemScriptable>> ConvertItems(List<AssetReference> from)
				   {
					   List<ItemScriptable> refs = new List<ItemScriptable>();
					   foreach (var it in from)
					   {
						   var asset = await LoadAssetItem(it);
						   refs.Add(asset);
					   }
					   return refs;
				   }
		   */

			static async Task<List<ItemScriptable>> ConvertItems(Scene.IResourcesLoader resourceLoader, List<AssetReference> from)
			{
				List<ItemScriptable> refs = new List<ItemScriptable>();

				var unboundResources = resourceLoader.FindUnboundResources(from);
				resourceLoader.LoadResourcesAsset(unboundResources);
				await resourceLoader.WaitASync(unboundResources);

				foreach (var it in from)
				{
					refs.Add(resourceLoader.GetResource(it) as ItemScriptable);
				}
				return refs;
			}

			static List<AssetReference> ConvertItems(List<ItemScriptable> from)
			{
				List<AssetReference> refs = new List<AssetReference>();
				foreach (var it in from)
				{
					refs.Add(it.AssetReference);
				}
				return refs;
			}

			/*
			 * 
			 * JSON I/O
			 * 
			 */


			public override string ToString()
			{
				return JsonUtility.ToJson(this, true);
			}

			static public InventoryStrucSpec FromString(string json)
			{
				var spec = new InventoryStrucSpec();
				spec = JsonUtility.FromJson<InventoryStrucSpec>(json);
				return spec;
			}

			/*
			 * 
			 * Make data
			 * 
			 */

			public async Task Make(Scene.IResourcesLoader resourceLoader, InventoryScriptable inventoryScriptable)
			{
				var assetItemReferencesAsync = await ConvertItems(resourceLoader, this.assetInventoryItemReferences);
				var assetItemsEquipedReferencesAsync = await ConvertItems(resourceLoader, this.assetItemsEquipedReferences);

				inventoryScriptable.IdPlayer = InventoryScriptable.GetIdPlayer(this.Id);
				inventoryScriptable.money = this.money;
				inventoryScriptable.inventoryItems = assetItemReferencesAsync;
				inventoryScriptable.itemsEquiped = assetItemsEquipedReferencesAsync;
			}

			static public InventoryStrucSpec Create(string domain, InventoryScriptable inventoryScriptable)
			{
				var inventoryItemsReferences = ConvertItems(inventoryScriptable.inventoryItems);
				var itemsEquipedReferences = ConvertItems(inventoryScriptable.itemsEquiped);

				return new InventoryStrucSpec
				{
					Id = InventoryScriptable.MakeId(inventoryScriptable.IdPlayer, domain),
					money = inventoryScriptable.money,
					assetInventoryItemReferences = inventoryItemsReferences,
					assetItemsEquipedReferences = itemsEquipedReferences
				};
			}

			/*
			 * 
			 * LifeCycle
			 * 
			 */

			public void OnBeforeSerialize()
			{
			}

			public void OnAfterDeserialize()
			{
				if (version == "2")
				{
				}
			}
		}

		#endregion
	}

	#endregion
}
