﻿using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine;
using UnityEngine.Serialization;
using System.Collections.ObjectModel;
using System.Collections;
using Zenject;
using System.Reflection;

namespace codingVR.Data
{
	[CreateAssetMenu(fileName = "QuestionData", menuName = "CodingVR/Data/QuestionData", order = 1)]
	public class QuestionDataScriptable : IGraphDataBaseScriptable<QuestionDataScriptable, QuestionDataScriptable.DataStruct>, Data.IActivityFieldServer 
	{
		public INetworkSession<codingVR.Data.GetGraphDataLinkSpec, codingVR.Data.SetGraphDataLinkResult> networkSession;

		// === DATA-INTERNAL ===

		#region DATA-INTERNAL

		bool isDirty = false;

		#endregion

		// === DataStruct ===

		#region DataStruct

		//Datastructure for storing the quetions data
		[System.Serializable]
		public class DataStruct : ActivityFieldManager
		{
			// === Interface ===

			// data
			[SerializeField]
			public string questionInfo;         //question text
			[SerializeField]
			public List<string> options;        //options to select
			[SerializeField]
			public string answer;

			// === Implementation ===

			public string Answer
			{
				get
				{
					return answer;
				}
				set
				{
					if (value != answer)
					{
						answer = value;
					}
				}
			}

			public string Title
			{
				get
				{
					return questionInfo;
				}
				set
				{
					if (value != questionInfo)
					{
						questionInfo = value;
					}
				}
			}

			public string Tag
			{
				get
				{
					return questionInfo;
				}
				set
				{
					if (value != questionInfo)
					{
						questionInfo = value;
					}
				}
			}

			public List<string> Options
			{
				get
				{
					return options;
				}
				set
				{
					if (value != options)
					{
						options = value;
					}
				}
			}

			// === ActivityFieldManager ===

			public Dictionary<string, FieldInfo> Fields
			{
				get
				{
					Dictionary<string, FieldInfo> fieldsCasted = new Dictionary<string, FieldInfo>();
					foreach (var it in base.Fields)
					{
						fieldsCasted.Add(it.Key, it.Value.fieldInfo);
					}
					return fieldsCasted;
				}
			}

			public IEnumerator GetEnumerator()
			{
				return base.GetEnumerator();
			}

			public void PopulateFields()
			{
				base.PopulateFields();
			}
		}

		#endregion

		// === DATA-UI ===

		#region DATA-UI

		[FormerlySerializedAs("question")]
		[SerializeField]
		DataStruct data =  new DataStruct();

		#endregion

		// === LIFECYCLE ===

		#region LIFECYCLE

		[Inject]
		void Construct(INetworkSession<codingVR.Data.GetGraphDataLinkSpec, codingVR.Data.SetGraphDataLinkResult> networkSession)
		{
			// global interfaces
			this.networkSession = networkSession;
		}

		void OnEnable()
		{
		}

		void OnChange()
		{
			IsDirty = true;
		}
		
		#endregion

		// === IMPLEMENTATION ===

		#region IMPLEMENTATION

		public void SetTag(string tag)
		{
			data.Tag = tag;
		}

		#endregion

		// === IGraphDataBaseScriptable ===

		#region IGraphDataBaseScriptable

		override public DataStruct Data
		{
			get
			{
				return data;
			}
			set
			{
				if (value != data)
				{
					data = value;
				}
			}
		}

		override public string Title
		{
			get
			{
				return data.Title;
			}
			set
			{
				if (value != data.Title)
				{
					data.Title = value;
					OnChange();
				}
			}
		}

		override public string Tag
		{
			get
			{
				return data.Tag;
			}
		}

		override public string Answer
		{
			get
			{
				return data.Answer;
			}
			set
			{
				if (value != data.Answer)
				{
					data.Answer = value;
					OnChange();
				}
			}
		}

		override public ReadOnlyCollection<string> Options
		{
			get
			{
				return data.Options.AsReadOnly();
			}
		}
		
		override public void AddOptionsRange(string[] range)
		{
			data.Options.AddRange(range);
			OnChange();
		}

		#endregion

		// === INetworkClient ===

		#region INetworkClient
		
		/*
		 * 
		 * Data Specification
		 * 
		 */

		public IEnumerator GetDataSpec(Data.DoneGet<Data.QuestionDataScriptable> doneSet = null)
		{
			Data.QuestionDataScriptable scriptable = this;
			if (scriptable != null)
			{
				bool done = false;

				void LocalDoneSet(Data.GetGraphDataLinkSpec dataRef)
				{
					// check if user found !!!
					if (dataRef != null)
					{
						dataRef.Make(scriptable);
						doneSet?.Invoke(scriptable);
					}
					// needed for stop corountine
					done = true;
				}

				// load data
				bool ret = networkSession.GetSpec(scriptable.Tag, LocalDoneSet);
				yield return new WaitUntil(() => done == true || ret == false);

				if (ret == false)
				{
					yield return PostDataSpec();
				}
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}

		public IEnumerator PostDataSpec()
		{
			Data.QuestionDataScriptable scriptable = this;
			if (scriptable != null)
			{
				bool done = false;
				void DoneSet(Data.GetGraphDataLinkSpec spec)
				{
					done = true;
				}
				var set = GetGraphDataLinkSpec.Create(scriptable);
				//var json = JsonUtility.ToJson(set, true);
				networkSession.PostSpec(set, DoneSet);
				yield return new WaitUntil(() => done == true);
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}

		/*
		 * 
		 * Data Result
		 * 
		 */

		public IEnumerator PutData()
		{
			Data.QuestionDataScriptable scriptable = this;
			if (scriptable != null)
			{
				bool done = false;
				void DoneSet(Data.SetGraphDataLinkResult spec)
				{
					done = true;
				}
				var set = SetGraphDataLinkResult.Create(scriptable);
				//var json = JsonUtility.ToJson(set, true);
				networkSession.Put(set, DoneSet);
				yield return new WaitUntil(() => done == true);
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}

		public IEnumerator PostData()
		{
			Data.QuestionDataScriptable scriptable = this;
			if (scriptable != null)
			{
				bool done = false;
				void DoneSet(Data.SetGraphDataLinkResult spec)
				{
					done = true;
				}
				var set = SetGraphDataLinkResult.Create(scriptable);
				//var json = JsonUtility.ToJson(set, true);
				networkSession.Post(set, DoneSet);
				yield return new WaitUntil(() => done == true);
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}

		public IEnumerator GetData(bool forceCreation, Data.DoneGet<Data.QuestionDataScriptable> doneSet = null)
		{
			Data.QuestionDataScriptable scriptable = this;
			if (scriptable != null)
			{
				bool done = false;

				void LocalDoneSet(Data.SetGraphDataLinkResult dataRef)
				{
					// check if user found !!!
					if (dataRef != null)
					{
						dataRef.Make(scriptable);
						doneSet?.Invoke(scriptable);
					}
					// needed for stop corountine
					done = true;
				}

				// load data
				bool ret = networkSession.GetResult(SetGraphDataLinkResult.MakeId(IGraphDataBase.IdPlayer, scriptable.Tag), LocalDoneSet);
				yield return new WaitUntil(() => done == true || ret == false);

				if (ret == false && forceCreation == true)
				{
					yield return PostData();
				}
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}

		#endregion

		// === IActivityField ===

		#region IActivityField

		public bool SetValue(string name, object value, Data.FieldSearchMethod method)
		{
			bool b = codingVR.Data.ActivityFieldUtilities.SetValue(this.Data, Tag, this, name, value, method);
			if (b == true)
			{
				OnChange();
			}
			return b;
		}

		public bool AddValue(string name, object value, FieldSearchMethod method)
		{
			bool b = codingVR.Data.ActivityFieldUtilities.AddValue(this.Data, Tag, this, name, value, method);
			if (b == true)
			{
				OnChange();
			}
			return b;
		}

		public bool SubValue(string name, object value, FieldSearchMethod method)
		{
			bool b = codingVR.Data.ActivityFieldUtilities.SubValue(this.Data, Tag, this, name, value, method);
			if (b == true)
			{
				OnChange();
			}
			return b;
		}

		public bool DivValue(string name, object value, FieldSearchMethod method)
		{
			bool b = codingVR.Data.ActivityFieldUtilities.DivValue(this.Data, Tag, this, name, value, method);
			if (b == true)
			{
				OnChange();
			}
			return b;
		}

		public bool MulValue(string name, object value, FieldSearchMethod method)
		{
			bool b = codingVR.Data.ActivityFieldUtilities.MulValue(this.Data, Tag, this, name, value, method);
			if (b == true)
			{
				OnChange();
			}
			return b;
		}

		public bool ResetValue(string name, FieldSearchMethod method)
		{
			bool b = codingVR.Data.ActivityFieldUtilities.ResetValue(this.Data, Tag, this, name, method);
			if (b == true)
			{
				OnChange();
			}
			return b;
		}

		public object GetValue(string name, FieldSearchMethod method)
		{
			return codingVR.Data.ActivityFieldUtilities.GetValue(this.Data, Tag, this, name, method);
		}

		public (bool, bool) CompareValue(string name, object value, FieldComparison comparison, FieldSearchMethod method)
		{
			return codingVR.Data.ActivityFieldUtilities.CompareValue(this.Data, Tag, this, name, value, comparison, method);
		}

		#endregion

		// === IActivityFieldServer ===

		#region  IActivityFieldServer

		public bool IsDirty
		{
			get
			{
				if (isDirty == true)
				{
					isDirty = false;
					return true;
				}
				else
				{
					return false;
				}
			}

			set
			{
				isDirty = value;
			}
		}

		public string GetFullPathField(string fieldName)
		{
			return ActivityFieldUtilities.GetFullPathField(Tag, fieldName);
		}

		public string Id
		{
			get
			{
				return Tag;
			}
			set
			{
				SetTag(value);
			}
		}

		public Dictionary<string, FieldInfo> Fields
		{
			get
			{
				return data.Fields;
			}
		}

		public IEnumerator GetEnumerator()
		{
			return data.GetEnumerator();
		}

		public void PopulateFields()
		{
			data.PopulateFields();
		}

		#endregion
	}

	// Data stucture used for managing transfert by client/server

	[System.Serializable]
	public class GetGraphDataLinkSpec : IGraphDataBase
	{
		[SerializeField]
		public List<string> nexts;

		/*
		 * 
		 * JSON I/O
		 * 
		 */

		public override string ToString()
		{
			return JsonUtility.ToJson(this, true);
		}

		static public GetGraphDataLinkSpec FromString(string json)
		{
			var spec = new GetGraphDataLinkSpec();
			spec = JsonUtility.FromJson<GetGraphDataLinkSpec>(json);
			return spec;
		}
				
		/*
		 * 
		 * Make Data
		 * 
		 */

		public void Make(QuestionDataScriptable data)
		{
			data.Data.Tag = this.Id;
			data.Data.Options = this.nexts;
		}

		static public GetGraphDataLinkSpec Create(QuestionDataScriptable data)
		{
			return new GetGraphDataLinkSpec
			{
				Id = data.Data.Tag,
				nexts = data.Data.Options
			};
		}
	}

	[System.Serializable]
	public class SetGraphDataLinkResult : IGraphDataBase
	{
		[SerializeField]
		string answer;

		/*
		 * 
		 * JSON I/O
		 * 
		 */

		public override string ToString()
		{
			return JsonUtility.ToJson(this, true);
		}

		static public SetGraphDataLinkResult FromString(string json)
		{
			var spec = new SetGraphDataLinkResult();
			spec = JsonUtility.FromJson<SetGraphDataLinkResult>(json);
			return spec;
		}

		/*
		 * 
		 * Make Data
		 * 
		 */

		public void Make(QuestionDataScriptable scriptable)
		{
			string idPlayer;
			string tag;
			(idPlayer, tag) = ParseId(Id);
			scriptable.SetTag(tag);
			scriptable.Answer = answer;
		}

		static public SetGraphDataLinkResult Create(QuestionDataScriptable data)
		{
			return new SetGraphDataLinkResult
			{
				Id = MakeId(IdPlayer, data.Tag),
				answer = data.Answer
			};
		}
	}
}
