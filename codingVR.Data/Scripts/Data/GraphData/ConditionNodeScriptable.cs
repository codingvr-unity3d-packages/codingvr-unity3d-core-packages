﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Data
{
    [CreateAssetMenu(fileName = "ConditionNode", menuName = "CodingVR/Data/ConditionNode", order = 5)]
    public class ConditionNodeScriptable : ScriptableObject
    {
        public int indexCondition;
        public List<string> optionsCondition;


        public bool CheckCondition(Data.GraphDataNodeGroupScriptable.GraphDataNodeGroupItemScriptable groupItem)
        {
            foreach(string option in optionsCondition)
            {
                if(option == groupItem.graphDataNodeList[indexCondition].Answer)
                {
                    return true;
                }
            }

            return false;
        }
    }
}