﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameCreator.Core;

namespace codingVR.Data
{
    [CreateAssetMenu(fileName = "GraphDataNodeGroup", menuName = "CodingVR/Data/GraphDataNodeGroup", order = 4)]
    public class GraphDataNodeGroupScriptable : ScriptableObject
    {
        public string elementToAdd;
        [HideInInspector]
        public int nbLoop;

        public List<ConditionNodeScriptable> listConditions;
        [HideInInspector]
        public List<Actions> actions;

        [System.Serializable]
        public class GraphDataNodeGroupItemScriptable
        {
            public List<GraphDataNodeScriptable> graphDataNodeList = new List<GraphDataNodeScriptable>();
        }

        public List<GraphDataNodeGroupItemScriptable> graphDataNodeGroupList;

    }

   
}