﻿using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections;
using Zenject;
using System.Reflection;

namespace codingVR.Data
{
	// Question type

	[System.Serializable]
	public enum GraphDataNodeType
	{
		LIST,
		OPEN,
	}

	// DataStructure encapsulated in scriptable object

	[CreateAssetMenu(fileName = "GraphDataNode", menuName = "CodingVR/Data/GraphDataNode", order = 1)]
	public class GraphDataNodeScriptable : IGraphDataBaseScriptable<GraphDataNodeScriptable, GraphDataNodeScriptable.DataStruct>, INetworkClient<GraphDataNodeScriptable>, Data.IActivityFieldServer
	{
		public INetworkSession<codingVR.Data.GetGraphDataNodeSpec, codingVR.Data.SetGraphDataNodeResult> networkSession;

		// === DataStruct ===

		#region DataStruct

		[System.Serializable]
		public class DataStruct : ActivityFieldManager
		{
			// == interface ==

			// data
			[SerializeField]
			public string question;         //question text
			[SerializeField]
			public GraphDataNodeType questionType;   //type
			[SerializeField]
			public List<string> options = new List<string>();        //options to select
			[SerializeField]
			public string answer;

			// === Implementation ===

			public string Answer
			{
				get
				{
					return answer;
				}
				set
				{
					if (value != answer)
					{
						answer = value;
					}
				}
			}

			public string Title
			{
				get
				{
					return question;
				}
				set
				{
					if (question != value)
					{
						question = value;
					}
				}
			}

			public GraphDataNodeType QuestionType
			{
				get
				{
					return questionType;
				}
				set
				{
					if (questionType != value)
					{
						questionType = value;
					}
				}
			}

			public List<string> Options
			{
				get
				{
					return options;
				}
				set
				{
					if (options != value)
					{
						options = value;
					}
				}
			}

			// === ActivityFieldManager ===

			public Dictionary<string, FieldInfo> Fields
			{
				get
				{
					Dictionary<string, FieldInfo> fieldsCasted = new Dictionary<string, FieldInfo>();
					foreach (var it in base.Fields)
					{
						fieldsCasted.Add(it.Key, it.Value.fieldInfo);
					}
					return fieldsCasted;
				}
			}

			public IEnumerator GetEnumerator()
			{
				return base.GetEnumerator();
			}

			public void PopulateFields()
			{
				base.PopulateFields();
			}

		}

		#endregion

		// === DATA-UI ===

		#region DATA-UI

		[SerializeField]
		public string tag;

		[SerializeField]
		DataStruct data = new DataStruct();

		#endregion

		// === DATA-INTERNAL ===

		#region DATA-INTERNAL

		bool isDirty = false;

		#endregion

		// === LIFECYCLE ===

		#region LIFECYCLE

		void OnEnable()
		{
		}

		void OnChange()
		{
			IsDirty = true;
		}

		[Inject]
		void Construct(INetworkSession<codingVR.Data.GetGraphDataNodeSpec, codingVR.Data.SetGraphDataNodeResult> networkSession)
		{
			// global interfaces
			this.networkSession = networkSession;
		}

		#endregion

		// === IMPLEMENTATION ===

		#region IMPLEMENTATION
			
		public void SetTag(string tag)
		{
			if (this.tag != tag)
			{
				this.tag = tag;
			}
		}

		#endregion

		// === IGraphDataBaseScriptable ===

		#region IGraphDataBaseScriptable

		override public string Title
		{
			get
			{
				return data.Title;
			}
			set
			{
				if (value != data.Title)
				{
					data.Title = value;
					OnChange();
				}
			}
		}

		override public string Tag
		{
			get
			{
				return tag;
			}
		}

		override public DataStruct Data
		{
			get
			{
				return data;
			}
			set
			{
				if (value != data)
				{
					data = value;
					OnChange();
				}
			}
		}

		override public string Answer
		{
			get
			{
				return data.Answer;
			}
			set
			{
				if (value != data.Answer)
				{
					data.Answer = value;
					OnChange();
				}
			}
		}

		override public ReadOnlyCollection<string> Options
		{
			get
			{
				return data.Options.AsReadOnly();
			}
		}

		override public void AddOptionsRange(string[] range)
		{
			data.Options.AddRange(range);
			OnChange();
		}

		#endregion

		// === INetworkClient ===

		#region INetworkClient

		/*
		 * 
		 * Data Specification
		 * 
		 */

		public IEnumerator GetDataSpec(Data.DoneGet<Data.GraphDataNodeScriptable> doneSet = null)
		{
			Data.GraphDataNodeScriptable scriptable = this;
			if (scriptable != null)
			{
				bool done = false;

				void LocalDoneSet(Data.GetGraphDataNodeSpec dataRef)
				{
					// check if user found !!!
					if (dataRef != null)
					{
						dataRef.Make(scriptable);
						doneSet?.Invoke(scriptable);
					}
					// needed for stop corountine
					done = true;
				}

				// load data
				bool ret = networkSession.GetSpec(scriptable.Tag, LocalDoneSet);
				yield return new WaitUntil(() => done == true || ret == false);

				// post data spec
				if (ret == false)
				{
					yield return PostDataSpec();
				}
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}

		public IEnumerator PostDataSpec()
		{
			Data.GraphDataNodeScriptable scriptable = this;
			if (scriptable != null)
			{
				bool done = false;
				void DoneSet(Data.GetGraphDataNodeSpec spec)
				{
					done = true;
				}
				var set = GetGraphDataNodeSpec.Create(scriptable);
				//var json = JsonUtility.ToJson(set, true);
				networkSession.PostSpec(set, DoneSet);
				yield return new WaitUntil(() => done == true);
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}

		/*
		 * 
		 * Data Result
		 * 
		 */

		public IEnumerator PutData()
		{
			Data.GraphDataNodeScriptable scriptable = this;
			if (scriptable != null)
			{
				bool done = false;
				void DoneSet(Data.SetGraphDataNodeResult spec)
				{
					done = true;
				}
				var set = SetGraphDataNodeResult.Create(scriptable);
				//var json = JsonUtility.ToJson(set, true);
				networkSession.Put(set, DoneSet);
				yield return new WaitUntil(() => done == true);
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}

		public IEnumerator PostData()
		{
			Data.GraphDataNodeScriptable scriptable = this;
			if (scriptable != null)
			{
				bool done = false;
				void DoneSet(Data.SetGraphDataNodeResult spec)
				{
					done = true;
				}
				var set = SetGraphDataNodeResult.Create(scriptable);
				//var json = JsonUtility.ToJson(set, true);
				networkSession.Post(set, DoneSet);
				yield return new WaitUntil(() => done == true);
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}

		public IEnumerator GetData(bool forceCreation, Data.DoneGet<Data.GraphDataNodeScriptable> doneSet = null)
		{
			Data.GraphDataNodeScriptable scriptable = this;
			if (scriptable != null)
			{
				bool done = false;

				void LocalDoneSet(Data.SetGraphDataNodeResult dataRef)
				{
					// check if user found !!!
					if (dataRef != null)
					{
						dataRef.Make(scriptable);
						doneSet?.Invoke(scriptable);
					}
					// needed for stop corountine
					done = true;
				}

				// load data
				bool ret = networkSession.GetResult(SetGraphDataNodeResult.MakeId(IGraphDataBase.IdPlayer, scriptable.Tag), LocalDoneSet);
				yield return new WaitUntil(() => done == true || ret == false);

				if (ret == false && forceCreation == true)
				{
					yield return PostData();
				}
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}

		#endregion

		// === IActivityField ===

		#region IActivityField

		public bool SetValue(string name, object value, Data.FieldSearchMethod method)
		{
			bool b = codingVR.Data.ActivityFieldUtilities.SetValue(this.Data, Tag, this, name, value, method);
			if (b == true)
			{
				OnChange();
			}
			return b;
		}

		public bool AddValue(string name, object value, FieldSearchMethod method)
		{
			bool b = codingVR.Data.ActivityFieldUtilities.AddValue(this.Data, Tag, this, name, value, method);
			if (b == true)
			{
				OnChange();
			}
			return b;
		}

		public bool SubValue(string name, object value, FieldSearchMethod method)
		{
			bool b = codingVR.Data.ActivityFieldUtilities.SubValue(this.Data, Tag, this, name, value, method);
			if (b == true)
			{
				OnChange();
			}
			return b;
		}

		public bool DivValue(string name, object value, FieldSearchMethod method)
		{
			bool b = codingVR.Data.ActivityFieldUtilities.DivValue(this.Data, Tag, this, name, value, method);
			if (b == true)
			{
				OnChange();
			}
			return b;
		}

		public bool MulValue(string name, object value, FieldSearchMethod method)
		{
			bool b = codingVR.Data.ActivityFieldUtilities.MulValue(this.Data, Tag, this, name, value, method);
			if (b == true)
			{
				OnChange();
			}
			return b;
		}

		public bool ResetValue(string name, FieldSearchMethod method)
		{
			bool b = codingVR.Data.ActivityFieldUtilities.ResetValue(this.Data, Tag, this, name, method);
			if (b == true)
			{
				OnChange();
			}
			return b;
		}

		public object GetValue(string name, FieldSearchMethod method)
		{
			return codingVR.Data.ActivityFieldUtilities.GetValue(this.Data, Tag, this, name, method);
		}

		public (bool, bool) CompareValue(string name, object value, FieldComparison comparison, FieldSearchMethod method)
		{
			return codingVR.Data.ActivityFieldUtilities.CompareValue(this.Data, Tag, this, name, value, comparison, method);
		}

		#endregion

		// === IActivityFieldServer ===

		#region  IActivityFieldServer

		public bool IsDirty
		{
			get
			{
				if (isDirty == true)
				{
					isDirty = false;
					return true;
				}
				else
				{
					return false;
				}
			}

			set
			{
				isDirty = value;
			}
		}

		public string GetFullPathField(string fieldName)
		{
			return ActivityFieldUtilities.GetFullPathField(Tag, fieldName);
		}

		public string Id
		{
			get
			{
				return Tag;
			}
			set
			{
				SetTag(value);
			}
		}

		public Dictionary<string, FieldInfo> Fields
		{
			get
			{
				return data.Fields;
			}
		}

		public IEnumerator GetEnumerator()
		{
			return data.GetEnumerator();
		}

		public void PopulateFields()
		{
			data.PopulateFields();
		}

		#endregion
	}

	// Data stucture used for managing transfert by client/server

	[System.Serializable]
	public class GetGraphDataNodeSpec : IGraphDataBase
	{
		[SerializeField]
		string question;
		[SerializeField]
		GraphDataNodeType questionType;   //type
		[SerializeField]
		List<string> options = new List<string>();        //options to select

		/*
		 * 
		 * JSON I/O
		 * 
		 */

		public override string ToString()
		{
			return JsonUtility.ToJson(this, true);
		}

		static public GetGraphDataNodeSpec FromString(string json)
		{
			var spec = new GetGraphDataNodeSpec();
			spec = JsonUtility.FromJson<GetGraphDataNodeSpec>(json);
			return spec;
		}

		/*
		 * 
		 * Make Data
		 * 
		 */

		public void Make(GraphDataNodeScriptable data)
		{
			data.SetTag ( this.Id );
			data.Data.QuestionType = this.questionType;
			data.Data.Title = this.question;
			data.Data.Options = this.options;
		}

		static public GetGraphDataNodeSpec Create(GraphDataNodeScriptable data)
		{
			return new GetGraphDataNodeSpec
			{
				Id = data.Tag,
				questionType = data.Data.QuestionType,
				question = data.Data.Title,
				options = data.Data.Options
			};
		}

	}

	[System.Serializable]
	public class SetGraphDataNodeResult : IGraphDataBase
	{
		[SerializeField]
		string answer;

		/*
		 * 
		 * JSON I/O
		 * 
		 */

		public override string ToString()
		{
			return JsonUtility.ToJson(this, true);
		}

		static public SetGraphDataNodeResult FromString(string json)
		{
			var spec = new SetGraphDataNodeResult();
			spec = JsonUtility.FromJson<SetGraphDataNodeResult>(json);
			return spec;
		}

		/*
		 * 
		 * Make Data
		 * 
		 */

		public void Make(GraphDataNodeScriptable scriptable)
		{
			string idPlayer;
			string tag;
			(idPlayer, tag) = ParseId(Id);
			scriptable.SetTag(tag);
			scriptable.Answer = answer;
		}

		static public SetGraphDataNodeResult Create(GraphDataNodeScriptable data)
		{
			return new SetGraphDataNodeResult
			{
				Id = MakeId(IdPlayer, data.Tag),
				answer = data.Answer
			};
		}
	}
}
