﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace codingVR.Data
{
	[CreateAssetMenu(fileName = "Item", menuName = "CodingVR/Data/Item", order = 9)]
	public class ItemScriptable : ScriptableObject
	{
		[SerializeField]
		public string nameItem;

		[SerializeField]
		public Sprite image;

		private int basePrice = 5;

		[SerializeField]
		public GameEngine.TypeItem typeItem = GameEngine.TypeItem.Torse;

		[SerializeField]
		public GameEngine.TypeStore typeStore = GameEngine.TypeStore.Clothes;

		[SerializeField]
		public GameEngine.TagObject tagObject = GameEngine.TagObject.cvrPlayerClothes;

		[SerializeField]
		[Range(1, 3)]
		int quality = 1;

		[SerializeField]
		[Range(1, 3)]
		int rarity = 1;

		[SerializeField]
		[Range(1, 3)]
		int luxury = 1;

		[SerializeField]
		string nameGameObject;

		[SerializeField]
		public List<Material> materialsItem = new List<Material>();

		[SerializeField]
		PersistentObject persistentObject;

		// === PERSISTENCY =========================================================

		#region UNITY-PERSISTENCY

		public AssetReference AssetReference => persistentObject.AssetReference;

		#endregion


		public string NameItem
		{
			get
			{
				return nameItem;
			}

			set
			{
				nameItem = value;
				this.name = name;
			}
		}

		public int GetPoints()
		{
			int points = quality + rarity + luxury;

			return points;
		}

		public int GetPrice()
		{
			int price = basePrice * (quality + rarity + luxury);

			return price;
		}

		public string NameGameObject
		{
			get
			{
				return nameGameObject;
			}

			set
			{
				nameGameObject = value;
			}
		}

		public void OnValidate()
		{
			// how to find a gameobjects prefab during runtime
			// https://stackoverflow.com/questions/53992675/how-to-find-a-gameobjects-prefab-during-runtime-with-unity-version-2018-3
			// This function is called when the script is loaded or a value is changed in the Inspector(Called in the editor only).
			persistentObject.Guid = persistentObject.GetGuidAddressFromPrefab(this);
		}		
	}
}
