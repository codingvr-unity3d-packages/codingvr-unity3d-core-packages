﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using UnityEngine;

namespace codingVR.Data
{
	[Serializable]
	public class PlayerStateDataSpec
	{
		/* 
		 * 
		 * key pseudo
		 * 
		 */


		[SerializeField]
		public string firstTheme;

		[Serializable]
		public class ThemeConfig : IThemeConfig
		{
			public ThemeRelationShip themeRelationShip = ThemeRelationShip.child;
            public ThemeUI themeUI = ThemeUI.completionBar|ThemeUI.debug|ThemeUI.menu|ThemeUI.money;
			public bool hasIsland = true;

			public ThemeRelationShip ThemeRelationShip => themeRelationShip;
            public ThemeUI ThemeUI => themeUI;
			public bool HasIsland => hasIsland;
		}

		[Serializable]
		public class ThemeConfigs : Core.cvrSerializableDictionary<string, ThemeConfig> { }

		[SerializeField]
		public ThemeConfigs themeConfigs;
	};
}
