﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace codingVR.Data
{
	[CreateAssetMenu(fileName = "PlayerStateData", menuName = "CodingVR/Data/PlayerState/ThemeData", order = 2)]
	public class PlayerStateDataScriptable : ScriptableObject
	{
		static Core.IEventManager eventManager;

		/* 
		 * 
		 * constructor
		 * 
		 */

		[Inject]
		public void Constructor(Core.IEventManager eventManager)
		{
			PlayerStateDataScriptable.eventManager = eventManager;
		}

		public void CleanInject()
		{
			PlayerStateDataScriptable.eventManager = null;
		}
		
		/* 
		 * 
		 * key pseudo
		 * 
		 */

		[SerializeField]
		[FormerlySerializedAs("pseudo")]
		string idPlayer;
		public string IdPlayer
		{
			get { return idPlayer; }
			set { idPlayer = value; }
		}

		/*
		 *
		 * Profile
		 * 
		 */

		[Serializable]
		public class Profile
		{
			// version
			static string currentVersion = "1";
			[SerializeField]
			string version = currentVersion;
		}
		Profile profile;

		/*
		 * 
		 * Current themes
		 * 
		 */

		[Serializable]
		public class CurrentThemeData
		{
			// version
			static string currentVersion = "1";
			[SerializeField]
			string version = currentVersion;

			// name
			[SerializeField]
			public string name;

			// Constructor
			public CurrentThemeData(string name)
			{
				this.name = name;
			}

			public override bool Equals(object other)
			{
				if (other == null)
					return false;
				if (other == this)
					return true;
				return ((other as CurrentThemeData).name == name);
			}

			public override int GetHashCode()
			{
				return name.GetHashCode();
			}
		}

		[SerializeField]
		public List<CurrentThemeData> currentThemes;
		
		private int LastCurrentThemeIndex
		{
			get
			{
				return currentThemes.Count - 1;
			}
		}
		
		public CurrentThemeData LastCurrentTheme
		{
			get { return currentThemes[LastCurrentThemeIndex]; }
			set { currentThemes[LastCurrentThemeIndex] = value; }
		}

		public CurrentThemeData[] CurrentThemes => currentThemes.ToArray();

		/* 
		 * 
		 * Theme state
		 *
		 */

		[Serializable]
		public class ThemeStateData : IThemeState
		{
			// version
			static string currentVersion = "1";
			[SerializeField]
			string version = currentVersion;

			[SerializeField]
			List<GameStateHistoryData> gameStateHistoryDataList;
			
			[SerializeField]
			string lastTag = Tags.Welcome.ToString();

			[SerializeField]
			float completionRateRequest = 0.0f;

			// effective complettion rate
			[SerializeField]
			float completionRate = 0.0f;

			// create theme state
			public static ThemeStateData CreateThemeState()
			{
				return new ThemeStateData();
			}

			// last tag
			public Tag LastTag
			{
				get
				{
					return Tag.CreateTag(lastTag);
				}
				set
				{
					lastTag = value.ToString();
				}
			}

			// completion rate request
			public float CompletionRateRequest
			{
				get
				{
					return completionRateRequest;
				}
				set
				{
					if (value != completionRateRequest)
					{
						completionRateRequest = value;
						PlayerStateDataScriptable.eventManager.QueueEvent(new Event_ThemeStateChange(Event_ThemeStateChange.Type_CompletionRateRequest, this));
					}
				}
			}

			// completion rate request
			public float CompletionRate
			{
				get
				{
					return completionRate;
				}
			}

			// Validate all requests
			public void ValidateAllRequests()
			{
				if (Mathf.Approximately(completionRateRequest, completionRate) == false)
				{
					completionRate = completionRateRequest;
					PlayerStateDataScriptable.eventManager.QueueEvent(new Event_ThemeStateChange(Event_ThemeStateChange.Type_CompletionRate, this));
				}
			}


			// game history data list
			public ref List<GameStateHistoryData> GameHistoryDataList
			{
				get
				{
					return ref gameStateHistoryDataList;
				}
			}

			// reset
			public void Reset()
			{
				gameStateHistoryDataList.Clear();
				lastTag = Tags.Welcome.ToString();
			}
		}

		[Serializable] public class ThemeStates : Core.cvrSerializableDictionary<string, ThemeStateData> { }
		[SerializeField]
		ThemeStates themeStates = new ThemeStates();

		public ThemeStates ThemeStatesContainer
		{
			get
			{
				return themeStates;
			}

			set
			{
				themeStates = value;
			}
		}


		/*
		 * 
		 * Interface
		 * 
		 */
				
			/*
		public void RemoveThemeState(string theme)
		{
			themeStates.Remove(theme);
		}
		*/

		public string FindKey(Data.IThemeState themeSate)
		{
			string key = null;
			foreach (var it in themeStates)
			{
				if (it.Value == themeSate)
				{
					key = it.Key;
				}
			}
			return key;
		}

		public bool ContainsThemeState(string theme)
		{
			return themeStates.ContainsKey(theme);
		}

		public ThemeStateData FindThemeState(string theme)
		{
			ThemeStateData themeStateData = null;
			if (themeStates.ContainsKey(theme))
				themeStateData = themeStates[theme];
			return themeStateData;
		}
					
		public ThemeStateData FindCurrentThemeState()
		{
			return themeStates[LastCurrentTheme.name];
		}
		
		public ICollection<string> GetThemeStateKeys()
		{
			return themeStates.Keys;
		}
		
		public void PushCurrentTheme(CurrentThemeData data)
		{
			void AddCurrentThemeIfNeeded(CurrentThemeData _data)
			{
				bool found = false;
				foreach (var it in currentThemes)
				{
					if (it.Equals(_data) == true)
					{
						found = true;
						break;
					}
				}

				if (found == false)
				{
					currentThemes.Add(_data);
				}
				else
				{
					currentThemes.Swap(currentThemes.IndexOf(data),  LastCurrentThemeIndex);
				}
			}

			void AddThemeStateIfNeeded(string newTheme)
			{
				if (ContainsThemeState(newTheme) == false)
				{
					themeStates.Add(newTheme, ThemeStateData.CreateThemeState());
				}
			}

			// add new current theme if needed
			{
				AddCurrentThemeIfNeeded(data);
			}

			// add new theme state the state 
			{
				AddThemeStateIfNeeded(data.name);
			}
		}

		public void PopCurrentTheme(string name)
		{
			if (LastCurrentTheme.name.Equals( name) == true)
				currentThemes.RemoveAt(LastCurrentThemeIndex);
			else
				Debug.LogErrorFormat("{0} : Can't pop current theme {1} which is not in top of the queue {2}", System.Reflection.MethodBase.GetCurrentMethod().Name, name, LastCurrentTheme.name);
		}

		public void ResetCurrentTheme(string theme)
		{
			var stateData = FindThemeState(theme);
			if (stateData != null)
			{
				stateData.Reset();
			}
		}

		public void ResetCurrentThemes()
		{
			currentThemes.Clear();
			foreach (var themeState in themeStates)
			{
				ResetCurrentTheme(themeState.Key);
			}
		}

		public void Reset(string firstTheme)
		{
			ResetCurrentThemes();
			CurrentThemeData currentThemeData = new CurrentThemeData(firstTheme);
			PushCurrentTheme(currentThemeData);
		}
				
		
		/*
		 * 
		 *  Serialization
		 *
		 */

		public void OnBeforeSerialize()
		{
		}

		public void OnAfterDeserialize()
		{
		}
	}

	// Data stucture used for managing transfert by client/server

	[System.Serializable]
	public class PlayerStateDataStructSpec : IGraphDataBase, ISerializationCallbackReceiver
	{
		static string currentVersion = "2";

		[SerializeField]
		string version = currentVersion;

		[SerializeField]
		List<PlayerStateDataScriptable.CurrentThemeData> currentThemes;

		[SerializeField]
		PlayerStateDataScriptable.ThemeStates themeStates = new PlayerStateDataScriptable.ThemeStates();

		public override string ToString()
		{
			return JsonUtility.ToJson(this, true);
		}

		static public PlayerStateDataStructSpec FromString(string json)
		{
			var data = new PlayerStateDataStructSpec();
			data = JsonUtility.FromJson<PlayerStateDataStructSpec>(json);
			return data;
		}

		public void Make(PlayerStateDataScriptable data)
		{
			data.IdPlayer = this.Id;
			data.currentThemes = this.currentThemes;
			data.ThemeStatesContainer = this.themeStates;
		}

		static public PlayerStateDataStructSpec Create(PlayerStateDataScriptable data)
		{
			return new PlayerStateDataStructSpec
			{
				Id = data.IdPlayer,
				currentThemes = data.currentThemes,
				themeStates = data.ThemeStatesContainer
			};
		}

		public void OnBeforeSerialize()
		{
		}

		public void OnAfterDeserialize()
		{
			if (version == "2")
			{
			}
		}
	}
}
