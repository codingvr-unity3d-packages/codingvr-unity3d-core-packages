﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

namespace codingVR.PotoGame.HubTheme
{
	[System.Serializable]
	public class GameObjectMultiLOCData : Data.ActivityData
	{
		[SerializeField]
		public string description = "Game Object Multi LOC";

		[SerializeField]
		[DefaultValue(0)]
		public int LOC = 0;

	}

	[CreateAssetMenu(fileName = "GameObjectMultiLOCDataScriptable", menuName = "CodingVR/Data/GameObjectMultiLOC/GameObjectMultiLOCDataScriptable")]
	[System.Serializable]
	public class GameObjectMultiLOCDataScriptable : Data.ActivityDataScriptable
	{
		[SerializeField]
		GameObjectMultiLOCData rawData;

		public override Data.IActivityFieldServer Data
		{
			get
			{
				return rawData as Data.IActivityFieldServer;
			}
			set
			{
				if (value != rawData)
				{
					rawData = value as GameObjectMultiLOCData;
				}
			}
		}
	}
}

