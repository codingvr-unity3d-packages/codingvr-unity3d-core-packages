﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Data
{

    [CreateAssetMenu(fileName = "Animations", menuName = "CodingVR/Data/Animations", order = 6)]
    public class AnimationsScriptable : ScriptableObject
    {
        [System.Serializable]
        public class DataAnimation
        {
            public string nameAnimation;
            public string triggerAnimation;
            public AnimationClip animationClip;
            public AvatarMask avatarMask;
        }
        [SerializeField]
        public List<DataAnimation> dataAnimations = new List<DataAnimation>();

        public string GetTriggerByName(string nameAnimation)
        {
            string trigger = "";

            foreach(DataAnimation data in dataAnimations)
            {
                if(data.nameAnimation == nameAnimation)
                {
                    trigger = data.triggerAnimation;
                }
            }

            return trigger;
        }

        public string GetTriggerByIndex(int index)
        {
            string trigger = "";

            if(index < dataAnimations.Count)
            {
                trigger = dataAnimations[index].triggerAnimation;
            }

            return trigger;
        }

        public DataAnimation GetDataAnimationByName(string nameAnimation)
        {
            DataAnimation dataAnimation = null;

            foreach (DataAnimation data in dataAnimations)
            {
                if (data.nameAnimation == nameAnimation)
                {
                    dataAnimation = data;
                }
            }

            return dataAnimation;
        }

        public AnimationClip GetAnimationClipByName(string nameAnimation)
        {
            AnimationClip animation = null;

            foreach (DataAnimation data in dataAnimations)
            {
                if (data.nameAnimation == nameAnimation)
                {
                    animation = data.animationClip;
                }
            }

            return animation;
        }

        public AnimationClip GetAnimationClipByIndex(int index)
        {
            AnimationClip animation = null;

            if (index < dataAnimations.Count)
            {
                animation = dataAnimations[index].animationClip;
            }

            return animation;
        }

        public int GetIndexByName(string nameAnimation)
        {
            int index = 0;

            foreach (DataAnimation data in dataAnimations)
            {
                if (data.nameAnimation == nameAnimation)
                {
                    index = dataAnimations.IndexOf(data);
                }
            }

            return index;
        }
    }
}