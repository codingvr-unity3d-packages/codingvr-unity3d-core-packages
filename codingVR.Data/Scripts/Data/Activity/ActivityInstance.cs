﻿using codingVR.Core;
using FSM;
using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace codingVR.Data
{
	[Serializable]
	public class ActivityInstance : ActivityFieldManager, IActivityInstance
	{
		// === DATA-UI ================================================

		#region DATA-UI

		[Header("Identification")]
		// activity instance ID
		[SerializeField]
		[ReadOnly]
		[FormerlySerializedAs("idActivity")]
		string idInstance;

		// activity instance ID
		[SerializeField]
		string idInstanceParent;

		// activity class ID
		[SerializeField]
		string idClass;

		[Header("Description")]
		// descritpion
		[SerializeField]
		public string nameInstance;
		[SerializeField]
		public string descriptionInstance;

		[Header("Lifecycle")]

		// select state
		[SerializeField]
		public bool isSelected = false;

		// active time in runtime
		[SerializeField]
		[ReadOnly]
		// runTime is initilized on Start event
		public int runTime;
			   		 
		// createTime is initialized with current time on Create event
		[SerializeField]
		[ReadOnly]
		public int createTime;

		// activityTime is initialized with current time on Register event
		// then activityTime is initialized with current time + delay on Create event
		// then activityTime is initialized with current time on Start event
		// then activityTime is initialized with current time on Stop event
		[SerializeField]
		[ReadOnly]
		[FormerlySerializedAs("startTime")]
		public int activityTime;

		[SerializeField]
		[ReadOnly]
		// completionTime is initialized with current time on Complete event
		public int completionTime;

		// completion
		[SerializeField]
		public float completion;

		// status
		[SerializeField]
		[ReadOnly]
		public ActivityInstanceLifeCycleStatus statusFlags;

		// number of loops completed and started as long as
		// the activity did not stop
		[SerializeField]
		[ReadOnly]
		public int countStarted = 0;
		[SerializeField]
		[ReadOnly]
		public int countCompleted = 0;

		[Header("Runtime status")]
		// sub status
		[SerializeField]
		public ActivityInstanceSubStatusFlags subStatusFlags;

		[SerializeField]
		[ReadOnly]
		public ActivityInstancePlayerStatusFlags playerStatusFlags;


		[Header("Gameplay")]
		// passage
		[SerializeField]
		public int instancePassage = 0;
		
		[SerializeField]
		public bool tutorialDone = false;

		[Header("Rewards")]
		//List gifts items
		[SerializeField]
		public List<ItemScriptable> itemsGift = new List<ItemScriptable>();
		[SerializeField]
		[ReadOnly]
		public int lastItemGiftRewared;
		[SerializeField]
		[ReadOnly]
		public int lastMoneyCredited;

		#endregion

		// === DATA-INTERNAL ================================================

		#region DATA-INTERNAL

		// interfaces
		Core.IEventManager eventManager;
		Data.IActivtyClassCollection activityClassCollection;
		// fsm
		StateMachine fsm;
		// activity class
		IActivityClass playerStateActivityClassData;
		// local timer
		float timer = 0.0f;
		// this flag can't be serialized
		bool isRunning = false;
		// is Dirty
		bool isDirty = false;

		#endregion

		// === UTILS  ===================================================

		#region UTILS

		string MakeTransition(string transition0, string transition1)
		{
			return transition0 + "->" + transition1;
		}

		void AddTriggerTransition(StateMachine fsm, string transition0, string transition1, Func<Transition, bool> condition = null)
		{
			fsm.AddTriggerTransition(MakeTransition(transition0, transition1), new Transition(transition0, transition1, condition));
		}

		void Trigger(StateMachine fsm, string transition0, string transition1)
		{
			fsm.Trigger(MakeTransition(transition0, transition1));
		}
		
		#endregion

		// === IActivityInstance =============================================

		#region IActivityInstance

		// activity instance ID
		public string IdInstance => idInstance;

		// activity instance ID
		public string IdInstanceParent => idInstanceParent;

		// activity class ID
		public string IdClass => idClass;
		public IActivityClass Class => playerStateActivityClassData;

		// activity selected
		public bool IsSelected
		{
			get
			{
				return isSelected;
			}
			set
			{
				if (isSelected != value)
				{
					isSelected = value;
					IsDirty = true;
				}
			}
		}
		
		// active time in runtime
		public int RunTime => runTime;

		// local create time
		public int LocalCreateTime => TimeUtils.GetCurrentUnixTimeSeconds() - createTime;

		// a LocalActivityTime < 0; is the delay before starting (created & not started)
		// a LocalActivityTime >= 0 && a LocalActivityTime < duration; a activity is alive (created & started)
		// a LocalActivityTime >= duration; a activity is completed
		public int LocalActivityTime => TimeUtils.GetCurrentUnixTimeSeconds() - activityTime;

		// local completion time
		public int LocalCompletionTime => TimeUtils.GetCurrentUnixTimeSeconds() - completionTime;
				
		// completion 
		public float Completion
		{
			get
			{
				return completion;
			}
			set
			{
				if (completion != value)
				{
					completion = value;
					IsDirty = true;
				}
			}
		}

		// status & sub status
		public ActivityInstanceLifeCycleStatus StatusFlags
		{
			get
			{
				return statusFlags;
			}
			set
			{
				/*
				if (idInstance == "#[Shop]")
				{
					Debug.LogError("status modified !!!");
				}
				*/

				if (statusFlags != value)
				{
					statusFlags = value;
					IsDirty = true;
				}
			}
		}

		public ActivityInstanceSubStatusFlags SubStatusFlags
		{
			get
			{
				return subStatusFlags;
			}
			set
			{
				if (subStatusFlags != value)
				{
					subStatusFlags = value;
					IsDirty = true;
				}
			}
		}

		public ActivityInstancePlayerStatusFlags PlayerStatusFlags
		{
			get
			{
				return playerStatusFlags;
			}
			set
			{
				if (playerStatusFlags != value)
				{
					playerStatusFlags = value;
					IsDirty = true;
				}
			}
		}

		// Count past by different status
		public int CountCompleted
		{
			get
			{
				return countCompleted;
			}
			set
			{
				if (countCompleted != value)
				{
					countCompleted = value;
					IsDirty = true;
				}
			}
		}

		public int CountStarted
		{
			get
			{
				return countStarted;
			}
			set
			{
				if (countStarted != value)
				{
					countStarted = value;
					IsDirty = true;
				}
			}
		}


		// passage
		public int InstancePassage
		{
			get
			{
				return instancePassage;
			}
			set
			{
				if (instancePassage != value)
				{
					instancePassage = value;
					IsDirty = true;
				}
			}
		}

		//List items gifts
		public List<ItemScriptable> ItemsGift => itemsGift;

		// last gift rewarded
		public ItemScriptable LastItemGiftRewared
		{
			get
			{
				ItemScriptable item = null;
				if (lastItemGiftRewared >= 0 && lastItemGiftRewared < itemsGift.Count)
					item = itemsGift[lastItemGiftRewared];
				return item;
			}
			set
			{
				if(value != null)
				{
					if (value.name.Equals(itemsGift[lastItemGiftRewared].name) == false)
					{
						int FindItemGiftIndex(ItemScriptable refItem)
						{
							return itemsGift.FindIndex(item => item.name.Equals(refItem.name) == true);
						}

						lastItemGiftRewared = FindItemGiftIndex(value);
						IsDirty = true;
					}
				}
				else
				{
					Debug.LogErrorFormat("{0} : last item gift rewarded is null !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
				}
			}
		}

		// last money credited
		public int LastMoneyCredited
		{
			get
			{
				return lastMoneyCredited;
			}
			set
			{
				if (lastMoneyCredited != value)
				{
					lastMoneyCredited = value;
					IsDirty = true;
				}
			}
		}
		
		// activity instance name
		public string NameInstance => nameInstance;

		// activity instance description
		public string DescriptionInstance => descriptionInstance;

		// activity tutorial done
		public bool TutorialDone => tutorialDone;
				
		// state machine management
		public bool Register(bool bForce)
		{
			bool b = false;
			if (statusFlags == ActivityInstanceLifeCycleStatus.isActivityStopped)
			{
				NextEventIsForced = bForce;
				fsm.RequestStateChange(RegisterActivity);
				b = true;
			}
			return b;
		}

		public bool Create(bool bForce)
		{
			/*
			if (idInstance == "#[Shop]")
			{
				Debug.LogError("Create");
			}
			*/

			bool b = false;
			{
				if (statusFlags == ActivityInstanceLifeCycleStatus.isActivityRegistered)
				{
					NextEventIsForced = bForce;
					fsm.RequestStateChange(CreateActivity);
					b = true;
				}
			}

			if (b == false)
			{
				if (statusFlags == ActivityInstanceLifeCycleStatus.isActivityCompleted)
				{
					NextEventIsForced = bForce;
					this.ResetAllTimes();
					Trigger(fsm, CompleteActivity, CreateActivity);
					b = true;
				}
			}
			return b;
		}

		public bool Start(bool bForce)
		{
			bool b = false;
			if (statusFlags == ActivityInstanceLifeCycleStatus.isActivityCreated)
			{
				NextEventIsForced = bForce;
				fsm.RequestStateChange(StartActivity);
				b = true;
			}
			return b;
		}

		public bool Complete(bool bForce)
		{
			bool b = false;
			if (statusFlags == ActivityInstanceLifeCycleStatus.isActivityStarted)
			{
				NextEventIsForced = bForce;
				// in order to avoid sending message after modifying 'completion'
				// dont't call 'C'ompletion; which call 'IsDirty'
				completion = 1.0f;
				b = true;
			}
			return b;
		}

		public bool Stop(bool bForce)
		{
			bool b = false;

			if (statusFlags == ActivityInstanceLifeCycleStatus.isActivityRegistered && isRunning == false && b == false)
			{
				NextEventIsForced = bForce;
				Trigger(fsm, RegisterActivity, StopActivity);
				b = true;
			}

			if (statusFlags == ActivityInstanceLifeCycleStatus.isActivityCreated && isRunning == false && b == false)
			{
				NextEventIsForced = bForce;
				Trigger(fsm, CreateActivity, StopActivity);
				b = true;
			}

			if (statusFlags == ActivityInstanceLifeCycleStatus.isActivityStarted && isRunning == true && b == false)
			{
				NextEventIsForced = bForce;
				Trigger(fsm, RunningActivity, StopActivity);
				b = true;
			}

			if (statusFlags == ActivityInstanceLifeCycleStatus.isActivityCompleted && isRunning == false && b == false)
			{
				NextEventIsForced = bForce;
				Trigger(fsm, CompleteActivity, StopActivity);
				b = true;
			}

			return b;
		}

		public void ResetActivityTime()
		{
			if (statusFlags == ActivityInstanceLifeCycleStatus.isActivityStarted && isRunning == true)
			{
				activityTime = TimeUtils.GetCurrentUnixTimeSeconds();
			}
		}

		void ResetAllTimes()
		{
			activityTime = TimeUtils.GetCurrentUnixTimeSeconds();
			createTime = TimeUtils.GetCurrentUnixTimeSeconds();
			completionTime = TimeUtils.GetCurrentUnixTimeSeconds();
		}

		#endregion

		// === IActivityFieldServer =============================================

		#region IActivityFieldServer

		public string GetFullPathField(string fieldName)
		{
			return ActivityFieldUtilities.GetFullPathField(IdInstance, fieldName);
		}

		public bool IsDirty
		{
			get
			{
				if (isDirty == true)
				{
					isDirty = false;
					return true;
				}
				else
				{
					return false;
				}
			}

			set
			{
				isDirty = value;
			}
		}

		public Dictionary<string, FieldInfo> Fields
		{
			get
			{
				Dictionary<string, FieldInfo> fieldsCasted = new Dictionary<string, FieldInfo>();
				foreach (var it in base.Fields)
				{
					fieldsCasted.Add(it.Key, it.Value.fieldInfo);
				}
				return fieldsCasted;
			}
		}

		public string Id
		{
			get
			{
				return idInstance;
			}
			set
			{
				idInstance = value;
			}
		}

		public void PopulateFields()
		{
			base.PopulateFields();
		}

		#endregion

		// === IActivityField =============================================

		#region IActivityField

		public bool SetValue(string name, object value, FieldSearchMethod method)
		{
			bool b = ActivityFieldUtilities.SetValue(this, IdInstance, this, name, value, method);
			if (b == true)
			{
				isDirty = true;
			}
			return b;
		}

		public bool AddValue(string name, object value, FieldSearchMethod method)
		{
			bool b = ActivityFieldUtilities.AddValue(this, IdInstance, this, name, value, method);
			if (b == true)
			{
				isDirty = true;
			}
			return b;
		}

		public bool SubValue(string name, object value, FieldSearchMethod method)
		{
			bool b = ActivityFieldUtilities.SubValue(this, IdInstance, this, name, value, method);
			if (b == true)
			{
				isDirty = true;
			}
			return b;
		}

		public bool DivValue(string name, object value, FieldSearchMethod method)
		{
			bool b = ActivityFieldUtilities.DivValue(this, IdInstance, this, name, value, method);
			if (b == true)
			{
				isDirty = true;
			}
			return b;
		}

		public bool MulValue(string name, object value, FieldSearchMethod method)
		{
			bool b = ActivityFieldUtilities.MulValue(this, IdInstance, this, name, value, method);
			if (b == true)
			{
				isDirty = true;
			}
			return b;
		}

		public bool ResetValue(string name, FieldSearchMethod method)
		{
			bool b = ActivityFieldUtilities.ResetValue(this, IdInstance, this, name, method);
			if (b == true)
			{
				isDirty = true;
			}
			return b;
		}

		public object GetValue(string name, FieldSearchMethod method)
		{
			return ActivityFieldUtilities.GetValue(this, IdInstance, this, name, method);
		}

		public (bool, bool) CompareValue(string name, object value, FieldComparison comparison, FieldSearchMethod method)
		{
			return ActivityFieldUtilities.CompareValue(this, IdInstance, this, name, value, comparison, method);
		}

		#endregion

		// === INTERFACE =============================================

		#region INTERFACE

		// ==== Constructor & Setup ========================================================================================

		// constructor
		protected ActivityInstance() : base()
		{
		}

		// constructor
		protected ActivityInstance(string idActivityInstance, string idActivityInstanceParent, string idClass) : base()
		{
			this.idInstance = idActivityInstance;
			this.idInstanceParent = idActivityInstanceParent;
			this.idClass = idClass;
			ResetToDefault();
		}

		public void ResetToDefault()
		{
			// we assume to not call explicitly IsDrty=true
			this.statusFlags = ActivityInstanceLifeCycleStatus.isActivityStopped;
			this.countCompleted = 0;
			this.countStarted = 0;
			this.isRunning = false;
			this.subStatusFlags = ActivityInstanceSubStatusFlags.none;
			this.ResetAllTimes();
			this.runTime = 0;
			this.playerStatusFlags = ActivityInstancePlayerStatusFlags.none;
			ResetGameplay();
		}

		public void ResetGameplay()
		{
			this.instancePassage = 0;
			this.tutorialDone = false;
		}

		protected ActivityInstance(IActivityInstance activityInstance) : base()
		{
			this.CopyDataFrom(activityInstance);
		}

		public void CopyDataFrom(IActivityInstance activityInstanceInterface)
		{
			var activityInstance = activityInstanceInterface as ActivityInstance;
			this.idInstance = activityInstance.idInstance;
			this.idInstanceParent = activityInstance.idInstanceParent;
			this.idClass = activityInstance.idClass;
			this.nameInstance = activityInstance.nameInstance;
			this.descriptionInstance = activityInstance.descriptionInstance;
			this.isSelected = activityInstance.isSelected;
			this.runTime = activityInstance.runTime;
			this.createTime = activityInstance.createTime;
			this.activityTime = activityInstance.activityTime;
			this.completionTime = activityInstance.completionTime;
			this.completion = activityInstance.completion;
			this.statusFlags = activityInstance.statusFlags;
			this.countStarted = activityInstance.countStarted;
			this.countCompleted = activityInstance.countCompleted;
			this.subStatusFlags = activityInstance.subStatusFlags;
			this.playerStatusFlags = activityInstance.playerStatusFlags;
			this.instancePassage = activityInstance.instancePassage;
			this.tutorialDone = activityInstance.tutorialDone;
			this.itemsGift = new List<ItemScriptable>(activityInstance.itemsGift);
			this.lastItemGiftRewared = activityInstance.lastItemGiftRewared;
			this.lastMoneyCredited = activityInstance.lastMoneyCredited;
		}

		public void UpdateDataFrom(IActivityInstance activityInstanceInterface)
		{
			var activityInstance = activityInstanceInterface as ActivityInstance;
			//this.idInstance = activityInstance.idInstance;
			this.idInstanceParent = activityInstance.idInstanceParent;
			this.idClass = activityInstance.idClass;
			this.nameInstance = activityInstance.nameInstance;
			this.descriptionInstance = activityInstance.descriptionInstance;
			//this.isSelected = activityInstance.isSelected;
			//this.runTime = activityInstance.runTime;
			//this.createTime = activityInstance.createTime;
			//this.activityTime = activityInstance.activityTime;
			//this.completionTime = activityInstance.completionTime;
			//this.completion = activityInstance.completion;
			//this.statusFlags = activityInstance.statusFlags;
			//this.countStarted = activityInstance.countStarted;
			//this.countCompleted = activityInstance.countCompleted;
			//this.subStatusFlags = activityInstance.subStatusFlags;
			//this.playerStatusFlags = activityInstance.playerStatusFlags;
			//this.instancePassage = activityInstance.instancePassage;
			//this.tutorialDone = activityInstance.tutorialDone;

			if (this.lastItemGiftRewared >= 0)
			{
				if (this.lastItemGiftRewared < activityInstance.itemsGift.Count)
				{
					this.itemsGift = new List<ItemScriptable>(activityInstance.itemsGift);
				}
				else
				{
					Debug.LogWarningFormat("{0} : impossible to provide a new items gift list with a size of {1} since the last item gift {2} is out of bound !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, this.lastItemGiftRewared, activityInstance.itemsGift.Count);
				}
			}

			//this.lastItemGiftRewared = activityInstance.lastItemGiftRewared;
			//this.lastMoneyCredited = activityInstance.lastMoneyCredited;
		}

		// creation
		static public IActivityInstance CreateInstance(IActivityInstance activityInstance)
		{
			ActivityInstance playerStateActivityInstanceData = new ActivityInstance(activityInstance);
			return playerStateActivityInstanceData;
		}

		// creation
		static public ActivityInstance CreateInstance(string idActivityInstance, string idActivityInstanceParent, string idClass)
		{
			ActivityInstance playerStateActivityInstanceData = new ActivityInstance(idActivityInstance, idActivityInstanceParent, idClass);
			return playerStateActivityInstanceData;
		}

		// setup

		public StateMachine Setup(MonoBehaviour owner, Core.IEventManager eventManager, Data.IActivtyClassCollection activityClassCollection)
		{
			if (fsm == null)
			{
				// extenal interface support
				this.eventManager = eventManager;
				this.activityClassCollection = activityClassCollection;

				// class id
				playerStateActivityClassData = activityClassCollection.FindActivityClass(idClass);

				// state machine
				if (playerStateActivityClassData != null)
				{
					// create fsm
					fsm = CreateStateMachine(owner);
					// populate also class
					base.PopulateFields(playerStateActivityClassData);
				}
				else
				{
					Debug.LogErrorFormat("{0} : instance state {1} has an invalid idClass {2} !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, idInstance, idClass);
				}

				// timer
				timer = 0.0f;

				EnableListeners();
			}
			return fsm;
		}

		// start
		public void StartService()
		{
			if (fsm != null)
			{
				fsm.SetStartState(AwakeActivity);
				fsm.Init();
			}
		}
		
		public void Update()
		{
			bool letFramePass = false;
			if (this.eventManager != null)
			{
				if (this.IsDirty == true)
				{
					QueueEventPlayerStateActivityInstanceChange();
					// in order to ensure that the data arrives before the FSM state
					// we let a frame pass on the FSM update
					letFramePass = true; 
				}
			}

			if (letFramePass == false)
			{
				fsm?.OnLogic();
			}
		}

		#endregion

		// === IMPLEMENTATION =================================================

		#region IMPLEMENTATION

		// === send events ==================================================

		bool NextEventIsForced { get; set; }

		void QueueEventPlayerStateActivityInstanceChange()
		{
			this.eventManager.QueueEvent(new Event_PlayerStateActivityInstanceChange((idInstance, this)));
		}

		void QueueEvent<ev>(bool progressing) where ev : IGameEvent, new()
		{
			var value = (ev)Activator.CreateInstance(typeof(ev), idInstance, progressing, NextEventIsForced);
			eventManager.QueueEvent(value);
			NextEventIsForced = false;
		}

		/*
		*
		* Triggering
		* 
		*/

		private void EnableListeners()
		{
			// add listener
			if (eventManager != null)
			{
				// tag group
				eventManager.AddListener<GameEngine.Event_ScreenPlayTagGroupEnter>(OnScreenPlayTagGroupEnter);
				eventManager.AddListener<GameEngine.Event_ScreenPlayTagGroupLeave>(OnScreenPlayTagGroupLeave);

			}
		}

		private void DisableListeners()
		{
			// remove listener
			if (eventManager != null)
			{
				// tag group
				eventManager.RemoveListener<GameEngine.Event_ScreenPlayTagGroupEnter>(OnScreenPlayTagGroupEnter);
				eventManager.RemoveListener<GameEngine.Event_ScreenPlayTagGroupLeave>(OnScreenPlayTagGroupLeave);
			}
		}

		bool ContainsIdInstanceActivity(string groupTagName)
		{
			return IdInstance.Contains(groupTagName);
		}


		bool IsThisGroup(GameEngine.Event_ScreenPlayTagBase ev)
		{
			var groupData = ev.Data as Tuple<string, Data.Tag>;
			return ContainsIdInstanceActivity(groupData.Item2.ToString());
		}


		void OnScreenPlayTagGroupEnter(GameEngine.Event_ScreenPlayTagGroupEnter ev)
		{
			if (IsThisGroup(ev) == true)
			{
				PlayerStatusFlags = ActivityInstancePlayerStatusFlags.isPlayerPending;
			}
			//heck if ev is NotFiniteNumberException for global value
		}

		void OnScreenPlayTagGroupLeave(GameEngine.Event_ScreenPlayTagGroupLeave ev)
		{
			if (IsThisGroup(ev) == true)
			{
				playerStatusFlags = ActivityInstancePlayerStatusFlags.none;

				// Incremment passage
				playerStateActivityClassData.ClassPassage++;
			}
		}
		
		// === States =======================================================

		static readonly string AwakeActivity = "AwakeActivity";
		static readonly string RegisterActivity = "RegisterActivity";
		static readonly string CreateActivity = "CreateActivity";
		static readonly string StartActivity = "StartActivity";
		static readonly string RunningActivity = "RunningActivity";
		static readonly string CompleteActivity = "CompleteActivity";
		static readonly string StopActivity = "StopActivity";
		
		// === State events ===================================================

		// awake
		void OnLogicAwake(State state)
		{
			// the activity is registered and not created
			if (statusFlags == ActivityInstanceLifeCycleStatus.isActivityRegistered)
			{
				fsm.RequestStateChange(RegisterActivity);
				return;
			}
			// the activity is started and not completed and not running
			if (statusFlags == ActivityInstanceLifeCycleStatus.isActivityCreated)
			{
				/*
				if (idInstance == "#[Shop]")
				{
					Debug.LogError("OnLogicAwake");
				}
				*/

				fsm.RequestStateChange(CreateActivity);
				return;
			}

			// the activity is started and not completed and not running
			if (statusFlags == ActivityInstanceLifeCycleStatus.isActivityStarted)
			{
				fsm.RequestStateChange(StartActivity);
				return;
			}

			// the activity is started and not completed and not running
			if (statusFlags == ActivityInstanceLifeCycleStatus.isActivityCompleted)
			{
				fsm.RequestStateChange(CompleteActivity);
				return;
			}
		}

		// register
		void OnEnterRegister(State state)
		{	
			/*
			if (idInstance == "#[Shop]")
			{
				Debug.LogError("OnEnterRegister");
			}
			*/
			var stateLifeCyle = ActivityInstanceLifeCycleStatus.isActivityRegistered;
			activityTime = TimeUtils.GetCurrentUnixTimeSeconds();
			var prevStatusFlags = statusFlags;
			statusFlags = stateLifeCyle; // normally already done
			countCompleted = 0;
			countStarted = 0;
			subStatusFlags = ActivityInstanceSubStatusFlags.none;
			playerStatusFlags = ActivityInstancePlayerStatusFlags.none;

			// the event 'Event_PlayerStateActivityInstanceChange' must be sent before
			QueueEventPlayerStateActivityInstanceChange();
			// the message of status must be sent after the msg 'Event_PlayerStateActivityInstanceChange'
			QueueEvent<Event_PlayerStateActivityRegister>(prevStatusFlags != stateLifeCyle);
		}

		// create
		void OnEnterCreate(State state)
		{
			/*
			if (idInstance == "#[Shop]")
			{
				Debug.LogError("OnEnterCreate");
			}
			*/
			var stateLifeCyle = ActivityInstanceLifeCycleStatus.isActivityCreated;
			activityTime = TimeUtils.GetCurrentUnixTimeSeconds() + playerStateActivityClassData.Delay;
			createTime = TimeUtils.GetCurrentUnixTimeSeconds();
			completion = 0;
			timer = 0.0f;
			var prevStatusFlags = statusFlags;
			statusFlags = stateLifeCyle;
			subStatusFlags = ActivityInstanceSubStatusFlags.none;
			playerStatusFlags = ActivityInstancePlayerStatusFlags.none;

			// the event 'Event_PlayerStateActivityInstanceChange' must be sent before
			// to avoid latency, we don't use IsDirty, we use directly 'Event_PlayerStateActivityInstanceChange'
			QueueEventPlayerStateActivityInstanceChange();
			// the message of status must be sent after the msg 'Event_PlayerStateActivityInstanceChange'
			QueueEvent<Event_PlayerStateActivityCreate>(prevStatusFlags != stateLifeCyle);
		}

		// start
		void OnEnterStart(State state)
		{
			/*
			if (idInstance == "#[Shop]")
			{
				Debug.LogError("OnEnterStart");
			}
			*/

			var stateLifeCyle = ActivityInstanceLifeCycleStatus.isActivityStarted;
			runTime = 0;
			var prevStatusFlags = statusFlags;
			statusFlags = stateLifeCyle;
			countStarted++;
			activityTime = TimeUtils.GetCurrentUnixTimeSeconds();
			playerStatusFlags = ActivityInstancePlayerStatusFlags.none;

			// the event 'Event_PlayerStateActivityInstanceChange' must be sent before
			// to avoid latency, we don't use IsDirty, we use directly 'Event_PlayerStateActivityInstanceChange'
			QueueEventPlayerStateActivityInstanceChange();
			// the message of status must be sent after the msg 'Event_PlayerStateActivityInstanceChange'
			QueueEvent<Event_PlayerStateActivityStart>(prevStatusFlags != stateLifeCyle);
		}

		// complete
		void OnEnterComplete(State state)
		{
			/*
			if (idInstance == "#[Shop]")
			{
				Debug.LogError("OnEnterComplete");
			}
			*/

			var stateLifeCyle = ActivityInstanceLifeCycleStatus.isActivityCompleted;
			completionTime = TimeUtils.GetCurrentUnixTimeSeconds();
			var prevStatusFlags = statusFlags;
			statusFlags = stateLifeCyle;
			countCompleted++;

			// the event 'Event_PlayerStateActivityInstanceChange' must be sent before (because completionTime) 
			// to avoid latency, we don't use IsDirty, we use directly 'Event_PlayerStateActivityInstanceChange'
			QueueEventPlayerStateActivityInstanceChange();
			// the message of status must be sent after the msg 'Event_PlayerStateActivityInstanceChange'
			QueueEvent<Event_PlayerStateActivityComplete>(prevStatusFlags != stateLifeCyle);
		}

		// stop
		void OnEnterStop(State state)
		{
			/*
			if (idInstance == "#[Shop]")
			{
				Debug.LogError("OnEnterStop");
			}
			*/

			var stateLifeCyle = ActivityInstanceLifeCycleStatus.isActivityStopped;
			this.ResetAllTimes();
			completion = 0;
			statusFlags = stateLifeCyle;
			countCompleted = 0;
			countStarted = 0;
			/* 
			 * WARNING the subStatusFlags here is rested; so this status will be intercepted by client before  (see QueueEventPlayerStateActivityInstanceChange called before)
			 * which can create a restart of notification because if we are 'STARTED'
			 * ======================================================================
			 * to have the change at level of life cycle (see QueueEvent<Event_PlayerStateActivityStop>(true) called after)
			 * 
			 */
			subStatusFlags = ActivityInstanceSubStatusFlags.none; 
			playerStatusFlags = ActivityInstancePlayerStatusFlags.none;
			instancePassage = 0;
			fsm.RequestStateChange(AwakeActivity);

			// the event 'Event_PlayerStateActivityInstanceChange' must be sent before
			// to avoid latency, we don't use IsDirty, we use directly 'Event_PlayerStateActivityInstanceChange'
			QueueEventPlayerStateActivityInstanceChange();
			// the message of status must be sent after the msg 'Event_PlayerStateActivityInstanceChange'
			QueueEvent<Event_PlayerStateActivityStop>(true);
		}

		// running
		void OnEnterRunning(State state)
		{
			isRunning = true;
		}

		void ComputeRunTime()
		{
			timer += Time.deltaTime;
			int count = Convert.ToInt32(timer);
			if (count > 0)
			{
				runTime = runTime + count;
				timer = timer - count;
			}
		}

		void OnLogicRunning(State state)
		{
			ComputeRunTime();
		}

		void OnExitRunning(State state)
		{
			isRunning = false;
		}

		// === Transition events ===================================================

		bool OnTransitionRegisterToCreate(Transition transition)
		{
			bool registered = statusFlags == ActivityInstanceLifeCycleStatus.isActivityRegistered;
			bool automatic = (playerStateActivityClassData.PropertyFlags & ActivityClassPropertyFlags.automaticCreation) != 0;
			return registered && automatic;
		}

		bool OnTransitionCreateToStart(Transition transition)
		{
			bool created = statusFlags == ActivityInstanceLifeCycleStatus.isActivityCreated;
			bool automatic = (playerStateActivityClassData.PropertyFlags & ActivityClassPropertyFlags.automaticStarting) != 0;
			bool done = (LocalActivityTime >= 0);
			return done && created && automatic;
		}

		bool OnTransitionStartToRunning(Transition transition)
		{
			bool started = statusFlags == ActivityInstanceLifeCycleStatus.isActivityStarted;
			bool timeReached = (LocalActivityTime >= 0 && LocalActivityTime <= playerStateActivityClassData.Duration) || (playerStateActivityClassData.Duration <= 0);
			bool completionReached = completion >= 1.0f;
			return (completionReached || timeReached) && started;
		}

		bool OnTransitionStartToComplete(Transition transition)
		{
			bool started = statusFlags == ActivityInstanceLifeCycleStatus.isActivityStarted;
			bool timeReached = (LocalActivityTime > playerStateActivityClassData.Duration) && (playerStateActivityClassData.Duration > 0);
			bool completionReached = completion >= 1.0f;
			return (completionReached || timeReached) && started;
		}

		bool OnTransitionRunningToComplete(Transition transition)
		{
			bool timeReached = (LocalActivityTime > playerStateActivityClassData.Duration) && (playerStateActivityClassData.Duration > 0);
			bool completionReached = completion >= 1.0f;
			return (completionReached || timeReached) && isRunning;
		}

		// === Create state machine =======================================================

		private StateMachine CreateStateMachine(MonoBehaviour mono)
		{
			// This is the nested state machine
			StateMachine fsm = new StateMachine(mono, needsExitTime: false);

			// first state called automatically
			fsm.AddState(AwakeActivity, new State(
							onLogic: OnLogicAwake
						));

			// register
			fsm.AddState(RegisterActivity, new State(
							onEnter: OnEnterRegister
						));

			// creation
			fsm.AddState(CreateActivity, new State(
							onEnter: OnEnterCreate
						));

			// start
			fsm.AddState(StartActivity, new State(
							onEnter: OnEnterStart
						));

			// running
			fsm.AddState(RunningActivity, new State(
							onEnter: OnEnterRunning,
							onLogic: OnLogicRunning,
							onExit: OnExitRunning
						));

			// completion
			fsm.AddState(CompleteActivity, new State(
							onEnter: OnEnterComplete
						));

			// stop
			fsm.AddState(StopActivity, new State(
							onEnter: OnEnterStop
						));


			// add coditionnal trigger bound to ex : OnTransitionRegisterToCreate
			// register to create 
			fsm.AddTransition(new Transition(RegisterActivity, CreateActivity, OnTransitionRegisterToCreate));
			// create to start
			fsm.AddTransition(new Transition(CreateActivity, StartActivity, OnTransitionCreateToStart));
			// start to running (time in a valid range)
			fsm.AddTransition(new Transition(StartActivity, RunningActivity, OnTransitionStartToRunning));
			// start to complete (=> time out)
			fsm.AddTransition(new Transition(StartActivity, CompleteActivity, OnTransitionStartToComplete));
			// running to complete
			fsm.AddTransition(new Transition(RunningActivity, CompleteActivity, OnTransitionRunningToComplete));

			// add manual triggers launched by ex: Trigger(fsm, CompleteActivity, StopActivity);
			AddTriggerTransition(fsm, RegisterActivity, StopActivity);
			AddTriggerTransition(fsm, CreateActivity, StopActivity);
			AddTriggerTransition(fsm, CompleteActivity, StopActivity);
			AddTriggerTransition(fsm, RunningActivity, StopActivity);
			AddTriggerTransition(fsm, CompleteActivity, CreateActivity);

			return fsm;
		}

		#endregion

		// === ENUMERABLE =============================================

		#region ENUMERABLE

		//IEnumerator and IEnumerable require these methods.
		public IEnumerator GetEnumerator()
		{
			return base.GetEnumerator();
		}

		#endregion
	}
}
