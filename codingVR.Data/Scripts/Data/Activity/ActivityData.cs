﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace codingVR.Data
{
	[System.Serializable]
	public class ActivityData : IActivityFieldServer
	{
		// === DATA-PRIVATE =================================================

		#region DATA-PRIVATE

		[SerializeField]
		public string idActivity;

		#endregion

		// === DATA-PRIVATE =================================================

		#region DATA-PRIVATE

		Dictionary<string, FieldInfo> fields = new Dictionary<string, FieldInfo>();
		bool isDirty = true;
		
		#endregion

		// === IMPLEMENTATION =================================================

		#region IMPLEMENTATION

		private string ContainsField(string fieldName, FieldSearchMethod method)
		{
			return ActivityFieldUtilities.ContainsField(idActivity, this, fieldName, method);
		}

		#endregion

		// === IActivityField =================================================

		#region IActivityField

		public bool SetValue(string name, object value, FieldSearchMethod method)
		{
			bool b = ActivityFieldUtilities.SetValue(this, idActivity, this, name, value, method);
			if (b == true)
			{
				isDirty = true;
			}
			return b;
		}

		public bool AddValue(string name, object value, FieldSearchMethod method)
		{
			bool b = ActivityFieldUtilities.AddValue(this, idActivity, this, name, value, method);
			if (b == true)
			{
				isDirty = true;
			}
			return b;
		}

		public bool SubValue(string name, object value, FieldSearchMethod method)
		{
			bool b = ActivityFieldUtilities.SubValue(this, idActivity, this, name, value, method);
			if (b == true)
			{
				isDirty = true;
			}
			return b;
		}

		public bool DivValue(string name, object value, FieldSearchMethod method)
		{
			bool b = ActivityFieldUtilities.DivValue(this, idActivity, this, name, value, method);
			if (b == true)
			{
				isDirty = true;
			}
			return b;
		}

		public bool MulValue(string name, object value, FieldSearchMethod method)
		{
			bool b = ActivityFieldUtilities.MulValue(this, idActivity, this, name, value, method);
			if (b == true)
			{
				isDirty = true;
			}
			return b;
		}

		public bool ResetValue(string name, FieldSearchMethod method)
		{
			bool b = ActivityFieldUtilities.ResetValue(this, idActivity, this, name, method);
			if (b == true)
			{
				isDirty = true;
			}
			return b;
		}

		public object GetValue(string name, FieldSearchMethod method)
		{
			return ActivityFieldUtilities.GetValue(this, idActivity, this, name, method);
		}

		public (bool, bool) CompareValue(string name, object value, FieldComparison comparison, FieldSearchMethod method)
		{
			return ActivityFieldUtilities.CompareValue(this, idActivity, this, name, value, comparison, method);
		}

		#endregion

		// === IActivityFieldServer =================================================

		#region IActivityFieldServer

		public bool IsDirty
		{
			get
			{
				if (isDirty == true)
				{
					isDirty = false;
					return true;
				}
				else
				{
					return false;
				}
			}

			set
			{
				isDirty = value;
			}
		}

		public string GetFullPathField(string fieldName)
		{
			return ActivityFieldUtilities.GetFullPathField(idActivity, fieldName);
		}

		public string Id
		{
			get
			{
				return idActivity;
			}
			set
			{
				idActivity = value;
			}
		}

		public Dictionary<string, FieldInfo> Fields => fields;

		public void PopulateFields()
		{
			var selectFields = BindingFlags.Instance | BindingFlags.Public;
			{
				Type t = GetType();
				while (t != null)
				{
					var fieldInfo = t.GetFields(selectFields);
					foreach (var it in fieldInfo)
					{
						if (fields.ContainsKey(it.Name) == false)
						{
							fields.Add(it.Name, it);
						}
					}
					t = t.BaseType;
				}
			}
		}

		#endregion

		// === ENUMERABLE =============================================

		#region ENUMERABLE

		//IEnumerator and IEnumerable require these methods.
		public IEnumerator GetEnumerator()
		{
			return Fields.GetEnumerator();
		}

		#endregion
	}
}