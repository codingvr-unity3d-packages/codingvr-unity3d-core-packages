﻿#define OPTIMISATION_EXPERIMENTAL

using NaughtyAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Profiling;
using Zenject;

namespace codingVR.Data
{
    [RequireComponent(typeof(global::GameCreator.Behavior.Behavior))]
    public class ActivitySyncingDataWithGCBehavior : MonoBehaviour, Data.IActivityField
    {
        // === DATA-UI ============================================================

        #region DATA-UI

        // Activity Instance

        [Header("Activity Instance")]
        [Tooltip
            (
            "Method 1:\n" +
            "LOCAL PATH\n" +
            "Set the main groupd ID GameObject corresponding to the {activityInstanceID}.\n" +
            "So you can call field as the example below \n" +
            "'completion' <==> '{activityInstanceID}.completion'\n\n\n" +
            "Method 2:\n" +
            "FULL PATH\n" +
            "ex '#[WelcomeGroup].completion\n"
            )]
        [SerializeField]
        GameObject activityInstanceId;

#if UNITY_EDITOR
        [SerializeField]
        [ResizableTextArea]
        string syncingFieldsActivityInstance;
#endif

        // Activity Data

        [Header("Activity Data")]
        [Tooltip
            (
                "Method 1:\n" +
                "LOCAL PATH\n" +
                "Set the main groupd ID GameObject corresponding to the {activityDataID}.\n" +
                "So you can call field as the example below \n" +
                "'description' <==> '{activityDataID}.description'\n\n\n" +
                "Method 2:\n" +
                "LOCAL PATH\n" +
                "Syncing variable with not specific tag class (as #[WelcomeGroup].description)\n" +
                "but declared under the same group (ie description inside groupId)" +
                "group defined both in this Mono and also from the Mono caller 'RunTimeObjects'\n\n\n" +
                "Method 3:\n" +
                "FULL PATH\n" +
                "ex '#[WelcomeGroup].description"
            )]
        [SerializeField]
        GameObject activityDataGroupId;

#if UNITY_EDITOR
        [SerializeField]
        [ResizableTextArea]
        string syncingFieldsActivityData;
#endif

        // Activity Fields Alias

        [Serializable] public class FieldAliasDictionary : Core.cvrSerializableDictionary<string, string> { }

        [Header("Alias To Field")]
        [Tooltip
            (
                "Declaring of alias coresponding to real fields .\n" +
                "Each alias can be used in the Behavior Blackboard \n" +
                "Each alias can be also used in GameCreator Activity field condition and setup \n" +
                "An alias allows to make an abstraction between the real name and a name indenprdently of owner \n"
            )]
        [SerializeField]
        FieldAliasDictionary fieldAliasToFieldDictionary;

        #endregion

        // === DATA-PRIVATE ============================================================

        #region DATA-PRIVATE

        // events
        Core.IEventManager eventManager;
        bool listernerInstalled = false;

        // player
        GameEngine.IPlayerStateActivityController playerStateAcitivty;

#if UNITY_EDITOR
        // logged fields
        Dictionary<int, Dictionary<string, string>> loggedFields = new Dictionary<int, Dictionary<string, string>>();
#endif

        // fields activity data
        Dictionary<string, (UnityEngine.Object, IActivityFieldServer)> activityDataEntryCollection = new Dictionary<string, (UnityEngine.Object, IActivityFieldServer)>();

        // fields activity instance
        Dictionary<string, IActivityFieldServer> activityInstanceEntryCollection = new Dictionary<string, IActivityFieldServer>();

        // local variables
        global::GameCreator.Behavior.Behavior localVariables_Behavior;
        global::GameCreator.Behavior.SetupNodeBehaviorGraph localVariables_SubBehavior;

        // cached group used for improving CPU performances
        // at starting (OnEnable) we check all Activity Instances (a.k.a group) used by GC Behaviour Blackboard
        HashSet<string> activityInstanceElligibleGroups;

        HashSet<string> ActivityInstanceElligibleGroups
        {
            get
            {
                return BuilCacheData_ActivityInstanceElligibleGroups();
            }
        }

        #endregion

        // === LIFE-CYCLE ============================================================

        #region LIFE-CYCLE

        /*
		 *
		 * life cycle
		 * 
		 */

        [Inject]
        public void Constructor(GameEngine.IPlayerStateActivityController playerStateAcitivty, Core.IEventManager eventManager)
        {
            this.playerStateAcitivty = playerStateAcitivty;
            this.eventManager = eventManager;
        }

        // Start is called before the first frame update
        void Start()
        {
        }

        void OnEnable()
        {
            EnableListeners();
            EnableCachedData();
        }

        void OnDisable()
        {
            DisableListeners();
            DisableCachedData();
        }

        void OnDestroy()
        {
        }

        void Update()
        {
        }

        #endregion

        // === LIFE-CYCLE ============================================================

        #region CACHED_DATA
            
        /*
         * 
         * To manage CPU performances
         * We get all Activity Instances (a.k.a group) used by GC Behaviour Blackboard
         * at runtime activityInstanceElligibleGroups is used to check elligible instance 
         * 
         */
        HashSet<string> BuilCacheData_ActivityInstanceElligibleGroups()
        {
            if (activityInstanceElligibleGroups == null && activityInstanceId != null)
            {
                activityInstanceElligibleGroups = new HashSet<string>();
                activityInstanceElligibleGroups.Add(activityInstanceId.name);
                foreach (global::GameCreator.Variables.MBVariable it in localVariables_Behavior.references)
                {
                    var fieldName = ConvertGCToCodingVRVarSyntax(it.variable.name);
                    string groupId = ActivityFieldUtilities.GetIdFromPathField(fieldName);
                    if (groupId != null)
                    {
                        activityInstanceElligibleGroups.Add(groupId);
                    }
                }
            }
            return activityInstanceElligibleGroups;
        }
        
        void DestroyCacheData_ActivityInstanceElligibleGroups()
        {
            if (activityInstanceElligibleGroups != null)
            {
                activityInstanceElligibleGroups.Clear();
                activityInstanceElligibleGroups = null;
            }
        }

        void BuildCacheData_Components()
        {
            localVariables_Behavior = GetComponent<global::GameCreator.Behavior.Behavior>();
            localVariables_SubBehavior = GetComponent<global::GameCreator.Behavior.SetupNodeBehaviorGraph>();
        }

        void DestroyCacheData_Components()
        {
            localVariables_Behavior = GetComponent<global::GameCreator.Behavior.Behavior>();
            localVariables_SubBehavior = GetComponent<global::GameCreator.Behavior.SetupNodeBehaviorGraph>();
        }

        void EnableCachedData()
        {
            BuildCacheData_Components();
        }

        void DisableCachedData()
        {
            DestroyCacheData_Components();
            DestroyCacheData_ActivityInstanceElligibleGroups();
        }

        #endregion

        // === IMPLEMENTATION-UTILS ============================================================

        #region IMPLEMENTATION-UTILS

        bool ContainsIdInstanceActivity(string groupTagName)
        {
            return activityInstanceId.name.Contains(groupTagName);
        }

        bool IsThisGroup(GameEngine.Event_ScreenPlayTagBase ev)
        {
            bool b = false;
            if (activityInstanceId != null)
            {
                var groupData = ev.Data as Tuple<string, Data.Tag>;
                b = ContainsIdInstanceActivity(groupData.Item2.ToString());
            }
            return b;
        }

        global::GameCreator.Variables.Variable.DataType ConvertDataType(System.Type typeActivityVar)
        {
            global::GameCreator.Variables.Variable.DataType typeGCVar = global::GameCreator.Variables.Variable.DataType.Null;
            var typeCodeActivity = System.Type.GetTypeCode(typeActivityVar);
            switch (typeCodeActivity)
            {
                case System.TypeCode.Empty:
                    typeGCVar = global::GameCreator.Variables.Variable.DataType.Null;
                    break;
                case System.TypeCode.Object:
                    break;
                case System.TypeCode.DBNull:
                    break;
                case System.TypeCode.Boolean:
                    typeGCVar = global::GameCreator.Variables.Variable.DataType.Bool;
                    break;
                case System.TypeCode.Char:
                    typeGCVar = global::GameCreator.Variables.Variable.DataType.Number;
                    break;
                case System.TypeCode.SByte:
                    typeGCVar = global::GameCreator.Variables.Variable.DataType.Number;
                    break;
                case System.TypeCode.Byte:
                    typeGCVar = global::GameCreator.Variables.Variable.DataType.Number;
                    break;
                case System.TypeCode.Int16:
                    typeGCVar = global::GameCreator.Variables.Variable.DataType.Number;
                    break;
                case System.TypeCode.UInt16:
                    typeGCVar = global::GameCreator.Variables.Variable.DataType.Number;
                    break;
                case System.TypeCode.Int32:
                    typeGCVar = global::GameCreator.Variables.Variable.DataType.Number;
                    break;
                case System.TypeCode.UInt32:
                    typeGCVar = global::GameCreator.Variables.Variable.DataType.Number;
                    break;
                case System.TypeCode.Int64:
                    typeGCVar = global::GameCreator.Variables.Variable.DataType.Number;
                    break;
                case System.TypeCode.UInt64:
                    typeGCVar = global::GameCreator.Variables.Variable.DataType.Number;
                    break;
                case System.TypeCode.Single:
                    typeGCVar = global::GameCreator.Variables.Variable.DataType.Number;
                    break;
                case System.TypeCode.Double:
                    typeGCVar = global::GameCreator.Variables.Variable.DataType.Number;
                    break;
                case System.TypeCode.Decimal:
                    typeGCVar = global::GameCreator.Variables.Variable.DataType.Number;
                    break;
                case System.TypeCode.DateTime:
                    break;
                case System.TypeCode.String:
                    typeGCVar = global::GameCreator.Variables.Variable.DataType.String;
                    break;
                default:
                    typeGCVar = global::GameCreator.Variables.Variable.DataType.Null;
                    break;
            }
            return typeGCVar;
        }

        private static readonly Regex REGEX_VARNAME = new Regex(@"[^\p{L}\p{Nd}-_]");

        string ConvertCodingVRToGCVarSyntax(string name)
        {
            string processed = name.Trim();
            processed = REGEX_VARNAME.Replace(processed, "-");
            return processed;
        }

        string ConvertGCToCodingVRVarSyntax(string name)
        {
            string processed = name.Trim();
            int i0 = processed.IndexOf("--");
            int i1 = processed.LastIndexOf("--");
            if (i0 == 0 && i1 > 1 && i1+2 < processed.Length)
            {
                processed = processed.Insert(i1, "].");
                processed = processed.Insert(i0, "#[");
                processed = processed.Replace("--", "");
            }
            return processed;
        }

        #endregion

        // === TRIGGERING ==============================================================================================================

        #region TRIGGERING

        /*
		 *
		 * Triggering
		 * 
		 */

        private void EnableListeners()
        {
            // listener on game event
            if (eventManager != null && listernerInstalled == false)
            {
                // activity data
                eventManager.AddListener<Data.Event_PlayerStateActivityDataChange>(OnActivityDataChange);
                // activity instance
                eventManager.AddListener<Data.Event_PlayerStateActivityRegister>(OnActivityRegister);
                eventManager.AddListener<Data.Event_PlayerStateActivityCreate>(OnActivityCreate);
                eventManager.AddListener<Data.Event_PlayerStateActivityStart>(OnActivityStart);
                eventManager.AddListener<Data.Event_PlayerStateActivityComplete>(OnActivityComplete);
                eventManager.AddListener<Data.Event_PlayerStateActivityStop>(OnActivityStop);
                eventManager.AddListener<Data.Event_PlayerStateActivityInstanceChange>(OnActivityInstanceChange);
                // screen play tag group
                eventManager.AddListener<GameEngine.Event_ScreenPlayTagGroupEnter>(OnScreenPlayTagGroupEnter);
                eventManager.AddListener<GameEngine.Event_ScreenPlayTagGroupLeave>(OnScreenPlayTagGroupLeave);

                listernerInstalled = true;
            }
        }

        private void DisableListeners()
        {
            // remove listener on game event
            if (eventManager != null && listernerInstalled == true)
            {
                // activity data
                eventManager.RemoveListener<Data.Event_PlayerStateActivityDataChange>(OnActivityDataChange);
                // activity instance
                eventManager.RemoveListener<Data.Event_PlayerStateActivityRegister>(OnActivityRegister);
                eventManager.RemoveListener<Data.Event_PlayerStateActivityCreate>(OnActivityCreate);
                eventManager.RemoveListener<Data.Event_PlayerStateActivityStart>(OnActivityStart);
                eventManager.RemoveListener<Data.Event_PlayerStateActivityComplete>(OnActivityComplete);
                eventManager.RemoveListener<Data.Event_PlayerStateActivityStop>(OnActivityStop);
                eventManager.RemoveListener<Data.Event_PlayerStateActivityInstanceChange>(OnActivityInstanceChange);
                // screen play tag group
                eventManager.RemoveListener<GameEngine.Event_ScreenPlayTagGroupEnter>(OnScreenPlayTagGroupEnter);
                eventManager.RemoveListener<GameEngine.Event_ScreenPlayTagGroupLeave>(OnScreenPlayTagGroupLeave);

                listernerInstalled = false;
            }
        }

        private void OnActivityRegister(Data.Event_PlayerStateActivityRegister ev)
        {
            OnActivityChange(ev);
        }

        private void OnActivityCreate(Data.Event_PlayerStateActivityCreate ev)
        {
            OnActivityChange(ev);
        }

        private void OnActivityStart(Data.Event_PlayerStateActivityStart ev)
        {
            OnActivityChange(ev);
        }

        private void OnActivityComplete(Data.Event_PlayerStateActivityComplete ev)
        {
            OnActivityChange(ev);
        }

        private void OnActivityStop(Data.Event_PlayerStateActivityStop ev)
        {
            OnActivityChange(ev);
        }

        private void OnActivityChange(Data.Event_PlayerStateActivityBase ev)
        {
            object GetStatusFlagsValue(object activityInstance)
            {
                return (activityInstance as IActivityInstance).StatusFlags.ToString();
            }

            SyncActivityInstanceField(activityInstance: playerStateAcitivty.FindActivityInstance(ev.Data as string), typeof(string), "statusFlags", GetStatusFlagsValue);
#if UNITY_EDITOR
            syncingFieldsActivityInstance = PrintSyncingFields(0);
#endif
        }

        private void OnActivityDataChange(Data.Event_PlayerStateActivityDataChange ev)
        {
            SyncActivityDataFields(((UnityEngine.Object, IActivityFieldServer))ev.Data);
        }

        private void OnActivityInstanceChange(Data.Event_PlayerStateActivityInstanceChange ev)
        {
            var data = ((string, IActivityFieldServer))ev.Data;
            SyncActivityInstanceFields(data.Item2);
        }

        void OnScreenPlayTagGroupEnter(GameEngine.Event_ScreenPlayTagGroupEnter ev)
        {
            if (IsThisGroup(ev))
            {
                PopulateActivityInstanceFields();
            }
        }

        void OnScreenPlayTagGroupLeave(GameEngine.Event_ScreenPlayTagGroupLeave ev)
        {
            if (IsThisGroup(ev))
            {
            }
        }

        #endregion


        // === IMPLEMENTATION ==============================================================================================================

        #region IMPLEMENTATION

        /*
		 * 
		 * Activity Fields Alias
		 * 
		 */

        string ApplyFieldToAlias(string fieldName)
        {
            foreach (var it in fieldAliasToFieldDictionary)
            {
                if (it.Value.Equals(fieldName))
                {
                    return it.Key;
                }
            }
            return fieldName;
        }

        string ApplyAliasToField(string aliasName)
        {
            if (fieldAliasToFieldDictionary.Contains(aliasName))
            {
                return fieldAliasToFieldDictionary[aliasName];
            }
            return aliasName;
        }


        /*
		* 
		* Logging
		*
		*/

        private void LogSyncingFields(int entry, string nameField0, Type typeField0, string nameField1, int typeField1)
        {
#if UNITY_EDITOR
            if (loggedFields.ContainsKey(entry) == false)
            {
                loggedFields.Add(entry, new Dictionary<string, string>());
            }

            var fieldsEntry = loggedFields[entry];
            if (fieldsEntry.ContainsKey(nameField0) == false)
            {
                string value = nameField0 + ":" + typeField0 + "   ->   " + nameField1 + ":" + (global::GameCreator.Variables.Variable.DataType)typeField1;
                fieldsEntry.Add(nameField0, value);
            }
#endif
        }

        private string PrintSyncingFields(int entry)
        {
            string ret = "";
#if UNITY_EDITOR
            if (loggedFields.ContainsKey(entry) == true)
            {
                var fieldsEntry = loggedFields[entry];
                if (fieldsEntry.Count > 0)
                {
                    foreach (var it in fieldsEntry)
                    {
                        ret += it.Value + "\n";
                    }
                }
            }
#endif
            return ret;
        }


        /*
		 * 
		 * Activity Instance Fields
		 * 
		 */

        private void AddActivityInstance(Data.IActivityFieldServer data)
        {
            if (activityInstanceEntryCollection.ContainsKey(data.Id) == false)
            {
                activityInstanceEntryCollection.Add(data.Id, data);
            }
        }

        private void PopulateActivityInstanceFields()
        {
            // TODO: OPTIMISATION REQUIRED
            Profiler.BeginSample("ActivitySyncingDataWithGCBehavior.PopulateActivityInstanceFields");

            foreach (var instance in playerStateAcitivty.GetActivityInstanceKeys(GameEngine.RelevantActivityFlags.all))
            {
                var activityInstance = playerStateAcitivty.FindActivityInstance(instance);
                if (activityInstance != null)
                {
                    SyncActivityInstanceFields(activityInstance);
                }
            }

            // TODO: OPTIMISATION REQUIRED
            Profiler.EndSample();
        }

        private void SyncActivityInstanceFields(IActivityFieldServer activityInstance)
        {
#if OPTIMISATION_EXPERIMENTAL
            if (ActivityInstanceElligibleGroups?.Contains(activityInstance.Id) ?? false)
#endif
            {
                foreach (Field field in activityInstance)
                {
                    SyncActivityInstanceField(activityInstance: activityInstance, type: field.fieldInfo.FieldType, fieldName: field.fieldInfo.Name, getFieldValue: field.getFieldValue);
                }
#if UNITY_EDITOR
                syncingFieldsActivityInstance = PrintSyncingFields(0);
#endif
            }
        }

        private void SyncActivityInstanceField(IActivityFieldServer activityInstance, Type type, string fieldName, GetFieldValue getFieldValue)
        {
            // TODO: OPTIMISATION REQUIRED
            Profiler.BeginSample("ActivitySyncingDataWithGCBehavior.SyncActivityInstanceField");

            // sync variables from main local variables (Behavior)
            {
                SyncActivityInstanceField(localVariables_Behavior, activityInstance, type, fieldName, getFieldValue);
            }

            // sync variable from optional sub behavior
            {
                if (localVariables_SubBehavior != null)
                {
                    SyncActivityInstanceField(localVariables_SubBehavior, activityInstance, type, fieldName, getFieldValue);
                }
            }

            // TODO: OPTIMISATION REQUIRED
            Profiler.EndSample();
        }

        private void SyncActivityInstanceField(global::GameCreator.Variables.LocalVariables localVariables, IActivityFieldServer activityInstance, Type type, string fieldName, GetFieldValue getFieldValue)
        {
            if (localVariables.references.Length > 0) // count of var in blackboard
            {
                // apply value to GameCreator variable
                global::GameCreator.Variables.Variable SetValueToGC(global::GameCreator.Variables.LocalVariables _localVariables, Type _localType, string _key, object _value)
                {
                    var localVariable = _localVariables.Get(ConvertCodingVRToGCVarSyntax(ApplyFieldToAlias(_key)));
                    if (localVariable != null)
                    {
                        var typeGC = ConvertDataType(_localType);
                        if (typeGC != global::GameCreator.Variables.Variable.DataType.Null)
                        {
                            localVariable.Set(typeGC, _value);
                        }
                        else
                        {
                            Debug.LogErrorFormat("{0} : impossible to convert data type for GC {1} => {2} ", System.Reflection.MethodBase.GetCurrentMethod().Name, typeof(string), localVariable);
                        }
                    }
                    return localVariable;
                }

                // sync GC/Activity Field
                if (activityInstance != null)
                {
                    string groupId = this.activityInstanceId != null ? this.activityInstanceId.name : "";

                    global::GameCreator.Variables.Variable localVariable = null;

                    if (activityInstance.Id.Equals(groupId) == true)
                    {
                        // syncing simple variable with
                        localVariable = SetValueToGC(localVariables, type, fieldName, getFieldValue(activityInstance));
                    }

                    if (localVariable == null)
                    {
                        // syncing variable with a specific tag class (as #tagGroup.foo); 
                        fieldName = activityInstance.GetFullPathField(fieldName);
                        localVariable = SetValueToGC(localVariables, type, fieldName, getFieldValue(activityInstance));
                    }

#if UNITY_EDITOR
                    if (localVariable != null)
                    {
                        LogSyncingFields(0, fieldName, type, localVariable.name, localVariable.type);
                    }
#endif

                    if (localVariable != null)
                    {
                        AddActivityInstance(activityInstance);
                    }
                }
            }
        }

        /*
		 * 
		 * Activity Data Fields
		 * 
		 */

        private void AddActivityData((UnityEngine.Object, IActivityFieldServer) data)
        {
            if (activityDataEntryCollection.ContainsKey(data.Item2.Id) == false)
            {
                activityDataEntryCollection.Add(data.Item2.Id, data);
            }
        }

        private void SyncActivityDataFields((UnityEngine.Object, IActivityFieldServer) data)
        {
            // sync variables from main behavior
            {
                SyncActivityDataFields(localVariables_Behavior, data);
            }

            // sync variable from optional sub behavior
            {
                if (localVariables_SubBehavior != null)
                {
                    SyncActivityDataFields(localVariables_SubBehavior, data);
                }
            }
        }

        private void SyncActivityDataFields(global::GameCreator.Variables.LocalVariables localVariables, (UnityEngine.Object, IActivityFieldServer) data)
        {
            if (localVariables.references.Length > 0) // count of var in blackboard
            {
                // apply value to GC
                global::GameCreator.Variables.Variable SetValueToGC(global::GameCreator.Variables.LocalVariables _localVariables, IActivityFieldServer _localData, string _key, Type _type)
                {
                    var localVariable = _localVariables.Get(ConvertCodingVRToGCVarSyntax(ApplyFieldToAlias(_key)));
                    if (localVariable != null)
                    {
                        var typeGC = ConvertDataType(_type);
                        if (typeGC != global::GameCreator.Variables.Variable.DataType.Null)
                        {
                            localVariable.Set(typeGC, _localData.GetValue(_key, FieldSearchMethod.any));
                        }
                        else
                        {
                            Debug.LogErrorFormat("{0} : impossible to convert data type for GC {1} => {2} ", System.Reflection.MethodBase.GetCurrentMethod().Name, _type, localVariable);
                        }
                    }
                    return localVariable;
                }

                // sync GC/Activity
                int eventGroupIdInstance = data.Item1.GetInstanceID();
                int groupIdInstance = this.activityDataGroupId != null ? this.activityDataGroupId.GetInstanceID() : -1;
                string groupIdName = this.activityDataGroupId != null ? this.activityDataGroupId.name : null;
                string idActivity = data.Item2.Id;
                int countVarFound = 0;

                foreach (var it in data.Item2.Fields)
                {
                    var key = it.Key;

                    global::GameCreator.Variables.Variable localVariable = null;

                    // syncing variable with not specific tag class (as #tagGroup.toto); 
                    // but declared under the same group (ie completion inside groupId)
                    // group defined both in this Mono Instance and also from the caller
                    if (eventGroupIdInstance == groupIdInstance)
                    {
                        localVariable = SetValueToGC(localVariables, data.Item2, key, it.Value.FieldType);
                    }
                    // syncing variable with a complete tag path (as #tagGroup.toto); 
                    if (localVariable == null)
                    {
                        key = data.Item2.GetFullPathField(key);
                        localVariable = SetValueToGC(localVariables, data.Item2, key, it.Value.FieldType);
                    }
                    // syncing by comparing group defined in this class wth the activity Id
                    if (localVariable == null)
                    {
                        if (groupIdName != null && idActivity.Equals(groupIdName) == true)
                        {
                            localVariable = SetValueToGC(localVariables, data.Item2, key, it.Value.FieldType);
                        }
                    }

                    // variable found
                    if (localVariable != null)
                    {
                        countVarFound++;
#if UNITY_EDITOR
                        // logs
                        LogSyncingFields(1, key, it.Value.FieldType, localVariable.name, localVariable.type);
#endif
                    }
                }

                // add fields
                if (countVarFound > 0)
                {
                    AddActivityData(data);
                }

                //logs
#if UNITY_EDITOR
                syncingFieldsActivityData = PrintSyncingFields(1);
#endif
            }
        }

        #endregion

        // === IActivityField ===================================================================================================

        /*
         * 
         * SetValue
         * 
         */

        delegate bool op(IActivityField activityData, string name, object value, FieldSearchMethod method);

        bool OperationValueOnActivityInstance(string name, object value, op op, FieldSearchMethod method)
        {
            name = ApplyAliasToField(name);

            foreach (var activityInstanceEntry in activityInstanceEntryCollection)
            {
                var activityInstance = activityInstanceEntry.Value;

                // sync GC/Activity Field
                if (method == FieldSearchMethod.defaultMethod)
                {
                    string groupId = this.activityInstanceId != null ? this.activityInstanceId.name : "";

                    if (activityInstance.Id.Equals(groupId) == true)
                    {
                        if (op(activityInstance, name, value, FieldSearchMethod.localPath))
                        {
                            return true;
                        }
                    }

                    // syncing variable with a specific tag class (as #tagGroup.foo); 
                    {
                        if (op(activityInstance, name, value, FieldSearchMethod.fullPath))
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    if (op(activityInstance, name, value, method))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        bool OperationValueOnActivityData(string name, object value, op op, FieldSearchMethod method)
        {
            name = ApplyAliasToField(name);

            foreach (var activityDataEntry in activityDataEntryCollection)
            {
                var activityData = activityDataEntry.Value.Item2;

                var eventGroupIdInstance = activityDataEntry.Value.Item1.GetInstanceID();
                int activityDataGroupIdInstance = this.activityDataGroupId != null ? this.activityDataGroupId.GetInstanceID() : -1;
                string activityDataIdActivity = activityData.Id;
                string activityDataGroupIdName = this.activityDataGroupId != null ? this.activityDataGroupId.name : null;

                if (method == FieldSearchMethod.defaultMethod)
                {
                    // syncing variable with not specific tag class (as #tagGroup.toto); 
                    // but declared under the same group (ie toto insinde groupId)
                    // group defined both in this Mono Instance and also from the caller
                    if (eventGroupIdInstance == activityDataGroupIdInstance)
                    {
                        if (op(activityData, name, value, FieldSearchMethod.localPath))
                        {
                            return true;
                        }
                    }

                    // syncing variable with a complete tag path (as #tagGroup.toto); 
                    {
                        if (op(activityData, name, value, FieldSearchMethod.fullPath))
                        {
                            return true;
                        }
                    }
                    // syncing by comparing group defined in this class wth the activity Id
                    if (activityDataGroupIdName != null && activityDataIdActivity.Equals(activityDataGroupIdName) == true)
                    {
                        if (op(activityData, name, value, FieldSearchMethod.localPath))
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    if (op(activityData, name, value, method))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool SetValue(string name, object value, FieldSearchMethod method)
        {
            bool op(IActivityField _activityData, string _name, object _value, FieldSearchMethod _method)
            {
                return _activityData.SetValue(_name, _value, _method);
            }
            bool ret = OperationValueOnActivityInstance(name, value, op, method);
            if (ret == false)
            {
                ret = OperationValueOnActivityData(name, value, op, method);
            }
            return ret;
        }

        public bool AddValue(string name, object value, FieldSearchMethod method)
        {
            bool op(IActivityField _activityData, string _name, object _value, FieldSearchMethod _method)
            {
                return _activityData.AddValue(_name, _value, _method);
            }
            bool ret = OperationValueOnActivityInstance(name, value, op, method);
            if (ret == false)
            {
                ret = OperationValueOnActivityData(name, value, op, method);
            }
            return ret;
        }

        public bool SubValue(string name, object value, FieldSearchMethod method)
        {
            bool op(IActivityField _activityData, string _name, object _value, FieldSearchMethod _method)
            {
                return _activityData.SubValue(_name, _value, _method);
            }
            bool ret = OperationValueOnActivityInstance(name, value, op, method);
            if (ret == false)
            {
                ret = OperationValueOnActivityData(name, value, op, method);
            }
            return ret;
        }

        public bool MulValue(string name, object value, FieldSearchMethod method)
        {
            bool op(IActivityField _activityData, string _name, object _value, FieldSearchMethod _method)
            {
                return _activityData.MulValue(_name, _value, _method);
            }
            bool ret = OperationValueOnActivityInstance(name, value, op, method);
            if (ret == false)
            {
                ret = OperationValueOnActivityData(name, value, op, method);
            }
            return ret;
        }

        public bool DivValue(string name, object value, FieldSearchMethod method)
        {
            bool op(IActivityField _activityData, string _name, object _value, FieldSearchMethod _method)
            {
                return _activityData.DivValue(_name, _value, _method);
            }
            bool ret = OperationValueOnActivityInstance(name, value, op, method);
            if (ret == false)
            {
                ret = OperationValueOnActivityData(name, value, op, method);
            }
            return ret;
        }

        public bool ResetValue(string name, FieldSearchMethod method)
        {
            bool op(IActivityField _activityData, string _name, object _value, FieldSearchMethod _method)
            {
                return _activityData.ResetValue(_name, _method);
            }
            bool ret = OperationValueOnActivityInstance(name, null, op, method);
            if (ret == false)
            {
                ret = OperationValueOnActivityData(name, null, op, method);
            }
            return ret;
        }

        /*
		 * 
		 * GetValue
		 * 
		 */

        public object GetValueOnActivityInstance(string name, FieldSearchMethod method)
        {
            name = ApplyAliasToField(name);

            object value = null;
            foreach (var activityInstanceEntry in activityInstanceEntryCollection)
            {
                var activityInstance = activityInstanceEntry.Value;

                // sync GC/Activity Field
                if (method == FieldSearchMethod.defaultMethod)
                {
                    string groupId = this.activityInstanceId != null ? this.activityInstanceId.name : "";

                    if (activityInstance.Id.Equals(groupId) == true)
                    {
                        value = activityInstance.GetValue(name, FieldSearchMethod.localPath);
                        if (value != null)
                        {
                            break;
                        }
                    }

                    // syncing variable with a specific tag class (as #tagGroup.foo); 
                    {
                        value = activityInstance.GetValue(name, FieldSearchMethod.fullPath);
                        if (value != null)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    value = activityInstance.GetValue(name, method);
                    if (value != null)
                    {
                        break;
                    }
                }
            }
            return value;
        }

        public object GetValueOnActivityData(string name, FieldSearchMethod method)
        {
            name = ApplyAliasToField(name);

            object value = null;
            foreach (var activityDataEntry in activityDataEntryCollection)
            {
                var activityData = activityDataEntry.Value.Item2;

                var eventGroupIdInstance = activityDataEntry.Value.Item1.GetInstanceID();
                int groupIdInstance = this.activityDataGroupId != null ? this.activityDataGroupId.GetInstanceID() : -1;
                string idActivity = activityData.Id;
                string groupIdName = this.activityDataGroupId != null ? this.activityDataGroupId.name : null;


                if (method == FieldSearchMethod.defaultMethod)
                {
                    // syncing variable with not specific tag class (as #tagGroup.toto); 
                    // but declared under the same group (ie toto insinde groupId)
                    // group defined both in this Mono Instance and also from the caller
                    if (eventGroupIdInstance == groupIdInstance)
                    {
                        value = activityData.GetValue(name, FieldSearchMethod.localPath);
                        if (value != null)
                        {
                            break;
                        }
                    }

                    // syncing variable with a complete tag path (as #tagGroup.toto); 
                    {
                        value = activityData.GetValue(name, FieldSearchMethod.fullPath);
                        if (value != null)
                        {
                            break;
                        }
                    }

                    // syncing by comparing group defined in this class wth the activity Id
                    if (groupIdName != null && idActivity.Equals(groupIdName) == true)
                    {
                        value = activityData.GetValue(name, FieldSearchMethod.localPath);
                        if (value != null)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    value = activityData.GetValue(name, method);
                    if (value != null)
                    {
                        break;
                    }
                }
            }
            return value;
        }

        public object GetValue(string name, FieldSearchMethod method)
        {
            object value = GetValueOnActivityInstance(name, method);
            if (null != value)
            {
                value = GetValueOnActivityData(name, method);
            }
            return value;
        }

        /*
		 * 
		 * CompareValue
		 * 
		 */

        public (bool, bool) CompareValueOnActivityInstance(string name, object value, FieldComparison comparison, FieldSearchMethod method)
        {
            name = ApplyAliasToField(name);

            (bool, bool) result = (false, false);
            foreach (var activityInstanceEntry in activityInstanceEntryCollection)
            {
                var activityInstance = activityInstanceEntry.Value;

                // sync GC/Activity Field
                if (method == FieldSearchMethod.defaultMethod)
                {
                    string groupId = this.activityInstanceId != null ? this.activityInstanceId.name : "";

                    if (activityInstance.Id.Equals(groupId) == true)
                    {
                        result = activityInstance.CompareValue(name, value, comparison, FieldSearchMethod.localPath);
                        if (result.Item1 == true)
                        {
                            break;
                        }
                    }

                    // syncing variable with a specific tag class (as #tagGroup.foo); 
                    {
                        result = activityInstance.CompareValue(name, value, comparison, FieldSearchMethod.fullPath);
                        if (result.Item1 == true)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    result = activityInstance.CompareValue(name, value, comparison, method);
                    if (result.Item1 == true)
                    {
                        break;
                    }
                }
            }
            return result;
        }


        public (bool, bool) CompareValueOnActivityData(string name, object value, FieldComparison comparison, FieldSearchMethod method)
        {
            name = ApplyAliasToField(name);

            (bool, bool) result = (false, false);
            foreach (var activityDataEntry in activityDataEntryCollection)
            {
                var activityData = activityDataEntry.Value.Item2;

                var eventGroupIdInstance = activityDataEntry.Value.Item1.GetInstanceID();
                int groupIdInstance = this.activityDataGroupId != null ? this.activityDataGroupId.GetInstanceID() : -1;
                string idActivity = activityData.Id;
                string groupIdName = this.activityDataGroupId != null ? this.activityDataGroupId.name : null;


                if (method == FieldSearchMethod.defaultMethod)
                {
                    // syncing variable with not specific tag class (as #tagGroup.toto); 
                    // but declared under the same group (ie toto insinde groupId)
                    // group defined both in this Mono Instance and also from the caller
                    if (eventGroupIdInstance == groupIdInstance)
                    {
                        result = activityData.CompareValue(name, value, comparison, FieldSearchMethod.localPath);
                        if (result.Item1 == true)
                        {
                            break;
                        }
                    }

                    // syncing variable with a complete tag path (as #tagGroup.toto); 
                    {
                        result = activityData.CompareValue(name, value, comparison, FieldSearchMethod.fullPath);
                        if (result.Item1 == true)
                        {
                            break;
                        }
                    }

                    // syncing by comparing group defined in this class wth the activity Id
                    if (groupIdName != null && idActivity.Equals(groupIdName) == true)
                    {
                        result = activityData.CompareValue(name, value, comparison, FieldSearchMethod.localPath);
                        if (result.Item1 == true)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    result = activityData.CompareValue(name, value, comparison, method);
                    if (result.Item1 == true)
                    {
                        break;
                    }
                }
            }
            return result;
        }

        public (bool, bool) CompareValue(string name, object value, FieldComparison comparison, FieldSearchMethod method)
        {
            var result = CompareValueOnActivityInstance(name, value, comparison, method);
            if (result.Item1 == false)
            {
                result = CompareValueOnActivityData(name, value, comparison, method);
            }
            return result;
        }
    }
}