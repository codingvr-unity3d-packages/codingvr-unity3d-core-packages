﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Scripting.APIUpdating;

namespace codingVR.Data
{
	[Serializable]
	[MovedFrom(false, null, null, "PlayerStateActivityClassData")]
	public class ActivityClass : ActivityFieldManager, IActivityClass
	{
		// === DATA-UI ================================================

		#region DATA-UI

		// identification
		[SerializeField]
		public string className;
		// description
		[SerializeField]
		public string description;
		// importance
		[SerializeField]
		public ActivityClassImportance importance;
		//Regularity
		[SerializeField]
		public ActivityClassRegularity regularity;
		//Size
		public ActivityClassSize size;
		// properties
		[SerializeField]
		public ActivityClassPropertyFlags propertyFlags;
		// duration of activity
		// 0 : for infinite duration time
		[SerializeField]
		public int duration = 86400;
		// delay duration before starting
		[SerializeField]
		public int delay = 0;
		// point count provided by this kind of activity
		[SerializeField]
		public int points = 0;
		// Passage
		[SerializeField]
		public int classPassage = 0;


		/*
		 *
		 * behavior
		 * 
		 */

		/*
		https://forum.unity.com/threads/add-a-custom-component-by-searching-for-its-name.323361/
		see also CityPartpart = UnityEngineInternal.APIUpdaterRuntimeServices.AddComponent(noob, "Assets/Scripts/CityParts/CityPart.cs (33,19)", className) asCityPart;
		 void Start () {
				//We have a string holding a script name
				String ScriptName = "YourScriptNameWithoutExtension";
				//We need to fetch the Type
				System.Type MyScriptType = System.Type.GetType (ScriptName + ",Assembly-CSharp");
				//Now that we have the Type we can use it to Add Component
				gameObject.AddComponent (MyScriptType);
			}
		*/
		//public System.Type scriptComponent;
		//public MonoBehaviour behaviour;

		// gameObject
		[SerializeField]
		private GameObject activityClassBehavior;

		#endregion

		// === INTERFACE =============================================

		#region INTERFACE

		// ==== Constructor & Setup ========================================================================================
		
		// constructor
		protected ActivityClass() : base()
		{
		}

		// constructor
		protected ActivityClass(string className,string description, ActivityClassImportance importance, ActivityClassRegularity regularity, ActivityClassSize size,int duration, int delay,int points, GameObject activityClassBehavior) : base()
		{
			this.className = className;
			this.description = description;
			this.importance = importance;
			this.regularity = regularity;
			this.size = size;
			this.duration = duration;
			this.delay = delay;
			this.points = points;
			this.propertyFlags = ActivityClassPropertyFlags.none;
			this.activityClassBehavior = activityClassBehavior;
			this.classPassage = 0;
		}

		// creation
		static public ActivityClass Create(string className, string description, ActivityClassImportance importance, ActivityClassRegularity regularity, ActivityClassSize size, int duration, int delay, int points, GameObject activityClassBehavior)
		{
			ActivityClass playerStateActivityInstanceData = new ActivityClass(className,description,importance,regularity,size,duration,delay,points, activityClassBehavior);
			return playerStateActivityInstanceData;
		}

		#endregion

		// === IActivityClass ================================================

		#region IActivityClass

		// identification
		public string ClassName => className;
		// description
		public string Description => description;
		// importance
		public ActivityClassImportance Importance => importance;
		//Regularity
		public ActivityClassRegularity Regularity => regularity;
		//Size
		public ActivityClassSize Size => size;
		// properties
		public ActivityClassPropertyFlags PropertyFlags => propertyFlags;
		// duration of activity
		// 0 : for infinite duration time
		public int Duration => duration;
		// delay duration before starting
		public int Delay => delay;
		// point count provided by this kind of activity
		public int Points => points;
		// Passage
		public int ClassPassage
		{
			get
			{
				return classPassage;
			}
			set
			{
				classPassage = value;
			}
		}
		// activity class behavior
		public GameObject ActivityClassBehavior => activityClassBehavior;

		#endregion

		// === ENUMERABLE =============================================

		#region ENUMERABLE

		//IEnumerator and IEnumerable require these methods.
		public IEnumerator GetEnumerator()
		{
			return base.GetEnumerator();
		}

		#endregion
	}
}