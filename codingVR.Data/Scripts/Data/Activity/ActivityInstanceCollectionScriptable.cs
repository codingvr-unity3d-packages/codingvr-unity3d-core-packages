﻿using codingVR.Core;
using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Scripting.APIUpdating;
using Zenject;

namespace codingVR.Data
{
	[Serializable] public class ActivityInstanceDictionary : Core.cvrSerializableDictionary<string, ActivityInstance> { }
	
	#region PlayerStateActivityInstanceCollectionDataScriptable

	[Serializable]
	[CreateAssetMenu(fileName = "PlayerStateActivityInstanceCollectionData", menuName = "CodingVR/Data/PlayerState/ActivityInstanceCollectionData", order = 2)]
	[MovedFrom(false, null, null, "PlayerStateActivityInstanceCollectionDataScriptable")]
	public class ActivityInstanceCollectionScriptable : ActivityFieldScriptable, IActivtyInstanceCollection
	{
		// === DATA-UI =========================================================

		#region DATA-UI

		[Header("Activities")]
		[HorizontalLine(color: EColor.Black)]
		[SerializeField]
		public ActivityInstanceDictionary data = new ActivityInstanceDictionary();

		[Header("Player")]
		[HorizontalLine(color: EColor.Black)]
		[SerializeField]
		string idPlayer;

		[SerializeField]
		public string IdPlayer
		{
			get
			{
				return idPlayer;
			}
			set
			{
				idPlayer = value;
			}
		}

		[Header("Current activity")]
		[HorizontalLine(color: EColor.Black)]
		[SerializeField]
		string currentActivity;

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		private bool RegisterCurrentActivity()
		{
			return RegisterActivity(currentActivity, true);
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		private bool CreateCurrentActivity()
		{
			return CreateActivity(currentActivity, true);
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		private bool StartCurrentActivity()
		{
			return StartActivity(currentActivity, true);
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		private bool CompleteCurrentActivity()
		{
			return CompleteActivity(currentActivity, true);
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		private bool StopCurrentActivity()
		{
			return StopActivity(currentActivity, true);
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		private void ResetCurrentActivityGameplay()
		{
			ResetActivityGameplay(currentActivity);
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		private void ResetCurrentActivityDefault()
		{
			ResetActivityDefault(currentActivity);
		}

		[ShowNativeProperty]
		private int CurrentActivityTime
		{
			get
			{
				var activity = FindActivityInstance(currentActivity);
				if (null != activity)
				{
					return activity.LocalActivityTime;
				}
				return 0;
			}
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		private void ResetStartTimeCurrentActivity()
		{
			var activity = FindActivityInstance(currentActivity);
			if (null != activity)
			{
				activity.ResetActivityTime();
			}
		}

		#endregion

		// === DATA-PRIVATE =========================================================

		#region DATA-PRIVATE

		bool isDirty = false;

		GameEngine.IPlayerState playerState;
		Data.INetworkSession<Data.PlayerStateActivityInstanceCollectionStructSpec, Data.PlayerStateActivityInstanceCollectionStructSpec> networkSession;

		#endregion

		// === UNITY-LIFECYCLE =========================================================

		#region UNITY-LIFECYCLE

		[Inject]
		public void Constructor(codingVR.GameEngine.IPlayerState playerState, Data.INetworkSession<Data.PlayerStateActivityInstanceCollectionStructSpec, Data.PlayerStateActivityInstanceCollectionStructSpec> networkSession)
		{
			// interface
			this.playerState = playerState;
			this.networkSession = networkSession;
			// id player
			this.IdPlayer = playerState.IdPlayer;
		}

		public void OnValidate()
		{
			SignalDirty();
		}

		#endregion

		// === IPlayerStateActivtyClassCollectionData ==========================================================================================

		#region IPlayerStateActivtyInstanceCollectionData

		public IActivityInstance FindActivityInstance(string idAcivity)
		{
			ActivityInstance activity = null;
			if (data.ContainsKey(idAcivity))
				activity = data[idAcivity];
			return activity;
		}
		
		public bool BuildActivity(string idActivityInstance, string idActivityInstanceParent, string idClass)
		{
			bool b = false;
			var instance = ActivityInstance.CreateInstance(idActivityInstance, idActivityInstanceParent, idClass);
			if (data.Contains(idActivityInstance) == true)
			{
				data.Remove(idActivityInstance);
			}
			data.Add(idActivityInstance, instance);
			b = true;
#if UNITY_EDITOR
			EditorUtility.SetDirty(this);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
#endif
			return b;
		}

		public bool CreateActivity(string idActivityInstance, bool bForce)
		{
			bool b = false;
			var activityInstance = FindActivityInstance(idActivityInstance);
			if (activityInstance != null)
			{
				b = activityInstance.Create(bForce);
			}
			return b;
		}


		public bool RegisterActivity(string idActivityInstance, bool bForce)
		{
			bool b = false;
			var activityInstance = FindActivityInstance(idActivityInstance);
			if (activityInstance != null)
			{
				b = activityInstance.Register(bForce);
			}
			return b;
		}

		public bool StartActivity(string idActivityInstance, bool bForce)
		{
			bool b = false;
			var activityInstance = FindActivityInstance(idActivityInstance);
			if (activityInstance != null)
			{
				b = activityInstance.Start(bForce);
			}
			return b;
		}

		public bool CompleteActivity(string idActivityInstance, bool bForce)
		{
			bool b = false;
			var activityInstance = FindActivityInstance(idActivityInstance);
			if (activityInstance != null)
			{
				b = activityInstance.Complete(bForce);
			}
			return b;
		}

		public bool StopActivity(string idActivityInstance, bool bForce)
		{
			bool b = false;
			var activityInstance = FindActivityInstance(idActivityInstance);
			if (activityInstance != null)
			{
				b = activityInstance.Stop(bForce);
			}
			return b;
		}

		public void ResetActivityGameplay(string idActivityInstance)
		{
			var activityInstance = FindActivityInstance(idActivityInstance);
			if (activityInstance != null)
			{
				activityInstance.ResetGameplay();
			}
		}

		public void ResetActivityDefault(string idActivityInstance)
		{
			var activityInstance = FindActivityInstance(idActivityInstance);
			if (activityInstance != null)
			{
				activityInstance.ResetToDefault();
			}
		}

		[Button(enabledMode: EButtonEnableMode.Always)]
		public void ResetToDefault()
		{
			foreach (var it in data)
			{
				var ativityInstance = it.Value;
				ativityInstance.ResetToDefault();
			}
		}

		public IEnumerator<KeyValuePair<string, ActivityInstance>> GetEnumerator()
		{
			return data.GetEnumerator();
		}

		public ICollection<string> GetActivityInstanceKeys()
		{
			return data.Keys;
		}

		#endregion

		// === IMPLEMENTATION =========================================================

		#region IMPLEMENTATION

		public bool IsDirty
		{
			get
			{
				if (isDirty == true)
				{
					isDirty = false;
					return true;
				}
				else
				{
					return false;
				}
			}

			set
			{
				isDirty = value;
			}
		}

		void SignalDirty()
		{
			foreach (var it in data)
			{
				it.Value.IsDirty = true;
			}
			IsDirty = true;
		}

		/*
		 * 
		 * Network
		 * 
		 */

		IEnumerator PostData()
		{
			if (data != null)
			{
				bool done = false;
				void DoneSet(Data.PlayerStateActivityInstanceCollectionStructSpec d)
				{
					done = true;
				}
				var set = Data.PlayerStateActivityInstanceCollectionStructSpec.Create(this);
				//var json = JsonUtility.ToJson(set, true);
				networkSession.Post(set, DoneSet);
				yield return new WaitUntil(() => done == true);
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}

		public IEnumerator PutData(Data.DoneGet<Data.ActivityInstanceCollectionScriptable> doneSet = null)
		{
			bool done = false;
			void DoneSet(Data.PlayerStateActivityInstanceCollectionStructSpec d)
			{
				doneSet?.Invoke(this);
				done = true;
			}
			var set = Data.PlayerStateActivityInstanceCollectionStructSpec.Create(this);
			//var json = JsonUtility.ToJson(set, true);
			networkSession.Put(set, DoneSet);
			yield return new WaitUntil(() => done == true);
			yield return 0;
		}

		public IEnumerator GetData(string idPlayer, Data.DoneGet<Data.ActivityInstanceCollectionScriptable> doneSet = null, ActivityInstanceCollectionScriptable defaultValues = null)
		{
			bool done = false;

			IdPlayer = idPlayer;

			void LocalDoneSet(Data.PlayerStateActivityInstanceCollectionStructSpec dataRef)
			{
				// check if user found !!!
				if (dataRef != null)
				{
					dataRef.Make(this);
					doneSet(this);
				}
				// needed for stop corountine
				done = true;
				SignalDirty();
			}

			// load data
			if (defaultValues == null) 
			{
				bool b = networkSession.GetResult(IdPlayer, LocalDoneSet);
				if (b == false)
				{
					ResetToDefault();
					// record reseted data
					yield return PostData();
					SignalDirty();
				}
			}
			else
			{
				// in case of reseting we use default data from scriptable 
				// record default data
				yield return defaultValues.PutData();
				// get back on this instance
				yield return GetData(idPlayer, doneSet);
			}
			yield return new WaitUntil(() => done == true);
			yield return 0;
		}
		
		// Make a copy of data from source

		public void CopyNewData(Data.ActivityInstanceCollectionScriptable from)
		{
			CopyData(from.data, false);
		}

		public void CopyData(ActivityInstanceDictionary from, bool evenExist)
		{
			foreach (var it in from)
			{
				// if the activity doesn't exist then add
				if (this.data.ContainsKey(it.Key) == false)
				{
					// we can't share reference; so we make a copy
					this.data.Add(it.Key, ActivityInstance.CreateInstance(it.Value));
				}
				else
				{
					// if the activity already exist then copy all data 
					if (evenExist == true)
					{
						this.data[it.Key].CopyDataFrom(it.Value);
					}
					// otherwise update activity from some data update
					else
					{
						this.data[it.Key].UpdateDataFrom(it.Value);
					}
				}
			}
		}

		#endregion

		// === IActivityField =========================================================

		#region IActivityField

		KeyValuePair<string, ActivityInstance> FindField(string name)
		{
			return data.SingleOrDefault(s => ActivityFieldUtilities.IsFullPathFieldOwnedById(s.Key, name));
		}

		override public bool SetValue(string name, object value, FieldSearchMethod method)
		{
			bool b = false;
			var single = FindField(name);
			if (single.IsNull() is false)
			{
				b = single.Value.SetValue(name, value, FieldSearchMethod.fullPath);
			}
			return b;
		}

		override public bool AddValue(string name, object value, FieldSearchMethod method)
		{
			bool b = false;
			var single = FindField(name);
			if (single.IsNull() is false)
			{
				b = single.Value.AddValue(name, value, FieldSearchMethod.fullPath);
			}
			return b;
		}

		override public bool SubValue(string name, object value, FieldSearchMethod method)
		{
			bool b = false;
			var single = FindField(name);
			if (single.IsNull() is false)
			{
				b = single.Value.SubValue(name, value, FieldSearchMethod.fullPath);
			}
			return b;
		}

		override public bool DivValue(string name, object value, FieldSearchMethod method)
		{
			bool b = false;
			var single = FindField(name);
			if (single.IsNull() is false)
			{
				b = single.Value.DivValue(name, value, FieldSearchMethod.fullPath);
			}
			return b;
		}

		override public bool MulValue(string name, object value, FieldSearchMethod method)
		{
			bool b = false;
			var single = FindField(name);
			if (single.IsNull() is false)
			{
				b = single.Value.MulValue(name, value, FieldSearchMethod.fullPath);
			}
			return b;
		}

		override public bool ResetValue(string name, FieldSearchMethod method)
		{
			bool b = false;
			var single = FindField(name);
			if (single.IsNull() is false)
			{
				b = single.Value.ResetValue(name, FieldSearchMethod.fullPath);
			}
			return b;
		}

		override public object GetValue(string name, FieldSearchMethod method)
		{
			object obj = null;
			var single = FindField(name);
			if (single.IsNull() is false)
			{
				obj = single.Value.GetValue(name, FieldSearchMethod.fullPath);
			}
			return obj;
		}

		override public (bool, bool) CompareValue(string name, object value, FieldComparison comparison, FieldSearchMethod method)
		{
			(bool, bool) pair = (false, false);
			var single = FindField(name);
			if (single.IsNull() is false)
			{
				pair = single.Value.CompareValue(name, value, comparison, FieldSearchMethod.fullPath);
			}
			return pair;
		}

		#endregion
	}

	#endregion

	// Data stucture used for managing transfert by client/server

	#region PlayerStateActivityInstanceCollectionStructSpec

	[System.Serializable]
	public class PlayerStateActivityInstanceCollectionStructSpec : IGraphDataBase, ISerializationCallbackReceiver
	{
		static string currentVersion = "1";

		[SerializeField]
		string version = currentVersion;

		[SerializeField]
		ActivityInstanceDictionary data = new ActivityInstanceDictionary();

		public override string ToString()
		{
			return JsonUtility.ToJson(this, true);
		}

		static public PlayerStateActivityInstanceCollectionStructSpec FromString(string json)
		{
			var spec = new PlayerStateActivityInstanceCollectionStructSpec();
			spec = JsonUtility.FromJson<PlayerStateActivityInstanceCollectionStructSpec>(json);
			return spec;
		}

		public void Make(ActivityInstanceCollectionScriptable activtyInstanceCollectionData)
		{
			activtyInstanceCollectionData.IdPlayer = Id;
			activtyInstanceCollectionData.CopyData(this.data, true);
		}

		static public PlayerStateActivityInstanceCollectionStructSpec Create(ActivityInstanceCollectionScriptable activtyInstanceCollectionData)
		{
			return new PlayerStateActivityInstanceCollectionStructSpec
			{
				Id = activtyInstanceCollectionData.IdPlayer,
				data = activtyInstanceCollectionData.data
			};
		}

		public void OnBeforeSerialize()
		{
		}

		public void OnAfterDeserialize()
		{
			if (version == "2")
			{
			}
		}
	}

	#endregion
}