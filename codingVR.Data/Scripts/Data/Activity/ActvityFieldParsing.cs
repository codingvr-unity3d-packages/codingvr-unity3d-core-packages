﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.PotoGame.Quests
{
	public class ActvityFieldParsing : MonoBehaviour, Data.IActivityField, Data.IActivityNestedField
	{
		// === IActivityField ===

		#region IActivityField

		/*
		 * 
		 * Generic functions
		 * 
		 */

		bool ParseAndSet(Func<Data.IActivityField, bool> action)
		{
			bool bSuccess = false;
			foreach (Transform child in transform)
			{
				var activityField = child.GetComponent<Data.IActivityField>();
				if (activityField != null)
				{
					bSuccess = action(activityField);
					if (bSuccess == true)
						break;
				}
			}
			return bSuccess;
		}

		object ParseAndGet(Func<Data.IActivityField, object> action)
		{
			object obj = null;
			foreach (Transform child in transform)
			{
				var activityField = child.GetComponent<Data.IActivityField>();
				if (activityField != null)
				{
					obj = action(activityField);
					if (obj != null)
						break;
				}
			}
			return obj;
		}

		(bool, bool) ParseAndCompare(Func<Data.IActivityField, (bool, bool)> action)
		{
			(bool bSuccess, bool bResult) ret = (false, false);
			foreach (Transform child in transform)
			{
				var activityField = child.GetComponent<Data.IActivityField>();
				if (activityField != null)
				{
					ret = action(activityField);
					if (ret.bSuccess == true)
						break;
				}
			}
			return ret;
		}

		/*
		 * 
		 * Set
		 * 
		 */

		public bool SetValue(string name, object value, Data.FieldSearchMethod method = Data.FieldSearchMethod.defaultMethod)
		{
			bool op(Data.IActivityField activityField)
			{
				return activityField.SetValue(name, value, method);
			}
			return ParseAndSet(op);
		}

		public bool AddValue(string name, object value, Data.FieldSearchMethod method = Data.FieldSearchMethod.defaultMethod)
		{
			bool op(Data.IActivityField activityField)
			{
				return activityField.AddValue(name, value, method);
			}
			return ParseAndSet(op);
		}

		public bool SubValue(string name, object value, Data.FieldSearchMethod method = Data.FieldSearchMethod.defaultMethod)
		{
			bool op(Data.IActivityField activityField)
			{
				return activityField.SubValue(name, value, method);
			}
			return ParseAndSet(op);
		}

		public bool DivValue(string name, object value, Data.FieldSearchMethod method = Data.FieldSearchMethod.defaultMethod)
		{
			bool op(Data.IActivityField activityField)
			{
				return activityField.DivValue(name, value, method);
			}
			return ParseAndSet(op);
		}

		public bool MulValue(string name, object value, Data.FieldSearchMethod method = Data.FieldSearchMethod.defaultMethod)
		{
			bool op(Data.IActivityField activityField)
			{
				return activityField.MulValue(name, value, method);
			}
			return ParseAndSet(op);
		}

		public bool ResetValue(string name, Data.FieldSearchMethod method = Data.FieldSearchMethod.defaultMethod)
		{
			bool op(Data.IActivityField activityField)
			{
				return activityField.ResetValue(name, method);
			}
			return ParseAndSet(op);
		}

		/*
		 * 
		 * Compare
		 * 
		 */

		public (bool, bool) CompareValue(string name, object value, Data.FieldComparison comparison, Data.FieldSearchMethod method = Data.FieldSearchMethod.defaultMethod)
		{
			(bool, bool) op(Data.IActivityField activityField)
			{
				return activityField.CompareValue(name, value, comparison, method);
			}
			return ParseAndCompare(op);
		}

		/*
		 * 
		 * Get
		 * 
		 */

		public object GetValue(string name, Data.FieldSearchMethod method = Data.FieldSearchMethod.defaultMethod)
		{
			object op(Data.IActivityField activityField)
			{
				return activityField.GetValue(name, method);
			}
			return ParseAndGet(op);
		}

		#endregion

		// === IActivityNestedField ===

		#region IActivityNestedField

		public string ParseNestedFields(string nestedFields)
		{
			return Data.ActivityFieldUtilities.ParseNestedFields(nestedFields, this);
		}

		#endregion
	}
}