﻿using codingVR.Core;
using FSM;
using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace codingVR.Data
{
	public class ActivityFieldManager
	{
		// === DATA-INTERNAL ================================================

		#region DATA-INTERNAL

		// fields management
		Dictionary<string, Field> fields;

		#endregion

		// === LIFE-CYCLE =================================================

		#region LIFE-CYCLE
			
		protected ActivityFieldManager()
		{
			PopulateFields();
		}

		#endregion

		// === INTERFACE-PROTECTED =================================================

		#region INTERFACE-PROTECTED

		// === reflection / populate all fields ==================================================

		virtual protected void PopulateFields()
		{
			// populate instance fields
			this.fields = new Dictionary<string, Field>();
			var type = GetType();

			// populate type
			PopulateFields(this);
		}

		protected void PopulateFields(object instance)
		{
			foreach (var field in instance.GetType().GetFields())
			{
				Field f = new Field(fieldInfo: field, getFieldValue: (activity) => { return field.GetValue(instance); });
				if (this.fields.ContainsKey(f.fieldInfo.Name) == false)
					this.fields.Add(f.fieldInfo.Name, f);
				else
					Debug.LogErrorFormat("{0} : field {1} already inserted !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, f.fieldInfo.Name);
			}
		}

		protected Dictionary<string, Field> Fields => fields;

		#endregion

		// === ENUMERABLE =============================================

		#region ENUMERABLE

		public IEnumerator GetEnumerator()
		{
			return fields.Values.GetEnumerator();
		}

		#endregion
	}
}
