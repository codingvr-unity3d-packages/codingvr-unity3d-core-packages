﻿using NaughtyAttributes;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Scripting.APIUpdating;

namespace codingVR.Data
{
	[Serializable]
	[CreateAssetMenu(fileName = "ActivtyClassCollectionScriptable", menuName = "CodingVR/Data/PlayerState/ActivtyClassCollectionScriptable", order = 2)]
	[MovedFrom(false, null, null, "PlayerStateActivtyClassCollectionDataScriptable")]
	public class ActivtyClassCollectionScriptable : ScriptableObject, IActivtyClassCollection
	{
		[Serializable] public class ActivityClassDictionary : Core.cvrSerializableDictionary<string, ActivityClass> { }
		[SerializeField]
		ActivityClassDictionary data = new ActivityClassDictionary();

		[Button]
		private void ResetActivityClass()
		{
			foreach (var it in data)
			{
				it.Value.classPassage = 0;
			}
		}

		// === IPlayerStateActivtyClassCollectionData ==========================================================================================

		#region IPlayerStateActivtyClassCollectionData

		public ICollection<string> GetActivityClassKeys()
		{
			return data.Keys;
		}

		public IActivityClass FindActivityClass(string idClass)
		{
			ActivityClass activity = null;
			if (data.ContainsKey(idClass))
				activity = data[idClass];
			return activity;
		}

		public void AddActivityClass(string className, string description, ActivityClassImportance importance, ActivityClassRegularity regularity, ActivityClassSize size, int duration, int delay, int points, GameObject activityClassBehavior)
		{
			var instance = ActivityClass.Create(className,description,importance,regularity,size,duration,delay,points,activityClassBehavior);
			if (data.Contains(className) == true)
			{
				data.Remove(className);
			}
			data.Add(className, instance);
		}

		public IEnumerator<KeyValuePair<string, ActivityClass>> GetEnumerator()
		{
			return data.GetEnumerator();
		}

		#endregion
	}
}