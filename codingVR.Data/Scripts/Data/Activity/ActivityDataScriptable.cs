﻿using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TypeReferences;
using UnityEngine;
using Zenject;

namespace codingVR.Data
{
	[System.Serializable]
	public abstract class ActivityDataScriptable : ActivityFieldScriptable
	{
		public abstract Data.IActivityFieldServer Data { get; set; }

		public void PopulateFields()
		{
			Data.PopulateFields();
		}

		public Dictionary<string, FieldInfo> Fields => Data.Fields;

		override public bool SetValue(string name, object value, FieldSearchMethod method)
		{
			return Data.SetValue(name, value, method);
		}

		override public bool AddValue(string name, object value, FieldSearchMethod method)
		{
			return Data.AddValue(name, value, method);
		}

		override public bool SubValue(string name, object value, FieldSearchMethod method)
		{
			return Data.SubValue(name, value, method);
		}

		override public bool DivValue(string name, object value, FieldSearchMethod method)
		{
			return Data.DivValue(name, value, method);
		}

		override public bool MulValue(string name, object value, FieldSearchMethod method)
		{
			return Data.MulValue(name, value, method);
		}

		override public bool ResetValue(string name, FieldSearchMethod method)
		{
			return Data.ResetValue(name, method);
		}

		override public object GetValue(string name, FieldSearchMethod method)
		{
			return Data.GetValue(name, method);
		}

		override public (bool, bool) CompareValue(string name, object value, FieldComparison comparison, FieldSearchMethod method)
		{
			return Data.CompareValue(name, value, comparison, method);
		}

		public void ResetAllValues()
		{
			foreach (var it in Fields)
			{
				Data.ResetValue(it.Key, FieldSearchMethod.localPath);
			}
		}
		
		public bool IsDirty
		{
			get => Data.IsDirty;
			set
			{
				Data.IsDirty = value;
			}
		}

		public void OnValidate()
		{
			Data.IsDirty = true;
		}
	}

	// Data stucture used for managing transfert by client/server

	[System.Serializable]
	public class ActivityDataStructSpec : IGraphDataBase, ISerializationCallbackReceiver
	{
		static string currentVersion = "2";

		[SerializeField]
		string version = currentVersion;

		[SerializeField]
		string json;

		[Inherits(typeof(IActivityFieldServer))]
		[SerializeField]
		public TypeReference activityDataType;

		public ActivityDataStructSpec()
		{
		}

		// json conv

		public override string ToString()
		{
			return JsonUtility.ToJson(this, true);
		}

		static public ActivityDataStructSpec FromString(string json)
		{
			var spec = new ActivityDataStructSpec();
			spec = JsonUtility.FromJson<ActivityDataStructSpec>(json);
			return spec;
		}

		// create data

		public void Make(ActivityDataScriptable scriptable)
		{
			string idPlayer;
			(idPlayer, scriptable.Data.Id) = ParseId(Id);
			if (activityDataType != null)
			{
				var activityData = System.Activator.CreateInstance(activityDataType) as IActivityFieldServer;
				activityData = JsonUtility.FromJson(json, activityDataType.Type) as IActivityFieldServer;
				scriptable.Data = activityData;
			}
		}

		static public ActivityDataStructSpec Create(string idPlayer, ActivityDataScriptable scriptable)
		{
			return new ActivityDataStructSpec
			{
				Id = MakeId(idPlayer, scriptable.Data.Id),
				json = JsonUtility.ToJson(scriptable.Data),
				activityDataType = new TypeReference(scriptable.Data.GetType())
			};
		}

		// serailization control

		public void OnBeforeSerialize()
		{
		}

		public void OnAfterDeserialize()
		{
			if (version == "2")
			{
			}
		}
	}
}
