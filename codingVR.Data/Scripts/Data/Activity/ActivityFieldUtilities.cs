﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using UnityEngine;

namespace codingVR.Data
{

	class MyMath
	{
		public static T Add<T>(T a, T b) where T : struct
		{
			switch (typeof(T).Name)
			{
				case "Int32":
					return (T)(object)((int)(object)a + (int)(object)b);
				case "Double":
					return (T)(object)((double)(object)a + (double)(object)b);
				case "Float":
					return (T)(object)((float)(object)a + (float)(object)b);
				case "Char":
					return (T)(object)((char)(object)a + (char)(object)b);
				case "Int64":
					return (T)(object)((long)(object)a + (long)(object)b);
				default:
					return default(T);
			}
		}

		public static T Sub<T>(T a, T b) where T : struct
		{
			switch (typeof(T).Name)
			{
				case "Int32":
					return (T)(object)((int)(object)a - (int)(object)b);
				case "Double":
					return (T)(object)((double)(object)a - (double)(object)b);
				case "Float":
					return (T)(object)((float)(object)a - (float)(object)b);
				case "Char":
					return (T)(object)((char)(object)a - (char)(object)b);
				case "Int64":
					return (T)(object)((long)(object)a - (long)(object)b);
				default:
					return default(T);
			}
		}

		public static T OrLogic<T>(T a, T b) where T : struct
		{
			switch (typeof(T).Name)
			{
				case "Int32":
					return (T)(object)((int)(object)a | (int)(object)b);
				//case "Double":
				//	return (T)(object)((double)(object)a | (double)(object)b);
				//case "Float":
				//	return (T)(object)((float)(object)a | (float)(object)b);
				case "Char":
					return (T)(object)((char)(object)a | (char)(object)b);
				case "Int64":
					return (T)(object)((long)(object)a | (long)(object)b);
				default:
					return default(T);
			}
		}

		public static T AndLogic<T>(T a, T b) where T : struct
		{
			switch (typeof(T).Name)
			{
				case "Int32":
					return (T)(object)((int)(object)a & ~(int)(object)b);
				//case "Double":
				//	return (T)(object)((double)(object)a | (double)(object)b);
				//case "Float":
				//	return (T)(object)((float)(object)a | (float)(object)b);
				case "Char":
					return (T)(object)((char)(object)a & ~(char)(object)b);
				case "Int64":
					return (T)(object)((long)(object)a & ~(long)(object)b);
				default:
					return default(T);
			}
		}

		public static T Div<T>(T a, T b) where T : struct
		{
			switch (typeof(T).Name)
			{
				case "Int32":
					return (T)(object)((int)(object)a / (int)(object)b);
				case "Double":
					return (T)(object)((double)(object)a / (double)(object)b);
				case "Float":
					return (T)(object)((float)(object)a / (float)(object)b);
				case "Char":
					return (T)(object)((char)(object)a / (char)(object)b);
				case "Int64":
					return (T)(object)((long)(object)a / (long)(object)b);

				default:
					return default(T);
			}
		}


		public static T Mult<T>(T a, T b) where T : struct
		{
			switch (typeof(T).Name)
			{
				case "Int32":
					return (T)(object)((int)(object)a * (int)(object)b);
				case "Double":
					return (T)(object)((double)(object)a * (double)(object)b);
				case "Float":
					return (T)(object)((float)(object)a * (float)(object)b);
				case "Char":
					return (T)(object)((char)(object)a * (char)(object)b);
				case "Int64":
					return (T)(object)((long)(object)a * (long)(object)b);

				default:
					return default(T);
			}
		}
	}

	public static class ActivityFieldUtilities
	{
		/*
		 * 
		 * Parsing Path
		 * 
		 */

		#region ParsingPath

		public static string GetFullPathField(string id, string fieldName)
		{
			return id + "." + fieldName;
		}

		public static string GetIdFromPathField(string fieldName)
		{
			string name = null;
			var names = fieldName.Split('.');
			if (names.Length >= 2)
			{
				name = names[0];
			}
			return name;
		}

		public static (string, string) SplitPathField(string fieldName)
		{
			var names = fieldName.Split('.');
			string id = fieldName;
			string name = "";
			if (names.Length >= 2)
			{
				id = names[0];
				name = names[1];
				for (var i = 2; i < names.Length; ++i)
				{
					name += "." + names[i];
				}
			}
			return (id, name);
		}

		public static bool IsFullPathFieldOwnedById(string id, string fieldName)
		{
			bool b = false;
			var name = GetIdFromPathField(fieldName);
			if (name != null)
			{
				b = name.Equals(id);
			}
			return b;
		}

		public static string ContainsField(string id, IActivityFieldServer fields, string fieldName, FieldSearchMethod method)
		{
			string originField = null;

			string localPath(string _fieldName)
			{
				string _originField = null;
				if (fields.Fields.ContainsKey(_fieldName) == true)
				{
					_originField = _fieldName;
				}
				return _originField;
			}

			string fullPath(string _fieldName)
			{
				string _originField = null;

				var names = _fieldName.Split('.');
				if (names.Length >= 2)
				{
					if (names[0].Equals(id) == true)
					{
						_originField = localPath(names[names.Length - 1]);
					}
				}

				return _originField;
			}

			string anyPath(string _fieldName)
			{
				string _originField = null;

				var names = _fieldName.Split('.');
				if (names.Length > 0)
				{
					_originField = localPath(names[names.Length - 1]);
				}

				return _originField;
			}

			switch (method)
			{
				case FieldSearchMethod.defaultMethod:
				case FieldSearchMethod.localPath:
					{
						originField = localPath(fieldName);
					}
					break;
				case FieldSearchMethod.fullPath:
					{
						originField = fullPath(fieldName);
					}
					break;
				case FieldSearchMethod.any:
					{
						originField = anyPath(fieldName);
					}
					break;
			}
			return originField;
		}

		#endregion

		/*
		 * 
		 * Operation on Values
		 * 
		 */

		#region OperationOnValues

		// dynamic cast
		// https://stackoverflow.com/questions/62110073/how-to-cast-an-object-to-type
		static object Cast(object obj, Type type)
		{
			// return the obj casted to type, but how?
			try
			{
				return Convert.ChangeType(obj, type);
			}

			catch (Exception ex)
			{
				Debug.LogErrorFormat("{0} : cast between {1} and {2} has failed !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, obj, type);
				return null;
			}
		}

		delegate void opSetValue(FieldInfo field, object value);

		static bool OpValue(string id, IActivityFieldServer fields, string name, object value, opSetValue op, FieldSearchMethod method)
		{
			bool b = false;
			name = ContainsField(id, fields, name, method);
			if (string.IsNullOrEmpty(name) == false)
			{
				var f = fields.Fields[name];
				var valueConverted = Cast(value, f.FieldType);
				if (valueConverted != null)
				{
					op(f, valueConverted);
					b = true;
				}
				else
				{
					Debug.LogErrorFormat("{0} : convertion has failed from {1} {2} to {3} !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, name, value.GetType(), f.FieldType);
				}
			}
			return b;
		}

		delegate void opResetValue(FieldInfo field);

		static bool OpResetValue(string id, IActivityFieldServer fields, string name, opResetValue op, FieldSearchMethod method)
		{
			bool b = false;
			name = ContainsField(id, fields, name, method);
			if (string.IsNullOrEmpty(name) == false)
			{
				var f = fields.Fields[name];
				op(f);
				b = true;
			}
			return b;
		}

		#endregion

		/*
		 * 
		 * Operation on fields
		 * 
		 */

		#region OperationOnFields



		public static bool SetValue(object fieldProvider, string id, IActivityFieldServer fields, string name, object value, FieldSearchMethod method)
		{
			void op(FieldInfo _field, object _value)
			{
				_field.SetValue(fieldProvider, _value);
			}

			return OpValue(id, fields, name, value, op, method);
		}

		public static bool AddValue(object fieldProvider, string id, IActivityFieldServer fields, string name, object value, FieldSearchMethod method)
		{
			void op(FieldInfo _field, object _value)
			{
				object curValue = _field.GetValue(fieldProvider);
				if (_field.FieldType.IsEnum == true)
				{
					switch (_value.GetType())
					{
						case Type intType when _value.GetType() == typeof(int):
							curValue = MyMath.OrLogic((int)curValue, (int)_value);
							break;
						case Type charType when _value.GetType() == typeof(char):
							curValue = MyMath.OrLogic((char)curValue, (char)_value);
							break;
						case Type longType when _value.GetType() == typeof(long):
							curValue = MyMath.OrLogic((long)curValue, (long)_value);
							break;

					}
				}
				else
					switch (_value.GetType())
					{
						case Type intType when _value.GetType() == typeof(int):
							curValue = MyMath.Add((int)curValue, (int)_value);
							break;
						case Type floatType when _value.GetType() == typeof(float):
							curValue = MyMath.Add((float)curValue, (float)_value);
							break;
						case Type doubleType when _value.GetType() == typeof(double):
							curValue = MyMath.Add((double)curValue, (double)_value);
							break;
						case Type charType when _value.GetType() == typeof(char):
							curValue = MyMath.Add((char)curValue, (char)_value);
							break;
						case Type longType when _value.GetType() == typeof(long):
							curValue = MyMath.Add((long)curValue, (long)_value);
							break;
						case Type stringType when _value.GetType() == typeof(string):
							curValue = (string)(object)((string)curValue + (string)_value);
							break;

					}
					//curValue += _value;

				_field.SetValue(fieldProvider, curValue);
			}

			return OpValue(id, fields, name, value, op, method);
		}

		public static bool SubValue(object fieldProvider, string id, IActivityFieldServer fields, string name, object value, FieldSearchMethod method)
		{
			void op(FieldInfo _field, object _value)
			{
				object curValue = _field.GetValue(fieldProvider);
				if (_field.FieldType.IsEnum == true)
					switch (_value.GetType())
					{
						case Type intType when _value.GetType() == typeof(int):
							curValue = MyMath.OrLogic((int)curValue, (int)_value);
							break;
						case Type charType when _value.GetType() == typeof(char):
							curValue = MyMath.OrLogic((char)curValue, (char)_value);
							break;
						case Type longType when _value.GetType() == typeof(long):
							curValue = MyMath.OrLogic((long)curValue, (long)_value);
							break;

					}
				//curValue &= ~_value;
				else

					switch (_value.GetType())
					{
						case Type intType when _value.GetType() == typeof(int):
							curValue = MyMath.Sub((int)curValue, (int)_value);
							break;
						case Type floatType when _value.GetType() == typeof(float):
							curValue = MyMath.Sub((float)curValue, (float)_value);
							break;
						case Type doubleType when _value.GetType() == typeof(double):
							curValue = MyMath.Sub((double)curValue, (double)_value);
							break;
						case Type charType when _value.GetType() == typeof(char):
							curValue = MyMath.Sub((char)curValue, (char)_value);
							break;
						case Type longType when _value.GetType() == typeof(long):
							curValue = MyMath.Sub((long)curValue, (long)_value);
							break;
						case Type stringType when _value.GetType() == typeof(string):
							curValue = (string)(object)(curValue.ToString().Replace((string)_value,""));
							break;

					}
				//curValue -= _value;
				_field.SetValue(fieldProvider, curValue);
			}

			return OpValue(id, fields, name, value, op, method);
		}

		public static bool DivValue(object fieldProvider, string id, IActivityFieldServer fields, string name, object value, FieldSearchMethod method)
		{
			void op(FieldInfo _field, object _value)
			{
				object curValue = _field.GetValue(fieldProvider);
				switch (_value.GetType())
				{
					case Type intType when _value.GetType() == typeof(int):
						curValue = MyMath.Div((int)curValue, (int)_value);
						break;
					case Type floatType when _value.GetType() == typeof(float):
						curValue = MyMath.Div((float)curValue, (float)_value);
						break;
					case Type doubleType when _value.GetType() == typeof(double):
						curValue = MyMath.Div((double)curValue, (double)_value);
						break;
					case Type charType when _value.GetType() == typeof(char):
						curValue = MyMath.Div((char)curValue, (char)_value);
						break;
					case Type longType when _value.GetType() == typeof(long):
						curValue = MyMath.Div((long)curValue, (long)_value);
						break;
				}
				//curValue /= _value;
				_field.SetValue(fieldProvider, curValue);
			}

			return OpValue(id, fields, name, value, op, method);
		}

		public static bool MulValue(object fieldProvider, string id, IActivityFieldServer fields, string name, object value, FieldSearchMethod method)
		{
			void op(FieldInfo _field, object _value)
			{
				object curValue = _field.GetValue(fieldProvider);
				switch (_value.GetType())
				{
					case Type intType when _value.GetType() == typeof(int):
						curValue = MyMath.Mult((int)curValue, (int)_value);
						break;
					case Type floatType when _value.GetType() == typeof(float):
						curValue = MyMath.Mult((float)curValue, (float)_value);
						break;
					case Type doubleType when _value.GetType() == typeof(double):
						curValue = MyMath.Mult((double)curValue, (double)_value);
						break;
					case Type charType when _value.GetType() == typeof(char):
						curValue = MyMath.Mult((char)curValue, (char)_value);
						break;
					case Type longType when _value.GetType() == typeof(long):
						curValue = MyMath.Mult((long)curValue, (long)_value);
						break;
				}
				//curValue *= _value;

				_field.SetValue(fieldProvider, curValue);
			}

			return OpValue(id, fields, name, value, op, method);
		}

		public static object GetValue(object fieldProvider, string id, IActivityFieldServer fields, string name, FieldSearchMethod method)
		{
			object obj = null;
			name = ContainsField(id, fields, name, method);
			if (string.IsNullOrEmpty(name) == false)
			{
				var f = fields.Fields[name];
				obj = f.GetValue(fieldProvider);
			}
			return obj;
		}

		public static (bool, bool) CompareValue(object fieldProvider, string id, IActivityFieldServer fields, string name, object value, FieldComparison comparison, FieldSearchMethod method)
		{
			bool bComparisonResult = false;
			bool bObjectFound = false;
			name = ContainsField(id, fields, name, method);
			if (string.IsNullOrEmpty(name) == false)
			{
				var sourceField = fields.Fields[name];
				var valueType = value.GetType();
				
				object stdCast()
				{
					var valueObject = sourceField.GetValue(fieldProvider);
					var sourceValue = Cast(valueObject, valueType);
					return sourceValue;
				}

				(float, float) floatCast()
				{
					var valueObject = sourceField.GetValue(fieldProvider);
					var sourceValue = Cast(valueObject, typeof(float));
					var destValue = Cast(value, typeof(float));
					return ((float)sourceValue, (float)destValue);
				}

				int compare()
				{
					var sourceValue = stdCast();
					return ((IComparable)sourceValue).CompareTo(value);
				}

				bObjectFound = true;
				switch (comparison)
				{
					case FieldComparison.Equal:
						{
							if (sourceField.FieldType.IsEnum && valueType.IsEnum)
							{
								var sourceValue = stdCast();
								if (((int)sourceValue & (int)value) == (int)value)
								{
									bComparisonResult = true;
								}
							}
							else
							{
								bComparisonResult = compare() == 0;
							}
						}
						break;
					case FieldComparison.NotEqual:
						{
							if (sourceField.FieldType.IsEnum && valueType.IsEnum)
							{
								var sourceValue = stdCast();
								if (((int)sourceValue & (int)value) != (int)value)
								{
									bComparisonResult = true;
								}
							}
							else
							{
								bComparisonResult = compare() != 0;
							}
						}
						break;
					case FieldComparison.EqualNear:
						{
							(float sourceValue, float destValue) = floatCast();
							bComparisonResult = Mathf.Approximately(sourceValue, destValue) == true;
						}
						break;
					case FieldComparison.NotEqualNear:
						{
							(float sourceValue, float destValue) = floatCast();
							bComparisonResult = Mathf.Approximately(sourceValue, destValue) == false;
						}
						break;
					case FieldComparison.Less:
						{
							bComparisonResult = compare() < 0;
						}
						break;
					case FieldComparison.LessOrEqual:
						{
							bComparisonResult = compare() <= 0;
						}
						break;
					case FieldComparison.Greater:
						{
							bComparisonResult = compare() > 0;
						}
						break;
					case FieldComparison.GreaterOrEqual:
						{
							bComparisonResult = compare() >= 0;
						}
						break;
				}
			}
			return (bObjectFound, bComparisonResult);
		}

		public static bool ResetValue(object fieldProvider, string id, IActivityFieldServer fields, string name, FieldSearchMethod method)
		{
			void op(FieldInfo _field)
			{
				DefaultValueAttribute myAttribute = (DefaultValueAttribute)_field.GetCustomAttribute(typeof(DefaultValueAttribute));
				if (myAttribute != null)
				{
					_field.SetValue(fieldProvider, myAttribute.Value);
				}
			}

			return OpResetValue(id, fields, name, op, method);
		}

		#endregion

		/*
		 * 
		 * Operation on nested fields
		 * 
		 */

		#region IActivityNestedField

		protected class FieldPosition
		{
			int start;
			int end;

			public FieldPosition(int start, int end)
			{
				this.start = start;
				this.end = end;
			}

			static public FieldPosition ParseCurlyBrace(string str)
			{
				FieldPosition result = null;
				var length = str.Length;
				var stack = new Stack<int>();
				bool hasIssue = false;

				for (int i = 0; i < str.Length && result == null; i++)
				{
					if (str[i] == '{')
					{
						stack.Push(i);
					}

					if (str[i] == '}')
					{
						if (stack.Count == 0)
						{
							hasIssue = true;
							break;
						}
						var open = stack.Pop();
						stack.Clear();
						result = new FieldPosition(open, i);
					}
				}

				if (stack.Count != 0)
				{
					hasIssue = true;
				}

				if (hasIssue)
				{
					Debug.LogErrorFormat("{0} : string {1} is incorrecly formatted !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, str);
				}

				return result;
			}

			/// str - the source string
			//// index- the start location to replace at (0-based)
			//// length - the number of characters to be removed before inserting
			//// replace - the string that is replacing characters
			///

			public string ReplaceAt(string str, int index, int length, string replace)
			{
				return str.Remove(index, Math.Min(length, str.Length - index))
						.Insert(index, replace);
			}

			public void Make(ref string str, IActivityField activityField)
			{
				{
					string finalValue = "unknow";
					var startB = start + 1;
					var endB = end - 1;
					var lengthB = endB - startB + 1;
					var v = activityField.GetValue(str.Substring(startB, lengthB));
					if (v != null)
					{
						finalValue = v.ToString();
					}
					else
					{
						Debug.LogErrorFormat("{0} : in activity nested fields {1}; getValue has returned null !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, str);
					}
					str = ReplaceAt(str, start, end - start + 1, finalValue);
				}
			}
		}

		static public string ParseNestedFields(string nestedFields, IActivityField activityField)
		{
			//nestedFields = "Match a {si{ngle}} character {present} in the list below";
			FieldPosition fieldPosition = null;
			while ((fieldPosition = FieldPosition.ParseCurlyBrace(nestedFields)) != null)
			{
				fieldPosition.Make(ref nestedFields, activityField);
			}
			return nestedFields;
		}

		#endregion
	}
}
