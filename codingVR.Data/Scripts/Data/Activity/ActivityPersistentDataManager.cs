﻿using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TypeReferences;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace codingVR.Data
{
	public class ActivityPersistentDataManager : MonoBehaviour, IActivityPersistentDataManager, IActivityFieldServer
	{
		// === DATA-UI ============================================================

		#region DATA-UI

		[Header("Screen Play Group")]
		[Tooltip
			(
			"Screen Play Group identifies the owner of this pesistent data manager.\n" +
			"So the persistent data manager follows order coming from the screen play group\n" +
			"ex: the owner can reset default values for all data managed by this component\n"
			)]
		[SerializeField]
		GameEngine.ScreenPlayGroup screenPlayGroup;

		[Header("Activty Data Syncing")]
		[SerializeField]
		Data.ActivityDataScriptable dataActvity;

		[SerializeField]
		[Tooltip
			(
				"Inside a behavior Tree or an action" +
				"activityDataGroupId allows to identifies fields (as #[WelcomeGroup].description)" +
				"see also ActivitySyncingDataWithGCBehavior.cs"
			)]
		GameObject activityDataGroupId;

		[Header("Activty Data Compute")]
		[SerializeField]
		[FormerlySerializedAs("activityDataBehavior")]
		GameObject activityDataCompute;

#if UNITY_EDITOR
		[SerializeField]
		[ResizableTextArea]
		string fieldsLog;
#endif

		#endregion

		// === DATA-PRIVATE ============================================================

		#region DATA-PRIVATE

		Data.INetworkSession<Data.ActivityDataStructSpec, Data.ActivityDataStructSpec> networkSession;
		GameEngine.IPlayerState playerState;
		Core.IEventManager eventManager;
		DiContainer diContainer;
		bool isContructDone = false;

		#endregion

		// === IMPLEMENTATION-UTILS ============================================================

		#region IMPLEMENTATION-UTILS

		bool ContainsIdInstanceActivity(string groupTagName)
		{
			bool b = false;
			if (screenPlayGroup != null)
			{
				b = screenPlayGroup.name.Contains(groupTagName);
			}
			return b;
		}

		bool IsThisGroup(GameEngine.Event_ScreenPlayTagBase ev)
		{
			var groupData = ev.Data as Tuple<string, Data.Tag>;
			return ContainsIdInstanceActivity(groupData.Item2.ToString());
		}

		bool IsThisGroup(Data.Event_PlayerStateActivityBase ev)
		{
			var groupData = ev.Data as string;
			return ContainsIdInstanceActivity(groupData);
		}

		#endregion

		// === LIFE-CYCLE ============================================================

		#region LIFE-CYCLE

		/*
		 *
		 * life cycle
		 * 
		 */

		[Inject]
		public void Constructor(DiContainer diContainer, Data.INetworkSession<Data.ActivityDataStructSpec, Data.ActivityDataStructSpec> networkSession, GameEngine.IPlayerState playerState, Core.IEventManager eventManager)
		{
			this.networkSession = networkSession;
			this.playerState = playerState;
			this.eventManager = eventManager;
			this.diContainer = diContainer;
			isContructDone = true;
			Setup();
		}

		void Setup()
		{
			if (isActiveAndEnabled == true && isContructDone == true)
			{
				Load();
				EnableListeners();
				StartActivityBehavior(diContainer, transform);
			}
		}

		void CopyActivityFieldsGroupFromParent()
		{
			Transform parent = transform.parent;
			var activityPersistentDataManagerParent = parent.GetComponent<ActivityPersistentDataManager>();
			if (activityPersistentDataManagerParent != null)
			{
				ScreenPlayGroup = activityPersistentDataManagerParent.ScreenPlayGroup;
				ActivityDataGroupId = activityPersistentDataManagerParent.ActivityDataGroupId;
			}
		}
				
		void Start()
		{
			CopyActivityFieldsGroupFromParent();
		}
		
		void OnEnable()
		{
			Setup();
		}

		void OnDisable()
		{
			DisableListeners();
		}

		void Update()
		{
			if (this.dataActvity != null && this.dataActvity.IsDirty == true)
			{
				this.eventManager.QueueEvent(new Event_PlayerStateActivityDataChange((activityDataGroupId, this.dataActvity.Data)));
				// TODO : ActivityData : ActivityPersistentDataManager : can't save data at each frame
				// https://gitlab.com/monamipoto/potomaze/-/issues/516
				Save();
			}
		}

		#endregion

		// === ActivityPersistentDataManager ============================================

		#region ActivityPersistentDataManager

		public GameEngine.ScreenPlayGroup ScreenPlayGroup
		{
			get
			{
				return screenPlayGroup;
			}

			set
			{
				screenPlayGroup = value;
			}
		}

		public GameObject ActivityDataGroupId
		{
			get
			{
				return activityDataGroupId;
			}

			set
			{
				activityDataGroupId = value;
			}
		}

		#endregion

		// === IActivityField ===========================================

		#region IActivityField

		public bool SetValue(string name, object value, FieldSearchMethod method)
		{
			bool b = false;
			if (dataActvity != null)
			{
				b = dataActvity.SetValue(name, value, method);
			}
			return b;
		}

		public bool AddValue(string name, object value, FieldSearchMethod method)
		{
			bool b = false;
			if (dataActvity != null)
			{
				b = dataActvity.AddValue(name, value, method);
			}
			return b;
		}

		public bool SubValue(string name, object value, FieldSearchMethod method)
		{
			bool b = false;
			if (dataActvity != null)
			{
				b = dataActvity.SubValue(name, value, method);
			}
			return b;
		}

		public bool DivValue(string name, object value, FieldSearchMethod method)
		{
			bool b = false;
			if (dataActvity != null)
			{
				b = dataActvity.DivValue(name, value, method);
			}
			return b;
		}

		public bool MulValue(string name, object value, FieldSearchMethod method)
		{
			bool b = false;
			if (dataActvity != null)
			{
				b = dataActvity.MulValue(name, value, method);
			}
			return b;
		}

		public bool ResetValue(string name, FieldSearchMethod method)
		{
			bool b = false;
			if (dataActvity != null)
			{
				b = dataActvity.ResetValue(name, method);
			}
			return b;
		}

		public object GetValue(string name, FieldSearchMethod method)
		{
			object obj = null;
			if (dataActvity != null)
			{
				obj = dataActvity.GetValue(name, method);
			}
			return obj;
		}

		public (bool, bool) CompareValue(string name, object value, FieldComparison comparison, FieldSearchMethod method)
		{
			(bool, bool) obj = (false, false);
			if (dataActvity != null)
			{
				obj = dataActvity.CompareValue(name, value, comparison, method);
			}
			return obj;
		}

		#endregion

		// === IActivityFieldServer ===========================================

		#region IActivityFieldServer

		public string GetFullPathField(string name)
		{
			string obj = null;
			if (dataActvity != null)
			{
				obj = dataActvity.Data.GetFullPathField(name);
			}
			return obj;
		}
		
		public bool IsDirty
		{
			get
			{
				bool isDirty = false;
				if (dataActvity != null)
				{
					isDirty = dataActvity.Data.IsDirty;
				}
				return isDirty;
			}
			set
			{
				if (dataActvity != null)
				{
					dataActvity.Data.IsDirty = value;
				}
			}
		}

		public Dictionary<string, FieldInfo> Fields
		{
			get
			{
				Dictionary<string, FieldInfo> fields = null;
				if (dataActvity != null)
				{
					fields = dataActvity.Data.Fields;
				}
				return fields;
			}
		}

		public string Id
		{
			get
			{
				string id = null;
				if (dataActvity != null)
				{
					id = dataActvity.Data.Id;
				}
				return id;
			}
			set
			{
				if (dataActvity != null)
				{
					dataActvity.Data.Id = value;
				}
			}
		}

		public void PopulateFields()
		{
			if (dataActvity != null)
			{
				dataActvity.PopulateFields();
			}
		}


		#endregion

		// === IEnumerator ===========================================

		#region IEnumerator

		public IEnumerator GetEnumerator()
		{
			IEnumerator enumerator = null;
			if (dataActvity != null)
			{
				enumerator = dataActvity.Data.GetEnumerator();
			}
			return enumerator;
		}

		#endregion
		
		// === TRIGGERING ============================================================

		#region TRIGGERING

		/*
		*
		* Triggering
		* 
		*/

		private void EnableListeners()
		{
			// add listener on player state activity event
			eventManager.AddListener<Data.Event_PlayerStateActivityStop>(OnActivityStop);
		}

		private void DisableListeners()
		{
			// listener on player state activity event
			eventManager.RemoveListener<Data.Event_PlayerStateActivityStop>(OnActivityStop);
		}


		void OnActivityStop(Data.Event_PlayerStateActivityStop ev)
		{
			if (IsThisGroup(ev))
			{
				ResetAllValues();
			}
		}

		#endregion
		
		// === IMPLEMENTATION =========================================================

		#region IMPLEMENTATION

		void LogFields()
		{
#if UNITY_EDITOR
			fieldsLog = "";
			foreach (var it in dataActvity.Fields)
			{
				fieldsLog = fieldsLog + it.Value + "\n";
			}
#endif
		}


		/*
		 * 
		 * Activity Data Behavior
		 * 
		 */

		public void StartActivityBehavior(DiContainer diContainer, Transform transform)
		{
			if (activityDataCompute != null)
			{
				var behavior = GameObject.Instantiate(activityDataCompute, Vector3.zero, Quaternion.identity, transform);
				var activityDataBehavior = behavior.GetComponent<Data.IActivityDataBehavior>();
				diContainer.Inject(activityDataBehavior);
				activityDataBehavior.Setup(screenPlayGroup.name, dataActvity.Data, this.ActivityDataGroupId);
			}
		}

		/*
		 * 
		 * Network
		 * 
		 */

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		public void Load()
		{
			if (dataActvity != null)
			{
				StartCoroutine(GetData(dataActvity, null));
				dataActvity.PopulateFields();
				LogFields();
			}
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		public void Save()
		{
			if (dataActvity != null)
			{
				StartCoroutine(PutData(dataActvity));
			}
		}

		[Button(enabledMode: EButtonEnableMode.Playmode)]
		public void ResetAllValues()
		{
			if (dataActvity != null)
			{
				dataActvity.ResetAllValues();
			}
		}

		/*
		 * 
		 * Network
		 * 
		 */

		IEnumerator PutData(Data.ActivityDataScriptable scriptable)
		{
			if (scriptable != null)
			{
				bool done = false;
				void DoneSet(Data.ActivityDataStructSpec spec)
				{
					done = true;
				}
				var set = Data.ActivityDataStructSpec.Create(playerState.IdPlayer, scriptable);
				//var json = JsonUtility.ToJson(set, true);
				networkSession.Put(set, DoneSet);
				yield return new WaitUntil(() => done == true);
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}

		IEnumerator PostData(Data.ActivityDataScriptable scriptable)
		{
			if (scriptable != null)
			{
				bool done = false;
				void DoneSet(Data.ActivityDataStructSpec spec)
				{
					done = true;
				}
				var set = Data.ActivityDataStructSpec.Create(playerState.IdPlayer, scriptable);
				//var json = JsonUtility.ToJson(set, true);
				networkSession.Post(set, DoneSet);
				yield return new WaitUntil(() => done == true);
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}

		IEnumerator GetData(Data.ActivityDataScriptable scriptable, Data.DoneGet<Data.ActivityDataScriptable> doneSet)
		{
			if (scriptable != null)
			{
				bool done = false;

				void LocalDoneSet(Data.ActivityDataStructSpec dataRef)
				{
					// check if user found !!!
					if (dataRef != null)
					{
						dataRef.Make(scriptable);
						doneSet?.Invoke(scriptable);
					}
					// needed for stop corountine
					done = true;
				}

				// load data
				bool ret = networkSession.GetResult(ActivityDataStructSpec.MakeId(playerState.IdPlayer, scriptable.Data.Id), LocalDoneSet);
				yield return new WaitUntil(() => done == true || ret == false);

				if (ret == false)
				{
					scriptable.ResetAllValues();
					yield return PostData(scriptable);
				}
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}
		
		#endregion
	}
}
