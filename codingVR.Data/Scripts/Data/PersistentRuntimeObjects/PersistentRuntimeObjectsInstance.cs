﻿using NaughtyAttributes;
using System;
using System.Collections;
using TypeReferences;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Scripting.APIUpdating;
using UnityEngine.Serialization;
using Zenject;

namespace codingVR.Data
{
	[Serializable]
	[MovedFrom(false, null, null, "RuntimeObjectsDataInstance")]
	public class PersistentRuntimeObjectsInstance : IPersistentRuntimeObjectsInstance
	{
		// === DATA-UI ================================================

		#region DATA-UI

		// activity instance ID
		[Header("Identification")]
		[SerializeField]
		[FormerlySerializedAs("idInstance")]
		string name;

		[Header("Transform")]
		[SerializeField]
		Core.SerializedTransform serializedTransform;

		[Header("Properties")]
		[SerializeField]
		bool isStatic = true;

		[Header("Properties")]
		[SerializeField]
		bool isActive = true;

		[Header("GameObject prefab")]
		[SerializeField]
		AssetReference gameObjectAssetReference;

		#endregion

		// === PersistentRuntimeObjectsInstance =================================

		#region PersistentRuntimeObjectsInstance

		/*
		 * 
		 * ctor
		 * 
		 */

		public PersistentRuntimeObjectsInstance(Transform transform, AssetReference assetReference)
		{
			serializedTransform = new Core.SerializedTransform(transform, false);
			gameObjectAssetReference = assetReference;
			name = transform.name;
			isActive = transform.gameObject.activeSelf;
			isStatic = transform.gameObject.isStatic;
		}

		static public PersistentRuntimeObjectsInstance Create(Transform instance)
		{
			PersistentRuntimeObjectsInstance persistentRuntimeObjectsInstance = null;
			var persistentRuntimeObject = instance.GetComponent<Data.IPersistentRuntimeObject>();
			if (persistentRuntimeObject != null)
			{
				AssetReference assetReference = persistentRuntimeObject.AssetReference;
				if (assetReference != null)
				{
					persistentRuntimeObjectsInstance = new PersistentRuntimeObjectsInstance(instance, assetReference);
				}
			}
			else
			{
				Debug.LogErrorFormat("{0} : the persistent game object {1} is not bound to Data.IPersistentRuntimeObject !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, instance.name);
			}

			return persistentRuntimeObjectsInstance;
		}

		/*
		 * 
		 * implementation
		 * 
		 */

		GameObject ErrorGizmo(string assetRessourcesPath, Transform parent)
		{
			var _prefab = Resources.Load("GameObjectDrawGizmosError");
			var _gameObject = GameObject.Instantiate(_prefab, serializedTransform._position, serializedTransform._rotation, parent) as GameObject;
			codingVR.Core.GameObjectDrawGizmos gameObjectError = _gameObject.GetComponent<codingVR.Core.GameObjectDrawGizmos>();
			gameObjectError.Title = assetRessourcesPath + "\nnot found !!!";
			return _gameObject;
		}

		#endregion

		// === IPersistentRuntimeObjectsInstance ================================

		#region IPersistentRuntimeObjectsInstance

		public string Name => name;
		
		public IEnumerator InstantiateGameObject(DiContainer diContainer, Scene.IResourcesLoader resourcesLoader, Transform parent, Vector3 positionOffset)
		{
			void Done(GameObject go)
			{
				go.SetActive(isActive);
				go.isStatic = isStatic;
                go.transform.position += positionOffset;
				go.name = name;
			}
			yield return resourcesLoader.InstantiateGameObject(diContainer, gameObjectAssetReference, serializedTransform._position, serializedTransform._rotation, parent, Done);
		}

		public void Serialize(Transform transform)
		{
			serializedTransform.SerialWorldTransform(transform);
			isActive = transform.gameObject.activeSelf;
			isStatic = transform.gameObject.isStatic;
		}

		#endregion
	}
}