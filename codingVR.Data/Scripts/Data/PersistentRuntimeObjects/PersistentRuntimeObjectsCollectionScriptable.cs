﻿using codingVR.Core;
using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Scripting.APIUpdating;
using Zenject;

namespace codingVR.Data
{
	#region PersistentRuntimeObjectsCollectionScriptable

	[Serializable] public class PersistentRuntimeObjectsInstanceDictionary : Core.cvrSerializableDictionary<string, PersistentRuntimeObjectsInstance> { }

	[Serializable]
	[CreateAssetMenu(fileName = "PersistentRuntimeObjectsCollectionData", menuName = "CodingVR/Data/RuntimeObjects/PersistentRuntimeObjectsCollectionData", order = 2)]
	[MovedFrom(false, null, null, "PersistentRuntimeObjectsCollectionScriptable")]
	public class PersistentRuntimeObjectsCollectionScriptable : ScriptableObject, IPersistentRuntimeObjectsCollection
	{
		// === DATA-UI =========================================================

		#region DATA-UI


		[Header("Persistent RuntimeObjects Instances")]
		[HorizontalLine(color: EColor.Black)]
		[SerializeField]
		public PersistentRuntimeObjectsInstanceDictionary data = new PersistentRuntimeObjectsInstanceDictionary();

		#endregion

		// === DATA-INTERNAL =========================================================

		#region DATA-INTERNAL

		Data.INetworkSession<Data.PersistentRuntimeObjectsCollectionStructSpec, Data.PersistentRuntimeObjectsCollectionStructSpec> networkSession;

		bool isDirty = false;
		bool preventDirtyFlag = false;

		#endregion

		// === UNITY-LIFECYCLE =========================================================

		#region UNITY-LIFECYCLE

		[Inject]
		public void Constructor(Data.INetworkSession<Data.PersistentRuntimeObjectsCollectionStructSpec, Data.PersistentRuntimeObjectsCollectionStructSpec> networkSession)
		{
			// interface
			this.networkSession = networkSession;
		}

		public void OnValidate()
		{
			IsDirty = true;
		}

		#endregion

		// === IPersistentRuntimeObjectsCollection ==========================================================================================

		#region IPersistentRuntimeObjectsCollection

		public void InstantiateGameObjects(DiContainer diContainer, Scene.IResourcesLoader resourcesLoader, MonoBehaviour mono, Transform parent, Vector3 positionOffset)
		{
			foreach (var it in data)
			{
				mono.StartCoroutine(it.Value.InstantiateGameObject(diContainer, resourcesLoader, parent, positionOffset));
			}
		}

		public void SerializeGameObjects(Transform parent)
		{
			List<string> listToRemove = new List<string>();
			foreach (var it in data)
			{
				if (parent.Find(it.Key) == false)
				{
					listToRemove.Add(it.Key);
				}
			}
			foreach (var it in listToRemove)
			{
				data.Remove(it);
			}
			
			foreach (Transform it in parent)
			{
				if (data.ContainsKey(it.name))
				{
					data[it.name].Serialize(it);
				}
				else
				{
					var instance = PersistentRuntimeObjectsInstance.Create(it);
					if (instance != null)
					{
						data.Add(it.name, instance);
					}
				}
			}
		}

		public void Clear()
		{
			data.Clear();
		}

		public bool IsDirty
		{
			get
			{
				return isDirty;
			}

			set
			{
				if (preventDirtyFlag == false)
				{
					isDirty = value;
				}
			}
		}

		public bool PreventDirtyFlag
		{
			get
			{
				return preventDirtyFlag;
			}

			set
			{
				preventDirtyFlag = value;
			}
		}


		#endregion

		// === IMPLEMENTATION =========================================================

		#region IMPLEMENTATION


		/*
		 * 
		 * Network
		 * 
		 */

		IEnumerator PostData(string idPlayer, string idActivity)
		{
			if (data != null)
			{
				bool done = false;
				void DoneSet(Data.PersistentRuntimeObjectsCollectionStructSpec d)
				{
					done = true;
					IsDirty = false;
				}
				var set = Data.PersistentRuntimeObjectsCollectionStructSpec.Create(idPlayer, idActivity, this);
				networkSession.Post(set, DoneSet);
				yield return new WaitUntil(() => done == true);
			}
			else
			{
				Debug.LogWarningFormat("{0} : not data selected !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
			}
			yield return 0;
		}

		public IEnumerator PutData(string idPlayer, string idActivity, Data.DoneGet<Data.PersistentRuntimeObjectsCollectionScriptable> doneSet = null)
		{
			bool done = false;
			void DoneSet(Data.PersistentRuntimeObjectsCollectionStructSpec d)
			{
				doneSet?.Invoke(this);
				IsDirty = false;
				done = true;
			}
			var set = Data.PersistentRuntimeObjectsCollectionStructSpec.Create(idPlayer, idActivity, this);
			networkSession.Put(set, DoneSet);
			yield return new WaitUntil(() => done == true);
			yield return 0;
		}

		public IEnumerator GetData(string idPlayer, string idActivity, Data.DoneGet<Data.PersistentRuntimeObjectsCollectionScriptable> doneSet)
		{
			int done = 0;

			Data.PersistentRuntimeObjectsCollectionStructSpec dataRef = null;

			void LocalDoneSet(Data.PersistentRuntimeObjectsCollectionStructSpec _dataRef)
			{
				// check if user found !!!
				if (_dataRef != null)
				{
					done = 1;
					dataRef = _dataRef;
				}
				else
				{
					done = -1;
				}
			}

			// load data
			bool b = networkSession.GetResult(PersistentRuntimeObjectsCollectionStructSpec.MakeId(idPlayer, idActivity), LocalDoneSet);
			yield return new WaitUntil(() => done != 0);

			if (done == -1)
			{
				yield return PostData(idPlayer, idActivity);
			}
			else
			{
				dataRef.Make(this);
			}

			doneSet?.Invoke(this);
			
			yield return 0;
		}

		#endregion
	}

	#endregion

	// ===== Data stucture used for managing transfert by client/server ======

	#region PersistentRuntimeObjectsCollectionStructSpec

	[System.Serializable]
	public class PersistentRuntimeObjectsCollectionStructSpec : IGraphDataBase, ISerializationCallbackReceiver
	{
		static string currentVersion = "1";

		[SerializeField]
		string version = currentVersion;

		[SerializeField]
		string assetRessourcesPath;

		[SerializeField]
		PersistentRuntimeObjectsInstanceDictionary data = new PersistentRuntimeObjectsInstanceDictionary();

		public override string ToString()
		{
			return JsonUtility.ToJson(this, true);
		}

		static public string MakeId(string idPlayer, string idActivity)
		{
			return idPlayer + "." + idActivity;
		}

		static public PersistentRuntimeObjectsCollectionStructSpec FromString(string json)
		{
			var spec = new PersistentRuntimeObjectsCollectionStructSpec();
			spec = JsonUtility.FromJson<PersistentRuntimeObjectsCollectionStructSpec>(json);
			return spec;
		}

		public void Make(PersistentRuntimeObjectsCollectionScriptable runtimeObjectsCollectionScriptable)
		{
			runtimeObjectsCollectionScriptable.data = data;
		}

		static public PersistentRuntimeObjectsCollectionStructSpec Create(string idPlayer, string idActivity, PersistentRuntimeObjectsCollectionScriptable runtimeObjectsCollectionScriptable)
		{
			return new PersistentRuntimeObjectsCollectionStructSpec
			{
				Id = MakeId(idPlayer, idActivity),
				data = runtimeObjectsCollectionScriptable.data
			};
		}

		public void OnBeforeSerialize()
		{
		}

		public void OnAfterDeserialize()
		{
			if (version == "2")
			{
			}
		}
	}

	#endregion
}