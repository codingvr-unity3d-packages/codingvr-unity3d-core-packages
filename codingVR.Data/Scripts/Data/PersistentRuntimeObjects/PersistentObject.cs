﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using NaughtyAttributes;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace codingVR.Data
{
	[System.Serializable]
	public class PersistentObject : IPersistentRuntimeObject
	{
		// === IPersistentRuntimeObject ===

		public AssetReference AssetReference
		{
			get
			{
				AssetReference assetReference = null;
				if (string.IsNullOrEmpty(guid) == false)
				{
					assetReference = new AssetReference(guid);
				}
				return assetReference;
			}
		}

		// === implemntation ===

		[SerializeField]
		[ReadOnly]
		string guid;

		public string Guid
		{
			get
			{
				return guid;
			}
			set
			{
				guid = value;
			}
		}

		public string GetGuidAddressFromPrefab(Object target)
		{
			string guid = "";
#if UNITY_EDITOR
			string path = AssetDatabase.GetAssetPath(target);
			guid = AssetDatabase.AssetPathToGUID(path);
#endif
			return guid;
		}
	}
}
