﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using NaughtyAttributes;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace codingVR.Data
{
	public class PersistentRuntimeObject : MonoBehaviour, IPersistentRuntimeObject
	{
		[SerializeField]
		PersistentObject persistentObject = new PersistentObject();

		public AssetReference AssetReference => persistentObject.AssetReference;

		// how to find a gameobjects prefab during runtime
		// https://stackoverflow.com/questions/53992675/how-to-find-a-gameobjects-prefab-during-runtime-with-unity-version-2018-3
		// This function is called when the script is loaded or a value is changed in the Inspector(Called in the editor only).
		void OnValidate() // Which will be called on build or editor play
		{
			persistentObject.Guid = persistentObject.GetGuidAddressFromPrefab(this.gameObject);
		}
	}
}
