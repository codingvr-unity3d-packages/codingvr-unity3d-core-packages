﻿using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TypeReferences;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace codingVR.Data
{
    public class ActivityPersistentRuntimeObjectsManager : MonoBehaviour
    {
        // === DATA-UI ===========================================================================

        #region DATA-UI

        [Header("Runtime Objects Collection")]
        [SerializeField]
        Data.PersistentRuntimeObjectsCollectionScriptable runtimeObjectsCollectionScriptable;

        [Header("Screen Play Group")]
        [Tooltip
            (
            "Screen Play Group identifies the owner of this pesistent runtime objects manager.\n"
            )]
        [SerializeField]
        GameEngine.ScreenPlayGroup screenPlayGroup;

        [Header("Transform")]
        [SerializeField]
        Vector3 positionOffset = Vector3.zero;
        
        #endregion

        // === DATA-PRIVATE ============================================================

        #region DATA-PRIVATE

        Scene.IResourcesLoader resourcesLoader;
        Data.INetworkSession<Data.ActivityDataStructSpec, Data.ActivityDataStructSpec> networkSession;
        GameEngine.IPlayerState playerState;
        Core.IEventManager eventManager;
        DiContainer diContainer;
        bool isLoaded = false;

        #endregion

        // === IMPLEMENTATION-UTILS ============================================================

        #region IMPLEMENTATION-UTILS

        bool ContainsIdInstanceActivity(string groupTagName)
        {
            bool b = false;
            if (screenPlayGroup != null)
            {
                b = screenPlayGroup.name.Contains(groupTagName);
            }
            return b;
        }

        bool IsThisGroup(GameEngine.Event_ScreenPlayTagBase ev)
        {
            var groupData = ev.Data as Tuple<string, Data.Tag>;
            return ContainsIdInstanceActivity(groupData.Item2.ToString());
        }

        bool IsThisGroup(Data.Event_PlayerStateActivityBase ev)
        {
            var groupData = ev.Data as string;
            return ContainsIdInstanceActivity(groupData);
        }

        #endregion

        // === LIFE-CYCLE ============================================================

        #region LIFE-CYCLE

        /*
		 *
		 * life cycle
		 * 
		 */

        [Inject]
        public void Constructor(DiContainer diContainer, Data.INetworkSession<Data.ActivityDataStructSpec, Data.ActivityDataStructSpec> networkSession, GameEngine.IPlayerState playerState, Core.IEventManager eventManager, Scene.IResourcesLoader resourcesLoader)
        {
            this.networkSession = networkSession;
            this.playerState = playerState;
            this.eventManager = eventManager;
            this.diContainer = diContainer;
            this.resourcesLoader = resourcesLoader;
            if (runtimeObjectsCollectionScriptable != null)
            {
                diContainer.Inject(runtimeObjectsCollectionScriptable);
            }
        }

        void OnEnable()
        {
            EnableListeners();
        }

        void OnDisable()
        {
            DisableListeners();
        }

        private void Start()
        {
        }

        public void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus == true)
            {
                Save();
            }
        }

        void Update()
        {
            if (this.runtimeObjectsCollectionScriptable != null && this.runtimeObjectsCollectionScriptable.IsDirty == true)
            {
                //this.eventManager.QueueEvent(new Event_PlayerStateActivityDataChange((activityDataGroupId, this.dataActvity.Data)));
                // TODO : ActivityData : ActivityPersistentDataManager : can't save data at each frame
                // https://gitlab.com/monamipoto/potomaze/-/issues/516
                Save();
            }
        }

        #endregion

        // === TRIGGERING ============================================================

        #region TRIGGERING

        /*
		*
		* Triggering
		* 
		*/

        private void EnableListeners()
        {
            // add listener on player state activity event
            eventManager.AddListener<Event_PlayerStateActivityStop>(OnActivityStop);

            // add listener on player state activity event
            eventManager.AddListener<Data.Event_PlayerStateActivitiesJustStarting>(OnActivitiesJustStarting);

            // add listener on runtime objects changes
            eventManager.AddListener<GameEngine.Event_RuntimeObjectsChange>(OnRuntimeObjectsChange);

            // add listener on Event_PlayerExitingGame
            eventManager.AddListener<Event_PlayerExitingGame>(OnPlayerExitingGame);

        }

        private void DisableListeners()
        {
            // remove listener on player state activity event
            eventManager.RemoveListener<Data.Event_PlayerStateActivityStop>(OnActivityStop);

            // remove listener on player state activity event
            eventManager.RemoveListener<Data.Event_PlayerStateActivitiesJustStarting>(OnActivitiesJustStarting);

            // remove listener on runtime objects changes
            eventManager.RemoveListener<GameEngine.Event_RuntimeObjectsChange>(OnRuntimeObjectsChange);

            // remove listener on Event_PlayerExitingGame
            eventManager.RemoveListener<Event_PlayerExitingGame>(OnPlayerExitingGame);
        }


        void OnActivityStop(Data.Event_PlayerStateActivityStop ev)
        {
            if (IsThisGroup(ev))
            {
                //ResetAllValues();
            }
        }

        void OnActivitiesJustStarting(Data.Event_PlayerStateActivitiesJustStarting ev)
        {
            Load();
        }

        void OnRuntimeObjectsChange(GameEngine.Event_RuntimeObjectsChange ev)
        {
            (string idGroup, string idObject, string objectName) = ((string idGroup, string idObject, string objectName))ev.Data;
            if (idGroup.Equals(screenPlayGroup.name) == true)
            {
                Save();
            }
        }

        private void OnPlayerExitingGame(codingVR.Data.Event_PlayerExitingGame ev)
        {
            Save();
        }

        #endregion

        // === IMPLEMENTATION =========================================================

        #region IMPLEMENTATION

        /*
		 * 
		 * Network
		 * 
		 */

        [Button(enabledMode: EButtonEnableMode.Playmode)]
        public void Load()
        {
            if (runtimeObjectsCollectionScriptable != null)
            {
                void Instantiate(Data.PersistentRuntimeObjectsCollectionScriptable data)
                {
                    data.InstantiateGameObjects(diContainer, resourcesLoader, this, transform, this.positionOffset);
                    this.isLoaded = true;
                    // HACK: to prevent isDirtyFlag = true when the inspector is modified on OnValidate; 
                    // we prevent dirty Flag and we wait a frame count; SO we "hope" that the inspector refresh is past
                    this.DelayedFunction(() => runtimeObjectsCollectionScriptable.PreventDirtyFlag = false, 10);
                }
                runtimeObjectsCollectionScriptable.PreventDirtyFlag = true;
                StartCoroutine(runtimeObjectsCollectionScriptable.GetData(playerState.IdPlayer, screenPlayGroup.name, Instantiate));
            }
        }

        [Button(enabledMode: EButtonEnableMode.Playmode)]
        public void Save()
        {
            if (isLoaded == true /* to prevent saving empty data ; we need to have loaded first */)
            {
                if (runtimeObjectsCollectionScriptable != null)
                {
                    runtimeObjectsCollectionScriptable.SerializeGameObjects(transform);
                    StartCoroutine(runtimeObjectsCollectionScriptable.PutData(playerState.IdPlayer, screenPlayGroup.name));
                }
            }
        }

        #endregion
    }
}
