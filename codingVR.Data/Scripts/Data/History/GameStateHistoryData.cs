﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using codingVR.Core;

namespace codingVR.Data
{
	[System.Serializable]
	public class GameStateHistoryData
	{
		// version
		static string currentVersion = "1";
		public string version = currentVersion;

		// Informations when the player is answering to a question
		public string titleQuestion;
		public string question;
		public string answer;

		// Informations when the player is entering a tag
		public string tag;

		// TODO: for futur usage
		public bool checkPoint = false;
	}
}
