﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

namespace codingVR.Data
{
    [Flags]
    [System.Serializable]
    public enum TypeTips
    {
        Finance = 1 << 0,
        Santé = 1 << 1,
        Education = 1 << 2,
        Travail = 1 << 3,
    }

    [CreateAssetMenu(fileName = "TipsData", menuName = "CodingVR/Data/TipsData", order = 6)]
    public class TipsDataScriptable : ScriptableObject
    {
        public int id;
        public string name;
        public string description;
        public TypeTips typeTips;
        public VideoPlayer video;

    }

}