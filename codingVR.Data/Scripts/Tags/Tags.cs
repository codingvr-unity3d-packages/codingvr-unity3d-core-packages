﻿namespace codingVR.Data
{
    public class Tags
    {
		public static Data.Tag Welcome { get; } = Data.Tag.CreateTag("welcome");
		public static Data.Tag Player { get; } = Data.Tag.CreateTag("player");
    }
}
