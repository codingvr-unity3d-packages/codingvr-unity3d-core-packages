﻿using System;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEngine.Events;

namespace codingVR.Data
{
	// === TagPair ===

	public class TagPair
	{
		Tag first;
		Tag second;

		public Tag First
		{
			get
			{
				return first;
			}
		}

		public Tag Second
		{
			get
			{
				return second;
			}
		}

		int hashCode;

		int ComputeHashCode(Tag first, Tag second)
		{
			string hashString = "";
			if (first != null)
				hashString += first.TagName;
			if (second != null)
				hashString += second.TagName;
			return hashString.GetHashCode();
		}

		private TagPair(Tag first, Tag second)
		{
			this.first = first;
			this.second = second;
			hashCode = ComputeHashCode(first, second);
		}

		public override string ToString()
		{
			return First.ToString() + "->" + Second.ToString();
		}

		static public TagPair Swap(TagPair pair)
		{
			return new TagPair(pair.second, pair.first);
		}

		static public TagPair MakePair(Tag first, Tag second)
		{
			return new TagPair(first, second);
		}

		static public TagPair MakePair(string first, string second)
		{
			return new TagPair(Tag.CreateTag(first), Tag.CreateTag(second));
		}

		static public TagPair MakeFirst(string first)
		{
			return new TagPair(Tag.CreateTag(first), null);
		}

		static public TagPair MakeFirst(Tag first)
		{
			return new TagPair(first, null);
		}

		public override bool Equals(object other)
		{
			if (other == null)
				return false;
			if (other == this)
				return true;
			bool a = (other as TagPair).first.Equals(first);
			bool b = (other as TagPair).second.Equals(second);

			return (a & b);
		}

		public override int GetHashCode()
		{
			return hashCode;
		}
	}

	// === TagPathPair ===

	public class TagPathPair
	{
		TagPath first;
		TagPath second;

		public TagPath First
		{
			get
			{
				return first;
			}
		}

		public TagPath Second
		{
			get
			{
				return second;
			}
		}

		int hashCode;

		int ComputeHashCode(TagPath first, TagPath second)
		{
			string hashString = "";
			hashString += first.TagPathName;
			hashString += second.TagPathName;
			return hashString.GetHashCode();
		}

		private TagPathPair(TagPath first, TagPath second)
		{
			this.first = first;
			this.second = second;
			hashCode = ComputeHashCode(first, second);
		}

		public override string ToString()
		{
			return First.ToString() + "->" + Second.ToString();
		}

		static public TagPathPair Swap(TagPathPair pair)
		{
			return new TagPathPair(pair.second, pair.first);
		}

		static public TagPathPair MakePair(TagPath first, TagPath second)
		{
			return new TagPathPair(first, second);
		}

		static public TagPathPair MakePair(string first, string second)
		{
			return new TagPathPair(TagPath.Parse(first), TagPath.Parse(second));
		}

		public TagPair TagPair
		{
			get
			{
				return TagPair.MakePair(first.tag, second.tag);
			}
		}


		public override bool Equals(object other)
		{
			if (other == null)
				return false;
			if (other == this)
				return true;
			bool a = (other as TagPathPair).first.Equals(first);
			bool b = (other as TagPathPair).second.Equals(second);

			return (a & b);
		}

		public override int GetHashCode()
		{
			return hashCode;
		}
	}

	// === Tag + Path

	public struct TagPath
	{
		readonly public string identifier;
		readonly public Tag tag;

		TagPath(string identifier, Tag tag)
		{
			this.identifier = identifier;
			this.tag = tag;
		}

		TagPath(Tag tag)
		{
			this.identifier = "";
			this.tag = tag;
		}

		public string TagPathName
		{
			get
			{
				if (String.IsNullOrEmpty(this.identifier))
					return tag.ToString();
				else
					return this.identifier + "." + tag.ToString();
			}
		}

		public override string ToString()
		{
			return TagPathName;
		}

		public override bool Equals(object other)
		{
			if (other == null)
				return false;
			return ((TagPath)other).TagPathName.Equals(TagPathName );
		}

		public override int GetHashCode()
		{
			return TagPathName.GetHashCode();
		}

		static public TagPath Parse(string fullTagPath)
		{
			TagPath tagPath;
			var split = fullTagPath.Split('.');
			switch (split.Length)
			{
				case 1:
					{
						tagPath = new TagPath(Tag.CreateTag(split[0]));
					}
					break;
				case 2:
					{
						tagPath = new TagPath(split[0], Tag.CreateTag(split[1]));
					}
					break;

				default:
					{
						tagPath = new TagPath(Tags.Welcome);
						Debug.LogErrorFormat("{0} : TagPath invalid {1} !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, fullTagPath);
					}
					break;
			}
			return tagPath;
		}
	}

	// === Tag ===

	public class Tag
	{
		readonly string tagName;

		private Tag(string tagName)
		{
			if (IsTag(tagName) == true)
			{
				this.tagName = tagName.Substring(1, tagName.Length - 1);
			}
			else
			{
				this.tagName = tagName;
			}
		}

		private Tag(Tag tag)
		{
			this.tagName = tag.tagName;
		}

		public string TagName
		{
			get
			{
				return tagName;
			}
		}

		public override string ToString()
		{
			if (String.IsNullOrEmpty(this.tagName) == false)
				return "#" + this.tagName;
			return "";
		}

		static public string ToTaggedString(string tagLabel)
		{
			if (IsTag(tagLabel) == false && String.IsNullOrEmpty(tagLabel) == false)
				tagLabel = "#" + tagLabel;
			return tagLabel;
		}

		static public bool IsTag(string tag)
		{
			bool bRet = false;
			if (String.IsNullOrEmpty(tag) == false)
			{
				if (tag[0] == '#')
				{
					bRet = true;
				}
			}
			return bRet;
		}

		static public string ToIDString(string tagLabel)
		{
			if (IsTag(tagLabel) == true && String.IsNullOrEmpty(tagLabel) == false)
				tagLabel = tagLabel.Substring(1);
			return tagLabel;
		}

		static public Tag CreateTag(string tag)
		{
			Tag tagEntity = null;
			if (String.IsNullOrEmpty(tag) == false)
				tagEntity = new Tag(tag);
			return tagEntity;
		}

		static public Tag CreateTag(Tag tag)
		{
			Tag tagEntity = tagEntity = new Tag(tag);
			return tagEntity;
		}

		public void Save(codingVR.Core.GameDataWriter writer)
		{
			writer.Write(tagName, "tag");
		}

		public static Tag Load(codingVR.Core.GameDataReader reader, int version)
		{
			var tagName = reader.ReadString();
			return new Tag(tagName);
		}

		public override bool Equals(object other)
		{
			if (other == null)
				return false;
			if (other == this)
				return true;

			bool bCheck = false;

			if ((other as Tag) != null)
			{
				bCheck = ((other as Tag).tagName == tagName);
			}

			if ((other as string) != null)
			{
				string s = (other as string);
				bCheck = (s.Equals(tagName) || s.Equals(ToString()));
			}

			return bCheck;
		}

		public override int GetHashCode()
		{
			return tagName.GetHashCode();
		}
	}

	// === Tag controler ===

	public class TagController<T>
	{
		// === ctor ===

		public TagController() { }
		public TagController(Tag tag)
		{
			this.tag = tag;
		}

		// === tag ===

		Tag tag;

		public Tag Tag
		{
			get
			{
				return tag;
			}
			set
			{
				if (tag != value)
				{
					tag = value;
				}
			}
		}

		// === delegate ===

		virtual public void OnRemoved() { }
		virtual public void OnRemovedLink(Tag linkedTag) { }
		virtual public void OnMoved(T previousData) { }
		virtual public void OnAdded() { }
		virtual public bool OnReplaced(Tag from, Tag to) { return false; }
		virtual public void Save(codingVR.Core.GameDataWriter writer) { }
		virtual public void Load(codingVR.Core.GameDataReader reader, Tag tag, int version) { }
	};
		
	// === TagCollectionManager ===

	public class TagCollectionManager<Controller, T> where T : TagController<T>, new()
	{
		public class TagCollectionOrdered : KeyedCollection<Tag, T>
		{
			protected override Tag GetKeyForItem(T item)
			{
				return item.Tag;
			}

			public void ReplaceKey(Data.Tag from, Data.Tag to)
			{
				// replace key by a new key
				// TODO : Hack : KeyedCollection fails to replace automatically keys
				var v = this[from];
				Remove(from);
				v.OnReplaced(from, to);
				Add(v);
			}
		}

		TagCollectionOrdered dictionay = new TagCollectionOrdered();

		readonly Controller controller;

		// === Ctor ===

		public TagCollectionManager(Controller controller)
		{
			this.controller = controller;
		}

		// === Contains ===

		public bool Contains(Tag tag)
		{
			return dictionay.Contains(tag);
		}

		// === Collection ===

		public Collection<T> Items
		{
			get
			{
				return dictionay;
			}
		}

		// === States ===

		public class Event : UnityEvent<TagPair> { }
		public Event OnSelecting { get; } = new Event();
		
		TagPair selectedItem;

		public TagPair Selected
		{
			get
			{
				return selectedItem;
			}
			set
			{
				if (value != selectedItem)
				{
					OnSelecting.Invoke(value);
					selectedItem = value;
				}
			}
		}

		// === Accessor ===

		public bool Set(T metaData)
		{
			if (metaData == null)
				return false;

			bool b = dictionay.Contains(metaData.Tag);
			if (b == false)
			{
				dictionay.Add(metaData);
				dictionay[metaData.Tag].OnAdded();
			}
			else
			{
				metaData = dictionay[metaData.Tag];
				dictionay.Remove(metaData.Tag);
				dictionay.Add(metaData);
				dictionay[metaData.Tag].OnMoved(metaData);
			}
			return !b;
		}

		public bool Remove(Tag tag)
		{
			bool b = false;
			if (tag != null)
			{
				b = dictionay.Contains(tag);
				if (b == true)
				{
					dictionay[tag].OnRemoved();
					dictionay.Remove(tag);
				}
			}
			return b;
		}

		public bool RemoveLink(TagPair tag)
		{
			bool b = false;
			if (tag != null)
			{
				b = dictionay.Contains(tag.First);
				if (b == true)
				{
					dictionay[tag.First].OnRemovedLink(tag.Second);
				}
			}
			return b;
		}

		public bool Replace(Tag from, Tag to)
		{
			bool b = false;
			if (from != null && to != null)
			{
				var fromFound = dictionay.Contains(from);
				var toFound = dictionay.Contains(to);
				if (fromFound == true && toFound == false)
				{
					b = true;
					// replace key by a new key
					dictionay.ReplaceKey(from, to);

					foreach (var it in dictionay)
					{
						if (it.OnReplaced(from, to) == true)
						{
						}
					}
				}
			}
			return b;
		}

		public T Find(Tag tag)
		{
			return tag != null ? dictionay[tag] : null;
		}

		public T Find(TagPair tagPair)
		{
			if (tagPair.First != null)
			{
				if (dictionay.Contains(tagPair.First))
				{
					return dictionay[tagPair.First];
				}
				else
				{
					Debug.LogError("tagPair.First " + tagPair.First + " not found !!!");
				}
			}
			return null;
		}
		
		// === Clear ===

		public void Clear()
		{
			dictionay.Clear();
		}

		// === Load/Save ===

		public void Save(codingVR.Core.GameDataWriter writer)
		{
			writer.Write(dictionay.Count);
			foreach (var it in dictionay)
			{
				Tag.CreateTag(it.Tag).Save(writer);
				it.Save(writer);
			}
		}

		public void Load(codingVR.Core.GameDataReader reader, int version)
		{
			var count = reader.ReadInt();
			for (int i = 0; i < count; ++i)
			{
				Tag tag = Tag.Load(reader, version);
				T value = (T)Activator.CreateInstance(typeof(T), controller);
				value.Load(reader, tag, version);
				Set(value);
			}
		}
	};
}