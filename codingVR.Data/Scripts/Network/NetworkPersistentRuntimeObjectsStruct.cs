﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Scripting.APIUpdating;

namespace codingVR.Data
{
	[MovedFrom(false, null, null, "NetworkRuntimeObjectsStruct")]
	public class NetworkPersistentRuntimeObjectsStruct : NetworkBase<PersistentRuntimeObjectsCollectionStructSpec, PersistentRuntimeObjectsCollectionStructSpec>
	{
		override public string DataName { get { return "PersistentRuntimeObjects"; } }
		
		[Serializable]
		public class PersistentRuntimeObjectsDataCollection : Core.cvrSerializableDictionary<string, PersistentRuntimeObjectsCollectionStructSpec> { }
		[SerializeField]
		PersistentRuntimeObjectsDataCollection data = new PersistentRuntimeObjectsDataCollection();


		// on OnEnable we load the entire Json file
		void OnEnable()
		{
			Load();
		}

		// on OnDisbale we save the entire json file
		void OnDisable()
		{
			Save();
		}
	
		/*
		 *
		 * Load / Save files
		 * 
		 */

		private void Load()
		{
			string json = LoadJsonResults();
			if (json.Length > 0)
			{
				data = JsonUtility.FromJson<PersistentRuntimeObjectsDataCollection>(json);
			}
		}

		private void Save()
		{
			string json = JsonUtility.ToJson(data, true);
			if (json.Length > 0)
			{
				SaveJsonResults(json);
			}
		}

		/*
		 * 
		 * Data specification
		 * 
		 */

		override public PersistentRuntimeObjectsCollectionStructSpec GetDataSpec(string Id)
		{
			PersistentRuntimeObjectsCollectionStructSpec item = null;
			Load();
			if (data.ContainsKey(Id) == true)
			{
				item = data[Id];
			}
			return item;
		}

		override public void PostDataSpec(string Id, PersistentRuntimeObjectsCollectionStructSpec dataItem)
		{
			if (data.ContainsKey(Id) == false)
			{
				var json = dataItem.ToString();
				var newDataItem = PersistentRuntimeObjectsCollectionStructSpec.FromString(json);
				data.Add(Id, newDataItem);
				Save();
			}
		}

		/*
		 * 
		 * Data result
		 * 
		 */

		override public PersistentRuntimeObjectsCollectionStructSpec GetDataResult(string Id)
		{
			PersistentRuntimeObjectsCollectionStructSpec item = null;
			Load();
			if (data.ContainsKey(Id) == true)
			{
				item = data[Id];
			}
			return item;
		}

		override public void PutData(string Id, PersistentRuntimeObjectsCollectionStructSpec dataItem)
		{
			if (data.ContainsKey(Id) == true)
			{
				data[Id] = dataItem;
				Save();
			}
		}

		override public void PostData(string Id, PersistentRuntimeObjectsCollectionStructSpec dataItem)
		{
			if (data.ContainsKey(Id) == false)
			{
				var json = dataItem.ToString();
				var newDataItem = PersistentRuntimeObjectsCollectionStructSpec.FromString(json);
				data.Add(Id, newDataItem);
				Save();
			}
		}
	}
}