﻿using System;
using UnityEngine;

namespace codingVR.Data
{
	public class NetworkGraphDataLink : NetworkBase<GetGraphDataLinkSpec, SetGraphDataLinkResult>
	{
		override public string DataName { get { return "Link"; } }

		/* 
		 * 
		 * GetGraphDataLinkSpec
		 * 
		 */
		 
		[Serializable]
		public class GetGraphDataLinkSpecCollection : Core.cvrSerializableDictionary<string, GetGraphDataLinkSpec> { }
		[SerializeField]
		GetGraphDataLinkSpecCollection dataSpec = new GetGraphDataLinkSpecCollection();
		

		/* 
		 * 
		 * SetGraphDataLinkResult
		 * 
		 */

		[Serializable]
		public class SetGraphDataLinkResultCollection : Core.cvrSerializableDictionary<string, SetGraphDataLinkResult> { }
		[SerializeField]
		SetGraphDataLinkResultCollection dataResult = new SetGraphDataLinkResultCollection();


		// on OnEnable we load the entire Json file
		void OnEnable()
		{
			Load();
		}

		// on OnDisbale we save the entire json file
		void OnDisable()
		{
			Save();
		}

		/*
		 *
		 * Load / Save files
		 * 
		 */

		private void Load()
		{
			{
				string json = LoadJsonResults();
				if (json.Length > 0)
				{
					dataResult = JsonUtility.FromJson<SetGraphDataLinkResultCollection>(json);
				}
			}
			{
				string json = LoadJsonSpecs();
				if (json.Length > 0)
				{
					dataSpec = JsonUtility.FromJson<GetGraphDataLinkSpecCollection>(json);
				}
			}
		}

		private void Save()
		{
			{
				string json = JsonUtility.ToJson(dataResult, true);
				if (json.Length > 0)
				{
					SaveJsonResults(json);
				}
			}
			{
				string json = JsonUtility.ToJson(dataSpec, true);
				if (json.Length > 0)
				{
					SaveJsonSpecs(json);
				}
			}
		}

		/*
		 * 
		 * Data Specification
		 * 
		 */

		override public GetGraphDataLinkSpec GetDataSpec(string Id)
		{
			GetGraphDataLinkSpec item = null;
			Load();
			if (dataSpec.ContainsKey(Id) == true)
			{
				item = dataSpec[Id];
			}
			return item;
		}

		override public void PostDataSpec(string Id, GetGraphDataLinkSpec dataItem)
		{
			if (dataSpec.ContainsKey(Id) == false)
			{
				var json = dataItem.ToString();
				var newDataItem = GetGraphDataLinkSpec.FromString(json);
				dataSpec.Add(Id, newDataItem);
				Save();
			}
		}

		/*
		 * 
		 * Data Result
		 * 
		 */

		override public SetGraphDataLinkResult GetDataResult(string Id)
		{
			SetGraphDataLinkResult item = null;
			Load();
			if (dataResult.ContainsKey(Id) == true)
			{
				item = dataResult[Id];
			}
			return item;
		}

		override public void PutData(string Id, SetGraphDataLinkResult dataItem)
		{
			if (dataResult.ContainsKey(Id) == true)
			{
				dataResult[Id] = dataItem;
				Save();
			}
		}

		override public void PostData(string Id, SetGraphDataLinkResult dataItem)
		{
			if (dataResult.ContainsKey(Id) == false)
			{
				var json = dataItem.ToString();
				var newDataItem = SetGraphDataLinkResult.FromString(json);
				dataResult.Add(Id, newDataItem);
				Save();
			}
		}
	}
}