﻿using UnityEngine;
using UnityEditor;
using Proyecto26;
using System.IO;
using System;

namespace codingVR.Data
{
	public abstract class NetworkBase<DataSpec, DataResult> : MonoBehaviour, INetworkSession<DataSpec, DataResult> where DataSpec : IGraphDataBase where DataResult : IGraphDataBase
	{
		// https://jsonlint.com/
		// https://my-json-server.typicode.com/shadd3/potodatamodel/db
		// https://github.com/shadd3/potodatamodel

		private RequestHelper currentRequest;
		private void LogMessage(string title, string message)
		{

#if UNITY_EDITOR

			if (EnabledDebugBox == true)
			{
				EditorUtility.DisplayDialog(title, message, "Ok");
			}

#endif

			Debug.Log(message);
		}

		// Enable Debug

		[SerializeField]
		bool debug = false;
		public bool EnabledDebug
		{
			get
			{
				return debug;
			}
			set
			{
				debug = value;
			}
		}

		[SerializeField]
		bool debugBox = false;
		public bool EnabledDebugBox
		{
			get
			{
				return debugBox;
			}
			set
			{
				debugBox = value;
			}
		}

		// Enable Emulation

		[SerializeField]
		bool emulation = false;
		public bool EnabledEmulation
		{
			get
			{
				return emulation;
			}
			set
			{
				emulation = value;
			}
		}

		// User route

		[SerializeField]
		string userRoute = "https://my-json-server.typicode.com/shadd3/potodatamodel/";
		public string UserRoute
		{
			get
			{
				return userRoute;
			}
			set
			{
				userRoute = value;
			}
		}

		[SerializeField]
		string userRouteEmulation = "potodatamodel/";
		public string UserRouteEmulation
		{
			get
			{
				return userRouteEmulation;
			}
			set
			{
				userRouteEmulation = value;
			}
		}
		
		// Data name

		abstract public string DataName { get; }

		// Last command

		string lastCommand;

		public string LastCommand
		{
			get
			{
				return lastCommand;
			}
		}

		/*
		 *
		 * Get DataSpec
		 * 
		 */

		public bool GetSpecNetwork(string id, DoneGet<DataSpec> done)
		{
			string Uri = UserRoute + "GetSpec" + this.DataName + "/" + id;
			currentRequest = new RequestHelper
			{
				Uri = Uri,
				EnableDebug = this.EnabledDebug
			};

			void ErrorCallback(Exception err)
			{
				this.LogMessage("Error", err.Message);

				// done
				done(null);
			}

			RestClient.Get<DataSpec>(currentRequest).Then(firstUser =>
			{
				RestClient.ClearDefaultHeaders();
				done(firstUser);
			}).Catch(err => ErrorCallback(err));

			lastCommand = Uri;

			// can't wait to get error result done in asynchrnous method
			return true;
		}

		abstract public DataSpec GetDataSpec(string id);

		public bool GetSpecEmulationNetwork(string id, DoneGet<DataSpec> done)
		{
			// get data
			var data = GetDataSpec(id);
			// send data
			done(data);

			// return
			bool bReturn = false;
			if (data != null)
			{ 
				bReturn = true;
			}
			else
			{
				this.LogMessage("Error", "GetSpecEmulationNetwork : id " + id + " not found for struct " +  typeof(DataSpec) + " !!!");
			}

			return bReturn;
		}

		public bool GetSpec(string id, DoneGet<DataSpec> done)
		{
			bool result = false;
			if (EnabledEmulation == false)
				result = GetSpecNetwork(id, done);
			else
				result = GetSpecEmulationNetwork(id, done);
			return result;
		}


		/*
		 *
		 * Post DataSpec
		 * 
		 */

		public bool PostSpecNetwork(DataSpec data, DoneSet<DataSpec> done)
		{
			string Uri = UserRoute + "SetSpec" + this.DataName;

			currentRequest = new RequestHelper
			{
				Uri = Uri,
				Body = data,
				EnableDebug = this.EnabledDebug
			};

			void ErrorCallback(Exception err)
			{
				this.LogMessage("Error", err.Message);

				// done
				done(null);
			}

			RestClient.Post<DataSpec>(currentRequest).Then(res =>
			{
				// done
				done(data);

				// And later we can clear the default query string params for all requests
				RestClient.ClearDefaultParams();

			}).Catch(err => ErrorCallback(err));

			lastCommand = Uri;

			// can't wait to get error result done in asynchrnous method
			return true;
		}

		public abstract void PostDataSpec(string Id, DataSpec data);

		public bool PostSpecEmulationNetwork(DataSpec data, DoneSet<DataSpec> done)
		{
			// Put data
			PostDataSpec(data.Id, data);
			// done
			done(data);

			return true;
		}

		public bool PostSpec(DataSpec data, DoneSet<DataSpec> done)
		{
			bool result = false;
			if (EnabledEmulation == false)
				result = PostSpecNetwork(data, done);
			else
				result = PostSpecEmulationNetwork(data, done);
			return result;
		}

		/*
		 *
		 * Get DataResult
		 * 
		 */

		public bool GetResultNetwork(string id, DoneGet<DataResult> done)
		{
			string Uri = UserRoute + "GetResult" + this.DataName + "/" + id;
			currentRequest = new RequestHelper
			{
				Uri = Uri,
				EnableDebug = this.EnabledDebug
			};

			void ErrorCallback(Exception err)
			{
				this.LogMessage("Error", err.Message);

				// done
				done(null);
			}

			RestClient.Get<DataResult>(currentRequest).Then(firstUser =>
			{
				RestClient.ClearDefaultHeaders();
				done(firstUser);

			}).Catch(err => ErrorCallback(err));

			lastCommand = Uri;

			// can't wait to get error result done in asynchrnous method
			return true;
		}

		public abstract DataResult GetDataResult(string Id);

		public bool GetResultEmulationNetwork(string id, DoneGet<DataResult> done)
		{
			// get data
			var data = GetDataResult(id);
			// send data
			done(data);

			// return
			bool bReturn = false;
			if (data != null)
			{
				bReturn = true;
			}
			else
			{
				this.LogMessage("Error", "GetResultEmulationNetwork : id " + id + " not found for struct " + typeof(DataSpec) + " !!!");
			}

			return bReturn;
		}

		public bool GetResult(string id, DoneGet<DataResult> done)
		{
			bool result;
			if (EnabledEmulation == false)
				result = GetResultNetwork(id, done);
			else
				result = GetResultEmulationNetwork(id, done);
			return result;
		}

		/*
		 *
		 * Post DataResult
		 * 
		 */

		public bool PostNetwork(DataResult data, DoneSet<DataResult> done)
		{
			string Uri = UserRoute + "SetResult" + this.DataName;

			currentRequest = new RequestHelper
			{
				Uri = Uri,
				Body = data,
				EnableDebug = this.EnabledDebug
			};

			void ErrorCallback(Exception err)
			{
				this.LogMessage("Error", err.Message);

				// done
				done(null);
			}

			RestClient.Post<DataResult>(currentRequest).Then(res =>
			{
				// done
				done(data);

				// And later we can clear the default query string params for all requests
				RestClient.ClearDefaultParams();

			}).Catch(err => ErrorCallback(err));

			lastCommand = Uri;

			// can't wait to get error result done in asynchrnous method
			return true;
		}

		public abstract void PostData(string Id, DataResult data);

		public bool PostEmulationNetwork(DataResult data, DoneSet<DataResult> done)
		{
			// Put data
			PostData(data.Id, data);
			// done
			done(data);

			return true;
		}

		public bool Post(DataResult data, DoneSet<DataResult> done)
		{
			bool result = false;
			if (EnabledEmulation == false)
				result = PostNetwork(data, done);
			else
				result = PostEmulationNetwork(data, done);
			return result;
		}

		/*
		 *
		 * Put DataResult
		 * 
		 */

		public bool PutNetwork(DataResult data, DoneSet<DataResult> done)
		{
			string Uri = UserRoute + "SetResult" + this.DataName + "/" + data.Id;

			currentRequest = new RequestHelper
			{
				Uri = Uri,
				Body = data,
				EnableDebug = this.EnabledDebug,
				Retries = 5,
				RetrySecondsDelay = 1,
				RetryCallback = (err, retries) =>
				{
					Debug.Log(string.Format("Retry #{0} Status {1}\nError: {2}", retries, err.StatusCode, err));
				}
			};

			void ErrorCallback(Exception err)
			{
				this.LogMessage("Error", err.Message);

				// done
				done(null);
			}


			RestClient.Put<DataResult>(currentRequest).Then(res =>
			{
				// done
				done(data);

				// And later we can clear the default query string params for all requests
				RestClient.ClearDefaultParams();
			}).Catch(err => ErrorCallback(err)); 
		

			lastCommand = Uri;

			// can't wait to get error result done in asynchrnous method
			return true;
		}

		public abstract void PutData(string Id, DataResult data);

		public bool PutEmulationNetwork(DataResult data, DoneSet<DataResult> done)
		{
			// Put data
			PutData(data.Id, data);
			// done
			done(data);

			return true;
		}

		public bool Put(DataResult data, DoneSet<DataResult> done)
		{
			bool result = true;
			if (EnabledEmulation == false)
				result = PutNetwork(data, done);
			else
				result = PutEmulationNetwork(data, done);
			return result;
		}

		/*
		 *
		 * ABORT REST function testing
		 * 
		 */

		public void AbortRequest()
		{
			if (currentRequest != null)
			{
				currentRequest.Abort();
				currentRequest = null;
			}
		}

		/*
		 * 
		 * JSON Utilities
		 * 
		 */

		public void SaveJsonResults(string json)
		{
			SaveJsonBase("Results", json);
		}

		public string LoadJsonResults()
		{
			return LoadJsonBase("Results");
		}

		public void SaveJsonSpecs(string json)
		{
			SaveJsonBase("Specs", json);
		}

		public string LoadJsonSpecs()
		{
			return LoadJsonBase("Specs");
		}

		void SaveJsonBase(string domain, string json)
		{
			string path = Path.Combine(Application.persistentDataPath, UserRouteEmulation);
			if (Directory.Exists(path) == false)
				Directory.CreateDirectory(path);
			path = Path.Combine(path, DataName + domain);
			path = Path.ChangeExtension(path, "json");
			File.WriteAllText(path, json);
		}

		string LoadJsonBase(string domain)
		{
			string json = "";
			string path = Path.Combine(Application.persistentDataPath, UserRouteEmulation);
			path = Path.Combine(path, DataName + domain);
			path = Path.ChangeExtension(path, "json");
			if (File.Exists(path))
				json = File.ReadAllText(path);
			return json;
		}

	}
}
 