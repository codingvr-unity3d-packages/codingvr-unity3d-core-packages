﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Data
{
	public class NetworkPlayerStateActivtyInstanceCollectionDataStruct : NetworkBase<PlayerStateActivityInstanceCollectionStructSpec, PlayerStateActivityInstanceCollectionStructSpec>
	{
		override public string DataName { get { return "PlayerStateActivty"; } }
		
		[Serializable]
		public class PlayerStateCollection : Core.cvrSerializableDictionary<string, PlayerStateActivityInstanceCollectionStructSpec> { }
		[SerializeField]
		PlayerStateCollection data = new PlayerStateCollection();


		// on OnEnable we load the entire Json file
		void OnEnable()
		{
			Load();
		}

		// on OnDisbale we save the entire json file
		void OnDisable()
		{
			Save();
		}

		/*
		 *
		 * Load / Save files
		 * 
		 */
		 
		private void Load()
		{
			string json = LoadJsonResults();
			if (json.Length > 0)
			{
				data = JsonUtility.FromJson<PlayerStateCollection>(json);
			}
		}

		private void Save()
		{
			string json = JsonUtility.ToJson(data, true);
			if (json.Length > 0)
			{
				SaveJsonResults(json);
			}
		}

		/*
		 * 
		 * Data specification
		 * 
		 */

		override public PlayerStateActivityInstanceCollectionStructSpec GetDataSpec(string Id)
		{
			PlayerStateActivityInstanceCollectionStructSpec item = null;
			Load();
			if (data.ContainsKey(Id) == true)
			{
				item = data[Id];
			}
			return item;
		}

		override public void PostDataSpec(string Id, PlayerStateActivityInstanceCollectionStructSpec dataItem)
		{
			if (data.ContainsKey(Id) == false)
			{
				var json = dataItem.ToString();
				var newDataItem = PlayerStateActivityInstanceCollectionStructSpec.FromString(json);
				data.Add(Id, newDataItem);
				Save();
			}
		}

		/*
		 * 
		 * Data result
		 * 
		 */

		override public PlayerStateActivityInstanceCollectionStructSpec GetDataResult(string Id)
		{
			PlayerStateActivityInstanceCollectionStructSpec item = null;
			Load();
			if (data.ContainsKey(Id) == true)
			{
				item = data[Id];
			}
			return item;
		}

		override public void PutData(string Id, PlayerStateActivityInstanceCollectionStructSpec dataItem)
		{
			if (data.ContainsKey(Id) == true)
			{
				data[Id] = dataItem;
				Save();
			}
		}

		override public void PostData(string Id, PlayerStateActivityInstanceCollectionStructSpec dataItem)
		{
			if (data.ContainsKey(Id) == false)
			{
				var json = dataItem.ToString();
				var newDataItem = PlayerStateActivityInstanceCollectionStructSpec.FromString(json);
				data.Add(Id, newDataItem);
				Save();
			}
		}
	}
}