﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Scripting.APIUpdating;

namespace codingVR.Data
{
	public class NetworkInventoryStruct : NetworkBase<InventoryScriptable.InventoryStrucSpec, InventoryScriptable.InventoryStrucSpec>
	{
		override public string DataName { get { return "InventoryData"; } }
		
		[Serializable]
		public class NetworkInventoryDataCollection : Core.cvrSerializableDictionary<string, InventoryScriptable.InventoryStrucSpec> { }
		[SerializeField]
		NetworkInventoryDataCollection data = new NetworkInventoryDataCollection();


		// on OnEnable we load the entire Json file
		void OnEnable()
		{
			Load();
		}

		// on OnDisbale we save the entire json file
		void OnDisable()
		{
			Save();
		}
	
		/*
		 *
		 * Load / Save files
		 * 
		 */

		private void Load()
		{
			string json = LoadJsonResults();
			if (json.Length > 0)
			{
				data = JsonUtility.FromJson<NetworkInventoryDataCollection>(json);
			}
		}

		private void Save()
		{
			string json = JsonUtility.ToJson(data, true);
			if (json.Length > 0)
			{
				SaveJsonResults(json);
			}
		}

		/*
		 * 
		 * Data specification
		 * 
		 */

		override public InventoryScriptable.InventoryStrucSpec GetDataSpec(string Id)
		{
			InventoryScriptable.InventoryStrucSpec item = null;
			Load();
			if (data.ContainsKey(Id) == true)
			{
				item = data[Id];
			}
			return item;
		}

		override public void PostDataSpec(string Id, InventoryScriptable.InventoryStrucSpec dataItem)
		{
			if (data.ContainsKey(Id) == false)
			{
				var json = dataItem.ToString();
				var newDataItem = InventoryScriptable.InventoryStrucSpec.FromString(json);
				data.Add(Id, newDataItem);
				Save();
			}
		}

		/*
		 * 
		 * Data result
		 * 
		 */

		override public InventoryScriptable.InventoryStrucSpec GetDataResult(string Id)
		{
			InventoryScriptable.InventoryStrucSpec item = null;
			Load();
			if (data.ContainsKey(Id) == true)
			{
				item = data[Id];
			}
			return item;
		}

		override public void PutData(string Id, InventoryScriptable.InventoryStrucSpec dataItem)
		{
			if (data.ContainsKey(Id) == true)
			{
				data[Id] = dataItem;
				Save();
			}
		}

		override public void PostData(string Id, InventoryScriptable.InventoryStrucSpec dataItem)
		{
			if (data.ContainsKey(Id) == false)
			{
				var json = dataItem.ToString();
				var newDataItem = InventoryScriptable.InventoryStrucSpec.FromString(json);
				data.Add(Id, newDataItem);
				Save();
			}
		}
	}
}