﻿using System;
using UnityEngine;

namespace codingVR.Data
{
	public class NetworkGraphDataNode : NetworkBase<GetGraphDataNodeSpec, SetGraphDataNodeResult>
	{
		override public string DataName { get { return "Node"; } }

		/* 
		 * 
		 * GetGraphDataLinkSpec
		 * 
		 */

		[Serializable]
		public class GetGraphDataNodeSpecCollection : Core.cvrSerializableDictionary<string, GetGraphDataNodeSpec> { }
		[SerializeField]
		GetGraphDataNodeSpecCollection dataSpec = new GetGraphDataNodeSpecCollection();

		/* 
		 * 
		 * SetGraphDataLinkResult
		 * 
		 */

		[Serializable]
		public class SetGraphDataNodeResultCollection : Core.cvrSerializableDictionary<string, SetGraphDataNodeResult> { }
		[SerializeField]
		SetGraphDataNodeResultCollection dataResult = new SetGraphDataNodeResultCollection();
		
		// on OnEnable we load the entire Json file
		void OnEnable()
		{
			Load();
		}

		// on OnDisbale we save the entire json file
		void OnDisable()
		{
			Save();
		}

		/*
		 *
		 * Load / Save files
		 * 
		 */

		private void Load()
		{
			{
				string json = LoadJsonResults();
				if (json.Length > 0)
				{
					dataResult = JsonUtility.FromJson<SetGraphDataNodeResultCollection>(json);
				}
			}
			{
				string json = LoadJsonSpecs();
				if (json.Length > 0)
				{
					dataSpec = JsonUtility.FromJson<GetGraphDataNodeSpecCollection>(json);
				}
			}
		}

		private void Save()
		{
			{
				string json = JsonUtility.ToJson(dataResult, true);
				if (json.Length > 0)
				{
					SaveJsonResults(json);
				}
			}
			{
				string json = JsonUtility.ToJson(dataSpec, true);
				if (json.Length > 0)
				{
					SaveJsonSpecs(json);
				}
			}
		}

		/*
		 * 
		 * Data specification
		 * 
		 */

		override public GetGraphDataNodeSpec GetDataSpec(string Id)
		{
			GetGraphDataNodeSpec item = null;
			Load();
			if (dataSpec.ContainsKey(Id) == true)
			{
				item = dataSpec[Id];
			}
			return item;
		}

		override public void PostDataSpec(string Id, GetGraphDataNodeSpec dataItem)
		{
			if (dataSpec.ContainsKey(Id) == false)
			{
				var json = dataItem.ToString();
				var newDataItem = GetGraphDataNodeSpec.FromString(json);
				dataSpec.Add(Id, newDataItem);
				Save();
			}
		}
		
		/*
		 * 
		 * Result specification
		 * 
		 */

		override public SetGraphDataNodeResult GetDataResult(string Id)
		{
			SetGraphDataNodeResult item = null;
			Load();
			if (dataResult.ContainsKey(Id) == true)
			{
				item = dataResult[Id];
			}
			return item;
		}
		
		override public void PutData(string Id, SetGraphDataNodeResult dataItem)
		{
			if (dataResult.ContainsKey(Id) == true)
			{
				dataResult[Id] = dataItem;
				Save();
			}
		}

		override public void PostData(string Id, SetGraphDataNodeResult dataItem)
		{
			if (dataResult.ContainsKey(Id) == false)
			{
				var json = dataItem.ToString();
				var newDataItem = SetGraphDataNodeResult.FromString(json);
				dataResult.Add(Id, newDataItem);
				Save();
			}
		}
	}
}