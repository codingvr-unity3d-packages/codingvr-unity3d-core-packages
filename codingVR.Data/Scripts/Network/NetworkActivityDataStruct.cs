﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Data
{
	public class NetworkActivityDataStruct : NetworkBase<ActivityDataStructSpec, ActivityDataStructSpec>
	{
		override public string DataName { get { return "ActivityData"; } }
		
		[Serializable]
		public class ActivityDataCollection : Core.cvrSerializableDictionary<string, ActivityDataStructSpec> { }
		[SerializeField]
		ActivityDataCollection data = new ActivityDataCollection();


		// on OnEnable we load the entire Json file
		void OnEnable()
		{
			Load();
		}

		// on OnDisbale we save the entire json file
		void OnDisable()
		{
			Save();
		}
	
		/*
		 *
		 * Load / Save files
		 * 
		 */

		private void Load()
		{
			string json = LoadJsonResults();
			if (json.Length > 0)
			{
				data = JsonUtility.FromJson<ActivityDataCollection>(json);
			}
		}

		private void Save()
		{
			string json = JsonUtility.ToJson(data, true);
			if (json.Length > 0)
			{
				SaveJsonResults(json);
			}
		}

		/*
		 * 
		 * Data specification
		 * 
		 */

		override public ActivityDataStructSpec GetDataSpec(string Id)
		{
			ActivityDataStructSpec item = null;
			Load();
			if (data.ContainsKey(Id) == true)
			{
				item = data[Id];
			}
			return item;
		}

		override public void PostDataSpec(string Id, ActivityDataStructSpec dataItem)
		{
			if (data.ContainsKey(Id) == false)
			{
				var json = dataItem.ToString();
				var newDataItem = ActivityDataStructSpec.FromString(json);
				data.Add(Id, newDataItem);
				Save();
			}
		}
		
		/*
		 * 
		 * Data result
		 * 
		 */
		 
		override public ActivityDataStructSpec GetDataResult(string Id)
		{
			ActivityDataStructSpec item = null;
			if (data.ContainsKey(Id) == true)
			{
				item = data[Id];
			}
			return item;
		}

		override public void PutData(string Id, ActivityDataStructSpec dataItem)
		{
			if (data.ContainsKey(Id) == true)
			{
				data[Id] = dataItem;
				Save();
			}
		}

		override public void PostData(string Id, ActivityDataStructSpec dataItem)
		{
			if (data.ContainsKey(Id) == false)
			{
				var json = dataItem.ToString();
				var newDataItem = ActivityDataStructSpec.FromString(json);
				data.Add(Id, newDataItem);
				Save();
			}
		}
	}
}