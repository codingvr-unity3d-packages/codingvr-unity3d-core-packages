﻿using System;

namespace codingVR.Data
{
    // === ThemeState =================================================================================================

    #region ThemeState

    // === Event_ThemeStateChange ===========================================================================================

    public class Event_ThemeStateChange : Core.IGameEvent
    {
        public const int Type_Any = 0;
        public const int Type_CompletionRateRequest = 1;
        public const int Type_CompletionRate = 2;

        public int Type
        {
            get;
        }

        public object Data
        {
            get;
        }

        public Event_ThemeStateChange(int type, IThemeState themeState)
        {
            this.Type = type;
            this.Data = themeState;
        }
    };

    #endregion

    // === GameStateHistory ===========================================================================================

    #region GameStateHistory

    // === Event_GameStateHistoryUpdate

    public class Event_GameStateHistoryUpdate : Core.IGameEvent
    {
        public int Type
        {
            get { return 0; }
        }

        public object Data
        {
            set; get;
        }


        public Event_GameStateHistoryUpdate(Data.GameStateHistoryData data)
        {
            Data = data;
        }
    };

    // === Event_GameStateHistoryRemove

    public class Event_GameStateHistoryRemove : Core.IGameEvent
    {
        public int Type
        {
            get { return 0; }
        }

        public object Data
        {
            set; get;
        }


        public Event_GameStateHistoryRemove(string data)
        {
            Data = data;
        }
    };

    // === Event_GameStateHistoryReset

    public class Event_GameStateHistoryReset : Core.IGameEvent
    {
        public int Type
        {
            get { return 0; }
        }

        public object Data
        {
            set; get;
        }

        public Event_GameStateHistoryReset()
        {
        }
    }

    // === Event_GameStateHistoryReset

    public class Event_GameStateHistoryEnableUI : Core.IGameEvent
    {
        public int Type
        {
            get { return 0; }
        }

        public object Data
        {
            set; get;
        }

        public Event_GameStateHistoryEnableUI(bool bValue)
        {
            Data = bValue;
        }
    }

    #endregion

    // === PlayerStateActivities ===========================================================================================

    #region PlayerStateActivities

    public class Event_PlayerStateActivitiesJustStarting : Core.IGameEvent
    {
        public int Type
        {
            get { return 0; }
        }

        public object Data
        {
            set; get;
        }

        public Event_PlayerStateActivitiesJustStarting()
        {
        }
    };

    public class Event_PlayerStateActivitiesStarting : Core.IGameEvent
    {
        public int Type
        {
            get { return 0; }
        }

        public object Data
        {
            set; get;
        }

        public Event_PlayerStateActivitiesStarting()
        {
        }
    };

    public class Event_PlayerStateActivitiesStartedAndReady : Core.IGameEvent
    {
        public int Type
        {
            get { return 0; }
        }

        public object Data
        {
            set; get;
        }

        public Event_PlayerStateActivitiesStartedAndReady()
        {
        }
    };

    #endregion

    // === Event_PlayerStateActivity... ===========================================================================================

    #region PlayerStateActivity

    // === Event_PlayerStateActivityBase ===========================================================================================

    public class Event_PlayerStateActivityBase : Core.IGameEvent
    {
        [Flags]
        public enum EventType
        {
            none,

            // the activity base status
            toStop = 1 << 0,                            // 1
            toRegister = 1 << 1,                        // 2
            toCreate = 1 << 2,                          // 4
            toStart = 1 << 3,                           // 8
            toComplete = 1 << 4,                        // 16
            all = toStop | toRegister | toCreate | toStart | toComplete,
            progressing = 1 << 5,                       // 32
            forced = 1 << 6,                            // 64

            // the activity is changing
            stopping = toStop | progressing,            // 33 
            registering = toRegister | progressing,     // 34
            creating = toCreate | progressing,          // 36
            starting = toStart | progressing,           // 40
            completing = toComplete | progressing,      // 48

            // the activity is changed
            stopped = toStop,                           // 1
            registered = toRegister,                    // 2
            created = toCreate,                         // 4
            started = toStart,                          // 8
            completed = toComplete,                     // 16
        }

        public int Type { get; private set; }

        public bool IsProgressing => (Event & EventType.progressing) != 0;

        public bool IsForced => (Event & EventType.forced) != 0;

        // if return > 0 then the test has succeeded
        // if return == 1 then the test has succeeded with strict equality
        // if return > 1 then the test has succeeded with greater
        // if return == 0 then the test has failed
        static public int IsEqual(EventType ev, EventType evRef)
        {
            ev = ToEventBase(ev);
            evRef = ToEventBase(evRef);
            return ev == evRef ? 1 : 0;
        }

        // if return > 0 then the test has succeeded
        // if return == 1 then the test has succeeded with strict equality
        // if return > 1 then the test has succeeded with greater
        // if return == 0 then the test has failed
        static public int IsEqualOrGreater(EventType ev, EventType evRef)
        {
            ev = ToEventBase(ev);
            evRef = ToEventBase(evRef);
            int found = 0;
            int cpt = 0;
            while ((evRef & EventType.all) != 0 && found == 0)
            {
                cpt++;
                if (evRef == ev)
                    found = cpt;
                evRef = (EventType)((int)evRef << 1);

            }
            return found;
        }

        // if return > 0 then the test has succeeded
        // if return == 1 then the test has succeeded with strict equality
        // if return > 1 then the test has succeeded with greater
        // if return == 0 then the test has failed
        static public bool IsPrevious(EventType from, EventType to)
        {
            from = ToEventBase(from);
            to = ToEventBase(to);
            bool found = false;

            if (from + 1 == to)
            {
                found = true;
            }

            return found;
        }

        public EventType EventBase
        {
            get
            {
                return ToEventBase(Event);
            }
        }

        static public EventType ToEventBase(EventType derivedEvent)
        {
            return derivedEvent & (~(EventType.progressing | EventType.forced));
        }

        static private EventType ToEventChild(EventType baseEvent, EventType childModifier)
        {
            return baseEvent | childModifier;
        }

        static public EventType ToEventChildProgressing(EventType baseEvent)
        {
            return ToEventChild(baseEvent, EventType.progressing);
        }

        static public bool IsEventProgressing(EventType derivedEvent)
        {
            return (derivedEvent & EventType.progressing) == EventType.progressing;
        }

        static public EventType ToEventChildForced(EventType baseEvent)
        {
            return ToEventChild(baseEvent, EventType.forced);
        }

        static public bool IsEventForced(EventType derivedEvent)
        {
            return (derivedEvent & EventType.forced) == EventType.forced;
        }

        public EventType Event
        {
            get
            {
                return (EventType)Type;
            }
        }

        public object Data
        {
            set; get;
        }

        public Event_PlayerStateActivityBase(EventType eventType, string idActivityInstance, bool progressing, bool forced)
        {
            Data = idActivityInstance;
            Type = (int)eventType;
            Type |= (progressing ? (int)EventType.progressing : 0);
            Type |= (forced ? (int)EventType.forced : 0);
        }
        public Event_PlayerStateActivityBase()
        {
        }
    };

    // === Event_PlayerStateActivityCreate ===========================================================================================

    public class Event_PlayerStateActivityCreate : Event_PlayerStateActivityBase
    {
        public Event_PlayerStateActivityCreate(string idActivityInstance, bool progressing, bool forced) : base(EventType.toCreate, idActivityInstance, progressing, forced)
        {
        }
        public Event_PlayerStateActivityCreate()
        {
        }
    };

    // === Event_PlayerStateActivityStart ===========================================================================================

    public class Event_PlayerStateActivityStart : Event_PlayerStateActivityBase
    {
        public Event_PlayerStateActivityStart(string idActivityInstance, bool progressing, bool forced) : base(EventType.toStart, idActivityInstance, progressing, forced)
        {
        }
        public Event_PlayerStateActivityStart()
        {
        }
    };

    // === Event_PlayerStateActivityComplete ===========================================================================================

    public class Event_PlayerStateActivityComplete : Event_PlayerStateActivityBase
    {
        public Event_PlayerStateActivityComplete(string idActivityInstance, bool progressing, bool forced) : base(EventType.toComplete, idActivityInstance, progressing, forced)
        {
        }
        public Event_PlayerStateActivityComplete()
        {
        }
    };

    // === Event_PlayerStateActivityRegister ===========================================================================================

    public class Event_PlayerStateActivityRegister : Event_PlayerStateActivityBase
    {
        public Event_PlayerStateActivityRegister(string idActivityInstance, bool progressing, bool forced) : base(EventType.toRegister, idActivityInstance, progressing, forced)
        {
        }
        public Event_PlayerStateActivityRegister()
        {
        }
    };

    // === Event_PlayerStateActivityStop ===========================================================================================

    public class Event_PlayerStateActivityStop : Event_PlayerStateActivityBase
    {
        public Event_PlayerStateActivityStop(string idActivityInstance, bool progressing, bool forced) : base(EventType.toStop, idActivityInstance, progressing, forced)
        {
        }
        public Event_PlayerStateActivityStop()
        {
        }
    };

    // === Event_PlayerStateActivityDataChange ===========================================================================================

    public class Event_PlayerStateActivityDataChange : Core.IGameEvent
    {
        public int Type
        {
            get { return 0; }
        }

        public object Data
        {
            set; get;
        }

        // tuple (int groupId, activityData data)
        public Event_PlayerStateActivityDataChange((UnityEngine.Object, IActivityFieldServer) data)
        {
            Data = data;
        }
    };

    // === Event_PlayerStateActivityInstanceChange ===========================================================================================

    public class Event_PlayerStateActivityInstanceChange : Core.IGameEvent
    {
        public int Type
        {
            get { return 0; }
        }

        public object Data
        {
            set; get;
        }

        // tuple (string idActivity, activityInstance data)
        public Event_PlayerStateActivityInstanceChange((string, IActivityFieldServer) data)
        {
            Data = data;
        }
    };

    #endregion

    // === Event_PlayerStateActivity... ===========================================================================================

    #region Event_PlayerStateActiviyPlannerRequestActivity

    // === Event_PlayerStateEventAndActiviyPlannerRequestActivity ===========================================================================================

    public class Event_PlayerStateActiviyPlannerRequestActivity : Core.IGameEvent
    {
        public enum RequestType
        {
            stop = 1 << 0,
            register = 1 << 1,
            create = 1 << 2,
            start = 1 << 3,
        }

        public int Type
        {
            set; get;
        }

        public object Data
        {
            set; get;
        }

        static public Event_PlayerStateActivityBase.EventType Convert(RequestType type)
        {
            Event_PlayerStateActivityBase.EventType event_ = Event_PlayerStateActivityBase.EventType.none;
            switch (type)
            {
                case RequestType.stop:
                    event_ = Event_PlayerStateActivityBase.EventType.stopped;
                    break;
                case RequestType.register:
                    event_ = Event_PlayerStateActivityBase.EventType.registered;
                    break;
                case RequestType.create:
                    event_ = Event_PlayerStateActivityBase.EventType.created;
                    break;
                case RequestType.start:
                    event_ = Event_PlayerStateActivityBase.EventType.started;
                    break;
            }
            return event_;
        }

        // tuple (int groupId, activityData data)
        public Event_PlayerStateActiviyPlannerRequestActivity(string idActivityInstance, RequestType type)
        {
            Data = idActivityInstance;
            Type = (int)type;
        }
    };

    // === Event_PlayerStateEventAndActiviyPlannerRequestActivity ===========================================================================================

    public class Event_PlayerStateEventAndActiviyPlannerRequestActivityRewards : Core.IGameEvent
    {
        public int Type
        {
            set; get;
        }

        public object Data
        {
            set; get;
        }

        // tuple (int groupId, activityData data)
        public Event_PlayerStateEventAndActiviyPlannerRequestActivityRewards(string idActivityInstance)
        {
            Data = idActivityInstance;
        }
    };

    // === Event_PlayerStateEventAndActiviyPlannerRequestActivity ===========================================================================================

    public class Event_PlayerStateEventAndActiviyPlannerRequestActivityNotification : Core.IGameEvent
    {
        public int Type
        {
            set; get;
        }

        public object Data
        {
            set; get;
        }

        public enum RequestType
        {
            activityNotificationStarted = 1 << 0,
            activityNotificationRewarded = 1 << 1,
        }

        // tuple (int groupId, activityData data)
        public Event_PlayerStateEventAndActiviyPlannerRequestActivityNotification(string idActivityInstance, RequestType requestType)
        {
            Data = idActivityInstance;
            Type = (int)requestType;
        }
    };

    #endregion

    // === InputEvents ===========================================================================================

    #region InputEvents

    public class Event_OnClickMouse : Core.IGameEvent
    {
        public int Type
        {
            get { return 0; }
        }

        public object Data
        {
            set; get;
        }
    }

    #endregion

    // === GameEvents ===========================================================================================

    #region GameEvents

    //Issue #577  Event to tell the player has clicked on the button "Exit Game"
    public class Event_PlayerExitingGame : Core.IGameEvent
    {
        public int Type
        {
            get { return 0; }
        }

        public object Data
        {
            set; get;
        }
    }

    #endregion

    // === InventoryEvents ===========================================================================================

    #region InventoryEvents

    public class Event_InventoryChange : Core.IGameEvent
    {
        public enum EventType
        {
            addMoney,
            removeMoney,
            addItem,
            removeItem,
            equipItem,
            tryEquipItem,
            revertEquipItem,
            changeMaterialBodyItem
        }

        public int Type
        {
            get; set;
        }

        public object Data
        {
            set; get;
        }

        // tuple (int groupId, activityData data)
        public Event_InventoryChange(int token, EventType eventType)
        {
            Data = token;
            Type = (int)eventType;
        }
    }

    #endregion
}
