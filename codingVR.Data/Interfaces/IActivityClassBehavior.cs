﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Data
{
	public interface IActivityClassBehavior
	{
		void Setup(IActivityInstance activityInstance);
	}
}