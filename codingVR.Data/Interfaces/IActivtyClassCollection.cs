﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Data
{
	public interface IActivtyClassCollection
	{
		ICollection<string> GetActivityClassKeys();
		IActivityClass FindActivityClass(string idClass);
		void AddActivityClass(string className, string description, ActivityClassImportance importance, ActivityClassRegularity regularity, ActivityClassSize size, int duration, int delay, int points, GameObject activityClassBehavior);
		IEnumerator<KeyValuePair<string, ActivityClass>> GetEnumerator();
	}
}
