﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace codingVR.Data
{
	// GraphDataNodeBase 

	[System.Serializable]
	public class IGraphDataBase
	{
		// Id structure

		[SerializeField]
		string id;
		public string Id { get => id; protected set => id = value; }
		
		// Id Player

		static string idPlayer;
		static public string IdPlayer { get => idPlayer; set => idPlayer = value; }

		// make & parse Ids

		static public string MakeId(string idPlayer, string idActivity)
		{
			return idPlayer + "." + idActivity;
		}

		static public (string, string) ParseId(string id)
		{
			string[] ids = id.Split('.');
			if (ids.Length == 2)
			{
				return (ids[0], ids[1]);
			}
			return (null, null);
		}
	}

	public abstract class IGraphDataBaseScriptable<DataScriptable, DataStruct> : ScriptableObject where DataScriptable : IGraphDataBaseScriptable<DataScriptable, DataStruct>
	{
		static private DataScriptable selected;

		static public DataScriptable Selected
		{
			get
			{
				return selected;
			}

			set
			{
				if (selected != value)
				{
					selected = value;
				}
			}
		}

		public abstract DataStruct Data { get; set; }
		public abstract string Title { get; set; }
		public abstract string Answer { get; set; }
		public abstract string Tag { get; }
		public abstract ReadOnlyCollection<string> Options { get; }
		public abstract void AddOptionsRange(string[] range);
	}
}
