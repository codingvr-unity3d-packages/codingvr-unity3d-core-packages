﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using Zenject;

namespace codingVR.Data
{
	public interface IActivityDataBehavior
	{
		void Setup(string activityInstanceId, Data.IActivityFieldServer activityData, GameObject activityDataGroupId);
	}
}
