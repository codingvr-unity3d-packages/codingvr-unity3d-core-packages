﻿using FSM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace codingVR.Data
{
	/*
	 * 
	 * Life cycle
	 * 
	 */

	// none =========> regsitered =========> created ========> started ========> completed

	public enum ActivityInstanceLifeCycleStatus
	{
		// the activty has been stopped
		isActivityStopped = 0,
		// the activty has been registered
		isActivityRegistered = 1,
		// the activty is created; but not started
		// according to the duration if negative
		isActivityCreated = 2,
		// the activity is started
		isActivityStarted = 3,
		// the activity is completed but not stopped
		isActivityCompleted = 4,
	}

	/*
	 * 
	 * Player status
	 * 
	 */
	[Flags]
	public enum ActivityInstancePlayerStatusFlags
	{
		none = 0,

		// the player is wating inside the activity
		//		1. either the player is inside the activty but does not nothing
		//		2. either the player follows a quest but does not nothing
		isPlayerPending = 1 << 0,
		// the player is playing to the activity
		//		1. either the player is inside the activty
		//		2. either the player follows a quest
		isPlayerPlaying = 1 << 1,
		// the player requests helps or tips from companion AI
		isPlayerRequestCompanion = 1 << 2,
	}

	/*
	 * 
	 * Sub status
	 * 
	 */

	[Flags]
	public enum ActivityInstanceSubStatusFlags
	{
		none = 0,

		// geo localisation is running
		geoLocalisationIsRunning = 1 << 0,
		// activity rewarded
		activityRewarded = 1 << 1,
		// activity notified
		activityStartingNotified = 1 << 2,
		activityRewardsNotified = 1 << 3,
	}

	/*
	 * 
	 * Activity instance
	 * 
	 */

	public interface IActivityInstance : IActivityFieldServer
	{
		/*
		 * 
		 * Service management
		 * 
		 */

		StateMachine Setup(MonoBehaviour owner, Core.IEventManager eventManager, Data.IActivtyClassCollection activityClassCollection);
		// start
		void StartService();
		// reset
		void ResetToDefault();
		// reset game play values only
		void ResetGameplay();
		// copy from
		void CopyDataFrom(IActivityInstance activityInstance);
		void UpdateDataFrom(IActivityInstance activityInstanceInterface);

		/*
		 * 
		 * Data
		 * 
		 */

		// activity instance ID
		string IdInstance { get; }

		// activity instance ID Parent
		string IdInstanceParent { get; }

		// activity class ID
		string IdClass { get; }
		IActivityClass Class { get; }

		// activity selected
		bool IsSelected { get; set; }

		// active time in runtime
		int RunTime { get; }

		// local create time
		int LocalCreateTime { get; }
		// a LocalActivityTime < 0; is the delay before starting (created & not started)
		// a LocalActivityTime >= 0 && a LocalActivityTime < ActivityClass.duration; a activity is alive (created & started)
		// a LocalActivityTime >= ActivityClass.duration; a activity is completed
		int LocalActivityTime { get; }
		// local completion time
		int LocalCompletionTime { get; }

		// completion
		float Completion { get; set; }
		// status & sub status
		ActivityInstanceLifeCycleStatus StatusFlags { get; }
		ActivityInstanceSubStatusFlags SubStatusFlags { get; set;  }
		ActivityInstancePlayerStatusFlags PlayerStatusFlags { get; }

		// Count past by different status
		int CountCompleted { get; }
		int CountStarted { get; }

		// Passage
		int InstancePassage { get; }

		// list gifts items
		List<ItemScriptable> ItemsGift { get; }
		// last gift rewarded
		ItemScriptable LastItemGiftRewared{ get; set; }
		// last money credited
		int LastMoneyCredited { get; set; }


		// properties
		string NameInstance { get; }
		string DescriptionInstance { get; }

		/*
		 * 
		 * State management
		 * 
		 */

		// bForce=true is used to indicate that the 
		// requirement is forced by a manual action from the user
		bool Register(bool bForce);
		bool Create(bool bForce);
		bool Start(bool bForce);
		bool Complete(bool bForce);
		bool Stop(bool bForce);
		void ResetActivityTime();
	}
}
