﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.Data
{
	/*
	 * 
	 * IPersistentRuntimeObjectsInstance
	 * 
	 */

	public interface IPersistentRuntimeObjectsInstance
	{
		// identification
		string Name { get; }
		// instantiation
		IEnumerator InstantiateGameObject(DiContainer diContainer, Scene.IResourcesLoader resourcesLoader, Transform parent, Vector3 positionOffset);
		// serialization
		void Serialize(Transform transform);
	}
}
