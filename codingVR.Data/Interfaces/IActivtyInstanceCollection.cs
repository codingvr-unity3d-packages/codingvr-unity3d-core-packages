﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Data
{
	public interface IActivtyInstanceCollection
	{
		IActivityInstance FindActivityInstance(string idAcivity);
		bool BuildActivity(string idActivityInstance, string idActivityInstanceParent, string idClass);
		bool RegisterActivity(string idActivityInstance, bool bForce);
		bool CreateActivity(string idActivityInstance, bool bForce);
		bool StartActivity(string idActivityInstance, bool bForce);
		bool CompleteActivity(string idActivityInstance, bool bForce);
		bool StopActivity(string idActivityInstance, bool bForce);
		void ResetActivityGameplay(string idActivityInstance);
		void ResetToDefault();
		IEnumerator<KeyValuePair<string, ActivityInstance>> GetEnumerator();
		ICollection<string> GetActivityInstanceKeys();
	}
}
