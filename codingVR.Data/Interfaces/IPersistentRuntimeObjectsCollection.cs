﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.Data
{
	public interface IPersistentRuntimeObjectsCollection
	{
		void InstantiateGameObjects(DiContainer diContainer, Scene.IResourcesLoader resourcesLoader, MonoBehaviour mono, Transform parent, Vector3 positionOffset);
		void SerializeGameObjects(Transform parent);
		void Clear();
		bool IsDirty { get; }
		bool PreventDirtyFlag { get; }
	}
}
