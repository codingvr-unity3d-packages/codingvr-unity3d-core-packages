﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace codingVR.Data
{
	public interface IPersistentRuntimeObject
	{
		AssetReference AssetReference{ get;}
	}
}
