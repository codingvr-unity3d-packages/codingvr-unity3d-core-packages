﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Data
{
	public interface IActivityPersistentDataManager
	{
		void Load();
		void Save();
	}
}