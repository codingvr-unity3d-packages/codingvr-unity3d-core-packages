﻿using System.Collections.Generic;

namespace codingVR.Data
{
	public interface IThemeState
	{
		// Tags
		Tag LastTag { get; }

		// Requests
		float CompletionRateRequest { get; set; }
		float CompletionRate { get; }
		void ValidateAllRequests();

		// History
		ref List<GameStateHistoryData> GameHistoryDataList { get; }
	}

}
