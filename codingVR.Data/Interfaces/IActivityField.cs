﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace codingVR.Data
{
	/*
	 * 
	 * Activity field
	 * 
	 */

	public delegate object GetFieldValue(object instance);

	public struct Field
	{
		public FieldInfo fieldInfo;
		public GetFieldValue getFieldValue;
		
		public Field(FieldInfo fieldInfo, GetFieldValue getFieldValue)
		{
			this.fieldInfo = fieldInfo;
			this.getFieldValue = getFieldValue;
		}
	}

	public enum FieldSearchMethod
	{
		defaultMethod,
		any,
		fullPath,
		localPath
	}

	public enum FieldComparison
	{
		Equal,
		NotEqual,
		EqualNear,
		NotEqualNear,
		Less,
		LessOrEqual,
		Greater,
		GreaterOrEqual
	}

	public interface IActivityField
	{
		bool SetValue(string name, object value, FieldSearchMethod method = FieldSearchMethod.defaultMethod);
		bool AddValue(string name, object value, FieldSearchMethod method = FieldSearchMethod.defaultMethod);
		bool SubValue(string name, object value, FieldSearchMethod method = FieldSearchMethod.defaultMethod);
		bool DivValue(string name, object value, FieldSearchMethod method = FieldSearchMethod.defaultMethod);
		bool MulValue(string name, object value, FieldSearchMethod method = FieldSearchMethod.defaultMethod);
		bool ResetValue(string name, FieldSearchMethod method = FieldSearchMethod.defaultMethod);
		object GetValue(string name, FieldSearchMethod method = FieldSearchMethod.defaultMethod);
		(bool, bool) CompareValue(string name, object value, FieldComparison comparison, FieldSearchMethod method = FieldSearchMethod.defaultMethod);
	}

	public interface IActivityNestedField
	{
		string ParseNestedFields(string nestedFields);
	}

	public interface IActivityFieldServer : IActivityField, IEnumerable
	{
		string GetFullPathField(string name);
		bool IsDirty { get; set; }
		Dictionary<string, FieldInfo> Fields { get; }
		string Id { get; set; }
		void PopulateFields();
	}

	public abstract class ActivityFieldScriptable : ScriptableObject, IActivityField
	{
		public abstract bool SetValue(string name, object value, FieldSearchMethod method);
		public abstract bool AddValue(string name, object value, FieldSearchMethod method);
		public abstract (bool, bool) CompareValue(string name, object value, FieldComparison comparison, FieldSearchMethod method);
		public abstract bool DivValue(string name, object value, FieldSearchMethod method);
		public abstract object GetValue(string name, FieldSearchMethod method);
		public abstract bool MulValue(string name, object value, FieldSearchMethod method);
		public abstract bool ResetValue(string name, FieldSearchMethod method);
		public abstract bool SubValue(string name, object value, FieldSearchMethod method);
	}
}
