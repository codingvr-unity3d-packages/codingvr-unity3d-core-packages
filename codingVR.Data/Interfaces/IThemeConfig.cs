﻿using System.Collections.Generic;
using System;

namespace codingVR.Data
{
	// Theme Accreditation
	public enum ThemeRelationShip
	{
		child,
		parent,
	};

    [Flags]
    public enum ThemeUI
    {
        none = 0,
        completionBar = 1 << 0,
        menu = 1 << 2,
        debug = 1 << 3,
        money = 1 << 4
    }

    public interface IThemeConfig
	{
		ThemeRelationShip ThemeRelationShip { get; }
        ThemeUI ThemeUI { get; }
		bool HasIsland { get; }
	}
}
