﻿using System.Collections;

namespace codingVR.Data
{
	public interface INetworkClient<DataScriptable>
	{
		// data spec
		IEnumerator GetDataSpec(Data.DoneGet<DataScriptable> doneSet = null);
		
		// data result
		IEnumerator PutData();
		IEnumerator PostData();
		IEnumerator GetData(bool forceCreation, Data.DoneGet<DataScriptable> doneSet = null);
	}
	
	public delegate void DoneGet<DataSpec>(DataSpec data);
	public delegate void DoneSet<DataResult>(DataResult data);

	public interface INetworkSession<DataSpec, DataResult>
	{
		string UserRoute { get; set; }

		/*
		 * 
		 * Specification
		 * 
		 */
		bool GetSpec(string id, DoneGet<DataSpec> done);
		bool PostSpec(DataSpec data, DoneSet<DataSpec> done);

		/*
		 * 
		 * Result
		 * 
		 */

		bool GetResult(string id, DoneGet<DataResult> done);
		bool Post(DataResult data, DoneSet<DataResult> done);
		bool Put(DataResult data, DoneSet<DataResult> done);

		/*
		 * 
		 * Administration
		 *
		 */

		string LastCommand { get; }
		void AbortRequest();
		bool EnabledDebug { get; set; }
	}
}
