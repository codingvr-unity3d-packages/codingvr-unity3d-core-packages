﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Data
{
    public interface IGameSessionDataLink : GameEngine.IGameSession<QuestionDataScriptable>
    {
    }

    public interface IGameSessionDataNode : GameEngine.IGameSession<GraphDataNodeScriptable>
    {
    }
}
