﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Data
{
	/*
	 * 
	 * Activity class property flags
	 * 
	 */

	[Flags]
	public enum ActivityClassPropertyFlags
	{
		none = 0,
		// a path, an icon to indicate the location of activy
		geoLocalisationSupport = 1 << 1,
		// the activity is localised by a Tag 
		geoLocalisedByTag = 1 << 2,
		// the activity alive in background mode even if an activity in exclusiveLiveModeSupport is running
		backgoundLiveModeSupport = 1 << 3,
		// the activity is exclusive; all others exclusive activity are disabled
		exclusiveLiveModeSupport = 1 << 4,
		// not visible from Game UI
		hiddenFromUI = 1 << 5,
		// the activity will be created automatically
		automaticCreation = 1 << 6,
		// the activity will be started automatically
		automaticStarting = 1 << 7,
	}

	/*
	 * 
	 * Activity class importance
	 * 
	 */

	public enum ActivityClassImportance
	{
		optional,
		recommended,
		mandatory,
		mission,
	}

	/*
	 * 
	 * Activity class Regularity
	 * 
	 */

	public enum ActivityClassRegularity
	{
		Regular,
		Event,
		Unique
	}
	
	public enum ActivityClassSize
	{
		Short,
		Medium,
		Long
	}

	/*
	 * 
	 * Activity class
	 * 
	 */

	public interface IActivityClass : IEnumerable
	{
		// identification
		string ClassName { get; }
		// description
		string Description { get; }
		// importance
		ActivityClassImportance Importance { get; }
		//Regularity
		ActivityClassRegularity Regularity { get; }
		//Size
		ActivityClassSize Size { get; }
		// properties
		ActivityClassPropertyFlags PropertyFlags { get; }
		// duration of activity
		// 0 : for infinite duration time
		int Duration { get; }
		// delay time
		int Delay { get; }
		// point count provided by this kind of activity
		int Points { get; }
		// prefab 
		GameObject ActivityClassBehavior { get; }
		// Passage
		int ClassPassage { get; set; }
	}
}
