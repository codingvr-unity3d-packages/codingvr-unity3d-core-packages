﻿using UnityEngine;
using UnityEditor;
using Proyecto26;
using UnityEngine.Networking;
using TMPro;
using System.Runtime.Serialization;

namespace codingVR.Data
{
	public class MainScript : MonoBehaviour
	{
		[SerializeField]
		GraphDataNodeScriptable dataNodeToPost;

		[SerializeField]
		QuestionDataScriptable dataLinkToPost;

		[SerializeField]
		PlayerStateDataScriptable dataPlayerStateToPost;
		
		[SerializeField]
		TMP_InputField Id;

		[SerializeField]
		TMP_InputField Data;

		[SerializeField]
		TMP_InputField LastCmd;

		[SerializeField]
		TMP_Text LastMsg;

		[SerializeField]
		TMP_Text Version;

		[SerializeField]
		GameObject NodePanel;
		
		[SerializeField]
		GameObject LinkPanel;

		[SerializeField]
		GameObject PlayerStatePanel;

		[SerializeField]
		NetworkGraphDataNode networkGraphDataNode;

		[SerializeField]
		NetworkGraphDataLink networkGraphDataLink;

		[SerializeField]
		NetworkPlayerStateDataStruct networkPlayerStateDataStruct;


		// https://jsonlint.com/
		// https://my-json-server.typicode.com/shadd3/potodatamodel/db
		// https://github.com/shadd3/potodatamodel

		private void LogMessage(string title, string message)
		{
			LastMsg.text = title + "\n" + message;
			Debug.Log(LastMsg.text);
		}

		private void Awake()
		{
			Id.text = PlayerPrefs.GetString("Id", "welcome");
			Version.text = Application.version;
		}

		private void OnApplicationQuit()
		{
			PlayerPrefs.SetString("Id", Id.text);
		}
		
		public void OnSelectDataType(int type)
		{
			switch(type)
			{
				case 0:
					NodePanel.SetActive(true);
					LinkPanel.SetActive(false);
					PlayerStatePanel.SetActive(false);
					break;
				case 1:
					NodePanel.SetActive(false);
					LinkPanel.SetActive(true);
					PlayerStatePanel.SetActive(false);
					break;
				case 2:
					NodePanel.SetActive(false);
					LinkPanel.SetActive(false);
					PlayerStatePanel.SetActive(true);
					break;
			}
		}

		// GetSpecNodes
		public void GetDataNodeSpec()
		{
			void done(GetGraphDataNodeSpec data)
			{
				data.Make(dataNodeToPost);
				LogMessage("GET", JsonUtility.ToJson(data, true));
			}
			var cmd = networkGraphDataNode.GetSpec(Id.text, done);
			LastCmd.text = networkGraphDataNode.LastCommand;
		}

		/*
		 * 
		 * GET
		 * 
		 */

		// GetSpecLinks
		public void GetDataLinkSpec()
		{
			void done(GetGraphDataLinkSpec data)
			{
				data.Make(dataLinkToPost);
				LogMessage("GET", JsonUtility.ToJson(data, true));
			}
			var cmd = networkGraphDataLink.GetSpec(Id.text, done);
			LastCmd.text = networkGraphDataLink.LastCommand;
		}

		// GetResultLinks
		public void GetDataLinkResult()
		{
			void done(SetGraphDataLinkResult data)
			{
				data.Make(dataLinkToPost);
				LogMessage("GET", JsonUtility.ToJson(data, true));
			}
			var cmd = networkGraphDataLink.GetResult(Id.text, done);
			LastCmd.text = networkGraphDataLink.LastCommand;
		}


		public void GetDataPlayerStateResult()
		{
			void done(PlayerStateDataStructSpec data)
			{
				data.Make(dataPlayerStateToPost);
				LogMessage("GET", JsonUtility.ToJson(data, true));
			}
			var cmd = networkPlayerStateDataStruct.GetSpec(Id.text, done);
			LastCmd.text = networkPlayerStateDataStruct.LastCommand;
		}


		/*
		 *
		 * POST
		 * 
		 */

		// SetResultNodes
		public void PostDataNodeResult()
		{
			void done(SetGraphDataNodeResult data)
			{
				LogMessage("POST", JsonUtility.ToJson(data, true));

			}
			dataNodeToPost.Answer = Data.text;
			var cmd = networkGraphDataNode.Post(SetGraphDataNodeResult.Create(dataNodeToPost), done);
			LastCmd.text = networkGraphDataNode.LastCommand;
		}

		public void PostDataPlayerStateSrtuct()
		{
			void done(PlayerStateDataStructSpec data)
			{
				LogMessage("PUT", JsonUtility.ToJson(data, true));
			}

			var set = PlayerStateDataStructSpec.Create(dataPlayerStateToPost);
			//var json = JsonUtility.ToJson(set, true);

			var cmd = networkPlayerStateDataStruct.Post(set, done);
			LastCmd.text = networkPlayerStateDataStruct.LastCommand;
		}

		/*
		 *
		 * PUT
		 * 
		 */
		 
		// SetResultNodes
		public void PutDataNodeSpec()
		{
			void done(SetGraphDataNodeResult data)
			{
				LogMessage("PUT", JsonUtility.ToJson(data, true));
			}
			dataNodeToPost.Answer = Data.text;
			var cmd = networkGraphDataNode.Put(SetGraphDataNodeResult.Create(dataNodeToPost), done);
			LastCmd.text = networkGraphDataNode.LastCommand;
		}

		public void PutDataPlayerStateSrtuct()
		{
			void done(PlayerStateDataStructSpec data)
			{
				LogMessage("PUT", JsonUtility.ToJson(data, true));
			}
			var set = PlayerStateDataStructSpec.Create(dataPlayerStateToPost);
//			var json = JsonUtility.ToJson(set, true);
			var cmd = networkPlayerStateDataStruct.Put(set, done);
			LastCmd.text = networkPlayerStateDataStruct.LastCommand;
		}
		
		/*
		 *
		 * ABORT
		 * 
		 */

		public void AbortRequest()
		{
			networkGraphDataNode.AbortRequest();
		}
	}
}
 