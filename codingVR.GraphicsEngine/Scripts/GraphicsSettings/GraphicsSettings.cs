﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

#if (UNITY_EDITOR)
using UnityEditor.Callbacks;
#endif

namespace codingVR.GraphicsEngine
{
	using UnityEditor;
	using UnityEngine.SceneManagement;

	public class GraphicsSettings : MonoBehaviour, IGraphicsSetting
	{
		// === DATA-UI ===

		#region DATA-UI

		[SerializeField]
		bool postProcessEnabled = true;

		[SerializeField]
		GameObject sceneTransition;

		//Use to determine if the graphicsSettings must check the graphics in the update
		[SerializeField]
		bool checkGraphicsActivated = false;

		#endregion

		// === DATA-INTERNAL =======================================================

		#region DATA-INTERNAL

		float deltaTime = 0.0f;
		bool checkGraphics = false;

		#endregion

		// === Lifecycle ===

		#region Lifecycle 

		// Start is called before the first frame update
		void Awake()
		{
#if (UNITY_STANDALONE)
			bool postProcessStandaloneTurnedOn = true;
#elif (UNITY_ANDROID)
			bool postProcessStandaloneTurnedOn = false;
#elif (UNITY_WEBGL)
			bool postProcessStandaloneTurnedOn = false;
#elif (UNITY_WSA)
			bool postProcessStandaloneTurnedOn = false;
#endif
			// setup settings
			SetupSettings(postProcessStandaloneTurnedOn);
				
			// enable unity post processing (not suitable for Mobile platfrom)
			PostProcessVolume postProcessVolumeStandalone = GetComponentInChildren<PostProcessVolume>(true);
			if (postProcessVolumeStandalone != null)
			{
				postProcessVolumeStandalone.gameObject.SetActive(postProcessStandaloneTurnedOn);
			}

			// deserialize
			Deserialize();
		}

		void Serialize()
		{
			// player ref extension
			void PlayerPrefs_SetBool(string key, bool v)
			{
				PlayerPrefs.SetInt(key, v ? 1 : 0);
			}

			// quality
			PlayerPrefs.SetInt("quality", QualityLevel);

			// post process
			PlayerPrefs.SetInt("postProcessEnabled", postProcessEnabled ? 1 : 0);

			// resolution
			PlayerPrefs.SetInt("resolutionWidth", ScreenResolutionWith);
			PlayerPrefs.SetInt("resolutionHeight", ScreenResolutionHeight);
			PlayerPrefs_SetBool("fullScreen", ScreenFullScreen);
		}

		void Deserialize()
		{
			// player ref extension
			bool PlayerPrefs_GetBool(string key, bool v)
			{
				return PlayerPrefs.GetInt(key, v ? 1 : 0) == 1 ? true : false;
			}

			// quality
			QualityLevel = PlayerPrefs.GetInt("quality", QualityLevel);

			// post process
			postProcessEnabled = PlayerPrefs_GetBool("postProcessEnabled", postProcessEnabled);
			SetPostPorcessing(postProcessEnabled);

			// resolution
			int width = PlayerPrefs.GetInt("resolutionWidth", ScreenResolutionWith);
			int height = PlayerPrefs.GetInt("resolutionHeight", ScreenResolutionHeight);
			bool fullScreen = PlayerPrefs_GetBool("fullScreen", ScreenFullScreen);
			ScreenResolution(width, height, fullScreen);
		}

		// Update is called once per frame
		void Update()
		{
			deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;

			if (checkGraphics == false && sceneTransition.activeInHierarchy == false && checkGraphicsActivated == true)
			{
				checkGraphics = true;
				StartCoroutine(CheckAverageFPS());

			}

			else if (checkGraphics == true && sceneTransition.activeInHierarchy == true && checkGraphicsActivated == true)
			{
				checkGraphics = false;
			}

			//Utilisé pour les tests de FPS
			/*
			if (Input.GetKeyUp(KeyCode.Space))
			{
				StartCoroutine(CheckAverageFPS());
			}
			*/
		}

		IEnumerator CheckAverageFPS()
		{
			float FPSTotal = 0;
			int averageTimer = 5;

			Debug.Log("CheckAverage FPS");

			for (int i = 0;i <= averageTimer; i++)
			{
				yield return new WaitForSeconds(1.0f);
				FPSTotal = FPSTotal + (1.0f / deltaTime);
				//Debug.Log("FPSTotal : " + FPSTotal);
				
			}

			float averageFPS = FPSTotal / averageTimer;

			if(averageFPS < 30)
			{

				Debug.Log("AverageFPS under 30 : " + averageFPS);
				//Debug.Log("Quality graphics was : " + QualityLevel);
				if (QualityLevel > 0)
				{
					QualityLevel = QualityLevel - 1;
					
					if (QualityLevel > 0)
					{
						StartCoroutine(CheckAverageFPS());
					}
				}

				//Debug.Log("Quality graphics are now : " + QualityLevel);

			}

			else
			{
				//Debug.Log("Average FPS better than 30 : " + averageFPS);
			}

			yield return 0;
		}

		private void OnEnable()
		{
			SceneManager.sceneLoaded += SetupScenePostProcess;
		}

		#endregion

		// === implementation ===

		#region implementation

		/// Use this method to get all loaded objects of some type, including inactive objects.
		/// This is an alternative to Resources.FindObjectsOfTypeAll (returns project assets, including prefabs), and GameObject.FindObjectsOfTypeAll (deprecated).
		public static List<Type> FindObjectsOfTypeAll<Type>(bool _findInactive = false)
		{
			var results = new List<Type>();
			for (int i = 0; i < SceneManager.sceneCount; i++)
			{
				var s = SceneManager.GetSceneAt(i);
				if (s.isLoaded)
				{
					var allGameObjects = s.GetRootGameObjects();
					for (int j = 0; j < allGameObjects.Length; j++)
					{
						var go = allGameObjects[j];
						var o = go.GetComponentsInChildren<Type>(_findInactive);
						results.AddRange(o);
					}
				}
			}
			return results;
		}

		// called second
		void SetupScenePostProcess(Scene scene, LoadSceneMode mode)
		{

#if (UNITY_STANDALONE)
			bool postProcessStandaloneTurnedOn = true;
			bool postProcessMobileTurnedOn = false;
#elif (UNITY_ANDROID)
			bool postProcessStandaloneTurnedOn = false;
			bool postProcessMobileTurnedOn = true;
#elif (UNITY_WEBGL)
			bool postProcessStandaloneTurnedOn = false;
			bool postProcessMobileTurnedOn = true;
#elif (UNITY_WSA)
			bool postProcessStandaloneTurnedOn = false;
			bool postProcessMobileTurnedOn = true;
#endif

			if (mode == LoadSceneMode.Additive)
			{
				{
					var roots = scene.GetRootGameObjects();
					foreach (var rootNode in roots)
					{
						if (rootNode != null)
						{
							// Post processing suitable for standalone
							var postProcessLayerStandalone = rootNode.GetComponentInChildren<PostProcessLayer>(true);
							if (postProcessLayerStandalone != null)
							{
								postProcessLayerStandalone.enabled = postProcessStandaloneTurnedOn;
							}

							// Post processing suitable for mobile
							var postProcessLayerMobile = rootNode.GetComponentInChildren<PostProcessor>(true);
							if (postProcessLayerMobile != null)
							{
								postProcessLayerMobile.enabled = postProcessMobileTurnedOn;
							}
						}
					}
				}
			}
		}

		// Setup settings
		static void EnablePostProcessAtRuntime(bool postProcessEnabled)
		{
			{
				// unity post processing 
				{
					var ar = FindObjectsOfTypeAll<PostProcessVolume>();
					foreach (var go in ar)
					{
						go.enabled = postProcessEnabled;
					}
				}

				{
					var ar = FindObjectsOfTypeAll<PostProcessLayer>();
					foreach (var go in ar)
					{
						go.enabled = postProcessEnabled;
					}
				}

				// mobile post processing
				{
					var ar = FindObjectsOfTypeAll<PostProcessor>();
					foreach (var go in ar)
					{
						go.enabled = postProcessEnabled;
					}
				}
			}
		}
		
		#endregion

		// === Build graphics setting according to platform ===

		#region buid_graphics_setting


		// Setup settings
		static void SetupSettings(bool postProcessStandaloneTurnedOn)
		{
#if (UNITY_EDITOR)
			if (BuildPipeline.isBuildingPlayer)
			{
				// unity post processing 
				{
					var ar = FindObjectsOfTypeAll<PostProcessVolume>();
					foreach (var go in ar)
					{
						if (postProcessStandaloneTurnedOn == false)
						{
							GameObject.DestroyImmediate(go);
						}
					}
				}

				{
					var ar = FindObjectsOfTypeAll<PostProcessLayer>();
					foreach (var go in ar)
					{
						if (postProcessStandaloneTurnedOn == false)
						{
							GameObject.DestroyImmediate(go);
						}
					}
				}

				// mobile post processing
				{
					var ar = FindObjectsOfTypeAll<PostProcessor>();
					foreach (var go in ar)
					{
						if (postProcessStandaloneTurnedOn == true)
						{
							GameObject.DestroyImmediate(go);
						}
					}
				}
			}
#endif
		}


#if (UNITY_EDITOR)
		[PostProcessScene(0)]
		static public void BuildPostProcessScene()
		{
#if (UNITY_STANDALONE)
			SetupSettings(true);
#elif (UNITY_ANDROID)
			SetupSettings(false);
#elif (UNITY_WEBGL)
			SetupSettings(false);
#elif (UNITY_WSA)
			SetupSettings(false);
#endif
		}
#endif
		#endregion

		// === IGraphicsSetting ===

		#region IGraphicsSetting

		// Quality

		public int QualityLevelCount
		{
			get
			{
				return QualitySettings.names.Length;
			}
		}

		void SetQualityLevel(int value)
		{
			if (value >= 0 && value < QualityLevelCount)
			{
				QualitySettings.SetQualityLevel(value, true);
				Serialize();
			}
		}

		public int QualityLevel
		{
			get
			{
				return QualitySettings.GetQualityLevel();
			}
			set
			{
				SetQualityLevel(value);
			}
		}

		// Screen

		public int ScreenResolutionWith
		{
			get
			{
				return Screen.width;
			}
		}

		public int ScreenResolutionHeight
		{
			get
			{
				return Screen.height;
			}
		}

		public bool ScreenFullScreen
		{
			get
			{
				return Screen.fullScreen;
			}
		}

		public void ScreenResolution(int width, int height, bool fullscreen)
		{
			if (width != ScreenResolutionWith || height != ScreenResolutionHeight || fullscreen != ScreenFullScreen)
			{
				Screen.SetResolution(width, height, fullscreen);
				Serialize();
			}
		}

		// post processing

		void SetPostPorcessing(bool value)
		{
			if (value != postProcessEnabled)
			{
				postProcessEnabled = value;
				EnablePostProcessAtRuntime(postProcessEnabled);
				Serialize();
			}
		}

		public bool PostProcessing
		{
			get
			{
				return postProcessEnabled;
			}

			set
			{
				SetPostPorcessing(value);
			}
		}

		#endregion
	}
}