﻿namespace codingVR.GraphicsEngine
{
	public interface IGraphicsSetting
	{
		// Quality level
		int QualityLevelCount { get; }
		int QualityLevel { get; set; }

		// Screen Resolution
		int ScreenResolutionWith { get; }
		int ScreenResolutionHeight { get; }
		bool ScreenFullScreen { get; }
		void ScreenResolution (int width, int height, bool fullscreen);
		
		// Rendering Properties
		bool PostProcessing { get; set; }
	}
}