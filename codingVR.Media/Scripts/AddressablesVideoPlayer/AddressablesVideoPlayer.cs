﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Video;

namespace codingVR.Media
{
	// https://www.gamedev.net/tutorials/programming/general-and-gameplay-programming/delivering-videos-with-unity-addressables-r5281/

	[RequireComponent(typeof(VideoPlayer))]
	public class AddressablesVideoPlayer : MonoBehaviour
	{
		[SerializeField] private AssetReference videoClipRefs = null;
		private VideoPlayer videoPlayer;

		public void LoadVideo()
		{
			if (videoClipRefs != null)
			{
				StartCoroutine(LoadVideoInternal());
			}
		}

		private IEnumerator LoadVideoInternal()
		{
			if (videoClipRefs != null)
			{
				var asyncOperationHandle = videoClipRefs.LoadAssetAsync<VideoClip>();
				yield return asyncOperationHandle;
				videoPlayer.clip = asyncOperationHandle.Result;
				Addressables.Release(asyncOperationHandle);
			}
			yield return null;
		}

		void Awake()
		{
			videoPlayer = GetComponent<VideoPlayer>();
			LoadVideo();
		}
	}
}
