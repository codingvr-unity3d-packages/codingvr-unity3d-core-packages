﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeshExtensions
{
    public static Mesh WeldVertices(this Mesh aMesh, float aMaxDelta = 0.01f)
    {
        var verts = aMesh.vertices;
        var normals = aMesh.normals;
        var uvs1 = aMesh.uv;
        var uvs2 = aMesh.uv2;
        var uvs3 = aMesh.uv3;
        var colors = aMesh.colors;
        
        Dictionary<Vector3, int> duplicateHashTable = new Dictionary<Vector3, int>();
        List<int> newVerts = new List<int>();
        int[] map = new int[verts.Length];

        //create mapping and find duplicates, dictionaries are like hashtables, mean fast
        for (int i = 0; i < verts.Length; i++)
        {
            if (!duplicateHashTable.ContainsKey(verts[i]))
            {
                duplicateHashTable.Add(verts[i], newVerts.Count);
                map[i] = newVerts.Count;
                newVerts.Add(i);
            }
            else
            {
                map[i] = duplicateHashTable[verts[i]];
            }
        }

        // create new vertices
        Vector3[] verts_copy = null;
        Vector3[] normals_copy = null;
        Vector2[] uvs1_copy = null;
        Vector2[] uvs2_copy = null;
        Vector2[] uvs3_copy = null;
        Color[] colors_copy = null;


        verts_copy = new Vector3[newVerts.Count];
        if (normals.Length > 0)
            normals_copy = new Vector3[newVerts.Count];
        if (uvs1.Length > 0)
            uvs1_copy = new Vector2[newVerts.Count];
        if (uvs2.Length > 0)
            uvs2_copy = new Vector2[newVerts.Count];
        if (uvs3.Length > 0)
            uvs3_copy = new Vector2[newVerts.Count];
        if (colors.Length > 0)
            colors_copy = new Color[newVerts.Count];

        for (int i = 0; i < newVerts.Count; i++)
        {
            int a = newVerts[i];
            verts_copy[i] = verts[a];
            if (normals_copy != null)
                normals_copy[i] = normals[a];
            if (uvs1_copy != null)
                uvs1_copy[i] = uvs1[a];
            if (uvs2_copy != null)
                uvs2_copy[i] = uvs2[a];
            if (uvs3_copy != null)
                uvs3_copy[i] = uvs3[a];
            if (colors_copy != null)
                colors_copy[i] = colors[a];
        }
        // map the triangle to the new vertices
        var tris = aMesh.triangles;
        for (int i = 0; i < tris.Length; i++)
        {
            tris[i] = map[tris[i]];
        }
        aMesh.triangles = tris;
        aMesh.vertices = verts_copy;
        if (normals_copy != null)
            aMesh.normals = normals_copy;
        if (uvs1_copy != null)
        aMesh.uv = uvs1_copy;
        if (uvs2_copy != null)
            aMesh.uv2 = uvs2_copy;
        if (uvs3_copy != null)
            aMesh.uv3 = uvs3_copy;
        if (colors_copy != null)
            aMesh.colors = colors_copy;

        aMesh.RecalculateBounds();
        aMesh.RecalculateNormals();

        return aMesh;
    }
}
