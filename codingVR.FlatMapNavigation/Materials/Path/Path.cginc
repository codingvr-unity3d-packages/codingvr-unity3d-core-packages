﻿float ComputeFadeOut(float pathDistance, float centerDistance)
{
	centerDistance = centerDistance + 8.0;
	float radius = abs(centerDistance - pathDistance) / 10.0;
	float r = clamp(radius, 0.0, 1.0);
	return 1.0 - pow(r, 2.0);
}

float invLerp(float from, float to, float value) {
	return (value - from) / (to - from);
}

float ComputePathLive(float pathDistance)
{
	float minRadius = 4.0;
	float maxRadius = 5.0;

	pathDistance = invLerp(minRadius, maxRadius, pathDistance);
	float r = clamp(pathDistance, 0.0, 1.0);
	return 1.0 - r;
}