﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "CodingVR/FlatMapNavigation/Path" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_CurrentPosition("Current Position", Vector) = (0, 0, 0)
		_DistanceFromStart("Distance From Start", Float) = 0.0
		_DistancePointToLine("Distance Point To Line", Float) = 0.0
	}
	SubShader {
		//Tags { "Queue" = "AlphaTest" "RenderType" = "TransparentCutout" "IgnoreProjector" = "True"  }
		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" "IgnoreProjector" = "True" /*"ForceNoShadowCasting" = "True"*/}

		ZWrite Off
		ZTest Less
		Cull Off
		AlphaTest GEqual 0.9
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 200

		//Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM

		#include "Path.cginc"

		// DOCUMENTATION
		// https://docs.unity3d.com/Manual/SL-SurfaceShaders.html?_ga=2.17161201.953556387.1634799673-351060288.1585493398
		// https://docs.unity3d.com/Manual/SL-SubShaderTags.html
		// https://docs.unity3d.com/Manual/shader-objects.html
		// https://github.com/TwoTailsGames/Unity-Built-in-Shaders/blob/master/DefaultResourcesExtra/Standard.shader

		// Physically based Standard lighting model, and enable shadows on all light types
		//#pragma surface surf Standard fullforwardshadows vertex:vert addshadow alpha:fade 
		#pragma surface surf Standard vertex:vert alpha:fade  noshadow noshadowmask

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 4.0
			
		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float4 texcoord;
			float4 vcolor : COLOR; // vertex color
			float3 startPosition;
			float3 vertexPosition;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float3 _CurrentPosition;
		float _DistanceFromStart;
		float _DistancePointToLine;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void vert(inout appdata_full v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.texcoord = v.texcoord;
			o.startPosition = _CurrentPosition;
			o.vertexPosition = v.vertex.xyz;
		}

		void surf(Input IN, inout SurfaceOutputStandard o) 
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color * IN.vcolor;
			o.Albedo.rgb = c.rgb;
			o.Alpha = c.a;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;

			/*
			float distanceFromStart = _DistanceFromStart + 5.0;

			if (IN.texcoord.y < distanceFromStart)
			{
				float v = (distanceFromStart - IN.texcoord.y) / 5.0;
				float r = (1.0 - clamp(v, 0.0, 1.0));
				o.Alpha *= r;
			}
			

			float distanceFromStart2 = _DistanceFromStart + 15.0;
			if (IN.texcoord.y > distanceFromStart2)
			{
				float v = (IN.texcoord.y - distanceFromStart2) / 5.0;
				float r = (1.0 - clamp(v, 0.0, 1.0));
				o.Alpha *= r;
			}
			*/

			o.Alpha *= ComputeFadeOut(IN.texcoord.y, _DistanceFromStart);
			o.Alpha *= ComputePathLive(_DistancePointToLine);
			
			/*
			if (distance(IN.startPosition, IN.vertexPosition) > 4.0)
			{
				o.Albedo.rgb = 1.0;
			}
			*/
			
		}
		ENDCG

		// ====================================================================================================================================================

		Pass
		{
			Name "ShadowCaster"
			Tags{ "Queue" = "AlphaTest" "LightMode" = "ShadowCaster" }

			ZWrite On ZTest LEqual
			
			CGPROGRAM
			#pragma target 3.0

			// -------------------------------------

			#pragma shader_feature_local _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
			#pragma shader_feature_local _METALLICGLOSSMAP
			#pragma shader_feature_local _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
			#pragma shader_feature_local _PARALLAXMAP
			#pragma shader_feature_local UNITY_STANDARD_USE_SHADOW_UVS
			#define UNITY_STANDARD_USE_SHADOW_UVS

			#pragma multi_compile_shadowcaster
			#pragma multi_compile_instancing

			// Uncomment the following line to enable dithering LOD crossfade. Note: there are more in the file to uncomment for other passes.
			//#pragma multi_compile _ LOD_FADE_CROSSFADE

			#include "UnityStandardShadow.cginc"
			#include "Path.cginc"

			#pragma vertex vert
			#pragma fragment frag

			// see https://github.com/TwoTailsGames/Unity-Built-in-Shaders/blob/master/CGIncludes/UnityStandardShadow.cginc

			struct OutputVertex {
				float3 startPosition : POSITION1;
				float3 vertexPosition : POSITION2;
				float2 tex : TEXCOORD2;
			};

			float3 _CurrentPosition;
			float _DistanceFromStart;
			float _DistancePointToLine;

			void vert(VertexInput v, out float4 opos : SV_POSITION, out VertexOutputShadowCaster oShadowCaster, out OutputVertex oVertex)
			{
				UNITY_SETUP_INSTANCE_ID(v);
				opos = UnityObjectToClipPos(v.vertex);

				UNITY_SETUP_INSTANCE_ID(v);
				TRANSFER_SHADOW_CASTER_NOPOS(oShadowCaster, opos)
				oShadowCaster.tex = TRANSFORM_TEX(v.uv0, _MainTex);
#ifdef _PARALLAXMAP
				TANGENT_SPACE_ROTATION;
				oShadowCaster.viewDirForParallax = mul(rotation, ObjSpaceViewDir(v.vertex));
#endif
				oVertex.startPosition = _CurrentPosition;
				oVertex.vertexPosition = v.vertex.xyz;
				oVertex.tex = v.uv0;
			}

			float4 frag(UNITY_POSITION(vpos), VertexOutputShadowCaster iShadowCaster, OutputVertex iVertex) : SV_Target
			{
				//float2 uv = (iShadowCaster.tex.xy - _MainTex_ST.zw) / _MainTex_ST.xy;
				float2 uv = iVertex.tex;

				//clip(- 0.1);


				float c = ComputeFadeOut(uv.y, _DistanceFromStart);
				c *= ComputePathLive(_DistancePointToLine);
				clip(c - 0.1);
				

				return fragShadowCaster(vpos, iShadowCaster);
			}
			

			ENDCG
		}

			

			/*
		// ------------------------------------------------------------------
				//  Base forward pass (directional light, emission, lightmaps, ...)
		Pass
		{
			Name "FORWARD"
			Tags { "LightMode" = "ForwardBase" }

			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite On

			CGPROGRAM
			#pragma target 2.0

			#pragma shader_feature _NORMALMAP
			#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
			#pragma shader_feature _EMISSION 
			#pragma shader_feature _METALLICGLOSSMAP 
			#pragma shader_feature ___ _DETAIL_MULX2
			// SM2.0: NOT SUPPORTED shader_feature _PARALLAXMAP

			#pragma skip_variants SHADOWS_SOFT DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE

			#pragma multi_compile_fwdbase
			#pragma multi_compile_fog

			#pragma vertex vertForwardBase
			#pragma fragment fragForwardBase

			#include "UnityStandardCore.cginc"

			ENDCG
		}
		*/
	}
	//FallBack "Standard"
}
