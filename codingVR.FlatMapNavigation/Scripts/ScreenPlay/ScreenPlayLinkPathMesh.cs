﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace codingVR.FlatMapNavigation
{
	[RequireComponent(typeof(GameEngine.IScreenPlayLinkObject))]
	public class ScreenPlayLinkPathMesh : MonoBehaviour, GameEngine.IScreenPlayLinkPathMesh
	{
		// === DATA-UI ==============================================================================================================

		#region DATA-UI
		[SerializeField, HideInInspector]
		GameObject meshHolder;

		[Header("Path settings")]
		[FormerlySerializedAs("roadWidth")]
		[SerializeField]
		float pathWidth = .4f;
		[Range(0, .5f)]
		[SerializeField]
		float thickness = .15f;

		[Header("Material settings")]
		[FormerlySerializedAs("roadMaterial")]
		[SerializeField]
		Material pathMaterial;
		[SerializeField]
		Material undersideMaterial;
		[SerializeField]
		float textureTiling = 1;

		[SerializeField]
		float lastdistancePointToLine = 0.0f;

		#endregion

		// === DATA-PRIVATE ==============================================================================================================

		#region DATA-PRIVATE

		MeshFilter meshFilter;
		MeshRenderer meshRenderer;
		Mesh mesh;
		bool created = false;

		

		#endregion

		// === UNITY-LIFECYCLE ==============================================================================================================

		#region UNITY-LIFECYCLE

		// Start is called before the first frame update
		void Start()
		{
		}

		// Update is called once per frame
		void Update()
		{
			var polyLine = LastPath.PolyLine;

			// compute closest position
			(float distanceTravelled, float distancePointToLine) = polyLine.ComputeDistanceTravelledToEnd(global::GameCreator.Core.Hooks.HookPlayer.Instance.transform.position, false);

			/*
			// compute closest position
			bool reached = false;
			float time = 0.0f;
			int numSegment = 0;
			float lengthPathRemaining = 0.0f;
			int startNode = 0;
			(reached, time, numSegment, lengthPathRemaining, startNode) = polyLine.ComputeClosestPositionAndLengthToEnd(global::GameCreator.Core.Hooks.HookPlayer.Instance.transform.position, false);
			lengthPathRemaining = polyLine.ComputeLength() - lengthPathRemaining;
			*/

			if (pathMaterial != null && undersideMaterial != null)
			{
				meshRenderer.sharedMaterials[0].SetVector("_CurrentPosition", global::GameCreator.Core.Hooks.HookPlayer.Instance.transform.position);
				meshRenderer.sharedMaterials[0].SetFloat("_DistanceFromStart", distanceTravelled);
				meshRenderer.sharedMaterials[0].SetFloat("_DistancePointToLine", distancePointToLine);
			}

			lastdistancePointToLine = distancePointToLine;
		}

		#endregion

		// === IScreenPlayLinkPathMesh ==============================================================================================================

		#region IScreenPlayLinkPathMesh

		// Update path mesh
		public void UpdatePathMesh()
		{
			if (created == true)
			{
				UpdatePath();
			}
			else
			{
				CreatePath();
				created = true;
			}
		}

		// Distance point to path
		public float DistancePointToPath
		{
			get
			{
				return lastdistancePointToLine;
			}
		}
		
		#endregion

		// === IMPLEMENTATION ==============================================================================================================

		#region IMPLEMENTATION

		[Button(enabledMode: EButtonEnableMode.Always)]
		void ManualUpdate()
		{
			UpdatePathMesh();
		}

		void CreatePath()
		{
			AssignMeshComponents();
			AssignMaterials();
			UpdatePath();
		}
		void UpdatePath()
		{
			RenewPath();
			CreateRoadMesh();
		}
		FlatMapNavigation.FlatMapPathDetails LastPath
		{
			get
			{
				return GetComponent< FlatMapNavigation.IFlatMapLinkObject>().Path;
			}
		}
		void RenewPath()
		{
			GetComponent<FlatMapNavigation.IFlatMapLinkObject>().RenewPath();
		}

		// create road mesh
		void CreateRoadMesh()
		{
			bool isClosedLoop = false;

			var polyLine = LastPath.PolyLine;
			var splinePoints  = polyLine.SplinePoints;

			Vector3[] verts = new Vector3[splinePoints.Length * 8];
			Vector2[] uvs = new Vector2[verts.Length];
			Vector3[] normals = new Vector3[verts.Length];

			int numTris = 2 * (splinePoints.Length - 1) + ((isClosedLoop) ? 2 : 0);
			int[] pathTriangles = new int[numTris * 3];
			int[] underRoadTriangles = new int[numTris * 3];
			int[] sideOfRoadTriangles = new int[numTris * 2 * 3];

			int vertIndex = 0;
			int triIndex = 0;

			// Vertices for the top of the path are layed out:
			// 0  1
			// 8  9
			// and so on... So the triangle map 0,8,1 for example, defines a triangle from top left to bottom left to bottom right.
			int[] triangleMap = { 0, 8, 1, 1, 8, 9 };
			int[] sidesTriangleMap = { 4, 6, 14, 12, 4, 14, 5, 15, 7, 13, 15, 5 };

			for (int i = 0; i < splinePoints.Length; i++)
			{
				Vector3 localUp = Vector3.up;
				Vector3 localRight = splinePoints[i].normal;

				// Find position to left and right of current path vertex
				Vector3 vertSideA = splinePoints[i].point - localRight * Mathf.Abs(pathWidth);
				Vector3 vertSideB = splinePoints[i].point + localRight * Mathf.Abs(pathWidth);

				/*
				RaycastHit hit;
				if (Physics.Raycast(vertSideA, Vector3.down, out hit))
				{
					vertSideA = hit.point;
				}
				if (Physics.Raycast(vertSideB, Vector3.down, out hit))
				{
					vertSideB = hit.point;
				}
				*/

				// Add top of path vertices
				verts[vertIndex + 0] = vertSideA;
				verts[vertIndex + 1] = vertSideB;
				// Add bottom of path vertices
				verts[vertIndex + 2] = vertSideA - localUp * thickness;
				verts[vertIndex + 3] = vertSideB - localUp * thickness;

				// Duplicate vertices to get flat shading for sides of path
				verts[vertIndex + 4] = verts[vertIndex + 0];
				verts[vertIndex + 5] = verts[vertIndex + 1];
				verts[vertIndex + 6] = verts[vertIndex + 2];
				verts[vertIndex + 7] = verts[vertIndex + 3];

				// Set uv on y axis to path time (0 at start of path, up to 1 at end of path)
				uvs[vertIndex + 0] = new Vector2(0, splinePoints[i].distance);
				uvs[vertIndex + 1] = new Vector2(1, splinePoints[i].distance);

				// Top of path normals
				normals[vertIndex + 0] = localUp;
				normals[vertIndex + 1] = localUp;
				// Bottom of path normals
				normals[vertIndex + 2] = -localUp;
				normals[vertIndex + 3] = -localUp;
				// Sides of path normals
				normals[vertIndex + 4] = -localRight;
				normals[vertIndex + 5] = localRight;
				normals[vertIndex + 6] = -localRight;
				normals[vertIndex + 7] = localRight;

				// Set triangle indices
				if (i < splinePoints.Length - 1 || isClosedLoop)
				{
					for (int j = 0; j < triangleMap.Length; j++)
					{
						pathTriangles[triIndex + j] = (vertIndex + triangleMap[j]) % verts.Length;
						// reverse triangle map for under path so that triangles wind the other way and are visible from underneath
						underRoadTriangles[triIndex + j] = (vertIndex + triangleMap[triangleMap.Length - 1 - j] + 2) % verts.Length;
					}
					for (int j = 0; j < sidesTriangleMap.Length; j++)
					{
						sideOfRoadTriangles[triIndex * 2 + j] = (vertIndex + sidesTriangleMap[j]) % verts.Length;
					}

				}
				vertIndex += 8;
				triIndex += 6;
			}

			mesh.Clear();
			mesh.vertices = verts;
			mesh.uv = uvs;
			mesh.normals = normals;
			mesh.subMeshCount = 3;
			mesh.SetTriangles(pathTriangles, 0);
			mesh.SetTriangles(underRoadTriangles, 1);
			mesh.SetTriangles(sideOfRoadTriangles, 2);
			mesh.RecalculateBounds();
		}
		
		// Add MeshRenderer and MeshFilter components to this gameobject if not already attached
		void AssignMeshComponents()
		{
			if (meshHolder == null)
			{
				meshHolder = new GameObject(name+"_MeshHolder");
				meshHolder.transform.parent = gameObject.transform.parent;
				meshHolder.transform.SetSiblingIndex(gameObject.transform.GetSiblingIndex() + 1);
			}

			meshHolder.transform.rotation = Quaternion.identity;
			meshHolder.transform.position = Vector3.zero;
			meshHolder.transform.localScale = Vector3.one;

			// Ensure mesh renderer and filter components are assigned
			if (!meshHolder.gameObject.GetComponent<MeshFilter>())
			{
				meshHolder.gameObject.AddComponent<MeshFilter>();
			}
			if (!meshHolder.GetComponent<MeshRenderer>())
			{
				meshHolder.gameObject.AddComponent<MeshRenderer>();
			}

			meshRenderer = meshHolder.GetComponent<MeshRenderer>();
			meshFilter = meshHolder.GetComponent<MeshFilter>();
			if (mesh == null)
			{
				mesh = new Mesh();
			}
			meshFilter.sharedMesh = mesh;
		}
		
		// add shared materials
		void AssignMaterials()
		{
			if (pathMaterial != null && undersideMaterial != null)
			{
				meshRenderer.sharedMaterials = new Material[] { pathMaterial, undersideMaterial, undersideMaterial };
				meshRenderer.sharedMaterials[0].mainTextureScale = new Vector3(1, textureTiling);
			}
		}

		#endregion
	}
}