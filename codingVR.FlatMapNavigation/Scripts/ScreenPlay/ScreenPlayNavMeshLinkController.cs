﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using codingVR.Core;
using NaughtyAttributes;
using UnityEngine;
using Zenject;

namespace codingVR.FlatMapNavigation
{
	[RequireComponent(typeof(Core.GameObjectDrawGizmos))]
	[RequireComponent(typeof(UnityEngine.AI.NavMeshLink))]
	[ExecuteInEditMode]
	public class ScreenPlayNavMeshLinkController : MonoBehaviour, IScreenPlayNavMeshLinkController
	{
		// === DATA-UI ==============================================

		#region DATA-UI

		[Header("Link settings")]
		[SerializeField]
		[OnValueChanged(nameof(OnChangeValues))]
		string tagFirst;

		[SerializeField]
		[OnValueChanged(nameof(OnChangeValues))]
		string tagSecond;

		[SerializeField]
		[OnValueChanged(nameof(OnChangeValues))]
		float width = 0.0f;

		[SerializeField]
		[OnValueChanged(nameof(OnChangeValues))]
		bool bidirectional = true;

		#endregion

		// === DATA-UI ==============================================

		#region DATA-PRIVATE


		#endregion

		// === UNITY-LIFECYCLE ==============================================

		#region UNITY-LIFECYCLE

		private codingVR.Core.IEventManager eventManager;
		private bool listernerInstalled = false;

		[Inject]
		public void Constructor(codingVR.Core.IEventManager eventManager)
		{
			this.eventManager = eventManager;
			// enable listeners
			EnableListeners();
		}

		// Start is called before the first frame update
		void Start()
		{
			OnChangeValues(); 
		}

		private void OnDrawGizmos()
		{
			
		}

		private void OnEnable()
		{
			EnableListeners();
		}

		private void OnDisable()
		{
			DisableListeners();
		}

		/*
		private void Update()
		{
			MakeNavMeshLink();
		}
		*/

		#endregion

		// === IMPLEMENTATION ==============================================================================================================

		#region IMPLEMENTATION

		/*
		 *
		 * Triggering
		 * 
		 */

		private void EnableListeners()
		{
			// listener on game event
			if (eventManager != null && listernerInstalled == false)
			{
				eventManager.AddListener<Scene.Event_SceneLoaded>(OnSceneLoaded);
				listernerInstalled = true;
			}
		}

		private void DisableListeners()
		{
			// remove listener on game event
			if (eventManager != null && listernerInstalled == true)
			{
				eventManager.RemoveListener<Scene.Event_SceneLoaded>(OnSceneLoaded);
				listernerInstalled = false;
			}
		}


		private void OnSceneLoaded(codingVR.Scene.Event_SceneLoaded ev)
		{
			OnChangeValues();
		}


		void MakeNavMeshLink()
		{
			if (string.IsNullOrEmpty(tagFirst) == false && string.IsNullOrEmpty(tagSecond) == false)
			{
				var firstGameObject = GameObjectExtensions.FindObjectFromTag("ScreenPlay", tagFirst);
				var secondGameObject = GameObjectExtensions.FindObjectFromTag("ScreenPlay", tagSecond);
				if (firstGameObject != null && secondGameObject != null)
				{
					var firstPosition = firstGameObject.transform.position;
					var secondPosition = secondGameObject.transform.position;

					var center = (firstPosition + secondPosition) * 0.5f;

					var offsetStart = firstPosition - center;
					var offsetEnd = secondPosition - center;

					transform.position = center;

					var navMeshLink = GetComponent<UnityEngine.AI.NavMeshLink>();
					navMeshLink.startPoint = offsetStart;
					navMeshLink.endPoint = offsetEnd;
					navMeshLink.width = width;
					navMeshLink.bidirectional = bidirectional;
				}
			}
		}

		void MakeDebugTitle()
		{
#if UNITY_EDITOR
			var debug = GetComponent<Core.GameObjectDrawGizmos>();
			if (TagPair != null)
				debug.SubTitle = TagPair.ToString();
			else
				debug.SubTitle = "";
#endif
		}

		Data.TagPair TagPair
		{
			get
			{
				Data.TagPair tagPair = null;
				if (string.IsNullOrEmpty(tagFirst) == false && string.IsNullOrEmpty(tagSecond) == false)
				{
					tagPair = Data.TagPair.MakePair(tagFirst, tagSecond);
				}
				return tagPair;
			}
		}

		void OnChangeValues()
		{
			MakeDebugTitle();
			MakeNavMeshLink();
		}
			
		#endregion

		// === IScreenPlayNavMeshLinkController ================================================================================

		#region IScreenPlayNavMeshLinkController

		public string TagFirst
		{
			get
			{
				return tagFirst;
			}

			set
			{
				if (value != tagFirst)
				{
					tagFirst = value;
					OnChangeValues();
				}
			}
		}

		public string TagSecond
		{
			get
			{
				return tagSecond;
			}

			set
			{
				if (value != tagSecond)
				{
					tagSecond = value;
					OnChangeValues();
				}
			}
		}

		public float Width
		{
			get
			{
				return width;
			}

			set
			{
				if (value != width)
				{
					width = value;
					OnChangeValues();
				}
			}

		}

		public bool Bidirectional
		{
			get
			{
				return bidirectional;
			}

			set
			{
				if (value != bidirectional)
				{
					bidirectional = value;
					OnChangeValues();
				}
			}
		}

		#endregion
	}
}