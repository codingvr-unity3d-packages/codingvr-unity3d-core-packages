﻿using codingVR.Core;
using NaughtyAttributes;
using UnityEngine;
using Zenject;

namespace codingVR.FlatMapNavigation
{
	[RequireComponent(typeof(Core.GameObjectDrawGizmos))]
	[ExecuteAlways]
	public class ScreenPlayLinkObject : MonoBehaviour, GameEngine.IScreenPlayLinkObject, IFlatMapLinkObject
	{
		// === DATA-UI ==============================================

		#region DATA-UI

		[Header("Link settings")]

		[SerializeField]
		codingVR.GameEngine.ScreenPlayRootKey screenPlayRootKey;

		[SerializeField]
		[OnValueChanged(nameof(OnChangedTag))]
		string tagFirst;
		[SerializeField]
		[OnValueChanged(nameof(OnChangedTag))]
		string tagSecond;

		[Header("Nav mesh settings")]
		[SerializeField]
		bool canUseNavMesh = false;
		[SerializeField]
		bool useRaycast = false;

		[Header("Debug settings")]
		[SerializeField]
		float sphereSize = 0.10f;
			   
		[Header("Update")]
		[SerializeField]
		bool manualUpdate = false;
		[Button(enabledMode: EButtonEnableMode.Always)]
		void ManualUpdate()
		{
			RenewPath();
		}

		#endregion

		// === UNITY-LIFECYCLE ==============================================

		#region DATA-PRIVATE

		FlatMapNavigation.FlatMapPathDetails lastFlatMapPathDetails = null;

		#endregion

		// === UNITY-LIFECYCLE ==============================================

		#region UNITY-LIFECYCLE

		GameEngine.IScreenPlay screenPlay = null;

		[Inject]
		public void Constructor(GameEngine.IScreenPlay screenPlay, Core.IEventManager eventManager)
		{
			this.screenPlay = screenPlay;
		}

		// Start is called before the first frame update
		void Start()
		{
			OnChangedTag();
		}

		// Update is called once per frame
		void Update()
		{

		}

		private void OnDrawGizmos()
		{
			DrawSpline();
		}

		#endregion

		// === IMPLEMENTATION ==============================================================================================================

		#region IMPLEMENTATION

		FlatMapNavigation.FlatMapPathDetails CreatePath()
		{
			FlatMapNavigation.FlatMapPathDetails path = null;
			var tagPair = TagPathPair;
			if (tagPair != null)
			{
				var first = tagPair.First;
				var second = tagPair.Second;
				GameObject currentGameObject = null;
				GameObject nextGameObject = null;

				// in edior mode the screenPlay is null
				if (screenPlay == null)
				{
					currentGameObject = GameObjectExtensions.FindObjectFromTag("ScreenPlay", first.ToString());
					nextGameObject = GameObjectExtensions.FindObjectFromTag("ScreenPlay", second.ToString());
				}
				else
				{
					currentGameObject = screenPlay.FindTagPath(first);
					nextGameObject = screenPlay.FindTagPath(second);
				}

				var current = currentGameObject.GetComponent<GameEngine.ScreenPlayObject>();
				var next = nextGameObject.GetComponent<GameEngine.ScreenPlayObject>();
				if (current != null && next != null)
				{
					Data.Tag tag0 = Data.Tag.CreateTag(current.name);
					Data.Tag tag1 = Data.Tag.CreateTag(next.name);
					Data.TagPair key = Data.TagPair.MakePair(tag0, tag1);
					SplineSegment.RaycastHandlerFunction ray = null;
					if (this.useRaycast == true)
					{
						ray = FlatMapNavigation.FlatMapPathDetails.RaycastHandler;
					}
					path = FlatMapNavigation.FlatMapPathDetails.MakeBezierQuadraticSplineFromPathController
					(
						key, 
						current.transform.position, 
						transform.position, 
						next.transform.position, 
						CanUseNavMesh, 
						1.0f, 
						ray
					);
				}
			}
			return path;
		}

		void DrawSpline()
		{
			if (manualUpdate == false)
			{
				RenewPath();
			}
			Path.DrawPolyLine(sphereSize, Color.red);
		}

		#endregion

		// === IScreenPlayLinkObject ===

		#region IScreenPlayLinkObject

		public Data.TagPathPair TagPathPair
		{
			get
			{
				Data.TagPathPair tagPathPair = null;
				if (string.IsNullOrEmpty(tagFirst) == false && string.IsNullOrEmpty(tagSecond) == false)
				{
					tagPathPair = Data.TagPathPair.MakePair(screenPlayRootKey?.LinkToRoot(tagFirst) ?? tagFirst, screenPlayRootKey?.LinkToRoot(tagSecond) ?? tagSecond);
				}
				return tagPathPair;
			}
		}

		public Data.TagPair TagPair
		{
			get
			{
				Data.TagPathPair tagPathPair = TagPathPair;
				Data.TagPair tagPair = null;
				if (tagPathPair != null)
				{
					tagPair = Data.TagPair.MakePair(tagPathPair.First.tag, tagPathPair.Second.tag);
				}
				
				return tagPair;
			}
		}

		public bool CanUseNavMesh => canUseNavMesh;

		public void OnChangedTag()
		{
			var debug = GetComponent<Core.GameObjectDrawGizmos>();
			if (TagPathPair != null)
				debug.SubTitle = TagPathPair.ToString();
			else
				debug.SubTitle = "";
		}

		#endregion

		// === IFlatMapLinkObject ===

		#region IFlatMapLinkObject

		// Gameobject
		public GameObject GameObject => gameObject;

		public FlatMapNavigation.FlatMapPathDetails Path
		{
			get
			{
				if (lastFlatMapPathDetails == null)
				{
					RenewPath();
				}
				return lastFlatMapPathDetails;
			}
		}

		public void RenewPath()
		{
			lastFlatMapPathDetails = CreatePath();
		}

		public 	(float, float) ComputeMinMaxDistanceFrom(Vector3 position)
		{
			return lastFlatMapPathDetails.ComputeMinMaxDistance(position);
		}

		#endregion
	}
}
