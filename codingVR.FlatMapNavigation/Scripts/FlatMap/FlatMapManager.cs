﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.FlatMapNavigation
{
	public class FlatMapManager : MonoBehaviour, GameEngine.IMapManager
	{
		/*
		 * 
		 * Life cycle
		 * 
		 */

		GameEngine.IScreenPlay screenPlay;

		// === Constructor ===

		[Inject]
		protected void Construct(GameEngine.IScreenPlay screenPlay)
		{
			this.screenPlay = screenPlay;
		}

		// Start is called before the first frame update
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{

		}

		/*
		 * 
		 * GameEngine.IMapManager
		 * 
		 */

		public bool Spawn(Data.Tag tag, GameObject player)
		{
			bool b = false;
			var unit = player.GetComponent<IFlatMapUnit>();
			if (unit != null)
			{
				(Vector3 pos, bool find) = screenPlay.PositionTag(tag);
				if (find == true)
				{
					unit.UnitTransform.position = pos;
					b = find;
				}
				else
				{
					Debug.LogErrorFormat("{0} : tag {1} not found !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, tag);
				}
			}

			// fix camera game location
			if (b == true)
			{
				var camGame = global::GameCreator.Core.Hooks.HookCamera.Instance;
				camGame.gameObject.transform.position = unit.UnitTransform.position + Vector3.up * 30.0f + unit.UnitTransform.forward * 30.0f;
				camGame.gameObject.transform.LookAt(unit.UnitTransform);
			}
			
			return b;
		}
	}
}
