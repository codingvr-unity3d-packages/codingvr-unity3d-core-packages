﻿using UnityEngine.Scripting.APIUpdating;
using Zenject;

namespace codingVR.FlatMapNavigation
{
	public class FlatMapNPCTravel : FlatMapCharacterTravel
	{
		// === Unity3d lifecycle ===

		// Start is called before the first frame update
		void Start()
		{}

		// Update is called once per frame
		void Update()
		{}

	   
		// === Dip Constructor ===

		[Inject]
		void Construct(GameEngine.IScreenPlay screenPLay)
		{
			base.Construct(screenPLay);
		}
	}
}
