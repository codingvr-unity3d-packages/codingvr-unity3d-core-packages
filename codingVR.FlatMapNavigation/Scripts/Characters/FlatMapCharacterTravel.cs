﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Scripting.APIUpdating;

namespace codingVR.FlatMapNavigation
{
	public class FlatMapCharacterTravel : MonoBehaviour, GameEngine.ICharacterTravel
	{
		// Screen play
		GameEngine.IScreenPlay screenPlay;

		// === Unity3d lifecycle ===

		// Start is called before the first frame update
		void Start()
		{}

		// Update is called once per frame
		void Update()
		{}

		// === Constructor ===

		protected void Construct(GameEngine.IScreenPlay screenPlay)
		{
			this.screenPlay = screenPlay;
		}

		// === GameEngine.ICharacterStates ===

		public void Teleport(Data.Tag tag)
		{
			var unit = this.gameObject.GetComponent<IFlatMapUnit>();
			Debug.Assert(unit != null);
			unit.Teleport(tag);
		}

		public void Teleport(Data.TagPath path)
		{
			var unit = this.gameObject.GetComponent<IFlatMapUnit>();
			Debug.Assert(unit != null);
			unit.Teleport(path);
		}

		public void SelectPath(Data.TagPair tagPair)
		{
			if (tagPair.First != Data.Tags.Player)
			{
				var unit = this.gameObject.GetComponent<IFlatMapUnit>();
				var pathTagStruct = screenPlay.PathTag(tagPair);
				if (pathTagStruct != null && unit != null)
				{
					if (pathTagStruct?.linkObject != null)
					{
						unit.PathToTravel = pathTagStruct?.linkObject.GameObject.GetComponent<IFlatMapLinkObject>().Path; 
					}
					else
					{
						if (pathTagStruct?.end != null)
						{
							unit.PathToTravel = FlatMapPathDetails.MakeBezierQuadraticSplineFromPathController(tagPair, pathTagStruct?.start ?? Vector3.zero, pathTagStruct?.mid, pathTagStruct?.end ?? Vector3.zero, false, 1.0f, null);
						}
						else
						{
							Debug.LogErrorFormat("{0} : Select path failed !!", System.Reflection.MethodBase.GetCurrentMethod().Name);

						}
					}
				}
				else
				{
					Debug.LogErrorFormat("{0} : Select path failed !!", System.Reflection.MethodBase.GetCurrentMethod().Name);
				}
			}
			else
			{
				var unit = this.gameObject.GetComponent<IFlatMapUnit>();
				(Vector3 end, bool findEnd) = screenPlay.PositionTag(tagPair.Second);
				if (findEnd == true && unit != null)
				{
					// a.b/|b| = |a|cosO 
					Vector3 start = transform.position;
					Vector3 mid = (end + start) * 0.5f;
					Vector3 projMid = Vector3.Dot(transform.forward, mid - start) * transform.forward;
					unit.PathToTravel = FlatMapPathDetails.MakeBezierQuadraticSplineFromPathController(tagPair, start, projMid, end, true, 1.0f, FlatMapPathDetails.RaycastHandler);
				}
			}
		}

		public void SelectPath(Data.TagPathPair tagPathPair)
		{
			if (tagPathPair.First.tag != Data.Tags.Player)
			{
				var unit = this.gameObject.GetComponent<IFlatMapUnit>();
				var pathTagStruct = screenPlay.PathFromTagPath(tagPathPair);
				if (pathTagStruct != null && unit != null)
				{
					if (pathTagStruct?.linkObject != null)
					{
						unit.PathToTravel = pathTagStruct?.linkObject.GameObject.GetComponent<IFlatMapLinkObject>().Path;
					}
					else
					{
						if (pathTagStruct?.end != null)
						{
							unit.PathToTravel = FlatMapPathDetails.MakeBezierQuadraticSplineFromPathController(tagPathPair.TagPair, pathTagStruct?.start ?? Vector3.zero, pathTagStruct?.mid, pathTagStruct?.end ?? Vector3.zero, false, 1.0f, null);
						}
						else
						{
							Debug.LogErrorFormat("{0} : Select path failed !!", System.Reflection.MethodBase.GetCurrentMethod().Name);

						}
					}
				}
				else
				{
					Debug.LogErrorFormat("{0} : Select path failed !!", System.Reflection.MethodBase.GetCurrentMethod().Name);
				}
			}
			else
			{
				var unit = this.gameObject.GetComponent<IFlatMapUnit>();
				(Vector3 end, bool findEnd) = screenPlay.PositionTagPath(tagPathPair.Second);
				if (findEnd == true && unit != null)
				{
					// a.b/|b| = |a|cosO 
					Vector3 start = transform.position;
					Vector3 mid = (end + start) * 0.5f;
					Vector3 projMid = Vector3.Dot(transform.forward, mid - start) * transform.forward;
					unit.PathToTravel = FlatMapPathDetails.MakeBezierQuadraticSplineFromPathController(tagPathPair.TagPair, start, projMid, end, true, 1.0f, FlatMapPathDetails.RaycastHandler);
				}
			}
		}

		public void SelectNextPath(Data.TagPair tagPair)
		{
		}


		public GameEngine.ICharacterMoving MoveOnPathBegin(bool reverse)
		{
			// select path
			var unit = this.gameObject.GetComponent<IFlatMapUnit>();
			Debug.Assert(unit != null);
			return unit.MoveOnPathBegin(reverse);
		}

		// Game object
		public GameObject GameObject
		{
			get
			{
				return gameObject;
			}
		}
	}
}
