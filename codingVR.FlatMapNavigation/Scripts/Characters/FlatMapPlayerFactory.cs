﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.FlatMapNavigation
{
	public class FlatMapPlayerFactory : IFactory<UnityEngine.Object, codingVR.GameEngine.ICharacterTravel>
	{
		DiContainer container;
		GameEngine.IGameRootManager gameRootManager;

		public FlatMapPlayerFactory(DiContainer container, GameEngine.IGameRootManager gameRootManager)
		{
			this.container = container;
			this.gameRootManager = gameRootManager;
		}

		public codingVR.GameEngine.ICharacterTravel Create(UnityEngine.Object playerPrefab)
		{
			return container.InstantiatePrefabForComponent<FlatMapCharacterTravel>(playerPrefab, gameRootManager.GameObject.transform);
		}
	}
}
