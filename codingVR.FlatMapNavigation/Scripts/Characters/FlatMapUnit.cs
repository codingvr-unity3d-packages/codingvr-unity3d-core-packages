﻿using UnityEngine.Scripting.APIUpdating;
using UnityEngine;
using System.Collections;
using Zenject;

namespace codingVR.FlatMapNavigation
{
	[MovedFrom(false, null, null, "HexUnit")]
	public class FlatMapUnit : MonoBehaviour, IFlatMapUnit
	{
		// === Constructor ===

		GameEngine.IScreenPlay screenPlay;

		[Inject]
		public void Construct(GameEngine.IScreenPlay screenPlay)
		{
			this.screenPlay = screenPlay;
		}

		// === Life cycle ===

		private void Awake()
		{
		}

		void OnEnable()
		{
		}

		// draw path
		void DrawPath(FlatMapPathDetails path, Color color)
		{
			if (path != null)
			{
				path.DrawPolyLine(0.10f, color);
			}
		}

		// draw debug inormation on path in editor mode
		void OnDrawGizmos()
		{
			DrawPath(PathToTravel, Color.green);
		}


		/*
		 * 
		 * IFlatMapUnit
		 * 
		 */

		public void Teleport(Data.Tag tag)
		{
			var goTag = screenPlay.FindTag(tag);
			if (goTag != null)
			{
				transform.position = goTag.transform.position;
			}
		}

		public void Teleport(Data.TagPath path)
		{
			var goTag = screenPlay.FindTagPath(path);
			if (goTag != null)
			{
				transform.position = goTag.transform.position;
			}
		}

		public GameEngine.ICharacterMoving MoveOnPathBegin(bool reverse)
		{
			var flatMapUnitMovingOnPath = FlatMapUnitMoveOnPath.Create(this, reverse);
			return flatMapUnitMovingOnPath;
		}

		// === path selection ===

		private FlatMapPathDetails currentPathToTravel;

		public FlatMapPathDetails PathToTravel
		{
			get
			{
				return currentPathToTravel;
			}
			set
			{
				currentPathToTravel = value;
			}
		}

		public Transform UnitTransform
		{
			get
			{
				return gameObject.transform;
			}
		}
	}
}