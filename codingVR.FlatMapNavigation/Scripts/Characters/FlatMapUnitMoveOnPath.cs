﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

// Computational Geometry Unity Library
// https://github.com/Habrador/Computational-geometry
// ComputeClosestPositionRecursive
// https://github.com/bluescan/bezier/blob/master/Bezier.cs

namespace codingVR.FlatMapNavigation
{
	//#define CLOSEST_ALTERNATIVE_METHOD

	// === FlatMapUnitMoveOnPath ====================================================================

	public class FlatMapUnitMoveOnPath : GameEngine.ICharacterMoving
	{
		IFlatMapUnit unit;
		bool forceStop = false;
		bool reverse = false;

		// constructor
		FlatMapUnitMoveOnPath(IFlatMapUnit unit, bool reverse)
		{
			// unit
			this.unit = unit;
			this.reverse = reverse;
		}

		// destructor
		~FlatMapUnitMoveOnPath()  // finalizer
		{
		}

		// create curved path
		static public GameEngine.ICharacterMoving Create(IFlatMapUnit unit, bool reverse)
		{
			return new FlatMapUnitMoveOnPath(unit, reverse);
		}


		// Update Iteration
		public (bool, Vector3, int) UpdateIteration(int findNode)
		{
			return this.unit.PathToTravel.UpdateIteration(unit.UnitTransform.position, findNode, reverse);
		}

		// Force stop
		public bool ForceStop
		{
			set
			{
				if (value != forceStop)
				{
					forceStop = value;
				}
			}
		}

		//  Excecute complete itration on path
		public IEnumerator ExecuteCompleteIteration(bool cancelable, global::GameCreator.Characters.Character character, float stopThreshold, float cancelDelay, GameObject target)
		{
			bool canceled = false;
			this.forceStop = false;

			// init time needed for cancelation process
			float initTime = Time.time;
			Vector3 lastPosition = unit.PathToTravel.LastPosition;
			int findNode = -1;

			// follow curved path
			bool reached = false;
			Vector3 velocity;
			while (reached == false && !forceStop && !canceled)
			{
				// we execute map travel
				(reached, velocity, findNode) = this.unit.PathToTravel.UpdateIteration(unit.UnitTransform.position, findNode, reverse);
				if (reached == false)
				{
					character.characterLocomotion.SetDirectionalDirection(velocity, null);
				}

				// cancel loop if needed
				if (cancelable == true && (Time.time - initTime) >= cancelDelay)
				{
					canceled = true;
				}

				// all right
				yield return null;
			}

			if (reached == true)
			{
			}

			if (canceled == true)
			{
			}

			yield return null;
		}
	};
}