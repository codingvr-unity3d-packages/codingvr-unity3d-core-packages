﻿using codingVR.Core;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

namespace codingVR.FlatMapNavigation
{
	public class FlatMapPathDetails
	{
		// === DATA =====================================================================================================================

		#region DATA

		Vector3[] sharedSplineControlPoints = null;
		Core.SplineSegment[] splineSegment = null;
		Core.PolyLine polyLine;
		static readonly Vector3 HORIZONTAL_PLANE = new Vector3(1, 0, 1);
		static readonly int layers = (1 << GameEngine.Layers.cvrTerrain | 1 << GameEngine.Layers.cvrBuilding);

		/*
		 * segments
		 * 0 1 2 3 4 5 6
		 * 
		 * 012 => first segment
		 * 234 => second segment
		 * 456 => third segment
		 * 
		 */

		#endregion

		// === Object-override =====================================================================================================================

		#region Object-override

		public override int GetHashCode()
		{
			return TagPair.GetHashCode();
		}

		public override bool Equals(object that)
		{
			bool equal = false;
			if (that is FlatMapPathDetails)
			{
				equal = ((FlatMapPathDetails)that).TagPair.Equals(this.TagPair);
			}
			return equal;
		}

		#endregion

		// === CONSTRUCTOR =====================================================================================================================

		#region CONSTRUCTOR

		static public FlatMapPathDetails MakeBezierQuadraticSplineFromControlPoints(Data.TagPair tag, Vector3 start, Vector3 end, float accuracy, Core.SplineSegment.RaycastHandlerFunction raycastHandlerFunction)
		{
			return new FlatMapPathDetails(tag, start, end, accuracy, raycastHandlerFunction);
		}

		static public FlatMapPathDetails MakeBezierQuadraticSplineFromControlPoints(Data.TagPair tag, Vector3 start, Vector3? mid, Vector3 end, float accuracy, Core.SplineSegment.RaycastHandlerFunction raycastHandlerFunction)
		{
			return new FlatMapPathDetails(tag, start, mid, end, accuracy, raycastHandlerFunction);
		}

		static public FlatMapPathDetails MakeBezierQuadraticSplineFromSegments(Data.TagPair tag, Vector3[] segments, float accuracy, Core.SplineSegment.RaycastHandlerFunction raycastHandlerFunction)
		{
			return new FlatMapPathDetails(tag, segments, accuracy, raycastHandlerFunction);
		}

		static public FlatMapPathDetails MakeBezierQuadraticSplineFromPolyline(Data.TagPair tag, Vector3[] polyline, float accuracy, Core.SplineSegment.RaycastHandlerFunction raycastHandlerFunction)
		{
			// fill curve
			Vector3[] segments = new Vector3[polyline.Length * 2 - 1];
			int j = 0;
			for (int i = 0; i < polyline.Length - 1; i += 1)
			{
				segments[j + 0] = polyline[i + 0];
				segments[j + 1] = (polyline[i + 0] + polyline[i + 1]) * 0.5f;
				j += 2;
			}
			// fill last one
			segments[j] = polyline[polyline.Length - 1];
			// make bezier quadratic
			return MakeBezierQuadraticSplineFromSegments(tag, segments, accuracy, raycastHandlerFunction);
		}
		 
		static public FlatMapPathDetails MakeBezierQuadraticSplineFromPathController(Data.TagPair tag, Vector3 start, Vector3? mid, Vector3 end, bool useNavMesh, float accuracy, Core.SplineSegment.RaycastHandlerFunction raycastHandlerFunction)
		{
			FlatMapPathDetails map = null;
			if (useNavMesh)
			{
				NavMeshPath path = new NavMeshPath();
				bool bFound = NavMesh.CalculatePath(start, end, NavMesh.AllAreas, path);
				if (bFound)
				{
					map = FlatMapPathDetails.MakeBezierQuadraticSplineFromPolyline(tag, path.corners, accuracy, raycastHandlerFunction);
				}
				else
				{
					map = FlatMapPathDetails.MakeBezierQuadraticSplineFromControlPoints(tag, start, mid, end, accuracy, raycastHandlerFunction);
				}
			}
			else
			{
				map = FlatMapPathDetails.MakeBezierQuadraticSplineFromControlPoints(tag, start, mid, end, accuracy, raycastHandlerFunction);
			}
			return map;
		}

		#endregion

		// === IMPLEMENTATION =====================================================================================================================

		#region IMPLEMENTATION

		protected FlatMapPathDetails(Data.TagPair tagPair, Vector3 start, Vector3 end, float accuracy, Core.SplineSegment.RaycastHandlerFunction raycastHandlerFunction)
		{
			sharedSplineControlPoints = new Vector3[3] { start, (start + end) * 0.5f, end };
			splineSegment = Core.SplineSegment.AllocateQuadraticSplineSegments(sharedSplineControlPoints);
			polyLine = Core.PolyLine.MakePolyLine(splineSegment, accuracy, raycastHandlerFunction);
			this.TagPair = tagPair;
		}

		protected FlatMapPathDetails(Data.TagPair tagPair, Vector3 start, Vector3? mid, Vector3 end, float accuracy, Core.SplineSegment.RaycastHandlerFunction raycastHandlerFunction)
		{
			sharedSplineControlPoints = new Vector3[3] { start, mid ?? (start + end) * 0.5f, end };
			splineSegment = Core.SplineSegment.AllocateQuadraticSplineSegments(sharedSplineControlPoints);
			polyLine = Core.PolyLine.MakePolyLine(splineSegment, accuracy, raycastHandlerFunction);
			this.TagPair = tagPair;
		}

		protected FlatMapPathDetails(Data.TagPair tagPair, Vector3[] segments, float accuracy, Core.SplineSegment.RaycastHandlerFunction raycastHandlerFunction)
		{
			sharedSplineControlPoints = segments;
			splineSegment = Core.SplineSegment.AllocateQuadraticSplineSegments(sharedSplineControlPoints);
			polyLine = Core.PolyLine.MakePolyLine(splineSegment, accuracy, raycastHandlerFunction);
			this.TagPair = tagPair;
		}

		Vector3 ComputePoint(int segment, float time)
		{
			return splineSegment[segment].ComputePoint(time);
		}

		Vector3 ComputeDerivative(int segment, float time)
		{
			return splineSegment[segment].ComputeDerivative(time);
		}

		#if UNITY_EDITOR
		void DrawCurveNode(Core.SplinePoint node, float size)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawSphere(node.point, size);
			Handles.color = Color.green;
			Handles.DrawLine(node.point, node.point + node.normal);
		}
		#endif

#endregion

		// === INTERFACE =====================================================================================================================

		#region INTERFACE

		public float  ComputeCurveMesh(float accuracy, Action<Core.SplinePoint> node, SplineSegment.RaycastHandlerFunction raycastHandlerFunc)
		{
			return SplineSegment.ComputeCurveMesh(splineSegment, node, accuracy, raycastHandlerFunc);
		}

		public float ComputeCurve(float accuracy, Action<Core.SplinePoint> node, SplineSegment.RaycastHandlerFunction raycastHandlerFunc)
		{
			return SplineSegment.ComputeCurve(splineSegment, node, accuracy, raycastHandlerFunc);
		}

		public void DrawPolyLine(float size, Color color)
		{
			polyLine.DrawPolyLine(size, color);
		}

		public void DrawAndComputeCurveMesh(float accuracy, float size, SplineSegment.RaycastHandlerFunction raycastHandlerFunc)
		{
			#if UNITY_EDITOR
			var restoreColor = GUI.color;

			// draw sphere
			{
				Gizmos.color = Color.red;
				ComputeCurveMesh(accuracy, node => DrawCurveNode(node, size), raycastHandlerFunc);
			}

			// draw line
			for (int i = 0; i < splineSegment.Length; ++i)
			{
				(Vector3 c0, Vector3 c1, Vector3 c2, Vector3 c3) = Core.Bezier.QuadraticToCubic(splineSegment[i].GetControlPoint(0), splineSegment[i].GetControlPoint(1), splineSegment[i].GetControlPoint(2));
				Handles.DrawBezier(c0, c3, c1, c2, Color.green, null, 2);
			}

			Gizmos.color = restoreColor;
			#endif
		}

		public void DrawAndComputeCurve(float accuracy, float size, SplineSegment.RaycastHandlerFunction raycastHandlerFunc)
		{
			#if UNITY_EDITOR
			// draw sphere
			{
				Gizmos.color = Color.red;
				ComputeCurve(accuracy, node => DrawCurveNode(node, size), raycastHandlerFunc);
			}

			// draw line
			for (int i = 0; i < splineSegment.Length; ++i)
			{
				(Vector3 c0, Vector3 c1, Vector3 c2, Vector3 c3) = Core.Bezier.QuadraticToCubic(splineSegment[i].GetControlPoint(0), splineSegment[i].GetControlPoint(1), splineSegment[i].GetControlPoint(2));
				Handles.DrawBezier(c0, c3, c1, c2, Color.green, null, 2);
			}
			#endif
		}

		// update position on path
		public (bool, Vector3, int) UpdateIteration(Vector3 position, int findNode, bool reverse)
		{
			// compute closest position
			float time = 0.0f;
			float lengthPathRemaining = 0.0f;
			int numSegment = 0;
			bool reached = false;
			(reached, time, numSegment, lengthPathRemaining, findNode) = polyLine.ComputeClosestPositionAndLengthToEnd(position, reverse, findNode);

			// if the unit has not reached the last node
			Vector3 characterDirection = Vector3.zero;
			if (reached == false)
			{
				// compute direction (derivative) of unit according to the current path
				var direction = ComputeDerivative(numSegment, time);
				// compute point 
				var point = ComputePoint(numSegment, time);

				// velocity accordint to speed and direction
				characterDirection = Vector3.Scale(reverse ? -direction : direction, HORIZONTAL_PLANE).normalized;

				// progressive catch up real pos / expected pos
				Vector3 differenceCharacterToPath = (point - position);
				var amount = 2.0f / lengthPathRemaining;
				characterDirection = characterDirection + Vector3.Scale(differenceCharacterToPath * amount, HORIZONTAL_PLANE);
			}
			return (reached, characterDirection, findNode);
		}

		public Vector3 LastPosition
		{
			get
			{
				return splineSegment[splineSegment.Length-1].LastPosition;
			}
		}

		public Core.PolyLine PolyLine
		{
			get
			{
				return polyLine;
			}
		}

		public Data.TagPair TagPair { get; }

		static public Vector3 RaycastHandler(Vector3 a)
		{
			RaycastHit hit;
			if (Physics.Raycast(a + Vector3.up * 15.0f, Vector3.down, out hit, Mathf.Infinity, layers) == true)
			{
				a = hit.point + Vector3.up * 0.1f;
			}
			return a;
		}

		// update position on path
		public (float, float) ComputeMinMaxDistance(Vector3 pos, int findNode = -1)
		{
			// compute closest position
			return polyLine.ComputeMinMaxDistance(pos, findNode);
		}

		#endregion
	};
}