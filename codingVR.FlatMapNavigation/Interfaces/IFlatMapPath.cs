﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.FlatMapNavigation
{
	public interface IFlatMapLinkObject
	{
		// nav mesh
		bool CanUseNavMesh { get; }

		// Path
		FlatMapNavigation.FlatMapPathDetails Path { get; }
		void RenewPath();
	}
}