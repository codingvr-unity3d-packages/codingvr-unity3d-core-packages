﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.FlatMapNavigation
{
	public interface IScreenPlayNavMeshLinkController
	{
		string TagFirst { get; set; }
		string TagSecond { get; set; }
		float Width { get; set; }
		bool Bidirectional { get; set; }
	}
}
