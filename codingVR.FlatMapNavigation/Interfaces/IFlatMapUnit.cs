﻿using System;
using UnityEngine;

namespace codingVR.FlatMapNavigation
{
	public interface IFlatMapUnit
	{
		// === Transform ===
		Transform UnitTransform { get; }

		// === Teleport ===
		void Teleport(Data.Tag tag);
		void Teleport(Data.TagPath path);

		// === Travel ===
		GameEngine.ICharacterMoving MoveOnPathBegin(bool reverse);
		FlatMapPathDetails PathToTravel { get; set; }
	};
}