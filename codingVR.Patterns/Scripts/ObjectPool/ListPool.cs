﻿using System.Collections.Generic;

namespace codingVR.Patterns
{
    public static class ListPool<T>
    {
        static Stack<List<T>> stack = new Stack<List<T>>();

        public static List<T> Get()
        {
            if (stack.Count > 0)
            {
                return stack.Pop();
            }
            return new List<T>();
        }

        public static void Add(List<T> list)
        {
            list.Clear();
            stack.Push(list);
        }
    }

    public interface ObjectBase
    {
        void Init();
    };

    public static class Object<T> where T : ObjectBase, new()
    {
        static Stack<T> stack = new Stack<T>();

        public static T Get()
        {
            if (stack.Count > 0)
            {
                return stack.Pop();
            }
            return new T();
        }

        public static void Add(T obj)
        {
            obj.Init();
            stack.Push(obj);
        }
    }


    public static class CollectionPool<Collection, T> where Collection : ICollection<T>, new()
    {
        static Stack<Collection> stack = new Stack<Collection>();

        public static Collection Get()
        {
            if (stack.Count > 0)
            {
                return stack.Pop();
            }
            return new Collection();
        }

        public static void Add(Collection list)
        {
            list.Clear();
            stack.Push(list);
        }
    }
}