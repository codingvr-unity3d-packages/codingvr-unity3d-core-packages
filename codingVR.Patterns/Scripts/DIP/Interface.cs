﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.Patterns
{
    public class Interface<INTERFACE> : IInitializable
    {
        static INTERFACE instance;

        static public INTERFACE Bind()
        {
            return instance;
        }

        static public void Unbind()
        {
            instance = default(INTERFACE);
        }

        [Inject]
        public Interface(INTERFACE interfaceObject)
        {
            Interface<INTERFACE>.instance = interfaceObject;
        }

        public void Initialize()
        {
            Debug.Log("Interface<" + typeof(INTERFACE) + "> container Initialized() !!!");
        }

		static public bool IsBound()
		{
			return Interface<INTERFACE>.instance == null ? false : true;
		}
    }
}
