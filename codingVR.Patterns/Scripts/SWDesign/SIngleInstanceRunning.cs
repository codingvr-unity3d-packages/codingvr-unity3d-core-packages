﻿using System.Collections;
using UnityEngine;

namespace codingVR.Patterns
{
	public class ISingleInstanceRunning<NAME>
	{
		static int countRunning = 0;

		public IEnumerator Lock()
		{
			yield return new WaitWhile(() => IsLocked() == true);
			countRunning++;
			yield return 0;
		}

		public bool IsLocked()
		{
			return countRunning > 0 ? true : false;
		}

		public void Unlock()
		{
			countRunning--;
		}
	}
}

