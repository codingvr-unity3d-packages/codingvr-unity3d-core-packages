﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.HexMapNavigation
{
	public interface IHexPlaceHolder
	{
		// === Place Holder type ===
		int Type { get; }
	};

	public static class HexPlaceHolder
	{
		public static void AddComponent(GameObject gameObject, int Type)
		{
			if (Type == 1)
			{
				gameObject.AddComponent<HexMapNavigation.HexGridObjectPlaceHolder>();
			}
			if (Type == 2)
			{
				gameObject.AddComponent<HexMapNavigation.HexGridObjectPlaceHolderPropsScene>();
			}
		}
	}
}
