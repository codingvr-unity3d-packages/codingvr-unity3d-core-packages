﻿namespace codingVR.HexMapNavigation
{
	using System.Collections;
	using UnityEngine;
	using TagManager = Data.TagCollectionManager<IHexGridManagerOrigin, HexCellLinked>;

	// === IHexMapGenerator ===================================================================================

	public interface IHexMapGenerator
	{
		// === hex grid creation ===

		bool GenerateMap(int x, int z, int chunkSizeX, int chunkSizeZ, bool wrapping);
	}

	// === IHexGridManagerOrigin ===============================================================================

	public interface IHexGridManagerOrigin : GameEngine.IMapManager
	{
		// === hex grid creation ===

		bool GenerateMap(int x, int z, int chunkSizeX, int chunkSizeZ, bool wrapping);

		// === hex grid creation ===

		bool CreateMap(int x, int z, int chunkSizeX, int chunkSizeZ, bool wrapping, bool emptyMap);

		// === close Map ===

		void CloseMap();

		// === hex cell prefab ===

		Object HexCellPrefab { get; set; }
		Object HexCellLabelPrefab { get; set; }
		Object HexChunkPrefab { get; set; }
		Object HexGridPrefab { get; set; }
		
		// === Linked cells ===

		bool LinkedHexRebuild { set; }
		bool PresentScreenPlayObjects { set; }
		bool LinkedHexInvalidDisplay { set; }

		// === Linked cell properties ===

		IHexCellLinkPropertiesFactory HexCellLinkPropertiesFactoryImpl { get;}

		// === Linked cells ===

		Data.TagPair SelectedHexCellLink { get; set; }
		IHexCellLinkProperties FindHexCellLinkProperties(Data.TagPair tagPair);
		TagManager.Event HexCellLinkOnSelectingEvent { get; }
		Data.TagPair ToggleSelectHexCellLink(Data.TagPair to);
		void RemoveSelectedLink();
		bool LinkHexCell(IHexCellOrigin from, IHexCellOrigin to);

		// === unit management ===

		// uneit move on preselected path    
		GameEngine.ICharacterMoving UnitMoveOnPathBegin(IHexGridUnit unit);
		// unit select path
		void UnitSelectNextPath(IHexGridUnit unit, Data.TagPair tagPair);
		void UnitSelectPath(IHexGridUnit unit, Data.TagPair tagPair);

		// === cells ===

		IHexCellOrigin GetCellUnderCursor();

		// === HexGrid ===

		IHexGrid HexGrid { get; }

		// === Load/Save ===

		void Save(Core.GameDataWriter writer);
		IEnumerator Load(Core.GameDataReader reader, int version, bool addressables, Core.Done done);

		// === Tag management ===

		void SetCellTag(IHexCellOrigin cell, Data.Tag tag);
		void ShowLinkedTags(bool invalidate);
	}
}