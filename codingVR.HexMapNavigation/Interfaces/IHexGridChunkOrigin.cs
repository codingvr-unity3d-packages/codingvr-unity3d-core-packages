﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.HexMapNavigation
{
	public enum HexEnvironmentManagerIDs
	{
		firstIdx = 0,
		PropsContainerIdx = firstIdx,
		FunctionsContainerIdx = 1,
		CharactersContainerIdx = 2,
		CountContainer,
	}

	public interface IHexGridChunkOrigin
	{
		/*
		 * 
		 * Injection
		 * 
		 */

		void Inject(DiContainer diContainer);
			
		
		/*
		 *
		 * Setup
		 * 
		 */

		void Setup(int chunkSizeX, int chunkSizeZ);

		/*
		 *
		 * Cell management
		 * 
		 */

		void AddCell(int index, IHexCellOrigin cell);

		/*
		 * 
		 * Refreshing chunk
		 * 
		 */

		void Refresh();

		/*
		 * 
		 * UI
		 * 
		 */

		void ShowUI(bool visible);

		/*
		 * 
		 * Load & Save
		 * 
		 */

		// Streaming
		void Save(Core.GameDataWriter writer);
		void Load(Core.GameDataReader reader, int version);

		/*
		 * 
		 * Environment
		 * 
		 */

		// make child of environment
		void MakeChildOfChunkEnvironement(HexEnvironmentManagerIDs containerIdx, Transform child);
		bool DestroyChildOfChunkEnvironement(HexEnvironmentManagerIDs containerIdx, string childID);
		Transform FindChildOfChunkEnvironement(HexEnvironmentManagerIDs containerIdx, string childID);
		List<Transform> FindChildsOfChunkEnvironement(HexEnvironmentManagerIDs containerIdx, string childFromParentID);
		List<Transform> FindChildsOfChunkEnvironement(HexEnvironmentManagerIDs containerIdx);


		/*
		 * 
		 * Rendering
		 * 
		 */

		Material TerrainMaterial { get; }
	};

	public interface IHexChunkFactory : IFactory<Transform, int, int, IHexGridChunkOrigin>
	{
	};
}