﻿using UnityEngine;

namespace codingVR.HexMapNavigation
{
	public interface IHexGridUnit
	{
		// === Transform ===
		Transform UnitTransform { get; }

		// === Path to travel ===
		void Teleport(Data.Tag tag);
		void Teleport(Data.TagPath path);
		void Teleport(IHexCellOrigin cell);
		GameEngine.ICharacterMoving MoveOnPathBegin(HexGridPathDetails path);
		HexGridPathDetails NextPathToTravel { get; set; }
		HexGridPathDetails PathToTravel { get; set; }
		HexGridPathDetails PreviousPathToTravel { get; }

		// === Cell ===
		IHexCellOrigin HexGridCellCurrent { get; }
	};
}