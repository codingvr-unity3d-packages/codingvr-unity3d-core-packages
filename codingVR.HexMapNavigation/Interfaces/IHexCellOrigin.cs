﻿using UnityEngine;

namespace codingVR.HexMapNavigation
{
	public interface IHexCellOrigin
	{
		/*
		 * 
		 * Coordinates
		 * 
		 */

		// hex coordinate
		HexCoordinates Coordinates { get; }

		// cells indexes
		int Index { get; }
		int ColumnIndex { get; }

		// position
		Vector3 LocalPosition { get; set; }
		Vector3 Position { get; }

		// Update new position
		void UpdateNewPosition(Vector3 newPosition);

		// Compute new local position accordint to elevation
		Vector3 ComputeNextLocalPosition(float elevation, float elevationStep, float elevationPerturbStrength);

		/*
		 * 
		 * Setup
		 * 
		 */

		void AddCellToChunk(IHexGridChunkOrigin chunk, Transform transformParent, Transform uiRectTransformParent);

		/*
		 * 
		 * Heuristsic Data
		 * 
		 */

		IHexCellHeuristicData HeuristicData { get; }

		/*
		 * 
		 * Tag Data
		 * 
		 */

		IHexCellTagData TagData { get; }

		/*
		 * 
		 * Unit Data
		 * 
		 */

		IHexCellUnitData UnitData { get; }

		/*
		 * 
		 * Neighbhors
		 * 
		 */

		// neighbors
		IHexCellOrigin GetNeighbor(HexDirection direction);
		void SetNeighbor(HexDirection direction, IHexCellOrigin cell);
		void SetNeighborCell(int index, IHexCellOrigin cell);
		int NeighborCount { get; }

		/*
		 * 
		 * UI
		 * 
		 */

		// Set label
		void SetLabel(string text);

		// highlight
		void DisableHighlight(Data.Tag tag);
		void EnableHighlight(Data.Tag tag, Color color);

		// Canvas
		void RefreshCanvasTextLabel();

		/*
		 * 
		 * Chunks
		 * 
		 */

		// parent chunk (owner of cell)
		IHexGridChunkOrigin Chunk { get; }
		// refresh chunk
		void RefreshChunkContainer();
		
		/*
		 * 
		 * States
		 * 
		 */

		// visibility and exploration status
		bool IsExplored { get; set; }
		bool Explorable { get; set; }
		float Range { get; }

		/*
		 * 
		 * Path constraints
		 * 
		 */

		bool IsValidDestination();
		int GetMoveCost(IHexCellOrigin fromCellOrigin, HexDirection direction);

		/*
		 * 
		 * Layout
		 * 
		 */

		void Refresh();
		
		
		/*
		 * 
		 * Load & Save
		 * 
		 */

		// Streaming
		void Save(Core.GameDataWriter writer);
		void Load(Core.GameDataReader reader, int version);
	};
}