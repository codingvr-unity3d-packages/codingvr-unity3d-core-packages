﻿using System.Collections.ObjectModel;
using UnityEngine;
using Zenject;

namespace codingVR.HexMapNavigation
{
	// === Hex cell linked constraints ===================================================

	public interface IHexCellLinkProperties
	{
		// Is a valid destination
		bool IsValidDestination(IHexCellOrigin cellOrigin);
		// waypoint or path
		bool Waypoints { get; set; }
		// Tag
		Data.Tag Tag { get; set; }

		// === Save / load ===

		void Save(Core.GameDataWriter writer);
	}

	// === Hex cell linked properties ===================================================

	public interface IHexCellLinkPropertiesFactory : IFactory<Data.Tag, IHexCellLinkProperties>
	{
		IHexCellLinkProperties Load(Core.GameDataReader reader, int version);
	}
}