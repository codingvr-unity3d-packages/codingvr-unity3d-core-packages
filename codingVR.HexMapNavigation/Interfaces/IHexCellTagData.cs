﻿	using UnityEngine.UI;

namespace codingVR.HexMapNavigation
{
	public interface IHexCellTagData
	{
		/* 
		 * 
		 * Tag
		 * 
		 */

		// tag
		Data.Tag Tag { get; set; }

		/*
		 * 
		 * UI
		 * 
		 */

		string RefreshCanvasTextLabel(Text label, string textLabel);

		/*
		 * 
		 * Load & Save
		 * 
		 */

		void Save(Core.GameDataWriter writer);
		void Load(Core.GameDataReader reader, int version);
	}
}