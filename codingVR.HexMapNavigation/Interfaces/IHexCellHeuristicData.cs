﻿using UnityEngine;

namespace codingVR.HexMapNavigation
{
	public interface IHexCellHeuristicData
	{
		/*
		 * 
		 * Path finding
		 * 
		 */

		// path finding & locality
		// we keep track distance during grid distance management
		// see HexGrid.Search HexGrid.GetVisibleCells
		// is a temporary information 
		int Distance { get; set; }
		IHexCellOrigin GetPathFrom(Data.TagPair tagPair);
		bool SetPathFrom(Data.TagPair tagPair, IHexCellOrigin cell);
		int SearchHeuristic { get; set; }
		int SearchPriority { get; }
		int SearchPhase { get; set; }
		IHexCellOrigin NextWithSamePriority { get; set; }
	}
}