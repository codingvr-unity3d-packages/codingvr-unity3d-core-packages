﻿using System.Collections.Generic;

namespace codingVR.HexMapNavigation
{
	public interface IHexGridNet
	{
		// === Hex grid path APIs ===
		// findpath from tagPair; however the correspoding cells are passed as parameter 
		void FindPath(Data.TagPair tagPair, IHexCellOrigin fromCell, IHexCellOrigin toCell, IHexGridUnit unit, IHexCellLinkProperties linkProperties, bool selected, bool invalidate);
		// findpath from tagPair; the correspoding cells are found internally
		void FindPath(IHexGridManagerOrigin hexGridManager, Data.TagPair tag, IHexGridUnit unit, IHexCellLinkProperties linkProperties, bool selected, bool invalidate);
		// clear path
		void ClearPath(Data.TagPair tagPair);
		void ClearAllPaths();
		// is the path valid
		bool IsValidPath(Data.TagPair tagPair);
		// get all cells from path
		HexGridPathDetails GetPathDetails(Data.TagPair tagPair);
		// Search phase status
		int SearchFrontierPhase { get; set; }
		// Cell Search Phase
		HexCellPriorityQueue SearchFrontier { get; }
	}
};