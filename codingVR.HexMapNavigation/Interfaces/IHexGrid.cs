﻿using UnityEngine;
using Zenject;

namespace codingVR.HexMapNavigation
{
	using System.Collections;
	using UnityEngine;

	// === Hex cell factory ===================================================

	public interface IHexCellFactory : IFactory<IHexCellLabelFactory, int, int, int, int, int, HexCellShaderData, IHexCellOrigin>
	{
	}

	// === Hex cell label factory ===================================================

	public interface IHexCellLabelFactory : IFactory<Vector2, RectTransform>
	{
	}
	
	// === Hex grid ============================================================

	public interface IHexGrid
	{
		// === hex grid creation ===

		bool CreateMap(IHexCellFactory hexCellFactory, IHexChunkFactory hexChunkFactory, IHexCellLabelFactory hexCellLabelFactory, int x, int z, int chunkSizeX, int chunkSizeZ, bool wrapping, bool centerMap);

		// === Find cell ===

		IHexCellOrigin FindCellFromTag(Data.Tag tag, bool bInvalidate);

		// === cells ===

		IHexCellOrigin GetCell(Vector3 position);
		IHexCellOrigin GetCell(Ray ray);
		IHexCellOrigin GetCell(Ray ray, float elevation, float elevationStep);
		RaycastHit? GetLastRaycastHit();
		IHexCellOrigin GetCell(int cellIndex);
		IHexCellOrigin GetCell(int xOffset, int zOffset);
		IHexCellOrigin GetCell(HexCoordinates coordinates);

		// === shader data ===

		HexCellShaderData ShaderData { get; }
		
		// === map ===

		void CenterMap(float xPosition);
		bool Wrapping { get; }

		// === Grid cell properties ===

		int CellCountX { get; }
		int CellCountZ { get; }

		// === chunks & columns ===
		
		IHexGridChunkOrigin GetGridChunk(Vector3 position);
		IHexGridChunkOrigin GetGridChunk(int index);
		int GetGridChunkCount();

		// === ui management ===

		void ShowUI(bool visible);

		// === hex grid net ===

		IHexGridNet Net { get; }

		// === load / save ===

		void Save(Core.GameDataWriter writer);
		void Load(IHexCellFactory hexCellFactory, IHexChunkFactory hexChunkFactory, IHexCellLabelFactory hexCellLabelFactory, Core.GameDataReader reader, int version, bool centerMap);
	}

	// === hex grid factory ============================================================

	public interface IHexGridFactory : IFactory<Transform, IHexGrid>
	{
	}
}
