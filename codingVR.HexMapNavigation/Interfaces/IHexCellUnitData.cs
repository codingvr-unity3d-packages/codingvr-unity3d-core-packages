﻿    using UnityEngine;

namespace codingVR.HexMapNavigation
{
    public interface IHexCellUnitData
    {
        /*
         * 
         * Unit
         * 
         */

        IHexGridUnit Unit { get; set; }
    }
}