﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace codingVR.HexMapNavigation
{
	public class HexGameUI : MonoBehaviour
	{
		// === mon ami poto theme ===

		string currentMonAmiPotoTheme;

		Data.TagPair playerPath = Data.TagPair.MakeFirst(Data.Tags.Player);

		public void SetMonAmiPotoTheme(string theme)
		{
			currentMonAmiPotoTheme = theme;
		}

		// === GameManager & Grid manager ===

		HexMapNavigation.IHexGridManagerOrigin hexGridManager;
		GameObject hexMapCamera;
		GameObject gameCamera;


		[Inject]
		public void Contruct(HexMapNavigation.IHexGridManagerOrigin hexGridManager)
		{
			this.hexGridManager = hexGridManager;
		}

		// === current cell management ===

		HexMapNavigation.IHexCellOrigin currentCell;

		bool UpdateCurrentCell()
		{
			var cell = hexGridManager.HexGrid.GetCell(Camera.main.ScreenPointToRay(Input.mousePosition));
			if (cell != currentCell)
			{
				currentCell = cell;
				return true;
			}
			return false;
		}

		// === Edit mode toogle ===

		public void SetEditMode(bool toggle)
		{
			hexGridManager.HexGrid.ShowUI(toggle);
			hexGridManager.HexGrid.Net.ClearAllPaths();
			hexGridManager.SelectedHexCellLink = null;
			hexGridManager.LinkedHexInvalidDisplay = true;
			Shader.EnableKeyword("HEX_MAP_EDIT_MODE");
			/*
			if (toggle)
			{
				Shader.EnableKeyword("HEX_MAP_EDIT_MODE");
			}
			else
			{
				Shader.DisableKeyword("HEX_MAP_EDIT_MODE");
			}
			*/

			// switch gameCamera / editor Camera
			hexMapCamera.SetActive(toggle);
			gameCamera.SetActive(!toggle);
	   }

		// === Start ===

		void Start()
		{
			// bind cameras
			hexMapCamera = global::GameCreator.Core.Hooks.HookHexMapCamera.Instance.gameObject;
			gameCamera = global::GameCreator.Core.Hooks.HookCamera.Instance.gameObject;
			hexMapCamera.SetActive(false);
			gameCamera.SetActive(true);

			// default theme edit
			var themeEdit = GameObject.Find("Theme Edit");
			if (themeEdit != null)
			{
				var inputField = themeEdit.GetComponent<InputField>();
				if (inputField != null)
				{
					inputField.text = "common";
					SetMonAmiPotoTheme(inputField.text);
				}
			}

			// enable game mode (and enable hex map war fog)
			SetEditMode(false);
		}

		// === GameManager UI lifecyle processing ===
		
		void Update()
		{
			// if the GUI has focus then we pass through GameManager UI
			// FIX: IsPointerOverGameObject is not compatible with GameCreator,
			// Instead EventSystem.current.currentSelectedGameObject does the job
			//see https://support.gamecreator.io/communities/1/topics/1121-ispointerovergameobject-every-time-true-when-gamecreatorcoreeventsystemmanagersingleton-is
			if (EventSystem.current != null)
			{
				if (EventSystem.current.currentSelectedGameObject == null)
				//if (!EventSystem.current.IsPointerOverGameObject())
				{
					if (Input.GetKeyDown(KeyCode.T))
					{
						DoSelection();
					}
					else if (selectedCell != null)
					{
						if (Input.GetMouseButtonDown(1))
						{
							DoPathfinding();
						}
					}
				}
			}
		}

		// === Path management ===

		HexMapNavigation.IHexCellOrigin selectedCell;

		void DoSelection()
		{
			UpdateCurrentCell();
			if (currentCell != null)
			{
				selectedCell = currentCell;
			}

			if (hexGridManager.SelectedHexCellLink == null)
			{
				hexGridManager.HexGrid.Net.ClearPath(playerPath);
			}
		}

		void DoPathfinding()
		{
			if (UpdateCurrentCell())
			{
				if (currentCell != null && selectedCell != null)
				{
					hexGridManager.HexGrid.Net.FindPath(playerPath, selectedCell, currentCell, null, null, true, true);
					hexGridManager.LinkedHexInvalidDisplay = true;
				}
				else
				{
					hexGridManager.HexGrid.Net.ClearPath(playerPath);
				}
			}
		}
	}
}