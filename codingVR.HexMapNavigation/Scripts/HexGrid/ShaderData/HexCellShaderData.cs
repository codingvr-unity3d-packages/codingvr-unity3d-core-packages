﻿using System.Collections.Generic;
using UnityEngine;

namespace codingVR.HexMapNavigation
{
	public class HexCellShaderData : MonoBehaviour
	{
		const float transitionSpeed = 255f * 0.1f;

		Texture2D cellTexture;
		Color32[] cellTextureData;

		List<IHexCellOrigin> transitioningCells = new List<IHexCellOrigin>();

		public bool ImmediateMode { get; set; }

		public void Initialize(int x, int z)
		{
			if (cellTexture)
			{
				cellTexture.Resize(x, z);
			}
			else
			{
				cellTexture = new Texture2D(x, z, TextureFormat.RGBA32, false, true);
				cellTexture.filterMode = FilterMode.Point;
				cellTexture.wrapModeU = TextureWrapMode.Repeat;
				cellTexture.wrapModeV = TextureWrapMode.Clamp;
				Shader.SetGlobalTexture("_HexCellData", cellTexture);
			}
			Shader.SetGlobalVector("_HexCellData_TexelSize", new Vector4(1f / x, 1f / z, x, z));
			if (cellTextureData == null || cellTextureData.Length != x * z)
			{
				cellTextureData = new Color32[x * z];
			}
			else
			{
				for (int i = 0; i < cellTextureData.Length; i++)
				{
					cellTextureData[i] = new Color32(0, 0, 0, 0);
				}
			}
			transitioningCells.Clear();
			enabled = true;
		}

		public void RefreshTerrain(HexMapNavigation.IHexCellOrigin cell, int terrainTypeIndex)
		{
			cellTextureData[cell.Index].a = (byte)terrainTypeIndex;
			enabled = true;
		}

		public void RefreshVisibility(HexMapNavigation.IHexCellOrigin cell)
		{
			int index = cell.Index;
			if (ImmediateMode)
			{
				cellTextureData[index].g = cell.IsExplored ? (byte)255 : (byte)0;
			}
			else
			{
				if (cell.IsExplored && cellTextureData[index].g != 255)
				{
					transitioningCells.Add(cell);
				}
			}
			enabled = true;
		}

		public void ViewElevationChanged()
		{
			enabled = true;
		}

		void LateUpdate()
		{
			int delta = (int)(Time.deltaTime * transitionSpeed);
			if (delta == 0)
			{
				delta = 1;
			}
			for (int i = 0; i < transitioningCells.Count; i++)
			{
				if (!UpdateCellData(transitioningCells[i], delta))
				{
					transitioningCells[i--] = transitioningCells[transitioningCells.Count - 1];
					transitioningCells.RemoveAt(transitioningCells.Count - 1);
				}
			}
			if (cellTexture != null)
			{
				cellTexture.SetPixels32(cellTextureData);
				cellTexture.Apply();
			}
			enabled = transitioningCells.Count > 0;
		}

		bool UpdateCellData(HexMapNavigation.IHexCellOrigin cell, int delta)
		{
			int index = cell.Index;
			Color32 data = cellTextureData[index];
			bool stillUpdating = false;

			if (cell.IsExplored && data.g < 255)
			{
				stillUpdating = true;
				int t = data.g + delta;
				data.g = t >= 255 ? (byte)255 : (byte)t;
			}

			cellTextureData[index] = data;
			return stillUpdating;
		}
	}
}