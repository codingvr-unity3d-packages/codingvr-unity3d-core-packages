﻿using System.Collections.Generic;

namespace codingVR.HexMapNavigation
{
	public class HexGridNet : IHexGridNet
	{
		public HexGridNet()
		{ }

		// === Tools for tracking search phase across frontier for each cell ===

		// Priority Queue
		/*
			3.1 Cell Search Phase
			https://catlikecoding.com/unity/tutorials/hex-map/part-17/
		*/

		HexCellPriorityQueue searchFrontier;
		public HexCellPriorityQueue SearchFrontier
		{
			get
			{
				if (searchFrontier == null)
					searchFrontier = new HexCellPriorityQueue();
				return searchFrontier;
			}
		}

		// Search phase status
		// See
		/*
			3.2 Entering the Frontier
			https://catlikecoding.com/unity/tutorials/hex-map/part-17/
			In HexGrid.Search, we could reset all cells to 0 and always use 1 for the frontier. 
			Or we could increment the frontier number for each new search.
			That way, we do not have to bother with resetting the cells, as long as we increment the frontier by two each time.
		*/
		int searchFrontierPhase;

		public int SearchFrontierPhase
		{
			get { return searchFrontierPhase; }
			set { searchFrontierPhase = value; }
		}

		// === Hex grid path collection ===

		Dictionary<Data.TagPair, HexGridPath> currentPaths = new Dictionary<Data.TagPair, HexGridPath>();

		public HexGridPath GetHexGridPath(Data.TagPair tagPair)
		{
			if (currentPaths.ContainsKey(tagPair) == false)
			{
				// TODO: pools    
				currentPaths.Add(tagPair, new HexGridPath(tagPair));

			}
			return currentPaths[tagPair];
		}


		public Dictionary<Data.TagPair, HexGridPath> CurrentPaths
		{
			get { return currentPaths; }
		}

		// === Hex grid path APIs ===

		public void FindPath(Data.TagPair tag, IHexCellOrigin fromCell, IHexCellOrigin toCell, IHexGridUnit unit, IHexCellLinkProperties linkProperties, bool selected, bool invalidate)
		{
			HexGridPath gridPath = GetHexGridPath(tag);
			gridPath.FindPath(this, fromCell, toCell, unit, linkProperties, selected, invalidate);
		}

		public void FindPath(IHexGridManagerOrigin hexGridManager, Data.TagPair tag, IHexGridUnit unit, IHexCellLinkProperties linkProperties, bool selected, bool invalidate)
		{
			HexGridPath gridPath = GetHexGridPath(tag);
			gridPath.FindPath(hexGridManager, this, unit, linkProperties, selected, invalidate);
		}

		public void ClearPath(Data.TagPair tagPair)
		{
			HexGridPath gridPath = GetHexGridPath(tagPair);
			gridPath.ClearPath();
		}

		public void ClearAllPaths()
		{
			foreach (var it in currentPaths)
			{
				ClearPath(it.Key);
			}
		}

		public bool IsValidPath(Data.TagPair tagPair)
		{
			HexGridPath gridPath = GetHexGridPath(tagPair);
			return gridPath.IsValid;
		}

		public HexGridPathDetails GetPathDetails(Data.TagPair tagPair)
		{
			HexGridPath gridPath = GetHexGridPath(tagPair);
			return gridPath.GetPathDetails();
		}

		// === Visibility APIs ===

		public List<IHexCellOrigin> GetVisibleCells(IHexCellOrigin fromCell, float range)
		{
			List<IHexCellOrigin> visibleCells = Patterns.ListPool<IHexCellOrigin>.Get();

			searchFrontierPhase += 2;
			if (searchFrontier == null)
			{
				searchFrontier = new HexCellPriorityQueue();
			}
			else
			{
				searchFrontier.Clear();
			}

			range += fromCell.Range;
			fromCell.HeuristicData.SearchPhase = searchFrontierPhase;
			fromCell.HeuristicData.Distance = 0;
			searchFrontier.Enqueue(fromCell);
			HexCoordinates fromCoordinates = fromCell.Coordinates;
			while (searchFrontier.Count > 0)
			{
				IHexCellOrigin current = searchFrontier.Dequeue();
				current.HeuristicData.SearchPhase += 1;
				visibleCells.Add(current);

				for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
				{
					IHexCellOrigin neighbor = current.GetNeighbor(d);
					if (
						neighbor == null ||
						neighbor.HeuristicData.SearchPhase > searchFrontierPhase ||
						!neighbor.Explorable
					)
					{
						continue;
					}

					int distance = current.HeuristicData.Distance + 1;
					if (distance + neighbor.Range > range ||
						distance > fromCoordinates.DistanceTo(neighbor.Coordinates)
					)
					{
						continue;
					}

					if (neighbor.HeuristicData.SearchPhase < searchFrontierPhase)
					{
						neighbor.HeuristicData.SearchPhase = searchFrontierPhase;
						neighbor.HeuristicData.Distance = distance;
						neighbor.HeuristicData.SearchHeuristic = 0;
						searchFrontier.Enqueue(neighbor);
					}
					else if (distance < neighbor.HeuristicData.Distance)
					{
						int oldPriority = neighbor.HeuristicData.SearchPriority;
						neighbor.HeuristicData.Distance = distance;
						searchFrontier.Change(neighbor, oldPriority);
					}
				}
			}
			return visibleCells;
		}
	}
}