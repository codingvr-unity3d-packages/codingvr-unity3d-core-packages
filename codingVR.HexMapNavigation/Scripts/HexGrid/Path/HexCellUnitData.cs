﻿using UnityEngine;
using UnityEngine.UI;

namespace codingVR.HexMapNavigation
{
    class HexCellUnitData : IHexCellUnitData
    {
        IHexCellOrigin cell;

        public HexCellUnitData(IHexCellOrigin cell)
        {
            this.cell = cell;
        }


        public HexMapNavigation.IHexGridUnit Unit { get; set; }
    }
}