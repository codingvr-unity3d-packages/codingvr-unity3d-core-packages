﻿using System.Collections.Generic;

namespace codingVR.HexMapNavigation
{
    // === HexCellPathFrom =======================================================================

    public class HexCellPathFrom
    {
        Dictionary<Data.TagPair, IHexCellOrigin> dictionay = new Dictionary<Data.TagPair, IHexCellOrigin>();

        public bool Contains(Data.TagPair tagPair)
        {
            return dictionay.ContainsKey(tagPair);
        }

        public bool Set(Data.TagPair tagPair, IHexCellOrigin cell)
        {
            bool b = true;
            if (dictionay.ContainsKey(tagPair) == false)
            {
                dictionay.Add(tagPair, cell);
            }
            else
            {
                dictionay.Remove(tagPair);
                dictionay.Add(tagPair, cell);
                b = false;
            }
            return b;
        }

        public IHexCellOrigin Get(Data.TagPair tagPair)
        {
            return dictionay[tagPair];
        }

        public Dictionary<Data.TagPair, IHexCellOrigin>.ValueCollection Values => dictionay.Values;
    }
}