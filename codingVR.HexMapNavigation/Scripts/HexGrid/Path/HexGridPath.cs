﻿using UnityEngine;
using Zenject;

namespace codingVR.HexMapNavigation
{
	// === HexGridPath =======================================================================

	public class HexGridPath
	{
		IHexCellOrigin currentPathFrom, currentPathTo;
		Data.TagPair tagPair;
		HexGridPathDetails cachedPathDetails = null;
			   
		// === ctor ===

		public HexGridPath(Data.TagPair tagPair)
		{
			this.tagPair = tagPair;
		}

		// === path exists ===

		bool currentPathExists;

		public bool IsValid
		{
			get
			{
				return currentPathExists;
			}
		}

		// === return path ===

		public HexGridPathDetails GetPathDetails()
		{
			if (!currentPathExists)
			{
				return null;
			}

			if (cachedPathDetails == null)
			{
				HexGridPathDetails path = Patterns.CollectionPool<HexGridPathDetails, IHexCellOrigin>.Get();
				path.TagPair = this.tagPair;
				// TODO: use pools
				for (IHexCellOrigin c = currentPathTo; c != currentPathFrom; c = c.HeuristicData.GetPathFrom(tagPair))
				{
					path.Add(c);
				}
				path.Add(currentPathFrom);
				path.Reverse();

				cachedPathDetails = path;
			}

			return cachedPathDetails;
		}

		// === clear path ===

		public void ClearPath()
		{
			if (currentPathExists)
			{
				IHexCellOrigin current = currentPathTo;
				while (current != currentPathFrom)
				{
					current.SetLabel(null);
					current.DisableHighlight(tagPair.Second);
					// TODO: use pools
					current = current.HeuristicData.GetPathFrom(tagPair);
				}
				current.DisableHighlight(tagPair.Second);
				currentPathExists = false;
			}
			else if (currentPathFrom != null)
			{
				currentPathFrom.DisableHighlight(tagPair.Second);
				currentPathTo.DisableHighlight(tagPair.Second);
			}
			currentPathFrom = currentPathTo = null;
			cachedPathDetails = null;
		}

		// === show path ===

		public void ShowPath(IHexGridUnit unit, bool selected, bool invalidate)
		{
			if (currentPathExists)
			{
				int speed = 1;
				/*
				if (unit != null)
				{
					speed = (int)unit.Speed;
				}
				*/

				IHexCellOrigin current = currentPathTo;
				while (current != currentPathFrom)
				{
					// label will be displayed only when the invalidate flag is true
					// SO the distance from cell is a temporary information and it can be updated at any time for any path, 
					// it will be not usable for this usecase
					if (invalidate == true)
					{
						int turn = (current.HeuristicData.Distance - 1) / speed;
						current.SetLabel(turn.ToString());
					}
					Color color = selected == true ? Color.white : Color.black;
					current.EnableHighlight(tagPair.Second, color);
					current = current.HeuristicData.GetPathFrom(tagPair);
				}
			}
			currentPathFrom.EnableHighlight(tagPair.Second, Color.red);
			currentPathTo.EnableHighlight(tagPair.Second, Color.red);
		}

		// === find path ===

		bool selectedState = false;

		public void FindPath(IHexGridNet net, IHexCellOrigin fromCell, IHexCellOrigin toCell, IHexGridUnit unit, IHexCellLinkProperties linkProperties, bool selected, bool invalidate)
		{
			if (currentPathTo != toCell || currentPathFrom != fromCell || invalidate == true || selectedState != selected)
			{
				ClearPath();
				selectedState = selected;
				currentPathFrom = fromCell;
				currentPathTo = toCell;
				if (linkProperties.Waypoints == true)
					currentPathExists = SetWaypoints(net, fromCell, toCell, unit, linkProperties);
				else
					currentPathExists = SearchPath(net, fromCell, toCell, unit, linkProperties);
				ShowPath(unit, selected, invalidate);
			}
		}

		public void FindPath(IHexGridManagerOrigin hexGridManager, IHexGridNet net, IHexGridUnit unit, IHexCellLinkProperties linkProperties, bool selected, bool invalidate)
		{
			IHexCellOrigin from = hexGridManager.HexGrid.FindCellFromTag(tagPair.First, invalidate);
			IHexCellOrigin to = hexGridManager.HexGrid.FindCellFromTag(tagPair.Second, invalidate);
			FindPath(net, from, to, unit, linkProperties, selected, invalidate);
		}

		// == Search ===

		bool SearchPath(IHexGridNet net, IHexCellOrigin fromCell, IHexCellOrigin toCell, IHexGridUnit unit, IHexCellLinkProperties linkProperties)
		{
			int speed = 1;

			net.SearchFrontierPhase += 2;
			net.SearchFrontier.Clear();

			fromCell.HeuristicData.SearchPhase = net.SearchFrontierPhase;
			fromCell.HeuristicData.Distance = 0;
			net.SearchFrontier.Enqueue(fromCell);
			while (net.SearchFrontier.Count > 0)
			{
				IHexCellOrigin current = net.SearchFrontier.Dequeue();
				current.HeuristicData.SearchPhase += 1;

				// we are arrived to path
				if (current == toCell)
				{
					return true;
				}

				int currentTurn = (current.HeuristicData.Distance - 1) / speed;

				for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
				{
					IHexCellOrigin neighbor = current.GetNeighbor(d);
					if (neighbor == null || neighbor.HeuristicData.SearchPhase > net.SearchFrontierPhase)
					{
						continue;
					}

					int moveCost = 0;
					if (unit != null)
					{
						if (!neighbor.IsValidDestination())
						{
							continue;
						}

						moveCost = neighbor.GetMoveCost(current, d);
						if (moveCost < 0)
						{
							continue;
						}
					}

					if (linkProperties != null)
					{
						if (!linkProperties.IsValidDestination(neighbor))
						{
							continue;
						}
					}

					int distance = current.HeuristicData.Distance + moveCost;
					int turn = (distance - 1) / speed;
					if (turn > currentTurn)
					{
						distance = turn * speed + moveCost;
					}

					if (neighbor.HeuristicData.SearchPhase < net.SearchFrontierPhase)
					{
						neighbor.HeuristicData.SearchPhase = net.SearchFrontierPhase;
						neighbor.HeuristicData.Distance = distance;
						// TODO: use pools
						neighbor.HeuristicData.SetPathFrom(tagPair, current);
						neighbor.HeuristicData.SearchHeuristic = neighbor.Coordinates.DistanceTo(toCell.Coordinates);
						net.SearchFrontier.Enqueue(neighbor);
					}
					else if (distance < neighbor.HeuristicData.Distance)
					{
						int oldPriority = neighbor.HeuristicData.SearchPriority;
						neighbor.HeuristicData.Distance = distance;
						// TODO: use pools
						neighbor.HeuristicData.SetPathFrom(tagPair, current);
						net.SearchFrontier.Change(neighbor, oldPriority);
					}
				}
			}
			return false;
		}
				
		bool SetWaypoints(IHexGridNet net, IHexCellOrigin fromCell, IHexCellOrigin toCell, IHexGridUnit unit, IHexCellLinkProperties linkProperties)
		{
			toCell.HeuristicData.SetPathFrom(tagPair, fromCell);
			return true;
		}
	}
}