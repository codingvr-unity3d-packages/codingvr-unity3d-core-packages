﻿using System.Collections.Generic;

namespace codingVR.HexMapNavigation
{
	public class HexCellPriorityQueue
	{
		List<IHexCellOrigin> list = new List<IHexCellOrigin>();

		int count = 0;
		int minimum = int.MaxValue;

		public int Count
		{
			get
			{
				return count;
			}
		}

		public void Enqueue(IHexCellOrigin cell)
		{
			count += 1;
			int priority = cell.HeuristicData.SearchPriority;
			if (priority < minimum)
			{
				minimum = priority;
			}
			while (priority >= list.Count)
			{
				list.Add(null);
			}
			cell.HeuristicData.NextWithSamePriority = list[priority];
			list[priority] = cell;
		}

		public IHexCellOrigin Dequeue()
		{
			count -= 1;
			for (; minimum < list.Count; minimum++)
			{
				IHexCellOrigin cell = list[minimum];
				if (cell != null)
				{
					list[minimum] = cell.HeuristicData.NextWithSamePriority;
					return cell;
				}
			}
			return null;
		}

		public void Change(IHexCellOrigin cell, int oldPriority)
		{
			IHexCellOrigin current = list[oldPriority];
			IHexCellOrigin next = current.HeuristicData.NextWithSamePriority;
			if (current == cell)
			{
				list[oldPriority] = next;
			}
			else
			{
				while (next != cell)
				{
					current = next;
					next = current.HeuristicData.NextWithSamePriority;
				}
				current.HeuristicData.NextWithSamePriority = cell.HeuristicData.NextWithSamePriority;
			}
			Enqueue(cell);
			count -= 1;
		}

		public void Clear()
		{
			list.Clear();
			count = 0;
			minimum = int.MaxValue;
		}
	}
}