﻿using System.Collections.ObjectModel;
using UnityEngine;

namespace codingVR.HexMapNavigation
{
	// === Hex cell linked implementation ===============================================================

	public class HexCellLinked : Data.TagController<HexCellLinked>
	{
		public class Links : KeyedCollection<Data.Tag, IHexCellLinkProperties>
		{
			protected override Data.Tag GetKeyForItem(IHexCellLinkProperties item)
			{
				return item.Tag;
			}

			public bool ReplaceKey(Data.Tag from, Data.Tag by)
			{
				bool b = false;
				if (Contains(from))
				{
					// replace key by a new key
					// TODO : Hack : KeyedCollection fails to replace automatically keys
					var v = this[from];
					Remove(from);
					v.Tag = by;
					Add(v);
					b = true;
				}
				return b;
			}
		}

		Links linkedTags = new Links();
		IHexGridManagerOrigin hexGridManager;

		// === Dip Constructor ===

		public HexCellLinked()
		{
		}

		public HexCellLinked(IHexGridManagerOrigin hexGridManager)
		{
			this.hexGridManager = hexGridManager;
		}

		public HexCellLinked(IHexGridManagerOrigin hexGrid, IHexCellOrigin cell, Data.Tag tag) : base(tag)
		{
			this.hexGridManager = hexGrid;

			IHexCellOrigin previousCell = hexGrid.HexGrid.FindCellFromTag(tag, true);
			if (previousCell != null)
			{
				previousCell.TagData.Tag = null;
			}
			cell.TagData.Tag = tag;
			RebuildPath();
		}

		// === Delegate ===

		// called by the manager via TagController
		public override void OnAdded()
		{
			base.OnAdded();
			IHexCellOrigin cell = hexGridManager.HexGrid.FindCellFromTag(Tag, true);
			if (cell != null)
			{
				cell.TagData.Tag = Tag;
				RefreshPath();
			}
		}

		public override void OnMoved(HexCellLinked previousData)
		{
			base.OnMoved(previousData);
			IHexCellOrigin cell = hexGridManager.HexGrid.FindCellFromTag(Tag, true);
			if (cell != null)
			{
				cell.TagData.Tag = Tag;
				linkedTags = previousData.linkedTags;
				RebuildPath();
			}
		}

		public override void OnRemoved()
		{
			base.OnRemoved();
			IHexCellOrigin cell = hexGridManager.HexGrid.FindCellFromTag(Tag, true);
			if (cell != null)
			{
				cell.TagData.Tag = null;
				RebuildPath();
			}
		}

		public override void OnRemovedLink(Data.Tag linkedTag)
		{
			if (linkedTags != null)
			{
				linkedTags.Remove(linkedTag);
				RebuildPath();
			}
		}

		public override bool OnReplaced(Data.Tag from, Data.Tag to)
		{
			bool b = false;
			if (linkedTags != null)
			{
				if (linkedTags.Contains(from))
				{
					// replace key by a new key
					if (linkedTags.ReplaceKey(from, to) == true)
					{
						b = true;
					}
				}
			}
			if (Tag.Equals(from))
			{
				IHexCellOrigin cell = hexGridManager.HexGrid.FindCellFromTag(Tag, true);
				Tag = to;
				cell.TagData.Tag = Tag;
				b = true;
			}

			if (b)
			{
				RebuildPath();
			}

			return b;
		}

		// === Path Grid ===

		public void RebuildPath()
		{
			hexGridManager.PresentScreenPlayObjects = true; 
			hexGridManager.LinkedHexRebuild = true;
		}

		public void RefreshPath()
		{
			hexGridManager.PresentScreenPlayObjects = true;
			hexGridManager.LinkedHexInvalidDisplay = true;
		}

		// === linked tag to cell ===

		public Data.Tag LinkedTag(int i)
		{
			if (null != linkedTags)
			{
				return linkedTags[i].Tag;
			}
			return null;
		}

		public IHexCellLinkProperties LinkedTagProperties(int i)
		{
			if (null != linkedTags)
			{
				return linkedTags[i];
			}
			return null;
		}

		public int LinkedTagsCount()
		{
			int count = 0;
			if (linkedTags != null)
			{
				count = linkedTags.Count;
			}
			return count;
		}

		public bool LinkedTagAdd(Data.Tag linkedTag)
		{
			bool bReturn = false;

			if (null == linkedTags)
			{
				linkedTags = new Links();
			}

			if (null != linkedTags)
			{
				// check if the linked Tag has not been already inserted yet
				var found = linkedTags.Contains(linkedTag);
				if (found == false && Tag != linkedTag)
				{
					linkedTags.Add(hexGridManager.HexCellLinkPropertiesFactoryImpl.Create(linkedTag));
					RebuildPath();
					bReturn = true;
				}
				else
				{
					Debug.LogErrorFormat("{0} : can't link tag #{1} to tag #{2} ", System.Reflection.MethodBase.GetCurrentMethod().Name, linkedTag.TagName, Tag.TagName);
					
				}
			}
			return bReturn;
		}
				
		public IHexCellLinkProperties FindLinkProperties(Data.Tag tag)
		{
			IHexCellLinkProperties link = null;
			var found = linkedTags.Contains(tag);
			if (found == true)
			{
				link = linkedTags[tag];
			}
			return link;
		}

		public bool ContainsLink(Data.Tag tag)
		{
			return linkedTags.Contains(tag); 
		}

		// === Save / load ===

		public override void Save(Core.GameDataWriter writer)
		{
			writer.WriteObjectStart("LinkSet");
			if (linkedTags != null)
			{
				writer.Write(linkedTags.Count, "count");
				foreach (var it in linkedTags)
				{
					it.Save(writer);
				}
			}
			else
			{
				writer.Write(0);
			}
			writer.WriteObjectEnd();
		}

		public override void Load(Core.GameDataReader reader, Data.Tag tag, int version)
		{
			this.Tag = tag;
			if (version >= 8)
			{
				reader.ReadObjectStart("LinkSet");
				var count = reader.ReadInt();
				if (count > 0)
				{
					linkedTags = new Links();

					for (int i = 0; i < count; ++i)
					{
					   var hexCellSingleLink = hexGridManager.HexCellLinkPropertiesFactoryImpl.Load(reader, version);
						var found = linkedTags.Contains(hexCellSingleLink.Tag);
						if (false == found)
						{
							linkedTags.Add(hexCellSingleLink);
						}
						else
						{
							Debug.LogErrorFormat("{0} : linked tag '#{1}' to root tag #{2} has been already inserted", System.Reflection.MethodBase.GetCurrentMethod().Name, hexCellSingleLink.Tag.TagName, tag.TagName);
						}
					}
					RefreshPath();
				}
				reader.ReadObjectEnd();
			}
			else
			{
				var count = reader.ReadInt();
				if (count > 0)
				{
					linkedTags = new Links();

					for (int i = 0; i < count; ++i)
					{
						var linkedTag = Data.Tag.Load(reader, version);
						var found = linkedTags.Contains(linkedTag);
						if (false == found)
						{
							linkedTags.Add(hexGridManager.HexCellLinkPropertiesFactoryImpl.Create(linkedTag));
						}
						else
						{
							Debug.LogErrorFormat("{0} : linked tag '#{1}' to root tag #{2} has been already inserted", System.Reflection.MethodBase.GetCurrentMethod().Name, linkedTag.TagName, tag.TagName);
						}
					}
					RefreshPath();
				}
			}
		}
	}
}