﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.HexMapNavigation
{
	// === HexGridPathDetails ====================================================================

	public class HexGridPathDetails : List<IHexCellOrigin>
	{
		public Data.TagPair TagPair { get; set; }

		public HexGridPathDetails()
		{
		}

		public override int GetHashCode()
		{
			return TagPair.GetHashCode();
		}

		public override bool Equals(object that)
		{
			bool equal = false;
			if (that is HexGridPathDetails)
			{
				equal = ((HexGridPathDetails)that).TagPair.Equals(this.TagPair);
			}
			return equal;
		}

		public static void ComputeCurve(HexGridPathDetails prevPath, HexGridPathDetails path, HexGridPathDetails nextPath, float accuracy, Action<Vector3> node)
		{
			Vector3 a, b, c;

			if (prevPath != null)
			{
				c = (prevPath[prevPath.Count - 2].Position + path[0].Position) * 0.5f;
			}
			else
			{
				c = path[0].Position;
			}

			for (int i = 1; i < path.Count; i++)
			{
				a = c;
				b = path[i - 1].Position;
				c = (path[i - 1].Position + path[i].Position) * 0.5f;
				for (float t = 0f; t < 1f; t += accuracy)
				{
					node(Core.Bezier.GetPoint(a, b, c, t));
				}
			}

			if (nextPath == null)
			{
				a = c;
				b = path[path.Count - 1].Position;
				c = b;
				for (float t = 0f; t < 1f; t += accuracy)
				{
					node(Core.Bezier.GetPoint(a, b, c, t));
				}
			}
		}
	};
}