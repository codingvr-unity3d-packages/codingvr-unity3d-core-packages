﻿using UnityEngine;
using UnityEngine.UI;

namespace codingVR.HexMapNavigation
{
	class HexCellHeuristicData : IHexCellHeuristicData
	{
		IHexCellOrigin cell;

		public HexCellHeuristicData(IHexCellOrigin cell)
		{
			this.cell = cell;
		}

		/*
         * 
         * Path finding
         * 
         */

		// path finding & locality
		int distance;

		// we keep track distance during grid distance management
		// see HexGrid.Search HexGrid.GetVisibleCells
		// is a temporary information 
		public int Distance
		{
			get
			{
				return distance;
			}
			set
			{
				distance = value;
			}
		}

		private HexCellPathFrom pathFrom = new HexCellPathFrom();

		public IHexCellOrigin GetPathFrom(Data.TagPair tagPair)
		{
			return pathFrom.Get(tagPair);
		}

		public bool SetPathFrom(Data.TagPair tagPair, IHexCellOrigin cell)
		{
			return pathFrom.Set(tagPair, cell);
		}

		public void DrawGizmos(Color color)
		{
			foreach (var v in pathFrom.Values)
			{
				Core.DrawArrow.ForGizmoLine(v.Position, cell.Position, color);
			}
		}

		public int SearchHeuristic { get; set; }

		public int SearchPriority
		{
			get
			{
				return distance + SearchHeuristic;
			}
		}

		/*
            3.1 Cell Search Phase
            https://catlikecoding.com/unity/tutorials/hex-map/part-17/
            How do we know whether a cell has already exited the frontier? 
            Currently, we cannot determine this. So we have to keep track of which phase of the search a cell is in. 
            It's either not yet in the frontier, currently part of the frontier, or behind the frontier. We can track this by adding a simple integer property to IHexCell.
            For example, 0 means the cell has not yet been reached, 1 indicated that the cell is currently in the frontier, while 2 means it has been taken out of the frontier.
        */
		public int SearchPhase { get; set; }

		public IHexCellOrigin NextWithSamePriority { get; set; }
	}
}