﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace codingVR.HexMapNavigation
{
	// === HexUnitMoveOnPath ====================================================================

	public class HexUnitMoveOnPath : GameEngine.ICharacterMoving
	{
		static readonly Vector3 HORIZONTAL_PLANE = new Vector3(1, 0, 1);
		static readonly int countPointByJunction = 10;
		List<Vector3> curvedPath = null;
		IHexGridUnit hexUnit;

		// constructor
		HexUnitMoveOnPath(IHexGridUnit hexUnit)
		{
			this.hexUnit = hexUnit;

			// create curved path
			curvedPath = new List<Vector3>((this.hexUnit.PathToTravel.Count - 1) * countPointByJunction);
			float bezierSpeed = 1.0f / (float)countPointByJunction;
			HexGridPathDetails.ComputeCurve(this.hexUnit.PreviousPathToTravel, this.hexUnit.PathToTravel, this.hexUnit.NextPathToTravel, bezierSpeed, node => curvedPath.Add(node));
			//HexGridPathDetails.ComputeCurve(null, this.hexUnit.PathToTravel, null, bezierSpeed, node => curvedPath.Add(node));
		}

		// destructor
		~HexUnitMoveOnPath()  // finalizer
		{
		}

		// create curved path
		static public GameEngine.ICharacterMoving Create(IHexGridUnit hexUnit)
		{
			return new HexUnitMoveOnPath(hexUnit);
		}

		// returned curved path
		public List<Vector3> CurvedPath => curvedPath;

		// update position on path
		public (bool, Vector3, int) UpdateIteration(int findNode)
		{
			// initialize initial findNode
			if (findNode == -1)
				findNode = 0;
			
			bool reached = false;
			Vector3 characterDirection = Vector3.zero;

			// update final location
			// TODO : PARSE according to prev position                                   
			// find hex unit cell position relative to path
			float distancePointToLine = 10000.0f;
			Vector3 pointToLine = Vector3.zero;
			float lengthPath = 0.0f;
			for (int i = findNode; i < curvedPath.Count - 1; ++i)
			{
				var p0 = curvedPath[i + 0];
				var p1 = curvedPath[i + 1];
				var localPointToLine = Vector3Extension.ProjectPointLine(hexUnit.UnitTransform.position, p0, p1);
				var localDistancePointToLine = Vector3.Magnitude(localPointToLine - hexUnit.UnitTransform.position);
				if (localDistancePointToLine < distancePointToLine)
				{
					distancePointToLine = localDistancePointToLine;
					pointToLine = localPointToLine;
					findNode = i;
					lengthPath = 0.0f;
				}
				lengthPath += Vector3.Magnitude(p1 - p0);
			}

			// check if the hex unit has reached the last position
			if (findNode == curvedPath.Count - 2)
			{
				// distance of hex unit from the current node
				float currentDistance = (hexUnit.UnitTransform.position - curvedPath[findNode]).magnitude;
				if (currentDistance > (curvedPath[findNode + 1] - curvedPath[findNode + 0]).magnitude)
				{
					reached = true;
				}
			}

			// if the hex unit has not reached the last node
			if (reached == false)
			{
				// compute direction of hex unit according to the current path between 2 nodes
				var direction = (curvedPath[findNode + 1] - curvedPath[findNode + 0]).normalized;
				// velocity accordint to speed and direction
				characterDirection = Vector3.Scale(direction, HORIZONTAL_PLANE).normalized;
				// catch up real pos / expected pos
				Vector3 differenceCharacterToPath = (pointToLine - hexUnit.UnitTransform.position);
				var amount = 2.0f / lengthPath;
				characterDirection = characterDirection + Vector3.Scale(differenceCharacterToPath * amount, HORIZONTAL_PLANE);
			}

			return (reached, characterDirection, findNode);
		}

		//  Excecute complete itration on path
		bool forceStop = false;

		// Force stop
		public bool ForceStop
		{
			set
			{
				if (value != forceStop)
				{
					forceStop = value;
				}
			}
		}

		// Execute sub travel
		private IEnumerator ExecuteSubTravel(bool canceled, bool cancelable, global::GameCreator.Characters.Character character, float stopThreshold, float cancelDelay, GameObject target, Vector3 prevPos, Vector3 nextPos, float initTime, Vector3 lastPosition)
		{
			Vector3 cPosition = Vector3.zero;
			
			// direction 
			Vector3 dirCharacter = (nextPos - prevPos);
			float sqrDistance = dirCharacter.sqrMagnitude;
			if (sqrDistance > 0.0)
			{
				dirCharacter = dirCharacter.normalized;

				character.characterLocomotion.SetDirectionalDirection(Vector3.Scale(dirCharacter, HORIZONTAL_PLANE), null);

				while (!canceled && !this.forceStop)
				{
					prevPos = character.transform.position;

					// remain distance on the part of path
					float remainingDistance = Vector3.Distance(Vector3.Scale(nextPos, HORIZONTAL_PLANE), Vector3.Scale(prevPos, HORIZONTAL_PLANE));

					// test if the movement has changed of direction
					Vector3 currentDirCharacter = (nextPos - prevPos).normalized;
					float dot = Vector3.Dot(dirCharacter, currentDirCharacter);
					if (remainingDistance <= stopThreshold || dot < 0.0)
					{
						break;
					}

					// overall distance to the travel arrival
					float remainingDistanceOverall = Vector3.Distance(Vector3.Scale(nextPos, HORIZONTAL_PLANE), Vector3.Scale(lastPosition, HORIZONTAL_PLANE));
					if (remainingDistanceOverall > 20.0f)
					{
						character.characterLocomotion.canRun = true;
					}
					else
					{
						character.characterLocomotion.canRun = false;
					}

					// cancel loop if needed
					if (cancelable && (Time.time - initTime) >= cancelDelay)
					{
						canceled = true;
					}
					yield return null;
				}
			}

			if (canceled)
				yield return 999999;
			else
				yield return 0;
		}

		public IEnumerator ExecuteCompleteIteration(bool cancelable, global::GameCreator.Characters.Character character, float stopThreshold, float cancelDelay, GameObject target)
		{
			bool canceled = false;
			this.forceStop = false;

			// init time needed for cancelation process
			float initTime = Time.time;
			Vector3 lastPosition = hexUnit.PathToTravel[hexUnit.PathToTravel.Count - 1].Position;
			
			// follow curved path
			bool reached = false;
			Vector3 velocity;
			int findNode = -1;
			while (reached == false && !forceStop && !canceled)
			{
				// we execute map travel
				(reached, velocity, findNode) = UpdateIteration(findNode);
				if (reached == false)
				{
					character.characterLocomotion.SetDirectionalDirection(velocity, null);
				}

				// cancel loop if needed
				if (cancelable == true && (Time.time - initTime) >= cancelDelay)
				{
					canceled = true;
				}

				// all right
				yield return null;
			}

			if (reached == true)
			{
			}

			if (canceled == true)
			{
			}

			yield return null;
		}
	};
}