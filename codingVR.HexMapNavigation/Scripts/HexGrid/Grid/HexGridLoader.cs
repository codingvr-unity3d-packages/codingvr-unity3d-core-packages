﻿using UnityEngine;
using System.IO;
using System;
using System.Collections;

namespace codingVR.HexMapNavigation
{
	public class HexGridLoader
	{
		public const int mapFileVersion = 24;

		public static bool NeedUpgrade(string destFile, string sourceFile)
		{
			bool needUpgradeDate = false;
#if UNITY_EDITOR
			var dstDate = File.GetLastWriteTime(destFile);
			var srcDate = File.GetLastWriteTime(sourceFile);
			bool db = DateTime.Compare(dstDate, srcDate) < 0;
			needUpgradeDate = db || !File.Exists(destFile);
#endif
			return needUpgradeDate;
		}


		// Start is called before the first frame update
		// WARNING: addressables is neved used since we consider json file as addressables object
		public static IEnumerator Load(IHexGridManagerOrigin hexGridManager, Scene.IResourcesLoader resourcesLoader, string path, bool addressables)
		{
			bool bReturn = false;
			var pathJson = path + ".json";
			var pathBin = path + ".bytes";
			var fullPathBin = SelectStreamingPath(pathBin);
			var fullPathJson = SelectStreamingPath(pathJson);
			bool useJson = false;
			TextAsset obj = null;

			if (NeedUpgrade(fullPathBin, fullPathJson))
			{
				resourcesLoader.LoadResource(pathJson, true);
				yield return resourcesLoader.Wait(pathJson);
				obj = resourcesLoader.GetResource(pathJson) as TextAsset;
				useJson = true;
				Debug.LogError("UPGRADE JSON FILE " + pathJson + " NEEDED ...");
			}
			else
			{
				resourcesLoader.LoadResource(pathBin, true);
				yield return resourcesLoader.Wait(pathBin);
				obj = resourcesLoader.GetResource(pathBin) as TextAsset;
			}

			if (obj != null && obj.bytes.Length > 0)
			{
				using (var memory = new MemoryStream(obj.bytes))
				{
					// read file
					if (useJson == false)
					{
						using (BinaryReader reader = new BinaryReader(memory))
						{
							var gameDataReader = new Core.GameDataReader(reader);
							gameDataReader.Open();
							int version = gameDataReader.ReadInt();

							if (version > 4 && version != 11)
							{
								if (version <= mapFileVersion)
								{
									if (version != HexGridLoader.mapFileVersion)
									{
										Debug.LogWarning("the map file must be upgraded");
									}

									yield return hexGridManager.Load(gameDataReader, version, true, (bool status) => { bReturn = status; });
								}
								else
								{
									Debug.LogWarning("Unknown map format " + version);
								}
							}
							else
							{
								Debug.LogError("the map file is deprecated !!!");
								Application.Quit();
							}
							gameDataReader.Close();
						}
					}
					else
					// read file text
					{
						string json;
						using (var reader = new StreamReader(memory))
						{
							json = reader.ReadToEnd();
						}

						if (json.Length > 0)
						{
							var readerJson = new LitJson.JsonReader(json);
							var gameDataReader = new Core.GameDataReader(readerJson);
							gameDataReader.Open();
							int version = gameDataReader.ReadInt();
							if (version <= mapFileVersion)
							{
								yield return hexGridManager.Load(gameDataReader, version, true, (bool status) => { bReturn = status; });
							}
							else
							{
								Debug.LogWarning("Unknown map format " + version);
							}
							gameDataReader.Close();
						}
					}
				}
			}
			yield return null;
		}

		// we select real path based on streaming assets 
		private static string SelectRealPath(string fullPath)
		{
			string sourceDir = Application.streamingAssetsPath;
			string destDir = Application.persistentDataPath;

			// remove sub string from (sourceDir) from full path
			string filename = fullPath.Remove(fullPath.IndexOf(sourceDir), sourceDir.Length);

			destDir = destDir + filename;

			return destDir;
		}

		private static string SelectStreamingPath(string file)
		{
			string destDir;
			destDir = Path.Combine("Assets/StreamingAddressablesAssets/", file);
			return destDir;
		}

		public static void Save(IHexGridManagerOrigin hexGridManager, string path)
		{
			// save json file
			{
				var writerJson = new LitJson.JsonWriter(new System.Text.StringBuilder());
				{
					Core.GameDataWriter gameDataWriter = new Core.GameDataWriter(writerJson);
					gameDataWriter.Open();
					gameDataWriter.Write(mapFileVersion);
					hexGridManager.Save(gameDataWriter);
					gameDataWriter.Close();

				}
				string json = writerJson.TextWriter.ToString();
				Debug.Log(json);
				var file_json = path + ".json";
				var fullPath = SelectStreamingPath(file_json);
				using (var writer = new StreamWriter(File.Open(fullPath, FileMode.Create)))
				{
					writer.Write(json);
				}
			}

			// save binary file
			{
				var file_bin = path + ".bytes";
				var fullPath = SelectStreamingPath(file_bin);
				using (var writer = new BinaryWriter(File.Open(fullPath, FileMode.Create)))
				{
					Core.GameDataWriter gameDataWriter = new Core.GameDataWriter(writer);
					gameDataWriter.Open();
					gameDataWriter.Write(mapFileVersion);
					hexGridManager.Save(gameDataWriter);
					gameDataWriter.Close();
				}
			}
		}

		// we select path from streaming assets
		public static string GetSelectedPath(string filename)
		{
			string mapName = filename;
			if (mapName.Length == 0)
			{
				return null;
			}
			return mapName + ".map";
		}
	}
}
			