﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace codingVR.HexMapNavigation
{
	// === Hex grid ===============================================================

	public class HexGrid : MonoBehaviour, IHexGrid
	{
		Transform[] columns;
		int chunkSizeX = HexMetrics.chunkSizeX;
		int chunkSizeZ = HexMetrics.chunkSizeZ;
		IHexGridChunkOrigin[] chunks;
		IHexCellOrigin[] cells;
		int chunkCountX, chunkCountZ;
		int currentCenterColumnIndex = -1;

		// === Navigation.IHexGrid ===

		int cellCountX = 20, cellCountZ = 15;

		public int CellCountX => cellCountX;
		public int CellCountZ => cellCountZ;

		bool wrapping = false;

		public bool Wrapping => wrapping;

		// === shader data ===

		HexCellShaderData cellShaderData;

		public HexCellShaderData ShaderData
		{
			get
			{
				return cellShaderData;
			}
		}

		// === unity call back ===

		void Awake()
		{
			cellShaderData = gameObject.AddComponent<HexCellShaderData>();
		}

		void OnEnable()
		{
			HexMetrics.wrapSize = wrapping ? cellCountX : 0;
		}
		
		void Update()
		{
		}

		// === Tag management ===

		Dictionary<Data.Tag, IHexCellOrigin> linkedHexCellsCache;

		public IHexCellOrigin FindCellFromTag(Data.Tag tag, bool bInvalidate)
		{
			if (bInvalidate)
				linkedHexCellsCache = null;
			if (linkedHexCellsCache == null)
			{
				linkedHexCellsCache = new Dictionary<Data.Tag, IHexCellOrigin>();
				foreach (var it in cells)
				{
					if (it.TagData.Tag != null)
					{
						linkedHexCellsCache.Add(it.TagData.Tag, it);
					}
				}
			}

			if (linkedHexCellsCache.ContainsKey(tag) == true)
			{
				return linkedHexCellsCache[tag];
			}
			return null;
		}

		// === ui management ===

		public void ShowUI(bool visible)
		{
			for (int i = 0; i < chunks.Length; i++)
			{
				chunks[i].ShowUI(visible);
			}
		}

		// === chunk & column management ===
				
		public void MakeChildOfColumn(Transform child, int columnIndex)
		{
			child.SetParent(columns[columnIndex], false);
		}

		// === map/chunk/cells management ===

		public bool CreateMap(IHexCellFactory hexCellFactory, IHexChunkFactory hexChunkFactory, IHexCellLabelFactory hexCellLabelFactory, int x, int z, int chunkSizeX, int chunkSizeZ, bool wrapping, bool centerMap)
		{
			if (
				x <= 0 || x % chunkSizeX != 0 ||
				z <= 0 || z % chunkSizeZ != 0
			)
			{
				Debug.LogError("Unsupported map size.");
				return false;
			}

			hexGridNet.ClearAllPaths();
			
			if (columns != null)
			{
				for (int i = 0; i < columns.Length; i++)
				{
					Destroy(columns[i].gameObject);
				}
			}

			cellCountX = x;
			cellCountZ = z;
			this.wrapping = wrapping;
			currentCenterColumnIndex = -1;
			HexMetrics.wrapSize = wrapping ? cellCountX : 0;
			chunkCountX = cellCountX / chunkSizeX;
			chunkCountZ = cellCountZ / chunkSizeZ;
			cellShaderData.Initialize(cellCountX, cellCountZ);
			CreateChunks(hexChunkFactory, chunkSizeX, chunkSizeZ);
			CreateCells(hexCellFactory, hexCellLabelFactory, chunkSizeX, chunkSizeZ);

			// center map
			if (centerMap == true)
			{
				var offsetCoordinateStart = HexCoordinates.FromOffsetCoordinates(0, 0);
				Vector3 positionStart = offsetCoordinateStart.GetPosition();
				var offsetCoordinateEnd = HexCoordinates.FromOffsetCoordinates(cellCountX - 1, cellCountZ - 1);
				Vector3 positionEnd = offsetCoordinateEnd.GetPosition();
				Vector3 sizeMap = positionEnd - positionStart;
				transform.position = new Vector3(-sizeMap.x * 0.5f, 0.0f, -sizeMap.z * 0.5f);
			}

			return true;
		}

		void CreateChunks(IHexChunkFactory hexChunkFactory, int chunkSizeX, int chunkSizeZ)
		{
			columns = new Transform[chunkCountX];
			for (int x = 0; x < chunkCountX; x++)
			{
				columns[x] = new GameObject("Column").transform;
				columns[x].SetParent(transform, false);
			}

			chunks = new IHexGridChunkOrigin[chunkCountX * chunkCountZ];
			for (int z = 0, i = 0; z < chunkCountZ; z++)
			{
				for (int x = 0; x < chunkCountX; x++)
				{
					var chunk = hexChunkFactory.Create(columns[x], chunkSizeX, chunkSizeZ);
					chunks[i++] = chunk;
				}
			}
		}

		// === cells management ===

		void CreateCells(IHexCellFactory hexCellFactory, IHexCellLabelFactory hexCellLabelFactory, int chunkSizeX, int chunkSizeZ)
		{
			cells = new IHexCellOrigin[cellCountZ * cellCountX];

			for (int z = 0, i = 0; z < cellCountZ; z++)
			{
				for (int x = 0; x < cellCountX; x++)
				{
					CreateCell(hexCellFactory, hexCellLabelFactory, x, z, chunkSizeX, chunkSizeZ, i++);
				}
			}
		}

		RaycastHit? lastRaycastHit = null;

		public IHexCellOrigin GetCell(Ray ray)
		{
			IHexCellOrigin hitCell = null;
			var hits = Core.PhysicsExtensions.RaycastAllOrdered(ray);
			if (hits.Length > 0)
			{
				var hexMapTerrain = "Ground";
				// find any "terrain" map collider in proximity of first suitable collider
				RaycastHit hit = hits[0];
				var name = hit.transform.gameObject.name;
				if (name != hexMapTerrain)
				{
					for (int i = 0; i < hits.Length; ++i)
					{
						var nameNext = hits[i].transform.gameObject.name;
						if (nameNext == hexMapTerrain)
						{
							if (Vector3.Distance(hits[i].point, hit.point) <= 0.05)
							{
								hit = hits[i];
								break;
							}
						}
					}
				}
				
				// draw way from last raycast to new raycast
				if (lastRaycastHit != null)
				{
					Vector3 hitPoint = lastRaycastHit?.point ?? Vector3.zero;
					if (hitPoint != Vector3.zero)
					{
						Plugins.DebugLine.DrawLine(hitPoint, hit.point, Color.red, 0.15f, 1.0f);
					}
				}
				lastRaycastHit = hit;

				// hit transform get cell
				name = hit.transform.gameObject.name;
				if (name == hexMapTerrain)
				{
					hitCell = GetCell(hits[0].point);
				}
			}
			else
			{
				lastRaycastHit = null;
			}
			return hitCell;
		}

		public RaycastHit? GetLastRaycastHit()
		{
			return lastRaycastHit;
		}

		public IHexCellOrigin GetCell(Ray ray, float elevation, float elevationStep)
		{
			float height = HexMetrics.ComputeHeight(elevation, elevationStep);

			IHexCellOrigin hitCell = null;
			Plane plane = new Plane(Vector3.up, height);
			//Get the point that is clicked
			float enter = 0.0f;
			if (plane.Raycast(ray, out enter))
			{
				Vector3 hitPoint = ray.GetPoint(enter);
				hitCell = GetCell(hitPoint);
			}
			return hitCell;
		}
		
		public IHexCellOrigin GetCell(Vector3 position)
		{
			position = transform.InverseTransformPoint(position);
			HexCoordinates coordinates = HexCoordinates.FromPosition(position);
			return GetCell(coordinates);
		}

		public IHexCellOrigin GetCell(HexCoordinates coordinates)
		{
			int z = coordinates.Z;
			if (z < 0 || z >= cellCountZ)
			{
				return null;
			}
			int x = coordinates.X + z / 2;
			if (x < 0 || x >= cellCountX)
			{
				return null;
			}
			return cells[x + z * cellCountX];
		}

		public IHexCellOrigin GetCell(int xOffset, int zOffset)
		{
			return cells[xOffset + zOffset * cellCountX];
		}

		public IHexCellOrigin GetCell(int cellIndex)
		{
			return cells[cellIndex];
		}

		void CreateCell(IHexCellFactory hexCellFactory, IHexCellLabelFactory hexCellLabelFactory, int x, int z, int chunkSizeX, int chunkSizeZ, int i)
		{
			IHexCellOrigin cell = cells[i] = hexCellFactory.Create(hexCellLabelFactory, x, z, chunkSizeX, chunkSizeZ, i, cellShaderData);

			if (wrapping)
			{
				cell.Explorable = z > 0 && z < cellCountZ - 1;
			}
			else
			{
				cell.Explorable =
					x > 0 && z > 0 && x < cellCountX - 1 && z < cellCountZ - 1;
			}

			if (x > 0)
			{
				cell.SetNeighbor(HexDirection.W, cells[i - 1]);
				if (wrapping && x == cellCountX - 1)
				{
					cell.SetNeighbor(HexDirection.E, cells[i - x]);
				}
			}
			if (z > 0)
			{
				if ((z & 1) == 0)
				{
					cell.SetNeighbor(HexDirection.SE, cells[i - cellCountX]);
					if (x > 0)
					{
						cell.SetNeighbor(HexDirection.SW, cells[i - cellCountX - 1]);
					}
					else if (wrapping)
					{
						cell.SetNeighbor(HexDirection.SW, cells[i - 1]);
					}
				}
				else
				{
					cell.SetNeighbor(HexDirection.SW, cells[i - cellCountX]);
					if (x < cellCountX - 1)
					{
						cell.SetNeighbor(HexDirection.SE, cells[i - cellCountX + 1]);
					}
					else if (wrapping)
					{
						cell.SetNeighbor(
							HexDirection.SE, cells[i - cellCountX * 2 + 1]
						);
					}
				}
			}
			
			AddCellToChunk(x, z, chunkSizeX, chunkSizeZ, cell);
		}

		void AddCellToChunk(int x, int z, int chunkSizeX, int chunkSizeZ, IHexCellOrigin cell)
		{
			int chunkX = x / chunkSizeX;
			int chunkZ = z / chunkSizeZ;
			IHexGridChunkOrigin chunk = chunks[chunkX + chunkZ * chunkCountX];

			int localX = x - chunkX * chunkSizeX;
			int localZ = z - chunkZ * chunkSizeZ;
			chunk.AddCell(localX + localZ * chunkSizeX, cell);
		}

		// we find chunk by parsing geometry [SLOW]
		/*
		public IHexGridChunkOrigin FindGridChunkFromGeometry(Vector3 position)
		{
			foreach (var it in chunks)
			{
				if (it.GeometryContains(position) == true)
				{ 
					return it;
				}
			}
			return null;
		}
		*/

		// we get chunk by direct position mapping [VERY FAST]
		public IHexGridChunkOrigin GetGridChunk(Vector3 position)
		{
			IHexGridChunkOrigin chunk = null;
			var hexCell = GetCell(position);
			if (null != hexCell)
				chunk = hexCell.Chunk;
			return chunk;
		}

		// we get chunk by index
		public IHexGridChunkOrigin GetGridChunk(int index)
		{
			return chunks[index];
		}

		public int GetGridChunkCount()
		{
			return chunks.Length;
		}

		/*
		public Transform GetGridColumn(Vector3 position)
		{
			foreach (var it in columns)
			{
				Bounds bounds = it.gameObject.GetComponent<Mesh>().bounds;

				Bounds bounds2d = new Bounds(new Vector3(bounds.center.x, 0.0f, bounds.center.z), new Vector3(bounds.size.x, 1.0f, bounds.size.z));
				Vector3 position2d = new Vector3(position.x, 0.5f, position.z);

				if (bounds2d.Contains(position2d) == true)
				{
					return it;
				}
			}
			return null;
		}
		*/

		// === load / save ===

		public void Save(Core.GameDataWriter writer)
		{
			// metrics
			{
				writer.WriteObjectStart("MetricsConfig");
				writer.Write(chunkSizeX, "ChunkSizeX");
				writer.Write(chunkSizeZ, "ChunkSizeZ");
				writer.Write(HexMetrics.size, "CellSize");
				writer.WriteObjectEnd();
			}

			// config
			{
				writer.WriteObjectStart("MapConfig");
				writer.Write(cellCountX, "CellCountX");
				writer.Write(cellCountZ, "CellCountZ");
				writer.Write(wrapping, "Wrapping");
				writer.WriteObjectEnd();
			}

			// save all cells
			writer.WriteObjectStart("Cells");
			for (int i = 0; i < cells.Length; i++)
			{
				// Cell object start
				writer.WriteObjectStart("Cell" + "[" + cells[i].Coordinates.X + "," + cells[i].Coordinates.Z + "]");
				// save
				cells[i].Save(writer);
				// end
				writer.WriteObjectEnd();

			}
			writer.WriteObjectEnd();

			// save all chunks
			writer.WriteObjectStart("Chunks");
			for (int i = 0; i < chunks.Length; i++)
			{
				// chunk object start
				writer.WriteObjectStart("Chunk" + "[" + i + "]");
				// save
				chunks[i].Save(writer);
				// chunk object end
				writer.WriteObjectEnd();
			}
			writer.WriteObjectEnd();
		}

		public void Load(IHexCellFactory hexCellFactory, IHexChunkFactory hexChunkFactory, IHexCellLabelFactory hexCellLabelFactory, Core.GameDataReader reader, int version, bool centerMap)
		{
			bool wrapping = false;

			hexGridNet.ClearAllPaths();
			HexMetrics.SetSize(10.0f);

			int x = 20, z = 15;
			if (version >= 11)
			{
				// metrics
				reader.ReadObjectStart("MetricsConfig");
				chunkSizeX = reader.ReadInt();
				chunkSizeZ = reader.ReadInt();
				HexMetrics.SetSize(reader.ReadFloat());
				reader.ReadObjectEnd();

				// config
				reader.ReadObjectStart("MapConfig");
				x = reader.ReadInt();
				z = reader.ReadInt();
				wrapping = reader.ReadBoolean();
				reader.ReadObjectEnd();

				// features
				if (version >= 12 && version <= 18)
				{
					reader.ReadObjectStart("FeaturesConfig");
					HexMapTerrain.HexMetrics.maxPlantCountByCell = reader.ReadInt();
					HexMapTerrain.HexMetrics.maxUrbanCountByCell = reader.ReadInt();
					HexMapTerrain.HexMetrics.maxFarmCountByCell = reader.ReadInt();
					reader.ReadObjectEnd();
				}
			}
			else
			{
				reader.ReadObjectStart("Header");
				x = reader.ReadInt();
				z = reader.ReadInt();
				chunkSizeX = 5;
				chunkSizeZ = 5;
				wrapping = reader.ReadBoolean();
				reader.ReadObjectEnd();
			}
			
			if (x != cellCountX || z != cellCountZ || this.wrapping != wrapping || cells == null)
			{
				if (!CreateMap(hexCellFactory, hexChunkFactory, hexCellLabelFactory, x, z, chunkSizeX, chunkSizeZ, wrapping, centerMap))
				{
					return;
				}
			}

			bool originalImmediateMode = cellShaderData.ImmediateMode;
			cellShaderData.ImmediateMode = true;

			// load all cells
			reader.ReadObjectStart("Cells");
			for (int i = 0; i < cells.Length; i++)
			{
				// cell object start
				reader.ReadObjectStart("Cell" + "[" + cells[i].Coordinates.X + "," + cells[i].Coordinates.Z + "]");
				// load    
				cells[i].Load(reader, version);
				// end
				reader.ReadObjectEnd();
			}
			reader.ReadObjectEnd();

			// chunks avaliable since version 1.0
			if (version >= 1.0)
			{
				// load all chunks
				reader.ReadObjectStart("Chunks");
				for (int i = 0; i < chunks.Length; i++)
				{
					// chunk object start
					reader.ReadObjectStart("Chunk" + "[" + i + "]");
					// load
					chunks[i].Load(reader, version);
					// end
					reader.ReadObjectEnd();
				}
				reader.ReadObjectEnd();
			}

			// refresh all chunks
			for (int i = 0; i < chunks.Length; i++)
			{
				chunks[i].Refresh();
			}

			cellShaderData.ImmediateMode = originalImmediateMode;

			return;
		}

		// === Hex grid net ===

		HexGridNet hexGridNet = new HexGridNet();

		public IHexGridNet Net
		{
			get { return hexGridNet; }
		}

		// === map / camera ===

		public void CenterMap(float xPosition)
		{
			int centerColumnIndex = (int)(xPosition / (HexMetrics.innerDiameter * chunkSizeX));
			if (centerColumnIndex == currentCenterColumnIndex)
			{
				return;
			}
			currentCenterColumnIndex = centerColumnIndex;
			int minColumnIndex = centerColumnIndex - chunkCountX / 2;
			int maxColumnIndex = centerColumnIndex + chunkCountX / 2;

			Vector3 position;
			position.y = position.z = 0f;
			for (int i = 0; i < columns.Length; i++)
			{
				if (i < minColumnIndex)
				{
					position.x = chunkCountX * (HexMetrics.innerDiameter * chunkSizeX);
				}
				else if (i > maxColumnIndex)
				{
					position.x = chunkCountX * -(HexMetrics.innerDiameter * chunkSizeX);
				}
				else
				{
					position.x = 0f;
				}
				columns[i].localPosition = position;
			}
		}
	}
}