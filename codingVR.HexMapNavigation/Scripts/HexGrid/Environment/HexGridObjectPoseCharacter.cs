﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.HexMapNavigation
{
    public class HexGridObjectPoseCharacter : HexGridObjectPoseBase
    {
        private void Awake()
        {
        }

        void LateUpdate()
        {
            base.UpdatePose(HexEnvironmentManagerIDs.CharactersContainerIdx);
        }
    }
}
