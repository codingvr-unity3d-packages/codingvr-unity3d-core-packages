﻿namespace codingVR.HexMapNavigation
{
    public class HexGridObjectPoseProps : HexGridObjectPoseBase
    {
        private void Awake()
        {
        }

        void LateUpdate()
        {
#if UNITY_EDITOR
            base.UpdatePose(HexEnvironmentManagerIDs.PropsContainerIdx);
#endif
        }
    }
}
