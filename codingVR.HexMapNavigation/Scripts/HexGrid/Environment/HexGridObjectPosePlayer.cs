﻿using UnityEngine;

namespace codingVR.HexMapNavigation
{
    [RequireComponent(typeof(IHexGridUnit))]
    public class HexGridObjectPosePlayer : HexGridObjectPoseCharacter
    {
        // HexGrid Object Pose Character
        private IHexGridUnit hexUnit;

        private void Awake()
        {
            hexUnit = GetComponent<IHexGridUnit>();
        }

        void LateUpdate()
        {
            base.UpdatePose(HexEnvironmentManagerIDs.CharactersContainerIdx);
        }

        protected override void OnCellLeaveNotify(IHexCellOrigin cell) 
        {
            cell.UnitData.Unit = null;
        }

        protected override void OnCellEnterNotify(IHexCellOrigin cell)
        {
            cell.UnitData.Unit = hexUnit;
        }
    }
}
