﻿using UnityEditor;

[CustomEditor(typeof(codingVR.HexMapNavigation.HexGridObjectPoseCharacter))]
public class HexGridObjectPoseCharacterEditor : HexGridObjectPoseBaseEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        PoseButtonInspectorGUI<codingVR.HexMapNavigation.HexGridObjectPoseCharacter>("Place character on map");
    }
}
