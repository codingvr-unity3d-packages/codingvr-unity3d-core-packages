﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(codingVR.HexMapNavigation.HexGridObjectPlaceHolderPropsScene))]
public class HexGridObjectPlaceHolderPropsSceneEditor : Editor
{
    private SerializedObject obj;

    public void OnEnable()
    {
        obj = new SerializedObject(target);
    }

    public override void OnInspectorGUI()
    {
        //InitStyles();
        DrawDefaultInspector();

        EditorGUILayout.Space();

        Rect drop_area = GUILayoutUtility.GetRect(0.0f, 50.0f, GUILayout.ExpandWidth(true));
        GUI.Box(drop_area, "Drag your game object here\nD'ont forget to pose this placeholder on map\nThen save");

        //string error = "";
        if (DropAreaGUI(drop_area) == false)
        {
            Debug.LogWarningFormat("{0} : only regular game object is authorized", System.Reflection.MethodBase.GetCurrentMethod().Name);
        }

        EditorGUILayout.Space();

        if (GUILayout.Button("Update"))
        {
            // object pose
            var targetObj = target as codingVR.HexMapNavigation.HexGridObjectPlaceHolderPropsScene;
            targetObj.UpdateScene();
        }
    }

    // https://gist.github.com/bzgeb/3800350
    public bool DropAreaGUI(Rect drop_area)
    {
        bool bReturn = true;

        Event evt = Event.current;

        switch (evt.type)
        {
            case EventType.DragUpdated:
            case EventType.DragPerform:
            {
                if (drop_area.Contains(evt.mousePosition))
                {
                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                    if (evt.type == EventType.DragPerform)
                    {
                        DragAndDrop.AcceptDrag();

                        foreach (Object dragged_object in DragAndDrop.objectReferences)
                        {
                            var targetObj = target as codingVR.HexMapNavigation.HexGridObjectPlaceHolderPropsScene;
                            PrefabAssetType prefabType = PrefabUtility.GetPrefabAssetType(dragged_object);
                            if (prefabType == PrefabAssetType.Regular)
                            {
                                GameObject go = dragged_object as GameObject;
                                if (go != null)
                                {
                                    var peristableObject = go.GetComponent<codingVR.RuntimeTools.PersistableObject>();

                                    if (peristableObject == null)
                                    {
                                        var gameObject = dragged_object as GameObject;
                                        targetObj.Scene = gameObject.transform;
                                    }
                                    else
                                    {
                                        bReturn = false;
                                        var errorString = string.Format("{0} : can't bind a persistable object\n{1} is a persitable object !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, dragged_object.name);
                                        EditorUtility.DisplayDialog("drag & drop error", errorString, "Ok");
                                    }
                                }
                                else
                                {
                                    bReturn = false;
                                    var errorString = string.Format("{0} : only game objects are accepted\n{1} is not a game object !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, dragged_object.name);
                                    EditorUtility.DisplayDialog("drag & drop error", errorString, "Ok");
                                }
                            }
                            else
                            {
                                bReturn = false;
                                var errorString = string.Format("{0} : can't bind a prefab object\n{1} is a prefab object !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, dragged_object.name);
                                EditorUtility.DisplayDialog("drag & drop error", errorString, "Ok");

                            }
                        }
                    }
                }
                break;
            }
        }
        return bReturn;
    }
}
 