﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(codingVR.HexMapNavigation.HexGridObjectPlaceHolder))]
public class HexGridObjectPlaceHolderEditor : Editor
{
    private SerializedObject obj;

    public void OnEnable()
    {
        obj = new SerializedObject(target);
    }
    /*
    private GUIStyle errorStyle = null;
    
    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];
        for (int i = 0; i < pix.Length; ++i)
        {
            pix[i] = col;
        }
        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();
        return result;
    }

    private void InitStyles()
    {
        if (errorStyle == null)
        {
            errorStyle = new GUIStyle(GUI.skin.box);
            errorStyle.normal.background = MakeTex(2, 2, new Color(0f, 1f, 0f, 0.5f));
        }
    }
    */

    public override void OnInspectorGUI()
    {
        //InitStyles();
        DrawDefaultInspector();

        EditorGUILayout.Space();

        Rect drop_area = GUILayoutUtility.GetRect(0.0f, 50.0f, GUILayout.ExpandWidth(true));
        GUI.Box(drop_area, "Drag your game object here\nD'ont forget to pose this placeholder on map\nThen save");

        //string error = "";
        if (DropAreaGUI(drop_area) == false)
        {
            Debug.LogWarningFormat("{0} : only regular game object is authorized", System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
        //GUI.backgroundColor = Color.red;
        //GUILayout.BeginHorizontal();
        //GUILayout.Box(error, errorStyle);
        //GUILayout.EndHorizontal();
    }

    // https://gist.github.com/bzgeb/3800350
    public bool DropAreaGUI(Rect drop_area)
    {
        bool bReturn = true;

        Event evt = Event.current;

        switch (evt.type)
        {
            case EventType.DragUpdated:
            case EventType.DragPerform:
                {
                    if (drop_area.Contains(evt.mousePosition))
                    {
                        DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                        if (evt.type == EventType.DragPerform)
                        {
                            DragAndDrop.AcceptDrag();

                            foreach (Object dragged_object in DragAndDrop.objectReferences)
                            {
                                SerializedProperty prop = obj.FindProperty("gameObjectTargetID");
                                if (prop != null)
                                {
                                    var targetObj = prop.serializedObject.targetObject as codingVR.HexMapNavigation.HexGridObjectPlaceHolder;
                                    PrefabAssetType prefabType = PrefabUtility.GetPrefabAssetType(dragged_object);
                                    if (prefabType == PrefabAssetType.NotAPrefab)
                                    {
                                        GameObject go = dragged_object as GameObject;
                                        if (go != null)
                                        {
                                            var peristableObject = go.GetComponent<codingVR.RuntimeTools.PersistableObject>();

                                            if (peristableObject == null)
                                            {
                                                targetObj.GameObjectTargetID = dragged_object.GetInstanceID();
                                            }
                                            else
                                            {
                                                bReturn = false;
                                                var errorString = string.Format("{0} : can't bind a persistable object\n{1} is a persitable object !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, dragged_object.name);
                                                EditorUtility.DisplayDialog("drag & drop error", errorString, "Ok");
                                            }
                                        }
                                        else
                                        {
                                            bReturn = false;
                                            var errorString = string.Format("{0} : only game objects are accepted\n{1} is not a game object !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, dragged_object.name);
                                            EditorUtility.DisplayDialog("drag & drop error", errorString, "Ok");
                                        }
                                    }
                                    else
                                    {
                                        bReturn = false;
                                        var errorString = string.Format("{0} : can't bind a prefab object\n{1} is a prefab object !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, dragged_object.name);
                                        EditorUtility.DisplayDialog("drag & drop error", errorString, "Ok");

                                    }
                                }
                                else
                                {
                                    var errorString = string.Format("{0} : gameObjectTargetID not found in place holder !!!", System.Reflection.MethodBase.GetCurrentMethod().Name);
                                    EditorUtility.DisplayDialog("drag & drop error", errorString, "Ok");
                                }

                                /*
                                                    // CODE BELOW DOESN'T CHANGE THE TARGET VALUE
                                                    SerializedProperty prop = obj.FindProperty("gameObjectTargetID");
                                                    prop.stringValue = dragged_object.name;
                                */

                            }

                            /*  
                                                // CODE BELOW DOESN'T CHANGE THE TARGET VALUE
                                                if (DragAndDrop.objectReferences.Length > 0)
                                                {
                                                    this.serializedObject.ApplyModifiedPropertiesWithoutUndo();
                                                }
                            */
                        }
                        
                    }
                    break;
                }
        }
        return bReturn;
    }
}
 