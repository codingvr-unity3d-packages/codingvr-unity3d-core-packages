﻿using UnityEditor;

[CustomEditor(typeof(codingVR.HexMapNavigation.HexGridObjectPoseFunction))]
public class HexGridObjectPoseFunctionEditor : HexGridObjectPoseBaseEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        PoseButtonInspectorGUI<codingVR.HexMapNavigation.HexGridObjectPoseFunction>("Place function on map");
    }
}
