﻿using UnityEditor;
using UnityEngine;

public class HexGridObjectPoseBaseEditor : Editor
{
    protected void PoseButtonInspectorGUI<clazz>(string buttonName) where clazz : codingVR.HexMapNavigation.HexGridObjectPoseBase, new()
    {
        if (GUILayout.Button(buttonName))
        {
            // object pose
            clazz gridObjectPose = target as clazz;
            if (gridObjectPose != null)
            {
                gridObjectPose.PoseFromTransform(codingVR.HexMapNavigation.HexEnvironmentManagerIDs.PropsContainerIdx);
            }

            // persistable object
            var persistableObject = gridObjectPose.gameObject.GetComponent<codingVR.RuntimeTools.PersistableObject>();
            if (persistableObject != null)
            {
                persistableObject.MakeIdentifier(true);
            }
        }
    }
}
