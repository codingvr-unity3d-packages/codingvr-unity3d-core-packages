﻿using UnityEditor;

[CustomEditor(typeof(codingVR.HexMapNavigation.HexGridObjectPosePlayer))]
public class HexGridObjectPosePlayerEditor : HexGridObjectPoseBaseEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        PoseButtonInspectorGUI<codingVR.HexMapNavigation.HexGridObjectPosePlayer>("Place player on map");
    }
}
