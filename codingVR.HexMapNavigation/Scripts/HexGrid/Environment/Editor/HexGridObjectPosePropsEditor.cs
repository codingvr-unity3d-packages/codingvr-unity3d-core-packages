﻿using UnityEditor;

[CustomEditor(typeof(codingVR.HexMapNavigation.HexGridObjectPoseProps))]
public class HexGridObjectPosePropsEditor : HexGridObjectPoseBaseEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        PoseButtonInspectorGUI<codingVR.HexMapNavigation.HexGridObjectPoseProps>("Place props on map");
    }
}
