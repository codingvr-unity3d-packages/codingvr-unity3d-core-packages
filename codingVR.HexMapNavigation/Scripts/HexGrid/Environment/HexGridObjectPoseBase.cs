﻿using System;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace codingVR.HexMapNavigation
{
	public class HexGridObjectPoseBase : MonoBehaviour
	{
		/*
		 *
		 * hex grid manager
		 * 
		 */

		[SerializeField]
		bool staticPosition = false;

		public bool StaticPosition
		{
			get
			{
				return staticPosition;
			}
			set
			{
				staticPosition = value;
			}
		}
			   
		/*
		 *
		 * hex grid manager
		 * 
		 */

		protected IHexGridManagerOrigin HexGridManager { get; private set; }

		/*
		 *
		 *  current hex grid cell
		 *  
		 */

		public IHexCellOrigin hexGridCellCurrent = null;
		public IHexCellOrigin HexGridCellCurrent
		{
			get
			{
				return hexGridCellCurrent;
			}

			private set
			{
				if (null != value)
				{
					if (value != hexGridCellCurrent)
					{
						// leave from nay cell
						if (this.OnCellLeave != null && hexGridCellCurrent != null)
						{
							this.OnCellLeave.Invoke(hexGridCellCurrent);
							OnCellLeaveNotify(hexGridCellCurrent);
							hexGridCellCurrent = null;
						}

						// enter to any cell
						if (this.OnCellEnter != null)
						{
							if (value.TagData.Tag != null)
							{
								// enter to tagged cell
								this.OnCellEnter.Invoke(value);
							}
							// enter to any cell
							OnCellEnterNotify(value);
							value.IsExplored = true;
						}

						// enter cell
						hexGridCellCurrent = value;
					}
				}
			}
		}
		
		/*
		 *
		 * events enter to or leave from cells
		 * 
		 */

		public class CellEvent : UnityEvent<IHexCellOrigin> { }
		public CellEvent OnCellEnter { get; } = new CellEvent();
		public CellEvent OnCellLeave { get; } = new CellEvent();
		// callback called at each time that the object leave or enter from cell
		protected virtual void OnCellLeaveNotify(IHexCellOrigin cell) { }
		protected virtual void OnCellEnterNotify(IHexCellOrigin cell) { }

		/*
		 * 
		 * Life cycle
		 * 
		 */

		private void Awake()
		{
		}

		void Start()
		{
			transform.hasChanged = false;
		}

		private void Update()
		{
			/*
			if (name == "MonAmiPotoNPC2")
				Debug.LogErrorFormat("{0} {1} {2}", transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);
				*/
		}

		/*
		 * 
		 * Update pose from current transform poisition
		 * 
		 */

		// pose from transform
		public IHexCellOrigin PoseFromTransform(HexEnvironmentManagerIDs containerIdx)
		{
			IHexCellOrigin gridCell = null;

			// we get the hexGridManager
			// TODO: HACK,HACK
			// TODO: PAY ATTENTION: normally the HexGridManager is assigned by Construct (Zenject DIP)
			// BUT apparently from editor mode is not running
			if (HexGridManager == null)
			{
				var hexGridManagerGameObject = GameObject.FindGameObjectWithTag("HexGridManager");
				HexGridManager = hexGridManagerGameObject.GetComponent<IHexGridManagerOrigin>();
			}

			// assign this gamobject to the props container 
			if (HexGridManager != null)
			{
				// get the correct cell
				IHexGridChunkOrigin gridChunk = null;
				gridCell = HexGridManager.HexGrid.GetCell(transform.position);
				// get the correct chunk
				if (gridCell != null)
				{
					gridChunk = gridCell.Chunk;
				}
				if (gridChunk != null)
				{
					// assing this chunk as the new parent of the object
					gridChunk.MakeChildOfChunkEnvironement(containerIdx, transform);
				}
				else
				{
					Debug.LogErrorFormat("{0} : object {1} if out of map boundary !!! destroying in progress ...", System.Reflection.MethodBase.GetCurrentMethod().Name, gameObject.name);
					Destroy(gameObject, 2);
				}
			}
			return gridCell;
		}
	   
		// Uodate pose if necessary
		public void UpdatePose(HexEnvironmentManagerIDs containerIdx)
		{
			if (transform.hasChanged)
			{
				if (staticPosition == false)
				{
					if (transform.parent != null)
					{
						if (transform.parent.tag == "HexGridObjectsContainer")
						{
							var cell = PoseFromTransform(containerIdx);
							this.HexGridCellCurrent = cell;
							transform.hasChanged = false;//set to false so we can detect the next time it changes
						}
					}
				}
			}
		}
	}
}
