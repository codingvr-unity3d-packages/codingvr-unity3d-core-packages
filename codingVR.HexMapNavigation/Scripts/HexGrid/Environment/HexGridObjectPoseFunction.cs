﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.HexMapNavigation
{
    public class HexGridObjectPoseFunction : HexGridObjectPoseBase
    {
        private void Awake()
        {
        }

        void LateUpdate()
        {
            base.UpdatePose(HexEnvironmentManagerIDs.FunctionsContainerIdx);
        }
    }
}
