﻿//#define USE_PLACE_HOLDER_PROPERTY

using UnityEngine;

namespace codingVR.HexMapNavigation
{
    public class HexGridObjectPlaceHolder : MonoBehaviour, RuntimeTools.IPersistableObject, IHexPlaceHolder
	{
        [ReadOnly]
        [SerializeField]
        private int gameObjectTargetID;

        [ReadOnly]
        [SerializeField]
        private string gameObjectTargetName;

        /*
         * 
         * Game object target 
         * 
         */

        public int GameObjectTargetID
        {
            get
            {
                return gameObjectTargetID;
            }

            set
            {
                if (value != gameObjectTargetID)
                {
                    gameObjectTargetID = value;
                    PoseObject(gameObjectTargetID);
                }
            }
        }
		
        /*
         * 
         * lifecycle
         * 
         */

        void Start()
        {
        }
              
        /*
         * 
         * pose object in place of placeholder
         * 
         */

        private void PoseObject(string gameObjectTargetName)
        {
            var gameObjectTarget = GameObjectExtensions.FindObjectEvenInactive(gameObjectTargetName);
            PoseObject(gameObjectTarget);
        }

        private void PoseObject(int gameObjectTargetID)
        {
            var gameObjectTarget = GameObjectExtensions.FindObjectEvenInactive(gameObjectTargetID);
            PoseObject(gameObjectTarget);
        }

        private void PoseObject(GameObject gameObjectTarget)
        {
            if (gameObjectTarget != null)
            {
                foreach (Transform child in transform)
                {
                    child.gameObject.SetActive(false);
                }
                gameObjectTargetName = gameObjectTarget.name;
                gameObjectTargetID = gameObjectTarget.GetInstanceID();

                var gizmo = GetComponent<Core.GameObjectDrawGizmos>();
                if (gizmo != null)
                {
                    gizmo.Title = gameObjectTargetName + "\nplaceholder";
                }

                // we assign place holder as parent
                // the world position of game object follows the place holder position
#if USE_PLACE_HOLDER_PROPERTY
                gameObjectTarget.transform.SetParent(transform, false);
#else
                gameObjectTarget.transform.SetParent(transform.parent, false);
                gameObjectTarget.transform.localPosition = transform.localPosition;
                gameObjectTarget.transform.localRotation = transform.localRotation;
                gameObjectTarget.transform.localScale = transform.localScale;
#endif
            }
            else
            {

                Debug.LogErrorFormat("{0} : Target from placeholder {1} not found", System.Reflection.MethodBase.GetCurrentMethod().Name, name);
                var gizmo = GetComponent<Core.GameObjectDrawGizmos>();
                if (gizmo != null)
                {
                    gizmo.Color = new Color(1.0f, 0.0f, 0.0f, 0.5f);
                    gizmo.Title = "game object on place holder \n" + name + "\nnot found !!!";
                }
            }
        }

		// === IHexPlaceHolder ===

		public int Type => 1;

		// === RuntimeTools.IPersistableObject ===

		/*
         * 
         * Save & Load
         * 
         */

		public void Save(Core.GameDataWriter writer)
        {
            // Chunk object start
            writer.WriteObjectStart("GameObject");

            // write target name
            writer.Write(gameObjectTargetName);

            // Chunk object end
            writer.WriteObjectEnd();
        }

        public void Load(Core.GameDataReader reader, int version)
        {
            // Environment
            if (version >= 15)
                reader.ReadObjectStart("GameObject");
            else
                reader.ReadObjectStart("Object-Placeholder");

            // read target name
            var gameObjectTarget = reader.ReadString();
            // pose object referenced by game object ID/name
            // this function below is delayed to prevent
            // an eventual pb of script ordering
            // (the position has been erased by some scripts initialisation)
            void DelayedPoseObject()
            {
                PoseObject(gameObjectTarget);
                return;
            }
            this.DelayedFunction(DelayedPoseObject);

            // Environment
            reader.ReadObjectEnd();
        }
    }
}