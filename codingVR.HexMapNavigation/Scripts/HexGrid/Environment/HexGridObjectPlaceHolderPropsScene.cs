﻿//#define USE_PLACE_HOLDER_PROPERTY

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Zenject;

namespace codingVR.HexMapNavigation
{
	[RequireComponent(typeof(RuntimeTools.PersistableObject))]
	public class HexGridObjectPlaceHolderPropsScene : MonoBehaviour, RuntimeTools.IPersistableObject, IHexPlaceHolder
	{
		/*
		 * 
		 * root scene transform
		 * 
		 */

		[SerializeField]
		[ReadOnly]
		string rootScenePrefabAssetPath;

		/*
		 *
		 * hex grid manager
		 * 
		 */

		protected IHexGridManagerOrigin HexGridManager { get; private set; }

		/*
		 * 
		 * Dip Constructor
		 * 
		 */

		[Inject]
		void Construct(IHexGridManagerOrigin hexGridManager)
		{
			this.HexGridManager = hexGridManager;
		}

		/*
		 * 
		 * Game object target 
		 * 
		 */
#if UNITY_EDITOR

		string GetPrefabeAssetPath(Object obj)
		{
			string assetPath = AssetDatabase.GetAssetPath(obj);
			assetPath = ResourcesExtensions.MakeResourcesPathFromAssetPath(assetPath);
			return assetPath;
		}

		public Transform Scene
		{
			set
			{
				// rootScene Transform
				this.rootScenePrefabAssetPath = GetPrefabeAssetPath(value);
				PoseObjects(value);
			}
		}

		public void UpdateScene()
		{
			if (string.IsNullOrEmpty(rootScenePrefabAssetPath) == false)
			{
				var gameObject = Resources.Load(rootScenePrefabAssetPath) as GameObject; // note: not .prefab!
				PoseObjects(gameObject.transform);
			}
		}

#endif

		/*
		 * 
		 * lifecycle
		 * 
		 */

		void Start()
		{
			SetupGizmo();
		}

		/*
		 * 
		 * populate all prefabs
		 * 
		 */
#if UNITY_EDITOR
		void PopulateAllPrefabsRecurse(Transform _parent, ref Transform _anchor, List<Transform> _children)
		{
			foreach (Transform trs in _parent.transform)
			{
				if (string.Equals(trs.name, "anchor", System.StringComparison.OrdinalIgnoreCase))
				{
					_anchor = trs;
				}

				var persistableObject = trs.GetComponent<RuntimeTools.PersistableObject>();
				var gridObjectPose = trs.GetComponent<HexGridObjectPoseBase>();
				if (persistableObject != null && gridObjectPose != null)
				{
					var prefab = PrefabUtilityExtension.GetCorrespondingObjectFromSource(trs.gameObject);
					if (prefab != null)
					{
						_children.Add(trs);
					}
					else
					{
						Debug.LogErrorFormat("{0} : the game object {1} is not a prefab", System.Reflection.MethodBase.GetCurrentMethod().Name, trs.gameObject.name);
					}
				}
				PopulateAllPrefabsRecurse(trs, ref _anchor, _children);
			}
		}

		// populate all prefabs
		(Transform, List<Transform>) PopulateAllPrefabs(Transform sceneTransform)
		{
			List<Transform> children = new List<Transform>();
			Transform anchor = null;
			PopulateAllPrefabsRecurse(sceneTransform, ref anchor, children);
			return (anchor != null ? anchor : children[0], children);
		}
#endif

		/*
		 * 
		 * destroy all standalone objects
		 * 
		 */

		void PopulateAllStandaloneObjectsRecurse(Transform _parent, List<Transform> _children)
		{
			foreach (Transform trs in _parent.transform)
			{
				var persistableObject = trs.GetComponent<RuntimeTools.PersistableObject>();
				var gridObjectPose = trs.GetComponent<HexGridObjectPoseBase>();
				if (persistableObject != null || gridObjectPose != null)
				{
					_children.Add(trs);
				}
				PopulateAllStandaloneObjectsRecurse(trs, _children);
			}
		}

		// destroy all standalone objects
		void DestroyAllStandaloneObjects(Transform sceneTransform)
		{
			List<Transform> children = new List<Transform>();
			PopulateAllStandaloneObjectsRecurse(sceneTransform, children);
			foreach (var it in children)
			{
				GameObject.DestroyImmediate(it.gameObject);
			}
		}

		/*
		 * 
		 * remove object not longer tracked
		 * 
		 */

		private void RemoveObjectsNoLongerTracked(List<Transform> previousTrackedObjects, List<Transform> newTrackedObjects)
		{
			List<Transform> removeObjects = new List<Transform>();
			foreach (var trs in previousTrackedObjects)
			{
				bool Predicate(Transform go)
				{
					var persistableGO = go.GetComponent<codingVR.RuntimeTools.PersistableObject>();
					var persistableTRS = trs.GetComponent<codingVR.RuntimeTools.PersistableObject>();
					bool b = string.Compare(persistableTRS.Identifier, persistableGO.Identifier) == 0;
					return b;
				};

				var findGo = newTrackedObjects.Find(Predicate);
				if (findGo == null)
				{
					removeObjects.Add(trs);
					Debug.Log("Destroying game object " + trs.name);
				}
			}

			while(removeObjects.Count > 0)
			{
				var trs = removeObjects[0];
				removeObjects.RemoveAt(0);
				previousTrackedObjects.Remove(trs);
				GameObject.DestroyImmediate(trs.gameObject);
			}
		}

		/*
		 * 
		 * Setup gizmo
		 * 
		 */

		void SetupGizmo()
		{
			// display the name of place holder
			var gizmo = GetComponent<codingVR.Core.GameObjectDrawGizmos>();
			if (gizmo != null)
			{
				if (string.IsNullOrEmpty(rootScenePrefabAssetPath) == false)
				{
					gizmo.Title = name + "\nprefab:" + rootScenePrefabAssetPath;
				}
				else
				{
					gizmo.Title = name + "\nprefab:not defined" + rootScenePrefabAssetPath;
				}
			}
		}
		
		/*
		 * 
		 * pose object in place of placeholder
		 * 
		 */

#if UNITY_EDITOR


		private void PoseObjects(Transform rootSceneTransform)
		{
			// pose all objects
			var allGameObject = PopulateAllPrefabs(rootSceneTransform);
			foreach (var trs in allGameObject.Item2)
			{
				PoseObject(allGameObject.Item1, trs);
			}

			// find all objects tracked by the place holder
			var previousTrackedObjects = new List<Transform>();
			var persistableThis = this.GetComponent<codingVR.RuntimeTools.PersistableObject>();
			for (int i = 0; i < HexGridManager.HexGrid.GetGridChunkCount(); ++i)
			{
				var chunk = HexGridManager.HexGrid.GetGridChunk(i);
				for (var it = HexEnvironmentManagerIDs.firstIdx; it != HexEnvironmentManagerIDs.CountContainer; it++)
				{
					previousTrackedObjects.AddRange(chunk.FindChildsOfChunkEnvironement(it, persistableThis.Identifier));
				}
			}

			// remove objects no longer tracked
			RemoveObjectsNoLongerTracked(previousTrackedObjects, allGameObject.Item2);

			// we deactivate child in the place holder
			foreach (Transform child in transform)
			{
				child.gameObject.SetActive(false);
			}

			// Setup gizmo
			SetupGizmo();
		}

		private void PoseObject(Transform anchor, Transform gameObjectTarget)
		{
			if (gameObjectTarget != null)
			{
				// Create game object from prefab
				var persistableThis = this.GetComponent<codingVR.RuntimeTools.PersistableObject>();
				var persistableObject = codingVR.RuntimeTools.PersistableObject.InstantiateFromPrefab(gameObjectTarget.gameObject, persistableThis.Identifier, gameObjectTarget.transform);
				gameObjectTarget = persistableObject.gameObject.transform;

				// Destroy all children which are also somme valid prefab couples
				DestroyAllStandaloneObjects(gameObjectTarget);

				// we get the hexGridManager
				// TODO: HACK,HACK
				// TODO: PAY ATTENTION: normally the HexGridManager is assigned by Construct (Zenject DIP)
				// BUT apparently from editor mode is not running
				if (HexGridManager == null)
				{
					var hexGridManagerGameObject = GameObject.FindGameObjectWithTag("HexGridManager");
					HexGridManager = hexGridManagerGameObject.GetComponent<HexMapNavigation.IHexGridManagerOrigin>();
				}

				// if the game object is already within the scene we destroy the old one and we put the new one in place
				bool found = false;
				for (int i = 0; i < HexGridManager.HexGrid.GetGridChunkCount() && found == false; ++i)
				{
					var chunk = HexGridManager.HexGrid.GetGridChunk(i);
					for(var it = HexEnvironmentManagerIDs.firstIdx; it != HexEnvironmentManagerIDs.CountContainer && found == false; it++)
					{
						found = chunk.DestroyChildOfChunkEnvironement(it, persistableObject.Identifier);
					}
				}

				// we assign place holder as parent
				// the world position of game object follows the place holder position
#if USE_PLACE_HOLDER_PROPERTY
				gameObjectTarget.transform.SetParent(transform, false);
#else
				gameObjectTarget.transform.SetParent(transform.parent, false);
				gameObjectTarget.transform.position = gameObjectTarget.transform.position - anchor.position + transform.position;
#endif

				/*
				Type[] obj = { typeof(HexGridObjectPoseProps), typeof(HexGridObjectPoseFunction) };
				foreach (var i in obj)
				{
					var poseProps1 = gameObjectTarget.GetComponent(i);
				}
				var poseProps = gameObjectTarget.GetComponent(typeof(HexGridObjectPoseProps));
				*/

				var pose = gameObjectTarget.GetComponent<HexGridObjectPoseProps>();
				pose.UpdatePose(HexMapNavigation.HexEnvironmentManagerIDs.PropsContainerIdx);
			}
		}
#endif

		// === IHexPlaceHolder ===

		public int Type => 2;

		// === RuntimeTools.IPersistableObject ===


		/*
		 * 
		 * Save & Load
		 * 
		 */

		public void Save(codingVR.Core.GameDataWriter writer)
		{
			// Chunk object start
			writer.WriteObjectStart("Scene-Placeholder");

			// write target name
			writer.Write(this.name, "name");

			// write root scene 
			if (string.IsNullOrEmpty(rootScenePrefabAssetPath) == false)
			{
				writer.Write(rootScenePrefabAssetPath, "prefab");
			}
			else
			{
				writer.Write("", "prefab");
			}

			// Chunk object end
			writer.WriteObjectEnd();
		}

		public void Load(codingVR.Core.GameDataReader reader, int version)
		{
			if (version >= 16)
			{
				// Environment
				reader.ReadObjectStart("Scene-Placeholder");

				// read target name
				this.name = reader.ReadString();

				// read root scene 
				rootScenePrefabAssetPath = reader.ReadString();

				// Environment
				reader.ReadObjectEnd();

				// Setup gizmo
				SetupGizmo();
			}
		}
	}
}