﻿using UnityEngine;
using UnityEngine.UI;

namespace codingVR.HexMapNavigation
{
    class HexCellTagData : IHexCellTagData
    {
        IHexCellOrigin cell;

        public HexCellTagData(IHexCellOrigin cell)
        {
            this.cell = cell;
        }

        /*
         * 
         * UI
         * 
         */
         
        public string RefreshCanvasTextLabel(Text label, string textLabel)
        {
            string text = null;
            // tag or label
            if (this.Tag != null)
                text = this.Tag.ToString();
            else
            if (textLabel != null && codingVR.Data.Tag.IsTag(textLabel) == false)
                text = textLabel;

            // manage cell label
            label.color = Color.black;
            return text;
        }
        

        /*
         * 
         * Load & Save
         * 
         */

        public void Save(Core.GameDataWriter writer)
        {
            // Tags
            writer.WriteObjectStart("Cell-Tags");
            bool hasTag = tag != null ? true : false;
            writer.Write(hasTag, "hasTag");
            if (hasTag == true)
                tag.Save(writer);
            writer.WriteObjectEnd();
        }

        public void Load(Core.GameDataReader reader, int version)
        {
            int refreshCanvasTextLabel = 0;

            // Tags
            reader.ReadObjectStart("Cell-Tags");
            bool hasTag = reader.ReadBoolean();
            if (hasTag == true)
            {
                tag = Data.Tag.Load(reader, version);
                refreshCanvasTextLabel++;
            }
            reader.ReadObjectEnd();

            if (refreshCanvasTextLabel > 0)
                cell.RefreshCanvasTextLabel();
        }


        /* 
         * 
         * Tag
         * 
         */

        public Data.Tag Tag
        {
            get
            {
                return tag;
            }
            set
            {
                if (tag != value)
                {
                    tag = value;
                    cell.RefreshCanvasTextLabel();
                }
            }
        }

        private Data.Tag tag;
    }
}