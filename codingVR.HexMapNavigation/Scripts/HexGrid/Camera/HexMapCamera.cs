﻿using UnityEngine;
using Zenject;

namespace codingVR.HexMapNavigation
{
	public class HexMapCamera : MonoBehaviour
	{
		[SerializeField]
		float _stickMinZoom, _stickMaxZoom;
		
		float StickMinZoom => _stickMinZoom * HexMapNavigation.HexMetrics.size; 
		float StickMaxZoom => _stickMaxZoom * HexMapNavigation.HexMetrics.size;

		[SerializeField]
		float _swivelMinZoom, _swivelMaxZoom;

		float SwivelMinZoom => _swivelMinZoom;
		float SwivelMaxZoom => _swivelMaxZoom;


		[SerializeField]
		float _moveSpeedMinZoom, _moveSpeedMaxZoom;

		float moveSpeedMinZoom => _moveSpeedMinZoom * HexMapNavigation.HexMetrics.size;
		float moveSpeedMaxZoom => _moveSpeedMaxZoom * HexMapNavigation.HexMetrics.size;

		[SerializeField]
		float rotationSpeed;

		Transform swivel, stick;

		HexMapNavigation.IHexGridManagerOrigin hexGridManager;
		
		float zoom = 1f;

		float rotationAngle;


		[Inject]
		public void Construct(HexMapNavigation.IHexGridManagerOrigin hexGridManager)
		{
			this.hexGridManager = hexGridManager;
		}
		
		public bool Locked
		{
			set
			{
				enabled = !value;
			}
		}

		public void ValidatePosition()
		{
			AdjustPosition(0f, 0f);
		}

		void Awake()
		{
			swivel = transform.GetChild(0);
			stick = swivel.GetChild(0);
		}

		void OnEnable()
		{
			ValidatePosition();
		}

		protected static readonly Vector3 HORIZONTAL_PLANE = new Vector3(1, 0, 1);

		void Update()
		{
			float zoomDelta = Input.GetAxis("Mouse ScrollWheel");
			if (zoomDelta != 0f)
			{
				AdjustZoom(zoomDelta);
			}

			float rotationDelta = Input.GetAxis("Rotation");
			if (rotationDelta != 0f)
			{
				AdjustRotation(rotationDelta);
			}

			float xDelta = Input.GetAxis("Horizontal");
			float zDelta = Input.GetAxis("Vertical");
			if (xDelta != 0f || zDelta != 0f)
			{
				AdjustPosition(xDelta, zDelta);
			}

			/*
			var player = gameManager.Player;
			Vector3 localPlayer = player.transform.position;
			localPlayer = Vector3.Scale(localPlayer, HORIZONTAL_PLANE);

			Vector3 localCamera = transform.position;
			localCamera = Vector3.Scale(localCamera, HORIZONTAL_PLANE);

			Vector3 Delta = (localPlayer - localCamera);
			//transform.position += Delta;

			AdjustPosition2(Delta.x, Delta.z);
			*/
		}

		void AdjustZoom(float delta)
		{
			zoom = Mathf.Clamp01(zoom + delta);

			float distance = Mathf.Lerp(StickMinZoom, StickMaxZoom, zoom);
			stick.localPosition = new Vector3(0f, 0f, distance);

			float angle = Mathf.Lerp(SwivelMinZoom, SwivelMaxZoom, zoom);
			swivel.localRotation = Quaternion.Euler(angle, 0f, 0f);
		}

		void AdjustRotation(float delta)
		{
			rotationAngle += delta * rotationSpeed * Time.deltaTime;
			if (rotationAngle < 0f)
			{
				rotationAngle += 360f;
			}
			else if (rotationAngle >= 360f)
			{
				rotationAngle -= 360f;
			}
			transform.localRotation = Quaternion.Euler(0f, rotationAngle, 0f);
		}

		void AdjustPosition(float xDelta, float zDelta)
		{
			Vector3 direction =
				transform.localRotation *
				new Vector3(xDelta, 0f, zDelta).normalized;
			float damping = Mathf.Max(Mathf.Abs(xDelta), Mathf.Abs(zDelta));
			float distance =
				Mathf.Lerp(moveSpeedMinZoom, moveSpeedMaxZoom, zoom) *
				damping * Time.deltaTime;

			Vector3 position = transform.localPosition;
			position += direction * distance;
			transform.localPosition =
				hexGridManager.HexGrid.Wrapping ? WrapPosition(position) : ClampPosition(position);
		}

		void AdjustPosition2(float xDelta, float zDelta)
		{
			Vector3 direction =
				//transform.localRotation *
				new Vector3(xDelta, 0f, zDelta).normalized;
			float damping = Mathf.Max(Mathf.Abs(xDelta), Mathf.Abs(zDelta));
			float distance = Vector3.Magnitude(new Vector3(xDelta, 0f, zDelta));

			//Mathf.Lerp(moveSpeedMinZoom, moveSpeedMaxZoom, zoom) *
			  //  damping * Time.deltaTime;

			Vector3 position = transform.localPosition;
			position += direction * distance;
			transform.localPosition =
				hexGridManager.HexGrid.Wrapping ? WrapPosition(position) : ClampPosition(position);
		}


		Vector3 ClampPosition(Vector3 position)
		{
			float xMax = (hexGridManager.HexGrid.CellCountX - 0.5f) * HexMapNavigation.HexMetrics.innerDiameter;
			position.x = Mathf.Clamp(position.x, 0f, xMax);

			float zMax = (hexGridManager.HexGrid.CellCountZ - 1) * (1.5f * HexMapNavigation.HexMetrics.outerRadius);
			position.z = Mathf.Clamp(position.z, 0f, zMax);

			return position;
		}

		Vector3 WrapPosition(Vector3 position)
		{
			float width = hexGridManager.HexGrid.CellCountX * HexMapNavigation.HexMetrics.innerDiameter;
			while (position.x < 0f)
			{
				position.x += width;
			}
			while (position.x > width)
			{
				position.x -= width;
			}

			float zMax = (hexGridManager.HexGrid.CellCountZ - 1) * (1.5f * HexMapNavigation.HexMetrics.outerRadius);
			position.z = Mathf.Clamp(position.z, 0f, zMax);

			hexGridManager.HexGrid.CenterMap(position.x);
			return position;
		}
	}
}