﻿using UnityEngine;

namespace codingVR.HexMapNavigation
{
	public enum HexDirection
	{
		NE, E, SE, SW, W, NW
	}
}

public static class HexDirectionExtensions
{
	public const int Count = 6;

	public static codingVR.HexMapNavigation.HexDirection RandomValue
	{
		get
		{
			return (codingVR.HexMapNavigation.HexDirection)Random.Range(0, Count);
		}
	}

	public static codingVR.HexMapNavigation.HexDirection Opposite(this codingVR.HexMapNavigation.HexDirection direction)
	{
		return (int)direction < 3 ? (direction + 3) : (direction - 3);
	}

	public static codingVR.HexMapNavigation.HexDirection Previous(this codingVR.HexMapNavigation.HexDirection direction)
	{
		return direction == codingVR.HexMapNavigation.HexDirection.NE ? codingVR.HexMapNavigation.HexDirection.NW : (direction - 1);
	}

	public static codingVR.HexMapNavigation.HexDirection Next(this codingVR.HexMapNavigation.HexDirection direction)
	{
		return direction == codingVR.HexMapNavigation.HexDirection.NW ? codingVR.HexMapNavigation.HexDirection.NE : (direction + 1);
	}

	public static codingVR.HexMapNavigation.HexDirection Previous2(this codingVR.HexMapNavigation.HexDirection direction)
	{
		direction -= 2;
		return direction >= codingVR.HexMapNavigation.HexDirection.NE ? direction : (direction + 6);
	}

	public static codingVR.HexMapNavigation.HexDirection Next2(this codingVR.HexMapNavigation.HexDirection direction)
	{
		direction += 2;
		return direction <= codingVR.HexMapNavigation.HexDirection.NW ? direction : (direction - 6);
	}

	private static codingVR.Core.IntVector3[] vectors = {
		new codingVR.Core.IntVector3(  1,  -1,  0),  // E
		new codingVR.Core.IntVector3(  1,   0, -1),  // SE
		new codingVR.Core.IntVector3(  0,   1, -1),  // SW
		new codingVR.Core.IntVector3( -1,   1,  0),   // W
		new codingVR.Core.IntVector3( -1,   0,  1),   // NW
		new codingVR.Core.IntVector3(  0,  -1,  1),  // NE
		};

	public static codingVR.Core.IntVector3 ToIntVector2(this codingVR.HexMapNavigation.HexDirection direction)
	{
		return vectors[(int)direction];
	}

}
