﻿using UnityEngine;

namespace codingVR.HexMapNavigation
{
	public static class HexMetrics
	{
		// hex cell proportion
		public const float outerToInner = 0.866025404f;
		public const float innerToOuter = 1f / outerToInner;

		// set hex cell size
		public static void SetSize(float value)
		{
			Shader.DisableKeyword("OUTER_RADIUS_IS_10d0");
			Shader.DisableKeyword("OUTER_RADIUS_IS_2d0");
			Shader.DisableKeyword("OUTER_RADIUS_IS_1d0");
			Shader.DisableKeyword("OUTER_RADIUS_IS_0d5");

			if (Mathf.Approximately(value, 0.5f))
			{
				size = 0.5f;
				Shader.EnableKeyword("OUTER_RADIUS_IS_0d5");
			}
			else
			if (Mathf.Approximately(value, 1.0f))
			{
				size = 1.0f;
				Shader.EnableKeyword("OUTER_RADIUS_IS_1d0");
			}
			else
			if (Mathf.Approximately(value, 2.0f))
			{
				size = 2.0f;
				Shader.EnableKeyword("OUTER_RADIUS_IS_2d0");
			}
			else
			if (Mathf.Approximately(value, 10.0f))
			{
				size = 10.0f;
				Shader.EnableKeyword("OUTER_RADIUS_IS_10d0");
			}
			else
			{
				Debug.LogErrorFormat("{0} hex cell size {1} not specified !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, value);
			}
		}

		// hex cell size
		public static float size = 10.0f;
		public static float scale => size / 10.0f;

		// hex chunks
		public static int chunkSizeX = 5, chunkSizeZ = 5;

		// hex cell radius
		public static float outerRadius => size;
		public static float innerRadius => outerRadius * outerToInner;
		public static float innerDiameter => innerRadius * 2f;

		/*              0
		 *             ***
		 *          **     **   
		 *      5**           **1
		 *      *               *
		 *      *               *   
		 *      *               *   
		 *      4**           **2
		 *          **     **   
		 *             ***
		 *              3
		 */

		public static Vector3[] corners =>
		new Vector3[]
			{
				new Vector3(0f, 0f, outerRadius),
				new Vector3(innerRadius, 0f, 0.5f * outerRadius),
				new Vector3(innerRadius, 0f, -0.5f * outerRadius),
				new Vector3(0f, 0f, -outerRadius),
				new Vector3(-innerRadius, 0f, -0.5f * outerRadius),
				new Vector3(-innerRadius, 0f, 0.5f * outerRadius),
				new Vector3(0f, 0f, outerRadius)
			};

		public static int wrapSize;

		public static bool Wrapping
		{
			get
			{
				return wrapSize > 0;
			}
		}

		public static float ComputeHeight(float elevation, float elevationStep)
		{
			float y = elevation * elevationStep * scale;
			return y;
		}
	}
}