﻿using UnityEngine.Scripting.APIUpdating;
using UnityEngine;
using System.Collections;
using Zenject;

namespace codingVR.HexMapNavigation
{
	[MovedFrom(false, null, null, "HexUnit")]
	public class HexGridUnit : MonoBehaviour, IHexGridUnit
	{
		// Location

		// Hex Grid
		private IHexGridManagerOrigin hexGridManager;

		// === Constructor ===

		[Inject]
		public void Construct(IHexGridManagerOrigin hexGridManager)
		{
			this.hexGridManager = hexGridManager;
		}

		// === Transform & Position ===

		public Transform UnitTransform
		{
			get
			{
				return transform;
			}
		}
		
		// === Life cycle ===

		private void Awake()
		{
		}

		void OnEnable()
		{
		}

		// draw path
		void DrawPath(HexGridPathDetails prevPath, HexGridPathDetails path, HexGridPathDetails nextPath, Color color)
		{
			if (path != null && path.Count != 0)
			{
				var restoreColor = GUI.color;
				Gizmos.color = color;

				// TODO : 0.1f is too much for Android in term of performances
				float accuracy = 0.15f;
				HexGridPathDetails.ComputeCurve(prevPath, path, nextPath, accuracy, node => Gizmos.DrawSphere(node, 0.75f));

				Gizmos.color = restoreColor;
			}
		}

		// draw debug inormation on path in editor mode
		void OnDrawGizmos()
		{
			DrawPath(null, PreviousPathToTravel, PathToTravel, Color.gray);
			DrawPath(PreviousPathToTravel, PathToTravel, NextPathToTravel, Color.green);
			DrawPath(PathToTravel, NextPathToTravel, null, Color.blue);
		}

		// === Location ===

		private IHexCellOrigin AssignLocation(IHexCellOrigin myLocation)
		{
			if (myLocation == null)
			{
				myLocation = hexGridManager.HexGrid.GetCell(gameObject.transform.localPosition);
				Debug.LogError("HexUnit.AssignLocation : unit is assigned to first cell ...");
			}
			return myLocation;
		}
		
		public IHexCellOrigin HexGridCellCurrent
		{
			get
			{
				return hexGridManager.HexGrid.GetCell(transform.position);
			}
		}
		
		// === path ===

		protected static readonly Vector3 HORIZONTAL_PLANE = new Vector3(1, 0, 1);

		public GameEngine.ICharacterMoving MoveOnPathBegin(HexGridPathDetails path)
		{
			GameEngine.ICharacterMoving moveOnPath = null;
			// update final location
			if (path != null)
			{
				PathToTravel = path;
				moveOnPath = HexUnitMoveOnPath.Create(this);
			}
			return moveOnPath;
		}

		// === path selection ===

		private HexGridPathDetails currentPathToTravel;
		private HexGridPathDetails nextPathToTravel;

		public HexGridPathDetails PathToTravel
		{
			get
			{
				return currentPathToTravel;
			}
			set
			{
				if (Object.Equals(currentPathToTravel, value) == false)
				{
					// manage next path to travel
					if (nextPathToTravel != null && value != null)
					{
						// to insure continuity between path; we must check if the current path is linked to next path (value)        
						if (Object.Equals(nextPathToTravel[0], value[value.Count - 1]) == false)
						{
							nextPathToTravel = null;
						}
					}

					// manage previous path to travel
					if (currentPathToTravel != null && value != null)
					{
						// to insure continuity between path; we must check if the current path is linked to next path (value)        
						if (Object.Equals(currentPathToTravel[currentPathToTravel.Count - 1], value[0]) == false)
						{
							PreviousPathToTravel = null;
							currentPathToTravel = value;
							return;
						}
					}
					PreviousPathToTravel = currentPathToTravel;
					currentPathToTravel = value;
				}
			}
		}
		
		public HexGridPathDetails NextPathToTravel
		{
			get
			{
				return nextPathToTravel;
			}
			set
			{
				if (Object.Equals(nextPathToTravel, value) == false)
				{
					if (currentPathToTravel != null && value != null)
					{
						// to insure continuity between path; we must check if the current path is linked to next path (value)        
						if (Object.Equals(currentPathToTravel[currentPathToTravel.Count - 1], value[0]) == false)
						{
							nextPathToTravel = null;
							return;
						}
					}
					nextPathToTravel = value;
				}
			}
		}

		public HexGridPathDetails PreviousPathToTravel { get; private set; }

		// === Teleportation ===

		public void Teleport(IHexCellOrigin cell)
		{
			// we fill up the final location
			transform.position = cell.Position;
		}
		public void Teleport(Data.Tag tag)
		{
			// we fill up the final location
			IHexCellOrigin cell = hexGridManager.HexGrid.FindCellFromTag(tag, false);
			if (cell != null)
				Teleport(cell);
			else
				Debug.LogErrorFormat("{0} : cell from tag {1} not found ", System.Reflection.MethodBase.GetCurrentMethod().Name, tag.TagName);
		}

		public void Teleport(Data.TagPath path)
		{
			// we fill up the final location
			IHexCellOrigin cell = hexGridManager.HexGrid.FindCellFromTag(path.tag, false);
			if (cell != null)
				Teleport(cell);
			else
				Debug.LogErrorFormat("{0} : cell from tag {1} not found ", System.Reflection.MethodBase.GetCurrentMethod().Name, path.tag.TagName);
		}
	}
}