﻿using UnityEngine;
using UnityEngine.Scripting.APIUpdating;
using Zenject;

namespace codingVR.HexMapNavigation
{
	[MovedFrom(false, null, null, "PlayerTravel")]
	public class HexGridPlayerTravel : HexGridCharacterTravel
	{
		// === Unity3d lifecycle ===

		// Start is called before the first frame update
		void Start()
		{}

		// Update is called once per frame
		void Update()
		{}

	
		// === Dip Constructor ===

		[Inject]
		void Construct(IHexGridManagerOrigin hexGridManager)
		{
			base.Construct(hexGridManager);
		}
   }
}
