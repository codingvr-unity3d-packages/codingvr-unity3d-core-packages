﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace codingVR.HexMapNavigation
{
	public class HexGridPlayerFactory : IFactory<UnityEngine.Object, codingVR.GameEngine.ICharacterTravel>
	{
		DiContainer container;

		public HexGridPlayerFactory(DiContainer container)
		{
			this.container = container;
		}

		public codingVR.GameEngine.ICharacterTravel Create(UnityEngine.Object playerPrefab)
		{
			return container.InstantiatePrefabForComponent<HexGridPlayerTravel>(playerPrefab);
		}
	}
}