﻿using UnityEngine;
using UnityEngine.Scripting.APIUpdating;

namespace codingVR.HexMapNavigation
{
	[MovedFrom(false, null, null, "CharacterTravel")]
	public class HexGridCharacterTravel : MonoBehaviour, GameEngine.ICharacterTravel
	{
		// hex grid manager
		IHexGridManagerOrigin hexGridManager;

		// === Unity3d lifecycle ===

		// Start is called before the first frame update
		void Start()
		{}

		// Update is called once per frame
		void Update()
		{}

		// === Constructor ===
		protected void Construct(IHexGridManagerOrigin hexGridManager)
		{
			this.hexGridManager = hexGridManager;
		}

		// === GameEngine.ICharacterStates ===

		public void Teleport(Data.Tag tag)
		{
			var hexUnit = this.gameObject.GetComponent<IHexGridUnit>();
			Debug.Assert(hexUnit != null);
			hexUnit.Teleport(tag);
		}

		public void Teleport(Data.TagPath tag)
		{
			var hexUnit = this.gameObject.GetComponent<IHexGridUnit>();
			Debug.Assert(hexUnit != null);
			hexUnit.Teleport(tag);
		}

		public void SelectPath(Data.TagPair tagPair)
		{
			// select path
			var hexUnit = this.gameObject.GetComponent<IHexGridUnit>();
			Debug.Assert(hexUnit != null);
			hexGridManager.SelectedHexCellLink = tagPair;
			hexGridManager.UnitSelectPath(hexUnit, tagPair);
		}

		public void SelectPath(Data.TagPathPair tagPathPair)
		{
			// select path
			var hexUnit = this.gameObject.GetComponent<IHexGridUnit>();
			Debug.Assert(hexUnit != null);
			hexGridManager.SelectedHexCellLink = tagPathPair.TagPair;
			hexGridManager.UnitSelectPath(hexUnit, tagPathPair.TagPair);
		}

		public void SelectNextPath(Data.TagPair tagPair)
		{
			// select next path
			var hexUnit = this.gameObject.GetComponent<IHexGridUnit>();
			Debug.Assert(hexUnit != null);
			hexGridManager.UnitSelectNextPath(hexUnit, tagPair);
		}

		public GameEngine.ICharacterMoving MoveOnPathBegin(bool reverse)
		{
			// select path
			var hexUnit = this.gameObject.GetComponent<IHexGridUnit>();
			Debug.Assert(hexUnit != null);
			return hexGridManager.UnitMoveOnPathBegin(hexUnit);
		}

		// Game object
		public GameObject GameObject
		{
			get
			{
				return gameObject;
			}
		}
	}
}
