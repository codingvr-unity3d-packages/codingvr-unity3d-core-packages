/// <summary>
/// The script gives you choice to whether to build addressable bundles when clicking the build button.
/// For custom build script, call PreExport method yourself.
/// For cloud build, put BuildAddressablesProcessor.PreExport as PreExport command.
/// Discussion: https://forum.unity.com/threads/how-to-trigger-build-player-content-when-build-unity-project.689602/
/// </summary>
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEngine;
using UnityEditor.Callbacks;
using System.Threading;
using System.IO;
using MS.Shell.Editor;

public static class BuildAddressablesProcessor
{
	/// <summary>
	/// Run a clean build before export.
	/// </summary>
	static public void BuildAddressables()
	{
		Debug.Log("BuildAddressablesProcessor.PreExport start");
		AddressableAssetSettings.CleanPlayerContent(
			AddressableAssetSettingsDefaultObject.Settings.ActivePlayerDataBuilder);
		AddressableAssetSettings.BuildPlayerContent();
		Debug.Log("BuildAddressablesProcessor.PreExport done");
	}

	[InitializeOnLoadMethod]
	private static void Initialize()
	{
		BuildPlayerWindow.RegisterBuildPlayerHandler(BuildPlayerHandler);
	}

	[MenuItem("Assets/CodingVR/Build and upload addressables")]
	private static void BuildAndUploadAddressables()
	{
		// Display dialog
		if (EditorUtility.DisplayDialog("Build with Addressables", "Do you want to build a clean addressables before export?", "Build with Addressables", "Skip"))
		{
			// build addressables
			BuildAddressables();

			// Execute command
			string cmdPath = Path.Combine(GetApplicationPath(), "utils/AddressablesRemoteWithGoogleCloud/");
			string workinPath = GetApplicationPath();
			UploadAddressables(cmdPath, workinPath);
		}
	}
	
	private static void BuildPlayerHandler(BuildPlayerOptions options)
	{
		// Build and upload addressables
		BuildAndUploadAddressables();

		// Build player
		BuildPlayerWindow.DefaultBuildMethods.BuildPlayer(options);
	}

	private static string GetApplicationPath()
	{
		string projectFolder = Path.Combine(Application.dataPath, "../");
		projectFolder = Path.GetFullPath(projectFolder);
		return projectFolder;
	}

	[PostProcessBuildAttribute(1)]
	public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
	{
		Debug.Log(pathToBuiltProject);
		Debug.Log(Application.dataPath);
	}
	
	// https://answers.unity.com/questions/1127023/running-command-line-action-through-c-script.html
	private static void UploadAddressables(string cmdPath, string workinPath)
	{
		var thread = new Thread(delegate () 
		{
			UploadAddressablesAsync(cmdPath, workinPath);
		});
		thread.Start();
	}

	public static void UploadAddressablesAsync(string cmdPath, string workingPath)
	{
		EditorShell.Options options = new EditorShell.Options();
		options.workDirectory = workingPath;
		string fullCmd = cmdPath + @"updateGCP.cmd " + workingPath + "ServerData/*";
		var operation = EditorShell.Execute(fullCmd, options);
		operation.onExit += (exitCode) => {
			Debug.LogError("ExecuteCommand : exit code " + exitCode);
		};
		operation.onLog += (EditorShell.LogType LogType, string log) => {
			Debug.Log(log);
		};
		return;
	}
}