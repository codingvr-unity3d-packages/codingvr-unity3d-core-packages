﻿namespace codingVR.Core
{
    [System.Serializable]
    public struct IntVector3
    {
        public int x, y, z;

        public IntVector3(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static IntVector3 operator +(IntVector3 a, IntVector3 b)
        {
            a.x += b.x;
            a.y += b.y;
            a.z += b.z;
            return a;
        }

        public static IntVector3 operator -(IntVector3 a, IntVector3 b)
        {
            a.x -= b.x;
            a.y -= b.y;
            a.z -= b.z;
            return a;
        }

        public static int Length2(IntVector3 a)
        {
            return a.x * a.x + a.y * a.y + a.z * a.z;
        }
    }
}