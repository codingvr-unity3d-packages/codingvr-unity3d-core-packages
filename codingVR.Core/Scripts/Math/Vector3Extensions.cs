﻿using UnityEngine;

// see also https://github.com/Elideb/UnExt/tree/master/Scripts

public static class Vector3Extension
{
	public static Vector2 xy(this Vector3 v)
	{
		return new Vector2(v.x, v.y);
	}

	public static Vector2 xz(this Vector3 v)
	{
		return new Vector2(v.x, v.z);
	}
	   
	public static Vector3 fromXZ(Vector2 v)
	{
		return new Vector3(v.x, 0.0f, v.y);
	}
	
	public static Vector3 WithX(this Vector3 v, float x)
	{
		return new Vector3(x, v.y, v.z);
	}

	public static Vector3 WithY(this Vector3 v, float y)
	{
		return new Vector3(v.x, y, v.z);
	}

	public static Vector3 WithZ(this Vector3 v, float z)
	{
		return new Vector3(v.x, v.y, z);
	}

	public static Vector2 WithX(this Vector2 v, float x)
	{
		return new Vector2(x, v.y);
	}

	public static Vector2 WithY(this Vector2 v, float y)
	{
		return new Vector2(v.x, y);
	}

	public static Vector3 WithZ(this Vector2 v, float z)
	{
		return new Vector3(v.x, v.y, z);
	}

	// axisDirection - unit vector in direction of an axis (eg, defines a line that passes through zero)
	// point - the point to find nearest on line for
	public static Vector3 NearestPointOnAxis(this Vector3 axisDirection, Vector3 point, bool isNormalized = false)
	{
		if (!isNormalized) axisDirection.Normalize();
		var d = Vector3.Dot(point, axisDirection);
		return axisDirection * d;
	}

	// lineDirection - unit vector in direction of line
	// pointOnLine - a point on the line (allowing us to define an actual line in space)
	// point - the point to find nearest on line for
	public static Vector3 NearestPointOnLine(this Vector3 lineDirection, Vector3 point, Vector3 pointOnLine, bool isNormalized = false)
	{
		if (!isNormalized) lineDirection.Normalize();
		var d = Vector3.Dot(point - pointOnLine, lineDirection);
		return pointOnLine + (lineDirection * d);
	}

	// see https://github.com/Unity-Technologies/UnityCsReference/blob/master/Editor/Mono/HandleUtility.cs
	// Project /point/ onto a line.
	public static Vector3 ProjectPointLine(Vector3 point, Vector3 lineStart, Vector3 lineEnd)
	{
		Vector3 relativePoint = point - lineStart;
		Vector3 lineDirection = lineEnd - lineStart;
		float length = lineDirection.magnitude;
		Vector3 normalizedLineDirection = lineDirection;
		if (length > .000001f)
			normalizedLineDirection /= length;

		float dot = Vector3.Dot(normalizedLineDirection, relativePoint);
		dot = Mathf.Clamp(dot, 0.0F, length);

		return lineStart + normalizedLineDirection * dot;
	}

	// see https://github.com/Unity-Technologies/UnityCsReference/blob/master/Editor/Mono/HandleUtility.cs
	// Calculate distance between a point and a line.
	public static float DistancePointLine(Vector3 point, Vector3 lineStart, Vector3 lineEnd)
	{
		return Vector3.Magnitude(ProjectPointLine(point, lineStart, lineEnd) - point);
	}

	// see https://stackoverflow.com/questions/217578/how-can-i-determine-whether-a-2d-point-is-within-a-polygon
	public static bool IsInConvexPolygonXZ(this Vector3 point, Vector3 center, Vector3[] corners)
	{
		bool c = false;
		int i, j = 0;
		for (i = 0, j = corners.Length - 1; i < corners.Length; j = i++)
		{
			Vector3 corner0 = corners[i] + center;
			Vector3 corner1 = corners[j] + center;
			if (((corner0.z > point.z) != (corner1.z > point.z)) && (point.x < (corner1.x - corner0.x) * (point.z - corner0.z) / (corner1.z - corner0.z) + corner0.x))
			{
				c = !c;
			}
		}
		return c;
	}

	public static bool IsInConvexPolygonXZ(this Vector3 point, Vector3[] corners)
	{
		bool c = false;
		int i, j = 0;
		for (i = 0, j = corners.Length - 1; i < corners.Length; j = i++)
		{
			Vector3 corner0 = corners[i];
			Vector3 corner1 = corners[j];
			if (((corner0.z > point.z) != (corner1.z > point.z)) && (point.x < (corner1.x - corner0.x) * (point.z - corner0.z) / (corner1.z - corner0.z) + corner0.x))
			{
				c = !c;
			}
		}
		return c;
	}

	public static bool IsTrianglesXZ(this Vector3 point, Vector3 corners0, Vector3 corners1, Vector3 corners2)
	{
		bool test(Vector3 pt0, Vector3 pt1)
		{
			return (((pt0.z > point.z) != (pt1.z > point.z)) && (point.x < (pt1.x - pt0.x) * (point.z - pt0.z) / (pt1.z - pt0.z) + pt0.x));
		}

		bool c = false;
		if (test(corners0, corners2) == true)
			c = !c;
		if (test(corners1, corners0) == true)
			c = !c;
		if (test(corners2, corners1) == true)
			c = !c;

		return c;
	}
	
	public static bool HasMagnitude(this Vector3 vec)
	{
		return (Mathf.Abs(vec.x) + Mathf.Abs(vec.y) + Mathf.Abs(vec.z)) > 0;
	}

	public static bool IsZero(this Vector3 vec)
	{
		return (Mathf.Approximately(vec.x, 0.0f) && Mathf.Approximately(vec.y, 0.0f) && Mathf.Approximately(vec.z, 0.0f));
	}
	
	public static Vector3 ScreenToWorld(this Vector3 vec)
	{
		Camera camera = Camera.current;
		return camera.ScreenToWorldPoint(vec);
	}

	public static Vector3 WorldToScreen(this Vector3 vec)
	{
		Camera camera = Camera.current;
		var screenPoint = camera.WorldToScreenPoint(vec);
		return screenPoint;
	}

	/// returns the smallest angle between ABC. Never greater than 180
	public static float MinAngle(Vector3 a, Vector3 b, Vector3 c)
	{
		return Vector3.Angle((a - b), (c - b));
	}
}