﻿using UnityEngine;

// https://forum.unity.com/threads/script-for-generating-continuous-random-range-values.539122/

namespace codingVR.Core
{
	public static class RandomContinuous
	{
		private static Random.State randomState;

		public static void InitState(int seed)
		{
			Random.InitState(seed);
			randomState = Random.state;
		}

		public static int Range(int min, int max)
		{
			Random.state = randomState;
			int selectedRandom = Random.Range(min, max);
			Random.InitState(Random.Range(int.MinValue, int.MaxValue));
			randomState = Random.state;
			return selectedRandom;
		}

		public static float Range(float min, float max)
		{
			Random.state = randomState;
			float selectedRandom = Random.Range(min, max);
			Random.InitState(Random.Range(int.MinValue, int.MaxValue));
			randomState = Random.state;
			return selectedRandom;
		}
	}
}