﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Core
{
	public class RandomHash
	{
		// hash
		public const int hashGridSize = 256;
		public const float hashGridScale = 1.0f;
				
		float[] hashGrid;

		void InitializeHashGrid(int seed)
		{
			hashGrid = new float[hashGridSize * hashGridSize];
			Random.State currentState = Random.state;
			Random.InitState(seed);
			for (int i = 0; i < hashGrid.Length; i++)
			{
				hashGrid[i] = Random.value * 0.999f;
			}
			Random.state = currentState;
		}

		public RandomHash(int seed)
		{
			InitializeHashGrid(seed);
		}

		public float Value(float ix, float iy)
		{
			int x = (int)(ix * hashGridScale) % hashGridSize;
			if (x < 0)
			{
				x += hashGridSize;
			}
			int y = (int)(iy * hashGridScale) % hashGridSize;
			if (y < 0)
			{
				y += hashGridSize;
			}
			return hashGrid[x + y * hashGridSize];
		}

		public int Range(float ix, float iy, int min, int max)
		{
			var v = Value(ix, iy);
			return (int)(v * (max - min) + min);
		}
	}
}