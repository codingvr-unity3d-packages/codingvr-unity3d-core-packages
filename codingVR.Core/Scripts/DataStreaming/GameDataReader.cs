﻿using System;
using System.IO;
using UnityEngine;

namespace codingVR.Core
{
    public class GameDataReader
    {
        BinaryReader binaryReader;
        LitJson.JsonReader jsonReader;

        public GameDataReader(BinaryReader reader)
        {
            this.binaryReader = reader;
        }

        public GameDataReader(LitJson.JsonReader reader)
        {
            this.jsonReader = reader;
        }

        public void Open()
        {
            if (jsonReader != null)
            {
                if (jsonReader.Read() == true)
                {
                    if (jsonReader.Value == null)
                    {

                    }
                }
            }
        }

        public void Close()
        {

        }

        private string ReadPropertyName()
        {
            string value;
            if (jsonReader.Read() == true)
            {
                if (jsonReader.Value != null && jsonReader.Token == LitJson.JsonToken.PropertyName)
                {
                    value = (string)jsonReader.Value;
                }
                else
                {
                    throw new Exception("GameDataReader.ReadPropertyName : reading has failed; bad type !!!");
                }
            }
            else
            {
                throw new Exception("GameDataReader.ReadPropertyName : end of file reached !!!");
            }
            return value;
        }
        
        private T ReadValue<T>(LitJson.JsonToken token)
        {
            T value;
            if (jsonReader.Read() == true)
            {
                if (jsonReader.Value != null && jsonReader.Token == token)
                {
                    var type = jsonReader.Value.GetType();
                    value = (T)jsonReader.Value;
                }
                else
                {
                    throw new Exception("GameDataReader : reading has failed; bad type !!!");
                }
            }
            else
            {
                throw new Exception("GameDataReader : end of file reached !!!");
            }
            return value;
        }

        private T ReadObject<T>()
        {
            T value;
            if (jsonReader.Read() == true)
            {
                value = LitJson.JsonMapper.ToObject<T>(jsonReader);
            }
            else
            {
                throw new Exception("GameDataReader : end of file reached !!!");
            }
            return value;
        }
        
        public string ReadString()
        {
            string value = "";
            if (binaryReader != null)
            {
                return binaryReader.ReadString();
            }
            if (jsonReader != null)
            {
                ReadPropertyName();
                value = ReadValue<string>(LitJson.JsonToken.String);
            }
            return value;
        }

        public float ReadFloat()
        {
            float value = 0.0f;
            if (binaryReader != null)
            {
                value = binaryReader.ReadSingle();
            }
            if (jsonReader != null)
            {
                ReadPropertyName();
                value = (float)ReadValue<double>(LitJson.JsonToken.Real);
            }
            return value;
        }

        public int ReadInt()
        {
            int value = 0;
            if (binaryReader != null)
            {
                value = binaryReader.ReadInt32();
            }
            if (jsonReader != null)
            {
                ReadPropertyName();
                value = (int)ReadValue<Int64>(LitJson.JsonToken.Natural);
            }
            return value;
        }

        public bool ReadBoolean()
        {
            bool value = false;
            if (binaryReader != null)
            {
                value = binaryReader.ReadBoolean();
            }
            if (jsonReader != null)
            {
                ReadPropertyName();
                value = ReadValue<bool>(LitJson.JsonToken.Boolean);
            }
            return value;
        }

        public byte ReadByte()
        {
            byte value = 0;
            if (binaryReader != null)
            {
                value = binaryReader.ReadByte();
            }

            if (jsonReader != null)
            {
                ReadPropertyName();
                value = (byte)ReadValue<Int64>(LitJson.JsonToken.Natural);
            }
            return value;
        }

        public Quaternion ReadQuaternion()
        {
            Quaternion value = new Quaternion();
            if (binaryReader != null)
            {
                value.x = binaryReader.ReadSingle();
                value.y = binaryReader.ReadSingle();
                value.z = binaryReader.ReadSingle();
                value.w = binaryReader.ReadSingle();
            }

            if (jsonReader != null)
            {
                value = ReadObject<Quaternion>();
            }
            return value;
        }

        public Vector3 ReadVector3()
        {
            Vector3 value = new Vector3();
            if (binaryReader != null)
            {
                value.x = binaryReader.ReadSingle();
                value.y = binaryReader.ReadSingle();
                value.z = binaryReader.ReadSingle();
            }

            if (jsonReader != null)
            {
                value = ReadObject<Vector3>();
            }
            return value;
        }

        public void ReadObjectStart()
        {
            if (jsonReader != null)
            {
                if (jsonReader.Read() == true)
                {
                    if (/*jsonReader.Value != null && */jsonReader.Token == LitJson.JsonToken.ObjectStart)
                    {
                    }
                    else
                    {
                        throw new Exception("GameDataReader.ReadObjectStart : reading has failed; bad type !!!");
                    }
                }
                else
                {
                    throw new Exception("GameDataReader.ReadObjectStart : end of file reached !!!");
                }
            }
        }

        public void ReadObjectEnd()
        {
            if (jsonReader != null)
            {
                if (jsonReader.Read() == true)
                {
                    if (/*jsonReader.Value != null && */jsonReader.Token == LitJson.JsonToken.ObjectEnd)
                    {
                    }
                    else
                    {
                        throw new Exception("GameDataReader.ReadObjectEnd : reading has failed; bad type !!!");
                    }
                }
                else
                {
                    throw new Exception("GameDataReader.ReadObjectEnd : end of file reached !!!");
                }
            }
        }

        public void ReadObjectStart(string propertyName, bool checkPropetryName = true)
        {
            if (jsonReader != null)
            {
                if (checkPropetryName == false || (ReadPropertyName() == propertyName && checkPropetryName == true))
                {
                    ReadObjectStart();
                }
                else
                {
                    throw new Exception("GameDataReader.ReadObjectStart : reading has failed; bad name !!!");
                }
            }
        }
    }
}