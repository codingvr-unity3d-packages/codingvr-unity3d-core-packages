﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace codingVR.Core
{
    public class GameDataWriter
    {
        BinaryWriter binaryWriter;
        LitJson.JsonWriter jsonWriter;
        int opened = 0;

        Stack<HashSet<string>> stack = new Stack<HashSet<string>>();

        public GameDataWriter(BinaryWriter writer)
        {
            this.binaryWriter = writer;
        }

        public GameDataWriter(LitJson.JsonWriter writer)
        {
            this.jsonWriter = writer;
            this.jsonWriter.PrettyPrint = true;
        }

        public void Open()
        {
            opened++;
            Validation();
            WriteObjectStart();
        }

        public void Close()
        {
            Validation();
            WriteObjectEnd();
            opened--;
        }
        
        public void Write(string value, string propName = "string")
        {
            Validation();
            if (this.binaryWriter != null)
            {
                this.binaryWriter.Write(value);
            }

            if (this.jsonWriter != null)
            {
                this.jsonWriter.WritePropertyName(GetId(propName));
                this.jsonWriter.Write(value);
            }
        }

        public void Write(float value, string propName = "float")
        {
            Validation();
            if (this.binaryWriter != null)
            {
                this.binaryWriter.Write(value);
            }

            if (this.jsonWriter != null)
            {
                this.jsonWriter.WritePropertyName(GetId(propName));
                this.jsonWriter.Write(value);
            }
        }

        public void Write(int value, string propName = "int")
        {
            Validation();
            if (this.binaryWriter != null)
            {
                binaryWriter.Write(value);
            }
            if (this.jsonWriter != null)
            {
                this.jsonWriter.WritePropertyName(GetId(propName));
                this.jsonWriter.Write(value);
            }
        }

        public void Write(bool value, string propName = "bool")
        {
            Validation();
            if (this.binaryWriter != null)
            {
                binaryWriter.Write(value);
            }
            if (this.jsonWriter != null)
            {
                this.jsonWriter.WritePropertyName(GetId(propName));
                this.jsonWriter.Write(value);
            }
        }

        public void Write(byte value, string propName = "bool")
        {
            Validation();
            if (this.binaryWriter != null)
            {
                binaryWriter.Write(value);
            }
            if (this.jsonWriter != null)
            {
                this.jsonWriter.WritePropertyName(GetId(propName));
                this.jsonWriter.Write(value);
            }
        }

        public void Write(Quaternion value, string propName = "quaternion")
        {
            Validation();
            if (this.binaryWriter != null)
            {
                binaryWriter.Write(value.x);
                binaryWriter.Write(value.y);
                binaryWriter.Write(value.z);
                binaryWriter.Write(value.w);
            }
            if (this.jsonWriter != null)
            {
                jsonWriter.WritePropertyName(propName);
                LitJson.JsonMapper.ToJson(value, jsonWriter);
            }
        }

        public void Write(Vector3 value, string propName = "vector3")
        {
            Validation();
            if (this.binaryWriter != null)
            {
                binaryWriter.Write(value.x);
                binaryWriter.Write(value.y);
                binaryWriter.Write(value.z);
            }
            if (this.jsonWriter != null)
            {
                jsonWriter.WritePropertyName(GetId(propName));
                LitJson.JsonMapper.ToJson(value, jsonWriter);
            }
        }

        private void Validation()
        {
            if (opened == 0)
                throw new Exception("GameDataWriter : game data writer is not opened ...");
            if (opened < 0)
                throw new Exception("GameDataWriter : game data writer is already closed ...");
            if (opened > 1)
                throw new Exception("GameDataWriter : game data writer is already opened ...");
        }

        private void PushObject()
        {
            if (this.jsonWriter != null)
            {
                this.stack.Push(new HashSet<string>());
            }
        }

        private void PopObject()
        {
            if (this.jsonWriter != null)
            {
                this.stack.Pop();
            }
        }

        private string GetId(string type)
        {
            if (this.jsonWriter != null)
            {
                var set = this.stack.Peek();
                if (set.Contains(type))
                {
                    type = type + "#" + set.Count;
                }
                set.Add(type);
            }
            return type;
        }


        public void WriteObjectStart(string nameObject)
        {
            Validation();
            if (this.jsonWriter != null)
            {
                this.jsonWriter.WritePropertyName(nameObject);
                this.jsonWriter.WriteObjectStart();
                PushObject();
            }
        }

        private void WriteObjectStart()
        {
            Validation();
            if (this.jsonWriter != null)
            {
                this.jsonWriter.WriteObjectStart();
                PushObject();
            }
        }

        public void WriteObjectEnd()
        {
            Validation();
            if (this.jsonWriter != null)
            {
                this.jsonWriter.WriteObjectEnd();
                PopObject();
            }
        }
    }
}