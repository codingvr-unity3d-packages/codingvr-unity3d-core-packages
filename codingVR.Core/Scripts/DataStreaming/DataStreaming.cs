﻿using UnityEngine;

public static class DataStreaming
{
	public static byte[] ReadDataFromStream(string path)
	{
		if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
		{
			path = "file://" + path;
		}
		// https://gist.github.com/amowu/8121334
		byte[] data = null;
		WWW reader = new WWW(path);
		while (!reader.isDone) { }
		if (reader.bytes.Length > 0)
		{
			data = reader.bytes;
		}
		return data;
	}
}
