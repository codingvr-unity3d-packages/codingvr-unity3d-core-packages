﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public static class GameObjectExtensions
{
	// find object even inactive from parent
	public static GameObject FindObjectEvenInactive(this GameObject parent, string name)
	{
		Transform[] trs = parent.GetComponentsInChildren<Transform>(true);
		foreach (Transform t in trs)
		{
			if (t.name == name)
			{
				return t.gameObject;
			}
		}
		return null;
	}

	// find object even inactive from parent
	public static GameObject FindObjectEvenInactive(this GameObject parent, int instanceID)
	{
		Transform[] trs = parent.GetComponentsInChildren<Transform>(true);
		foreach (Transform t in trs)
		{
			if (t.GetInstanceID() == instanceID)
			{
				return t.gameObject;
			}
		}
		return null;
	}

	// find object even inactive
	public static GameObject FindObjectEvenInactive(string name)
	{
#if !FindObjectEvenInactive_Method2
		bool IsObject(GameObject g)
		{
#if UNITY_EDITOR
			return g.name == name && PrefabUtility.GetPrefabAssetType(g) == PrefabAssetType.NotAPrefab;
#else
			return g.name == name;
#endif
		}
		return Resources.FindObjectsOfTypeAll<GameObject>().FirstOrDefault(IsObject);
#else
		List<GameObject> rootObjects = new List<GameObject>();
		Scene[] scene = SceneManager.GetAllScenes();
		foreach (var it in scene)
		{
			var sc = it.GetRootGameObjects();
			rootObjects.AddRange(sc);
		}

		GameObject root = null;

		foreach (var it in rootObjects)
		{
			root = it.FindObjectEvenInactive(name);
			if (root != null)
				return root;
		}
		return null;
#endif
	}

	// find object even inactive
	public static GameObject FindObjectEvenInactive(int instanceID)
	{
		bool IsObject(GameObject g)
		{
#if UNITY_EDITOR
			return g.GetInstanceID() == instanceID && PrefabUtility.GetPrefabAssetType(g) == PrefabAssetType.NotAPrefab;
#else
			return g.GetInstanceID() == instanceID;
#endif
		}

		return Resources.FindObjectsOfTypeAll<GameObject>().FirstOrDefault(IsObject);
	}

	// find object from tag & name
	public static GameObject FindObjectFromTag(string tag, string name)
	{
		var screenPlays = GameObject.FindGameObjectsWithTag(tag);
		foreach (var it in screenPlays)
		{
			var obj = it.FindObjectEvenInactive(name);
			if (obj != null)
			{
				return obj;
			}
		}
		return null;
	}

	static public void DestroyChildren(this GameObject gameObjectRoot, bool immediate = true)
	{
		//Debug.Log(gameObjectRoot.transform.childCount);
		int i = 0;

		//Array to hold all child obj
		GameObject[] allChildren = new GameObject[gameObjectRoot.transform.childCount];

		//Find all child obj and store to that array
		foreach (Transform child in gameObjectRoot.transform)
		{
			allChildren[i++] = child.gameObject;
		}

		//Now destroy them
		foreach (GameObject child in allChildren)
		{
			if (immediate == true)
				GameObject.DestroyImmediate(child.gameObject);
			else
				GameObject.Destroy(child.gameObject);
		}
	}

	static public Transform RecursiveFindChild(this Transform parent, string childName)
	{
		Transform result = null;

		foreach (Transform child in parent)
		{
			if (child.name.Equals(childName))
				result = child;
			else
				result = RecursiveFindChild(child, childName);

			if (result != null) break;
		}

		return result;
	}

	static public GameObject RecursiveFindChild(this GameObject parent, string childName)
	{
		Transform result = RecursiveFindChild(parent.transform, childName);
		return result != null ? result.gameObject : null;
	}

	static public Transform RecursiveFindChildByTag(this Transform parent, string childTag)
	{
		Transform result = null;

		foreach (Transform child in parent)
		{
			if (child.tag.Equals(childTag))
				result = child;
			else
				result = RecursiveFindChildByTag(child, childTag);

			if (result != null) break;
		}

		return result;
	}

	static public GameObject RecursiveFindChildByTag(this GameObject parent, string childTag)
	{
		Transform result = RecursiveFindChildByTag(parent.transform, childTag);
		return result != null ? result.gameObject : null;
	}

	static public Transform RecursiveFindChildContainsName(this Transform parent, string childPartialName)
	{
		Transform result = null;

		foreach (Transform child in parent)
		{
			if (child.name.Contains(childPartialName))
				result = child;
			else
				result = RecursiveFindChildContainsName(child, childPartialName);

			if (result != null) break;
		}

		return result;
	}
	
	static public T RecursiveFindChild<T>(this Transform parent, string childName) where T:Component
	{
		T result = null;

		var components = parent.GetComponentsInChildren<T>();
		
		foreach (var c in components)
		{
			if (c.name == childName)
			{
				result = c;
				break;
			}
		}

		return result;
	}

	static public bool CompareTo(this Object lhs, Object rhs)
	{
		bool flag1 = lhs == null;
		bool flag2 = rhs == null;
		if (flag2 || flag1)
			return false;
		return lhs.GetInstanceID() == rhs.GetInstanceID();
	}
	
	/// <summary>
	/// //// https://answers.unity.com/questions/285133/find-child-of-a-game-object-using-tag.html
	/// Find all children of the Transform by tag (includes self)
	/// </summary>
	/// <param name="transform"></param>
	/// <param name="names"></param>
	/// <returns></returns>
	public static List<Transform> RecursiveFindChildren(this Transform transform, params string[] names)
	{
		List<Transform> list = new List<Transform>();
		foreach (var tran in transform.Cast<Transform>().ToList())
			list.AddRange(tran.RecursiveFindChildren(names)); // recursively check children
		if (names.Any(name => name == transform.name))
			list.Add(transform); // we matched, add this transform
		return list;
	}

	/// <summary>
	/// Find all children of the GameObject by tag (includes self)
	/// </summary>
	/// <param name="gameObject"></param>
	/// <param name="names"></param>
	/// <returns></returns>
	public static List<GameObject> RecursiveFindChildren(this GameObject gameObject, params string[] names)
	{
		return RecursiveFindChildren(gameObject.transform, names)
			//.Cast<GameObject>() // Can't use Cast here :(
			.Select(tran => tran.gameObject)
			.Where(gameOb => gameOb != null)
			.ToList();
	}

	/// <summary>
	/// //// https://answers.unity.com/questions/285133/find-child-of-a-game-object-using-tag.html
	/// Find all children of the Transform by tag (includes self)
	/// </summary>
	/// <param name="transform"></param>
	/// <param name="tags"></param>
	/// <returns></returns>
	public static List<Transform> RecursiveFindChildrenByTag(this Transform transform, params string[] tags)
	{
		List<Transform> list = new List<Transform>();
		foreach (var tran in transform.Cast<Transform>().ToList())
			list.AddRange(tran.RecursiveFindChildrenByTag(tags)); // recursively check children
		if (tags.Any(tag => tag == transform.tag))
			list.Add(transform); // we matched, add this transform
		return list;
	}

	/// <summary>
	/// Find all children of the GameObject by tag (includes self)
	/// </summary>
	/// <param name="gameObject"></param>
	/// <param name="tags"></param>
	/// <returns></returns>
	public static List<GameObject> RecursiveFindChildrenByTag(this GameObject gameObject, params string[] tags)
	{
		return RecursiveFindChildrenByTag(gameObject.transform, tags)
			//.Cast<GameObject>() // Can't use Cast here :(
			.Select(tran => tran.gameObject)
			.Where(gameOb => gameOb != null)
			.ToList();
	}

}
