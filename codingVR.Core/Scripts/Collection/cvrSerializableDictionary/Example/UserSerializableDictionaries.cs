﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class StringStringDictionary : codingVR.Core.cvrSerializableDictionary<string, string> {}

[Serializable]
public class ObjectColorDictionary : codingVR.Core.cvrSerializableDictionary<UnityEngine.Object, Color> {}

[Serializable]
public class ColorArrayStorage : codingVR.Core.cvrSerializableDictionary.Storage<Color[]> {}

[Serializable]
public class StringColorArrayDictionary : codingVR.Core.cvrSerializableDictionary<string, Color[], ColorArrayStorage> {}

[Serializable]
public class MyClass
{
    public int i;
    public string str;
}

[Serializable]
public class QuaternionMyClassDictionary : codingVR.Core.cvrSerializableDictionary<Quaternion, MyClass> {}

#if NET_4_6 || NET_STANDARD_2_0
[Serializable]
public class StringHashSet : codingVR.Core.cvrSerializableHashSet<string> {}
#endif
