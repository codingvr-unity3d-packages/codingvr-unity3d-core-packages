﻿using UnityEngine;

namespace codingVR.Core
{
    public static class BinaryTree
    {
        public static int Parent(int id)
        {
            return id / 2;
        }

        public static int FirstChildren(int id)
        {
            return id * 2;
        }

        public static int NextSibling(int id)
        {
            return id ^ 1;
        }
    }
}