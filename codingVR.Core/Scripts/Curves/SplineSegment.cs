﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Core
{
	// === SplinePoint =============================================================================================

	public class SplinePoint
	{
		public float time;
		public float distance;
		public int segment;
		public Vector3 point;
		public Vector3 normal;

		public SplinePoint(float time, float distance, int segment, Vector3 point, Vector3 normal)
		{
			this.time = time;
			this.distance = distance;
			this.segment = segment;
			this.point = point;
			this.normal = normal;
		}

		public static SplinePoint Create(float time, float distance, int segment, Vector3 point, Vector3 normal)
		{
			return new SplinePoint(time, distance, segment, point, normal);
		}
	};

	// === SplineSegment =============================================================================================

	public class SplineSegment
	{
		// === DATA ======================================================================================================================================================

		#region DATA

		int countBySegment = 0;
		Vector3[] sharedPoints;
		int numSegment;
		int index;

		#endregion

		// === CONSTRUCTOR ======================================================================================================================================================

		#region CONSTRUCTOR

		void Construct(int numSegment, Vector3[] sharedPoints)
		{
			this.numSegment = numSegment;
			this.sharedPoints = sharedPoints;
			this.countBySegment = 3;
			this.index = numSegment * (countBySegment - 1);
		}

		SplineSegment(int numSegment, Vector3[] sharedPoints)
		{
			Construct(numSegment, sharedPoints);
		}

		static public SplineSegment MakeBezierQuadraticSpline(int numSegment, Vector3[] sharedPoints)
		{
			return new SplineSegment(numSegment, sharedPoints);
		}

		static public SplineSegment[] AllocateQuadraticSplineSegments(Vector3[] points)
		{
			if (points.Length >= 3)
			{
				int countPoints = Math.Max(3, points.Length);
				if ((countPoints-1) % 2 == 0)
				{
					int countSegment = (countPoints-1) / 2;

					List<SplineSegment> splineSegments = new List<SplineSegment>();

					for (int i = 0; i < countSegment; ++i)
					{
						splineSegments.Add(SplineSegment.MakeBezierQuadraticSpline(i, points));
					}
					
					return splineSegments.ToArray();
				}
			}
			return null;
		}

		#endregion

		// === IMPLEMENTATION ======================================================================================================================================================

		#region IMPLEMENTATION

		private float ComputeSpline(bool reachEnd, Action<SplinePoint> node, float distance, float accuracy, RaycastHandlerFunction raycastHandlerFunction)
		{
			SplinePoint GetPoint(Vector3 a, Vector3 b, Vector3 c, float t, SplinePoint prev)
			{
				var point = Core.Bezier.GetPoint(a, b, c, t);
				if (raycastHandlerFunction != null)
					point = raycastHandlerFunction(point);
				distance += Vector3.Magnitude(point - prev.point);
				var current = SplinePoint.Create(t, distance, numSegment, point, Core.Bezier.GetNormal(a, b, c, t));
				return current;
			}

			if (countBySegment == 3)
			{
				float t = 0.0f;
				var a = sharedPoints[index + 0];
				var b = sharedPoints[index + 1];
				var c = sharedPoints[index + 2];
				var l = Core.Bezier.FastArcLength(a, b, c);
				if (l > 0.0)
				{
					accuracy = 1f / (l * accuracy);

					var prev = SplinePoint.Create(t, distance, numSegment, Core.Bezier.GetPoint(a, b, c, t), Core.Bezier.GetNormal(a, b, c, t));
					node(prev);
					for (t = accuracy; t < 1f; t += accuracy)
					{
						var current = GetPoint(a, b, c, t, prev);
						node(current);
						prev = current;
					}

					if (reachEnd == true)
					{
						var current = GetPoint(a, b, c, 1.0f, prev);
						node(current);
					}
				}
			}
			return distance;
		}

		class SplineMeshState
		{
			public float distance = 0.0f;
			public float dstSinceLastVertex = 0.0f;
			public SplinePoint lastAddedSplinePoint = null;
		};

		private void ComputeSplineMesh(bool reachEnd, Action<SplinePoint> node, SplineMeshState state, float accuracy, RaycastHandlerFunction raycastHandlerFunction)
		{
			SplinePoint GetPoint(Vector3 a, Vector3 b, Vector3 c, float t)
			{
				var point = Core.Bezier.GetPoint(a, b, c, t);
				if (raycastHandlerFunction != null)
				{
					point = raycastHandlerFunction(point);
				}
				var prevSplinePoint = SplinePoint.Create(t, state.distance, numSegment, point, Core.Bezier.GetNormal(a, b, c, t));
				return prevSplinePoint;
			}

			SplinePoint GetPointFromPrev(Vector3 a, Vector3 b, Vector3 c, float t, Vector3 prevPoint)
			{
				var point = Core.Bezier.GetPoint(a, b, c, t);
				if (raycastHandlerFunction != null)
				{
					point = raycastHandlerFunction(point);
				}
				state.distance += Vector3.Magnitude(point - prevPoint);
				var prevSplinePoint = SplinePoint.Create(t, state.distance, numSegment, point, Core.Bezier.GetNormal(a, b, c, t));
				
				return prevSplinePoint;
			}
			
			if (countBySegment == 3)
			{
				var a = sharedPoints[index + 0];
				var b = sharedPoints[index + 1];
				var c = sharedPoints[index + 2];
				var l = Core.Bezier.FastArcLength(a, b, c);
				//(var p0, var p1, var p2, var p3) = Core.Bezier.QuadraticToCubic(a, b, c);
				if (l > 0.0f)
				{
					float maxAngleError = 0.1f;
					float minVertexDst = 0.0f;
					float maxVertexDst = 0.25f;
					float inc = 1f / (l * accuracy);

					// first spline point
					float t = 0.0f;
					var prevSplinePoint = GetPoint(a, b, c, t);
					node(prevSplinePoint);
					state.lastAddedSplinePoint = prevSplinePoint;
					
					// next spline point
					t = inc;
					var nextSplinePoint = GetPointFromPrev(a, b, c, t, prevSplinePoint.point);

					for (; t < 1f; t += inc)
					{
						// current
						var currentSplinePoint = nextSplinePoint;

						// next
						nextSplinePoint = GetPointFromPrev(a, b, c, Mathf.Clamp01(t + inc), currentSplinePoint.point);
						
						// angle at current point on path
						float localAngle = 180 - Vector3Extension.MinAngle(prevSplinePoint.point, currentSplinePoint.point, nextSplinePoint.point);
						// angle between the last added vertex, the current point on the path, and the next point on the path
						float angleFromPrevVertex = 180 - Vector3Extension.MinAngle(state.lastAddedSplinePoint.point, currentSplinePoint.point, nextSplinePoint.point);
						float angleError = Mathf.Max(localAngle, angleFromPrevVertex);

						if (state.dstSinceLastVertex > maxVertexDst || (angleError > maxAngleError && state.dstSinceLastVertex >= minVertexDst))
						{
							node(currentSplinePoint);
							state.dstSinceLastVertex = 0;
							state.lastAddedSplinePoint = currentSplinePoint;
						}
						else
						{
							state.dstSinceLastVertex += (currentSplinePoint.point - prevSplinePoint.point).magnitude;
						}

						// previous
						prevSplinePoint = currentSplinePoint;
					}

					if (reachEnd == true)
					{
						node(nextSplinePoint);
					}
				}
			}
		}

		#endregion

		// === INTERFACE ===========================================================================================================================================================

		#region INTERFACE

		public Vector3 ComputePoint(float time)
		{
			Vector3 point = Vector3.zero;
			if (countBySegment == 3)
			{
				var a = sharedPoints[index + 0];
				var b = sharedPoints[index + 1];
				var c = sharedPoints[index + 2];
				point = Core.Bezier.GetPoint(a, b, c, time);
			}
			return point;
		}

		public Vector3 ComputeDerivative(float time)
		{
			Vector3 tangent = Vector3.zero;
			if (countBySegment == 3)
			{
				var a = sharedPoints[index + 0];
				var b = sharedPoints[index + 1];
				var c = sharedPoints[index + 2];
				tangent = Core.Bezier.GetFirstDerivative(a, b, c, time);
			}
			return tangent;
		}

		public Vector3 GetControlPoint(int i)
		{
			return sharedPoints[index + i];
		}

		public Vector3 LastPosition
		{
			get
			{
				return sharedPoints[sharedPoints.Length - 1];
			}
		}

		public float ArcLength
		{
			get
			{
				float l = 0.0f;
				if (countBySegment == 3)
				{
					var a = sharedPoints[index + 0];
					var b = sharedPoints[index + 1];
					var c = sharedPoints[index + 2];
					l = Core.Bezier.FastArcLength(a, b, c);
				}
				return l;
			}
		}

		public delegate Vector3 RaycastHandlerFunction(Vector3 a);

		public static float ComputeCurveMesh(SplineSegment[] splines, Action<Core.SplinePoint> node, float accuracy, RaycastHandlerFunction raycastHandlerFunction)
		{
			SplineMeshState splineMeshState = new SplineMeshState();
			for (int i = 0; i < splines.Length - 1; ++i)
			{
				splines[i].ComputeSplineMesh(false, node, splineMeshState, accuracy, raycastHandlerFunction);
			}
			splines[splines.Length - 1].ComputeSplineMesh(true, node, splineMeshState, accuracy, raycastHandlerFunction);
			return splineMeshState.distance;
		}

		public static float ComputeCurve(SplineSegment[] splines, Action<Core.SplinePoint> node, float accuracy, RaycastHandlerFunction raycastHandlerFunction)
		{
			float distance = 0.0f;
			for (int i = 0; i < splines.Length - 1; ++i)
			{
				distance = splines[i].ComputeSpline(false, node, distance, accuracy, raycastHandlerFunction);
			}
			distance = splines[splines.Length - 1].ComputeSpline(true, node, distance, accuracy, raycastHandlerFunction);
			return distance;
		}

		public int Index => index;

		#endregion
	};
}
