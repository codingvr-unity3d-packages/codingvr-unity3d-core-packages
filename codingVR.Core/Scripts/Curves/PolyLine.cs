﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

//#define CLOSEST_ALTERNATIVE_METHOD

namespace codingVR.Core
{
	// === PolyLine =============================================================================================

	public class PolyLine
	{
		// data
		List<Core.SplinePoint> curvedPath = null;

		// constructor
		protected PolyLine(SplineSegment[] splines, float accuracy, Core.SplineSegment.RaycastHandlerFunction raycastHandlerFunction)
		{
			// create curved path
			curvedPath = new List<Core.SplinePoint>();
			SplineSegment.ComputeCurveMesh(splines, node => curvedPath.Add(node), accuracy, raycastHandlerFunction);
		}
		
		public static PolyLine MakePolyLine(SplineSegment[] splines, float accuracy, Core.SplineSegment.RaycastHandlerFunction raycastHandlerFunction)
		{
			return new PolyLine(splines, accuracy, raycastHandlerFunction);
		}

        #if UNITY_EDITOR
        void DrawCurveNode(Core.SplinePoint node, float size, Color color)
		{
			Gizmos.color = color;
			Gizmos.DrawSphere(node.point, size);
			Handles.color = Color.green;
			Handles.DrawLine(node.point, node.point + node.normal);
		}
		#endif

		/*
		 *
		 * Compute Distance Travelled
		 * 
		 */

		// get closest point
		(float, float) ComputeDistanceTravelledToEndForward(List<Core.SplinePoint> curvedPath, Vector3 pos, int startNode = -1)
		{
			// initialize initial findNode
			if (startNode == -1)
				startNode = 0;

			float distanceTravelled = 0.0f;
			float distancePointToLine = 10000.0f;
			{
				Vector3 projectedtPoint = curvedPath[startNode].point;
				int projectedNode = startNode;

				// compute the projected point
				for (int i = startNode; i < curvedPath.Count - 1; ++i)
				{
					var p0 = curvedPath[i + 0];
					var p1 = curvedPath[i + 1];
					// projected point on line
					var localPointToLine = Vector3Extension.ProjectPointLine(pos, p0.point, p1.point);
					// distance real unit position from point to line
					var localDistancePointToLine = Vector3.Magnitude(localPointToLine - pos);
					// compute percent of polyline segment 
					float distanceLine = Vector3.Magnitude(p1.point - p0.point);
					float distanceSubLine = Vector3.Magnitude(localPointToLine - p0.point);
					float relativeDistance = Mathf.Abs(distanceSubLine / distanceLine);

					if (localDistancePointToLine < distancePointToLine && relativeDistance < 1.0f)
					{
						distancePointToLine = localDistancePointToLine;
						projectedtPoint = localPointToLine;
						projectedNode = i;
					}
				}

				Vector3 lasttPoint = curvedPath[startNode].point;
				for (int i = startNode; i < projectedNode; ++i)
				{
					distanceTravelled += Vector3.Magnitude(curvedPath[i+1].point - lasttPoint);
					lasttPoint = curvedPath[i + 1].point;
				}

				distanceTravelled += Vector3.Magnitude(projectedtPoint - lasttPoint);

			}
			return (distanceTravelled, distancePointToLine);
		}

		// get closest point
		(float, float) ComputeDistanceTravelledToEndBackward(List<Core.SplinePoint> curvedPath, Vector3 pos, int startNode = 0)
		{
			// initialize initial findNode
			if (startNode == -1)
				startNode = curvedPath.Count - 1;

			// reached
			float distanceTravelled = 0.0f;
			float distancePointToLine = 10000.0f;
			{
				Vector3 projectedtPoint = curvedPath[startNode].point;
				int projectedNode = startNode;

				for (int i = startNode; i > 0; --i)
				{
					var p0 = curvedPath[i - 1];
					var p1 = curvedPath[i + 0];
					// projected point on line
					var localPointToLine = Vector3Extension.ProjectPointLine(pos, p0.point, p1.point);
					// distance real unit position from point to line
					var localDistancePointToLine = Vector3.Magnitude(localPointToLine - pos);
					// compute percent of polyline segment 
					float distanceLine = Vector3.Magnitude(p1.point - p0.point);
					float distanceSubLine = Vector3.Magnitude(localPointToLine - p0.point);
					float relativeDistance = Mathf.Abs(distanceSubLine / distanceLine);

					if (localDistancePointToLine < distancePointToLine && relativeDistance < 1.0f)
					{
						distancePointToLine = localDistancePointToLine;
						projectedtPoint = localPointToLine;
						projectedNode = i;
					}
				}

				Vector3 lasttPoint = curvedPath[startNode].point;
				for (int i = startNode; i > projectedNode; --i)
				{
					distanceTravelled += Vector3.Magnitude(curvedPath[i - 1].point - lasttPoint);
					lasttPoint = curvedPath[i - 1].point;
				}

				distanceTravelled += Vector3.Magnitude(projectedtPoint - lasttPoint);

			}
			return (distanceTravelled, distancePointToLine);
		}

		public (float, float) ComputeDistanceTravelledToEnd(Vector3 pos, bool reverse, int startNode = 0)
		{
			// compute closest position
			(float distanceTravelled, float distancePointToLine) = reverse == false ? ComputeDistanceTravelledToEndForward(curvedPath, pos, startNode) : ComputeDistanceTravelledToEndBackward(curvedPath, pos, startNode);
			return (distanceTravelled, distancePointToLine);
		}

		/*
		 *
		 * Compute Closest Position
		 * 
		 */


#if CLOSEST_ALTERNATIVE_METHOD
				
		float ComputeClosestPosition(Vector3 pos, float paramThreshold = 0.000001f)
		{
			return ComputeClosestPositionRecursive(pos, 0.0f, 1.0f, paramThreshold);
		}

		float ComputeClosestPositionRecursive(Vector3 pos, float beginT, float endT, float thresholdT)
		{
			float mid = (beginT + endT) / 2.0f;

			// Base case for recursion.
			if ((endT - beginT) < thresholdT)
				return mid;

			// The two halves have param range [start, mid] and [mid, end]. We decide which one to use by using a midpoint param calculation for each section.
			float paramA = (beginT + mid) / 2.0f;
			float paramB = (mid + endT) / 2.0f;

			Vector3 posA = FlatMapPathDetails.ComputePoint(this.unit.PathToTravel, paramA);
			Vector3 posB = FlatMapPathDetails.ComputePoint(this.unit.PathToTravel, paramB);
			float distASq = (posA - pos).sqrMagnitude;
			float distBSq = (posB - pos).sqrMagnitude;

			if (distASq < distBSq)
				endT = mid;
			else
				beginT = mid;

			// The (tail) recursive call.
			return ComputeClosestPositionRecursive(pos, beginT, endT, thresholdT);
		}
#endif

		// get closest point
		(bool, float, int, float, int) ComputeClosestPositionAndLengthToEndForward(List<Core.SplinePoint> curvedPath, Vector3 pos, int startNode = -1)
		{
			// initialize initial findNode
			if (startNode == -1)
				startNode = 0;

			// update final location
			// find unit cell position relative to path
			float lengthPathRemaining = 0.0f;
			float time = 0.0f;

			// reached
			bool reached = false;

			int segment = 0;
			{
				float distancePointToLine = 10000.0f;
				for (int i = startNode; i < curvedPath.Count - 1; ++i)
				{
					var p0 = curvedPath[i + 0];
					var p1 = curvedPath[i + 1];
					// projected point on line
					var localPointToLine = Vector3Extension.ProjectPointLine(pos, p0.point, p1.point);
					// distance real unit position from point to line
					var localDistancePointToLine = Vector3.Magnitude(localPointToLine - pos);
					// select the nearest point 
					if (localDistancePointToLine < distancePointToLine)
					{
						// parameters
						distancePointToLine = localDistancePointToLine;
						lengthPathRemaining = 0.0f;
						startNode = i;
						segment = p0.segment;

						// compute percent of polyline segment 
						float distanceLine = Vector3.Magnitude(p1.point - p0.point);
						float distanceSubLine = Vector3.Magnitude(localPointToLine - p0.point);
						float ratio = distanceSubLine / distanceLine;

						// detect reached
						if (startNode == curvedPath.Count - 2 && Mathf.Approximately(ratio, 1.0f))
						{
							reached = true;
						}

						// each segment time length is between 0 & 1
						// if we reach at end of segeent and so at thethe start of next segmment t1 will be lower than t0
						var t0 = p0.time;
						var t1 = p1.time;
						if (t0 > t1)
						{
							t1 = 1.0f;
						}
						
						// compute time from percent
						// time = (1.0f - ratio) * p0.time + ratio * p1.time;
						time = Mathf.Lerp(t0, t1, ratio);
					}
					lengthPathRemaining += Vector3.Magnitude(p1.point - p0.point);
				}
			}
			return (reached, time, segment, lengthPathRemaining, startNode);
		}

		// get closest point
		(bool, float, int, float, int) ComputeClosestPositionAndLengthToEndBackward(List<Core.SplinePoint> curvedPath, Vector3 pos, int startNode = 0)
		{
			// initialize initial findNode
			if (startNode == -1)
				startNode = curvedPath.Count - 1;

			// reached
			bool reached = false;

			float lengthPathRemaining = 0.0f;
			float time = 0.0f;
			int segment = 0;
			{
				float distancePointToLine = 10000.0f;
				for (int i = startNode; i > 0; --i)
				{
					var p0 = curvedPath[i - 1];
					var p1 = curvedPath[i + 0];
					// projected point on line
					var localPointToLine = Vector3Extension.ProjectPointLine(pos, p0.point, p1.point);
					// distance real unit position from point to line
					var localDistancePointToLine = Vector3.Magnitude(localPointToLine - pos);
					// select the nearest point 
					if (localDistancePointToLine < distancePointToLine)
					{
						// parameters
						distancePointToLine = localDistancePointToLine;
						lengthPathRemaining = 0.0f;
						startNode = i;
						segment = p0.segment;
						
						// compute percent of polyline segment 
						float distanceLine = Vector3.Magnitude(p1.point - p0.point);
						float distanceSubLine = Vector3.Magnitude(localPointToLine - p0.point);
						float ratio = distanceSubLine / distanceLine;

						// detect reached
						if (startNode == 1 && Mathf.Approximately(ratio, 0.0f))
						{
							reached = true;
						}
												
						// each segment time length is between 0 & 1
						// if we reach at end of segeent and so at thethe start of next segmment t1 will be lower than t0
						var t0 = p0.time;
						var t1 = p1.time;
						if (t0 > t1)
						{
							t1 = 1.0f;
						}
											   						 
						// compute time from percent
						// time = (1.0f - ratio) * p0.time + ratio * p1.time;
						time = Mathf.Lerp(p0.time, p1.time, ratio);
					}
					lengthPathRemaining += Vector3.Magnitude(p1.point - p0.point);
				}
			}
			return (reached, time, segment, lengthPathRemaining, startNode);
		}

		public (bool, float, int, float, int) ComputeClosestPositionAndLengthToEnd(Vector3 pos, bool reverse, int startNode = 0)
		{
			// compute closest position
			bool reached = false;
			float time = 0.0f;
			int numSegment = 0;
			float lengthPathRemaining = 0.0f;
			(reached, time, numSegment, lengthPathRemaining, startNode) = reverse == false ? ComputeClosestPositionAndLengthToEndForward(curvedPath, pos, startNode) : ComputeClosestPositionAndLengthToEndBackward(curvedPath, pos, startNode);
#if CLOSEST_ALTERNATIVE_METHOD
			time = ComputeClosestPosition(unit.UnitTransform.position);
#endif
			return (reached, time, numSegment, lengthPathRemaining, startNode);
		}

		// get closest point
		(float, float) ComputeMinMaxDistance(List<Core.SplinePoint> curvedPath, Vector3 pos, int startNode = -1)
		{
			// initialize initial findNode
			if (startNode == -1)
				startNode = 0;

			// update final location
			// find unit cell position relative to path
			float minDistance = float.MaxValue;
			float maxDistance = float.MinValue;
			for (int i = startNode; i < curvedPath.Count - 1; ++i)
			{
				var p0 = curvedPath[i + 0];
				var p1 = curvedPath[i + 1];
				// projected point on line
				float distance = Vector3Extension.DistancePointLine(pos, p0.point, p1.point);
				minDistance = Mathf.Min(minDistance, distance);
				maxDistance = Mathf.Max(maxDistance, distance);
			}
			return (minDistance, maxDistance);
		}

		/*
		 *
		 * Compute Min Max Distance
		 * 
		 */

		public (float, float) ComputeMinMaxDistance(Vector3 pos, int findNode = -1)
		{
			// compute min max distance
			(float minValue, float maxValue) = ComputeMinMaxDistance(curvedPath, pos, findNode);
			return (minValue, maxValue);
		}

		/*
		 *
		 * Compute Length
		 * 
		 */

		public float ComputeLength(int startNode = -1)
		{
			// initialize initial findNode
			if (startNode == -1)
				startNode = 0;

			// compute min max distance
			float distance = 0.0f;
			for (int i = startNode; i < curvedPath.Count - 1; ++i)
			{
				distance += Vector3.Magnitude(curvedPath[i + 1].point - curvedPath[i + 0].point);
			}

			return distance;
		}

		/*
		 *
		 * Draw Poly Line
		 * 
		 */

		public void DrawPolyLine(float size, Color color)
		{
        #if UNITY_EDITOR

            var restoreColor = GUI.color;
			for (int i=0;i<curvedPath.Count-1;++i)
			{
				DrawCurveNode(curvedPath[i], size, color);
				Handles.color = Color.green;
				Handles.DrawLine(curvedPath[i + 0].point, curvedPath[i + 1].point);
			}
			DrawCurveNode(curvedPath[curvedPath.Count - 1], size, color);
			Gizmos.color = restoreColor;
        #endif
        }

		/*
		 *
		 * Get Spline Points
		 * 
		 */

		public Core.SplinePoint[] SplinePoints
		{
			get
			{
				return curvedPath.ToArray();
			}
		}
	}
}
