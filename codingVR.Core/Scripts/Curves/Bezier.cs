﻿using System;
using UnityEngine;

// https://github.com/SebLague/Path-Creator
// https://github.com/bluescan/bezier
// https://www.redblobgames.com/articles/curved-paths/


namespace codingVR.Core
{
	public static class Bezier
	{
		/*
		 * 
		 * quadratric bezier spline (3 points)
		 * 
		 */

		public static Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, float t)
		{
			// https://stackoverflow.com/questions/785097/how-do-i-implement-a-b%C3%A9zier-curve-in-c
			//  below cubics method
			// p0 : control point 0
			// p1 : handler
			// p2 : control point 1

			// FYI when curbature is unknow we compute p2 = (p0 + p1) * 0.5; and p0 & p1 are middle of the segment

			// p = (1 - t) ^ 2 * P0 + 2 * (1 - t) * t * P1 + t * t * P2
			t = Mathf.Clamp01(t);
			float r = 1f - t;
			return r * r * p0 + 2f * r * t * p1 + t * t * p2;
		}

		public static Vector3 GetNormal(Vector3 a1, Vector3 c1, Vector3 a2, float t)
		{
			Vector3 tangent = GetFirstDerivative(a1, c1, a2, t);
			//Vector3 nextTangent = GetSecondDerivative(a1, c1, a2, t);
			//Vector3 c = Vector3.Cross(nextTangent, tangent);
			return Vector3.Cross(Vector3.up, tangent).normalized;
		}


		public static Vector3 GetFirstDerivative(Vector3 p0, Vector3 p1, Vector3 p2, float t)
		{
			t = Mathf.Clamp01(t);
			return 2f * ((1f - t) * (p1 - p0) + t * (p2 - p1));
		}

		// https://catlikecoding.com/unity/tutorials/curves-and-splines/
		// The second derivative is the derivative of the first derivative, which defines the acceleration 
		// The change of velocity – along the curve.
		// For the quadratic Bézier curve, it is B''(t) = 2(P2 - 2 P1 + P0).
		// As t is not part of the formula, quadratic curves have a constant acceleration.
		public static Vector3 GetSecondDerivative(Vector3 p0, Vector3 p1, Vector3 p2, float t)
		{
			return 2f * (p2 - 2f * p1 + p0);
		}

		// https://stackoverflow.com/questions/29438398/cheap-way-of-calculating-cubic-bezier-length
		public static float FastArcLength(Vector3 p0, Vector3 p1, Vector3 p2)
		{
			float arcLength = 0.0f;
			ArcLengthUtil(p0, p1, p1, p2, 5, ref arcLength);
			return arcLength;
		}

		// https://github.com/HTD/FastBezier/blob/master/Program.cs
		public static float ArcLength(Vector3 p0, Vector3 p1, Vector3 p2)
		{
			if (p0 == p2)
			{
				if (p0 == p1)
				{
					return 0.0f;
				}
				return (p0 - p1).magnitude;
			}
			if (p1 == p0 || p1 == p2)
			{
				return (p0 - p2).magnitude;
			}
			Vector3 A0 = p1 - p0;
			Vector3 A1 = p0 - 2.0f * p1 + p2;
			if (!A1.IsZero())
			{
				float c = 4.0f * Vector3.Dot(A1, A1);
				float b = 8.0f * Vector3.Dot(A0, A1);
				float a = 4.0f * Vector3.Dot(A0, A0);
				float q = 4.0f * a * c - b * b;
				float twoCpB = 2.0f * c + b;
				float sumCBA = c + b + a;
				var l0 = (0.25f / c) * (twoCpB * Mathf.Sqrt(sumCBA) - b * Mathf.Sqrt(a));
				if (q == 0.0f) return l0;
				var l1 = (q / (8.0f * Mathf.Pow(c, 1.5f))) * (Mathf.Log(2.0f * Mathf.Sqrt(c * sumCBA) + twoCpB) - Mathf.Log(2.0f * Mathf.Sqrt(c * a) + b));
				return l0 + l1;
			}
			else
			{
				return 2.0f * A0.magnitude;
			}
		}

		/*
		 * 
		 * Quadratic to Cubic 
		 * 
		 */

		// https://stackoverflow.com/questions/3162645/convert-a-quadratic-bezier-to-a-cubic-one
		public static (Vector3 cp0, Vector3 cp1, Vector3 cp2, Vector3 cp3) QuadraticToCubic(Vector3 p0, Vector3 p1, Vector3 p2)
		{
			var cp0 = p0;
			var cp1 = p0 + (2f / 3f) * (p1 - p0);
			var cp2 = p2 + (2f / 3f) * (p1 - p2);
			var cp3 = p2;
			return (cp0, cp1, cp2, cp3);
		}

		/*
		 * 
		 * cubic bezier spline (4 points)
		 * 
		 */

		/// Returns point at time 't' (between 0 and 1)  along bezier curve defined by 4 points (anchor_1, control_1, control_2, anchor_2)
		public static Vector3 GetPoint(Vector3 a1, Vector3 c1, Vector3 c2, Vector3 a2, float t)
		{
			t = Mathf.Clamp01(t);
			return (1 - t) * (1 - t) * (1 - t) * a1 + 3 * (1 - t) * (1 - t) * t * c1 + 3 * (1 - t) * t * t * c2 + t * t * t * a2;
		}

		/// Calculates the normal vector (vector perpendicular to the curve) at specified time
		public static Vector3 GetNormal(Vector3 a1, Vector3 c1, Vector3 c2, Vector3 a2, float t)
		{
			Vector3 tangent = GetFirstDerivative(a1, c1, c2, a2, t);
			Vector3 nextTangent = GetSecondDerivative(a1, c1, c2, a2, t);
			Vector3 c = Vector3.Cross(nextTangent, tangent);
			return Vector3.Cross(c, tangent).normalized;
		}

		/// This is the vector tangent to the curve at that point
		public static Vector3 GetFirstDerivative(Vector3 a1, Vector3 c1, Vector3 c2, Vector3 a2, float t)
		{
			t = Mathf.Clamp01(t);
			return 3 * (1 - t) * (1 - t) * (c1 - a1) + 6 * (1 - t) * t * (c2 - c1) + 3 * t * t * (a2 - c2);
		}

		///Returns the second derivative of the curve at time 't'
		public static Vector3 GetSecondDerivative(Vector3 a1, Vector3 c1, Vector3 c2, Vector3 a2, float t)
		{
			t = Mathf.Clamp01(t);
			return 6 * (1 - t) * (c2 - 2 * c1 + a1) + 6 * t * (a2 - 2 * c2 + c1);
		}
			   
		// https://stackoverflow.com/questions/29438398/cheap-way-of-calculating-cubic-bezier-length
		public static float FastArcLength(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
		{
			float arcLength = 0.0f;
			ArcLengthUtil(p0, p1, p2, p3, 5, ref arcLength);
			return arcLength;
		}

		// https://stackoverflow.com/questions/29438398/cheap-way-of-calculating-cubic-bezier-length
		static void ArcLengthUtil(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, uint subdiv, ref float L)
		{
			if (subdiv > 0)
			{
				Vector3 a = p0 + (p1 - p0) * 0.5f;
				Vector3 b = p1 + (p2 - p1) * 0.5f;
				Vector3 c = p2 + (p3 - p2) * 0.5f;
				Vector3 d = a + (b - a) * 0.5f;
				Vector3 e = b + (c - b) * 0.5f;
				Vector3 f = d + (e - d) * 0.5f;

				// left branch
				ArcLengthUtil(p0, a, d, f, subdiv - 1, ref L);
				// right branch
				ArcLengthUtil(f, e, c, p3, subdiv - 1, ref L);
			}
			else
			{
				float controlNetLength = (p1 - p0).magnitude + (p2 - p1).magnitude + (p3 - p2).magnitude;
				float chordLength = (p3 - p0).magnitude;
				L += (chordLength + controlNetLength) / 2.0f;
			}
		}
	}
}