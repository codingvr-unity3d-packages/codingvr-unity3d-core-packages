﻿using System.Linq;
using UnityEngine;

namespace codingVR.Core
{
	public static class PhysicsExtensions
	{
		public static RaycastHit[] RaycastAllOrdered(Ray ray)
		{
			var hitpoints = Physics.RaycastAll(ray).OrderBy(h => h.distance).ToArray();
			return hitpoints;
		}
	}
}
