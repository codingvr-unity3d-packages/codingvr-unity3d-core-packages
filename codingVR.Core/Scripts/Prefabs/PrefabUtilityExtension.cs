﻿using UnityEditor;
using UnityEngine;

public static class PrefabUtilityExtension
{
    public static Object GetCorrespondingObjectFromSource(Object obj )
    {
        Object prefab = null;
#if UNITY_EDITOR
        prefab = PrefabUtility.GetCorrespondingObjectFromSource(obj);
        if (null == prefab)
            prefab = PrefabUtility.GetCorrespondingObjectFromOriginalSource(obj);
#endif
        return prefab;
      }
}
