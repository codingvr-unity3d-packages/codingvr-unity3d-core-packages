﻿using System;

namespace codingVR.Core
{
	public static class TimeUtils
	{
		/// <summary>
		/// Returns Unix Time in seconds
		/// </summary>
		public static int GetCurrentUnixTimeSeconds()
		{
			return (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
		}

		/// <summary>
		/// Returns Date time from Unix Time in seconds
		/// </summary>

		public static DateTime GetDateTimeFromUnixTimeSeconds(double seconds)
		{
			// Unix timestamp is seconds past epoch
			System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
			dtDateTime = dtDateTime.AddSeconds(seconds).ToLocalTime();
			return dtDateTime;
		}

		/// <summary>
		/// Returns Date time from Unix Time in minutes
		/// </summary>

		public static DateTime GetDateTimeFromUnixTimeMinutes(double minutes)
		{
			// Unix timestamp is seconds past epoch
			System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
			dtDateTime = dtDateTime.AddMinutes(minutes).ToLocalTime();
			return dtDateTime;
		}

		/// <summary>
		/// Returns Date time from Unix Time in hours
		/// </summary>

		public static DateTime GetDateTimeFromUnixTimeHours(double hours)
		{
			// Unix timestamp is seconds past epoch
			System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
			dtDateTime = dtDateTime.AddHours(hours).ToLocalTime();
			return dtDateTime;
		}

		/// <summary>
		/// Returns Date time from Unix Time in days
		/// </summary>

		public static DateTime GetDateTimeFromUnixTimeDays(double days)
		{
			// Unix timestamp is seconds past epoch
			System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
			dtDateTime = dtDateTime.AddHours(days).ToLocalTime();
			return dtDateTime;
		}

		/// <summary>
		/// Returns current day number
		/// </summary>
		public static int GetCurrentDayNumber()
		{
			return (int)DateTime.Now.DayOfWeek;
		}

		/// <summary>
		/// Returns current day name
		/// </summary>
		public static string GetCurrentDayName()
		{
			return DateTime.Now.DayOfWeek.ToString();
		}

		/// <summary>
		/// Returns current hour and minutes
		/// </summary>
		public static string GetCurrentTime()
		{
			return DateTime.Now.ToString("HH:mm");
		}
	}
}