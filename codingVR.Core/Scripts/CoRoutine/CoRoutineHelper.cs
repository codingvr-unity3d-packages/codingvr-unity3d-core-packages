using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// // https://answers.unity.com/questions/541880/is-there-a-way-to-tell-if-a-particular-coroutine-i.html
namespace codingVR.Core
{
    public class CoRoutineHelper : MonoBehaviour
    {
        private static CoRoutineHelper ins;
        public static CoRoutineHelper Instance
        {
            get
            {
                if (ins == null)
                {
                    var go = new GameObject("CoroutineHelper");
                    DontDestroyOnLoad(go);
                    ins = go.AddComponent<CoRoutineHelper>();
                }
                return ins;
            }
        }

        public void StartCoroutineEx(string name, IEnumerator routine, out CoRoutineController coroutineController)
        {
            if (routine == null)
            {
                throw new System.ArgumentNullException("routine");
            }
            coroutineController = new CoRoutineController(name, routine);
            coroutineController.Start();
        }
    }
}