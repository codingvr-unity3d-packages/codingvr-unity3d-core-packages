using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// https://answers.unity.com/questions/541880/is-there-a-way-to-tell-if-a-particular-coroutine-i.html
namespace codingVR.Core
{
    public enum CoRoutineState
    {
        Ready,
        Running,
        Finished
    }

    [Serializable]
    public class CoRoutineController
    {
        private IEnumerator routine;
        private Coroutine coroutine;
        public CoRoutineState state;
        [SerializeField]
        private string name;

        public CoRoutineController(string name, IEnumerator routine)
        {
            this.routine = routine;
            this.state = CoRoutineState.Ready;
            this.name = name;
        }
               
        public void Start()
        {
            if (state != CoRoutineState.Ready)
            {
                throw new System.InvalidOperationException("Unable to start coroutine in state: " + state);
            }

            state = CoRoutineState.Running;
            coroutine = CoRoutineHelper.Instance.StartCoroutine(RealRun());
        }

        private IEnumerator RealRun()
        {
            yield return CoRoutineHelper.Instance.StartCoroutine(routine);
            state = CoRoutineState.Finished;
        }

        public bool Stop()
        {
            bool b = false;

            if (state != CoRoutineState.Running)
            {
                throw new System.InvalidOperationException("Unable to Stop coroutine in state: " + state);
            }
            CoRoutineHelper.Instance.StopCoroutine(coroutine);
            state = CoRoutineState.Finished;

            b = true;

            return b;
        }

        public IEnumerator Wait()
        {
            if (state != CoRoutineState.Running)
            {
                throw new System.InvalidOperationException("Unable to Stop coroutine in state: " + state);
            }

            yield return new WaitUntil(() => state == CoRoutineState.Finished);
        }

        public string Name => name;
    }
}