using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Core
{
    public class CoRoutineManager : MonoBehaviour, ICoRoutineManager
    {
        [Serializable]
        internal class CoRoutineCollection
        {
            [SerializeField]
            List<CoRoutineController> coRoutineControllerCollection = new List<CoRoutineController>();

            public CoRoutineCollection()
            {

            }

            public void Start(string name, IEnumerator routine)
            {
                var coRoutine = new CoRoutineController(name, routine);
                coRoutineControllerCollection.Add(coRoutine);
                coRoutine.Start();
            }

            public bool StopAll()
            {
                int countStopped = 0;
                bool bStopped = false;
                foreach (var it in coRoutineControllerCollection)
                {
                    if (it.state == CoRoutineState.Running)
                    {
                        countStopped += it.Stop() ? 1 : 0;
                    }
                }

                if (countStopped == coRoutineControllerCollection.Count)
                {
                    coRoutineControllerCollection.Clear();
                    bStopped = true;
                }
                return bStopped;
            }

            public bool Update()
            {
                return coRoutineControllerCollection.RemoveAll(item => item.state == CoRoutineState.Finished) < coRoutineControllerCollection.Count ? true : false;
            }

            public IEnumerator WaitAll()
            {
                int countStopped = 0;
                bool bStopped = false;
                foreach (var it in coRoutineControllerCollection)
                {
                    if (it.state == CoRoutineState.Running)
                    {
                        Debug.LogWarningFormat("{0} : the CoRoutine {1} is waiting !!!", System.Reflection.MethodBase.GetCurrentMethod().Name, it.Name);
                        yield return it.Wait();
                    }
                }

                if (countStopped == coRoutineControllerCollection.Count)
                {
                    coRoutineControllerCollection.Clear();
                    bStopped = true;
                }
                yield return null;
            }
        }

        [Serializable] class CoRoutineSetDictionary : Core.cvrSerializableDictionary<string, CoRoutineCollection> { }
        [SerializeField]
        CoRoutineSetDictionary coRoutineSetDictionary = new CoRoutineSetDictionary();

        Stack<string> tokens = new Stack<string>();
        
        void Start()
        {
        }

        void OnEnable()
        {
            tokens.Push("root");
        }

        void OnDisable()
        {
            tokens.Pop();
        }

        public void Update()
        {
            List<string> itemsToRemove = new List<string>();

            foreach (var it in coRoutineSetDictionary)
            {
                if (it.Value.Update() == false)
                {
                    itemsToRemove.Add(it.Key);
                }
            }

            foreach (var it in itemsToRemove)
            {
                coRoutineSetDictionary.Remove(it);
            }
        }

        public void StartCoRoutine(string token, string name, IEnumerator routine)
        {
            if (coRoutineSetDictionary.ContainsKey(token) == false)
            {
                coRoutineSetDictionary.Add(token, new CoRoutineCollection());
            }

            if (coRoutineSetDictionary.ContainsKey(token) == true)
            {
                coRoutineSetDictionary[token].Start(name, routine);
            }
        }

        public bool StopCoRoutines(string token)
        {
            bool bDone = false;
            if (coRoutineSetDictionary.ContainsKey(token) == true)
            {
                bDone = coRoutineSetDictionary[token].StopAll();
            }
            if (bDone == true)
            {
                coRoutineSetDictionary.Remove(token);
            }
            return bDone;
        }

        public void StartCoRoutine(string name, IEnumerator routine)
        {
            StartCoRoutine(CurrentToken, name, routine);
        }

        public bool StopCoRoutines()
        {
            return StopCoRoutines(CurrentToken);
        }

        public IEnumerator WaitCoRoutines(string token)
        {
            bool bDone = false;
            if (coRoutineSetDictionary.ContainsKey(token) == true)
            {
                yield return coRoutineSetDictionary[token].WaitAll();
                coRoutineSetDictionary.Remove(token);
            }
            yield return null;
        }
        
        public string CurrentToken => tokens.Peek();

        
        public void PushToken(string token)
        {
            tokens.Push(token);
        }
        public IEnumerator PopToken()
        {
            var token = tokens.Peek();
            yield return WaitCoRoutines(token);

            if (tokens.Count > 1)
            {
                tokens.Pop();
            }

            yield return null;
        }

    }
}