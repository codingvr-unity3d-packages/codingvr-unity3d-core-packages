﻿using UnityEngine;
using System.IO;
using System;
using System.Security.Cryptography;

public static class md5
{
	private static byte[] ComputeMD5(string filename)
	{
		byte[] MD5String;
		using (var md5 = MD5.Create())
		{
			using (var stream = File.OpenRead(filename))
			{
				MD5String = md5.ComputeHash(stream);
			}
		}
		return MD5String;
	}

	private static string MakeMD5Filename(string filename)
	{
		filename = filename + ".md5";
		return filename;
	}

	private static byte[] ReadMD5FromStream(string filename)
	{
		var path = MakeMD5Filename(filename);
		return DataStreaming.ReadDataFromStream(path);
	}

	private static byte[] ReadMD5(string filename)
	{
		byte[] MDString = null;
		if (File.Exists(filename))
		{
			var path = MakeMD5Filename(filename);
			if (File.Exists(path))
			{
				using (var file = File.Open(path, FileMode.Open))
				{
					using (var writer = new BinaryReader(file))
					{
						MDString = writer.ReadBytes(16);
					}
				}
				Debug.LogWarning("MD5 read file:" + path + ":" + MDString);
			}
			else
			{
				Debug.LogError("MD5 file:" + path + " doesn't exist !!!");
			}
		}
		return MDString;
	}

	// #INTERFACE

	public static bool EqualMD5(string filename0, string filename1)
	{
		var md5_0 = ReadMD5(filename0);
		var md5_1 = ReadMD5FromStream(filename1);
		bool b = (md5_0 != null && md5_1 != null);
		if (b == true)
		{
			b = System.Collections.StructuralComparisons.StructuralEqualityComparer.Equals(md5_0, md5_1);
		}
		if (b == false)
		{
			Debug.LogWarning("MD5 comparing failed between file:" + filename0 + ":" + md5_0 + " and file:" + filename1 + ":" + md5_1 + " !!!");
		}
		return b;
	}

	public static void WriteMD5(string filename)
	{
		byte[] MDString = ComputeMD5(filename);
		var path = MakeMD5Filename(filename);
		using (var file = File.Open(path, FileMode.Create))
		{
			using (var writer = new BinaryWriter(file))
			{
				writer.Write(MDString);
				Debug.LogWarning("MD5 writing file:" + path + ":" + MDString);
			}
		}
	}
}
