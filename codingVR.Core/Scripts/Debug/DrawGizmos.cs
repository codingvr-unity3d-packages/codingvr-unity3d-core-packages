﻿using UnityEngine;

public static class DrawGizmos
{
	static public void DrawCube(Vector3 worldPos, Vector3 scale, Color color)
	{
		var restoreColor = color;
		worldPos.y += 0.5f;
		Gizmos.color = Color.white;
		Gizmos.DrawWireCube(worldPos, scale);
		Gizmos.color = color;
		Gizmos.DrawCube(worldPos, scale);

		Gizmos.color = restoreColor;
	}

	static public void DrawPlane(Vector3 worldPos, Vector3 scale, Color color, Color colorWire)
	{
		var restoreColor = color;
		scale.y = 0.05f;
		Gizmos.color = colorWire;
		Gizmos.DrawWireCube(worldPos, scale);
		Gizmos.color = color;
		Gizmos.DrawCube(worldPos, scale);
		Gizmos.color = restoreColor;
	}
	
	static public void DrawPlane(Vector3 worldPos, Vector3 scale, Color color)
	{
		var restoreColor = color;
		scale.y = 0.05f;
		Gizmos.color = Color.white;
		Gizmos.DrawWireCube(worldPos, scale);
		Gizmos.color = color;
		Gizmos.DrawCube(worldPos, scale);
		Gizmos.color = restoreColor;
	}

	private const float GIZMO_DISK_THICKNESS = 0.01f;
	public static void DrawDisk(Vector3 worldPos, Quaternion rotation, float radius, Color color)
	{
		Matrix4x4 oldMatrix = Gizmos.matrix;
		Gizmos.color = color;
		Gizmos.matrix = Matrix4x4.TRS(worldPos, rotation, new Vector3(1, 1, GIZMO_DISK_THICKNESS));
		Gizmos.DrawSphere(Vector3.zero, radius);
		Gizmos.matrix = oldMatrix;
	}

	/// <summary>
	/// Draws a wire cube with a given rotation 
	/// </summary>
	/// <param name="center"></param>
	/// <param name="size"></param>
	/// <param name="rotation"></param>
	public static void DrawWireCube(Vector3 center, Vector3 size, Color color, Quaternion? rotation = null)
	{
		var old = Gizmos.matrix;
		Gizmos.matrix = Matrix4x4.TRS(center, rotation??Quaternion.identity, size);
		Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
		Gizmos.matrix = old;
	}

	public static void DrawArrow(Vector3 from, Vector3 to, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
	{
		Gizmos.DrawLine(from, to);
		var direction = to - from;
		var right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
		var left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
		Gizmos.DrawLine(to, to + right * arrowHeadLength);
		Gizmos.DrawLine(to, to + left * arrowHeadLength);
	}

	public static void DrawWireSphere(Vector3 center, float radius, Color color, Quaternion? rotation)
	{
		var old = Gizmos.matrix;
		Gizmos.color = color;
		Gizmos.matrix = Matrix4x4.TRS(center, rotation ?? Quaternion.identity, Vector3.one);
		Gizmos.DrawWireSphere(Vector3.zero, radius);
		Gizmos.matrix = old;
	}

	public static void DrawSphere(Vector3 center, float radius, Color color, Quaternion? rotation)
	{
		var old = Gizmos.matrix;
		Gizmos.color = color;
		Gizmos.matrix = Matrix4x4.TRS(center, rotation ?? Quaternion.identity, Vector3.one);
		Gizmos.DrawSphere(Vector3.zero, radius);
		Gizmos.matrix = old;
	}

	/// <summary>
	/// Draws a flat wire circle (up)
	/// </summary>
	/// <param name="center"></param>
	/// <param name="radius"></param>
	/// <param name="segments"></param>
	/// <param name="rotation"></param>
	public static void DrawWireCircle(Vector3 center, float radius, Color color, int segments = 20, Quaternion? rotation = null)
	{
		DrawWireArc(center, radius, 360, color, segments, rotation);
	}

	/// <summary>
	/// Draws an arc with a rotation around the center
	/// </summary>
	/// <param name="center">center point</param>
	/// <param name="radius">radiu</param>
	/// <param name="angle">angle in degrees</param>
	/// <param name="segments">number of segments</param>
	/// <param name="rotation">rotation around the center</param>


	static Quaternion quaternion;	

	public static void DrawWireArc(Vector3 center, float radius, float angle, Color color, int segments = 20, Quaternion? rotation = null)
	{
		var old = Gizmos.matrix;
		Gizmos.color = color;
		Gizmos.matrix = Matrix4x4.TRS(center, rotation ?? Quaternion.identity, Vector3.one);
		Vector3 from = Vector3.forward * radius;
		var step = Mathf.RoundToInt(angle / segments);
		for (int i = 0; i <= angle; i += step)
		{
			var to = new Vector3(radius * Mathf.Sin(i * Mathf.Deg2Rad), 0, radius * Mathf.Cos(i * Mathf.Deg2Rad));
			Gizmos.DrawLine(from, to);
			from = to;
		}
		Gizmos.matrix = old;
	}


	/// <summary>
	/// Draws an arc with a rotation around an arbitraty center of rotation
	/// </summary>
	/// <param name="center">the circle's center point</param>
	/// <param name="radius">radius</param>
	/// <param name="angle">angle in degrees</param>
	/// <param name="segments">number of segments</param>
	/// <param name="rotation">rotation around the centerOfRotation</param>
	/// <param name="centerOfRotation">center of rotation</param>
	public static void DrawWireArc(Vector3 center, float radius, float angle, Color color, int segments, Quaternion rotation, Vector3 centerOfRotation)
	{
		var old = Gizmos.matrix;
		Gizmos.color = color;
		Gizmos.matrix = Matrix4x4.TRS(centerOfRotation, rotation, Vector3.one);
		var deltaTranslation = centerOfRotation - center;
		Vector3 from = deltaTranslation + Vector3.forward * radius;
		var step = Mathf.RoundToInt(angle / segments);
		for (int i = 0; i <= angle; i += step)
		{
			var to = new Vector3(radius * Mathf.Sin(i * Mathf.Deg2Rad), 0, radius * Mathf.Cos(i * Mathf.Deg2Rad)) + deltaTranslation;
			Gizmos.DrawLine(from, to);
			from = to;
		}

		Gizmos.matrix = old;
	}

	/// <summary>
	/// Draws an arc with a rotation around an arbitraty center of rotation
	/// </summary>
	/// <param name="matrix">Gizmo matrix applied before drawing</param>
	/// <param name="radius">radius</param>
	/// <param name="angle">angle in degrees</param>
	/// <param name="segments">number of segments</param>
	public static void DrawWireArc(Matrix4x4 matrix, float radius, float angle, Color color, int segments)
	{
		var old = Gizmos.matrix;
		Gizmos.color = color;
		Gizmos.matrix = matrix;
		Vector3 from = Vector3.forward * radius;
		var step = Mathf.RoundToInt(angle / segments);
		for (int i = 0; i <= angle; i += step)
		{
			var to = new Vector3(radius * Mathf.Sin(i * Mathf.Deg2Rad), 0, radius * Mathf.Cos(i * Mathf.Deg2Rad));
			Gizmos.DrawLine(from, to);
			from = to;
		}

		Gizmos.matrix = old;
	}

	/// <summary>
	/// Draws a wire cylinder face up with a rotation around the center
	/// </summary>
	/// <param name="center"></param>
	/// <param name="radius"></param>
	/// <param name="height"></param>
	/// <param name="rotation"></param>
	public static void DrawWireCylinder(Vector3 center, float radius, float height, Color color, Quaternion? rotation = null)
	{
		var old = Gizmos.matrix;
		Gizmos.color = color;
		Gizmos.matrix = Matrix4x4.TRS(center, rotation ?? Quaternion.identity, Vector3.one);
		var half = height / 2;

		//draw the 4 outer lines
		Gizmos.DrawLine(Vector3.right * radius - Vector3.up * half, Vector3.right * radius + Vector3.up * half);
		Gizmos.DrawLine(-Vector3.right * radius - Vector3.up * half, -Vector3.right * radius + Vector3.up * half);
		Gizmos.DrawLine(Vector3.forward * radius - Vector3.up * half, Vector3.forward * radius + Vector3.up * half);
		Gizmos.DrawLine(-Vector3.forward * radius - Vector3.up * half, -Vector3.forward * radius + Vector3.up * half);

		//draw the 2 cricles with the center of rotation being the center of the cylinder, not the center of the circle itself
		DrawWireArc(center + Vector3.up * half, radius, 360, color, 20, rotation ?? Quaternion.identity, center);
		DrawWireArc(center + Vector3.down * half, radius, 360, color, 20, rotation ?? Quaternion.identity, center);
		Gizmos.matrix = old;
	}

	/// <summary>
	/// Draws a wire capsule face up
	/// </summary>
	/// <param name="center"></param>
	/// <param name="radius"></param>
	/// <param name="height"></param>
	/// <param name="rotation"></param>
	public static void DrawWireCapsule(Vector3 center, float radius, float height, Color color, Quaternion? rotation_ = null)
	{
		Quaternion rotation = rotation_ ?? Quaternion.identity;
		var old = Gizmos.matrix;
		Gizmos.color = color;
		Gizmos.matrix = Matrix4x4.TRS(center, rotation, Vector3.one);
		var half = height / 2 - radius;

		//draw cylinder base
		DrawWireCylinder(center, radius, height - radius * 2, color, rotation);

		//draw upper cap
		//do some cool stuff with orthogonal matrices
		var mat = Matrix4x4.Translate(center + rotation * Vector3.up * half) * Matrix4x4.Rotate(rotation * Quaternion.AngleAxis(90, Vector3.forward));
		DrawWireArc(mat, radius, 180, color, 20);
		mat = Matrix4x4.Translate(center + rotation * Vector3.up * half) * Matrix4x4.Rotate(rotation * Quaternion.AngleAxis(90, Vector3.up) * Quaternion.AngleAxis(90, Vector3.forward));
		DrawWireArc(mat, radius, 180, color, 20);

		//draw lower cap
		mat = Matrix4x4.Translate(center + rotation * Vector3.down * half) * Matrix4x4.Rotate(rotation * Quaternion.AngleAxis(90, Vector3.up) * Quaternion.AngleAxis(-90, Vector3.forward));
		DrawWireArc(mat, radius, 180, color, 20);
		mat = Matrix4x4.Translate(center + rotation * Vector3.down * half) * Matrix4x4.Rotate(rotation * Quaternion.AngleAxis(-90, Vector3.forward));
		DrawWireArc(mat, radius, 180, color, 20);

		Gizmos.matrix = old;

	}
	static public void DrawString(string text, Vector3 worldPos, int textSize, Color color, FontStyle fontStyle = FontStyle.Bold)
	{
#if UNITY_EDITOR
		Camera camera = null;
		float height = 0.0f;
		// editor cam
		var view = UnityEditor.SceneView.currentDrawingSceneView;
		if (view != null)
		{
			camera = view.camera;
			height = view.position.height;
		}
		// engine cam
		if (camera == null)
		{
			camera = Camera.current;
			if (camera == null)
			{
				camera = Camera.main;
			}
			if (camera != null)
			{
				camera = Camera.main;
				height = Screen.height;
			}
		}

		// draw text
		if (camera != null)
		{
			worldPos.y += 1.0f;
			//camera = view.camera;

			Vector3 screenPos = camera.WorldToScreenPoint(worldPos);

			if (screenPos.y >= 0 && screenPos.y < Screen.height && screenPos.x >= 0 && screenPos.x < Screen.width && screenPos.z > 0)
			{
				UnityEditor.Handles.BeginGUI();

				// save state
				var restoreColor = GUI.color;
				var restoreFonSize = GUI.skin.label.fontSize;
				var restoreFonStyle = GUI.skin.label.fontStyle;

				// apply style
				GUI.skin.label.fontSize = textSize;
				GUI.skin.label.fontStyle = fontStyle;

				// draw outline text
				GUI.color = Color.black;
				Vector2 size = GUI.skin.label.CalcSize(new GUIContent(text));
				GUI.Label(new Rect(screenPos.x - (size.x / 2), -screenPos.y + height, size.x, size.y), text);

				// draw text
				color.a = 1.0f;
				GUI.color = color;
				size.x -= 3.0f;
				size.y -= 3.0f;
				GUI.Label(new Rect(screenPos.x - (size.x / 2), -screenPos.y + height, size.x, size.y), text);

				// restore
				GUI.color = restoreColor;
				GUI.skin.label.fontSize = restoreFonSize;
				GUI.skin.label.fontStyle = restoreFonStyle;

				UnityEditor.Handles.EndGUI();
			}
		}
#endif
	}
}
