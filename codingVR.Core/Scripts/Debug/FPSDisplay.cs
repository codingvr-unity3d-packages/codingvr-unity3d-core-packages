﻿using UnityEngine;
using System.Collections;

namespace codingVR.Core
{
    public class FPSDisplay : MonoBehaviour, IFPSDisplay
    {
        float deltaTime = 0.0f;

		void Update()
        {
			if (Enabled)
			{
				deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
			}
        }

        void OnGUI()
        {
			if (Enabled)
			{
				int w = Screen.width, h = Screen.height;

				GUIStyle style = new GUIStyle();

				Rect rect = new Rect(0, 0, w, h * 2 / 100);
				style.alignment = TextAnchor.UpperLeft;
				style.fontSize = h * 5 / 100;
				style.normal.textColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
				float msec = deltaTime * 1000.0f;
				float fps = 1.0f / deltaTime;
				string text = string.Format("v{0} | {1:0.0} ms ({2:0.} fps)", Application.version, msec, fps);
				GUI.Label(rect, text, style);
			}
        }

		// === IFPSDisplay ===

		public bool Enabled { get; set; } = false;
	}
}
