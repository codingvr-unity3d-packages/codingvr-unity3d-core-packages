﻿using System;
using UnityEditor;
using UnityEngine;

namespace codingVR.Core
{
	public class GameObjectDrawGizmos : MonoBehaviour
	{
		[SerializeField]
		Color gizmoColor = new Color(192, 255, 128, 64);

		public enum GizmoType
		{
			cube,
			sphere,
			disc,
		}

		[SerializeField]
		GizmoType gizmoType = GizmoType.cube;

		// sub title
		string subTitle;

		// Start is called before the first frame update
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{

		}

		public Vector3 Offset { get; set; }
		public string Title { get; set; }
		public Color Color
		{
			get
			{
				return gizmoColor;
			}
			set
			{
				if (value != gizmoColor)
				{
					gizmoColor = value;
				}
			}
		}

		public string SubTitle
		{
			get
			{
				return subTitle;
			}
			set
			{
				if (value != subTitle)
				{
					subTitle = value;
				}
			}
		}

		void drawGizmos(int scale, FontStyle fontStyle)
		{
			// gizmo handle
			switch (gizmoType)
			{
				case GizmoType.cube:
					DrawGizmos.DrawCube(transform.position, transform.localScale, gizmoColor);
					break;
				case GizmoType.sphere:
					DrawGizmos.DrawSphere(transform.position, Math.Max(Math.Max(transform.localScale.x, transform.localScale.y), transform.localScale.z), gizmoColor, null);
					break;
				case GizmoType.disc:
						DrawGizmos.DrawDisk(transform.position, Camera.current.transform.rotation, Math.Max(Math.Max(transform.localScale.x, transform.localScale.y), transform.localScale.z), gizmoColor);
					break;
			}

			// Title
			if (String.IsNullOrEmpty(Title) == false)
			{
				DrawGizmos.DrawString(Title, transform.position + Offset, scale, gizmoColor, fontStyle);
			}
			else
			{
				DrawGizmos.DrawString(name, transform.position + Offset, scale, gizmoColor, fontStyle);
			}

			// sub title
			if (string.IsNullOrEmpty(SubTitle) == false)
			{
				var w = transform.position.WorldToScreen();
				w = (w - Vector3.up * (float)scale).ScreenToWorld();
				DrawGizmos.DrawString("(" + SubTitle + ")", w + Offset, scale / 2, gizmoColor, fontStyle);
				
			}
		}

		void OnDrawGizmos()
		{
#if UNITY_EDITOR
			if (Selection.Contains(gameObject) == false)
			{
				drawGizmos(25, FontStyle.Bold);
			}
#endif
		}

		void OnDrawGizmosSelected()
		{
#if UNITY_EDITOR
			if (Selection.Contains(gameObject) == true)
			{
				drawGizmos(40, FontStyle.BoldAndItalic);
			}
#endif
		}
	}
}
