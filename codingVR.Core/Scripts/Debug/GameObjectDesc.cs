﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace codingVR.Core
{
    public class GameObjectDesc : MonoBehaviour
    {
        TMP_Text text = null;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void SetTextErrorDescription(string desc, float localScaling)
        {
            if (null == text)
            {
                text = GetComponentInChildren<TMP_Text>();
            }

            if (text != null)
            {
                text.text = desc;
                gameObject.transform.GetChild(0).transform.localScale = Vector3.one * localScaling;
                gameObject.transform.GetChild(0).transform.localPosition *= localScaling;
            }
        }
    }
}
