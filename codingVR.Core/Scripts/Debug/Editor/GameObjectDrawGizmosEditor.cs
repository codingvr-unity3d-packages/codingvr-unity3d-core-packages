﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Core
{
	using UnityEditor;

	[CustomEditor(typeof(GameObjectDrawGizmos))]
	public class GameObjectDrawGizmosEditor : Editor
	{
		public float circleSize = 1;

		// Custom in-scene UI for when ExampleScript
		// component is selected.
		public void OnSceneGUI()
		{
			var t = target as GameObjectDrawGizmos;
			var tr = t.transform;
			var pos = tr.position;
			// display a disc where the object is
			var color = new Color(0.0f, 1.0f, 0.0f, 1);
			Handles.color = color;
			Handles.DrawWireDisc(pos, tr.up, 1.0f);
			// display object "value" in scene
			GUI.color = color;
		}
	}
}