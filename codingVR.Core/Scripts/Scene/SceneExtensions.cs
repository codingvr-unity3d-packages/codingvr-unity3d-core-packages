﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class SceneExtensions
{
	public static void GetAllGameObjects(this Scene scene, out List<GameObject> allGameObjects, bool includeInactive = false)
	{
		GameObject[] rootGameObjects = scene.GetRootGameObjects();
		allGameObjects = new List<GameObject>(rootGameObjects);
		for (int a = 0; a < rootGameObjects.Length; a++)
		{
			//? This is kind of a hack because you can use recursive or other methods to access children via `Transform - GetChild()`. I haven't tested which method is more efficient.
			Transform[] childrenTransform = rootGameObjects[a].GetComponentsInChildren<Transform>(includeInactive);

			for (int b = 0; b < childrenTransform.Length; b++)
				allGameObjects.Add(childrenTransform[b].gameObject);
		}
	}

	public static GameObject[] GetAllGameObjects(this Scene scene, bool includeInactive = false)
	{
		scene.GetAllGameObjects(out List<GameObject> allGameObjects, includeInactive);
		return allGameObjects.ToArray();
	}
}