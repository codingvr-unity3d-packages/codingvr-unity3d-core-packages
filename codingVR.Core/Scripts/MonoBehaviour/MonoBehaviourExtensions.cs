﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class MonoBehaviourExtensions
{
    private static IEnumerator delayedAction(UnityAction action, int countFrame = 1)
    {
        yield return null; // optional
		for (int i = 0; i < countFrame; ++i)
		{
			yield return new WaitForEndOfFrame(); // Wait for the next frame
		}
        action.Invoke(); // execute a delegate
    }

	public static void DelayedFunction(this MonoBehaviour mono, UnityAction action, int countFrame = 1)
	{
		mono.StartCoroutine(delayedAction(action, countFrame));
	}

	private static IEnumerator delayedAction<T>(T gameObject, System.Action<T> action, int countFrame = 1)
    {
        yield return null; // optional
		for (int i = 0; i < countFrame; ++i)
		{
			yield return new WaitForEndOfFrame(); // Wait for the next frame
		}
        action.Invoke(gameObject); // execute a delegate
    }

    public static void DelayedFunction<T>(this MonoBehaviour mono, T gameObject, System.Action<T> action, int countFrame = 1)
    {
        mono.StartCoroutine(delayedAction<T>(gameObject, action, countFrame));
    }
}
