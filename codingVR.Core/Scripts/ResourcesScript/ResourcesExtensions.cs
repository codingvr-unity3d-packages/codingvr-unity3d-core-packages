﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static public class ResourcesExtensions
{
    public static string MakeResourcesPathFromAssetPath(string assetPath)
    {
        string path = "";
        if (assetPath.Length > 0)
        {
            // remove extension
            var Prefab = ".prefab";
            int index = assetPath.IndexOf(Prefab);
            if (index >= 0 && index < assetPath.Length)
            {
                assetPath = assetPath.Remove(index, Prefab.Length - 2);

				string removeSubString(string _subString)
				{
					string _ret = null;
					var Resources = _subString;
					int lengthResources = Resources.Length;
					int indexResources = assetPath.IndexOf(Resources);
					// only assets from resources are supported
					if (indexResources > 0)
						_ret = assetPath.Substring(indexResources + lengthResources, assetPath.Length - 2 - indexResources - lengthResources);
					return _ret;
				}

				// remove sub string up to "Resources/"
				var ret = removeSubString("Resources/");
				if (ret == null)
					ret = removeSubString("Addressables/");
				if (ret != null)
					path = ret;
			}
            else
            {
                //Debug.LogErrorFormat("{0} : this resources {1} is not a prefab", System.Reflection.MethodBase.GetCurrentMethod().Name, assetPath);
            }
        }
        return path;
    }
}
