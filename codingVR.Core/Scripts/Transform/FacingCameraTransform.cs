﻿using UnityEngine;

namespace codingVR.Core
{
	public class FacingCameraTransform : MonoBehaviour
	{
		public float rotateAroundUp = 0.0f;
		// Start is called before the first frame update
		void Start()
		{

		}

		// Update is called once per frame
		void LateUpdate()
		{
			// We have to ensure that character animation & position are finished before computing message show
			// see issue https://gitlab.com/monamipoto/potomaze/-/issues/189
			Transform camTransform = null;
			if (Camera.current != null)
			{
				camTransform = Camera.current.transform;
			}
			if (camTransform == null)
			{
				if (Camera.main != null)
				{
					camTransform = Camera.main.transform;
				}
			}
			
			if (camTransform != null)
			{
				transform.rotation = Quaternion.LookRotation(camTransform.forward, camTransform.transform.up);
				/// do the same, almost but the "up" can be handled in another way
				//transform.forward = camTransform.forward;
				//transform.LookAt(transform.position + camTransform.rotation * Vector3.forward,  camTransform.rotation * Vector3.up);
				if (Mathf.Approximately(0.0f, rotateAroundUp) == false)
				{
					transform.Rotate(Vector3.up * rotateAroundUp, Space.Self);
				}
			}
		}

		void Update()
		{
		}
	}
}