﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Core
{
    public class TransformHasChanged : MonoBehaviour
    {
        /*
        // Start is called before the first frame update
        void Start()
        {
            StartCoroutine(ResetTransformHasChanged());
        }

        IEnumerator ResetTransformHasChanged()
        {
            while (true)
            {
                yield return new WaitForFixedUpdate();
                if (transform.hasChanged)
                {
                    //Print("The transform has changed!");
                    transform.hasChanged = false;
                }
            }
            yield return null;
        }

        public bool HasChangedByObject(object whosAsking)
        {
            return transform.hasChanged;
        }
        */

        bool hasEverChanged = false;  //until the ctransform has changed at least once, none of the perObject stuff is needed
        HashSet<object> objectsThatCheckedSinceLastTimeChanged = new HashSet<object>();
        //bool b = false;
        /// <summary>
        /// This function will return the HasChanged value of this component's transform, uniquely for the 'whosAsking' object passed to this function.
        /// So, calling this function after the transform has changed, with the same object passed as the parameter- will yield true only the first time it's called, and false thereafter (until the transform changes again).
        /// But calling the function with different object parameters, would yield true for each different object passed (the first time is is passed- untill the transform changes again).
        /// </summary>
        /// <param name="whosAsking">This object will be noted internally, incase it asks again before the transform has actually changed.</param>
        /// <returns>True if the transform has changed since the last time the whosAsking object checked. False otherwise.</returns>
        public bool HasChangedByObject(object whosAsking)
        {
            //return transform.hasChanged;
            if (!hasEverChanged) return false;
            if (objectsThatCheckedSinceLastTimeChanged.Contains(whosAsking)) return false;
            objectsThatCheckedSinceLastTimeChanged.Add(whosAsking);
            return true;

        }

        IEnumerator coroutine;
        void Start()
        {
            transform.hasChanged = false;
            coroutine = EndOfFrameReset();
            StartCoroutine(coroutine);
        }

        IEnumerator EndOfFrameReset()
        {
            while (true)
            {
                yield return new WaitForEndOfFrame();
                if (transform.hasChanged)
                {
                    // we clear the hash set, so in HasChangedByObject will able to deliver true since the hash set is true
                    objectsThatCheckedSinceLastTimeChanged.Clear();
                    hasEverChanged = true;
                }
                transform.hasChanged = false;//set to false so we can detect the next time it changes
            }
            yield return null;
        }
    }
}