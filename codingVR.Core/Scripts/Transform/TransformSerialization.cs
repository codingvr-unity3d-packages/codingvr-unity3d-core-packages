﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Core
{
	[Serializable]
	public class SerializedTransform
	{
		public Vector3 _position;
		public Quaternion _rotation;
		public Vector3 _scale;

		public SerializedTransform(Transform transform, bool localSpace)
		{
			if (localSpace)
			{
				SerialLocalTransform(transform);
			}
			else
			{
				SerialWorldTransform(transform);
			}
		}

		public void DeserialLocalTransform(Transform _transform)
		{
			_transform.localPosition = _position;
			_transform.localRotation = _rotation;
			_transform.localScale = _scale;
		}
		public void DeserialWorldTransform(Transform _transform)
		{
			_transform.localPosition = _position;
			_transform.localRotation = _rotation;
			_transform.localScale = _scale;
		}
		public void SerialLocalTransform(Transform _transform)
		{
			_position = _transform.localPosition;
			_rotation = _transform.localRotation;
			_scale = _transform.localScale;
		}
		public void SerialWorldTransform(Transform _transform)
		{
			_position = _transform.position;
			_rotation = _transform.rotation;
			_scale = _transform.localScale;
		}
	}

	//Utilities

	public static class TransformExtention
	{
		public static void SetTransformEX(this Transform original, Transform copy)
		{
			original.position = copy.position;
			original.rotation = copy.rotation;
			original.localScale = copy.localScale;
		}
	}
}