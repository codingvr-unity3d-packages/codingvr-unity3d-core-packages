﻿using UnityEngine;

namespace codingVR.Core
{
	public class StoreTransform
	{
		public Vector3 position;
		public Quaternion rotation;
		public Vector3 localScale;
	}

	public static class TransformExtension
	{
		public static StoreTransform SaveLocal(this Transform aTransform)
		{
			return new StoreTransform()
			{
				position = aTransform.localPosition,
				rotation = aTransform.localRotation,
				localScale = aTransform.localScale,
			};
		}

		public static StoreTransform SaveWorld(this Transform aTransform)
		{
			return new StoreTransform()
			{
				position = aTransform.position,
				rotation = aTransform.rotation,
				localScale = aTransform.localScale,
			};
		}

		public static void LoadLocal(this Transform aTransform, StoreTransform aData)
		{
			aTransform.localPosition = aData.position;
			aTransform.localRotation = aData.rotation;
			aTransform.localScale = aData.localScale;
		}

		public static void LoadWorld(this Transform aTransform, StoreTransform aData)
		{
			aTransform.position = aData.position;
			aTransform.rotation = aData.rotation;
			aTransform.localScale = aData.localScale;
		}

		public static Rect ScreenRect(this Transform refPosition, Rect rect)
		{
			Vector3 tl = TransformExtension.ScreenToWorld(refPosition, rect.x, rect.y);
			Vector3 br = TransformExtension.ScreenToWorld(refPosition, rect.x + rect.width, rect.y + rect.height);
			return new Rect(tl.x, tl.y, br.x - tl.x, br.y - tl.y);
		}

		public static Vector3 ScreenToWorld(this Transform refPosition, float x, float y)
		{
			Camera camera = Camera.current;
			Vector3 s = camera.WorldToScreenPoint(refPosition.position);
			return camera.ScreenToWorldPoint(new Vector3(x, y, s.z));
		}
	}
}