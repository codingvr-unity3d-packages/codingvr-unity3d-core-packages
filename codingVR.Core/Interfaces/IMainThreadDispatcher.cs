﻿using System;
using System.Collections;
using System.Threading.Tasks;

namespace codingVR.Core
{
	public interface IMainThreadDispatcher
	{
		void Enqueue(IEnumerator action);
		void Enqueue(Action action);
		Task EnqueueAsync(Action action);
	}
}
