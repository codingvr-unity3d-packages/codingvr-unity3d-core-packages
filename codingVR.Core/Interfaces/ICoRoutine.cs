using System.Collections;

namespace codingVR.Core
{
    public delegate void Done(bool status);

    public interface ICoRoutineManager
    {
        // explicit token
        void StartCoRoutine(string token, string name, IEnumerator routine);
        bool StopCoRoutines(string token);

        // with implicit current token
        void StartCoRoutine(string name, IEnumerator routine);
        bool StopCoRoutines();

        // Wait Co Routines
        IEnumerator WaitCoRoutines(string token);

        // with tokens
        string CurrentToken { get; }
        void PushToken(string token);
        IEnumerator PopToken();
    }
}
