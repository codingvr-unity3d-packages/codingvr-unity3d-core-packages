﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Core
{
	public delegate void EventDelegate<T>(T e) where T : IGameEvent;
	public delegate void EventDelegate(IGameEvent e);

	public class IGameEvent
	{
		int Type { get; }
		object Data { get; }
		public EventDelegate Done { get; }

		public IGameEvent(EventDelegate done)
		{
			Done = done;
		}

		public IGameEvent()
		{
			Done = null;
		}
	}

	public interface IEventManager
	{
		// Listener
		void AddListener<T>(EventDelegate<T> del) where T : IGameEvent;
		void AddListenerOnce<T>(EventDelegate<T> del) where T : IGameEvent;
		void RemoveListener<T>(EventDelegate<T> del) where T : IGameEvent;
		bool HasListener<T>(EventDelegate<T> del) where T : IGameEvent;

		// Inserts the event into the current queue.
		bool QueueEvent(IGameEvent evt);

		// Administation
		void RemoveAll();

		// Stats
		int EventCount { get; }
		bool HasEvent(IGameEvent evt);
	};
}
