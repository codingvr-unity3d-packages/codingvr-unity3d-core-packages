﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace codingVR.Core
{
	public interface IFPSDisplay
	{
		bool Enabled{get; set;}
	}
}
